import 'dart:convert';
import 'package:camis_application_flutter/model/authenication_model/auth_model.dart';
import 'package:camis_application_flutter/service/base_url.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:http/http.dart' as http;

class AuthApi {
  final BaseUrl _baseUrl = BaseUrl();
  Future<AuthModel> signInRequestApi(
      {required String schoolCode,
      required String loginName,
      required String password,
      String? deviceToken,
      required String platform }) async {
    await FirebaseMessaging.instance.getToken().then((value) {
      deviceToken = value.toString();
      print("dsfasd$value");
    });
    http.Response response = await http.post(
      Uri.parse("${_baseUrl.adminBaseUrl}/student-access-school-login"),
      body: {
        "school_code": schoolCode,
        "loginname": loginName,
        "password": password,
        "device_token": deviceToken,
        "platform": platform
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      return AuthModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e.toString());
      });
    }
  }
}
