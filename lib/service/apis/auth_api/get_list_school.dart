import 'dart:convert';
import 'package:camis_application_flutter/model/authenication_model/list_school.dart';
import 'package:camis_application_flutter/service/base_url.dart';
import 'package:http/http.dart' as http;

class GetListSchoolApi {
  Future<ListSchoolModel> getListSchoolRequestApi() async {
    BaseUrl baseUrl = BaseUrl();
    http.Response response = await http.get(
      Uri.parse("${baseUrl.adminBaseUrl}/list-customer-school"),
      headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return ListSchoolModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}