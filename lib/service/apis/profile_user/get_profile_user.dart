import 'dart:convert';

import 'package:camis_application_flutter/model/base/base_respone_model.dart';
import 'package:camis_application_flutter/model/user_profile/user_profile.dart';
import 'package:camis_application_flutter/service/apis/base_request_api/base_requested_api.dart';
import 'package:camis_application_flutter/storages/get_storage.dart';
import 'package:http/http.dart' as http;
import '../../../model/user_profile/user_guideline.dart';

class GetProfileUserApi extends BaseRequestOwnerApi {
//  Future<BaseResponseModel<UserInfoModel>> getProfileUserApi() => get(
//     "/get-profile",
//     fromJsonT: (json)  {
//       return UserInfoModel.fromJson(json);
//     } 
//   );

  Future<UserInfoModel> getProfileUserApi() async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/get-profile"),
      headers: <String, String>{
          "Accept": "application/json",
          "Content-Type":"application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return UserInfoModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }

  Future<UserGuidelineModel> getUserguidelineApi() async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/guideline/query"),
      headers: <String, String>{
          "Accept": "application/json",
          "Content-Type":"application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("fasdfasd"+response.body);
      return UserGuidelineModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}