import 'dart:convert';
import 'dart:io';
import 'package:camis_application_flutter/model/user_profile/update_child_profile.dart';
import 'package:camis_application_flutter/storages/get_storage.dart';
import 'package:http/http.dart' as http;
class UpdateProfileChildApi {
  Future<UpdateChildModel> updateChildProfileImageRequestApi({
  required File image,
  required String idChild ,
  required String fristname,
  required String lastname,
  required String fristnameEn,
  required String lastnameEn,
  required String gender,
  required String dob,}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;

    var request = http.MultipartRequest(
        'POST', Uri.parse(
            "${dataaccess.accessUrl}/update-profile-children/$idChild"),
      );
      request.fields.addAll({
      });
      Map<String, String> headers = {
        "Content-type": "multipart/form-data",
        "Content-Type":"application/json",
        "Authorization":"Bearer${dataaccess.accessToken}",
      };
      request.fields.addAll({
        "firstname":fristname,
        "lastname":lastname,
        "firstname_en": fristnameEn,
        "lastname_en": lastnameEn,
        "gender":gender,
        "dob":dob,
      });
      if (image != null) {
        request.files.add(
          http.MultipartFile(
            'file',
            image.readAsBytes().asStream(),
            image.lengthSync(),
            filename: image.path.split('/').last,
          ),
        );
      }
      request.headers.addAll(headers);
      var res = await request.send();
      http.Response response = await http.Response.fromStream(res);
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("${response.body}");
      return UpdateChildModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }

  Future<UpdateChildModel> updateProfileChildRequestApi({
    required String fristname,
    required String lastname,
    required String fristnameEn,
    required String lastnameEn,
    required String gender,
    required String dob, 
    required String idChild
    }) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/update-profile-children/$idChild"),
      headers: <String, String>{
          "Accept": "application/json",
          "Content-Type":"application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
      body: jsonEncode(<String, String>{
          "firstname":fristname,
          "lastname":lastname,
          "firstname_en": fristnameEn,
          "lastname_en": lastnameEn,
          "gender":gender,
          "dob":dob,
      })
    ); 
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("${response.body}");
      return UpdateChildModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}