part of 'get_profile_user_bloc.dart';

abstract class GetProfileUserState extends Equatable {
  const GetProfileUserState();
  
  @override
  List<Object> get props => [];
}

class GetProfileUserInitial extends GetProfileUserState {}

class ProfileUserLoadingState extends GetProfileUserState {
  final String message;

  const ProfileUserLoadingState({
    required this.message,
  });
}

class ProfileUserLoaded extends GetProfileUserState{
  final UserInfoModel profileusermodel;
  const ProfileUserLoaded({required this.profileusermodel});
}

class ProfileUserErrorState extends GetProfileUserState {
  final String error;

  const ProfileUserErrorState({
    required this.error,
  });
}
