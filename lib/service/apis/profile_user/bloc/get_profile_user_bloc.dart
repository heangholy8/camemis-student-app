import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:camis_application_flutter/model/base/base_respone_model.dart';
import 'package:camis_application_flutter/model/user_profile/user_profile.dart';
import 'package:camis_application_flutter/model/user_infor_model.dart';
import 'package:camis_application_flutter/service/apis/profile_user/get_profile_user.dart';
import 'package:camis_application_flutter/storages/save_storage.dart';
import 'package:equatable/equatable.dart';
part 'get_profile_user_event.dart';
part 'get_profile_user_state.dart';

class GetProfileUserBloc
 extends Bloc<GetProfileUserEvent, GetProfileUserState> {
  final GetProfileUserApi getprofileuserapi;
  GetProfileUserBloc({required this.getprofileuserapi})
      : super(GetProfileUserInitial()) {
    on<GetProfileUser>((event, emit) async {
      emit(const ProfileUserLoadingState(message: "Loading....."));
      try {
        var data = await getprofileuserapi.getProfileUserApi();
        emit(ProfileUserLoaded(profileusermodel: data));
      } catch (e) {
        emit(const ProfileUserErrorState(error: "Error data"));
      }
    });
  }
}
