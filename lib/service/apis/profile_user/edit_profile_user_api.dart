import 'dart:convert';
import 'dart:io';
import 'package:camis_application_flutter/storages/get_storage.dart';
import 'package:http/http.dart' as http;
import '../../../model/account_confrim_model/phone_verify_model.dart';
import '../../../model/user_profile/update_model.dart';
import '../../../model/user_profile/user_profile.dart';
class UpdateProfileUserApi {
  Future<UpdateUserModel> updateUserProfileImageRequestApi({
    required File image,
    }) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;

    var request = http.MultipartRequest(
        'POST', Uri.parse(
            "${dataaccess.accessUrl}/update-profile-information"),
      );
      request.fields.addAll({
      });
      Map<String, String> headers = {
        "Content-type": "multipart/form-data",
        "Content-Type":"application/json",
        "Authorization":"Bearer${dataaccess.accessToken}",
      };
      if (image != null) {
        request.files.add(
          http.MultipartFile(
            'file',
            image.readAsBytes().asStream(),
            image.lengthSync(),
            filename: image.path.split('/').last,
          ),
        );
      }
      request.headers.addAll(headers);
      var res = await request.send();
      http.Response response = await http.Response.fromStream(res);
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("${response.body}");
      return UpdateUserModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }

  Future<PhonnVerifyModel> updateUserPhoneRequestApi({
    required String phone,
    }) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/verification/verify-guardian-number"),
      headers: <String, String>{
          "Accept": "application/json",
          "Content-Type":"application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
      body: jsonEncode(<String, String>{
          "phone":phone,
      })
    ); 
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("${response.body}");
      return PhonnVerifyModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print("fjasf$e");
      });
    }
  }
}