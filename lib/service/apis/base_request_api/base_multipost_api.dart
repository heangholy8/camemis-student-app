import 'dart:io';
import 'package:camis_application_flutter/app/modules/auth_screens/e_school_code.dart';
import 'package:http/http.dart' as http;
import '../../../storages/get_storage.dart';

class BaseGuardianMultiPostApi with Toast {
  Future multipostApi({
    required String firstname,
    required String lastName,
    required String firstNameEn,
    required String lastNameEn,
    required String email,
    required String dob,
    required String phoneNum,
    required File file,
  }) async {
    GetStoragePref _jsonToken = GetStoragePref();
    var token = await _jsonToken.getJsonToken;
    var request = http.MultipartRequest('POST',
        Uri.parse("${token.accessUrl.toString()}/update-profile-information"));
    request.fields['firstname'] = firstname;
    request.fields['lastname'] = lastName;
    request.fields['firstname_en'] = firstNameEn;
    request.fields['lastname_en'] = lastNameEn;
    request.fields['email'] = email;
    request.fields['dob'] = dob;
    request.fields['phone'] = phoneNum;
    request.files.add(
      http.MultipartFile.fromBytes(
        'file',
        File(file.path).readAsBytesSync(),
        filename: file.path,
      ),
    );

    Map<String, String> headers = {
      "Authorization": '${token.tokenType}${token.accessToken}',
      "Content-type": "multipart/form-data",
      "Accept": "application/json",
    };
    request.headers.addAll(headers);
    print("request: " + request.toString());
    var res = await request.send();
    http.Response response = await http.Response.fromStream(res);
    print("sinatiansdf" + res.toString());
    print("response: " + response.toString());

    // request.files.add(http.MultipartFile.fromBytes(
    //   'picture',
    //   File(file.path).readAsBytesSync(),
    //   filename: file.path.split("/").last
    // ));
  }
}

// class BaseRequestGuardApi with Toast {
//   final BaseUrl _baseUrl = BaseUrl();
//   Future<BaseResponseModel<T>> gget<T>(
//     String path, {
//     required T Function(dynamic json) fromJsonT,
//   }) async {
//     GetStoragePref _jsonToken = GetStoragePref();
//     var token = await _jsonToken.getJsonToken;
//     // print to know request is calling
//     //print('\n\n=> [GET] ${token.accessUrl}$path requesting...\n');
//     try {
//       // 1. call api service
//       final response = await http.get(
//         Uri.parse(token.accessUrl.toString() + path),
//         headers: {
//           "Accept": "application/json",
//           "Content-Type": "application/json",
//           "Authorization": '${token.tokenType}${token.accessToken}'
//         },
//       );
//       print(
//           '\n\n=> Response [GET] from: ${token.accessUrl}$path\n=> status: ${response.statusCode}\n=> body: ${response.body}\n');
//       // 3. check if access token was expired
//       if (response.statusCode == 401) {
//         print("Token Expired !! => Please login your account again ");
//         return BaseResponseModel<T>(
//             isSuccess: false,
//             message: "Token Expired !! => Please login your account again");
//       }
//       // 4.convert from string json to Map
//       var data = json.decode(response.body);
//       // 5. check status code if it's equal 200 or 201 it's request ok
//       if (response.statusCode == 200 || response.statusCode == 201) {
//         // + add is_success to know on BaseResponse object success or
//         data['is_success'] = true;
//         // + return BasesResponse
//         return BaseResponseModel.fromJson(data, fromJsonT);
//       }
//       // + not success return base response with error message
//       return BaseResponseModel<T>(
//         message: data['message'],
//         isSuccess: false,
//       );
//       // ignore: nullable_type_in_catch_clause
//     } on SocketException {
//       return BaseResponseModel<T>(
//           isSuccess: false, message: 'Problem Internet Connection');
//     } on HttpException {
//       return BaseResponseModel<T>(isSuccess: false, message: 'Not Found');
//     } on FormatException {
//       return BaseResponseModel<T>(
//           isSuccess: false, message: "Something when wrong");
//     }
//   }

//   Future<BaseResponseModel<T>> gpost<T>(
//     String path, {
//     Object? body,
//     required T Function(dynamic json) fromJsonT,
//   }) async {
//     GetStoragePref _jsonToken = GetStoragePref();
//     var token = await _jsonToken.getJsonToken;
//     try {
//       final response = await http.post(
//         Uri.parse("${token.accessUrl}" + path.trim()),
//         body: body,
//         headers: <String, String>{
//           "Authorization": "${token.tokenType}${token.accessToken}".trim(),
//           // "Accept": "application/json",
//           // "Content-Type": "application/json",
//         },
//       );
//       print(
//           '\nResponse [POST] from: $_baseUrl$path\nstatus: ${response.statusCode}\nbody: ${response.body}');
//       if (response.statusCode == 401) {
//         print("Token Expired !! => Please login your account again ");
//       }
//       var data = json.decode(response.body);
//       if (response.statusCode == 200 || response.statusCode == 201) {
//         data['is_success'] = true;

//         return BaseResponseModel.fromJson(data, fromJsonT);
//       }

//       return BaseResponseModel<T>(
//         message: data['message'],
//         isSuccess: false,
//       );
//     } on SocketException {
//       return BaseResponseModel<T>(
//           isSuccess: false, message: 'Problem Internet Connection');
//     } on HttpException {
//       return BaseResponseModel<T>(isSuccess: false, message: 'Not Found');
//     } on FormatException {
//       return BaseResponseModel<T>(
//           isSuccess: false, message: "Something when wrong!!");
//     } catch (e) {
//       throw Exception(e.toString());
//     }
//   }

//   Future<BaseResponseModel<T>> hpost<T>(
//     String path, {
//     Object? body,
//     required T Function(dynamic json) fromJsonT,
//   }) async {
//     GetStoragePref _jsonToken = GetStoragePref();
//     var token = await _jsonToken.getJsonToken;
//     try {
//       final response = await http.post(
//         Uri.parse("${token.accessUrl}" + path.trim()),
//         body: body,
//         headers: <String, String>{
//           "Authorization": "${token.tokenType}${token.accessToken}".trim(),
//           "Accept": "application/json",
//           "Content-type": "multipart/form-data"
//         },
//       );
//       print(
//           '\nResponse [POST] from: $_baseUrl$path\nstatus: ${response.statusCode}\nbody: ${response.body}');
//       if (response.statusCode == 401) {
//         print("Token Expired !! => Please login your account again ");
//       }
//       var data = json.decode(response.body);
//       if (response.statusCode == 200 || response.statusCode == 201) {
//         data['is_success'] = true;

//         return BaseResponseModel.fromJson(data, fromJsonT);
//       }

//       return BaseResponseModel<T>(
//         message: data['message'],
//         isSuccess: false,
//       );
//     } on SocketException {
//       return BaseResponseModel<T>(
//           isSuccess: false, message: 'Problem Internet Connection');
//     } on HttpException {
//       return BaseResponseModel<T>(isSuccess: false, message: 'Not Found');
//     } on FormatException {
//       return BaseResponseModel<T>(
//           isSuccess: false, message: "Something when wrong!!");
//     } catch (e) {
//       throw Exception(e.toString());
//     }
//   }

//   multipartProdecudre(String path) async {
//     GetStoragePref _jsonToken = GetStoragePref();
//     var token = await _jsonToken.getJsonToken;
//     var request = http.MultipartRequest(
//         'POST', Uri.parse("${token.accessUrl}" + path.trim()));

//     request.headers.addAll({
//       "Authorization": "${token.tokenType}${token.accessToken}".trim(),
//       "Accept": "application/json",
//       "Content-Type": "application/json",
//     });
//     request.files
//         .add(await http.MultipartFile.fromPath("", "image_path/video/path"));
//     var response = await request.send();
//     var responsed = await http.Response.fromStream(response);
//     final responseData = json.decode(responsed.body);
//     if (response.statusCode == 200) {
//       print("SUCCESS");
//       print(responseData);
//     } else {
//       print("ERROR");
//     }
//   }

//   Future<dynamic> postMultiImage(
//       {required String path, required File selectedImage}) async {
//     GetStoragePref _jsonToken = GetStoragePref();
//     var token = await _jsonToken.getJsonToken;
//     var request = http.MultipartRequest(
//       'POST',
//       Uri.parse("${token.accessUrl}" + path.trim()),
//     );
//     Map<String, String> headers = {"Content-type": "multipart/form-data"};
//     request.files.add(
//       http.MultipartFile(
//         'image',
//         selectedImage.readAsBytes().asStream(),
//         selectedImage.lengthSync(),
//         filename: selectedImage.path.split('/').last,
//       ),
//     );
//     request.fields.addAll(headers);
//   }

//   // Future<BaseResponseModel<T>> request<T>(
//   //   String method,
//   //   String path, {
//   //   Object? body,
//   //   http.MultipartFile? multipartFile,
//   //   required T Function(dynamic json) fromJsonT,
//   // }) async {
//   //   try {
//   //     // 1. call api service
//   //     final request = http.MultipartRequest(
//   //       method,
//   //       Uri.parse(_baseUrl + path),
//   //     );
//   //     request.headers
//   //         .assignAll({"Authorization": 'Bearer ${token.accessToken}'});
//   //     if (body != null) {
//   //       request.fields.assignAll(body as Map<String, String>);
//   //     }
//   //     if (multipartFile != null) {
//   //       request.files.add(multipartFile);
//   //     }
//   //     final response = await request.send();
//   //     final dataAsString = await response.stream.bytesToString();
//   //     // 2. print response to know have any issue or not
//   //     print(
//   //         '\nResponse from: $_baseUrl$path\nstatus: ${response.statusCode}\nbody: $dataAsString');
//   //     // 3. check if access token was expired
//   //     if (response.statusCode == 401) {
//   //       await Storage().refreshToken();
//   //       return post(path, body: body, fromJsonT: fromJsonT);
//   //     }
//   //     // 4.convert from string json to Map
//   //     var data = json.decode(dataAsString);
//   //     // 5. check status code if it's equal 200 or 201 it's request ok
//   //     if (response.statusCode == 200 || response.statusCode == 201) {
//   //       // + add is_success to know on BaseResponse object success or
//   //       data['is_success'] = true;
//   //       // + return BasesResponse
//   //       return BaseResponseModel.fromJson(data, fromJsonT);
//   //     }
//   //     // + not success return base response with error message
//   //     return BaseResponseModel<T>(
//   //       message: data['message'],
//   //       isSuccess: false,
//   //     );
//   //   } on SocketException {
//   //     // + alert dialog when no internet
//   //     showErrorDialog(
//   //         title: 'No Internet',
//   //         content: 'Please, check your internet connection.');
//   //     // + return base response with message no internet
//   //     return BaseResponseModel<T>(
//   //         isSuccess: false, message: 'Problem Internet Connection');
//   //   } on HttpException {
//   //     // + return base response with message not found
//   //     return BaseResponseModel<T>(isSuccess: false, message: 'Not Found');
//   //   } on FormatException {
//   //     // + alert dialog
//   //     showErrorDialog(title: 'Error', content: LocaleKeys.sth_wrong.tr);
//   //     // + return base response with message sth when wrong
//   //     return BaseResponseModel<T>(
//   //         isSuccess: false, message: LocaleKeys.sth_wrong.tr);
//   //   }
//   // }
// }
