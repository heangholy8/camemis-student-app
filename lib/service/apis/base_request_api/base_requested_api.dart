// ignore_for_file: avoid_print
import 'dart:convert';
import 'dart:io';
import 'package:camis_application_flutter/service/base_url.dart';
import 'package:camis_application_flutter/storages/get_storage.dart';
import 'package:http/http.dart' as http;
import '../../../app/modules/auth_screens/e_school_code.dart';

import '../../../model/base/base_respone_model.dart';

class BaseRequestOwnerApi with Toast {
  final BaseUrl _baseUrl = BaseUrl();
  Future<BaseResponseModel<T>> get<T>(
    String path, {
    required T Function(dynamic json) fromJsonT,
  }) async {
    GetStoragePref _jsonToken = GetStoragePref();
    var token = await _jsonToken.getJsonToken;
    // print to know request is calling
    //print('\n\n=> [GET] ${token.accessUrl}$path requesting...\n');
    try {
      // 1. call api service
      final response = await http.get(
        Uri.parse(token.accessUrl.toString() + path),
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": '${token.tokenType}${token.accessToken}'
        },
      );
      print(
          '\n\n=> Response [GET] from: ${token.accessUrl}$path\n=> status: ${response.statusCode}\n=> body: ${response.body}\n');
      // 3. check if access token was expired
      if (response.statusCode == 401) {
        print("Token Expired !! => Please login your account again ");
      }
      // 4.convert from string json to Map
      var data = json.decode(response.body);
      // 5. check status code if it's equal 200 or 201 it's request ok
      if (response.statusCode == 200 || response.statusCode == 201) {
        // + add is_success to know on BaseResponse object success or
        data['is_success'] = true;
        // + return BasesResponse
        return BaseResponseModel.fromJson(data, fromJsonT);
      }

      if (response.statusCode == 500) {
        print("Internal Server Error");
      }
      // + not success return base response with error message
      return BaseResponseModel<T>(
        message: data['message'],
        isSuccess: false,
      );
    } on SocketException {
      return BaseResponseModel<T>(
          isSuccess: false, message: 'Problem Internet Connection');
    } on HttpException {
      return BaseResponseModel<T>(isSuccess: false, message: 'Not Found');
    } on FormatException {
      return BaseResponseModel<T>(
          isSuccess: false, message: "Something when wrong");
    }
  }

  Future<BaseResponseModel<T>> post<T>(
    String path, {
    Object? body,
    required T Function(dynamic json) fromJsonT,
  }) async {
    GetStoragePref _jsonToken = GetStoragePref();
    var token = await _jsonToken.getJsonToken;
    try {
      final response = await http.post(
        Uri.parse(_baseUrl.adminBaseUrl + path),
        body: body,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          "Authorization": '${token.tokenType}${token.accessToken}',
        },
      );

      print(
          '\nResponse [POST] from: $_baseUrl$path\nstatus: ${response.statusCode}\nbody: ${response.body}');

      if (response.statusCode == 401) {}

      var data = json.decode(response.body);

      if (response.statusCode == 200 || response.statusCode == 201) {
        data['is_success'] = true;

        return BaseResponseModel.fromJson(data, fromJsonT);
      }

      return BaseResponseModel<T>(
        message: data['message'],
        isSuccess: false,
      );
    } on SocketException {
      return BaseResponseModel<T>(
          isSuccess: false, message: 'Problem Internet Connection');
    } on HttpException {
      return BaseResponseModel<T>(isSuccess: false, message: 'Not Found');
    } on FormatException {
      return BaseResponseModel<T>(
          isSuccess: false, message: "Something when wrong!!");
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<bool?> uploadImage({File? file, String? url}) async {
    GetStoragePref _jsonToken = GetStoragePref();
    var token = await _jsonToken.getJsonToken;
    var uri = Uri.parse('http://18.224.86.8:4000/api/v1/posts/add');
    http.MultipartRequest request = http.MultipartRequest('POST', uri);
    request.headers.addAll({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      "Authorization": '${token.tokenType}${token.accessToken}',
    });
  }

  Future<BaseResponseModel<T>> request<T>(
    String method,
    String path, {
    Object? body,
    http.MultipartFile? multipartFile,
    required T Function(dynamic json) fromJsonT,
  }) async {
    GetStoragePref _jsonToken = GetStoragePref();
    var token = await _jsonToken.getJsonToken;
    try {
      // 1. call api service
      final request = http.MultipartRequest(
        method,
        Uri.parse("$_baseUrl/$path"),
      );
      request.headers.addAll({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        "Authorization": '${token.tokenType}${token.accessToken}',
      });
      if (body != null) {
        request.fields.addAll(body as Map<String, String>);
      }
      if (multipartFile != null) {
        request.files.add(multipartFile);
      }
      final response = await request.send();
      final dataAsString = await response.stream.bytesToString();
      // 2. print response to know have any issue or not
      print(
          '\nResponse from: $_baseUrl$path\nstatus: ${response.statusCode}\nbody: $dataAsString');
      var data = json.decode(dataAsString);

      if (response.statusCode == 200 || response.statusCode == 201) {
        data['is_success'] = true;

        return BaseResponseModel.fromJson(data, fromJsonT);
      }

      return BaseResponseModel<T>(
        message: data['message'],
        isSuccess: false,
      );
    } on SocketException {
      return BaseResponseModel<T>(
          isSuccess: false, message: 'Problem Internet Connection');
    } on HttpException {
      return BaseResponseModel<T>(isSuccess: false, message: 'Not Found');
    } on FormatException {
      return BaseResponseModel<T>(
          isSuccess: false, message: "Something when wrong!");
    }
  }
}
