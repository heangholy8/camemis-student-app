
import 'dart:convert';

import 'package:camis_application_flutter/model/base/base_respone_model.dart';
import 'package:camis_application_flutter/service/apis/base_request_api/base_requested_api.dart';
import '../../../model/get_children_model/get_children_model.dart';
import '../../../model/get_children_model/get_detail_childmodel.dart';
import '../../../model/result/list_month.dart';
import '../../../storages/get_storage.dart';
import 'package:http/http.dart' as http;

class GetChildrenApi extends BaseRequestOwnerApi {
  Future<ChildrenModel> getChildrenApi() async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/get-children"),
      headers: <String, String>{
          "Accept": "application/json",
          "Content-Type":"application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return ChildrenModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
  // Future<BaseResponseModel<List<ChildrenModel>>>? getChildrenApi() => get(
  //       "/get-childrn",
  //       fromJsonT: (json) => List<ChildrenModel>.from(
  //         json.map(
  //           (e) => ChildrenModel.fromJson(e),
  //         ),
  //       ),
  //     );

  Future<GetDetailChildModel> getDetailChildApi({required String childid}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/get-children/$childid"),
      headers: <String, String>{
          "Accept": "application/json",
          "Content-Type":"application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return GetDetailChildModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }

  // Future<BaseResponseModel<GetDetailChildModel>> getDetailChildApi({required String childid}) => get(
  //   "/get-childre/$childid",
  //   fromJsonT: (json)  {
  //     return GetDetailChildModel.fromJson(json);
  //   } 
  // );
  Future<BaseResponseModel<ListMonthModel>> getListMonthApi({required String childid}) => get(
    "/get-children-all-detail-list/$childid",
    fromJsonT: (json)  {
      return ListMonthModel.fromJson(json);
    } 
  );
}
