import 'dart:convert';

import "package:http/http.dart" as http;
import '../../../storages/get_storage.dart';
class PostSeenTransection{
  Future<http.Response> postReact(String tranId) async {
      GetStoragePref _pref = GetStoragePref();
      var auth = await _pref.getJsonToken;
      return http.post(Uri.parse("${auth.accessUrl}/payment-guardian/seen-transaction".trim()),
      headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "${auth.tokenType}${auth.accessToken}"
        },
        body: jsonEncode(<String, String>{
          "transaction_id": tranId.toString(),
        }),
      );
  }
}