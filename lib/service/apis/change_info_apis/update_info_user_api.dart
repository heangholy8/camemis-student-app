import 'dart:convert';
import 'dart:io';
import 'package:camis_application_flutter/model/base/base_respone_model.dart';
import '../../../model/user_profile/user_profile.dart';
import 'package:http/http.dart' as http;
import '../base_request_api/base_guardian_api.dart';

class UpdateUserInfoApi extends BaseRequestGuardApi {
  Future<BaseResponseModel<UserInfoModel>?> updateInfoGuardApi({
    required String firstname,
    required String lastname,
    required String firstnameEn,
    required String lastnameEn,
    required String dob,
    required String phone,
    required String email,
    required String gender,
    required File? file,
    String? address,
  }) {
    http.MultipartFile? multipartFile;
    if (file != null) {
      multipartFile = http.MultipartFile(
        'file',
        file.readAsBytes().asStream(),
        file.lengthSync(),
        filename: file.path.split('/').last,
      );
    }
    return request(
      "POST",
      "/update-profile-information",
      body: <String, String>{
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "phone": phone,
        "dob": dob,
        "gender": gender,
        "email": email,
        "address": address.toString()
      },
      multipartFile: multipartFile,
      fromJsonT: ((json) => UserInfoModel.fromJson(json)),
    );
  }

  Future<BaseResponseModel<UserInfoModel>?> updateInfoGuardApiNotimage({
    required String firstname,
    required String lastname,
    required String firstnameEn,
    required String lastnameEn,
    required String dob,
    required String phone,
    required String email,
    required String address,
    required String gender,
  }) {
    http.MultipartFile? multipartFile;
    return request(
      "POST",
      "/update-profile-information",
      body: <String, String>{
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "phone": phone,
        "dob": dob,
        "email": email,
        "gender": gender,
        "address": address.toString()
      },
      multipartFile: multipartFile,
      fromJsonT: ((json) => UserInfoModel.fromJson(json)),
    );
  }
}
