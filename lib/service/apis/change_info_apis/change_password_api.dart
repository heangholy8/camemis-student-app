import 'dart:convert';
import 'package:camis_application_flutter/model/base/base_respone_model.dart';
import 'package:camis_application_flutter/service/apis/base_request_api/base_guardian_api.dart';
import 'package:camis_application_flutter/storages/get_storage.dart';
import 'package:camis_application_flutter/storages/key_storage.dart';
import 'package:http/http.dart' as http;
import '../../../model/change_password_model.dart';

class ChangeUserInformationApi extends BaseRequestGuardApi with keyStoragePref {
  Future<ChangePassModel?> changePasswordApi(
      {required String currentPassword,
      required String newPassword,
      required String confirmPassword}) async {
    try {
      GetStoragePref _pref = GetStoragePref();
      var auth = await _pref.getJsonToken;

      http.Response response = await http.post(
        Uri.parse("${auth.accessUrl}/change-password".trim()),
        headers: <String, String>{
          // "Accept": "application/json",
          // "Content-Type": "application/json",
          "Authorization": "${auth.tokenType}${auth.accessToken}"
        },
        body: <String, String>{
          "current_password": currentPassword.toString(),
          "password": newPassword.toString(),
          "password_confirmation": confirmPassword.toString(),
        },
      );

      if (response.statusCode == 200 || response.statusCode == 201) {
        print(response.body);
        return ChangePassModel.fromJson(jsonDecode(response.body));
      } else {
        throw Exception("Change Password was Failed");
      }
    } catch (e) {
      print(e.toString());
    }
  }

  // Future<BaseResponseModel<ChangePassModel>?> changePassword({
  //   required String currentPassword,
  //   required String newPassword,
  //   required String confirmPassword,
  // }) async {
  //   GetStoragePref _pref = GetStoragePref();
  //   var auth = await _pref.getJsonToken;
  //   var data = await gpost(
  //     "${auth.accessUrl}/change-password".trim(),
  //     body: <String, String>{
  //       "current_password": currentPassword,
  //       "password": newPassword,
  //       "password_confirmation": confirmPassword
  //     },
  //     fromJsonT: (json) => ChangePassModel.fromJson(json),
  //   );
  //   print(
  //       "sfkjasld;fkjasdf${currentPassword + newPassword + confirmPassword + data.message.toString()}");
  // }

  Future<BaseResponseModel<ChangePassModel>> changePassword({
    required String currentPassword,
    required String newPassword,
    required String confirmPassword,
  }) async {
    GetStoragePref _jsonToken = GetStoragePref();
    var token = await _jsonToken.getJsonToken;
    try {
      final response = await http.post(
        Uri.parse("${token.accessUrl}/change-password"),
        body: jsonEncode(<String, String>{
          "current_password": currentPassword,
          "password": newPassword,
          "password_confirmation": confirmPassword,
        }),
        headers: <String, String>{
          "Authorization": "${token.tokenType}${token.accessToken}".trim(),
          "Accept": "application/json",
          "Content-Type": "application/json",
        },
      );

      var data = json.decode(response.body);
      if (response.statusCode == 200 || response.statusCode == 201) {
        data['is_success'] = true;

        return BaseResponseModel<ChangePassModel>.fromJson(data, data);
      }
      return BaseResponseModel<ChangePassModel>(
        message: data['message'],
        isSuccess: false,
      );
    } catch (e) {
      throw Exception(e.toString());
    }
  }
}
