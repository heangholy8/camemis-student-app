import 'package:camis_application_flutter/model/teaching_model/detail_teaching_model.dart';
import 'package:camis_application_flutter/model/teaching_model/homework_model.dart';
import 'package:camis_application_flutter/model/teaching_model/lesson_model.dart';
import 'package:camis_application_flutter/model/teaching_model/onlinelive_model.dart';

import '../../../model/base/base_respone_model.dart';
import '../../../model/teaching_model/teaching_model.dart';
import '../base_request_api/base_guardian_api.dart';

class TeachingApi extends BaseRequestGuardApi {
  Future<BaseResponseModel<TeachingAndLearningModel>> getTeachingApi(
          {required String classId,
          required String subjectId,
          required String month,
          required String year}) async =>
      gpost(
        "/teaching-learning/get-by-month",
        body: {
          "class_id": classId.trim(),
          "subject_id": subjectId.trim(),
          "month": month.trim(),
          "year": year.trim()
        },
        fromJsonT: (json) => TeachingAndLearningModel.fromJson(json),
      );

  Future<BaseResponseModel<DetailTeachingModel>> getDetailTeachingApi(
      {required String studentId,required String lessonId}) async {
    return gpost(
      "/teaching-learning/get-detail",
      body: <String, String>{
        "student_id": studentId,
        "lesson_assignment_id": lessonId,
      },
      fromJsonT: (json) => DetailTeachingModel.fromJson(json),
    );
  }

  // Homework api requesting
  Future<BaseResponseModel<HomeWorkModel>> getHomeWorkApi({required String studentId,required String lessonId})async{
    return gpost(
      "/teaching-learning/get-detail",
      body: <String, String>{
        "student_id": studentId,
        "lesson_assignment_id": lessonId,
      },
      fromJsonT: (json) => HomeWorkModel.fromJson(json),
    );
  }

  // Lesson api reqesting
  Future<BaseResponseModel<LessonModel>> getLessonApi({required String studentId,required String lessonId})async{
      return gpost(
        "/teaching-learning/get-detail",
        body: <String, String>{
          "student_id": studentId,
          "lesson_assignment_id": lessonId,
        },
        fromJsonT: (json) => LessonModel.fromJson(json),
      );
    }
  
  // Online Learning api requesting
  Future<BaseResponseModel<OnlineLiveModel>> getOnlineLearningApi({required String studentId,required String lessonId})async{
    return gpost(
      "/teaching-learning/get-detail",
      body: <String, String>{
        "student_id": studentId,
        "lesson_assignment_id": lessonId,
      },
      fromJsonT: (json) => OnlineLiveModel.fromJson(json),
    );
  }
}
