import 'dart:convert';
import 'dart:io';
import 'package:camis_application_flutter/mixins/toast.dart';
import 'package:camis_application_flutter/model/base/base_respone_model.dart';
import 'package:camis_application_flutter/storages/get_storage.dart';
import 'package:http/http.dart' as http;

class BaseUserRequestApi with Toast {
  Future<BaseResponseModel<T>> post<T>(
    String path, {
    Object? body,
    required T Function(dynamic json) fromJsonT,
  }) async {
    GetStoragePref _pref = GetStoragePref();
    var auth = await _pref.getJsonToken;

    try {
      final respone = await http.post(
        Uri.parse("$path/${auth.schoolGuid}"),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "${auth.tokenType}${auth.accessToken}"
        },
        body: body,
      );
      print("$path/${auth.schoolGuid}");
      var data = json.decode(respone.body);

      if (respone.statusCode == 200 || respone.statusCode == 201) {
        data['is_success'] = true;
        return BaseResponseModel.fromJson(data, fromJsonT);
      } else {
        print("error api");
        return BaseResponseModel<T>(
          message: data['message'],
          isSuccess: false,
        );
      }
    } on SocketException {
      // Show dialogbox
      return BaseResponseModel<T>(
          isSuccess: false, message: 'Problem Internet Connection');
    } on HttpException {
      // + return base response with message not found
      return BaseResponseModel<T>(isSuccess: false, message: 'Not Found');
    } on FormatException {
      // + alert dialog
      // + return base response with message sth when wrong
      return BaseResponseModel<T>(
          isSuccess: false, message: "Something when wrong!");
    }
  }
}
