import 'package:camis_application_flutter/model/reply_comment_model/reply_model.dart';

import '../../../model/base/base_respone_model.dart';
import '../base_request_api/base_guardian_api.dart';

class ReplyCommentApi extends BaseRequestGuardApi {
  // Homework api requesting
  Future<BaseResponseModel<ReplyCommentModel>> postReplyCommentApi(
      {required String id,
      required String parentId,
      required String reply}) async {
    return gpost(
      "/teaching-learning/post-forum/$id",
      body: <String, String>{
        "comment": reply,
        "parent": parentId,
      },
      fromJsonT: (json) => ReplyCommentModel.fromJson(json),
    );
  }
}
