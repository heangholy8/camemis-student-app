import 'dart:convert';
import 'package:camis_application_flutter/model/timeline/class_timeline_model.dart';
import 'package:camis_application_flutter/storages/get_storage.dart';
import 'package:http/http.dart' as http;
import '../../../model/timeline/detail_school_time_line_model.dart';
import '../../../model/timeline/school_timeline.dart';
import '../base_request_api/base_requested_api.dart';

class GetSchoolTimeLineApi extends BaseRequestOwnerApi {

  Future<SchoolTimelineModel> getSchoolTimelineApi(
      {required int page, required int limit}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse(
          "${dataaccess.accessUrl}/school-time-line?page=$page&limit=$limit"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization": "Bearer${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      return SchoolTimelineModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
       print('Debug getSchoolTimeline API: $e');
      });
    }
  }

  Future<ClassTimeLineModel> getClassTimelineApi(
      {required String classId,required int page,required int limit}) async {
     GetStoragePref prefs = GetStoragePref();
  
    var dataaccess = await prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse(
          "${dataaccess.accessUrl}/class-time-line/$classId?page=$page&limit=$limit"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization": "Bearer${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      return ClassTimeLineModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print('Debug getClassTimeline API: $e');
      });
    }
  }

  Future<GetDetailSchoolTimelineModel> getClassTimelineDetailApi(
      {required String classId, required String postId}) async {
    GetStoragePref prefs = GetStoragePref();
    var dataaccess = await prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/class-time-line/$classId/$postId".trim()),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization": "Bearer${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      return GetDetailSchoolTimelineModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print("Debug detailSchoolTimeline API: $e");
      });
    }
  }

  Future<GetDetailSchoolTimelineModel> getSchoolTimelineDetailApi(
      {required int postId}) async {
    GetStoragePref prefs = GetStoragePref();
    var dataaccess = await prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/school-time-line/$postId"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization": "Bearer${dataaccess.accessToken}",
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      return GetDetailSchoolTimelineModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print("Debug : $e");
      });
    }
  }
}
