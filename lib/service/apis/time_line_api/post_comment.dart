import 'dart:convert';

import "package:http/http.dart" as http;
import '../../../storages/get_storage.dart';
class PostComment{
  Future<http.Response> postComment(String url,String comment,int idPost) async {
      GetStoragePref _pref = GetStoragePref();
      var auth = await _pref.getJsonToken;
      return http.post(Uri.parse("${auth.accessUrl}$url$idPost".trim()),
      headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "${auth.tokenType}${auth.accessToken}"
        },
        body: jsonEncode(<String, String>{
          "post_text": comment,
        }),
        
      
      );
  }
}