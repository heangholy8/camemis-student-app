import 'dart:convert';
import 'package:camis_application_flutter/storages/get_storage.dart';
import 'package:http/http.dart' as http;
import '../../../model/teacher_list_model/teacher_list_model.dart';

class GetTeacherListApi {
  Future<TeacherListModel> getTeacherRequestApi({required String? idClass}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/list-subjects-teachers-in-class/$idClass"),
      headers: <String, String>{
          "Accept": "application/json",
          "Content-Type":"application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return TeacherListModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}