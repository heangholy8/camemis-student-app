import 'package:camis_application_flutter/model/base/base_respone_model.dart';
import 'package:camis_application_flutter/model/create_payment_model/create_payment_model.dart';
import '../base_request_api/base_guardian_api.dart';

class CreatePaymentApi extends BaseRequestGuardApi {
  // Future<BaseResponseModel<CreatePaymentData>> getPaymentData({
  //   required int chosepaymentoption,
  // }) async {
  //   try {
  //     GetStoragePref _pref = GetStoragePref();
  //     var auth = await _pref.getJsonToken;

  //     http.Response response = await http.post(
  //       Uri.parse(auth.accessUrl.toString() +
  //           "/payment-guardian/create-payment".trim()),
  //       headers: <String, String>{
  //         "Accept": "application/json",
  //         "Content-Type": "application/json",
  //         "Authorization": "${auth.tokenType}${auth.accessToken}".trim()
  //       },
  //       body: jsonEncode(<String, int>{
  //         "choose_pay_option": chosepaymentoption,
  //       }),
  //     );

  //     if (response.statusCode == 200 || response.statusCode == 201) {
  //       print(" asdfasdfafsd: ${response.body}");
  //       return BaseResponseModel<CreatePaymentData>.fromJson(
  //           jsonDecode(response.body),
  //           (json) => CreatePaymentData.fromJson(json.data));
  //     } else {
  //       throw Exception("CreatePayemnt was Failed ${response.body}");
  //     }
  //   } catch (e) {
  //     print(e.toString());
  //   }
  //   throw Exception("Get Monthly Result was Failed");
  // }

  Future<BaseResponseModel<CreatePaymentModel>> getPaymentData({
    required String choosepaymentoption,
    required String guardinaId,
  }) async =>
      gpost("/payment-guardian/create-payment",
          body: <String, String>{
            "choose_pay_option": choosepaymentoption,
            "guardian_id":guardinaId,
          },
          fromJsonT: (json) => CreatePaymentModel.fromJson(json));
}
