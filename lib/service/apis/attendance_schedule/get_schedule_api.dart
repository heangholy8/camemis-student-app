import 'dart:convert';
import 'package:camis_application_flutter/storages/get_storage.dart';
import 'package:http/http.dart' as http;
import '../../../model/attendance_schedule/get_attendance_schedule_by_day_model.dart';

class GetScheduleApi {
  Future<ScheduleModel> getScheduleRequestApi({required String? date,required String? cladsId,required String? childId}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/attendance-schedule/get-by-day"),
      headers: <String, String>{
          "Accept": "application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
      body: {
        "date": date,
        "student_id": childId,
        "class_id": cladsId,
      }
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return ScheduleModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}