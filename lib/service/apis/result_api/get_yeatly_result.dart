import 'dart:convert';
import '../../../model/result/yearly_result.dart';
import '../../../storages/get_storage.dart';
import '../../base_url.dart';
import 'package:http/http.dart' as http;

import '../base_user_api/base_user_respone_api.dart';

class YearResultApi extends BaseUserRequestApi {
  final GetStoragePref _getStoragePref = GetStoragePref();
  final BaseUrl _baseUrl = BaseUrl();

  Future<YearResultModel> getYearResultApi({
    required String studenId,
    required String classId,
  }) async {
    try {
      GetStoragePref _pref = GetStoragePref();
      var auth = await _pref.getJsonToken;

      http.Response response = await http.post(
        Uri.parse(
            auth.accessUrl.toString() + "/get-learning-result-in-year".trim()),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "${auth.tokenType}${auth.accessToken}".trim()
        },
        body: jsonEncode(<String, String>{
          "student_id": studenId,
          "class_id": classId,
        }),
      );

      if (response.statusCode == 200 || response.statusCode == 201) {
        print("Term Data : ${response.body}");
        return YearResultModel.fromJson(jsonDecode(response.body));
      } else {
        throw Exception("Get Monthly Result was Failed ${response.body}");
      }
    } catch (e) {
      print(e.toString());
    }
    throw Exception("Get Monthly Result was Failed");
  }
}
