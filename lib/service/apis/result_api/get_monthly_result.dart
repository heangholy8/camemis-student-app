import 'dart:convert';

import 'package:camis_application_flutter/model/result/monthly_result.dart';
import '../../../model/base/base_respone_model.dart';
import '../../../storages/get_storage.dart';
import '../../base_url.dart';
import 'package:http/http.dart' as http;

import '../base_user_api/base_user_respone_api.dart';

// class MonthlyResultApi {
//   final BaseUrl _baseUrl = BaseUrl();
//   Future<MonthlyResultModel> monthlyResult({
//     required String studenId,
//     required String classId,
//     required String month,
//     required String term,
//   }) async {
//     http.Response response = await http.post(
//       Uri.parse("${_baseUrl.guardianBaseUrl}/get-learning-result-by-month"),
//       body: {
//         "student_id": "38551833374350007371",
//         "class_id": "210",
//         "month": "2",
//         "term": "FIRST_SEMESTER",
//       },
//     );
//     if (response.statusCode == 200 || response.statusCode == 201) {
//       return MonthlyResultModel.fromJson(jsonDecode(response.body));
//     } else {
//       throw Exception((e) {
//         print(e.toString());
//       });
//     }
//   }
// }

class MonthlyResultApi extends BaseUserRequestApi {
  final GetStoragePref _getStoragePref = GetStoragePref();
  final BaseUrl _baseUrl = BaseUrl();

  Future<MonthlyResultModel> getMonthlyResultApi({
    required String studenId,
    required String classId,
    required String month,
    required String term,
  }) async {
    try {
      GetStoragePref _pref = GetStoragePref();
      var auth = await _pref.getJsonToken;

      http.Response response = await http.post(
        Uri.parse(
            auth.accessUrl.toString() + "/get-learning-result-by-month".trim()),
        headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "${auth.tokenType}${auth.accessToken}".trim()
        },
        body: jsonEncode(<String, String>{
          "student_id": studenId,
          "class_id": classId,
          "month": month,
          "term": term,
        }),
      );

      if (response.statusCode == 200 || response.statusCode == 201) {
        print("Data : ${response.body}");
        return MonthlyResultModel.fromJson(jsonDecode(response.body));
      } else {
        throw Exception("Get Monthly Result was Failed ${response.body}");
      }
    } catch (e) {
      print(e.toString());
    }
    throw Exception("Get Monthly Result was Failed");
  }
}
