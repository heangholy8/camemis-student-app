import 'dart:convert';
import 'package:camis_application_flutter/service/base_url.dart';
import 'package:http/http.dart' as http;
import '../../../model/check_version_update_model/check_update.dart';
import '../../../storages/get_storage.dart';

class CheckUpdateVersionApi {
  final BaseUrl _baseUrl = BaseUrl();
  Future<CheckVersionUpdateModel> checkUpdateVersionApi() async {
     GetStoragePref _prefs = GetStoragePref();
    http.Response response = await http.get(
      Uri.parse("${_baseUrl.adminBaseUrl}/get-current-app-version/2"),
      headers: <String, String>{
          "Accept": "application/json",
          "Content-Type": "application/json",
          "APPKEY": "L2tPsd3PJ26RQwuaQvNVyhabWqVcdE",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return CheckVersionUpdateModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}