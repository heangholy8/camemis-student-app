import 'dart:convert';

import 'package:camis_application_flutter/model/payment_option_model/current_payment_model.dart';
import 'package:http/http.dart' as http;
import '../../../storages/get_storage.dart';

class PaymentCurrentApi {
  Future<PaymentCurrentModel> getPaymentCurrentApi() async {
     GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/payment-guardian/get-current-payment"),
      headers: <String, String>{
          "Accept": "application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return PaymentCurrentModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}