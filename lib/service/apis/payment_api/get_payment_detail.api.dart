import 'package:camis_application_flutter/model/payment_detail_model/payment_detail.dart';

import '../../../model/base/base_respone_model.dart';
import '../base_request_api/base_requested_api.dart';

class PaymentDetailApi extends BaseRequestOwnerApi {
  Future<BaseResponseModel<PaymentDetailData>> getPaymentDetailApi(
          int paymentID) =>
      get("/payment-guardian/get-payment/$paymentID", fromJsonT: (json) {
        return PaymentDetailData.fromJson(json);
      });
}
