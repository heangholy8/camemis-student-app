import 'dart:convert';
import 'package:camis_application_flutter/model/payment/check_payment.dart';
import 'package:http/http.dart' as http;
import '../../../storages/get_storage.dart';

class PaymentExpireApi {
  Future<CheckPaymentModel> checkPaymentExpireApi() async {
     GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${dataaccess.accessUrl}/payment-guardian/check-expire"),
      headers: <String, String>{
          "Accept": "application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return CheckPaymentModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}