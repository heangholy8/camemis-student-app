import 'dart:convert';
import 'package:http/http.dart' as http;

import '../../../model/payment_option_model/aba_deep_link_model.dart';
import '../../../storages/get_storage.dart';

class GetABADeepLinkApi {
  Future<AbaDeepLinkModel> getABADeepLinkRequestApi({required int? typePayment,required String guardianId}) async {
     GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/payment-guardian/aba-payment-deep-link"),
      headers: <String, String>{
          "Accept": "application/json",
          "Authorization":"Bearer${dataaccess.accessToken}",
        },
      body: {
        "choose_pay_option" : typePayment.toString(),
        "guardian_id" : guardianId.toString(),
      }
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return AbaDeepLinkModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}