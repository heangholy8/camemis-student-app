import 'package:camis_application_flutter/model/base/base_respone_model.dart';
import 'package:camis_application_flutter/model/payment_option_model/payment_option_model.dart';
import 'package:camis_application_flutter/service/apis/base_request_api/base_requested_api.dart';

class PaymentOptionApi extends BaseRequestOwnerApi {
  Future<BaseResponseModel<List<PaymentOptionData>>> getPaymentOptionApi() =>
      get("/payment-guardian/list-payment-price",
          fromJsonT: (json) => List<PaymentOptionData>.from(
                json.map(
                  (e) => PaymentOptionData.fromJson(e),
                ),
              ));
}
