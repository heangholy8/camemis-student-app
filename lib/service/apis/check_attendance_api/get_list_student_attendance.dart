import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../../../model/check_attendance_model/check_attendance_model.dart';
import '../../../model/check_attendance_model/respon_attendace_model.dart';
import '../../../storages/get_storage.dart';
import '../../base_url.dart';

class GetCheckStudentAttendanceList {
  final BaseUrl _baseUrl = BaseUrl();
  Future<CheckAttendanceModel> getCheckAttendanceList(
      {required String? class_id,
      required String? schedule_id,
      required String? date}) async {
    GetStoragePref _prefs = GetStoragePref();
    var dataaccess = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${dataaccess.accessUrl}/schedule/get-list-student-attendance"),
      headers: <String, String>{
        "Accept": "application/json",
        "Authorization": "Bearer ${dataaccess.accessToken}",
      },
      body: {
        "class_id": class_id,
        "schedule_id": schedule_id,
        "date": date,
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return CheckAttendanceModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print("Error:" + e);
      });
    }
  }

  Future<ResponCheckAttendanceModel> postAttendanceApi({Data? data}) async {
    GetStoragePref _prefs = GetStoragePref();
    var accessURL = await _prefs.getJsonToken;
   
    http.Response response = await http.post(
      Uri.parse("${accessURL.accessUrl}/schedule/save-list-student-attendance"),
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer ${accessURL.accessToken}",
        "Content-Type": "application/json"
      },
      body: json.encode(data),
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("alksjflaksdfj" + response.body);
      return ResponCheckAttendanceModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        debugPrint("Error Post Attendance : $e");
        return e.toString();
      });
    }
  }
}
