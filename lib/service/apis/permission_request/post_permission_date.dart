import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:camis_application_flutter/service/apis/base_request_api/base_guardian_api.dart';
import '../../../model/base/base_respone_model.dart';
import '../../../model/permission_request_model/permissioo_detail.dart';

class PostPermission extends BaseRequestGuardApi{
  Future<BaseResponseModel<GetPermissionRequestDetailModel>>? postPermissionDayApi({
    required String studentId,
    required String comment,
    required String startDate,
    required String endDate,
    required String reason,
    //required File? file,
  }) {
    http.MultipartFile? multipartFile;
    // if (file != null) {
    //   multipartFile = http.MultipartFile(
    //     'file',
    //     file.readAsBytes().asStream(),
    //     file.lengthSync(),
    //     filename: file.path.split('/').last,
    //   );
    // }
    return request(
      "POST",
      "/request-absence-permission/add-permission-request",
      body: <String, String>{
        "request_type": "2",
        "student_id": studentId,
        "start_date": startDate,
        "end_date": endDate,
        "reason": reason,
        "comment": comment,
      },
      multipartFile: multipartFile,
      fromJsonT: ((json) => GetPermissionRequestDetailModel.fromJson(json)),
    );
  }

  Future<BaseResponseModel<GetPermissionRequestDetailModel>>? postPermissionSchaduleApi({
    required String studentId,
    required String comment,
    required String startDate,
    required String endDate,
    required List scheduleid,
    required String reason,
    //required File? file,
  }) {
    http.MultipartFile? multipartFile;
    // if (file != null) {
    //   multipartFile = http.MultipartFile(
    //     'file',
    //     file.readAsBytes().asStream(),
    //     file.lengthSync(),
    //     filename: file.path.split('/').last,
    //   );
    // }
    return request(
      "POST",
      "/request-absence-permission/add-permission-request",
      body: <String, String>{
        "request_type": "1",
        "student_id": studentId,
        "start_date": startDate,
        "end_date": endDate,
        "schedule_id[]":json.encode(scheduleid),
        "reason": reason,
        "comment": comment,
      },
      multipartFile: multipartFile,
      fromJsonT: ((json) => GetPermissionRequestDetailModel.fromJson(json)),
    );
  }
}