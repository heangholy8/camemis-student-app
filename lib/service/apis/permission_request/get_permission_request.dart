import '../../../model/base/base_respone_model.dart';
import '../../../model/permission_request_model/permission_request.dart';
import '../../../model/permission_request_model/permissioo_detail.dart';
import '../base_request_api/base_requested_api.dart';

class GetPermissionRequestApi extends BaseRequestOwnerApi {
  Future<BaseResponseModel<List<GetPermissionRequestModel>>>? getPermissionRequestApi({required String status,required String schoolYearId}) => get(
        "/request-absence-permission/query-permission-request?status=$status&schoolyear_id=$schoolYearId",
        fromJsonT: (json) => List<GetPermissionRequestModel>.from(
          json.map(
            (e) => GetPermissionRequestModel.fromJson(e),
          ),
        ),
      );
  Future<BaseResponseModel<GetPermissionRequestDetailModel>> getDetailPermissionApi({required String idPermission}) => get(
    "/request-absence-permission/get-detail-permission-request/$idPermission",
    fromJsonT: (json)  {
      return GetPermissionRequestDetailModel.fromJson(json);
    }
    
  );
}