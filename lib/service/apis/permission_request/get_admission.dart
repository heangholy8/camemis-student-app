import '../../../model/base/base_respone_model.dart';
import '../../../model/permission_request_model/admission_model.dart';
import '../base_request_api/base_requested_api.dart';

class GetadmissionApi extends BaseRequestOwnerApi {
  Future<BaseResponseModel<List<GetAdmissionModel>>>? getadmissionApi() => get(
        "/request-absence-permission/get-admission-type-data",
        fromJsonT: (json) => List<GetAdmissionModel>.from(
          json.map(
            (e) => GetAdmissionModel.fromJson(e),
          ),
        ),
      );
}