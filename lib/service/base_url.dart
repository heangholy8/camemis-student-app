// ignore_for_file: non_constant_identifier_names

class BaseUrl {
  final String _admin_base_url =
      "https://api-services.camemis-learn.com/api/v1/admin";
  final String _student_base_url =
      "https://api-services.camemis-learn.com/api/v1/admin";

  String get adminBaseUrl => _admin_base_url;
  String get studentBaseUrl => _student_base_url;
}
