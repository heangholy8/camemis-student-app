import 'package:bloc/bloc.dart';
import 'package:camis_application_flutter/service/apis/check_update_api/check_update.dart';
import 'package:equatable/equatable.dart';

import '../../../model/check_version_update_model/check_update.dart';
part 'check_version_update_event.dart';
part 'check_version_update_state.dart';

class CheckVersionUpdateBloc extends Bloc<CheckVersionUpdateEvent, CheckVersionUpdateState> {
  final CheckUpdateVersionApi checkUpdateVersion;
  CheckVersionUpdateBloc({required this.checkUpdateVersion}) : super(CheckVersionUpdateInitial()) {
    on<CheckVersionUpdate>((event, emit) async{
      emit(CheckVersionUpdateLoading());
      try{
        var data = await checkUpdateVersion.checkUpdateVersionApi();
        emit(CheckVersionUpdateLoaded(checkUpdateModel:data));
      }catch(e){
        emit(CheckVersionUpdateError());
      }
    });
  }
}
