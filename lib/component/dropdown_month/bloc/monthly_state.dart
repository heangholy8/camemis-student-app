part of 'monthly_bloc.dart';

abstract class MonthlyState extends Equatable {
  const MonthlyState();

  @override
  List<Object> get props => [];
}

class MonthlyInitial extends MonthlyState {}

class MonthlyLoadingState extends MonthlyState {}

class MonthlyLoadedState extends MonthlyState {
  final GetDetailChildModel? currentMonthly;
  const MonthlyLoadedState({required this.currentMonthly});
}

class SpecificChildLoadedState extends MonthlyState {
  final GetDetailChildModel? specificChildModel;
  const SpecificChildLoadedState({required this.specificChildModel});
}

class MonthlyErrorState extends MonthlyState {
  final String error;
  const MonthlyErrorState({required this.error});
}
