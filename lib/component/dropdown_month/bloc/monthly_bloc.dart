import 'package:bloc/bloc.dart';
import 'package:camis_application_flutter/model/base/base_respone_model.dart';
import 'package:camis_application_flutter/model/get_children_model/get_detail_childmodel.dart';
import 'package:camis_application_flutter/storages/save_storage.dart';
import 'package:equatable/equatable.dart';
import '../../../service/apis/get_children_api/get_children_api.dart';
import '../../../storages/get_storage.dart';

part 'monthly_event.dart';
part 'monthly_state.dart';

class MonthlyBloc extends Bloc<MonthlyEvent, MonthlyState> {
  final GetChildrenApi currenApi;
  final GetStoragePref _pref = GetStoragePref();
  final SaveStoragePref _savePref = SaveStoragePref();
  BaseResponseModel<GetDetailChildModel>? specificChildModel =
      BaseResponseModel<GetDetailChildModel>();
  MonthlyBloc({required this.currenApi}) : super(MonthlyLoadingState()) {
    on<GetDetailChildMonthEvent>((event, emit) async {
      
      emit(MonthlyLoadingState());
      try {
      var data=  await currenApi.getDetailChildApi(childid: event.childID);
      emit(MonthlyLoadedState(currentMonthly:data));
      } catch (e) {
        emit(MonthlyErrorState(error: e.toString()));
      }
    });
    on<GetSpecificChildEvent>((event, emit) async {
      var idChild = "";
      emit(MonthlyLoadingState());
      try {
        await currenApi
            .getDetailChildApi(childid: idChild.toString())
            .then((value) {
          emit(SpecificChildLoadedState(specificChildModel: value));
          
        });
      } catch (e) {
        emit(MonthlyErrorState(error: e.toString()));
      }
    });
  }
}
