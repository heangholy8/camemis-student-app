part of 'monthly_bloc.dart';

abstract class MonthlyEvent extends Equatable {
  const MonthlyEvent();

  @override
  List<Object> get props => [];
}


class GetDetailChildMonthEvent extends MonthlyEvent{
  final String childID;
  const GetDetailChildMonthEvent({required this.childID});
}


class GetSpecificChildEvent extends MonthlyEvent{
  
}
