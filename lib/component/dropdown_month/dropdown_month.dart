

import 'package:camis_application_flutter/component/dropdown_month/bloc/monthly_bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../app/modules/auth_screens/e_school_code.dart';
import '../../widgets/model_bottomSheet.dart';

class DropdowmMonth extends StatefulWidget {
  const DropdowmMonth({Key? key}) : super(key: key);

  @override
  State<DropdowmMonth> createState() => _DropdowmMonthState();
}

class _DropdowmMonthState extends State<DropdowmMonth> {
  @override
  void initState() {
    super.initState();
    setState(() {
      BlocProvider.of<MonthlyBloc>(context).add(GetDetailChildMonthEvent(childID: "37854342273749328133"));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          color: const Color(0xFFF1F9FF),
          borderRadius: BorderRadius.circular(12.0)),
      child: BlocBuilder<MonthlyBloc, MonthlyState>(
        builder: (context, state) {
          if (state is MonthlyLoadingState) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is SpecificChildLoadedState) {
            var data = state.specificChildModel!.data;
            return MaterialButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12.0),
              ),
              padding: const EdgeInsets.all(0.0),
              onPressed: () {
                showModalBottomSheet(
                    backgroundColor: Colors.transparent,
                    isScrollControlled: true,
                    context: context,
                    isDismissible: true,
                    builder: (context) {
                      return BuildBottomSheet(
                        initialChildSize: 0.55,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              padding: const EdgeInsets.only(top: 10.0),
                              alignment: Alignment.center,
                              child: Align(
                                child: Text("PLEASESELECTMONTH".tr(),
                                    style: ThemeConstands.texttheme.headline6!
                                        .copyWith(color: Colorconstands.black)),
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.symmetric(
                                  horizontal: 30, vertical: 20),
                              child: Align(
                                child: Text(
                                  "Excepteur sint occaecat cupidatat non proident, sunt in culpa",
                                  style: ThemeConstands.texttheme.subtitle2!
                                      .copyWith(color: Colorconstands.black),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            Divider(
                              height: 2,
                              thickness: 1,
                              color: Colorconstands.darkGray.withOpacity(0.1),
                            )
                          ],
                        ),
                        expanded: ListView.builder(
                            physics: const NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            padding: const EdgeInsets.all(0),
                            itemCount: data!.listMonths!.length,
                            itemBuilder: (context, index) {
                              return Column(
                                  children: List.generate(
                                      data.listMonths![index].months!.length +
                                          1, (subindex) {
                                var count = subindex !=
                                    data.listMonths![index].months!.length;
                          
                                return Container(
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    color: Colorconstands.white,
                                    border: Border(
                                      bottom: BorderSide(
                                        color: Colorconstands.darkGray
                                            .withOpacity(0.1),
                                        width: 1,
                                      ),
                                    ),
                                  ),
                                  height: 50,
                                  child: Container(
                                    margin: const EdgeInsets.symmetric(
                                        horizontal: 15.0),
                                    child: ListTile(
                                      title: Text(
                                        count
                                            ? data.listMonths![index]
                                                .months![subindex].displayMonth
                                                .toString()
                                            : data.listMonths![index].name
                                                .toString(),
                                        style: ThemeConstands
                                            .texttheme.headline6!
                                            .copyWith(
                                                fontWeight: FontWeight.normal,
                                                color: count
                                                    ? Colorconstands.primaryColor
                                                    : Colors.red),
                                      ),
                                      trailing: const Icon(
                                        Icons.circle_outlined,
                                        size: 20,
                                        color: Colorconstands.primaryColor,
                                      ),
                                      //trailing:const Icon(Icons.check_circle),
                                      onTap: () {
                                        setState(() {
                                          if (!count && index == 0) {
                                            print("semester 1");
                                          } else if (!count && index == 1) {
                                            print("semester 2");
                                          } else if (!count && index == 2) {
                                            print("year ");
                                            print(index);
                                          } else {
                                            // print(data.listMonths![index].months![subindex].month);
                                            print(subindex);
                                          }
                                          //Navigator.of(context).pop();
                                        });
                                      },
                                    ),
                                  ),
                                );
                             
                              }));
                            }),
                      );
                    });
              },
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "( " + data!.schoolyearName.toString() + " )",
                      style: const TextStyle(
                          color: Colorconstands.secondaryColor, fontSize: 16.0),
                    ),
                    const Icon(
                      Icons.keyboard_arrow_down_rounded,
                      size: 28,
                      color: Colorconstands.secondaryColor,
                    )
                  ],
                ),
                alignment: Alignment.centerLeft,
              ),
            );
          }
          return Text("Testing");
        },
      ),
      // child: BlocBuilder<MonthlyBloc, MonthlyState>(
      // builder: (context, state) {
      //   if (state is MonthlyLoadingState) {
      //     return _buildLoading();
      //   }
      //   if (state is MonthlyLoadedState) {
      //     var data = state.currentMonthly!.data;
      //     return MaterialButton(
      //       shape: RoundedRectangleBorder(
      //           borderRadius: BorderRadius.circular(12.0)),
      //       padding: const EdgeInsets.all(0.0),
      //       onPressed: (){
      //         setState(() {
      //           showModalBottomSheet(
      //             backgroundColor: Colors.transparent,
      //             isScrollControlled: true,
      //             context: context,
      //             isDismissible: true,
      //             builder: (context) {
      //               return BuildBottomSheet(
      //                 initialChildSize: 0.55,
      //                 child: Column(
      //                         mainAxisSize: MainAxisSize.min,
      //                         children: [
      //                           Container(
      //                             padding: const EdgeInsets.only(top: 10.0),
      //                             alignment: Alignment.center,
      //                             child: Align(
      //                               child: Text("Choose Month",
      //                                   style: ThemeConstands.texttheme.headline6!
      //                                       .copyWith(color: Colorconstands.black)),
      //                             ),
      //                           ),
      //                           Container(
      //                             margin: const EdgeInsets.symmetric(
      //                                 horizontal: 30, vertical: 20),
      //                             child: Align(
      //                               child: Text(
      //                                 "Excepteur sint occaecat cupidatat non proident, sunt in culpa",
      //                                 style: ThemeConstands.texttheme.subtitle2!
      //                                     .copyWith(color: Colorconstands.black),
      //                                 textAlign: TextAlign.center,
      //                               ),
      //                             ),
      //                           ),
      //                           Divider(
      //                            height: 2,
      //                            thickness: 1,
      //                             color: Colorconstands.darkGray.withOpacity(0.1),
      //                           )
      //                         ],
      //                       ),
      //                 expanded: ListView.builder(
      //                           physics: const NeverScrollableScrollPhysics(),
      //                           shrinkWrap: true,
      //                           padding: const EdgeInsets.all(0),
      //                           itemCount: data!.listMonths!.length,
      //                           itemBuilder: (context, index) {
      //                             return  Column(
      //                               children: List.generate(data.listMonths![index].months!.length+1, (subindex){
      //                                 var count = subindex!=data.listMonths![index].months!.length;
      //                               return Container(
      //                                   alignment: Alignment.center,
      //                                   decoration: BoxDecoration(
      //                                       color: Colorconstands.white,
      //                                       border: Border(
      //                                           bottom: BorderSide(
      //                                         color:
      //                                             Colorconstands.darkGray.withOpacity(0.1),
      //                                         width: 1,
      //                                       ))),
      //                                   height: 50,
      //                                   child: Container(
      //                                      margin:const EdgeInsets.symmetric(horizontal: 15.0),
      //                                     child: ListTile(
      //                                       title:Text(
      //                                       count? data.listMonths![index].months![subindex].displayMonth.toString():data.listMonths![index].name.toString(),
      //                                         style: ThemeConstands.texttheme.headline6!
      //                                             .copyWith(fontWeight: FontWeight.normal,color:count? Colorconstands.primaryColor:Colors.red),
      //                                       ),
      //                                       trailing: Icon(Icons.circle_outlined,size: 20,color: Colorconstands.primaryColor,),
      //                                       //trailing:const Icon(Icons.check_circle),
      //                                       onTap: () {
      //                                         setState(() {
      //                                           if(!count&&index==0){
      //                                             print("semester 1");
      //                                           }
      //                                           else if(!count&&index==1){
      //                                              print("semester 2");
      //                                           }
      //                                           else if(!count&&index==2){
      //                                              print("year ");
      //                                              print(index);
      //                                           }
      //                                           else{
      //                                              // print(data.listMonths![index].months![subindex].month);
      //                                               print(subindex);
      //                                           }
      //                                           //Navigator.of(context).pop();
      //                                         });
      //                                       },
      //                                     ),
      //                                   ),
      //                                 );
      //                               }
      //                               )
      //                             );
      //                           }),

      //               );
      //             });
      //         });
      //       },
      //       child: Container(
      //         margin:const EdgeInsets.symmetric(horizontal: 25),
      //         child: Row(
      //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //           children: [
      //             Text("( "+data!.schoolyearName.toString()+" )",style:const TextStyle(color: Colorconstands.secondaryColor,fontSize: 16.0),),
      //             const Icon(Icons.keyboard_arrow_down_rounded,size: 28,color: Colorconstands.secondaryColor,)
      //           ],
      //         ),
      //         alignment: Alignment.centerLeft,
      //       ),
      //     );

      //   }
      //   else{
      //     return _buildLoading();
      //   }
      // }),
    );
  }

  Widget _buildLoading() => Container(
      margin: const EdgeInsets.only(right: 10.0),
      child: Center(
          child: Image.asset(
        "assets/images/gifs/loading.gif",
        height: 45,
        width: 45,
      )));
}
