// ignore_for_file: must_be_immutable

part of 'change_child_bloc.dart';

abstract class ChangeChildState extends Equatable {
  const ChangeChildState();

  @override
  List<Object> get props => [];
}

class ChangeChildInitial extends ChangeChildState {}

class ChangeChildLoadingState extends ChangeChildState {}

// Get All Childrent

class ChangeChildLoadState extends ChangeChildState {
  final ChildrenModel? childrenModel;
  const ChangeChildLoadState({this.childrenModel});
}

// Get Detail Specific children

class GetDetailChildState extends ChangeChildState {
  final BaseResponseModel<GetDetailChildModel> detailChildModel;
  const GetDetailChildState({ required this.detailChildModel});
}

// Selecting a specific Children State
class SelectedChildState extends ChangeChildState {
  String childId;
  String? childIndex;
  SelectedChildState({required this.childId, this.childIndex});
}

class SelectedChildErrorState extends ChangeChildState {
  final String error;
  const SelectedChildErrorState({required this.error});
}

class ChangeChildErrorState extends ChangeChildState {
  final String? error;
  const ChangeChildErrorState({required this.error});
}
