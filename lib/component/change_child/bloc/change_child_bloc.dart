import 'package:camis_application_flutter/service/apis/get_children_api/get_children_api.dart';
import 'package:equatable/equatable.dart';
import '../../../app/routes/e.route.dart';
import '../../../model/base/base_respone_model.dart';
import '../../../model/get_children_model/get_children_model.dart';
import '../../../model/get_children_model/get_detail_childmodel.dart';

part 'change_child_event.dart';
part 'change_child_state.dart';

class ChangeChildBloc extends Bloc<ChangeChildEvent, ChangeChildState> {
  final GetChildrenApi childrenApi;
  int activeChild = 0;
  String? classIDBloc;

  ChangeChildBloc({required this.childrenApi})
      : super(ChangeChildLoadingState()) {
    on<GetChildrenEvent>((event, emit) async {
      emit(ChangeChildLoadingState());
      try {
        var getChildModel = await childrenApi.getChildrenApi();
        emit(ChangeChildLoadState(childrenModel: getChildModel));
      } catch (e) {
        emit(ChangeChildErrorState(error: e.toString()));
      }
    });

    on<SelectedChildEvent>((event, emit) {
      emit(SelectedChildState(
          childId: event.selectIndex, childIndex: event.selectIndex));
    });
  }
}
