part of 'change_child_bloc.dart';

abstract class ChangeChildEvent extends Equatable {
  const ChangeChildEvent();
  @override
  List<Object> get props => [];
}

class GetChildrenEvent extends ChangeChildEvent {}

class GetDetailChildrenEvent extends ChangeChildEvent {}

class SelectedChildEvent extends ChangeChildEvent {
  final String selectIndex;
  final String? childIndex;
  const SelectedChildEvent({required this.selectIndex, this.childIndex});
}
