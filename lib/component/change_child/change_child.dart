import 'package:camis_application_flutter/component/change_child/bloc/change_child_bloc.dart';
import 'package:camis_application_flutter/storages/save_storage.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../app/core/themes/color_app.dart';
import '../../app/core/themes/themes.dart';
import '../../model/get_children_model/get_children_model.dart';
import '../../widgets/model_bottomSheet.dart';
import '../../widgets/profile_widget.dart';
import '../dropdown_month/bloc/monthly_bloc.dart';

// import '../dropdown_month/bloc/drop_month_bloc.dart';
class ChangeChildren extends StatefulWidget {
  const ChangeChildren({Key? key}) : super(key: key);

  @override
  State<ChangeChildren> createState() => _ChangeChildrenState();
}

class _ChangeChildrenState extends State<ChangeChildren> {
  final SaveStoragePref _savePref = SaveStoragePref();

  int? selectChild;
  String? profilchlid;
  @override
  void initState() {
    super.initState();
    setState(() {
      BlocProvider.of<ChangeChildBloc>(context).add(GetChildrenEvent());
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChangeChildBloc, ChangeChildState>(
        builder: (context, state) {
      if (state is ChangeChildLoadingState) {
        return _buildLoading();
      }
      if (state is ChangeChildLoadState) {
        var data = state.childrenModel!.data!;
        return Profile(
          height: 70,
          width: 70,
          pandding: 1.5,
          namechild: '',
          sizetextname: 17.0,
          onPressed: () {
            showModalBottomSheet(
              backgroundColor: Colors.transparent,
              isScrollControlled: true,
              context: context,
              isDismissible: true,
              builder: (context) => BuildBottomSheet(
                initialChildSize: 0.5,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      padding: const EdgeInsets.only(top: 30.0),
                      alignment: Alignment.center,
                      child: Align(
                        child: Text("Choose Childrent",
                            style: ThemeConstands.texttheme.headline6!
                                .copyWith(color: Colorconstands.black)),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(
                          horizontal: 30, vertical: 20),
                      child: Align(
                        child: Text(
                          "Excepteur sint occaecat cupidatat non proident, sunt in culpa",
                          style: ThemeConstands.texttheme.subtitle2!
                              .copyWith(color: Colorconstands.black),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ],
                ),
                expanded: Container(
                  color: Colorconstands.white,
                  child: ListView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      padding: const EdgeInsets.all(0),
                      itemCount: data.length,
                      itemBuilder: (context, index) {
                        String idChild = data[index].id.toString();
                        return Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: selectChild == index
                                  ? Colorconstands.primaryBackgroundColor
                                  : Colorconstands.white,
                              border: Border( bottom: BorderSide(
                                color: Colorconstands.darkGray.withOpacity(0.1),
                                width: 1,
                              ))),
                          height: 85,
                          child: ListTile(
                            leading: Container(
                              height: 50,
                              width: 50,
                              child: ClipOval(
                                  child: FadeInImage.assetNetwork(
                                fit: BoxFit.cover,
                                image: data[index].profileMedia!.fileThumbnail ==null? data[index].profileMedia!.fileShow.toString()
                                        : data[index].profileMedia!.fileThumbnail.toString(),
                                placeholder: 'assets/images/gifs/loading.gif',
                              )),
                            ),
                            title: Text(
                              data[index].name.toString(),
                              style: ThemeConstands.texttheme.headline6!.copyWith(fontWeight: selectChild == index? FontWeight.bold: FontWeight.normal),
                            ),
                            subtitle: Text(
                             "CLASS".tr()+": " + data[index].currentClass!.className.toString(),
                              style:ThemeConstands.texttheme.subtitle1!.copyWith(),
                            ),
                            trailing: selectChild == index
                                ? const Icon(
                                    Icons.check_circle_outline_sharp,
                                    color: Colorconstands.secondaryColor,
                                  )
                                : Container(
                                    decoration: BoxDecoration(
                                        color: Colorconstands.secondaryColor,
                                        borderRadius:
                                            BorderRadius.circular(12)),
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 15, vertical: 5),
                                    child: Text(
                                      'Choose',
                                      style: ThemeConstands.texttheme.subtitle2!
                                          .copyWith(color: Colorconstands.white),
                                    ),
                                  ),
                            onTap: () {
                              setState(() {
                                Navigator.of(context).pop();
                                BlocProvider.of<ChangeChildBloc>(context)
                                    .add(GetChildrenEvent());
                                BlocProvider.of<MonthlyBloc>(context)
                                    .add(GetDetailChildMonthEvent(childID: "37854342273749328133"));
                                selectChild = index;
                              });
                            },
                          ),
                        );
                      }),
                ),
              ),
            );
          },
          imageProfile: data[selectChild!].profileMedia!.fileShow.toString(),
        );
      } else {
        return Container(
            margin: const EdgeInsets.only(right: 10.0),
            child: Center(
                child: Image.asset(
              "assets/images/gifs/loading.gif",
              height: 45,
              width: 45,
            )));
      }
    });
  }

  Widget _buildLoading() => Container(
      margin: const EdgeInsets.only(right: 10.0),
      child: Center(
          child: Image.asset(
        "assets/images/gifs/loading.gif",
        height: 45,
        width: 45,
      )));
}
