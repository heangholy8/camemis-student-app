part of 'list_month_bloc.dart';

abstract class ListMonthState extends Equatable {
  const ListMonthState();

  @override
  List<Object> get props => [];
}

class ListMonthInitial extends ListMonthState {}

class ListMonthLoadingState extends ListMonthState {}

class ListMonthLoadedState extends ListMonthState {
  final BaseResponseModel<ListMonthModel> currentListMonth;
  const ListMonthLoadedState({required this.currentListMonth});
}


class ListMonthErrorState extends ListMonthState {
  final String error;
  const ListMonthErrorState({required this.error});
}
