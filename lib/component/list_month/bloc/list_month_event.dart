part of 'list_month_bloc.dart';

abstract class ListMonthEvent extends Equatable {
  const ListMonthEvent();

  @override
  List<Object> get props => [];
}


class GetListMonthEvent extends ListMonthEvent{
  final String childID;
  const GetListMonthEvent({required this.childID});
}


class GetSpecificChildEvent extends ListMonthEvent{
  
}
