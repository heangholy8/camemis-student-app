import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../model/base/base_respone_model.dart';
import '../../../model/result/list_month.dart';
import '../../../service/apis/get_children_api/get_children_api.dart';

part 'list_month_event.dart';
part 'list_month_state.dart';

class ListMonthBloc extends Bloc<ListMonthEvent, ListMonthState> {
  final GetChildrenApi currenApi;
  BaseResponseModel<ListMonthModel> specificChildModel =
      BaseResponseModel<ListMonthModel>();
  ListMonthBloc({required this.currenApi}) : super(ListMonthLoadingState()) {
    on<GetListMonthEvent>((event, emit) async {
      
      emit(ListMonthLoadingState());
      try {
      var data =  await currenApi.getListMonthApi(childid: event.childID);
      emit(ListMonthLoadedState(currentListMonth:data));
      } catch (e) {
        print("sdfsadfa : $e");
        emit(ListMonthErrorState(error: e.toString()));
      }
    }
    );
  }
}
