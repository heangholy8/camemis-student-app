// ignore_for_file: sort_child_properties_last
import 'package:camis_application_flutter/widgets/button_widget/button_customwidget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../app/core/themes/color_app.dart';
import '../app/core/themes/themes.dart';
import '../widgets/custom_alertdialog.dart';

mixin Toast {
  Future<T> showSuccessDialog<T>( 
    {required Function() callback,
    String? title,
    String? discription,
    context,
   }
  ) async {
    return await showDialog(
      barrierDismissible: false,
      barrierColor: Colors.black38,
      context: context,
      builder: (context) {
        return CustomAlertDialog(
          blurColor: Colorconstands.primaryColor.withOpacity(0.1),
          child: Icon(
            Icons.check_circle,
            color: Colorconstands.primaryColor,
            size: MediaQuery.of(context).size.width / 6,
          ),
          title: title ?? "SUCCESSFULLY".tr(),
          subtitle:discription??"SUCCESS_SUBTITLE".tr(),
          onPressed: callback,
          // () {
          //   Navigator.push(
          //     context,
          //     PageTransition(
          //       child: const HomeScreen(),
          //       type: PageTransitionType.rightToLeft,
          //     ),
          //   );
          //},
          titleButton: 'OK'.tr(),
          buttonColor: Colorconstands.primaryColor,
          titlebuttonColor: Colorconstands.white,
        );
      },
    );
  }

  Future<T> showErrorDialog<T>(
    Function() callback,
    context,{String? title, String? description}
  ) async {
    return await showDialog(
      barrierDismissible: false,
      barrierColor: Colors.black38,
      context: context,
      builder: (context) {
        return CustomAlertDialog(
          child: Icon(
            Icons.cancel,
            color: Colors.red,
            size: MediaQuery.of(context).size.width / 6,
          ),
          title: title?? "INVALID_CODE".tr(),
          subtitle:description?? "INVALID_SUBTITLE".tr(),
          onPressed: callback,
          titleButton: "TRYAGAIN".tr(),
          buttonColor: Colors.red,
          titlebuttonColor: Colorconstands.white,
        );
      },
    );
  }

  // Future introDialog(context) async {
  //   final SaveStoragePref _savePref = SaveStoragePref();
  //   String giudsuccess = 'Guid-Success';
  //   Future.delayed(Duration.zero, () {
  //     showDialog(
  //       barrierDismissible: false,
  //       barrierColor: Colors.black38,
  //       context: context,
  //       builder: (context) {
  //         return CustomAlertDialog_Guide(
  //           child: Image.asset("assets/images/svg/logo.png",
  //               width: 45,
  //               fit: BoxFit.cover,
  //               filterQuality: FilterQuality.high),
  //           title: "Welcome to CAMEMIS!",
  //           subtitle:
  //               "Hi [Guardian Name]! Thank you for using our app. We hope CAMEMIS will become a ________.Please check out our User Guide video to get started. ",
  //           onPressed: () {
  //             Navigator.of(context).pop();
  //             _savePref.saveViewGuidSuccess(
  //                 guidSuccess: giudsuccess.toString());
  //                 Navigator.push(
  //             context,
  //             MaterialPageRoute(
  //               builder: (context) => const GuidLineScreen(),
  //             ),
  //           );
  //           },
  //           titleButton: 'Watch Guide',
  //           buttonColor: Colorconstands.primaryColor,
  //           titlebuttonColor: Colorconstands.white,
  //           onPressedSkip: () {
  //             Navigator.of(context).pop();
  //             _savePref.saveViewGuidSuccess(
  //                 guidSuccess: giudsuccess.toString());
  //             print(giudsuccess);
  //           },
  //         );
  //       },
  //     );
  //   });
  // }

  Future success(
      context, String? title, String? subtitle, Function onTap) async {
    showDialog(
      barrierDismissible: false,
      barrierColor: Colors.black38,
      context: context,
      builder: (context) {
        return CustomAlertDialog(
          blurColor: const Color(0xFF2699FB),
          child: Center(
            child: Icon(
              Icons.check_circle,
              color: Colorconstands.white,
              size: MediaQuery.of(context).size.width / 6,
            ),
          ),
          title: title!,
          subtitle: subtitle!,
          onPressed: () {
            onTap();
          },
          titleButton: 'OK',
          buttonColor: Colorconstands.secondaryColor,
          titlebuttonColor: Colorconstands.white,
        );
      },
    );
  }

  Future unsuccess(
      context, String? title, String? subtitle, Function onTap) async {
    showDialog(
      barrierDismissible: false,
      barrierColor: Colors.black38,
      context: context,
      builder: (context) {
        return CustomAlertDialog(
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: SvgPicture.asset(
              "assets/icons/cross_circle.svg",
              width: MediaQuery.of(context).size.width / 6,
            ),
          ),
          title: title!,
          subtitle: subtitle!,
          onPressed: () {
            onTap();
          },
          titleButton: 'Try Again!',
          buttonColor: const Color(0xFFEB4D4B),
          titlebuttonColor: Colorconstands.white,
        );
      },
    );
  }

  void showMessage(
      BuildContext context, String title, double width, double hight) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      duration: const Duration(milliseconds: 800),
      width: width,
      elevation: 2.0,
      behavior: SnackBarBehavior.floating,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      content: Wrap(
        children: [
          Container(
            height: hight,
            child: Center(
              child: Text(
                title,
                style: ThemeConstands.headline6_Regular_14_24height
                    .copyWith(color: Colorconstands.neutralWhite),
              ),
            ),
          ),
        ],
      ),
    ));
  }

  Future childNoClass(context, String name, String profile) async {
    Future.delayed(Duration.zero, () {
      showDialog(
        barrierDismissible: true,
        barrierColor: Colors.black38,
        context: context,
        builder: (context) {
          return Dialog(
            elevation: 5,
            backgroundColor: Colorconstands.neutralWhite,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            child: Container(
              //width: MediaQuery.of(context).size.width / 1.5,
              height: 350,
              child: Center(
                child: Container(
                  margin: const EdgeInsets.all(22),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          child: CircleAvatar(
                        radius: 30, // Image radius
                        backgroundImage: NetworkImage(profile),
                      )),
                      const SizedBox(
                        height: 15,
                      ),
                      Container(
                        child: Text(
                          name,
                          style: ThemeConstands.headline4_SemiBold_18
                              .copyWith(color: Colorconstands.neutralDarkGrey),
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Column(
                        children: [
                          Container(
                            child: Text(
                              "CHILD_NOT_CLASS".tr(),
                              style: ThemeConstands.headline5_SemiBold_16.copyWith(
                                  color: Colorconstands.darkTextsPlaceholder),textAlign: TextAlign.center,
                            ),
                          ),
                          Container(
                            child: Text(
                              "CHILD_NOT_CLASS_DES".tr(),
                              style: ThemeConstands.headline5_SemiBold_16.copyWith(
                                  color: Colorconstands.darkTextsPlaceholder),textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Container(
                        child: MaterialButton(
                          color: Colorconstands.primaryColor,
                          shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(12))),
                          padding: const EdgeInsets.symmetric(
                              vertical: 6, horizontal: 28),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            "យល់ព្រម",
                            style: ThemeConstands.headline6_SemiBold_14
                                .copyWith(color: Colorconstands.neutralWhite),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      );
    });
  }

  Future RedirectApp(context, VoidCallback onPressed) async {
    Future.delayed(Duration.zero, () {
      showDialog(
        barrierDismissible: false,
        barrierColor: Colors.black38,
        context: context,
        builder: (context) {
          return Dialog(
            elevation: 5,
            backgroundColor: Colorconstands.neutralWhite,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 243,
              child: Container(
                child: Column(
                  children: [
                    Container(
                      margin:
                          const EdgeInsets.only(top: 22, left: 22, right: 22),
                      child: Text(
                        "App Redirect",
                        style: ThemeConstands.headline4_SemiBold_18
                            .copyWith(color: Colorconstands.lightBlack),
                      ),
                    ),
                    const SizedBox(
                      height: 18,
                    ),
                    Container(
                      margin: const EdgeInsets.only(
                          bottom: 22, left: 22, right: 22),
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: ' "CAMEMIS" ',
                              style: ThemeConstands.headline5_SemiBold_16
                                  .copyWith(color: Colorconstands.lightBlack),
                            ),
                            TextSpan(
                                text: 'WANTTOUSE'.tr(),
                                style: ThemeConstands.headline5_Medium_16
                                    .copyWith(
                                        color: Colorconstands.lightBlack)),
                            TextSpan(
                              text: ' "ABA Mobile" ',
                              style: ThemeConstands.headline5_SemiBold_16
                                  .copyWith(color: Colorconstands.lightBlack),
                            ),
                            TextSpan(
                                text: 'VERRIFYPAYMENT'.tr(),
                                style: ThemeConstands.headline5_Medium_16
                                    .copyWith(
                                        color: Colorconstands.lightBlack)),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 22,
                    ),
                    Expanded(
                      child: Container(),
                    ),
                    const Divider(
                      height: 0,
                      thickness: 1,
                      color: Colorconstands.neutralGrey,
                    ),
                    Container(
                      height: 60,
                      child: Row(
                        children: [
                          Expanded(
                            child: MaterialButton(
                              shape: const RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(15)),
                              ),
                              padding: const EdgeInsets.all(0),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Center(
                                child: Text(
                                  "Cancel",
                                  style: ThemeConstands.headline4_Regular_18
                                      .copyWith(
                                          color: Colorconstands.primaryColor),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            height: 60,
                            width: 1,
                            color: Colorconstands.neutralGrey,
                          ),
                          Expanded(
                            child: MaterialButton(
                              shape: const RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(15)),
                              ),
                              padding: const EdgeInsets.all(0),
                              onPressed: onPressed,
                              child: Center(
                                child: Text(
                                  "Allow",
                                  style: ThemeConstands.headline4_Regular_18
                                      .copyWith(
                                          color: Colorconstands.primaryColor),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        },
      );
    });
  }

  Future paymentSecuess(context, String monthPayment, String expaiDate) async {
    Future.delayed(Duration.zero, () {
      return showModalBottomSheet(
          context: context,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
          builder: (context) {
            return Container(
              padding: const EdgeInsets.only(
                top: 12.0,
              ),
              decoration: const BoxDecoration(
                  color: Colorconstands.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(15),
                      topRight: Radius.circular(15))),
              child: Container(
                margin: const EdgeInsets.all(22),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                        height: 67,
                        width: 67,
                        decoration: const BoxDecoration(
                            color: Colorconstands.secondaryColor,
                            shape: BoxShape.circle),
                        child: const Icon(
                          Icons.check_outlined,
                          color: Colorconstands.neutralWhite,
                          size: 48,
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 18),
                        child: Text(
                          "ការបង់ប្រាក់ជោគជ័យ",
                          style: ThemeConstands.headline2_SemiBold_242
                              .copyWith(color: Colorconstands.primaryColor),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 18),
                        child: Text(
                          "អ្នកបានបង់ប្រាក់ជោគជ័យសម្រាប់",
                          style: ThemeConstands.headline3_Medium_20_26height
                              .copyWith(color: Colorconstands.lightBlack),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 18),
                        child: Text(
                          monthPayment,
                          style: ThemeConstands.headline2_SemiBold_242
                              .copyWith(color: Colorconstands.primaryColor),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 18),
                        child: Text(
                          "កញ្ចប់សេវាកម្មរបស់អ្នកនឹងផុតកំណត់",
                          style: ThemeConstands.headline5_SemiBold_16
                              .copyWith(color: Colorconstands.lightBlack),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 18),
                        child: Text(
                          expaiDate,
                          style: ThemeConstands.headline4_SemiBold_18
                              .copyWith(color: Colorconstands.primaryColor),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 22),
                        child: Container(
                          child: Button_Custom(
                            titleButton: "រួចរាល់",
                            buttonColor: Colorconstands.primaryColor,
                            radiusButton: 10,
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            hightButton: 55,
                            titlebuttonColor: Colorconstands.primaryColor,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          });
    });
    
  }
  Future<T> showAlertLogout<T>(
      {required String title,
      String? titlebuttoncencel,
      String? titlebuttonok,
      void Function()? onPressed,
      required BuildContext context}) async {
    return await showDialog(
      barrierDismissible: false,
      barrierColor: Colors.black38,
      context: context,
      builder: (context) {
        return Dialog(
          elevation: 5,
          backgroundColor: Colorconstands.neutralWhite,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: SizedBox(
            width: MediaQuery.of(context).size.width / 2,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: const EdgeInsets.only(top: 25),
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width / 6,
                  height: MediaQuery.of(context).size.width / 6,
                  decoration: BoxDecoration(
                      color: Colorconstands.alertsNotifications.withOpacity(0.1),
                      shape: BoxShape.circle),
                  child: Container(
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                    ),
                    child: Icon(
                      Icons.info,
                      color: Colors.green,
                      size: MediaQuery.of(context).size.width / 6,
                    ),
                  ),
                ),
                Container(
                    margin:
                        const EdgeInsets.only(top: 20, left: 17.0, right: 17.0),
                    child: Align(
                      child: Text(
                        title,
                        style: ThemeConstands.button_SemiBold_16.copyWith(
                            color: Colorconstands.lightBulma,
                            fontWeight: FontWeight.w600),
                        textAlign: TextAlign.center,
                      ),
                    )),
                Container(
                  margin: const EdgeInsets.only(top: 25, bottom: 25),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Button_Custom(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          buttonColor: Colorconstands.neutralGrey,
                          hightButton: 45,
                          radiusButton: 12,
                          widthButton: double.maxFinite,
                          maginleft: 17.0,
                          titleButton:titlebuttoncencel??"NOT".tr(),
                          titlebuttonColor: Colorconstands.lightBlack,
                        ),
                      ),
                      const SizedBox(width: 10),
                      Expanded(
                        child: Button_Custom(
                          onPressed: onPressed,
                          buttonColor: Colorconstands.mainColorSecondary,
                          hightButton: 45,
                          radiusButton: 12,
                          maginRight: 17.0,
                          titleButton: titlebuttonok??"LOGOUT".tr(),
                          titlebuttonColor: Colorconstands.neutralWhite,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
