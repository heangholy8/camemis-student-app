// To parse this JSON data, do
//
//     final teacherListModel = teacherListModelFromJson(jsonString);

import 'dart:convert';

TeacherListModel teacherListModelFromJson(String str) => TeacherListModel.fromJson(json.decode(str));

String teacherListModelToJson(TeacherListModel data) => json.encode(data.toJson());

class TeacherListModel {
    TeacherListModel({
        this.data,
    });

    List<Datum>? data;

    factory TeacherListModel.fromJson(Map<String, dynamic> json) => TeacherListModel(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class Datum {
    Datum({
        this.teacherId,
        this.teacherName,
        this.teacherNameEn,
        this.teacherGender,
        this.teacherPhone,
        this.profileMedia,
        this.subjectId,
        this.gradeSubjectId,
        this.subjectShort,
        this.subjectName,
        this.subjectNameEn,
        this.description,
        this.scoreMin,
        this.scoreMax,
        this.isYourSubject,
        this.isInstructor,
    });

    String? teacherId;
    String? teacherName;
    String? teacherNameEn;
    int? teacherGender;
    String? teacherPhone;
    ProfileMedia? profileMedia;
    int? subjectId;
    int? gradeSubjectId;
    String? subjectShort;
    String? subjectName;
    String? subjectNameEn;
    String? description;
    int? scoreMin;
    int? scoreMax;
    dynamic isYourSubject;
    bool? isInstructor;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        teacherId: json["teacher_id"],
        teacherName: json["teacher_name"],
        teacherNameEn: json["teacher_name_en"],
        teacherGender: json["teacher_gender"],
        teacherPhone: json["teacher_phone"],
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
        subjectId: json["subject_id"],
        gradeSubjectId: json["grade_subject_id"],
        subjectShort: json["subject_short"],
        subjectName: json["subject_name"],
        subjectNameEn: json["subject_name_en"],
        description: json["description"],
        scoreMin: json["score_min"],
        scoreMax: json["score_max"],
        isYourSubject: json["is_your_subject"],
        isInstructor: json["is_instructor"],
        
    );

    Map<String, dynamic> toJson() => {
        "teacher_id": teacherId,
        "teacher_name": teacherName,
        "teacher_name_en": teacherNameEn,
        "teacher_gender": teacherGender,
        "teacher_phone": teacherPhone,
        "profileMedia": profileMedia!.toJson(),
        "subject_id": subjectId,
        "grade_subject_id": gradeSubjectId,
        "subject_short": subjectShort,
        "subject_name": subjectName,
        "subject_name_en": subjectNameEn,
        "description": description,
        "score_min": scoreMin,
        "score_max": scoreMax,
        "is_your_subject": isYourSubject,
        "is_instructor": isInstructor,
    };
}

class ProfileMedia {
    ProfileMedia({
        this.fileShow,
        this.fileThumbnail,
    });

    String? fileShow;
    String? fileThumbnail;

    factory ProfileMedia.fromJson(Map<String, dynamic> json) => ProfileMedia(
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
    );

    Map<String, dynamic> toJson() => {
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
    };
}
