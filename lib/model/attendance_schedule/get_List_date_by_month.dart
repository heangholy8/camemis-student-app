// To parse this JSON data, do
//
//     final listDateInMonthModel = listDateInMonthModelFromJson(jsonString);

import 'dart:convert';

ListDateInMonthModel listDateInMonthModelFromJson(String str) => ListDateInMonthModel.fromJson(json.decode(str));

String listDateInMonthModelToJson(ListDateInMonthModel data) => json.encode(data.toJson());

class ListDateInMonthModel {
    ListDateInMonthModel({
        this.data,
    });

    Data? data;

    factory ListDateInMonthModel.fromJson(Map<String, dynamic> json) => ListDateInMonthModel(
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "data": data!.toJson(),
    };
}

class Data {
    Data({
        this.month,
        this.displayMonth,
        this.displayMonthEn,
        this.year,
        this.isCurrent,
        this.monthData,
    });

    String? month;
    String? displayMonth;
    String? displayMonthEn;
    String? year;
    bool? isCurrent;
    List<MonthDatum>? monthData;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        month: json["month"],
        displayMonth: json["display_month"],
        displayMonthEn: json["display_month_en"],
        year: json["year"],
        isCurrent: json["is_current"],
        monthData: List<MonthDatum>.from(json["month_data"].map((x) => MonthDatum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "month": month,
        "display_month": displayMonth,
        "display_month_en": displayMonthEn,
        "year": year,
        "is_current": isCurrent,
        "month_data": List<dynamic>.from(monthData!.map((x) => x.toJson())),
    };
}

class MonthDatum {
    MonthDatum({
        this.day,
        this.shortName,
        this.date,
        this.isActive,
        this.disable,
        this.index,
    });

    int? day;
    String? shortName;
    String? date;
    bool? isActive;
    bool? disable;
    int? index;

    factory MonthDatum.fromJson(Map<String, dynamic> json) => MonthDatum(
        day: json["day"],
        shortName: json["short_name"],
        date: json["date"],
        isActive: json["is_active"],
        disable: json["disable"],
        index: json["index"],
    );

    Map<String, dynamic> toJson() => {
        "day": day,
        "short_name": shortName,
        "date": date,
        "is_active": isActive,
        "disable": disable,
        "index": index,
    };
}
