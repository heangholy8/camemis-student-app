// To parse this JSON data, do
//
//     final scheduleModel = scheduleModelFromJson(jsonString);

import 'dart:convert';

ScheduleModel scheduleModelFromJson(String str) => ScheduleModel.fromJson(json.decode(str));

String scheduleModelToJson(ScheduleModel data) => json.encode(data.toJson());

class ScheduleModel {
    ScheduleModel({
        this.status,
        this.data,
        this.message,
    });
    bool? status;
    List<DataSchedule>? data;
    String? message;

    factory ScheduleModel.fromJson(Map<String, dynamic> json) => ScheduleModel(
        status: json["status"],
        data: List<DataSchedule>.from(json["data"].map((x) => DataSchedule.fromJson(x))),
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "message": message,
        
    };
}

class DataSchedule {
    DataSchedule({
        this.id,
        this.scheduleType,
        this.isActive,
        this.bgColor,
        this.startTime,
        this.endTime,
        this.duration,
        this.isCurrent,
        this.date,
        this.shortDay,
        this.event,
        this.subjectId,
        this.subjectName,
        this.subjectNameEn,
        this.subjectColor,
        this.classId,
        this.className,
        this.teacherName,
        this.teacherNameEn,
        this.teacherId,
        this.gradeId,
        this.schoolyearId,
        this.description,
        this.attendanceStatus,
        this.isExam,
        this.examItem,
        this.activities,
        this.isTakeAttendance
    });

    int? id;
    int? scheduleType;
    bool? isActive;
    String? bgColor;
    String? startTime;
    String? endTime;
    String? duration;
    bool? isCurrent;
    String? date;
    String? shortDay;
    String? event;
    int? subjectId;
    String? subjectName;
    String? subjectNameEn;
    String? subjectColor;
    int? classId;
    String? className;
    String? teacherName;
    String? teacherNameEn;
    String? teacherId;
    int? gradeId;
    String? schoolyearId;
    String? description;
    String? attendanceStatus;
    int? isTakeAttendance;
    bool? isExam;
    ExamItem? examItem;
    List<Activity>? activities;

    factory DataSchedule.fromJson(Map<String, dynamic> json) => DataSchedule(
        id: json["id"],
        scheduleType: json["schedule_type"],
        isActive: json["is_active"],
        bgColor: json["bg_color"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        duration: json["duration"],
        isCurrent: json["is_current"],
        date: json["date"],
        shortDay: json["short_day"],
        event: json["event"],
        subjectId: json["subject_id"],
        subjectName: json["subject_name"],
        subjectNameEn: json["subject_name_en"],
        subjectColor: json["subject_color"],
        classId: json["class_id"],
        className: json["class_name"],
        teacherName: json["teacher_name"],
        teacherNameEn: json["teacher_name_en"],
        teacherId: json["teacher_id"],
        gradeId: json["grade_id"],
        schoolyearId: json["schoolyear_id"],
        description: json["description"],
        attendanceStatus: json["attendance_status"],
        isTakeAttendance: json["is_take_attendance"],
        isExam: json["is_exam"],
        examItem: json["exam_item"] == null ? null : ExamItem.fromJson(json["exam_item"]),
        activities: List<Activity>.from(json["activities"].map((x) => Activity.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "schedule_type": scheduleType,
        "is_active": isActive,
        "bg_color": bgColor,
        "start_time": startTime,
        "end_time": endTime,
        "duration": duration,
        "is_current": isCurrent,
        "date": date,
        "short_day": shortDay,
        "event": event,
        "subject_id": subjectId,
        "subject_name": subjectName,
        "subject_name_en": subjectNameEn,
        "subject_color": subjectColor,
        "class_id": classId,
        "class_name": className,
        "teacher_name": teacherName,
        "teacher_name_en": teacherNameEn,
        "teacher_id": teacherId,
        "grade_id": gradeId,
        "schoolyear_id": schoolyearId,
        "description": description,
        "attendance_status": attendanceStatus,
        "is_exam": isExam,
        "is_take_attendance":isTakeAttendance,
        "exam_item": examItem == null ? null : examItem!.toJson(),
        "activities": List<dynamic>.from(activities!.map((x) => x.toJson())),
    };
}
    class Activity {
    Activity({
        this.id,
        this.activityType,
        this.title,
        this.description,
        this.expiredAt,
        this.file,
    });

    int? id;
    int? activityType;
    String? title;
    String? description;
    String? expiredAt;
    List<FileElement>? file;

    factory Activity.fromJson(Map<String, dynamic> json) => Activity(
        id: json["id"],
        activityType: json["activity_type"],
        title: json["title"],
        description: json["description"],
        expiredAt: json["expired_at"],
        file: List<FileElement>.from(json["file"].map((x) => FileElement.fromJson(x))),  
    );
    Map<String, dynamic> toJson() => {
        "id": id,
        "activity_type": activityType,
        "title": title,
        "description": description,
        "expired_at": expiredAt,
        "file": List<dynamic>.from(file!.map((x) => x.toJson())),
    };
 }
class ExamItem {
    ExamItem({
        this.id,
        this.subjectId,
        this.classId,
        this.type,
        this.examDate,
        this.month,
        this.semester,
        this.description,
        this.isApprove,
    });

    int? id;
    dynamic subjectId;
    dynamic classId;
    int? type;
    String? examDate;
    String? month;
    String? semester;
    String? description;
    dynamic isApprove;

    factory ExamItem.fromJson(Map<String, dynamic> json) => ExamItem(
        id: json["id"],
        subjectId: json["subject_id"],
        classId: json["class_id"],
        type: json["type"],
        examDate: json["exam_date"],
        month: json["month"],
        semester: json["semester"],
        description: json["description"],
        isApprove: json["is_approve"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "subject_id": subjectId,
        "class_id": classId,
        "type": type,
        "exam_date": examDate,
        "month": month,
        "semester": semester,
        "description": description,
        "is_approve": isApprove,
    };
}

class FileElement {
    FileElement({
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileShow,
        this.fileThumbnail,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
    });

    int? id;
    String? fileName;
    String? fileSize;
    String? fileType;
    String? fileShow;
    String? fileThumbnail;
    String? fileIndex;
    String? fileArea;
    String? objectId;
    String? schoolUrl;
    String? postDate;
    int? isGdrive;
    String? fileGdriveId;
    int? isS3;

    factory FileElement.fromJson(Map<String, dynamic> json) => FileElement(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
    };
}
