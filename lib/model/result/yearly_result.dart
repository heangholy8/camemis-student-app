class YearResultModel {
  bool? status;
  YearData? data;

  YearResultModel({this.status, this.data});

  YearResultModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new YearData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class YearData {
  YearResult? yearResult;
  List<YearSubjects>? yearSubjects;
  int? totalStudent;

  YearData({this.yearResult, this.yearSubjects, this.totalStudent});

  YearData.fromJson(Map<String, dynamic> json) {
    yearResult = json['year_result'] != null
        ? new YearResult.fromJson(json['year_result'])
        : null;
    if (json['year_subjects'] != null) {
      yearSubjects = <YearSubjects>[];
      json['year_subjects'].forEach((v) {
        yearSubjects!.add(new YearSubjects.fromJson(v));
      });
    }
    totalStudent = json['total_student'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.yearResult != null) {
      data['year_result'] = this.yearResult!.toJson();
    }
    if (this.yearSubjects != null) {
      data['year_subjects'] =
          this.yearSubjects!.map((v) => v.toJson()).toList();
    }
    data['total_student'] = this.totalStudent;
    return data;
  }
}

class YearResult {
  int? id;
  dynamic maxAvgScore;
  dynamic avgScore;
  int? rank;
  dynamic teacherComment;
  String? recommendation;
  String? behavior;
  int? absenceTotal;
  int? absenceWithPermission;
  int? absenceWithoutPermission;
  int? absenceWithoutLate;
  String? grading;
  String? gradingEn;
  String? letterGrade;
  int? gradePoints;
  int? isFail;
  String? morality;
  String? bangkeunPhal;
  String? health;
  int? firstSemesterTotalExamScore;
  double? firstSemesterAvgExamScore;
  double? firstSemesterMonthAvgScore;
  double? firstSemesterAvg;
  int? firstSemesterRank;
  String? firstSemesterLetterGrade;
  double? secondSemesterTotalExamScore;
  double? secondSemesterAvgExamScore;
  double? secondSemesterMonthAvgScore;
  double? secondSemesterAvg;
  int? secondSemesterRank;
  String? secondSemesterLetterGrade;

  YearResult(
      {this.id,
      this.maxAvgScore,
      this.avgScore,
      this.rank,
      this.teacherComment,
      this.recommendation,
      this.behavior,
      this.absenceTotal,
      this.absenceWithPermission,
      this.absenceWithoutPermission,
      this.absenceWithoutLate,
      this.grading,
      this.gradingEn,
      this.letterGrade,
      this.gradePoints,
      this.isFail,
      this.morality,
      this.bangkeunPhal,
      this.health,
      this.firstSemesterTotalExamScore,
      this.firstSemesterAvgExamScore,
      this.firstSemesterMonthAvgScore,
      this.firstSemesterAvg,
      this.firstSemesterRank,
      this.firstSemesterLetterGrade,
      this.secondSemesterTotalExamScore,
      this.secondSemesterAvgExamScore,
      this.secondSemesterMonthAvgScore,
      this.secondSemesterAvg,
      this.secondSemesterRank,
      this.secondSemesterLetterGrade});

  YearResult.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    maxAvgScore = json['max_avg_score'];
    avgScore = json['avg_score'];
    rank = json['rank'];
    teacherComment = json['teacher_comment'];
    recommendation = json['recommendation'];
    behavior = json['behavior'];
    absenceTotal = json['absence_total'];
    absenceWithPermission = json['absence_with_permission'];
    absenceWithoutPermission = json['absence_without_permission'];
    absenceWithoutLate = json['absence_without_late'];
    grading = json['grading'];
    gradingEn = json['grading_en'];
    letterGrade = json['letter_grade'];
    gradePoints = json['grade_points'];
    isFail = json['is_fail'];
    morality = json['morality'];
    bangkeunPhal = json['bangkeun_phal'];
    health = json['health'];
    firstSemesterTotalExamScore = json['first_semester_total_exam_score'];
    firstSemesterAvgExamScore = json['first_semester_avg_exam_score'];
    firstSemesterMonthAvgScore = json['first_semester_month_avg_score'];
    firstSemesterAvg = json['first_semester_avg'];
    firstSemesterRank = json['first_semester_rank'];
    firstSemesterLetterGrade = json['first_semester_letter_grade'];
    secondSemesterTotalExamScore = json['second_semester_total_exam_score'];
    secondSemesterAvgExamScore = json['second_semester_avg_exam_score'];
    secondSemesterMonthAvgScore = json['second_semester_month_avg_score'];
    secondSemesterAvg = json['second_semester_avg'];
    secondSemesterRank = json['second_semester_rank'];
    secondSemesterLetterGrade = json['second_semester_letter_grade'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['max_avg_score'] = this.maxAvgScore;
    data['avg_score'] = this.avgScore;
    data['rank'] = this.rank;
    data['teacher_comment'] = this.teacherComment;
    data['recommendation'] = this.recommendation;
    data['behavior'] = this.behavior;
    data['absence_total'] = this.absenceTotal;
    data['absence_with_permission'] = this.absenceWithPermission;
    data['absence_without_permission'] = this.absenceWithoutPermission;
    data['absence_without_late'] = this.absenceWithoutLate;
    data['grading'] = this.grading;
    data['grading_en'] = this.gradingEn;
    data['letter_grade'] = this.letterGrade;
    data['grade_points'] = this.gradePoints;
    data['is_fail'] = this.isFail;
    data['morality'] = this.morality;
    data['bangkeun_phal'] = this.bangkeunPhal;
    data['health'] = this.health;
    data['first_semester_total_exam_score'] = this.firstSemesterTotalExamScore;
    data['first_semester_avg_exam_score'] = this.firstSemesterAvgExamScore;
    data['first_semester_month_avg_score'] = this.firstSemesterMonthAvgScore;
    data['first_semester_avg'] = this.firstSemesterAvg;
    data['first_semester_rank'] = this.firstSemesterRank;
    data['first_semester_letter_grade'] = this.firstSemesterLetterGrade;
    data['second_semester_total_exam_score'] =
        this.secondSemesterTotalExamScore;
    data['second_semester_avg_exam_score'] = this.secondSemesterAvgExamScore;
    data['second_semester_month_avg_score'] = this.secondSemesterMonthAvgScore;
    data['second_semester_avg'] = this.secondSemesterAvg;
    data['second_semester_rank'] = this.secondSemesterRank;
    data['second_semester_letter_grade'] = this.secondSemesterLetterGrade;
    return data;
  }
}

class YearSubjects {
  String? name;
  String? nameEn;
  String? short;
  int? maxScore;
  dynamic avgScore;
  dynamic firstSemesterAvgScore;
  dynamic secondSemesterAvgScore;
  int? rank;
  String? letterGradeEn;
  String? letterGrade;
  String? teacherComment;

  YearSubjects(
      {this.name,
      this.nameEn,
      this.short,
      this.maxScore,
      this.avgScore,
      this.firstSemesterAvgScore,
      this.secondSemesterAvgScore,
      this.rank,
      this.letterGradeEn,
      this.letterGrade,
      this.teacherComment});

  YearSubjects.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    nameEn = json['name_en'];
    short = json['short'];
    maxScore = json['max_score'];
    avgScore = json['avg_score'];
    firstSemesterAvgScore = json['first_semester_avg_score'];
    secondSemesterAvgScore = json['second_semester_avg_score'];
    rank = json['rank'];
    letterGradeEn = json['letter_grade_en'];
    letterGrade = json['letter_grade'];
    teacherComment = json['teacher_comment'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['name_en'] = this.nameEn;
    data['short'] = this.short;
    data['max_score'] = this.maxScore;
    data['avg_score'] = this.avgScore;
    data['first_semester_avg_score'] = this.firstSemesterAvgScore;
    data['second_semester_avg_score'] = this.secondSemesterAvgScore;
    data['rank'] = this.rank;
    data['letter_grade_en'] = this.letterGradeEn;
    data['letter_grade'] = this.letterGrade;
    data['teacher_comment'] = this.teacherComment;
    return data;
  }
}
