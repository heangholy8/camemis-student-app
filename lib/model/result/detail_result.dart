class DetailResultModel {
  bool? status;
  DetailData? data;

  DetailResultModel({this.status, this.data});

  DetailResultModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new DetailData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class DetailData {
  List<TotalAttendance>? totalAttendance;
  List<Evaluation>? evaluation;

  DetailData({this.totalAttendance, this.evaluation});

  DetailData.fromJson(Map<String, dynamic> json) {
    if (json['total_attendance'] != null) {
      totalAttendance = <TotalAttendance>[];
      json['total_attendance'].forEach((v) {
        totalAttendance!.add(new TotalAttendance.fromJson(v));
      });
    }
    if (json['evaluation'] != null) {
      evaluation = <Evaluation>[];
      json['evaluation'].forEach((v) {
        evaluation!.add(new Evaluation.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.totalAttendance != null) {
      data['total_attendance'] =
          this.totalAttendance!.map((v) => v.toJson()).toList();
    }
    if (this.evaluation != null) {
      data['evaluation'] = this.evaluation!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TotalAttendance {
  int? firstSemesterPermission;
  int? firstSemesterWithoutPermission;
  int? secondSemesterPermission;
  int? secondSemesterWithoutPermission;
  int? inYearPermission;
  int? inYearWithoutPermission;

  TotalAttendance(
      {this.firstSemesterPermission,
      this.firstSemesterWithoutPermission,
      this.secondSemesterPermission,
      this.secondSemesterWithoutPermission,
      this.inYearPermission,
      this.inYearWithoutPermission});

  TotalAttendance.fromJson(Map<String, dynamic> json) {
    firstSemesterPermission = json['first_semester_permission'];
    firstSemesterWithoutPermission = json['first_semester_without_permission'];
    secondSemesterPermission = json['second_semester_permission'];
    secondSemesterWithoutPermission =
        json['second_semester_without_permission'];
    inYearPermission = json['in_year_permission'];
    inYearWithoutPermission = json['in_year_without_permission'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['first_semester_permission'] = this.firstSemesterPermission;
    data['first_semester_without_permission'] =
        this.firstSemesterWithoutPermission;
    data['second_semester_permission'] = this.secondSemesterPermission;
    data['second_semester_without_permission'] =
        this.secondSemesterWithoutPermission;
    data['in_year_permission'] = this.inYearPermission;
    data['in_year_without_permission'] = this.inYearWithoutPermission;
    return data;
  }
}

class Evaluation {
  Null? firstSemisterMorality;
  Null? firstSemisterBangkeunPhal;
  Null? firstSemisterHealth;
  Null? secondSemisterMorality;
  Null? secondSemisterBangkeunPhal;
  Null? secondSemisterHealth;
  Null? inYearMorality;
  Null? inYearBangkeunPhal;
  Null? inYearHealth;

  Evaluation(
      {this.firstSemisterMorality,
      this.firstSemisterBangkeunPhal,
      this.firstSemisterHealth,
      this.secondSemisterMorality,
      this.secondSemisterBangkeunPhal,
      this.secondSemisterHealth,
      this.inYearMorality,
      this.inYearBangkeunPhal,
      this.inYearHealth});

  Evaluation.fromJson(Map<String, dynamic> json) {
    firstSemisterMorality = json['first_semister_morality'];
    firstSemisterBangkeunPhal = json['first_semister_bangkeun_phal'];
    firstSemisterHealth = json['first_semister_health'];
    secondSemisterMorality = json['second_semister_morality'];
    secondSemisterBangkeunPhal = json['second_semister_bangkeun_phal'];
    secondSemisterHealth = json['second_semister_health'];
    inYearMorality = json['in_year_morality'];
    inYearBangkeunPhal = json['in_year_bangkeun_phal'];
    inYearHealth = json['in_year_health'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['first_semister_morality'] = this.firstSemisterMorality;
    data['first_semister_bangkeun_phal'] = this.firstSemisterBangkeunPhal;
    data['first_semister_health'] = this.firstSemisterHealth;
    data['second_semister_morality'] = this.secondSemisterMorality;
    data['second_semister_bangkeun_phal'] = this.secondSemisterBangkeunPhal;
    data['second_semister_health'] = this.secondSemisterHealth;
    data['in_year_morality'] = this.inYearMorality;
    data['in_year_bangkeun_phal'] = this.inYearBangkeunPhal;
    data['in_year_health'] = this.inYearHealth;
    return data;
  }
}
