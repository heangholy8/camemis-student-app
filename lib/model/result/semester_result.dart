class TermResultModel {
  bool? status;
  TermData? data;

  TermResultModel({this.status, this.data});

  TermResultModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new TermData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class TermData {
  TermResult? termResult;
  TermExamResult? termExamResult;
  List<TermSubjects>? termSubjects;
  int? totalStudent;

  TermData(
      {this.termResult,
      this.termExamResult,
      this.termSubjects,
      this.totalStudent});

  TermData.fromJson(Map<String, dynamic> json) {
    termResult = json['term_result'] != null
        ? new TermResult.fromJson(json['term_result'])
        : null;
    termExamResult = json['term_exam_result'] != null
        ? new TermExamResult.fromJson(json['term_exam_result'])
        : null;
    if (json['term_subjects'] != null) {
      termSubjects = <TermSubjects>[];
      json['term_subjects'].forEach((v) {
        termSubjects!.add(new TermSubjects.fromJson(v));
      });
    }
    totalStudent = json['total_student'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.termResult != null) {
      data['term_result'] = this.termResult!.toJson();
    }
    if (this.termExamResult != null) {
      data['term_exam_result'] = this.termExamResult!.toJson();
    }
    if (this.termSubjects != null) {
      data['term_subjects'] =
          this.termSubjects!.map((v) => v.toJson()).toList();
    }
    data['total_student'] = this.totalStudent;
    return data;
  }
}

class TermResult {
  int? id;
  dynamic maxAvgScore;
  dynamic avgScore;
  dynamic examAvg;
  dynamic monthAvg;
  int? rank;
  String? term;
  dynamic teacherComment;
  dynamic recommendation;
  String? behavior;
  int? absenceTotal;
  int? absenceWithPermission;
  int? absenceWithoutPermission;
  int? absenceWithLate;
  String? grading;
  String? gradingEn;
  String? letterGrade;
  int? gradePoints;
  int? isFail;
  String? morality;
  String? bangkeunPhal;
  String? health;

  TermResult(
      {this.id,
      this.maxAvgScore,
      this.avgScore,
      this.examAvg,
      this.monthAvg,
      this.rank,
      this.term,
      this.teacherComment,
      this.recommendation,
      this.behavior,
      this.absenceTotal,
      this.absenceWithPermission,
      this.absenceWithoutPermission,
      this.absenceWithLate,
      this.grading,
      this.gradingEn,
      this.letterGrade,
      this.gradePoints,
      this.isFail,
      this.morality,
      this.bangkeunPhal,
      this.health});

  TermResult.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    maxAvgScore = json['max_avg_score'];
    avgScore = json['avg_score'];
    examAvg = json['exam_avg'];
    monthAvg = json['month_avg'];
    rank = json['rank'];
    term = json['term'];
    teacherComment = json['teacher_comment'];
    recommendation = json['recommendation'];
    behavior = json['behavior'];
    absenceTotal = json['absence_total'];
    absenceWithPermission = json['absence_with_permission'];
    absenceWithoutPermission = json['absence_without_permission'];
    absenceWithLate = json['absence_with_late'];
    grading = json['grading'];
    gradingEn = json['grading_en'];
    letterGrade = json['letter_grade'];
    gradePoints = json['grade_points'];
    isFail = json['is_fail'];
    morality = json['morality'];
    bangkeunPhal = json['bangkeun_phal'];
    health = json['health'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['max_avg_score'] = this.maxAvgScore;
    data['avg_score'] = this.avgScore;
    data['exam_avg'] = this.examAvg;
    data['month_avg'] = this.monthAvg;
    data['rank'] = this.rank;
    data['term'] = this.term;
    data['teacher_comment'] = this.teacherComment;
    data['recommendation'] = this.recommendation;
    data['behavior'] = this.behavior;
    data['absence_total'] = this.absenceTotal;
    data['absence_with_permission'] = this.absenceWithPermission;
    data['absence_without_permission'] = this.absenceWithoutPermission;
    data['absence_with_late'] = this.absenceWithLate;
    data['grading'] = this.grading;
    data['grading_en'] = this.gradingEn;
    data['letter_grade'] = this.letterGrade;
    data['grade_points'] = this.gradePoints;
    data['is_fail'] = this.isFail;
    data['morality'] = this.morality;
    data['bangkeun_phal'] = this.bangkeunPhal;
    data['health'] = this.health;
    return data;
  }
}

class TermExamResult {
  int? id;
  dynamic totalScore;
  dynamic maxAvgScore;
  double? avgScore;
  int? rank;
  String? term;
  dynamic teacherComment;
  String? grading;
  String? gradingEn;
  String? letterGrade;
  int? gradePoints;
  int? isFail;

  TermExamResult(
      {this.id,
      this.totalScore,
      this.maxAvgScore,
      this.avgScore,
      this.rank,
      this.term,
      this.teacherComment,
      this.grading,
      this.gradingEn,
      this.letterGrade,
      this.gradePoints,
      this.isFail});

  TermExamResult.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    totalScore = json['total_score'];
    maxAvgScore = json['max_avg_score'];
    avgScore = json['avg_score'];
    rank = json['rank'];
    term = json['term'];
    teacherComment = json['teacher_comment'];
    grading = json['grading'];
    gradingEn = json['grading_en'];
    letterGrade = json['letter_grade'];
    gradePoints = json['grade_points'];
    isFail = json['is_fail'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['total_score'] = this.totalScore;
    data['max_avg_score'] = this.maxAvgScore;
    data['avg_score'] = this.avgScore;
    data['rank'] = this.rank;
    data['term'] = this.term;
    data['teacher_comment'] = this.teacherComment;
    data['grading'] = this.grading;
    data['grading_en'] = this.gradingEn;
    data['letter_grade'] = this.letterGrade;
    data['grade_points'] = this.gradePoints;
    data['is_fail'] = this.isFail;
    return data;
  }
}

class TermSubjects {
  String? name;
  String? nameEn;
  String? short;
  dynamic maxScore;
  dynamic avg;
  dynamic score;
  int? rank;
  String? grading;
  String? gradingEn;
  String? letterGrade;
  String? teacherComment;

  TermSubjects(
      {this.name,
      this.nameEn,
      this.short,
      this.maxScore,
      this.avg,
      this.score,
      this.rank,
      this.grading,
      this.gradingEn,
      this.letterGrade,
      this.teacherComment});

  TermSubjects.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    nameEn = json['name_en'];
    short = json['short'];
    maxScore = json['max_score'];
    avg = json['avg'];
    score = json['score'];
    rank = json['rank'];
    grading = json['grading'];
    gradingEn = json['grading_en'];
    letterGrade = json['letter_grade'];
    teacherComment = json['teacher_comment'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['name_en'] = this.nameEn;
    data['short'] = this.short;
    data['max_score'] = this.maxScore;
    data['avg'] = this.avg;
    data['score'] = this.score;
    data['rank'] = this.rank;
    data['grading'] = this.grading;
    data['grading_en'] = this.gradingEn;
    data['letter_grade'] = this.letterGrade;
    data['teacher_comment'] = this.teacherComment;
    return data;
  }
}
