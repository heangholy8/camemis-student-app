 class ListMonthModel {
  List<MonthData>? data;

  ListMonthModel({this.data});

  ListMonthModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <MonthData>[];
      json['data'].forEach((v) {
        data!.add( MonthData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MonthData {
  int? classId;
  String? schoolyearId;
  String? className;
  String? classNameEn;
  int? sortkey;
  int? level;
  String? short;
  int? schoolyearStatus;
  String? schoolyearName;
  // List<ListMonths>? listMonths;
  // List<ListSubjects>? listSubjects;

  MonthData(
      {this.classId,
      this.schoolyearId,
      this.className,
      this.classNameEn,
      this.sortkey,
      this.level,
      this.short,
      this.schoolyearStatus,
      this.schoolyearName,
      // this.listMonths,
      // this.listSubjects
      });

  MonthData.fromJson(Map<String, dynamic> json) {
    classId = json['class_id'];
    schoolyearId = json['schoolyear_id'];
    className = json['class_name'];
    classNameEn = json['class_name_en'];
    sortkey = json['sortkey'];
    level = json['level'];
    short = json['short'];
    schoolyearStatus = json['schoolyear_status'];
    schoolyearName = json['schoolyear_name'];
    // if (json['list_months'] != null) {
    //   listMonths = <ListMonths>[];
    //   json['list_months'].forEach((v) {
    //     listMonths!.add(new ListMonths.fromJson(v));
    //   });
    // }
    // if (json['list_subjects'] != null) {
    //   listSubjects = <ListSubjects>[];
    //   json['list_subjects'].forEach((v) {
    //     listSubjects!.add(new ListSubjects.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['class_id'] = this.classId;
    data['schoolyear_id'] = this.schoolyearId;
    data['class_name'] = this.className;
    data['class_name_en'] = this.classNameEn;
    data['sortkey'] = this.sortkey;
    data['level'] = this.level;
    data['short'] = this.short;
    data['schoolyear_status'] = this.schoolyearStatus;
    data['schoolyear_name'] = this.schoolyearName;
    // if (this.listMonths != null) {
    //   data['list_months'] = this.listMonths!.map((v) => v.toJson()).toList();
    // }
    // if (this.listSubjects != null) {
    //   data['list_subjects'] =
    //       this.listSubjects!.map((v) => v.toJson()).toList();
    // }
    return data;
  }
}

class ListMonths {
  String? semester;
  String? name;
  List<Months>? months;
  String? startDate;
  String? endDate;
  String? nameEn;

  ListMonths(
      {this.semester,
      this.name,
      this.months,
      this.startDate,
      this.endDate,
      this.nameEn});

  ListMonths.fromJson(Map<String, dynamic> json) {
    semester = json['semester'];
    name = json['name'];
    if (json['months'] != null) {
      months = <Months>[];
      json['months'].forEach((v) {
        months!.add(new Months.fromJson(v));
      });
    }
    startDate = json['startDate'];
    endDate = json['endDate'];
    nameEn = json['name_en'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['semester'] = this.semester;
    data['name'] = this.name;
    if (this.months != null) {
      data['months'] = this.months!.map((v) => v.toJson()).toList();
    }
    data['startDate'] = this.startDate;
    data['endDate'] = this.endDate;
    data['name_en'] = this.nameEn;
    return data;
  }
}

class Months {
  String? month;
  String? displayMonth;
  String? year;
  String? displayMonthEn;
  bool? isCurrent;

  Months(
      {this.month,
      this.displayMonth,
      this.year,
      this.displayMonthEn,
      this.isCurrent});

  Months.fromJson(Map<String, dynamic> json) {
    month = json['month'];
    displayMonth = json['displayMonth'];
    year = json['year'];
    displayMonthEn = json['displayMonthEn'];
    isCurrent = json['isCurrent'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['month'] = this.month;
    data['displayMonth'] = this.displayMonth;
    data['year'] = this.year;
    data['displayMonthEn'] = this.displayMonthEn;
    data['isCurrent'] = this.isCurrent;
    return data;
  }
}

class ListSubjects {
  int? id;
  int? gradeSubjectId;
  String? guid;
  String? short;
  String? name;
  String? nameEn;
  int? status;
  int? subjectType;
  Null? description;
  int? coeffValue;
  int? numberSession;
  int? numberCredit;
  int? scoreMin;
  int? scoreMax;
  int? sortkey;
  int? level;
  int? internationalEdu;
  int? gradeAcademicType;
  int? complementaryScore;
  int? mapDefaultSubjectId;

  ListSubjects(
      {this.id,
      this.gradeSubjectId,
      this.guid,
      this.short,
      this.name,
      this.nameEn,
      this.status,
      this.subjectType,
      this.description,
      this.coeffValue,
      this.numberSession,
      this.numberCredit,
      this.scoreMin,
      this.scoreMax,
      this.sortkey,
      this.level,
      this.internationalEdu,
      this.gradeAcademicType,
      this.complementaryScore,
      this.mapDefaultSubjectId});

  ListSubjects.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    gradeSubjectId = json['grade_subject_id'];
    guid = json['guid'];
    short = json['short'];
    name = json['name'];
    nameEn = json['name_en'];
    status = json['status'];
    subjectType = json['subject_type'];
    description = json['description'];
    coeffValue = json['coeff_value'];
    numberSession = json['number_session'];
    numberCredit = json['number_credit'];
    scoreMin = json['score_min'];
    scoreMax = json['score_max'];
    sortkey = json['sortkey'];
    level = json['level'];
    internationalEdu = json['international_edu'];
    gradeAcademicType = json['grade_academic_type'];
    complementaryScore = json['complementary_score'];
    mapDefaultSubjectId = json['map_default_subject_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['grade_subject_id'] = this.gradeSubjectId;
    data['guid'] = this.guid;
    data['short'] = this.short;
    data['name'] = this.name;
    data['name_en'] = this.nameEn;
    data['status'] = this.status;
    data['subject_type'] = this.subjectType;
    data['description'] = this.description;
    data['coeff_value'] = this.coeffValue;
    data['number_session'] = this.numberSession;
    data['number_credit'] = this.numberCredit;
    data['score_min'] = this.scoreMin;
    data['score_max'] = this.scoreMax;
    data['sortkey'] = this.sortkey;
    data['level'] = this.level;
    data['international_edu'] = this.internationalEdu;
    data['grade_academic_type'] = this.gradeAcademicType;
    data['complementary_score'] = this.complementaryScore;
    data['map_default_subject_id'] = this.mapDefaultSubjectId;
    return data;
  }
}
