class MonthlyResultModel {
  bool? status;
  MonthlyData? data;

  MonthlyResultModel({this.status, this.data});

  MonthlyResultModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new MonthlyData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class MonthlyData {
  ResultMonthly? resultMonthly;
  List<Subject>? subject;
  int? totalStudent;

  MonthlyData({this.resultMonthly, this.subject, this.totalStudent});

  MonthlyData.fromJson(Map<String, dynamic> json) {
    resultMonthly = json['result_monthly'] != null
        ? new ResultMonthly.fromJson(json['result_monthly'])
        : null;
    if (json['subject'] != null) {
      subject = <Subject>[];
      json['subject'].forEach((v) {
        subject!.add(new Subject.fromJson(v));
      });
    }
    totalStudent = json['total_student'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resultMonthly != null) {
      data['result_monthly'] = this.resultMonthly!.toJson();
    }
    if (this.subject != null) {
      data['subject'] = this.subject!.map((v) => v.toJson()).toList();
    }
    data['total_student'] = this.totalStudent;
    return data;
  }
}

class ResultMonthly {
  String? id;
  dynamic performancePercentage;
  dynamic avgScore;
  dynamic maxAvgScore;
  dynamic totalScore;
  dynamic totalCoeff;
  int? rank;
  int? month;
  String? teacherComment;
  String? recommendation;
  String? behavior;
  int? absenceTotal;
  int? absenceWithPermission;
  int? absenceWithoutPermission;
  int? absenceExam;
  String? grading;
  String? gradingEn;
  String? letterGrade;
  int? gradePoints;
  int? isFail;

  ResultMonthly(
      {this.id,
      this.performancePercentage,
      this.avgScore,
      this.maxAvgScore,
      this.totalScore,
      this.totalCoeff,
      this.rank,
      this.month,
      this.teacherComment,
      this.recommendation,
      this.behavior,
      this.absenceTotal,
      this.absenceWithPermission,
      this.absenceWithoutPermission,
      this.absenceExam,
      this.grading,
      this.gradingEn,
      this.letterGrade,
      this.gradePoints,
      this.isFail});

  ResultMonthly.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    performancePercentage = json['performance_percentage'];
    avgScore = json['avg_score'];
    maxAvgScore = json['max_avg_score'];
    totalScore = json['total_score'];
    totalCoeff = json['total_coeff'];
    rank = json['rank'];
    month = json['month'];
    teacherComment = json['teacher_comment'];
    recommendation = json['recommendation'];
    behavior = json['behavior'];
    absenceTotal = json['absence_total'];
    absenceWithPermission = json['absence_with_permission'];
    absenceWithoutPermission = json['absence_without_permission'];
    absenceExam = json['absence_exam'];
    grading = json['grading'];
    gradingEn = json['grading_en'];
    letterGrade = json['letter_grade'];
    gradePoints = json['grade_points'];
    isFail = json['is_fail'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['performance_percentage'] = this.performancePercentage;
    data['avg_score'] = this.avgScore;
    data['max_avg_score'] = this.maxAvgScore;
    data['total_score'] = this.totalScore;
    data['total_coeff'] = this.totalCoeff;
    data['rank'] = this.rank;
    data['month'] = this.month;
    data['teacher_comment'] = this.teacherComment;
    data['recommendation'] = this.recommendation;
    data['behavior'] = this.behavior;
    data['absence_total'] = this.absenceTotal;
    data['absence_with_permission'] = this.absenceWithPermission;
    data['absence_without_permission'] = this.absenceWithoutPermission;
    data['absence_exam'] = this.absenceExam;
    data['grading'] = this.grading;
    data['grading_en'] = this.gradingEn;
    data['letter_grade'] = this.letterGrade;
    data['grade_points'] = this.gradePoints;
    data['is_fail'] = this.isFail;
    return data;
  }
}

class Subject {
  String? name;
  String? nameEn;
  String? short;
  int? maxScore;
  int? absentExam;
  dynamic score;
  dynamic rank;
  dynamic grading;
  dynamic gradingEn;
  dynamic letterGrade;
  dynamic teacherComment;
  bool isSelected = false;

  Subject(
      {this.name,
      this.nameEn,
      this.short,
      this.maxScore,
      this.absentExam,
      this.score,
      this.rank,
      this.grading,
      this.gradingEn,
      this.letterGrade,
      this.teacherComment,
      this.isSelected=false});

  Subject.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    nameEn = json['name_en'];
    short = json['short'];
    maxScore = json['max_score'];
    absentExam = json['absent_exam'];
    score = json['score'];
    rank = json['rank'];
    grading = json['grading'];
    gradingEn = json['grading_en'];
    letterGrade = json['letter_grade'];
    teacherComment = json['teacher_comment'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['name_en'] = this.nameEn;
    data['short'] = this.short;
    data['max_score'] = this.maxScore;
    data['absent_exam'] = this.absentExam;
    data['score'] = this.score;
    data['rank'] = this.rank;
    data['grading'] = this.grading;
    data['grading_en'] = this.gradingEn;
    data['letter_grade'] = this.letterGrade;
    data['teacher_comment'] = this.teacherComment;
    return data;
  }
}
