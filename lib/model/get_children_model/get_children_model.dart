import 'dart:io';


// To parse this JSON data, do
//
//     final childrenModel = childrenModelFromJson(jsonString);

import 'dart:convert';

ChildrenModel childrenModelFromJson(String str) => ChildrenModel.fromJson(json.decode(str));

String childrenModelToJson(ChildrenModel data) => json.encode(data.toJson());

class ChildrenModel {
    ChildrenModel({
        this.data,
    });

    List<Datum>? data;

    factory ChildrenModel.fromJson(Map<String, dynamic> json) => ChildrenModel(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}
class Datum {
    Datum({
        this.id,
        this.schoolCode,
        this.code,
        this.name,
        this.nameEn,
        this.firstname,
        this.lastname,
        this.firstnameEn,
        this.lastnameEn,
        this.gender,
        this.genderName,
        this.dob,
        this.phone,
        this.email,
        this.address,
        this.role,
        this.currentClass,
        this.profileMedia,
    });

    String? id;
    String? schoolCode;
    String? code;
    String? name;
    String? nameEn;
    String? firstname;
    String? lastname;
    String? firstnameEn;
    String? lastnameEn;
    int? gender;
    String? genderName;
    String? dob;
    String ?phone;
    String ?email;
    String ?address;
    int? role;
    CurrentClass ?currentClass;
    ProfileMedia ?profileMedia;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"]==""||json["firstname_en"]==null?"----":json["firstname_en"],
        lastnameEn: json["lastname_en"]==""||json["lastname_en"]==null?"----":json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        email: json["email"],
        address: json["address"],
        role: json["role"],
        currentClass:json["current_class"]==null?null:CurrentClass.fromJson(json["current_class"]),
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn==""||firstnameEn==null?"----":firstnameEn,
        "lastname_en": lastnameEn==""||lastnameEn==null?"----":lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "email": email,
        "address": address,
        "role": role,
        "current_class":currentClass==null?null:currentClass?.toJson(),
        "profileMedia": profileMedia?.toJson(),
    };
}

class CurrentClass {
    CurrentClass({
        this.classId,
        this.schoolyearId,
        this.className,
        this.classNameEn,
        this.sortkey,
        this.level,
        this.short,
        this.schoolyearName,
        this.listMonths,
        this.listSubjects,
    });

    int? classId;
    String? schoolyearId;
    String? className;
    String? classNameEn;
    int? sortkey;
    int? level;
    String ?short;
    String ?schoolyearName;
    List<ListMonth>? listMonths;
    List<ListSubject> ?listSubjects;

    factory CurrentClass.fromJson(Map<String, dynamic> json) => CurrentClass(
        classId: json["class_id"],
        schoolyearId: json["schoolyear_id"],
        className: json["class_name"],
        classNameEn: json["class_name_en"],
        sortkey: json["sortkey"],
        level: json["level"],
        short: json["short"],
        schoolyearName: json["schoolyear_name"],
        listMonths: List<ListMonth>.from(json["list_months"].map((x) => ListMonth.fromJson(x))),
        listSubjects: List<ListSubject>.from(json["list_subjects"].map((x) => ListSubject.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "class_id": classId,
        "schoolyear_id": schoolyearId,
        "class_name": className,
        "class_name_en": classNameEn,
        "sortkey": sortkey,
        "level": level,
        "short": short,
        "schoolyear_name": schoolyearName,
        "list_months": List<dynamic>.from(listMonths!.map((x) => x.toJson())),
        "list_subjects": List<dynamic>.from(listSubjects!.map((x) => x.toJson())),
    };
}

class ListMonth {
    ListMonth({
        this.semester,
        this.name,
        this.months,
        this.startDate,
        this.endDate,
        this.nameEn,
    });

    String? semester;
    String? name;
    List<Month>? months;
    String? startDate;
    String ?endDate;
    String? nameEn;

    factory ListMonth.fromJson(Map<String, dynamic> json) => ListMonth(
        semester: json["semester"] ,
        name: json["name"],
        months: List<Month>.from(json["months"].map((x) => Month.fromJson(x))),
        startDate: json["startDate"],
        endDate: json["endDate"],
        nameEn: json["name_en"],
    );

    Map<String, dynamic> toJson() => {
        "semester": semester,
        "name": name,
        "months": List<dynamic>.from(months!.map((x) => x.toJson())),
        "startDate": startDate ,
        "endDate": endDate ,
        "name_en": nameEn ,
    };
}

class Month {
    Month({
        this.month,
        this.displayMonth,
        this.year,
        this.displayMonthEn,
        this.isCurrent,
    });

    String? month;
    String? displayMonth;
    String? year;
    String? displayMonthEn;
    bool? isCurrent;

    factory Month.fromJson(Map<String, dynamic> json) => Month(
        month: json["month"],
        displayMonth: json["displayMonth"],
        year: json["year"],
        displayMonthEn: json["displayMonthEn"],
        isCurrent: json["isCurrent"],
    );

    Map<String, dynamic> toJson() => {
        "month": month,
        "displayMonth": displayMonth,
        "year": year,
        "displayMonthEn": displayMonthEn,
        "isCurrent": isCurrent,
    };
}

class ListSubject {
    ListSubject({
        this.id,
        this.gradeSubjectId,
        this.guid,
        this.short,
        this.name,
        this.nameEn,
        this.status,
        this.subjectType,
        this.description,
        this.coeffValue,
        this.numberSession,
        this.numberCredit,
        this.scoreMin,
        this.scoreMax,
        this.sortkey,
        this.level,
        this.internationalEdu,
        this.gradeAcademicType,
        this.complementaryScore,
        this.mapDefaultSubjectId,
    });

    int? id;
    int? gradeSubjectId;
    String? guid;
    String? short;
    String? name;
    String? nameEn;
    int? status;
    int ?subjectType;
    dynamic description;
    double ?coeffValue;
    int? numberSession;
    int ?numberCredit;
    int ?scoreMin;
    int ?scoreMax;
    int ?sortkey;
    int ?level;
    int ?internationalEdu;
    int ?gradeAcademicType;
    int ?complementaryScore;
    int ?mapDefaultSubjectId;

    factory ListSubject.fromJson(Map<String, dynamic> json) => ListSubject(
        id: json["id"],
        gradeSubjectId: json["grade_subject_id"],
        guid: json["guid"],
        short: json["short"],
        name: json["name"],
        nameEn: json["name_en"],
        status: json["status"],
        subjectType: json["subject_type"],
        description: json["description"],
        coeffValue: json["coeff_value"].toDouble(),
        numberSession: json["number_session"],
        numberCredit: json["number_credit"],
        scoreMin: json["score_min"],
        scoreMax: json["score_max"],
        sortkey: json["sortkey"],
        level: json["level"],
        internationalEdu: json["international_edu"],
        gradeAcademicType: json["grade_academic_type"],
        complementaryScore: json["complementary_score"],
        mapDefaultSubjectId: json["map_default_subject_id"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "grade_subject_id": gradeSubjectId,
        "guid": guid,
        "short": short,
        "name": name,
        "name_en": nameEn,
        "status": status,
        "subject_type": subjectType,
        "description": description,
        "coeff_value": coeffValue,
        "number_session": numberSession,
        "number_credit": numberCredit,
        "score_min": scoreMin,
        "score_max": scoreMax,
        "sortkey": sortkey,
        "level": level,
        "international_edu": internationalEdu,
        "grade_academic_type": gradeAcademicType,
        "complementary_score": complementaryScore,
        "map_default_subject_id": mapDefaultSubjectId,
    };
}

class ProfileMedia {
    ProfileMedia({
        this.fileShow,
        this.fileThumbnail,
        this.imageprofile,
    });

    String ?fileShow;
    String ?fileThumbnail;
    File? imageprofile;

    factory ProfileMedia.fromJson(Map<String, dynamic> json) => ProfileMedia(
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
    );

    Map<String, dynamic> toJson() => {
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
    };
}

