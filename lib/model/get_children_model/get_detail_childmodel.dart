import 'dart:convert';

GetDetailChildModel myTaskDashboardModelFromJson(String str) => GetDetailChildModel.fromJson(json.decode(str));

String myTaskDashboardModelToJson(GetDetailChildModel data) => json.encode(data.toJson());

class GetDetailChildModel {
    GetDetailChildModel({
        this.data,
    });

    Data? data;

    factory GetDetailChildModel.fromJson(Map<String, dynamic> json) => GetDetailChildModel(
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "data": data!.toJson(),
    };
}
class Data {
    Data({
        this.classId,
        this.schoolyearId,
        this.className,
        this.classNameEn,
        this.sortkey,
        this.level,
        this.short,
        this.schoolyearName,
        this.listMonths,
        this.listSubjects,
    });

    int? classId;
    String ?schoolyearId;
    String? className;
    String? classNameEn;
    int? sortkey;
    int? level;
    String? short;
    String? schoolyearName;
    List<ListMonth>? listMonths;
    List<ListSubject> ?listSubjects;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        classId: json["class_id"],
        schoolyearId: json["schoolyear_id"],
        className: json["class_name"],
        classNameEn: json["class_name_en"],
        sortkey: json["sortkey"],
        level: json["level"],
        short: json["short"],
        schoolyearName: json["schoolyear_name"],
        listMonths: List<ListMonth>.from(json["list_months"].map((x) => ListMonth.fromJson(x))),
        listSubjects: List<ListSubject>.from(json["list_subjects"].map((x) => ListSubject.fromJson(x))),
    );

  Map<String, dynamic> toJson() => {
        "class_id": classId,
        "schoolyear_id": schoolyearId,
        "class_name": className,
        "class_name_en": classNameEn,
        "sortkey": sortkey,
        "level": level,
        "short": short,
        "schoolyear_name": schoolyearName,
        "list_months": List<dynamic>.from(listMonths!.map((x) => x.toJson())),
        "list_subjects": List<dynamic>.from(listSubjects!.map((x) => x.toJson())),
    };
}

class ListMonth {
  ListMonth(
      {this.semester,
      this.name,
      this.months,
      this.startDate,
      this.endDate,
      this.nameEn,
      this.isSelected = false});

  String? semester;
  String? name;
  List<Month>? months;
  String? startDate;
  String? endDate;
  String? nameEn;
  bool isSelected;

  factory ListMonth.fromJson(Map<String, dynamic> json) => ListMonth(
        semester: json["semester"],
        name: json["name"],
        months: List<Month>.from(json["months"].map((x) => Month.fromJson(x))),
        startDate: json["startDate"],
        endDate: json["endDate"] ,
        nameEn: json["name_en"],
      );

    Map<String, dynamic> toJson() => {
        "semester": semester ,
        "name": name,
        "months": List<dynamic>.from(months!.map((x) => x.toJson())),
        "startDate": startDate,
        "endDate": endDate,
        "name_en": nameEn ,
    };
}

class Month {
  Month(
      {this.month,
      this.displayMonth,
      this.year,
      this.displayMonthEn,
      this.isCurrent,
      this.isSelected = false});

  String? month;
  String? displayMonth;
  String? year;
  String? displayMonthEn;
  bool? isCurrent;
  bool isSelected;

  factory Month.fromJson(Map<String, dynamic> json) => Month(
        month: json["month"],
        displayMonth: json["displayMonth"],
        year: json["year"],
        displayMonthEn: json["displayMonthEn"],
        isCurrent: json["isCurrent"],
      );

  Map<String, dynamic> toJson() => {
        "month": month,
        "displayMonth": displayMonth,
        "year": year,
        "displayMonthEn": displayMonthEn,
        "isCurrent": isCurrent,
      };
}

class ListSubject {
    ListSubject({
        this.id,
        this.gradeSubjectId,
        this.guid,
        this.short,
        this.name,
        this.nameEn,
        this.status,
        this.subjectType,
        this.description,
        this.coeffValue,
        this.numberSession,
        this.numberCredit,
        this.scoreMin,
        this.scoreMax,
        this.sortkey,
        this.level,
        this.internationalEdu,
        this.gradeAcademicType,
        this.complementaryScore,
        this.mapDefaultSubjectId,
    });

    int? id;
    int? gradeSubjectId;
    String? guid;
    String? short;
    String? name;
    String? nameEn;
    int? status;
    int? subjectType;
    dynamic description;
    double? coeffValue;
    int? numberSession;
    int ?numberCredit;
    int? scoreMin;
    int? scoreMax;
    int ?sortkey;
    int? level;
    int? internationalEdu;
    int? gradeAcademicType;
    int? complementaryScore;
    int? mapDefaultSubjectId;

    factory ListSubject.fromJson(Map<String, dynamic> json) => ListSubject(
        id: json["id"],
        gradeSubjectId: json["grade_subject_id"],
        guid: json["guid"],
        short: json["short"],
        name: json["name"],
        nameEn: json["name_en"],
        status: json["status"],
        subjectType: json["subject_type"],
        description: json["description"],
        coeffValue: json["coeff_value"].toDouble(),
        numberSession: json["number_session"],
        numberCredit: json["number_credit"],
        scoreMin: json["score_min"],
        scoreMax: json["score_max"],
        sortkey: json["sortkey"],
        level: json["level"],
        internationalEdu: json["international_edu"],
        gradeAcademicType: json["grade_academic_type"],
        complementaryScore: json["complementary_score"],
        mapDefaultSubjectId: json["map_default_subject_id"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "grade_subject_id": gradeSubjectId,
        "guid": guid,
        "short": short,
        "name": name,
        "name_en": nameEn,
        "status": status,
        "subject_type": subjectType,
        "description": description,
        "coeff_value": coeffValue,
        "number_session": numberSession,
        "number_credit": numberCredit,
        "score_min": scoreMin,
        "score_max": scoreMax,
        "sortkey": sortkey,
        "level": level,
        "international_edu": internationalEdu,
        "grade_academic_type": gradeAcademicType,
        "complementary_score": complementaryScore,
        "map_default_subject_id": mapDefaultSubjectId,
    };
}
