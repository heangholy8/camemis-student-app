// To parse this JSON data, do
//
//     final checkVersionUpdateModel = checkVersionUpdateModelFromJson(jsonString);

import 'dart:convert';

CheckVersionUpdateModel? checkVersionUpdateModelFromJson(String str) => CheckVersionUpdateModel.fromJson(json.decode(str));

String checkVersionUpdateModelToJson(CheckVersionUpdateModel? data) => json.encode(data!.toJson());

class CheckVersionUpdateModel {
    CheckVersionUpdateModel({
        this.data,
        this.message,
    });

    Data? data;
    String? message;

    factory CheckVersionUpdateModel.fromJson(Map<String, dynamic> json) => CheckVersionUpdateModel(
        data: Data.fromJson(json["data"]),
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "data": data!.toJson(),
        "message": message,
    };
}

class Data {
    Data({
        this.id,
        this.appType,
        this.releaseDateAndroid,
        this.releaseDateIos,
        this.versionAndroid,
        this.versionIos,
        this.playStoreLink,
        this.appStoreLink,
        this.featuresRelease,
        this.isActive,
    });

    int? id;
    int? appType;
    String? releaseDateIos;
    String? releaseDateAndroid;
    String? versionAndroid;
    String? versionIos;
    dynamic playStoreLink;
    dynamic appStoreLink;
    String? featuresRelease;
    bool? isActive;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        appType: json["app_type"],
        versionAndroid: json["version_android"],
        releaseDateIos: json["release_date_ios"],
        releaseDateAndroid: json["release_date"],
        versionIos: json["version_ios"],
        playStoreLink: json["play_store_link"],
        appStoreLink: json["app_store_link"],
        featuresRelease: json["features_release"],
        isActive: json["is_active"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "app_type": appType,
        "version_android": versionAndroid,
        "release_date_ios": releaseDateIos,
        "release_date": releaseDateAndroid,
        "version_ios": versionIos,
        "play_store_link": playStoreLink,
        "app_store_link": appStoreLink,
        "features_release": featuresRelease,
        "is_active": isActive,
    };
}
