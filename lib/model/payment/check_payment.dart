// To parse this JSON data, do
//
//     final checkPaymentModel = checkPaymentModelFromJson(jsonString);

import 'dart:convert';

CheckPaymentModel checkPaymentModelFromJson(String str) => CheckPaymentModel.fromJson(json.decode(str));

String checkPaymentModelToJson(CheckPaymentModel data) => json.encode(data.toJson());

class CheckPaymentModel {
    CheckPaymentModel({
        this.data,
        this.status,
    });

    Data? data;
    bool? status;

    factory CheckPaymentModel.fromJson(Map<String, dynamic> json) => CheckPaymentModel(
        data: json["data"]==null?null: Data.fromJson(json["data"]),
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "data": data==null?null:data!.toJson(),
        "status": status,
    };
}

class Data {
    Data({
        this.id,
        this.guardianId,
        this.invoiceNumber,
        this.status,
        this.piadAt,
        this.amount,
        this.currency,
        this.choosePayOptionName,
        this.choosePayOptionNameEn,
        this.choosePayOption,
        this.paymentMethod,
        this.transactionId,
        this.expiredAt,
        this.expiredIn,
        this.createdAt,
    });

    int? id;
    String? guardianId;
    String? invoiceNumber;
    String? status;
    String? piadAt;
    int? amount;
    String? currency;
    String? choosePayOptionName;
    String? choosePayOptionNameEn;
    int? choosePayOption;
    String? paymentMethod;
    String? transactionId;
    String? expiredAt;
    int? expiredIn;
    String? createdAt;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        guardianId: json["guardian_id"],
        invoiceNumber: json["invoice_number"],
        status: json["status"],
        piadAt: json["piad_at"],
        amount: json["amount"],
        currency: json["currency"],
        choosePayOptionName: json["choose_pay_option_name"],
        choosePayOptionNameEn: json["choose_pay_option_name_en"],
        choosePayOption: json["choose_pay_option"],
        paymentMethod: json["payment_method"],
        transactionId: json["transaction_id"],
        expiredAt: json["expired_at"],
        expiredIn: json["expired_in"],
        createdAt: json["created_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "guardian_id": guardianId,
        "invoice_number": invoiceNumber,
        "status": status,
        "piad_at": piadAt,
        "amount": amount,
        "currency": currency,
        "choose_pay_option_name": choosePayOptionName,
        "choose_pay_option_name_en": choosePayOptionNameEn,
        "choose_pay_option": choosePayOption,
        "payment_method": paymentMethod,
        "transaction_id": transactionId,
        "expired_at": expiredAt,
        "expired_in": expiredIn,
        "created_at": createdAt,
    };
}
