// To parse this JSON data, do
//
//     final schoolTimelineModel = schoolTimelineModelFromJson(jsonString);

import 'dart:convert';

SchoolTimelineModel schoolTimelineModelFromJson(String str) => SchoolTimelineModel.fromJson(json.decode(str));

String schoolTimelineModelToJson(SchoolTimelineModel data) => json.encode(data.toJson());

class SchoolTimelineModel {
    SchoolTimelineModel({
         this.data,
         this.links,
         this.meta,
    });

    List<Datum>? data;
    Links? links;
    Meta? meta;

    factory SchoolTimelineModel.fromJson(Map<String, dynamic> json) => SchoolTimelineModel(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        links: Links.fromJson(json["links"]),
        meta: Meta.fromJson(json["meta"]),
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "links": links!.toJson(),
        "meta": meta!.toJson(),
    };
}

class Datum {
    Datum({
         this.id,
         this.parent,
         this.postText,
         this.user,
         this.comments,
         this.isLike,
         this.likeCount,
         this.isReact,
        this.reactionType,
         this.totalReact,
         this.totalReactType,
         this.commentCount,
         this.attachments,
         this.youtubes,
         this.createdDate,
         this.updatedDate,
    });

    dynamic id;
    dynamic parent;
    String? postText;
    DatumUser? user;
    List<DatumComment>? comments;
    dynamic isLike;
    dynamic likeCount;
    dynamic isReact;
    dynamic reactionType;
    dynamic totalReact;
    List<TotalReactType>? totalReactType;
    dynamic commentCount;
    List<Attachment> ?attachments;
    List<dynamic> ?youtubes;
    String? createdDate;
    String? updatedDate;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        parent: json["parent"],
        postText: json["post_text"],
        user: DatumUser.fromJson(json["user"]),
        comments: List<DatumComment>.from(json["comments"].map((x) => DatumComment.fromJson(x))),
        isLike: json["is_like"],
        likeCount: json["like_count"],
        isReact: json["is_react"],
        reactionType: json["reaction_type"],
        totalReact: json["total_react"],
        totalReactType: List<TotalReactType>.from(json["total_react_type"].map((x) => TotalReactType.fromJson(x))),
        commentCount: json["comment_count"],
        attachments: List<Attachment>.from(json["attachments"].map((x) => Attachment.fromJson(x))),
        youtubes: List<dynamic>.from(json["youtubes"].map((x) => x)),
        createdDate: json["created_date"],
        updatedDate: json["updated_date"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "parent": parent,
        "post_text": postText,
        "user": user!.toJson(),
        "comments": List<dynamic>.from(comments!.map((x) => x.toJson())),
        "is_like": isLike,
        "like_count": likeCount,
        "is_react": isReact,
        "reaction_type": reactionType,
        "total_react": totalReact,
        "total_react_type": List<dynamic>.from(totalReactType!.map((x) => x.toJson())),
        "comment_count": commentCount,
        "attachments": List<dynamic>.from(attachments!.map((x) => x.toJson())),
        "youtubes": List<dynamic>.from(youtubes!.map((x) => x)),
        "created_date": createdDate,
        "updated_date": updatedDate,
    };
}

class Attachment {
    Attachment({
         this.id,
         this.fileName,
         this.fileSize,
         this.fileType,
         this.fileShow,
         this.fileThumbnail,
         this.fileIndex,
         this.fileArea,
         this.objectId,
         this.schoolUrl,
         this.postDate,
         this.isGdrive,
         this.fileGdriveId,
         this.isS3,
    });

    dynamic id;
    String? fileName;
    String? fileSize;
    String? fileType;
    String? fileShow;
    String? fileThumbnail;
    String? fileIndex;
    String? fileArea;
    String? objectId;
    String? schoolUrl;
    String? postDate;
    dynamic isGdrive;
    String ?fileGdriveId;
    dynamic isS3;

    factory Attachment.fromJson(Map<String, dynamic> json) => Attachment(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
    };
}

class DatumComment {
    DatumComment({
         this.id,
         this.parent,
         this.postText,
         this.user,
         this.comments,
         this.isLike,
         this.likeCount,
         this.isReact,
        this.reactionType,
         this.totalReact,
         this.totalReactType,
         this.commentCount,
         this.attachments,
         this.youtubes,
         this.createdDate,
         this.updatedDate,
    });

    dynamic id;
    dynamic parent;
    String? postText;
    FluffyUser? user;
    List<PurpleComment> ?comments;
    dynamic isLike;
    dynamic likeCount;
    dynamic isReact;
    dynamic reactionType;
    dynamic totalReact;
    List<TotalReactType> ?totalReactType;
    dynamic commentCount;
    List<dynamic>? attachments;
    List<dynamic>? youtubes;
    String? createdDate;
    String? updatedDate;

    factory DatumComment.fromJson(Map<String, dynamic> json) => DatumComment(
        id: json["id"],
        parent: json["parent"],
        postText: json["post_text"],
        user: FluffyUser.fromJson(json["user"]),
        comments: List<PurpleComment>.from(json["comments"].map((x) => PurpleComment.fromJson(x))),
        isLike: json["is_like"],
        likeCount: json["like_count"],
        isReact: json["is_react"],
        reactionType: json["reaction_type"],
        totalReact: json["total_react"],
        totalReactType: List<TotalReactType>.from(json["total_react_type"].map((x) => TotalReactType.fromJson(x))),
        commentCount: json["comment_count"],
        attachments: List<dynamic>.from(json["attachments"].map((x) => x)),
        youtubes: List<dynamic>.from(json["youtubes"].map((x) => x)),
        createdDate: json["created_date"],
        updatedDate: json["updated_date"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "parent": parent,
        "post_text": postText,
        "user": user!.toJson(),
        "comments": List<dynamic>.from(comments!.map((x) => x.toJson())),
        "is_like": isLike,
        "like_count": likeCount,
        "is_react": isReact,
        "reaction_type": reactionType,
        "total_react": totalReact,
        "total_react_type": List<dynamic>.from(totalReactType!.map((x) => x.toJson())),
        "comment_count": commentCount,
        "attachments": List<dynamic>.from(attachments!.map((x) => x)),
        "youtubes": List<dynamic>.from(youtubes!.map((x) => x)),
        "created_date": createdDate,
        "updated_date": updatedDate,
    };
}

class PurpleComment {
    PurpleComment({
         this.id,
         this.parent,
         this.postText,
         this.user,
         this.comments,
         this.isLike,
         this.likeCount,
         this.isReact,
        this.reactionType,
         this.totalReact,
         this.totalReactType,
         this.commentCount,
         this.attachments,
         this.youtubes,
         this.createdDate,
         this.updatedDate,
    });

    dynamic id;
    dynamic parent;
    String? postText;
    FluffyUser? user;
    List<FluffyComment> ?comments;
    dynamic isLike;
    dynamic likeCount;
    dynamic isReact;
    dynamic reactionType;
    int ?totalReact;
    List<dynamic> ?totalReactType;
    dynamic commentCount;
    List<dynamic>? attachments;
    List<dynamic>? youtubes;
    String? createdDate;
    String? updatedDate;

    factory PurpleComment.fromJson(Map<String, dynamic> json) => PurpleComment(
        id: json["id"],
        parent: json["parent"],
        postText: json["post_text"],
        user: FluffyUser.fromJson(json["user"]),
        comments: List<FluffyComment>.from(json["comments"].map((x) => FluffyComment.fromJson(x))),
        isLike: json["is_like"],
        likeCount: json["like_count"],
        isReact: json["is_react"],
        reactionType: json["reaction_type"],
        totalReact: json["total_react"],
        totalReactType: List<dynamic>.from(json["total_react_type"].map((x) => x)),
        commentCount: json["comment_count"],
        attachments: List<dynamic>.from(json["attachments"].map((x) => x)),
        youtubes: List<dynamic>.from(json["youtubes"].map((x) => x)),
        createdDate: json["created_date"],
        updatedDate: json["updated_date"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "parent": parent,
        "post_text": postText,
        "user": user!.toJson(),
        "comments": List<dynamic>.from(comments!.map((x) => x.toJson())),
        "is_like": isLike,
        "like_count": likeCount,
        "is_react": isReact,
        "reaction_type": reactionType,
        "total_react": totalReact,
        "total_react_type": List<dynamic>.from(totalReactType!.map((x) => x)),
        "comment_count": commentCount,
        "attachments": List<dynamic>.from(attachments!.map((x) => x)),
        "youtubes": List<dynamic>.from(youtubes!.map((x) => x)),
        "created_date": createdDate,
        "updated_date": updatedDate,
    };
}

class FluffyComment {
    FluffyComment({
         this.id,
         this.parent,
         this.postText,
         this.user,
         this.comments,
         this.isLike,
         this.likeCount,
         this.isReact,
        this.reactionType,
         this.totalReact,
         this.totalReactType,
         this.commentCount,
         this.attachments,
         this.youtubes,
         this.createdDate,
         this.updatedDate,
    });

    dynamic id;
    dynamic parent;
    String? postText;
    PurpleUser ?user;
    List<dynamic>? comments;
    dynamic isLike;
    dynamic likeCount;
    dynamic isReact;
    dynamic reactionType;
    dynamic totalReact;
    List<dynamic>? totalReactType;
    dynamic commentCount;
    List<dynamic>? attachments;
    List<dynamic>? youtubes;
    String? createdDate;
    String? updatedDate;

    factory FluffyComment.fromJson(Map<String, dynamic> json) => FluffyComment(
        id: json["id"],
        parent: json["parent"],
        postText: json["post_text"],
        user: PurpleUser.fromJson(json["user"]),
        comments: List<dynamic>.from(json["comments"].map((x) => x)),
        isLike: json["is_like"],
        likeCount: json["like_count"],
        isReact: json["is_react"],
        reactionType: json["reaction_type"],
        totalReact: json["total_react"],
        totalReactType: List<dynamic>.from(json["total_react_type"].map((x) => x)),
        commentCount: json["comment_count"],
        attachments: List<dynamic>.from(json["attachments"].map((x) => x)),
        youtubes: List<dynamic>.from(json["youtubes"].map((x) => x)),
        createdDate: json["created_date"],
        updatedDate: json["updated_date"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "parent": parent,
        "post_text": postText,
        "user": user!.toJson(),
        "comments": List<dynamic>.from(comments!.map((x) => x)),
        "is_like": isLike,
        "like_count": likeCount,
        "is_react": isReact,
        "reaction_type": reactionType,
        "total_react": totalReact,
        "total_react_type": List<dynamic>.from(totalReactType!.map((x) => x)),
        "comment_count": commentCount,
        "attachments": List<dynamic>.from(attachments!.map((x) => x)),
        "youtubes": List<dynamic>.from(youtubes!.map((x) => x)),
        "created_date": createdDate,
        "updated_date": updatedDate,
    };
}

class PurpleUser {
    PurpleUser({
         this.id,
         this.schoolCode,
         this.code,
         this.name,
         this.nameEn,
         this.firstname,
         this.lastname,
         this.firstnameEn,
         this.lastnameEn,
         this.gender,
         this.genderName,
         this.dob,
         this.phone,
         this.email,
         this.address,
         this.role,
         this.isVerify,
         this.profileMedia,
         this.qrUrl,
    });

    String? id;
    String? schoolCode;
    String? code;
    String? name;
    String? nameEn;
    String ?firstname;
    String? lastname;
    String? firstnameEn;
    String? lastnameEn;
    dynamic gender;
    String ?genderName;
    String? dob;
    String? phone;
    String? email;
    String? address;
    String? role;
    dynamic isVerify;
    Attachment? profileMedia;
    String? qrUrl;

    factory PurpleUser.fromJson(Map<String, dynamic> json) => PurpleUser(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        email: json["email"],
        address: json["address"],
        role: json["role"],
        isVerify: json["is_verify"],
        profileMedia: Attachment.fromJson(json["profileMedia"]),
        qrUrl: json["qr_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "email": email,
        "address": address,
        "role": role,
        "is_verify": isVerify,
        "profileMedia": profileMedia!.toJson(),
        "qr_url": qrUrl,
    };
}

class FluffyUser {
    FluffyUser({
         this.id,
         this.schoolCode,
         this.code,
         this.name,
         this.nameEn,
         this.firstname,
         this.lastname,
         this.firstnameEn,
         this.lastnameEn,
         this.gender,
         this.genderName,
         this.dob,
         this.phone,
         this.email,
         this.address,
         this.role,
        this.isVerify,
         this.profileMedia,
        this.qrUrl,
        this.framework,
        this.prokas,
    });

    String? id;
    String? schoolCode;
    String? code;
    String? name;
    String? nameEn;
    String? firstname;
    String? lastname;
    String? firstnameEn;
    String? lastnameEn;
    dynamic gender;
    String? genderName;
    String? dob;
    String? phone;
    String? email;
    String? address;
    dynamic role;
    dynamic isVerify;
    Attachment? profileMedia;
    String? qrUrl;
    String? framework;
    String? prokas;

    factory FluffyUser.fromJson(Map<String, dynamic> json) => FluffyUser(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        email: json["email"],
        address: json["address"],
        role: json["role"],
        isVerify: json["is_verify"],
        profileMedia: Attachment.fromJson(json["profileMedia"]),
        qrUrl: json["qr_url"],
        framework: json["framework"],
        prokas: json["prokas"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "email": email,
        "address": address,
        "role": role,
        "is_verify": isVerify,
        "profileMedia": profileMedia!.toJson(),
        "qr_url": qrUrl,
        "framework": framework,
        "prokas": prokas,
    };
}

class TotalReactType {
    TotalReactType({
         this.reactionType,
         this.total,
    });

    dynamic reactionType;
    dynamic total;

    factory TotalReactType.fromJson(Map<String, dynamic> json) => TotalReactType(
        reactionType: json["reaction_type"],
        total: json["total"],
    );

    Map<String, dynamic> toJson() => {
        "reaction_type": reactionType,
        "total": total,
    };
}

class DatumUser {
    DatumUser({
         this.id,
         this.schoolCode,
         this.code,
         this.name,
         this.nameEn,
         this.firstname,
         this.lastname,
         this.firstnameEn,
         this.lastnameEn,
         this.gender,
         this.genderName,
         this.dob,
         this.phone,
        this.isVerify,
         this.email,
         this.address,
         this.framework,
         this.prokas,
         this.role,
         this.profileMedia,
    });

    String? id;
    String? schoolCode;
    String? code;
    String? name;
    String? nameEn;
    String? firstname;
    String? lastname;
    String? firstnameEn;
    String? lastnameEn;
    dynamic gender;
    String? genderName;
    String? dob;
    String? phone;
    dynamic isVerify;
    String? email;
    String? address;
    String? framework;
    String? prokas;
    dynamic role;
    Attachment? profileMedia;

    factory DatumUser.fromJson(Map<String, dynamic> json) => DatumUser(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        isVerify: json["is_verify"],
        email: json["email"],
        address: json["address"],
        framework: json["framework"],
        prokas: json["prokas"],
        role: json["role"],
        profileMedia: Attachment.fromJson(json["profileMedia"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "is_verify": isVerify,
        "email": email,
        "address": address,
        "framework": framework,
        "prokas": prokas,
        "role": role,
        "profileMedia": profileMedia!.toJson(),
    };
}

class Links {
    Links({
         this.first,
         this.last,
        this.prev,
         this.next,
    });

    String? first;
    String? last;
    dynamic prev;
    String ?next;

    factory Links.fromJson(Map<String, dynamic> json) => Links(
        first: json["first"],
        last: json["last"],
        prev: json["prev"],
        next: json["next"],
    );

    Map<String, dynamic> toJson() => {
        "first": first,
        "last": last,
        "prev": prev,
        "next": next,
    };
}

class Meta {
    Meta({
         this.currentPage,
         this.from,
         this.lastPage,
         this.path,
         this.perPage,
         this.to,
         this.total,
    });

    dynamic currentPage;
    dynamic from;
    dynamic lastPage;
    String? path;
    dynamic perPage;
    dynamic to;
    dynamic total;

    factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        currentPage: json["current_page"],
        from: json["from"],
        lastPage: json["last_page"],
        path: json["path"],
        perPage: json["per_page"],
        to: json["to"],
        total: json["total"],
    );

    Map<String, dynamic> toJson() => {
        "current_page": currentPage,
        "from": from,
        "last_page": lastPage,
        "path": path,
        "per_page": perPage,
        "to": to,
        "total": total,
    };
}
