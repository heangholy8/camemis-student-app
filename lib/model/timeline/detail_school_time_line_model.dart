
import 'dart:convert';

GetDetailSchoolTimelineModel getDetailSchoolTimelineModelFromJson(String str) => GetDetailSchoolTimelineModel.fromJson(json.decode(str));

String getDetailSchoolTimelineModelToJson(GetDetailSchoolTimelineModel data) => json.encode(data.toJson());

class GetDetailSchoolTimelineModel {
    GetDetailSchoolTimelineModel({
        this.data,
    });

    Data? data;

    factory GetDetailSchoolTimelineModel.fromJson(Map<String, dynamic> json) => GetDetailSchoolTimelineModel(
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "data": data!.toJson(),
    };
}
class Data {
    Data({
        this.id,
        this.parent,
        this.postText,
        this.user,
        this.comments,
        this.isLike,
        this.likeCount,
        this.isReact,
        this.reactionType,
        this.totalReact,
        this.totalReactType,
        this.commentCount,
        this.attachments,
        this.youtubes,
        this.createdDate,
        this.updatedDate,
    });
    int? id;
    int? parent;
    String? postText;
    DataUser? user;
    List<DataComment>? comments;
    int? isLike;
    int? likeCount;
    int? isReact;
    dynamic reactionType;
    int? totalReact;
    List<TotalReactType>? totalReactType;
    int? commentCount;
    List<Attachment>? attachments;
    List<dynamic>? youtubes;
    String? createdDate;
    String? updatedDate;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        parent: json["parent"],
        postText: json["post_text"],
        user: DataUser.fromJson(json["user"]),
        comments: List<DataComment>.from(json["comments"].map((x) => DataComment.fromJson(x))),
        isLike: json["is_like"],
        likeCount: json["like_count"],
        isReact: json["is_react"],
        reactionType: json["reaction_type"],
        totalReact: json["total_react"],
        totalReactType: List<TotalReactType>.from(json["total_react_type"].map((x) => TotalReactType.fromJson(x))),
        commentCount: json["comment_count"],
        attachments: List<Attachment>.from(json["attachments"].map((x) => Attachment.fromJson(x))),
        youtubes: List<dynamic>.from(json["youtubes"].map((x) => x)),
        createdDate: json["created_date"],
        updatedDate: json["updated_date"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "parent": parent,
        "post_text": postText,
        "user": user!.toJson(),
        "comments": List<dynamic>.from(comments!.map((x) => x.toJson())),
        "is_like": isLike,
        "like_count": likeCount,
        "is_react": isReact,
        "reaction_type": reactionType,
        "total_react": totalReact,
        "total_react_type": List<dynamic>.from(totalReactType!.map((x) => x.toJson())),
        "comment_count": commentCount,
        "attachments": List<dynamic>.from(attachments!.map((x) => x.toJson())),
        "youtubes": List<dynamic>.from(youtubes!.map((x) => x)),
        "created_date": createdDate,
        "updated_date": updatedDate,
    };
}

class Attachment {
    Attachment({
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileShow,
        this.fileThumbnail,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
    });

    int? id;
    String? fileName;
    String? fileSize;
    String? fileType;
    String? fileShow;
    String? fileThumbnail;
    String? fileIndex;
    String? fileArea;
    String? objectId;
    String? schoolUrl;
    String? postDate;
    int? isGdrive;
    String? fileGdriveId;
    int? isS3;

    factory Attachment.fromJson(Map<String, dynamic> json) => Attachment(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
    };
}

class DataComment {
    DataComment({
        this.id,
        this.parent,
        this.postText,
        this.user,
        this.comments,
        this.isLike,
        this.likeCount,
        this.isReact,
        this.reactionType,
        this.totalReact,
        this.totalReactType,
        this.commentCount,
        this.attachments,
        this.youtubes,
        this.createdDate,
        this.updatedDate,
    });

    int? id;
    int? parent;
    String? postText;
    FluffyUser? user;
    List<CommentComment>? comments;
    int? isLike;
    int? likeCount;
    int? isReact;
    dynamic reactionType;
    int? totalReact;
    List<dynamic>? totalReactType;
    int? commentCount;
    List<dynamic>? attachments;
    List<dynamic>? youtubes;
    String? createdDate;
    String? updatedDate;

    factory DataComment.fromJson(Map<String, dynamic> json) => DataComment(
        id: json["id"],
        parent: json["parent"],
        postText: json["post_text"],
        user: FluffyUser.fromJson(json["user"]),
        comments: List<CommentComment>.from(json["comments"].map((x) => CommentComment.fromJson(x))),
        isLike: json["is_like"],
        likeCount: json["like_count"],
        isReact: json["is_react"],
        reactionType: json["reaction_type"],
        totalReact: json["total_react"],
        totalReactType: List<dynamic>.from(json["total_react_type"].map((x) => x)),
        commentCount: json["comment_count"],
        attachments: List<dynamic>.from(json["attachments"].map((x) => x)),
        youtubes: List<dynamic>.from(json["youtubes"].map((x) => x)),
        createdDate: json["created_date"],
        updatedDate: json["updated_date"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "parent": parent,
        "post_text": postText,
        "user": user!.toJson(),
        "comments": List<dynamic>.from(comments!.map((x) => x.toJson())),
        "is_like": isLike,
        "like_count": likeCount,
        "is_react": isReact,
        "reaction_type": reactionType,
        "total_react": totalReact,
        "total_react_type": List<dynamic>.from(totalReactType!.map((x) => x)),
        "comment_count": commentCount,
        "attachments": List<dynamic>.from(attachments!.map((x) => x)),
        "youtubes": List<dynamic>.from(youtubes!.map((x) => x)),
        "created_date": createdDate,
        "updated_date": updatedDate,
    };
}

class CommentComment {
    CommentComment({
        this.id,
        this.parent,
        this.postText,
        this.user,
        this.comments,
        this.isLike,
        this.likeCount,
        this.isReact,
        this.reactionType,
        this.totalReact,
        this.totalReactType,
        this.commentCount,
        this.attachments,
        this.youtubes,
        this.createdDate,
        this.updatedDate,
    });

    int? id;
    int? parent;
    String? postText;
    PurpleUser? user;
    List<dynamic>? comments;
    int? isLike;
    int? likeCount;
    int? isReact;
    dynamic reactionType;
    int? totalReact;
    List<dynamic>? totalReactType;
    int? commentCount;
    List<dynamic>? attachments;
    List<dynamic>? youtubes;
    String? createdDate;
    String? updatedDate;

    factory CommentComment.fromJson(Map<String, dynamic> json) => CommentComment(
        id: json["id"],
        parent: json["parent"],
        postText: json["post_text"],
        user: PurpleUser.fromJson(json["user"]),
        comments: List<dynamic>.from(json["comments"].map((x) => x)),
        isLike: json["is_like"],
        likeCount: json["like_count"],
        isReact: json["is_react"],
        reactionType: json["reaction_type"],
        totalReact: json["total_react"],
        totalReactType: List<dynamic>.from(json["total_react_type"].map((x) => x)),
        commentCount: json["comment_count"],
        attachments: List<dynamic>.from(json["attachments"].map((x) => x)),
        youtubes: List<dynamic>.from(json["youtubes"].map((x) => x)),
        createdDate: json["created_date"],
        updatedDate: json["updated_date"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "parent": parent,
        "post_text": postText,
        "user": user!.toJson(),
        "comments": List<dynamic>.from(comments!.map((x) => x)),
        "is_like": isLike,
        "like_count": likeCount,
        "is_react": isReact,
        "reaction_type": reactionType,
        "total_react": totalReact,
        "total_react_type": List<dynamic>.from(totalReactType!.map((x) => x)),
        "comment_count": commentCount,
        "attachments": List<dynamic>.from(attachments!.map((x) => x)),
        "youtubes": List<dynamic>.from(youtubes!.map((x) => x)),
        "created_date": createdDate,
        "updated_date": updatedDate,
    };
}

class PurpleUser {
    PurpleUser({
        this.id,
        this.schoolCode,
        this.code,
        this.name,
        this.nameEn,
        this.firstname,
        this.lastname,
        this.firstnameEn,
        this.lastnameEn,
        this.gender,
        this.genderName,
        this.dob,
        this.phone,
        this.email,
        this.address,
        this.role,
        this.profileMedia,
        this.qrUrl,
    });

    String? id;
    String? schoolCode;
    String? code;
    String? name;
    String? nameEn;
    String? firstname;
    String? lastname;
    String? firstnameEn;
    String? lastnameEn;
    int? gender;
    String? genderName;
    String? dob;
    String? phone;
    String? email;
    String? address;
    dynamic role;
    Attachment? profileMedia;
    String? qrUrl;

    factory PurpleUser.fromJson(Map<String, dynamic> json) => PurpleUser(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        email: json["email"],
        address: json["address"],
        role: json["role"],
        profileMedia: Attachment.fromJson(json["profileMedia"]),
        qrUrl: json["qr_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "email": email,
        "address": address,
        "role": role,
        "profileMedia": profileMedia!.toJson(),
        "qr_url": qrUrl,
    };
}

class FluffyUser {
    FluffyUser({
        this.id,
        this.schoolCode,
        this.code,
        this.name,
        this.nameEn,
        this.firstname,
        this.lastname,
        this.firstnameEn,
        this.lastnameEn,
        this.gender,
        this.genderName,
        this.dob,
        this.phone,
        this.email,
        this.address,
        this.role,
        this.profileMedia,
        this.qrUrl,
    });

    String? id;
    String? schoolCode;
    String? code;
    String? name;
    String? nameEn;
    String? firstname;
    String? lastname;
    String? firstnameEn;
    String? lastnameEn;
    int? gender;
    String? genderName;
    String? dob;
    String? phone;
    String? email;
    String? address;
    dynamic role;
    ProfileMedia? profileMedia;
    String? qrUrl;

    factory FluffyUser.fromJson(Map<String, dynamic> json) => FluffyUser(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        email: json["email"],
        address: json["address"],
        role: json["role"],
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
        qrUrl: json["qr_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "email": email,
        "address": address,
        "role": role,
        "profileMedia": profileMedia!.toJson(),
        "qr_url": qrUrl,
    };
}

class ProfileMedia {
    ProfileMedia({
        this.fileShow,
        this.fileThumbnail,
    });

    String? fileShow;
    String? fileThumbnail;

    factory ProfileMedia.fromJson(Map<String, dynamic> json) => ProfileMedia(
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
    );

    Map<String, dynamic> toJson() => {
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
    };
}

class TotalReactType {
    TotalReactType({
        this.reactionType,
        this.total,
    });

    int? reactionType;
    int? total;

    factory TotalReactType.fromJson(Map<String, dynamic> json) => TotalReactType(
        reactionType: json["reaction_type"],
        total: json["total"],
    );

    Map<String, dynamic> toJson() => {
        "reaction_type": reactionType,
        "total": total,
    };
}

class DataUser {
    DataUser({
        this.id,
        this.schoolCode,
        this.code,
        this.name,
        this.nameEn,
        this.firstname,
        this.lastname,
        this.firstnameEn,
        this.lastnameEn,
        this.gender,
        this.genderName,
        this.dob,
        this.phone,
        this.email,
        this.address,
        this.framework,
        this.prokas,
        this.role,
        this.profileMedia,
    });

    String? id;
    String? schoolCode;
    String? code;
    String? name;
    String? nameEn;
    String? firstname;
    String? lastname;
    String? firstnameEn;
    String? lastnameEn;
    int? gender;
    String? genderName;
    String? dob;
    String? phone;
    String? email;
    String? address;
    String? framework;
    String? prokas;
    dynamic role;
    Attachment? profileMedia;

    factory DataUser.fromJson(Map<String, dynamic> json) => DataUser(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        email: json["email"],
        address: json["address"],
        framework: json["framework"],
        prokas: json["prokas"],
        role: json["role"],
        profileMedia: Attachment.fromJson(json["profileMedia"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "email": email,
        "address": address,
        "framework": framework,
        "prokas": prokas,
        "role": role,
        "profileMedia": profileMedia!.toJson(),
    };
}
