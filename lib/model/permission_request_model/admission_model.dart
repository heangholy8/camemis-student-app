class GetAdmissionModel {
    GetAdmissionModel({
        this.id,
        this.parentId,
        this.name,
        this.nameEn,
        this.sortkey,
        this.objectType,
        this.description,
        this.children,
    });

    int? id;
    int? parentId;
    String? name;
    String? nameEn;
    int? sortkey;
    String? objectType;
    String? description;
    List<dynamic>? children;

    factory GetAdmissionModel.fromJson(Map<String, dynamic> json) => GetAdmissionModel(
        id: json["id"],
        parentId: json["parent_id"],
        name: json["name"],
        nameEn: json["name_en"],
        sortkey: json["sortkey"],
        objectType: json["object_type"],
        description: json["description"],
        children: List<dynamic>.from(json["children"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "parent_id": parentId,
        "name": name,
        "name_en": nameEn,
        "sortkey": sortkey,
        "object_type": objectType,
        "description": description,
        "children": List<dynamic>.from(children!.map((x) => x)),
    };
}
