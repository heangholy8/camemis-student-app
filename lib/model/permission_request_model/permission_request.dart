class GetPermissionRequestModel {
    GetPermissionRequestModel({
        this.id,
        this.startDate,
        this.endDate,
        this.requestTypes,
        this.classId,
        this.schoolyearId,
        this.description,
        this.admissionTypes,
        this.isApprove,
        this.approveComment,
        this.approveBy,
        this.approveDate,
        this.schedule,
        this.reason,
        this.file,
        this.userRequest,
        this.student,
    });

    int? id;
    String? startDate;
    String? endDate;
    int? requestTypes;
    int? classId;
    String? schoolyearId;
    String? description;
    int? admissionTypes;
    int? isApprove;
    String? approveComment;
    dynamic approveBy;
    String? approveDate;
    List<Schedule>? schedule;
    Reason? reason;
    List<FileElement>? file;
    Student? userRequest;
    Student? student;

    factory GetPermissionRequestModel.fromJson(Map<String, dynamic> json) => GetPermissionRequestModel(
        id: json["id"],
        startDate: json["start_date"],
        endDate: json["end_date"],
        requestTypes: json["request_types"],
        classId: json["class_id"],
        schoolyearId: json["schoolyear_id"],
        description: json["description"],
        admissionTypes: json["admission_types"],
        isApprove: json["is_approve"],
        approveComment: json["approve_comment"],
        approveBy: json["approve_by"],
        approveDate: json["approve_date"],
        schedule: List<Schedule>.from(json["schedule"].map((x) => Schedule.fromJson(x))),
        reason: Reason.fromJson(json["reason"]),
        file: List<FileElement>.from(json["file"].map((x) => FileElement.fromJson(x))),
        userRequest: Student.fromJson(json["user_request"]),
        student: Student.fromJson(json["student"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "start_date": startDate,
        "end_date": endDate,
        "request_types": requestTypes,
        "class_id": classId,
        "schoolyear_id": schoolyearId,
        "description": description,
        "admission_types": admissionTypes,
        "is_approve": isApprove,
        "approve_comment": approveComment,
        "approve_by": approveBy,
        "approve_date": approveDate,
        "schedule": List<dynamic>.from(schedule!.map((x) => x.toJson())),
        "reason": reason!.toJson(),
        "file": List<dynamic>.from(file!.map((x) => x.toJson())),
        "user_request": userRequest!.toJson(),
        "student": student!.toJson(),
    };
}

class FileElement {
    FileElement({
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileShow,
        this.fileThumbnail,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
    });

    int? id;
    String? fileName;
    String? fileSize;
    String? fileType;
    String? fileShow;
    String? fileThumbnail;
    String? fileIndex;
    String? fileArea;
    String? objectId;
    String? schoolUrl;
    String? postDate;
    int? isGdrive;
    String? fileGdriveId;
    int? isS3;

    factory FileElement.fromJson(Map<String, dynamic> json) => FileElement(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
    };
}

class Reason {
    Reason({
        this.id,
        this.name,
        this.nameEn,
    });

    int? id;
    String? name;
    String? nameEn;

    factory Reason.fromJson(Map<String, dynamic> json) => Reason(
        id: json["id"],
        name: json["name"],
        nameEn: json["name_en"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "name_en": nameEn,
    };
}

class Schedule {
    Schedule({
        this.id,
        this.scheduleType,
        this.code,
        this.event,
        this.eventTitle,
        this.sessionPeriod,
        this.startTime,
        this.endTime,
        this.shortDay,
    });

    int? id;
    int? scheduleType;
    String? code;
    String? event;
    String? eventTitle;
    int? sessionPeriod;
    String? startTime;
    String? endTime;
    String? shortDay;

    factory Schedule.fromJson(Map<String, dynamic> json) => Schedule(
        id: json["id"],
        scheduleType: json["schedule_type"],
        code: json["code"],
        event: json["event"],
        eventTitle: json["event_title"],
        sessionPeriod: json["session_period"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        shortDay: json["short_day"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "schedule_type": scheduleType,
        "code": code,
        "event": event,
        "event_title": eventTitle,
        "session_period": sessionPeriod,
        "start_time": startTime,
        "end_time": endTime,
        "short_day": shortDay,
    };
}

class Student {
    Student({
        this.id,
        this.name,
        this.nameEn,
    });

    String? id;
    String? name;
    String? nameEn;

    factory Student.fromJson(Map<String, dynamic> json) => Student(
        id: json["id"],
        name: json["name"],
        nameEn: json["name_en"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "name_en": nameEn,
    };
}
