// To parse this JSON data, do
//
//     final listSchoolModel = listSchoolModelFromJson(jsonString);

import 'dart:convert';

ListSchoolModel listSchoolModelFromJson(String str) => ListSchoolModel.fromJson(json.decode(str));

String listSchoolModelToJson(ListSchoolModel data) => json.encode(data.toJson());

class ListSchoolModel {
    ListSchoolModel({
        this.data,
        this.message,
    });

    List<Datum>? data;
    String? message;

    factory ListSchoolModel.fromJson(Map<String, dynamic> json) => ListSchoolModel(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "message": message,
    };
}

class Datum {
    Datum({
        this.id,
        this.schoolName,
        this.schoolNameEn,
        this.isActive,
    });

    int? id;
    String? schoolName;
    dynamic schoolNameEn;
    int? isActive;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        schoolName: json["school_name"],
        schoolNameEn: json["school_name_en"],
        isActive: json["is_active"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "school_name": schoolName,
        "school_name_en": schoolNameEn,
        "is_active": isActive,
    };
}
