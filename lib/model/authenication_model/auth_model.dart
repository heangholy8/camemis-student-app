// To parse this JSON data, do
//
//     final authModel = authModelFromJson(jsonString);

import 'dart:convert';

AuthModel authModelFromJson(String str) => AuthModel.fromJson(json.decode(str));

String authModelToJson(AuthModel data) => json.encode(data.toJson());

class AuthModel {
    AuthModel({
        this.accessToken,
        this.tokenType,
        this.expiresIn,
        this.expiresInDate,
        this.authUser,
        this.schoolCode,
        this.schoolGuid,
        this.schoolName,
        this.accessUrl,
    });

    String? accessToken;
    String? tokenType;
    int? expiresIn;
    String? expiresInDate;
    AuthUser? authUser;
    int? schoolCode;
    String? schoolGuid;
    String? schoolName;
    String? accessUrl;

    factory AuthModel.fromJson(Map<String, dynamic> json) => AuthModel(
        accessToken: json["access_token"],
        tokenType: json["token_type"],
        expiresIn: json["expires_in"],
        expiresInDate: json["expires_in_date"],
        authUser: AuthUser.fromJson(json["auth_user"]),
        schoolCode: json["school_code"],
        schoolGuid: json["school_guid"],
        schoolName: json["school_name"],
        accessUrl: json["access_url"],
    );

    Map<String, dynamic> toJson() => {
        "access_token": accessToken,
        "token_type": tokenType,
        "expires_in": expiresIn,
        "expires_in_date": expiresInDate,
        "auth_user": authUser!.toJson(),
        "school_code": schoolCode,
        "school_guid": schoolGuid,
        "school_name": schoolName,
        "access_url": accessUrl,
    };
}

class AuthUser {
    AuthUser({
        this.id,
        this.schoolCode,
        this.code,
        this.name,
        this.nameEn,
        this.firstname,
        this.lastname,
        this.firstnameEn,
        this.lastnameEn,
        this.gender,
        this.genderName,
        this.dob,
        this.birthPlace,
        this.phone,
        this.email,
        this.address,
        this.role,
        this.fatherName,
        this.fatherLastname,
        this.fatherFirstname,
        this.fatherLastnameLatin,
        this.fatherFirstnameLatin,
        this.fatherPhone,
        this.fatherJob,
        this.motherName,
        this.motherLastname,
        this.motherFirstname,
        this.motherLastnameLatin,
        this.motherFirstnameLatin,
        this.motherPhone,
        this.guardianName,
        this.guardianPhone,
        this.motherJob,
        this.profileMedia,
        this.guardian,
        this.currentClass,
    });

    String? id;
    String? schoolCode;
    String? code;
    String? name;
    String? nameEn;
    String? firstname;
    String? lastname;
    String? firstnameEn;
    String? lastnameEn;
    int? gender;
    String? genderName;
    String? dob;
    String? birthPlace;
    String? phone;
    String? email;
    String? address;
    int? role;
    String? fatherName;
    String? fatherLastname;
    String? fatherFirstname;
    String? fatherLastnameLatin;
    String? fatherFirstnameLatin;
    String? fatherPhone;
    String? fatherJob;
    String? motherName;
    String? motherLastname;
    String? motherFirstname;
    String? motherLastnameLatin;
    String? motherFirstnameLatin;
    String? motherPhone;
    String? guardianName;
    String? guardianPhone;
    String? motherJob;
    ProfileMedia? profileMedia;
    Guardian? guardian;
    CurrentClass? currentClass;

    factory AuthUser.fromJson(Map<String, dynamic> json) => AuthUser(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        birthPlace: json["birth_place"],
        phone: json["phone"],
        email: json["email"],
        address: json["address"],
        role: json["role"],
        fatherName: json["father_name"],
        fatherLastname: json["father_lastname"],
        fatherFirstname: json["father_firstname"],
        fatherLastnameLatin: json["father_lastname_latin"],
        fatherFirstnameLatin: json["father_firstname_latin"],
        fatherPhone: json["father_phone"],
        fatherJob: json["father_job"],
        motherName: json["mother_name"],
        motherLastname: json["mother_lastname"],
        motherFirstname: json["mother_firstname"],
        motherLastnameLatin: json["mother_lastname_latin"],
        motherFirstnameLatin: json["mother_firstname_latin"],
        motherPhone: json["mother_phone"],
        guardianName: json["guardian_name"],
        guardianPhone: json["guardian_phone"],
        motherJob: json["mother_job"],
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
        guardian: Guardian.fromJson(json["guardian"]),
        currentClass: CurrentClass.fromJson(json["current_class"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "birth_place": birthPlace,
        "phone": phone,
        "email": email,
        "address": address,
        "role": role,
        "father_name": fatherName,
        "father_lastname": fatherLastname,
        "father_firstname": fatherFirstname,
        "father_lastname_latin": fatherLastnameLatin,
        "father_firstname_latin": fatherFirstnameLatin,
        "father_phone": fatherPhone,
        "father_job": fatherJob,
        "mother_name": motherName,
        "mother_lastname": motherLastname,
        "mother_firstname": motherFirstname,
        "mother_lastname_latin": motherLastnameLatin,
        "mother_firstname_latin": motherFirstnameLatin,
        "mother_phone": motherPhone,
        "guardian_name": guardianName,
        "guardian_phone": guardianPhone,
        "mother_job": motherJob,
        "profileMedia": profileMedia!.toJson(),
        "guardian": guardian!.toJson(),
        "current_class": currentClass!.toJson(),
    };
}

class CurrentClass {
    CurrentClass({
        this.classId,
        this.schoolyearId,
        this.className,
        this.classNameEn,
        this.sortkey,
        this.level,
        this.short,
        this.schoolyearStatus,
        this.schoolyearName,
        this.leadership,
        this.listMonths,
        this.listSubjects,
    });

    int? classId;
    String? schoolyearId;
    String? className;
    String? classNameEn;
    int? sortkey;
    int? level;
    String? short;
    int? schoolyearStatus;
    String? schoolyearName;
    int? leadership;
    List<ListMonth>? listMonths;
    List<ListSubject>? listSubjects;

    factory CurrentClass.fromJson(Map<String, dynamic> json) => CurrentClass(
        classId: json["class_id"],
        schoolyearId: json["schoolyear_id"],
        className: json["class_name"],
        classNameEn: json["class_name_en"],
        sortkey: json["sortkey"],
        level: json["level"],
        short: json["short"],
        schoolyearStatus: json["schoolyear_status"],
        schoolyearName: json["schoolyear_name"],
        leadership: json["leadership"],
        listMonths: List<ListMonth>.from(json["list_months"].map((x) => ListMonth.fromJson(x))),
        listSubjects: List<ListSubject>.from(json["list_subjects"].map((x) => ListSubject.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "class_id": classId,
        "schoolyear_id": schoolyearId,
        "class_name": className,
        "class_name_en": classNameEn,
        "sortkey": sortkey,
        "level": level,
        "short": short,
        "schoolyear_status": schoolyearStatus,
        "schoolyear_name": schoolyearName,
        "leadership": leadership,
        "list_months": List<dynamic>.from(listMonths!.map((x) => x.toJson())),
        "list_subjects": List<dynamic>.from(listSubjects!.map((x) => x.toJson())),
    };
}

class ListMonth {
    ListMonth({
        this.semester,
        this.name,
        this.months,
        this.startDate,
        this.endDate,
        this.nameEn,
    });

    String? semester;
    String? name;
    List<Month>? months;
    String? startDate;
    String? endDate;
    String? nameEn;

    factory ListMonth.fromJson(Map<String, dynamic> json) => ListMonth(
        semester: json["semester"],
        name: json["name"],
        months: List<Month>.from(json["months"].map((x) => Month.fromJson(x))),
        startDate: json["startDate"],
        endDate: json["endDate"],
        nameEn: json["name_en"],
    );

    Map<String, dynamic> toJson() => {
        "semester": semester,
        "name": name,
        "months": List<dynamic>.from(months!.map((x) => x.toJson())),
        "startDate": startDate,
        "endDate": endDate,
        "name_en": nameEn,
    };
}

class Month {
    Month({
        this.month,
        this.displayMonth,
        this.year,
        this.displayMonthEn,
        this.isCurrent,
    });

    String? month;
    String? displayMonth;
    String? year;
    String? displayMonthEn;
    bool? isCurrent;

    factory Month.fromJson(Map<String, dynamic> json) => Month(
        month: json["month"],
        displayMonth: json["displayMonth"],
        year: json["year"],
        displayMonthEn: json["displayMonthEn"],
        isCurrent: json["isCurrent"],
    );

    Map<String, dynamic> toJson() => {
        "month": month,
        "displayMonth": displayMonth,
        "year": year,
        "displayMonthEn": displayMonthEn,
        "isCurrent": isCurrent,
    };
}

class ListSubject {
    ListSubject({
        this.id,
        this.gradeSubjectId,
        this.guid,
        this.short,
        this.name,
        this.nameEn,
        this.status,
        this.subjectType,
        this.description,
        this.coeffValue,
        this.numberSession,
        this.numberCredit,
        this.scoreMin,
        this.scoreMax,
        this.sortkey,
        this.level,
        this.internationalEdu,
        this.gradeAcademicType,
        this.complementaryScore,
        this.mapDefaultSubjectId,
    });

    int? id;
    int? gradeSubjectId;
    String? guid;
    String? short;
    String? name;
    String? nameEn;
    int? status;
    int? subjectType;
    dynamic description;
    dynamic coeffValue;
    int? numberSession;
    int? numberCredit;
    int? scoreMin;
    int? scoreMax;
    int? sortkey;
    int? level;
    int? internationalEdu;
    int? gradeAcademicType;
    int? complementaryScore;
    int? mapDefaultSubjectId;

    factory ListSubject.fromJson(Map<String, dynamic> json) => ListSubject(
        id: json["id"],
        gradeSubjectId: json["grade_subject_id"],
        guid: json["guid"],
        short: json["short"],
        name: json["name"],
        nameEn: json["name_en"],
        status: json["status"],
        subjectType: json["subject_type"],
        description: json["description"],
        coeffValue: json["coeff_value"].toDouble(),
        numberSession: json["number_session"],
        numberCredit: json["number_credit"],
        scoreMin: json["score_min"],
        scoreMax: json["score_max"],
        sortkey: json["sortkey"],
        level: json["level"],
        internationalEdu: json["international_edu"],
        gradeAcademicType: json["grade_academic_type"],
        complementaryScore: json["complementary_score"],
        mapDefaultSubjectId: json["map_default_subject_id"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "grade_subject_id": gradeSubjectId,
        "guid": guid,
        "short": short,
        "name": name,
        "name_en": nameEn,
        "status": status,
        "subject_type": subjectType,
        "description": description,
        "coeff_value": coeffValue,
        "number_session": numberSession,
        "number_credit": numberCredit,
        "score_min": scoreMin,
        "score_max": scoreMax,
        "sortkey": sortkey,
        "level": level,
        "international_edu": internationalEdu,
        "grade_academic_type": gradeAcademicType,
        "complementary_score": complementaryScore,
        "map_default_subject_id": mapDefaultSubjectId,
    };
}

class Guardian {
    Guardian({
        this.id,
        this.schoolCode,
        this.code,
        this.name,
        this.nameEn,
        this.firstname,
        this.lastname,
        this.firstnameEn,
        this.lastnameEn,
        this.gender,
        this.genderName,
        this.dob,
        this.phone,
        this.email,
        this.address,
        this.role,
        this.isVerify,
        this.profileMedia,
        this.qrUrl,
    });

    String? id;
    String? schoolCode;
    String? code;
    String? name;
    String? nameEn;
    String? firstname;
    String? lastname;
    String? firstnameEn;
    String? lastnameEn;
    int? gender;
    String? genderName;
    String? dob;
    String? phone;
    String? email;
    String? address;
    String? role;
    int? isVerify;
    ProfileMedia? profileMedia;
    String? qrUrl;

    factory Guardian.fromJson(Map<String, dynamic> json) => Guardian(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        email: json["email"],
        address: json["address"],
        role: json["role"],
        isVerify: json["is_verify"],
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
        qrUrl: json["qr_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "email": email,
        "address": address,
        "role": role,
        "is_verify": isVerify,
        "profileMedia": profileMedia!.toJson(),
        "qr_url": qrUrl,
    };
}

class ProfileMedia {
    ProfileMedia({
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileShow,
        this.fileThumbnail,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
    });

    int? id;
    String? fileName;
    String? fileSize;
    String? fileType;
    String? fileShow;
    String? fileThumbnail;
    String? fileIndex;
    String? fileArea;
    String? objectId;
    String? schoolUrl;
    String? postDate;
    int? isGdrive;
    String? fileGdriveId;
    int? isS3;

    factory ProfileMedia.fromJson(Map<String, dynamic> json) => ProfileMedia(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
    };
}
