class LessonModel {
    LessonModel({
        this.id,
        this.name,
        this.startDate,
        this.endDate,
        this.startTime,
        this.endTime,
        this.description,
        this.type,
        this.category,
        this.subjectId,
        this.classId,
        this.createdDate,
        this.attachments,
        this.forums,
    });

    int? id;
    String? name;
    String? startDate;
    String? endDate;
    String? startTime;
    String? endTime;
    String? description;
    int? type;
    String ?category;
    int? subjectId;
    int? classId;
    String? createdDate;
    List<Attachment>? attachments;
    List<Forum>? forums;

    factory LessonModel.fromJson(Map<String, dynamic> json) => LessonModel(
        id: json["id"],
        name: json["name"],
        startDate: json["start_date"],
        endDate: json["end_date"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        description: json["description"],
        type: json["type"],
        category: json["category"],
        subjectId: json["subject_id"],
        classId: json["class_id"],
        createdDate: json["created_date"],
        attachments: List<Attachment>.from(json["attachments"].map((x) => Attachment.fromJson(x))),
        forums: List<Forum>.from(json["forums"].map((x) => Forum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "start_date": startDate,
        "end_date": endDate,
        "start_time": startTime,
        "end_time": endTime,
        "description": description,
        "type": type,
        "category": category,
        "subject_id": subjectId,
        "class_id": classId,
        "created_date": createdDate,
        "attachments": List<dynamic>.from(attachments!.map((x) => x.toJson())),
        "forums": List<dynamic>.from(forums!.map((x) => x.toJson())),
    };
}

class Attachment {
    Attachment({
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileShow,
        this.fileThumbnail,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
    });

    int? id;
    String? fileName;
    String? fileSize;
    String? fileType;
    String? fileShow;
    String? fileThumbnail;
    String? fileIndex;
    String? fileArea;
    String? objectId;
    String? schoolUrl;
    String? postDate;
    int ?isGdrive;
    String? fileGdriveId;
    int? isS3;

    factory Attachment.fromJson(Map<String, dynamic> json) => Attachment(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
    };
}

class Forum {
    Forum({
        this.id,
        this.parent,
        this.lessonAssignmentId,
        this.comment,
        this.createdDate,
        this.roleId,
        this.userProfile,
        this.children,
    });

    int? id;
    int? parent;
    int? lessonAssignmentId;
    String? comment;
    String ?createdDate;
    int? roleId;
    UserProfile ?userProfile;
    List<Forum> ?children;

    factory Forum.fromJson(Map<String, dynamic> json) => Forum(
        id: json["id"],
        parent: json["parent"],
        lessonAssignmentId: json["lesson_assignment_id"],
        comment: json["comment"],
        createdDate: json["created_date"],
        roleId: json["role_id"],
        userProfile: UserProfile.fromJson(json["userProfile"]),
        children: List<Forum>.from(json["children"].map((x) => Forum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "parent": parent,
        "lesson_assignment_id": lessonAssignmentId,
        "comment": comment,
        "created_date": createdDate,
        "role_id": roleId,
        "userProfile": userProfile?.toJson(),
        "children": List<dynamic>.from(children!.map((x) => x.toJson())),
    };
}

class UserProfile {
    UserProfile({
        this.id,
        this.schoolCode,
        this.code,
        this.name,
        this.nameEn,
        this.firstname,
        this.lastname,
        this.firstnameEn,
        this.lastnameEn,
        this.gender,
        this.genderName,
        this.dob,
        this.phone,
        this.email,
        this.address,
        this.role,
        this.profileMedia,
        this.qrUrl,
    });

    String? id;
    String ?schoolCode;
    String ?code;
    String ?name;
    String ?nameEn;
    String? firstname;
    String? lastname;
    String ?firstnameEn;
    String ?lastnameEn;
    int? gender;
    String? genderName;
    String? dob;
    String? phone;
    String? email;
    String? address;
    String? role;
    Attachment? profileMedia;
    String? qrUrl;

    factory UserProfile.fromJson(Map<String, dynamic> json) => UserProfile(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        email: json["email"],
        address: json["address"],
        role: json["role"],
        profileMedia: Attachment.fromJson(json["profileMedia"]),
        qrUrl: json["qr_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "email": email,
        "address": address,
        "role": role,
        "profileMedia": profileMedia?.toJson(),
        "qr_url": qrUrl,
    };
}
