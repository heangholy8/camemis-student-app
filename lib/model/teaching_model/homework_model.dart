import 'package:camis_application_flutter/model/get_children_model/get_children_model.dart';

class HomeWorkModel {
  HomeWorkModel({
    this.id,
    this.name,
    this.startDate,
    this.endDate,
    this.startTime,
    this.endTime,
    this.description,
    this.type,
    this.category,
    this.subjectId,
    this.classId,
    this.createdDate,
    this.attachments,
    this.forums,
    this.countSubmitedHomework,
    this.submitHomeworks,
  });

  int? id;
  String? name;
  String? startDate;
  String? endDate;
  String? startTime;
  String? endTime;
  String? description;
  int? type;
  String? category;
  int? subjectId;
  int? classId;
  String? createdDate;
  List<Attachment>? attachments;
  List<Forum>? forums;
  int? countSubmitedHomework;
  List<SubmitHomework>? submitHomeworks;

  factory HomeWorkModel.fromJson(Map<String, dynamic> json) => HomeWorkModel(
        id: json["id"],
        name: json["name"],
        startDate: json["start_date"],
        endDate: json["end_date"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        description: json["description"],
        type: json["type"],
        category: json["category"],
        subjectId: json["subject_id"],
        classId: json["class_id"],
        createdDate: json["created_date"],
        attachments: List<Attachment>.from(
            json["attachments"].map((x) => Attachment.fromJson(x))),
        forums: List<Forum>.from(json["forums"].map((x) => Forum.fromJson(x))),
        countSubmitedHomework: json["count_submited_homework"],
        submitHomeworks: List<SubmitHomework>.from(
            json["submit_homeworks"].map((x) => SubmitHomework.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "start_date": startDate,
        "end_date": endDate,
        "start_time": startTime,
        "end_time": endTime,
        "description": description,
        "type": type,
        "category": category,
        "subject_id": subjectId,
        "class_id": classId,
        "created_date": createdDate,
        "attachments": List<dynamic>.from(attachments!.map((x) => x.toJson())),
        "forums": List<dynamic>.from(forums!.map((x) => x.toJson())),
        "count_submited_homework": countSubmitedHomework,
        "submit_homeworks":
            List<dynamic>.from(submitHomeworks!.map((x) => x.toJson())),
      };
}

class Attachment {
  Attachment({
    this.id,
    this.fileName,
    this.fileSize,
    this.fileType,
    this.fileShow,
    this.fileThumbnail,
    this.fileIndex,
    this.fileArea,
    this.objectId,
    this.schoolUrl,
    this.postDate,
    this.isGdrive,
    this.fileGdriveId,
    this.isS3,
  });

  int? id;
  String? fileName;
  String? fileSize;
  String? fileType;
  String? fileShow;
  String? fileThumbnail;
  String? fileIndex;
  String? fileArea;
  String? objectId;
  String? schoolUrl;
  String? postDate;
  int? isGdrive;
  String? fileGdriveId;
  int? isS3;

  factory Attachment.fromJson(Map<String, dynamic> json) => Attachment(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
      };
}

class Forum {
  Forum({
    this.id,
    this.parent,
    this.lessonAssignmentId,
    this.comment,
    this.createdDate,
    this.roleId,
    this.userProfile,
    this.children,
  });

  int? id;
  int? parent;
  int? lessonAssignmentId;
  String? comment;
  String? createdDate;
  int? roleId;
  ForumUserProfile? userProfile;
  List<Child>? children;

  factory Forum.fromJson(Map<String, dynamic> json) => Forum(
        id: json["id"],
        parent: json["parent"],
        lessonAssignmentId: json["lesson_assignment_id"],
        comment: json["comment"],
        createdDate: json["created_date"],
        roleId: json["role_id"],
        userProfile: ForumUserProfile.fromJson(json["userProfile"]),
        children:
            List<Child>.from(json["children"].map((x) => Child.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "parent": parent,
        "lesson_assignment_id": lessonAssignmentId,
        "comment": comment,
        "created_date": createdDate,
        "role_id": roleId,
        "userProfile": userProfile!.toJson(),
        "children": List<dynamic>.from(children!.map((x) => x.toJson())),
      };
}

class Child {
  Child({
    this.id,
    this.parent,
    this.lessonAssignmentId,
    this.comment,
    this.createdDate,
    this.roleId,
    this.userProfile,
    this.children,
  });

  int? id;
  int? parent;
  int? lessonAssignmentId;
  String? comment;
  String? createdDate;
  int? roleId;
  ChildUserProfile? userProfile;
  List<Child>? children;

  factory Child.fromJson(Map<String, dynamic> json) => Child(
        id: json["id"],
        parent: json["parent"],
        lessonAssignmentId: json["lesson_assignment_id"],
        comment: json["comment"],
        createdDate: json["created_date"],
        roleId: json["role_id"],
        userProfile: ChildUserProfile.fromJson(json["userProfile"]),
        children:
            List<Child>.from(json["children"].map((x) => Child.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "parent": parent,
        "lesson_assignment_id": lessonAssignmentId,
        "comment": comment,
        "created_date": createdDate,
        "role_id": roleId,
        "userProfile": userProfile?.toJson(),
        "children": List<dynamic>.from(children!.map((x) => x)),
      };
}

class ChildUserProfile {
  ChildUserProfile({
    this.id,
    this.schoolCode,
    this.code,
    this.name,
    this.nameEn,
    this.firstname,
    this.lastname,
    this.firstnameEn,
    this.lastnameEn,
    this.gender,
    this.genderName,
    this.dob,
    this.phone,
    this.email,
    this.address,
    this.role,
    this.profileMedia,
    this.qrUrl,
  });

  String? id;
  String? schoolCode;
  String? code;
  String? name;
  String? nameEn;
  String? firstname;
  String? lastname;
  String? firstnameEn;
  String? lastnameEn;
  int? gender;
  String? genderName;
  String? dob;
  String? phone;
  String? email;
  String? address;
  String? role;
  Attachment? profileMedia;
  String? qrUrl;

  factory ChildUserProfile.fromJson(Map<String, dynamic> json) =>
      ChildUserProfile(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        email: json["email"],
        address: json["address"],
        role: json["role"],
        profileMedia: Attachment.fromJson(json["profileMedia"]),
        qrUrl: json["qr_url"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "email": email,
        "address": address,
        "role": role,
        "profileMedia": profileMedia!.toJson(),
        "qr_url": qrUrl,
      };
}

class ForumUserProfile {
  ForumUserProfile({
    this.id,
    this.schoolCode,
    this.code,
    this.name,
    this.nameEn,
    this.firstname,
    this.lastname,
    this.firstnameEn,
    this.lastnameEn,
    this.gender,
    this.genderName,
    this.dob,
    this.phone,
    this.email,
    this.address,
    this.role,
    this.profileMedia,
    this.qrUrl,
  });

  String? id;
  String? schoolCode;
  String? code;
  String? name;
  String? nameEn;
  String? firstname;
  String? lastname;
  String? firstnameEn;
  String? lastnameEn;
  int? gender;
  String? genderName;
  String? dob;
  String? phone;
  String? email;
  String? address;
  dynamic role;
  Attachment? profileMedia;
  String? qrUrl;

  factory ForumUserProfile.fromJson(Map<String, dynamic> json) =>
      ForumUserProfile(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        email: json["email"],
        address: json["address"],
        role: json["role"],
        profileMedia: Attachment.fromJson(json["profileMedia"]),
        qrUrl: json["qr_url"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "email": email,
        "address": address,
        "role": role,
        "profileMedia": profileMedia?.toJson(),
        "qr_url": qrUrl,
      };
}

class SubmitHomework {
  SubmitHomework({
    this.id,
    this.classId,
    this.lessonAssignmentId,
    this.content,
    this.createdDate,
    this.student,
    this.teacherComment,
    this.teacherId,
    this.teacherCommentDate,
    this.medias,
    this.mediasTeacherCorrection,
  });

  int? id;
  int? classId;
  int? lessonAssignmentId;
  String? content;
  String? createdDate;
  Student? student;
  String? teacherComment;
  String? teacherId;
  String? teacherCommentDate;
  List<Attachment>? medias;
  List<Attachment>? mediasTeacherCorrection;

  factory SubmitHomework.fromJson(Map<String, dynamic> json) => SubmitHomework(
        id: json["id"],
        classId: json["class_id"],
        lessonAssignmentId: json["lesson_assignment_id"],
        content: json["content"],
        createdDate: json["created_date"],
        student: Student.fromJson(json["student"]),
        teacherComment: json["teacher_comment"],
        teacherId: json["teacher_id"],
        teacherCommentDate: json["teacher_comment_date"],
        medias: List<Attachment>.from(
            json["medias"].map((x) => Attachment.fromJson(x))),
        mediasTeacherCorrection: List<Attachment>.from(
            json["mediasTeacherCorrection"].map((x) => Attachment.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "class_id": classId,
        "lesson_assignment_id": lessonAssignmentId,
        "content": content,
        "created_date": createdDate,
        "student": student!.toJson(),
        "teacher_comment": teacherComment,
        "teacher_id": teacherId,
        "teacher_comment_date": teacherCommentDate,
        "medias": List<dynamic>.from(medias!.map((x) => x.toJson())),
        "mediasTeacherCorrection":
            List<dynamic>.from(mediasTeacherCorrection!.map((x) => x)),
      };
}

class Student {
  Student({
    this.id,
    this.schoolCode,
    this.code,
    this.name,
    this.nameEn,
    this.firstname,
    this.lastname,
    this.firstnameEn,
    this.lastnameEn,
    this.gender,
    this.genderName,
    this.dob,
    this.phone,
    this.email,
    this.address,
    this.role,
    this.profileMedia,
  });

  String? id;
  String? schoolCode;
  String? code;
  String? name;
  String? nameEn;
  String? firstname;
  String? lastname;
  String? firstnameEn;
  String? lastnameEn;
  int? gender;
  String? genderName;
  String? dob;
  String? phone;
  String? email;
  String? address;
  int? role;
  Attachment? profileMedia;

  factory Student.fromJson(Map<String, dynamic> json) => Student(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        email: json["email"],
        address: json["address"],
        role: json["role"],
        profileMedia: Attachment.fromJson(json["profileMedia"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "email": email,
        "address": address,
        "role": role,
        "profileMedia": profileMedia!.toJson(),
      };
}
