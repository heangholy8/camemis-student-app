// To parse this JSON data, do
//
//     final teachingModel = teachingModelFromJson(jsonString);

class TeachingAndLearningModel {
    TeachingAndLearningModel({
        this.ongoing,
        this.history,
    });

    List<History>? ongoing;
    List<History>? history;

    factory TeachingAndLearningModel.fromJson(Map<String, dynamic> json) => TeachingAndLearningModel(
        ongoing: List<History>.from(json["ongoing"].map((x) => History.fromJson(x))),
        history: List<History>.from(json["history"].map((x) => History.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "ongoing": List<dynamic>.from(ongoing!.map((x) => x.toJson())),
        "history": List<dynamic>.from(history!.map((x) => x.toJson())),
    };
}


class History {
    History({
        this.id,
        this.name,
        this.startDate,
        this.endDate,
        this.startTime,
        this.endTime,
        this.description,
        this.type,
        this.category,
        this.subjectId,
        this.classId,
        this.createdDate,
    });

    int? id;
    String? name;
    String? startDate;
    String? endDate;
    String? startTime;
    String? endTime;
    String? description;
    int? type;
    String? category;
    int? subjectId;
    int? classId;
    String? createdDate;

    factory History.fromJson(Map<String, dynamic> json) => History(
        id: json["id"],
        name: json["name"],
        startDate: json["start_date"],
        endDate: json["end_date"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        description: json["description"],
        type: json["type"],
        category: json["category"],
        subjectId: json["subject_id"],
        classId: json["class_id"],
        createdDate: json["created_date"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "start_date": startDate,
        "end_date": endDate,
        "start_time": startTime,
        "end_time": endTime,
        "description": description,
        "type": type,
        "category": category,
        "subject_id": subjectId,
        "class_id": classId,
        "created_date": createdDate,
    };
}
