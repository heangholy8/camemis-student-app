// To parse this JSON data, do
//
//     final phonnVerifyModel = phonnVerifyModelFromJson(jsonString);

import 'dart:convert';

PhonnVerifyModel? phonnVerifyModelFromJson(String str) => PhonnVerifyModel.fromJson(json.decode(str));

String phonnVerifyModelToJson(PhonnVerifyModel? data) => json.encode(data!.toJson());

class PhonnVerifyModel {
    PhonnVerifyModel({
        this.data,
        this.status,
    });

    Data? data;
    bool? status;

    factory PhonnVerifyModel.fromJson(Map<String, dynamic> json) => PhonnVerifyModel(
        data: Data.fromJson(json["data"]),
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "data": data!.toJson(),
        "status": status,
    };
}

class Data {
    Data({
        this.id,
        this.schoolCode,
        this.code,
        this.name,
        this.nameEn,
        this.firstname,
        this.lastname,
        this.firstnameEn,
        this.lastnameEn,
        this.gender,
        this.genderName,
        this.dob,
        this.phone,
        this.email,
        this.address,
        this.role,
        this.isVerify,
        this.profileMedia,
        this.qrUrl,
    });

    String? id;
    String? schoolCode;
    String? code;
    String? name;
    String? nameEn;
    String? firstname;
    String? lastname;
    String? firstnameEn;
    String? lastnameEn;
    int? gender;
    String? genderName;
    String? dob;
    String? phone;
    String? email;
    String? address;
    String? role;
    int? isVerify;
    ProfileMedia? profileMedia;
    String? qrUrl;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        email: json["email"],
        address: json["address"],
        role: json["role"],
        isVerify: json["is_verify"],
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
        qrUrl: json["qr_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "email": email,
        "address": address,
        "role": role,
        "is_verify": isVerify,
        "profileMedia": profileMedia!.toJson(),
        "qr_url": qrUrl,
    };
}

class ProfileMedia {
    ProfileMedia({
        this.fileShow,
        this.fileThumbnail,
    });

    String? fileShow;
    String? fileThumbnail;

    factory ProfileMedia.fromJson(Map<String, dynamic> json) => ProfileMedia(
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
    );

    Map<String, dynamic> toJson() => {
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
    };
}
