// ignore_for_file: public_member_api_docs, sort_constructors_first

class GuardinaConfirmAccountModel {
  String guardianName;
  String guardianRole;
  List<ChildrenConfirmModel> childrenConfirmModel;
  GuardinaConfirmAccountModel({
    required this.guardianName,
    required this.guardianRole,
    required this.childrenConfirmModel,
  });

  static List<GuardinaConfirmAccountModel> generate() {
    return [
      GuardinaConfirmAccountModel(
          guardianName: "លួន សុធារ៉ា",
          guardianRole: "ឪពុក",
          childrenConfirmModel: [
            ChildrenConfirmModel(
              name: "លួន តារារស្មី",
              indexChild: "១",
              gradeChild: "9 B2",
            ),
            ChildrenConfirmModel(
              name: "លួន រស្មី",
              indexChild: "២",
              gradeChild: "10 A1",
            ),
            ChildrenConfirmModel(
              name: "លួន តារារស្មី",
              indexChild: "៣",
              gradeChild: "9 B2",
            ),
            ChildrenConfirmModel(
              name: "លួន តារារស្មី",
              indexChild: "១",
              gradeChild: "9 B2",
            ),
            ChildrenConfirmModel(
              name: "លួន រស្មី",
              indexChild: "២",
              gradeChild: "10 A1",
            ),
            ChildrenConfirmModel(
              name: "លួន តារារស្មី",
              indexChild: "៣",
              gradeChild: "9 B2",
            ),
          ])
    ];
  }
}

class ChildrenConfirmModel {
  String name;
  String indexChild;
  String gradeChild;
  ChildrenConfirmModel({
    required this.name,
    required this.indexChild,
    required this.gradeChild,
  });
}
