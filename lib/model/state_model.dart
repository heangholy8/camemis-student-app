abstract class IState{
  bool handle();
  String message();
}

class ErrorState extends IState{
  @override
  bool handle() {
    return true;
  }

  @override
  String message() {
    throw UnimplementedError();
  }
}

class SuccessState extends IState{
  @override
  bool handle() {
    return true;
  }

  @override
  String message() {
    throw UnimplementedError();
  }

}

class StateContext{
  final IState? state;
  const StateContext({this.state});
}