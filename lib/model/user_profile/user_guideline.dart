// To parse this JSON data, do
//
//     final userGuidelineModel = userGuidelineModelFromJson(jsonString);

import 'dart:convert';

UserGuidelineModel userGuidelineModelFromJson(String str) => UserGuidelineModel.fromJson(json.decode(str));

String userGuidelineModelToJson(UserGuidelineModel data) => json.encode(data.toJson());

class UserGuidelineModel {
    UserGuidelineModel({
        this.data,
    });

    List<Datum>? data;

    factory UserGuidelineModel.fromJson(Map<String, dynamic> json) => UserGuidelineModel(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class Datum {
    Datum({
        this.id,
        this.feature,
        this.title,
        this.titleEn,
        this.description,
        this.descriptionEn,
        this.useFor,
        this.fileType,
        this.file,
        this.thumbnail,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    int? feature;
    String? title;
    String ?titleEn;
    dynamic description;
    dynamic descriptionEn;
    int? useFor;
    String ?fileType;
    String? file;
    dynamic thumbnail;
    DateTime? createdAt;
    DateTime? updatedAt;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        feature: json["feature"],
        title: json["title"],
        titleEn: json["title_en"],
        description: json["description"],
        descriptionEn: json["description_en"],
        useFor: json["use_for"],
        fileType: json["file_type"],
        file: json["file"],
        thumbnail: json["thumbnail"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "feature": feature,
        "title": title,
        "title_en": titleEn,
        "description": description,
        "description_en": descriptionEn,
        "use_for": useFor,
        "file_type": fileType,
        "file": file,
        "thumbnail": thumbnail,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
    };
}
