class BaseResponseModel<T> {
  BaseResponseModel({
    this.data,
    this.isSuccess = false,
    this.message,
  });

  T? data;
  final bool isSuccess;
  final String? message;

  factory BaseResponseModel.fromJson(
          Map<String, dynamic> json, T Function(dynamic) fromJsonT) =>
      BaseResponseModel(
        data: fromJsonT(json['data']),
        isSuccess: json['is_success'],
        message: json['message'],
      );
}
