// To parse this JSON data, do
//
//     final transactionPaymentModel = transactionPaymentModelFromJson(jsonString);

import 'dart:convert';

TransactionPaymentModel? transactionPaymentModelFromJson(String str) => TransactionPaymentModel.fromJson(json.decode(str));

String transactionPaymentModelToJson(TransactionPaymentModel? data) => json.encode(data!.toJson());

class TransactionPaymentModel {
    TransactionPaymentModel({
        this.data,
        this.status,
    });

    Data? data;
    bool? status;

    factory TransactionPaymentModel.fromJson(Map<String, dynamic> json) => TransactionPaymentModel(
        data:json["data"]==null?null:Data.fromJson(json["data"]),
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "data":data==null?null:data!.toJson(),
        "status": status,
    };
}

class Data {
    Data({
        this.id,
        this.guardianId,
        this.invoiceNumber,
        this.status,
        this.piadAt,
        this.amount,
        this.currency,
        this.choosePayOptionName,
        this.choosePayOptionNameEn,
        this.choosePayOption,
        this.paymentMethod,
        this.transactionId,
        this.expiredAt,
        this.expiredIn,
        this.createdAt,
        this.isSeen,
    });

    int? id;
    String? guardianId;
    String? invoiceNumber;
    String? status;
    dynamic piadAt;
    int? amount;
    String? currency;
    String? choosePayOptionName;
    String? choosePayOptionNameEn;
    int? choosePayOption;
    String? paymentMethod;
    String? transactionId;
    String? expiredAt;
    int? expiredIn;
    String? createdAt;
    dynamic isSeen;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        guardianId: json["guardian_id"],
        invoiceNumber: json["invoice_number"],
        status: json["status"],
        piadAt: json["piad_at"],
        amount: json["amount"],
        currency: json["currency"],
        choosePayOptionName: json["choose_pay_option_name"],
        choosePayOptionNameEn: json["choose_pay_option_name_en"],
        choosePayOption: json["choose_pay_option"],
        paymentMethod: json["payment_method"],
        transactionId: json["transaction_id"],
        expiredAt: json["expired_at"],
        expiredIn: json["expired_in"],
        createdAt: json["created_at"],
        isSeen: json["is_seen"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "guardian_id": guardianId,
        "invoice_number": invoiceNumber,
        "status": status,
        "piad_at": piadAt,
        "amount": amount,
        "currency": currency,
        "choose_pay_option_name": choosePayOptionName,
        "choose_pay_option_name_en": choosePayOptionNameEn,
        "choose_pay_option": choosePayOption,
        "payment_method": paymentMethod,
        "transaction_id": transactionId,
        "expired_at": expiredAt,
        "expired_in": expiredIn,
        "created_at": createdAt,
        "is_seen": isSeen,
    };
}
