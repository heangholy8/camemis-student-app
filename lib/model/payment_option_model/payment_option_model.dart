class PaymentOptionData {
  String? name;
  String? nameEn;
  int? amount;
  int? type;
  String? currency;
  bool? isSelected = false;
  int? amountOfMonth;

  PaymentOptionData(
      {this.name,
      this.amount,
      this.type,
      this.currency,
      this.isSelected,
      this.amountOfMonth,
      this.nameEn});

  PaymentOptionData.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    amount = json['amount'];
    type = json['type'];
    currency = json['currency'];
    amountOfMonth = json['amount_of_month'];
    nameEn = json['name_en'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['amount'] = this.amount;
    data['type'] = this.type;
    data['currency'] = this.currency;
    data['amount_of_month'] = this.amountOfMonth;
    data['name_en'] = this.nameEn;
    return data;
  }
}
