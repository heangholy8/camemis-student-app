// To parse this JSON data, do
//
//     final khqrModel = khqrModelFromJson(jsonString);

import 'dart:convert';

KhqrModel khqrModelFromJson(String str) => KhqrModel.fromJson(json.decode(str));

String khqrModelToJson(KhqrModel data) => json.encode(data.toJson());

class KhqrModel {
    KhqrModel({
        this.success,
        this.data,
    });

    bool? success;
    Data? data;

    factory KhqrModel.fromJson(Map<String, dynamic> json) => KhqrModel(
        success: json["success"],
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "success": success,
        "data": data!.toJson(),
    };
}

class Data {
    Data({
        this.transactionid,
        this.location,
    });
    int? transactionid;
    String? location;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        transactionid: json["transaction_id"],
        location: json["location"],
    );

    Map<String, dynamic> toJson() => {
        "transaction_id": transactionid,
        "location": location,
    };
}
