// To parse this JSON data, do
//
//     final abaDeepLinkModel = abaDeepLinkModelFromJson(jsonString);

import 'dart:convert';

AbaDeepLinkModel? abaDeepLinkModelFromJson(String str) => AbaDeepLinkModel.fromJson(json.decode(str));

String abaDeepLinkModelToJson(AbaDeepLinkModel? data) => json.encode(data!.toJson());

class AbaDeepLinkModel {
    AbaDeepLinkModel({
        this.success,
        this.data,
    });

    bool? success;
    Data? data;

    factory AbaDeepLinkModel.fromJson(Map<String, dynamic> json) => AbaDeepLinkModel(
        success: json["success"],
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "success": success,
        "data": data!.toJson(),
    };
}

class Data {
    Data({
        this.status,
        this.description,
        this.qrString,
        this.abapayDeeplink,
        this.appStore,
        this.playStore,
        this.transactionId,
    });

    Status? status;
    String? description;
    String? qrString;
    String? abapayDeeplink;
    String? appStore;
    String? playStore;
    int? transactionId;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        status: Status.fromJson(json["status"]),
        description: json["description"],
        qrString: json["qrString"],
        abapayDeeplink: json["abapay_deeplink"],
        appStore: json["app_store"],
        playStore: json["play_store"],
        transactionId: json["transaction_id"],
    );

    Map<String, dynamic> toJson() => {
        "status": status!.toJson(),
        "description": description,
        "qrString": qrString,
        "abapay_deeplink": abapayDeeplink,
        "app_store": appStore,
        "play_store": playStore,
        "transaction_id": transactionId,
    };
}

class Status {
    Status({
        this.code,
        this.message,
        this.tranId,
    });

    String? code;
    String? message;
    String? tranId;

    factory Status.fromJson(Map<String, dynamic> json) => Status(
        code: json["code"],
        message: json["message"],
        tranId: json["tran_id"],
    );

    Map<String, dynamic> toJson() => {
        "code": code,
        "message": message,
        "tran_id": tranId,
    };
}
