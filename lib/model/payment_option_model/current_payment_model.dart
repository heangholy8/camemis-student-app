// To parse this JSON data, do
//
//     final paymentCurrentModel = paymentCurrentModelFromJson(jsonString);

import 'dart:convert';

PaymentCurrentModel paymentCurrentModelFromJson(String str) => PaymentCurrentModel.fromJson(json.decode(str));

String paymentCurrentModelToJson(PaymentCurrentModel data) => json.encode(data.toJson());

class PaymentCurrentModel {
    PaymentCurrentModel({
        this.data,
    });

    Data? data;

    factory PaymentCurrentModel.fromJson(Map<String, dynamic> json) => PaymentCurrentModel(
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "data": data!.toJson(),
    };
}

class Data {
    Data({
        this.currentPayment,
        this.newPlan,
    });

    CurrentPayment? currentPayment;
    CurrentPayment? newPlan;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        currentPayment:json["current_payment"]==null?null:CurrentPayment.fromJson(json["current_payment"]),
        newPlan:json["new_plan"]==null?null: CurrentPayment.fromJson(json["new_plan"]),
    );

    Map<String, dynamic> toJson() => {
        "current_payment": currentPayment==null?null:currentPayment!.toJson(),
        "new_plan": newPlan==null?null: newPlan!.toJson(),
    };
}

class CurrentPayment {
    CurrentPayment({
        this.id,
        this.guardianId,
        this.invoiceNumber,
        this.status,
        this.piadAt,
        this.amount,
        this.currency,
        this.choosePayOptionName,
        this.choosePayOptionNameEn,
        this.choosePayOption,
        this.paymentMethod,
        this.transactionId,
        this.expiredAt,
        this.expiredIn,
        this.createdAt,
    });

    int? id;
    String? guardianId;
    String? invoiceNumber;
    String? status;
    String? piadAt;
    int? amount;
    String? currency;
    String? choosePayOptionName;
    String? choosePayOptionNameEn;
    int? choosePayOption;
    String? paymentMethod;
    String? transactionId;
    String? expiredAt;
    int? expiredIn;
    String? createdAt;

    factory CurrentPayment.fromJson(Map<String, dynamic> json) => CurrentPayment(
        id: json["id"],
        guardianId: json["guardian_id"],
        invoiceNumber: json["invoice_number"],
        status: json["status"],
        piadAt: json["piad_at"] == null ? null : json["piad_at"],
        amount: json["amount"],
        currency: json["currency"],
        choosePayOptionName: json["choose_pay_option_name"],
        choosePayOptionNameEn: json["choose_pay_option_name_en"],
        choosePayOption: json["choose_pay_option"],
        paymentMethod: json["payment_method"],
        transactionId: json["transaction_id"],
        expiredAt: json["expired_at"],
        expiredIn: json["expired_in"],
        createdAt: json["created_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "guardian_id": guardianId,
        "invoice_number": invoiceNumber,
        "status": status,
        "piad_at": piadAt == null ? null : piadAt,
        "amount": amount,
        "currency": currency,
        "choose_pay_option_name": choosePayOptionName,
        "choose_pay_option_name_en": choosePayOptionNameEn,
        "choose_pay_option": choosePayOption,
        "payment_method": paymentMethod,
        "transaction_id": transactionId,
        "expired_at": expiredAt,
        "expired_in": expiredIn,
        "created_at": createdAt,
    };
}
