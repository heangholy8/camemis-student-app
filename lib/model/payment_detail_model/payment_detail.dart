class PaymentDetailData {
  int? id;
  String? guardianId;
  String? invoiceNumber;
  String? status;
  String? piadAt;
  int? amount;
  String? currency;
  String? choosePayOptionName;
  String? choosePayOptionNameEn;
  int? choosePayOption;
  String? paymentMethod;
  dynamic transactionId;
  dynamic expiredAt;
  int? expiredIn;
  String? createdAt;

  PaymentDetailData(
      {this.id,
      this.guardianId,
      this.invoiceNumber,
      this.status,
      this.piadAt,
      this.amount,
      this.currency,
      this.choosePayOptionName,
      this.choosePayOptionNameEn,
      this.choosePayOption,
      this.paymentMethod,
      this.transactionId,
      this.expiredAt,
      this.expiredIn,
      this.createdAt});

  PaymentDetailData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    guardianId = json['guardian_id'];
    invoiceNumber = json['invoice_number'];
    status = json['status'];
    piadAt = json['piad_at'];
    amount = json['amount'];
    currency = json['currency'];
    choosePayOptionName = json['choose_pay_option_name'];
    choosePayOptionNameEn = json['choose_pay_option_name_en'];
    choosePayOption = json['choose_pay_option'];
    paymentMethod = json['payment_method'];
    transactionId = json['transaction_id'];
    expiredAt = json['expired_at'];
    expiredIn = json['expired_in'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['guardian_id'] = this.guardianId;
    data['invoice_number'] = this.invoiceNumber;
    data['status'] = this.status;
    data['piad_at'] = this.piadAt;
    data['amount'] = this.amount;
    data['currency'] = this.currency;
    data['choose_pay_option_name'] = this.choosePayOptionName;
    data['choose_pay_option_name_en'] = this.choosePayOptionNameEn;
    data['choose_pay_option'] = this.choosePayOption;
    data['payment_method'] = this.paymentMethod;
    data['transaction_id'] = this.transactionId;
    data['expired_at'] = this.expiredAt;
    data['expired_in'] = this.expiredIn;
    data['created_at'] = this.createdAt;
    return data;
  }
}
