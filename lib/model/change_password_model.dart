class ChangePassModel {
    ChangePassModel({
        this.qr,
    });
    String? qr;
    factory ChangePassModel.fromJson(Map<String, dynamic> json) => ChangePassModel(
        qr: json["qr"],
    );
    Map<String, dynamic> toJson() => {
        "qr": qr,
    };
}
