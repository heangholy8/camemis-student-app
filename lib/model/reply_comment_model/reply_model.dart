class ReplyCommentModel {
    ReplyCommentModel({
        this.id,
        this.name,
        this.startDate,
        this.endDate,
        this.startTime,
        this.endTime,
        this.description,
        this.type,
        this.virtualRoom,
        this.virtualUrl,
        this.isLive,
        this.category,
        this.subjectId,
        this.classId,
        this.createdDate,
        this.attachments,
        this.forums,
        this.liveSchedules,
        this.virtualLearningActivitys,
        this.joinVirtualLearningActivitys,
    });

    int? id;
    String? name;
    String? startDate;
    String? endDate;
    String? startTime;
    String? endTime;
    String? description;
    int? type;
    String ?virtualRoom;
    String ?virtualUrl;
    bool? isLive;
    String ?category;
    int ?subjectId;
    int? classId;
    String? createdDate;
    List<dynamic>? attachments;
    List<Forum> ?forums;
    List<dynamic>? liveSchedules;
    List<VirtualLearningActivity>? virtualLearningActivitys;
    List<VirtualLearningActivity>? joinVirtualLearningActivitys;

    factory ReplyCommentModel.fromJson(Map<String, dynamic> json) => ReplyCommentModel(
        id: json["id"],
        name: json["name"],
        startDate: json["start_date"],
        endDate: json["end_date"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        description: json["description"],
        type: json["type"],
        virtualRoom: json["virtual_room"],
        virtualUrl: json["virtual_url"],
        isLive: json["is_live"],
        category: json["category"],
        subjectId: json["subject_id"],
        classId: json["class_id"],
        createdDate: json["created_date"],
        attachments: List<dynamic>.from(json["attachments"].map((x) => x)),
        forums: List<Forum>.from(json["forums"].map((x) => Forum.fromJson(x))),
        liveSchedules: List<dynamic>.from(json["liveSchedules"].map((x) => x)),
        virtualLearningActivitys: List<VirtualLearningActivity>.from(json["virtualLearningActivitys"].map((x) => VirtualLearningActivity.fromJson(x))),
        joinVirtualLearningActivitys: List<VirtualLearningActivity>.from(json["joinVirtualLearningActivitys"].map((x) => VirtualLearningActivity.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "start_date": startDate,
        "end_date": endDate,
        "start_time": startTime,
        "end_time": endTime,
        "description": description,
        "type": type,
        "virtual_room": virtualRoom,
        "virtual_url": virtualUrl,
        "is_live": isLive,
        "category": category,
        "subject_id": subjectId,
        "class_id": classId,
        "created_date": createdDate,
        "attachments": List<dynamic>.from(attachments!.map((x) => x)),
        "forums": List<dynamic>.from(forums!.map((x) => x.toJson())),
        "liveSchedules": List<dynamic>.from(liveSchedules!.map((x) => x)),
        "virtualLearningActivitys": List<dynamic>.from(virtualLearningActivitys!.map((x) => x.toJson())),
        "joinVirtualLearningActivitys": List<dynamic>.from(joinVirtualLearningActivitys!.map((x) => x.toJson())),
    };
}

class Forum {
    Forum({
        this.id,
        this.parent,
        this.lessonAssignmentId,
        this.comment,
        this.createdDate,
        this.roleId,
        this.userProfile,
        this.children,
    });

    int? id;
    int? parent;
    int? lessonAssignmentId;
    String? comment;
    String? createdDate;
    int? roleId;
    UserProfile? userProfile;
    List<dynamic>? children;

    factory Forum.fromJson(Map<String, dynamic> json) => Forum(
        id: json["id"],
        parent: json["parent"],
        lessonAssignmentId: json["lesson_assignment_id"],
        comment: json["comment"],
        createdDate: json["created_date"],
        roleId: json["role_id"],
        userProfile: UserProfile.fromJson(json["userProfile"]),
        children: List<dynamic>.from(json["children"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "parent": parent,
        "lesson_assignment_id": lessonAssignmentId,
        "comment": comment,
        "created_date": createdDate,
        "role_id": roleId,
        "userProfile": userProfile?.toJson(),
        "children": List<dynamic>.from(children!.map((x) => x)),
    };
}

class UserProfile {
    UserProfile({
        this.id,
        this.schoolCode,
        this.code,
        this.name,
        this.nameEn,
        this.firstname,
        this.lastname,
        this.firstnameEn,
        this.lastnameEn,
        this.gender,
        this.genderName,
        this.dob,
        this.phone,
        this.email,
        this.address,
        this.framework,
        this.prokas,
        this.role,
        this.profileMedia,
        this.qrUrl,
    });

    String? id;
    String? schoolCode;
    String? code;
    String? name;
    String? nameEn;
    String? firstname;
    String? lastname;
    String? firstnameEn;
    String? lastnameEn;
    int? gender;
    String? genderName;
    String? dob;
    String? phone;
    String? email;
    String? address;
    String? framework;
    String? prokas;
    dynamic role;
    UserProfileProfileMedia ?profileMedia;
    String? qrUrl;

    factory UserProfile.fromJson(Map<String, dynamic> json) => UserProfile(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        email: json["email"],
        address: json["address"],
        framework: json["framework"] ,
        prokas: json["prokas"],
        role: json["role"],
        profileMedia: UserProfileProfileMedia.fromJson(json["profileMedia"]),
        qrUrl: json["qr_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "email": email,
        "address": address,
        "framework": framework ,
        "prokas": prokas,
        "role": role,
        "profileMedia": profileMedia?.toJson(),
        "qr_url": qrUrl,
    };
}

class UserProfileProfileMedia {
    UserProfileProfileMedia({
        this.fileShow,
        this.fileThumbnail,
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
    });

    String? fileShow;
    String? fileThumbnail;
    int? id;
    String? fileName;
    String? fileSize;
    String? fileType;
    String? fileIndex;
    String ?fileArea;
    String? objectId;
    String? schoolUrl;
    String? postDate;
    int? isGdrive;
    String? fileGdriveId;
    int? isS3;

    factory UserProfileProfileMedia.fromJson(Map<String, dynamic> json) => UserProfileProfileMedia(
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
        id: json["id"] == null ? null : json["id"],
        fileName: json["file_name"] == null ? null : json["file_name"],
        fileSize: json["file_size"] == null ? null : json["file_size"],
        fileType: json["file_type"] == null ? null : json["file_type"],
        fileIndex: json["file_index"] == null ? null : json["file_index"],
        fileArea: json["file_area"] == null ? null : json["file_area"],
        objectId: json["object_id"] == null ? null : json["object_id"],
        schoolUrl: json["school_url"] == null ? null : json["school_url"],
        postDate: json["post_date"] == null ? null : json["post_date"],
        isGdrive: json["is_gdrive"] == null ? null : json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"] == null ? null : json["file_gdrive_id"],
        isS3: json["is_s3"] == null ? null : json["is_s3"],
    );

    Map<String, dynamic> toJson() => {
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
        "id": id == null ? null : id,
        "file_name": fileName == null ? null : fileName,
        "file_size": fileSize == null ? null : fileSize,
        "file_type": fileType == null ? null : fileType,
        "file_index": fileIndex == null ? null : fileIndex,
        "file_area": fileArea == null ? null : fileArea,
        "object_id": objectId == null ? null : objectId,
        "school_url": schoolUrl == null ? null : schoolUrl,
        "post_date": postDate == null ? null : postDate,
        "is_gdrive": isGdrive == null ? null : isGdrive,
        "file_gdrive_id": fileGdriveId == null ? null : fileGdriveId,
        "is_s3": isS3 == null ? null : isS3,
    };
}

class VirtualLearningActivity {
    VirtualLearningActivity({
        this.id,
        this.lessonAssignmentId,
        this.userId,
        this.userRole,
        this.profile,
        this.classId,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.activity,
    });

    int? id;
    int? lessonAssignmentId;
    String? userId;
    int? userRole;
    Profile? profile;
    int ?classId;
    int? status;
    DateTime ?createdAt;
    DateTime? updatedAt;
    int? activity;

    factory VirtualLearningActivity.fromJson(Map<String, dynamic> json) => VirtualLearningActivity(
        id: json["id"],
        lessonAssignmentId: json["lesson_assignment_id"],
        userId: json["user_id"],
        userRole: json["user_role"],
        profile: Profile.fromJson(json["profile"]),
        classId: json["class_id"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        activity: json["activity"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "lesson_assignment_id": lessonAssignmentId,
        "user_id": userId,
        "user_role": userRole,
        "profile": profile?.toJson(),
        "class_id": classId,
        "status": status ,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
        "activity": activity ,
    };
}

class Profile {
    Profile({
        this.id,
        this.schoolCode,
        this.code,
        this.name,
        this.nameEn,
        this.firstname,
        this.lastname,
        this.firstnameEn,
        this.lastnameEn,
        this.gender,
        this.genderName,
        this.dob,
        this.phone,
        this.email,
        this.address,
        this.role,
        this.profileMedia,
    });

    String? id;
    String? schoolCode;
    String? code;
    String? name;
    String? nameEn;
    String? firstname;
    String? lastname;
    String? firstnameEn;
    String? lastnameEn;
    int? gender;
    String? genderName;
    String? dob;
    String? phone;
    String? email;
    String? address;
    int? role;
    ProfileProfileMedia? profileMedia;

    factory Profile.fromJson(Map<String, dynamic> json) => Profile(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        email: json["email"],
        address: json["address"],
        role: json["role"],
        profileMedia: ProfileProfileMedia.fromJson(json["profileMedia"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "email": email,
        "address": address,
        "role": role,
        "profileMedia": profileMedia?.toJson(),
    };
}

class ProfileProfileMedia {
    ProfileProfileMedia({
        this.fileShow,
        this.fileThumbnail,
    });

    String? fileShow;
    String? fileThumbnail;

    factory ProfileProfileMedia.fromJson(Map<String, dynamic> json) => ProfileProfileMedia(
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
    );

    Map<String, dynamic> toJson() => {
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
    };
}
