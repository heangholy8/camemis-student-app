class CreatePaymentModel {
  CreatePaymentModel({
    this.id,
    this.invoiceNumber,
    this.guardianId,
    this.amount,
    this.currency,
    this.choosePayOption,
    this.choosePayOptionName,
    this.transactionId,
    this.status,
    this.month,
    this.semester,
    this.piadfor,
    this.paymentMethod,
    this.expiredAt,
    this.paidAt,
    this.createdAt,
    this.updatedAt,
    this.createdBy,
    this.createdByName,
    this.guardian,
  });

  dynamic id;
  dynamic invoiceNumber;
  dynamic guardianId;
  dynamic amount;
  dynamic currency;
  dynamic choosePayOption;
  dynamic choosePayOptionName;
  dynamic transactionId;
  dynamic status;
  dynamic month;
  dynamic semester;
  dynamic piadfor;
  dynamic paymentMethod;
  dynamic expiredAt;
  dynamic paidAt;
  dynamic createdAt;
  dynamic updatedAt;
  dynamic createdBy;
  dynamic createdByName;
  Guardian? guardian;

  factory CreatePaymentModel.fromJson(Map<String, dynamic> json) =>
      CreatePaymentModel(
        id: json["id"],
        invoiceNumber: json["invoice_number"],
        guardianId: json["guardian_id"],
        amount: json["amount"],
        currency: json["currency"],
        choosePayOption: json["choose_pay_option"],
        choosePayOptionName: json["choose_pay_option_name"],
        transactionId: json["transaction_id"],
        status: json["status"],
        month: json["month"],
        semester: json["semester"],
        piadfor: json["piadfor"],
        paymentMethod: json["payment_method"],
        expiredAt: json["expired_at"],
        paidAt: json["paid_at"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        createdBy: json["created_by"],
        createdByName: json["created_by_name"],
        guardian: Guardian.fromJson(json["guardian"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "invoice_number": invoiceNumber,
        "guardian_id": guardianId,
        "amount": amount,
        "currency": currency,
        "choose_pay_option": choosePayOption,
        "choose_pay_option_name": choosePayOptionName,
        "transaction_id": transactionId,
        "status": status,
        "month": month,
        "semester": semester,
        "piadfor": piadfor,
        "payment_method": paymentMethod,
        "expired_at": expiredAt,
        "paid_at": paidAt,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "created_by": createdBy,
        "created_by_name": createdByName,
        "guardian": guardian?.toJson(),
      };
}

class Guardian {
  Guardian({
    this.id,
    this.schoolCode,
    this.code,
    this.name,
    this.nameEn,
    this.firstname,
    this.lastname,
    this.firstnameEn,
    this.lastnameEn,
    this.gender,
    this.genderName,
    this.dob,
    this.phone,
    this.email,
    this.address,
    this.role,
    this.profileMedia,
    this.qrUrl,
  });

  dynamic id;
  dynamic schoolCode;
  dynamic code;
  dynamic name;
  dynamic nameEn;
  dynamic firstname;
  dynamic lastname;
  dynamic firstnameEn;
  dynamic lastnameEn;
  dynamic gender;
  dynamic genderName;
  dynamic dob;
  dynamic phone;
  dynamic email;
  dynamic address;
  dynamic role;
  ProfileMedia? profileMedia;
  dynamic qrUrl;

  factory Guardian.fromJson(Map<String, dynamic> json) => Guardian(
        id: json["id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        firstnameEn: json["firstname_en"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        email: json["email"],
        address: json["address"],
        role: json["role"],
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
        qrUrl: json["qr_url"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "firstname": firstname,
        "lastname": lastname,
        "firstname_en": firstnameEn,
        "lastname_en": lastnameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "email": email,
        "address": address,
        "role": role,
        "profileMedia": profileMedia?.toJson(),
        "qr_url": qrUrl,
      };
}

class ProfileMedia {
  ProfileMedia({
    this.id,
    this.fileName,
    this.fileSize,
    this.fileType,
    this.fileShow,
    this.fileThumbnail,
    this.fileIndex,
    this.fileArea,
    this.objectId,
    this.schoolUrl,
    this.postDate,
    this.isGdrive,
    this.fileGdriveId,
    this.isS3,
  });

  dynamic id;
  dynamic fileName;
  dynamic fileSize;
  dynamic fileType;
  dynamic fileShow;
  dynamic fileThumbnail;
  dynamic fileIndex;
  dynamic fileArea;
  dynamic objectId;
  dynamic schoolUrl;
  dynamic postDate;
  dynamic isGdrive;
  dynamic fileGdriveId;
  dynamic isS3;

  factory ProfileMedia.fromJson(Map<String, dynamic> json) => ProfileMedia(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
      };
}
