// To parse this JSON data, do
//
//     final responCheckAttendanceModel = responCheckAttendanceModelFromJson(jsonString);

import 'dart:convert';

ResponCheckAttendanceModel responCheckAttendanceModelFromJson(String str) => ResponCheckAttendanceModel.fromJson(json.decode(str));

String responCheckAttendanceModelToJson(ResponCheckAttendanceModel data) => json.encode(data.toJson());

class ResponCheckAttendanceModel {
    ResponCheckAttendanceModel({
        this.status,
        this.data,
    });

    bool? status;
    List<Datum>? data;

    factory ResponCheckAttendanceModel.fromJson(Map<String, dynamic> json) => ResponCheckAttendanceModel(
        status: json["status"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class Datum {
    Datum({
        this.studentId,
        this.schoolCode,
        this.code,
        this.name,
        this.nameEn,
        this.gender,
        this.genderName,
        this.dob,
        this.phone,
        this.email,
        this.attendanceStatus,
        this.profileMedia,
    });

    String? studentId;
    String? schoolCode;
    String? code;
    String? name;
    dynamic nameEn;
    int? gender;
    String? genderName;
    String? dob;
    dynamic phone;
    dynamic email;
    dynamic attendanceStatus;
    ProfileMedia? profileMedia;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        studentId: json["student_id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        email: json["email"],
        attendanceStatus: json["attendance_status"],
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
    );

    Map<String, dynamic> toJson() => {
        "student_id": studentId,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "email": email,
        "attendance_status": attendanceStatus,
        "profileMedia": profileMedia!.toJson(),
    };
}

class ProfileMedia {
    ProfileMedia({
        this.id,
        this.fileName,
        this.fileSize,
        this.fileType,
        this.fileIndex,
        this.fileArea,
        this.objectId,
        this.schoolUrl,
        this.postDate,
        this.isGdrive,
        this.fileGdriveId,
        this.isS3,
        this.fileShow,
        this.fileThumbnail,
    });

    int? id;
    dynamic fileName;
    dynamic fileSize;
    dynamic fileType;
    dynamic fileIndex;
    dynamic fileArea;
    dynamic objectId;
    dynamic schoolUrl;
    dynamic postDate;
    int? isGdrive;
    dynamic fileGdriveId;
    int? isS3;
    String? fileShow;
    String? fileThumbnail;

    factory ProfileMedia.fromJson(Map<String, dynamic> json) => ProfileMedia(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
    };
}
