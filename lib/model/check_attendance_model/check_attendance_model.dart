// To parse this JSON data, do
//
//     final checkAttendanceModel = checkAttendanceModelFromJson(jsonString);
class CheckAttendanceModel {
  CheckAttendanceModel({
    this.data,
  });

  Data? data;

  factory CheckAttendanceModel.fromJson(Map<String, dynamic> json) =>
      CheckAttendanceModel(
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "data": data!.toJson(),
      };
}

class Data {
  Data({
    this.scheduleId,
    this.subjectId,
    this.subjectName,
    this.subjectNameEn,
    this.classId,
    this.className,
    this.startTime,
    this.endTime,
    this.duration,
    this.date,
    this.teacherName,
    this.teacherNameEn,
    this.teacherId,
    this.attendance,
    this.listAttendance,
  });

  int? scheduleId;
  int? subjectId;
  String? subjectName;
  String? subjectNameEn;
  int? classId;
  String? className;
  String? startTime;
  String? endTime;
  String? duration;
  String? date;
  String? teacherName;
  String? teacherNameEn;
  String? teacherId;
  Attendance? attendance;
  List<ListAttendance>? listAttendance;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        scheduleId: json["schedule_id"],
        subjectId: json["subject_id"],
        subjectName: json["subject_name"],
        subjectNameEn: json["subject_name_en"],
        classId: json["class_id"],
        className: json["class_name"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        duration: json["duration"],
        date: json["date"],
        teacherName: json["teacher_name"],
        teacherNameEn: json["teacher_name_en"],
        teacherId: json["teacher_id"],
        attendance: Attendance.fromJson(json["attendance"]),
        listAttendance: List<ListAttendance>.from(
            json["list_attendance"].map((x) => ListAttendance.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "schedule_id": scheduleId,
        "subject_id": subjectId,
        "subject_name": subjectName,
        "subject_name_en": subjectNameEn,
        "class_id": classId,
        "class_name": className,
        "start_time": startTime,
        "end_time": endTime,
        "duration": duration,
        "date": date,
        "teacher_name": teacherName,
        "teacher_name_en": teacherNameEn,
        "teacher_id": teacherId,
        "attendance": attendance?.toJson(),
        "list_attendance":
            List<dynamic>.from(listAttendance!.map((x) => x.toJson())),
      };
}

class Attendance {
  Attendance({
    this.totalStudent,
    this.present,
    this.absent,
    this.late,
    this.permission,
  });

  int? totalStudent;
  int? present;
  int? absent;
  int? late;
  int? permission;

  factory Attendance.fromJson(Map<String, dynamic> json) => Attendance(
        totalStudent: json["total_student"],
        present: json["present"],
        absent: json["absent"],
        late: json["late"],
        permission: json["permission"],
      );

  Map<String, dynamic> toJson() => {
        "total_student": totalStudent,
        "present": present,
        "absent": absent,
        "late": late,
        "permission": permission,
      };
}

class ListAttendance {
  ListAttendance({
    this.studentId,
    this.schoolCode,
    this.code,
    this.name,
    this.nameEn,
    this.gender,
    this.genderName,
    this.dob,
    this.phone,
    this.email,
    this.attendanceStatus,
    this.profileMedia,
  });

  String? studentId;
  String? schoolCode;
  String? code;
  String? name;
  dynamic nameEn;
  int? gender;
  String? genderName;
  String? dob;
  dynamic phone;
  dynamic email;
  int? attendanceStatus;
  ProfileMedia? profileMedia;

  factory ListAttendance.fromJson(Map<dynamic, dynamic> json) => ListAttendance(
        studentId: json["student_id"],
        schoolCode: json["school_code"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        gender: json["gender"],
        genderName: json["gender_name"],
        dob: json["dob"],
        phone: json["phone"],
        email: json["email"],
        attendanceStatus: json["attendance_status"],
        profileMedia: ProfileMedia.fromJson(json["profileMedia"]),
      );

  Map<dynamic, dynamic> toJson() => {
        "student_id": studentId,
        "school_code": schoolCode,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "gender": gender,
        "gender_name": genderName,
        "dob": dob,
        "phone": phone,
        "email": email,
        "attendance_status": attendanceStatus,
        "profileMedia": profileMedia!.toJson(),
      };
}

class ProfileMedia {
  ProfileMedia({
    this.id,
    this.fileName,
    this.fileSize,
    this.fileType,
    this.fileIndex,
    this.fileArea,
    this.objectId,
    this.schoolUrl,
    this.postDate,
    this.isGdrive,
    this.fileGdriveId,
    this.isS3,
    this.fileShow,
    this.fileThumbnail,
  });

  int? id;
  dynamic fileName;
  dynamic fileSize;
  dynamic fileType;
  dynamic fileIndex;
  dynamic fileArea;
  dynamic objectId;
  dynamic schoolUrl;
  dynamic postDate;
  int? isGdrive;
  dynamic fileGdriveId;
  int? isS3;
  String? fileShow;
  String? fileThumbnail;

  factory ProfileMedia.fromJson(Map<String, dynamic> json) => ProfileMedia(
        id: json["id"],
        fileName: json["file_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileIndex: json["file_index"],
        fileArea: json["file_area"],
        objectId: json["object_id"],
        schoolUrl: json["school_url"],
        postDate: json["post_date"],
        isGdrive: json["is_gdrive"],
        fileGdriveId: json["file_gdrive_id"],
        isS3: json["is_s3"],
        fileShow: json["file_show"],
        fileThumbnail: json["file_thumbnail"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "file_name": fileName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_index": fileIndex,
        "file_area": fileArea,
        "object_id": objectId,
        "school_url": schoolUrl,
        "post_date": postDate,
        "is_gdrive": isGdrive,
        "file_gdrive_id": fileGdriveId,
        "is_s3": isS3,
        "file_show": fileShow,
        "file_thumbnail": fileThumbnail,
      };
}
