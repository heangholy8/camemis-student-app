import 'package:camis_application_flutter/app/modules/about_screen/view/about_screen.dart';
import 'package:camis_application_flutter/app/modules/attendance_screen.dart/view/schedule_screen.dart';
import 'package:camis_application_flutter/app/modules/account_confirm_screen/view/account_confirm_screen.dart';
import 'package:camis_application_flutter/app/modules/auth_screens/views/code_entry_screen.dart';
import 'package:camis_application_flutter/app/modules/auth_screens/views/option_login_screen.dart';
import 'package:camis_application_flutter/app/modules/auth_screens/views/otp/login_otp_screen.dart';
import 'package:camis_application_flutter/app/modules/guide_line/view/guid_line_screen.dart';
import 'package:camis_application_flutter/app/modules/help_screen/view/help_screen.dart';
import 'package:camis_application_flutter/app/modules/teacher_list_screen/view/teacher_list_screen.dart';
import 'package:camis_application_flutter/app/modules/my_profile_screen/views/my_profile_screen.dart';
import 'package:camis_application_flutter/app/modules/result_screen/view/result_screen_V2.dart';
import '../modules/PaymentAndBillScreen/view/current_payment_screen.dart';
import '../modules/PaymentAndBillScreen/view/payment_history.dart';
import '../modules/PaymentAndBillScreen/view/selected_payment_option.dart';
import '../modules/absent_request_screen.dart/view/absent_request_screen.dart';
import '../modules/chat_screen/views/message_screen.dart';
import '../modules/child_list_screen/view/child_list_screen.dart';
import '../modules/teaching_learning_screen/view/teaching_learning_screen.dart';
import '../modules/time_line/time_line.dart';
import 'e.route.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // Getting arguments passed in while calling Navigator.pushNamed
    final args = settings.arguments;
    switch (settings.name) {
      case Routes.SPLAHSSCREEN:
        return MaterialPageRoute(builder: (_) => const SplashScreen());
      case Routes.ONBOARDING:
        return MaterialPageRoute(builder: (_) => const LandingScreen());
        case Routes.OPTIONLOGINSCREEN:
        return MaterialPageRoute(builder: (_) =>  OptionLoginScreen());
      case Routes.HOMESCREEN:
        return MaterialPageRoute(builder: (_) => HomeScreen());
      case Routes.RESULT:
        return MaterialPageRoute(builder: (_) =>  ResultScreenV2());
      case Routes.TIMELINE:
        return MaterialPageRoute(builder: (_) =>  TimeLineScreen());
      case Routes.PAYMENT:
        return MaterialPageRoute(builder: (_) => const PaymentAndBillScreen());
      case Routes.LEARNING:
        return MaterialPageRoute(builder: (_) =>const TeachingLearningScreen());
      case Routes.MORE:
        return MaterialPageRoute(builder: (_) => const MoreScreen());
      case Routes.CHAT:
        return MaterialPageRoute(builder: (_) => const MessageScreen());
      case Routes.MAKEREQUEST:
        return MaterialPageRoute(builder: (_) => const OngoingAndHistoryAbsent());
      case Routes.GUIDLINE:
        return MaterialPageRoute(builder: (_) => const GuidLineScreen());
      case Routes.HELPSCREEN:
        return MaterialPageRoute(builder: (_) => const HelpScreen());
      case Routes.ABOUTSCREEN:
        return MaterialPageRoute(builder: (_) => const AboutScreen());
      case Routes.PAYMENTOPTIONSCREEN:
        return MaterialPageRoute(builder: (_) => const SelectedPaymentOption());
      case Routes.QRCODESCREEN:
        return MaterialPageRoute(builder: (_) =>  const QrCodeLoginScreen());
      case Routes.ENTRYCODELOGINSCREEN:
        return MaterialPageRoute(builder: (_) => const LoginCodeScreen());
      case Routes.TEACHERLISTSCREEN:
        return MaterialPageRoute(builder: (_) =>  TeacherListScreen());
      case Routes.CHLIDLISTSCREEN:
        return MaterialPageRoute(builder: (_) => const ChildListScreen());
      case Routes.ATTENDANCESCREEN:
        return MaterialPageRoute(builder: (_) => ScheduleScreen());
      case Routes.LOGINOTP:
        return MaterialPageRoute(builder: (_) => LoginOTPScreen());
      case Routes.ACCOUNTCONFIRM:
        return MaterialPageRoute(builder: (_) => const AccountConfirmScreen());
      case Routes.MYPROFILE:
        return MaterialPageRoute(builder: (_) => const MyProfileScreen());
      case Routes.CURRENTPAYMENTSCREEN:
        return MaterialPageRoute(builder: (_) => const PaymentCurrentScreen());
      default:
         return MaterialPageRoute(builder: (_) => HomeScreen());
    }
  }
}
