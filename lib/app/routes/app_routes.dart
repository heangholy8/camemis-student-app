// ignore_for_file: constant_identifier_names

import 'package:camis_application_flutter/app/modules/my_profile_screen/views/my_profile_screen.dart';

abstract class Routes {
  Routes._();

  static const SPLAHSSCREEN = _Paths.SPLASH;
  static const ONBOARDING = _Paths.ONBOARD;
  static const LOGINSCREEN = _Paths.LOGIN;

  // Features Home Screen
  static const HOMESCREEN = _Paths.HOME;
  static const ANNOUNCEMENTSCREEN = _Paths.ANNOUNCEMENT;
  static const ATTENDANCESCREEN = _Paths.ATTENDANCE;
  static const NOTIFICATION = _Paths.NOTIFICATION;
  static const RESULT = _Paths.RESULT;
  static const TIMELINE = _Paths.TIMELINE;
  static const PAYMENT = _Paths.PAYMENT;
  static const LEARNING = _Paths.LEARNING;
  static const MORE = _Paths.MORE;
  static const CHAT = _Paths.CHAT;
  static const MAKEREQUEST = _Paths.MAKEREQUEST;
  static const GUIDLINE = _Paths.GUIDLINE;
  static const OPTIONLOGINSCREEN = _Paths.OPTIONLOGIN;
  static const HELPSCREEN = _Paths.HELP;
  static const ABOUTSCREEN = _Paths.ABOUT;
  static const PAYMENTOPTIONSCREEN = _Paths.PAYMENTOPTION;
  static const QRCODESCREEN = _Paths.QRCODE;
  static const ENTRYCODELOGINSCREEN = _Paths.ENTRYCODELOGIN;
  static const TEACHERLISTSCREEN = _Paths.TEACHERLIST;
  static const CHLIDLISTSCREEN = _Paths.CHILDLIST;
  static const LOGINOTP = _Paths.LOGINOTP;
  static const MYPROFILE = _Paths.MYPROFILE;
  static const ACCOUNTCONFIRM = _Paths.ACCOUNTCONFIRM;
  static const CURRENTPAYMENTSCREEN = _Paths.CURRENTPAYMENT;
}

abstract class _Paths {
  static const SPLASH = '/splash';
  static const ONBOARD = '/onboarding';
  static const LOGIN = '/login';
  static const HOME = '/home';
  static const ANNOUNCEMENT = '/announcement';
  static const NOTIFICATION = '/notification';
  static const RESULT = '/result';
  static const TIMELINE = '/timeline';
  static const PAYMENT = '/payment';
  static const LEARNING = '/learning';
  static const MORE = '/more';
  static const CHAT = '/chat';
  static const MAKEREQUEST = '/makerequest';
  static const GUIDLINE = '/guidline';
  static const OPTIONLOGIN = '/option_login';
  static const HELP = '/help';
  static const ABOUT = '/about';
  static const PAYMENTOPTION = '/payment_option';
  static const QRCODE = '/qr_code_login';
  static const ENTRYCODELOGIN = '/entry_code_login';
  static const TEACHERLIST = '/teacher_list';
  static const CHILDLIST = '/child_list';
  static const ATTENDANCE = '/attendance_screen';
  static const LOGINOTP = '/login_otp';
  static const MYPROFILE = '/my_profile_screen';
  static const ACCOUNTCONFIRM = '/account_confirm_screen';
  static const CURRENTPAYMENT = '/current_payment_screen';
}
