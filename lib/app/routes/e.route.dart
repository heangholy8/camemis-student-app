export 'package:camis_application_flutter/app/modules/auth_screens/e_school_code.dart';
export 'package:camis_application_flutter/app/modules/landing_screen/view/landing_screen.dart';
export 'package:camis_application_flutter/app/modules/more_screen/view/more_screen.dart';
export 'package:camis_application_flutter/app/modules/splash_screen/view/splash_screen.dart';
export 'package:camis_application_flutter/app/routes/app_routes.dart';
