import 'package:flutter/material.dart';

class PaginationScreen extends StatefulWidget {
  const PaginationScreen({Key? key}) : super(key: key);

  @override
  State<PaginationScreen> createState() => _PaginationScreenState();
}

class _PaginationScreenState extends State<PaginationScreen> {
  final ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Pagination View"),
      ),
      body: Column(
        children: <Widget>[
          Container(
            height: 50.0,
            color: Colors.green,
            child: Center(
              child: Text("message"),
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: 30,
              itemBuilder: (context, index) {
                return ListTile(title: Text("Segment : $index"));
              },
            ),
          ),
        ],
      ), 
    );
  }

  @override
  void initState() {
    //added the pagination function with listener
    scrollController.addListener(pagination);
    super.initState();
  }

//_subCategoryModel only use for check the length of product

  void pagination() {
    // if ((scrollController.position.pixels ==
    //         scrollController.position.maxScrollExtent) &&
    //     (_subCategoryModel.products.length < total)) {
    //   setState(() {
    //     isLoading = true;
    //     page += 1;
    //     //add api for load the more data according to new page
    //   });
    // }
  }
}
