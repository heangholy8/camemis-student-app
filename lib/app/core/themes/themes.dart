import 'package:flutter/material.dart';

class ThemeConstands {
  static const List<String> fontFamilyFallback = ['KantumruyPro'];

  static TextTheme get texttheme {
    return const TextTheme(
      headline1: TextStyle(
        fontSize: 53,
        fontWeight: FontWeight.w300,
        letterSpacing: -1.5,
        height: 112,
        fontFamilyFallback: fontFamilyFallback,
      ),
      headline2: TextStyle(
        fontSize: 46,
        fontWeight: FontWeight.w300,
        letterSpacing: -1.5,
        height: 58,
        fontFamilyFallback: fontFamilyFallback,
      ),
      headline3: TextStyle(
        fontSize: 33,
        fontWeight: FontWeight.w400,
        letterSpacing: 0,
        fontFamilyFallback: fontFamilyFallback,
      ),
      headline4: TextStyle(
        fontSize: 23,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.25,
        fontFamilyFallback: fontFamilyFallback,
      ),
      headline5: TextStyle(
        fontSize: 19,
        fontWeight: FontWeight.w400,
        letterSpacing: 0,
        fontFamilyFallback: fontFamilyFallback,
      ),
      headline6: TextStyle(
        fontSize: 17,
        fontWeight: FontWeight.w500,
        letterSpacing: 0.15,
        fontFamilyFallback: fontFamilyFallback,
      ),
      subtitle1: TextStyle(
        fontSize: 15,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.15,
        fontFamilyFallback: fontFamilyFallback,
      ),
      subtitle2: TextStyle(
        fontSize: 13,
        fontWeight: FontWeight.w500,
        letterSpacing: 0.1,
        fontFamilyFallback: fontFamilyFallback,
      ),
      bodyText1: TextStyle(
        fontSize: 15,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.5,
        fontFamilyFallback: fontFamilyFallback,
      ),
      bodyText2: TextStyle(
        fontSize: 13,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.25,
        fontFamilyFallback: fontFamilyFallback,
      ),
      button: TextStyle(
        fontSize: 13,
        fontWeight: FontWeight.w500,
        letterSpacing: 1.25,
        fontFamilyFallback: fontFamilyFallback,
      ),
      caption: TextStyle(
        fontSize: 12,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.4,
        fontFamilyFallback: fontFamilyFallback,
      ),
      overline: TextStyle(
        fontSize: 10,
        fontWeight: FontWeight.w400,
        letterSpacing: 1.5,
        fontFamilyFallback: fontFamilyFallback,
      ),
    );
  }

  static const  headline1_SemiBold_32 =  TextStyle(
    fontFamily: "KantumruyPro-SemiBold",
    fontStyle:  FontStyle.normal,
    fontSize: 32.0
);
  static const  headline1_SemiBold_322 =  TextStyle(
    fontFamily: "KantumruyPro-Regular",
    fontStyle:  FontStyle.normal,
    fontSize: 32.0
);
  static const  headline2_SemiBold_242 =  TextStyle(
    fontFamily: "KantumruyPro-SemiBold",
    fontStyle:  FontStyle.normal,
    fontSize: 24.0
);
  static const  headline2_SemiBold_24 =  TextStyle(
    fontFamily: "KantumruyPro-SemiBold",
    fontStyle:  FontStyle.normal,
    fontSize: 24.0
);
  static const  headline3_SemiBold_20 =  TextStyle(
    fontFamily: "KantumruyPro-SemiBold",
    fontStyle:  FontStyle.normal,
    fontSize: 20.0
);
  static const  headline3_Medium_20_26height =  TextStyle(
    fontFamily: "KantumruyPro-Regular",
    fontStyle:  FontStyle.normal,
    fontSize: 20.0
);
  static const  headline3_Medium_20 =  TextStyle(
    fontFamily: "KantumruyPro-Regular",
    fontStyle:  FontStyle.normal,
    fontSize: 20.0
);
  static const  headline4_SemiBold_18 =  TextStyle(
    fontFamily: "KantumruyPro-SemiBold",
    fontStyle:  FontStyle.normal,
    fontSize: 18.0
);
  static const  headline4_Medium_18 =  TextStyle(
    fontFamily: "KantumruyPro-Regular",
    fontStyle:  FontStyle.normal,
    fontSize: 18.0
);
  static const  headline4_Regular_18 =  TextStyle(
    fontFamily: "KantumruyPro-Regular",
    fontStyle:  FontStyle.normal,
    fontSize: 18.0
);
  static const  headline5_SemiBold_16 =  TextStyle(
    fontFamily: "KantumruyPro-SemiBold",
    fontStyle:  FontStyle.normal,
    fontSize: 16.0
);
  static const  button_SemiBold_16 =  TextStyle(
    fontFamily: "KantumruyPro-SemiBold",
    fontStyle:  FontStyle.normal,
    fontSize: 16.0
);
  static const  headline5_Medium_16 =  TextStyle(
    fontFamily: "KantumruyPro-Regular",
    fontStyle:  FontStyle.normal,
    fontSize: 16.0
);
  static const  subtitle1_Regular_16 =  TextStyle(
    fontFamily: "KantumruyPro-Regular",
    fontStyle:  FontStyle.normal,
    fontSize: 16.0
);
  static const  headline6_SemiBold_14 =  TextStyle(
    fontFamily: "KantumruyPro-SemiBold",
    fontStyle:  FontStyle.normal,
    fontSize: 14.0
);
  static const  headline6_Medium_14 =  TextStyle(
    fontFamily: "KantumruyPro-Regular",
    fontStyle:  FontStyle.normal,
    fontSize: 14.0
);
  static const  headline6_Regular_14_24height =  TextStyle(
    fontFamily: "KantumruyPro-Regular",
    fontStyle:  FontStyle.normal,
    fontSize: 14.0
);
  static const  headline6_Regular_14_20height =  TextStyle(
    fontFamily: "KantumruyPro-Regular",
    fontStyle:  FontStyle.normal,
    fontSize: 14.0
);
  static const  overline_Semibold_12 =  TextStyle(
    fontFamily: "KantumruyPro-SemiBold",
    fontStyle:  FontStyle.normal,
    fontSize: 12.0
);
  static const  caption_Regular_12 =  TextStyle(
    fontFamily: "KantumruyPro-Regular",
    fontStyle:  FontStyle.normal,
    fontSize: 12.0
);
}
