// ignore_for_file: constant_identifier_names
const String ICONS_PATH = "assets/icons";
const String IMAGE_PATH = "assets/images";
const String LOGO_PATH = "assets/logo";
const String CLIP_PATH = "assets/clip_images";
const String SVG_PATH = "assets/images/svg";
const String ICONS_SVG_PATH = "assets/icons/svg";

class ImageAssets {
  // <<<<<<<<<<<<<<< LOGO >>>>>>>>>>>>>>>>> //

  static const String app_logo = 'assets/logo/logo.svg';
  static const String logoCamis = '$IMAGE_PATH/logo.svg';
  static const String lock = '$SVG_PATH/lock.png';


  // *************** ICON ***************** //

  static const String menu_icon = '$ICONS_PATH/menu_icon.svg';
  static const String scan_icon = '$ICONS_PATH/qr_code_icon.svg';
  static const String number_icon = '$ICONS_PATH/number_code_icon.svg';
  static const String flash_icon = "$ICONS_PATH/flash_icon.svg";
  static const String error_icon = "$ICONS_PATH/error_icon.svg";
  static const String bus_icon = "$ICONS_PATH/bus.svg";
  static const String food_icon = "$ICONS_PATH/foods.svg";
  static const String health_icon = "$ICONS_PATH/health.svg";
  static const String onRecord_icon = "$ICONS_PATH/onRecord.svg";
  static const String recorder_icon = "$ICONS_PATH/recorder.svg";
  static const String sent_icon = "$ICONS_PATH/send.svg";
  static const String cross_circle = "$ICONS_PATH/cross-circle.svg";
  static const String delete_bin = "$ICONS_PATH/delete_bin.svg";
  static const String notification_dot = "$ICONS_PATH/notification_dot.svg";
  static const String error_line = "$ICONS_PATH/error-warning-line.svg";
  static const String screen_shoot = "$ICONS_PATH/screen_shot.svg";
  static const String aceleda = "$ICONS_PATH/aceleda.png";
  static const String tick_icon_with_border = "$ICONS_PATH/tick_icon_with_border.svg";
  static const String like_icon = "$ICONS_PATH/icon_like.svg";
  static const String comment_icon = "$ICONS_PATH/icon_comment.svg";
  static const String reply_icon = "$ICONS_PATH/icon_reply.svg";
  static const String questoin_icon = "$SVG_PATH/question.svg";
  static const String backgroundResult = "$SVG_PATH/background_result.svg";
  static const String path20 = "$SVG_PATH/Path_20.svg";
  static const String award = "$SVG_PATH/award.svg";
  static const String addItem = "$SVG_PATH/additem.svg";
  static const String chart = "$SVG_PATH/chart.svg";
  static const String absent = "$SVG_PATH/profile-remove.svg";
  static const String comment = "$SVG_PATH/comment.svg";
  static const String status_up = "$SVG_PATH/status-up.svg";





 // Icon buttom navigator=================================
  static const String notification_outline_icon = '$ICONS_SVG_PATH/vuesax_outline_notification.svg';
  static const String notification_icon = '$ICONS_SVG_PATH/notification-bing.svg';
  static const String more_outline_icon = '$ICONS_SVG_PATH/81-more-circle-2.svg';
  static const String more_icon = '$ICONS_SVG_PATH/vuesax_bold_more-circle.svg';
  static const String task_outline_icon = '$ICONS_SVG_PATH/vuesax_outline_task-square.svg';
  static const String task_icon = '$ICONS_SVG_PATH/task-square.svg';
  static const String home_icon = '$ICONS_SVG_PATH/home-2.svg';
  static const String home_outline_icon ='$ICONS_SVG_PATH/vuesax_outline_home-2.svg';
  static const String tilemline_icon = 'assets/icons/features/timeline_icon.svg';
  static const String tilemline_outline_icon ='assets/icons/features/timeline_icon_outline.svg';

  //===============more Screen ===========================
  static const String user_edit = '$ICONS_SVG_PATH/vuesax_outline_user-edit.svg';
  static const String people_icon = '$ICONS_SVG_PATH/vuesax_bold_people.svg';
  static const String clipboard_icon = '$ICONS_SVG_PATH/vuesax_bold_clipboard-text.svg';
  static const String setting_icon = '$ICONS_SVG_PATH/vuesax_bold_setting-2.svg';
  static const String phone_icon = '$ICONS_SVG_PATH/vuesax_bold_call.svg';
  static const String chevron_right_icon = '$ICONS_SVG_PATH/chevron-right.svg';
  static const String ranking_icon = '$ICONS_SVG_PATH/ranking.svg';
  static const String call_outlinr_icon = '$ICONS_SVG_PATH/call.svg';
  static const String setting_outlinr_icon = '$ICONS_SVG_PATH/setting-2.svg';
  static const String profile_2users = '$ICONS_SVG_PATH/profile2user.svg';
  static const String profile2user_outline = '$ICONS_SVG_PATH/profile2user_outline.svg';
  static const String profile_circle_outline = '$ICONS_SVG_PATH/profile_circle_outline.svg';
  static const String profile_circle = '$ICONS_SVG_PATH/profile_circle.svg';
  static const String CAMERA_icon = "$ICONS_SVG_PATH/ic_camera.svg";
  static const String telegram_icon = "assets/icons/telegram.svg";
  
  
  static const String card_tick = '$ICONS_SVG_PATH/card-tick.svg';
  static const String card_tick_outline = '$ICONS_SVG_PATH/card-tick-outline.svg';
  //===============help Screen ===========================
  static const String Path_18 = "$IMAGE_PATH/Path_18.png";
  static const String Path_19 = "$IMAGE_PATH/Path_19.png";
  static const String Path_20 = "$IMAGE_PATH/Path_20.png";
  static const String Path_21 = "$IMAGE_PATH/Path_21.png";
  static const String Path_22 = "$IMAGE_PATH/Path_22.png";
  static const String Path_19_About = "$IMAGE_PATH/Path_19_about.png";
  static const String Path_19_help = "$IMAGE_PATH/Path_19_help.png";
  static const String Path_21_help = "$IMAGE_PATH/Path_21_help.png";
  static const String Path_18_sch = "$IMAGE_PATH/path_18_sch.png";
  static const String SEARCH_ICON = "$ICONS_SVG_PATH/search_icon.svg";
  static const String OFFICESPACE = "$ICONS_SVG_PATH/officespace.svg";
  

  //============Attendant Screen ====================
  static const String EXAM_ICON = "$ICONS_SVG_PATH/exam_icon.svg";
  static const String book_notebook = "$ICONS_SVG_PATH/book_notebook.svg";
  static const String HOMEWORK_ICON = "$ICONS_SVG_PATH/homework.svg";
  static const String current_date = '$ICONS_SVG_PATH/current-date.svg';
  static const String book = '$ICONS_SVG_PATH/book.svg';
  static const String lockIcon = '$ICONS_SVG_PATH/lock.svg';
  static const String checkIcon= '$ICONS_SVG_PATH/check_icon.svg';
  static const String qnquotedIcon= '$ICONS_SVG_PATH/qnquoted.svg';
  static const String clockIcon= '$ICONS_SVG_PATH/clock.svg';

  //============Home Screen ====================
  static const String CHART_ICON = "$ICONS_SVG_PATH/chart.svg";
  static const String AWARD_ICON = "$ICONS_SVG_PATH/award.svg";
  static const String CALENDER_REMOVE_ICON = "$ICONS_SVG_PATH/calendar-remove.svg";
  static const String TREND_UP_ICON = "$ICONS_SVG_PATH/trend-up.svg";
  static const String SCHOOL_ICON = "$ICONS_SVG_PATH/school-icon.svg";

  //============About Screen ====================
  static const String globe_icon = "$ICONS_SVG_PATH/globe.svg";
  //============Payment Screen ====================
  static const String one_peaple_icon = "$ICONS_SVG_PATH/frame.svg";
  static const String one_peaple_check_icon = "$ICONS_SVG_PATH/profile-tick.svg";
  static const String no_access_icon = "$ICONS_SVG_PATH/chart-fail.svg";
  static const String doucment_icon = "$ICONS_SVG_PATH/clipboard-text.svg";
  static const String doucment_check_icon = "$ICONS_SVG_PATH/clipboard-tick.svg";
  static const String doucment_check_icon_menu = "$ICONS_SVG_PATH/clipboard-tick-icon-menu-bar.svg";
  static const String access_icon = "$ICONS_SVG_PATH/chart-square.svg";

  // +++++++++++++++ IMAGE ++++++++++++++++ //
  static const String scan_qr_icon = "$SVG_PATH/scan-barcode.svg";
  static const String login_code_icon = '$SVG_PATH/login_code.svg';
  static const String app_logo_png = '$LOGO_PATH/camis_logo.png';
  static const String wing_png = '$ICONS_PATH/wing.png';
  static const String emoney_png = '$ICONS_PATH/emoney.png';
  static const String true_money_png = '$ICONS_PATH/true_money.png';

  static const String bus_background = '$SVG_PATH/bus_background_screen.svg';
  static const String food_background = '$SVG_PATH/food_background_sceren.svg';
  static const String health_background =
      '$SVG_PATH/health_background_screen.svg';

  // --------------- CLIP IMAGE -----------------camis-mobile-application/ //

  static const String login_background_clip =
      "$CLIP_PATH/login_background_clip.svg";
  static const String school_code_background =
      '$CLIP_PATH/school_code_background.svg';
  static const String scan_background = '$CLIP_PATH/scan_background_clip.svg';
}
