// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class DurationConstant {
  static const d200ms = Duration(milliseconds: 200);
  static const d300ms = Duration(milliseconds: 200);
  static const d500ms = Duration(milliseconds: 500);
  static const d1000ms = Duration(seconds: 1);
  static const d2000ms = Duration(seconds: 2);
  static const d3000ms = Duration(seconds: 3);
  static const d4000ms = Duration(seconds: 4);
  static const d5000ms = Duration(seconds: 5);
}

class Geometry {
  static const padding0 = EdgeInsets.symmetric(horizontal: 8.0);
  static const padding1 = EdgeInsets.symmetric(horizontal: 10.0);
  static const padding2 = EdgeInsets.symmetric(horizontal: 12.0);
  static const padding3 = EdgeInsets.symmetric(horizontal: 14.0);
  static const padding4 = EdgeInsets.symmetric(horizontal: 16.0);
  static const padding5 = EdgeInsets.symmetric(horizontal: 18.0);
  static const padding6 = EdgeInsets.symmetric(horizontal: 20.0);
  static const padding7 = EdgeInsets.symmetric(horizontal: 22.0);
  static const padding8 = EdgeInsets.symmetric(horizontal: 24.0);
  static const tableContentPadding = EdgeInsets.all(20);
  static const tableContentSimple = EdgeInsets.symmetric(
    horizontal: 30,
    vertical: 20,
  );
  static const paddingScreen = EdgeInsets.symmetric(horizontal: 18);
}

class StyleConstant {
  static BoxDecoration boxDecoration = BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.circular(8),
    boxShadow: [
      BoxShadow(
        offset: const Offset(1, 0),
        blurRadius: 14,
        color: Colors.black.withOpacity(0.15),
      ),
    ],
  );
}

class ConfigConstant {
  /// ```
  /// Margin 4.0
  /// ```
  static const double margin0 = 4.0;

  /// ```
  /// Margin 8.0
  /// ```
  static const double margin1 = 10.0;

  /// ```
  /// Margin 16.0
  /// ```
  static const double margin2 = 16.0;

  /// ```
  /// Margin 18.0
  /// ```
  static const double margin3 = 18.0;

  /// ```
  /// Margin 24.0
  /// ```
  static const double margin4 = 24.0;

  /// ```
  /// Margin 36.0
  /// ```
  static const double margin5 = 36.0;

  /// ```
  /// Margin 38.0
  /// ```
  static const double margin6 = 38.0;

  /// ```
  /// Margin 40.0
  /// ```
  static const double margin7 = 40.0;



  /////////// Constant Padding \\\\\\\\\\\\

  /// ```
  /// Padding 4.0
  /// ```
  static const double padding0 = 4.0;

  /// ```
  /// Padding 8.0
  /// ```
  static const double padding1 = 12.0;

  /// ```
  /// Padding 16.0
  /// ```
  static const double padding2 = 16.0;

  /// ```
  /// Padding 18.0
  /// ```
  static const double padding3 = 18.0;

  /// ```
  /// Padding 24.0
  /// ```
  static const double padding4 = 24;

  /// ```
  /// Padding 36.0
  /// ```
  static const double padding5 = 36;

  /// ```
  /// Padding 40.0
  /// ```
  static const double padding6 = 40;

  ///```
  /// Icon size 1 - 4
  /// ```
  static const double iconSize1 = 16.0;
  static const double iconSize2 = 18.0;
  static const double iconSize3 = 20.0;
  static const double iconSize4 = 24.0;
  static const double iconSize5 = 26.0;
  static const double iconSize6 = 28.0;
  static const double iconSize7 = 30.0;
  static const double iconSize8 = 32.0;
  static const double iconSize9 = 48.0;
  static const double iconSize10 = 56.0;
  static const double iconSize11 = 64.0;

  ///```
  ///objectHeight 1 - 7
  ///```
  static const double objectHeight1 = 48.0;
  static const double objectHeight2 = 56.0;
  static const double objectHeight3 = 64.0;
  static const double objectHeight4 = 72.0;
  static const double objectHeight5 = 96.0;
  static const double objectHeight6 = 120.0;
  static const double objectHeight7 = 240.0;

  /// ```
  /// radius 1 - 4;
  /// ```
  static const double radius1 = 4.0;
  static const double radius2 = 8.0;
  static const double radius3 = 15.0;
  static const double radius4 = 24.0;
  static const double radius5 = 20.0;
  static const double radius6 = 10.0;

  /// ```
  /// circlarRadius1 = BorderRadius.circular(4.0)
  /// ```
  static final BorderRadius circlarRadius1 = BorderRadius.circular(4.0);
  static final BorderRadius circlarRadius2 = BorderRadius.circular(10.0);

  /// ```
  /// circlarRadiusTop1 = BorderRadius.vertical(
  ///   top: Radius.circular(4.0),
  /// );
  /// ```
  static final BorderRadius circlarRadiusTop1 = BorderRadius.vertical(
    top: const Radius.circular(4.0),
  );

  /// ```
  /// circlarRadiusTop2 = BorderRadius.vertical(
  ///   top: Radius.circular(10.0),
  /// );
  /// ```

  static final BorderRadius circlarRadiusTop2 = BorderRadius.vertical(
    top: Radius.circular(10.0),
  );

  /// ```
  /// circlarRadiusBottom1 = BorderRadius.vertical(
  ///   top: Radius.circular(4.0),
  /// );
  /// ```
  static final BorderRadius circlarRadiusBottom1 = BorderRadius.vertical(
    top: Radius.circular(4.0),
  );

  /// ```
  /// circlarRadiusBottom2 = BorderRadius.vertical(
  ///   top: Radius.circular(10.0),
  /// );
  /// ```
  static final BorderRadius circlarRadiusBottom2 = BorderRadius.vertical(
    bottom: Radius.circular(10.0),
  );

  /// ```
  /// fadeDuration = const Duration(milliseconds: 250)
  /// ```
  static const Duration fadeDuration = Duration(milliseconds: 250);

  /// ```
  /// duration = const Duration(milliseconds: 350)
  /// ```
  static const Duration duration = Duration(milliseconds: 350);

  /// ```
  /// layoutPadding = const EdgeInsets.symmetric(
  ///   horizontal: margin2,
  ///   vertical: margin1,
  /// );
  /// ```
  static const EdgeInsets layoutPadding = EdgeInsets.symmetric(
    horizontal: margin2,
    vertical: margin1,
  );

  static final Border border = Border.all(
    width: 1,
    color: const Color(0xFFCECECE),
  );

  static final List<BoxShadow> boxShadows0 = [
    BoxShadow(
      blurRadius: 14,
      offset: const Offset(2.0, 0),
      color: const Color(0xFF7C7C7C).withOpacity(0.4),
    ),
  ];
}
