import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:url_launcher/url_launcher.dart';

import '../controller/contact_models.dart';

class ContactScreen extends StatefulWidget {
  const ContactScreen({ Key? key }) : super(key: key);

  @override
  State<ContactScreen> createState() => _ContactScreenState();
}

class _ContactScreenState extends State<ContactScreen> {

List<ContactModels> contacts = allcontact;

  Future<void> _launchLink(String url) async {
  if(await canLaunch(url)){
    await launch(url, forceWebView: false, forceSafariVC: false);
  }else{
    print('$url');
  }

}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: Stack(
        children: [
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: Container(
              alignment: Alignment.centerLeft,
              child: SizedBox(
                height: MediaQuery.of(context).size.height/1.1,
                width: MediaQuery.of(context).size.width,
                child: SvgPicture.asset("assets/images/svg/background_contact.svg",fit: BoxFit.fill,)),
            ),
          ),
          Column(
            children: [
              Container(
                margin: const EdgeInsets.only(top: 55),
                child: Row(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(left: 8),
                      child: IconButton(icon:const Icon(Icons.arrow_back_ios_new_outlined,size: 25,color: Colorconstands.white,), onPressed: () { Navigator.of(context).pop(); },),
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 8),
                      child: Align(
                        child: Text("Contact Us",style: ThemeConstands.texttheme.headline5!.copyWith(color: Colorconstands.white)),
                      )
                    )
                  ],
                ),
              ),
              Container(
                padding:const EdgeInsets.all(8),
                margin: const EdgeInsets.only(top: 25),
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width/4,
                height: MediaQuery.of(context).size.width/4,
                decoration: BoxDecoration(
                  color: Colorconstands.white.withOpacity(0.06),
                  shape: BoxShape.circle
                ),
                child: Container(
                  padding:const EdgeInsets.all(20),
                  decoration:const BoxDecoration(
                    color: Colorconstands.secondaryColor,
                    shape: BoxShape.circle,
                  ),
                  child: SvgPicture.asset("assets/icons/contact_icon.svg"),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 18,bottom: 35),
                child: Column(
                  children: [
                    Container(
                      alignment: Alignment.center,
                      margin: const EdgeInsets.only(top: 8,left: 17.0,right: 17.0),
                      child: Align(
                        child: Text("Need Help?",style: ThemeConstands.texttheme.headline6!.copyWith(color: Colorconstands.white)),
                      )
                    ),
                    Container(
                      alignment: Alignment.center,
                      margin: const EdgeInsets.only(top: 8,left: 17.0,right: 17.0),
                      child: Align(
                        child: Text("Contact us by selecting any channel you prefer below",style: ThemeConstands.texttheme.subtitle2!.copyWith(color: Colorconstands.white),),
                      )
                    )
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  padding: const EdgeInsets.only(top: 17),
                  decoration:const BoxDecoration(
                    color: Colorconstands.white,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0),topRight: Radius.circular(15.0))
                  ),
                    child: ListView.builder(
                    padding:const EdgeInsets.all(0),
                    itemCount: contacts.length,
                    itemBuilder: (context, index) {
                      final contact= contacts[index];
                      return Container(
                        decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                          color: Colorconstands.darkGray.withOpacity(0.1),
                          width: 2,
                          )
                        )
                      ),
                      height: 85,
                        child: MaterialButton(
                          padding:const EdgeInsets.all(0),
                          onPressed: () {
                            setState(() {
                              _launchLink(contact.link);
                            });
                          },
                          child: Container(
                            margin:const EdgeInsets.only(left: 20.0),
                            child: Row(
                              children: [
                                Container(
                                  height: 55,
                                  width: 55,
                                  child: ClipOval(child: Image.network(contact.image,fit: BoxFit.cover,)),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 25,right: 17.0),
                                  height: 60,
                                  width: 2,
                                  color: Colorconstands.darkGray.withOpacity(0.1),
                                ),
                                Expanded(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Align(
                                        alignment: Alignment.centerLeft,
                                        child: Text(contact.title,style: ThemeConstands.texttheme.headline6!.copyWith(color: Colorconstands.primaryColor,fontWeight: FontWeight.bold),),
                                      ),
                                       Align(
                                         alignment: Alignment.centerLeft,
                                        child: Text(contact.subtitle),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.centerRight,
                                  margin: const EdgeInsets.only(right: 14,left: 17.0),
                                  child:const Icon(Icons.arrow_forward_ios_outlined,size: 25,),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    }
                  ),
                ),
              ),
            ],
          )
        ],
      ),
      
    );
  }
}