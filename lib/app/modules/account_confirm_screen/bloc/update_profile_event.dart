part of 'update_profile_bloc.dart';
abstract class UpdateProfileEvent extends Equatable {
  const UpdateProfileEvent();

  @override
  List<Object> get props => [];
}

class UpdateProfileUserEvent extends UpdateProfileEvent{
   final File? imageFile;
   const UpdateProfileUserEvent({this.imageFile});
}
