import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:camis_application_flutter/service/apis/profile_user/edit_profile_user_api.dart';
import 'package:equatable/equatable.dart';

import '../../../../model/user_profile/update_model.dart';

part 'update_profile_event.dart';
part 'update_profile_state.dart';

class UpdateProfileBloc extends Bloc<UpdateProfileEvent, UpdateProfileState> {
  final UpdateProfileUserApi upDateProfile;
  UpdateProfileBloc({required this.upDateProfile}) : super(UpdateProfileInitial()) {
    on<UpdateProfileUserEvent>((event, emit) async {
      emit(UpdateProfileLoading());
      try{
         var data = await upDateProfile.updateUserProfileImageRequestApi(image: event.imageFile!);
         emit(UpdateProfileLoaded(infoModel: data));
      }
      catch (e){
        emit(UpdateProfileError());
        print(e);
      }
    });
  }
}
