part of 'update_profile_bloc.dart';

abstract class UpdateProfileState extends Equatable {
  const UpdateProfileState();
  
  @override
  List<Object> get props => [];
}

class UpdateProfileInitial extends UpdateProfileState {}

class UpdateProfileLoading extends UpdateProfileState {}
class UpdateProfileLoaded extends UpdateProfileState {
  final UpdateUserModel infoModel;
  const UpdateProfileLoaded({required this.infoModel});
}
class UpdateProfileError extends UpdateProfileState {}
