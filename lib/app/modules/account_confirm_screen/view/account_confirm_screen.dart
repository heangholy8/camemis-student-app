import 'dart:async';
import 'dart:io';
import 'package:camis_application_flutter/app/modules/account_confirm_screen/bloc/update_profile_bloc.dart';
import 'package:camis_application_flutter/app/modules/auth_screens/e_school_code.dart';
import 'package:camis_application_flutter/model/account_confrim_model/account_confrim_model.dart';
import 'package:camis_application_flutter/storages/get_storage.dart';
import 'package:camis_application_flutter/storages/save_storage.dart';
import 'package:camis_application_flutter/widgets/button_widget/button_widget_custom.dart';
import 'package:camis_application_flutter/widgets/take_upload_image.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:image_picker/image_picker.dart';
import '../../../../service/apis/profile_user/bloc/get_profile_user_bloc.dart';
import '../../../../service/apis/profile_user/edit_profile_user_api.dart';
import '../../../../widgets/no_connection_alert_widget.dart';
import '../../../routes/app_routes.dart';

class AccountConfirmScreen extends StatefulWidget {
  const AccountConfirmScreen({Key? key}) : super(key: key);

  @override
  State<AccountConfirmScreen> createState() => _AccountConfirmScreenState();
}

class _AccountConfirmScreenState extends State<AccountConfirmScreen>
    with Toast {
  GetStoragePref _getStoragePref = GetStoragePref();
  SaveStoragePref _saveStorage = SaveStoragePref();
  final UpdateProfileUserApi updateinfo = UpdateProfileUserApi();
  final guardinaConfirmAccountModel = GuardinaConfirmAccountModel.generate();
  TextEditingController fullnameteController = TextEditingController();
  final roleGuardianteController = TextEditingController();
  late final List<TextEditingController> nameChildController;
  late final List<TextEditingController> gradeChildController;
  final _formKey = GlobalKey<FormState>();
  final ImagePicker _picker = ImagePicker();
  final scrollController = ScrollController();
  File? _image;
  XFile? file;
  bool checkfocusfullname = false;
  bool checkfocusroleguardian = false;
  bool loadingcontinu = false;
  String? phoneNumber;
  bool connection = true;
  StreamSubscription? sub;

  void getPhoneStore() async {
    var getPhone = await _getStoragePref.getPhoneNumber();
    setState(() {
      phoneNumber = getPhone;
    });
  }

  @override
  void initState() {
    setState(() {
      //=============Check internet====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          connection = (event != ConnectivityResult.none);
        });
      });
      //=============Eend Check internet====================
    });
    getPhoneStore();
    BlocProvider.of<GetProfileUserBloc>(context).add(GetProfileUser());
    super.initState();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: WillPopScope(
        onWillPop: () => exit(0),
        child: Stack(
          children: [
            Positioned(
              top: 0,
              right: 0,
              child: Image.asset(
                "assets/images/Oval.png",
                width: MediaQuery.of(context).size.width / 1.7,
              ),
            ),
            Positioned(
              top: 150,
              left: 0,
              child: Image.asset(
                "assets/images/Oval (1).png",
              ),
            ),
            Positioned(
              bottom: 35,
              left: 0,
              child: Image.asset(
                "assets/images/Oval (3).png",
              ),
            ),
            Positioned(
              bottom: MediaQuery.of(context).size.height / 5,
              right: MediaQuery.of(context).size.width / 4,
              child: Center(
                  child: Image.asset(
                "assets/images/Oval (2).png",
              )),
            ),
            BlocListener<UpdateProfileBloc, UpdateProfileState>(
              listener: (context, state) {
                if (state is UpdateProfileLoading) {
                  setState(() {
                    loadingcontinu = true;
                  });
                } else if (state is UpdateProfileLoaded) {
                  setState(() {
                    loadingcontinu = false;
                    Navigator.pushNamedAndRemoveUntil(context,
                        Routes.HOMESCREEN, (Route<dynamic> route) => false);
                  });
                } else if (state is UpdateProfileError) {
                  setState(() {
                    loadingcontinu = false;
                  });
                } else {
                  setState(() {
                    loadingcontinu = false;
                    showErrorDialog(() {
                      Navigator.of(context).pop();
                    }, context);
                  });
                }
              },
              child: Container(
                margin: const EdgeInsets.only(
                  top: 70,
                ),
                alignment: Alignment.center,
                child: Column(
                  children: [
                    /* ----------------------- Head body ---------------------------------- */
                    Container(
                      child: Container(
                        margin: const EdgeInsets.only(
                          left: 18,
                        ),
                        child: Text(
                          'INFORMATIONVERIFICATION'.tr(),
                          style: ThemeConstands.headline2_SemiBold_24
                              .copyWith(color: Colorconstands.neutralWhite),
                        ),
                      ),
                    ),
                    Container(
                      margin:
                          const EdgeInsets.only(top: 28, left: 22, right: 22),
                      child: RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                              style: const TextStyle(
                                color: Colorconstands.neutralWhite,
                              ), //style for all textspan
                              children: [
                                TextSpan(
                                    text: 'WELCOM'.tr(),
                                    style: ThemeConstands.subtitle1_Regular_16),
                                TextSpan(
                                  text: "CAMEMIS",
                                  style: ThemeConstands.subtitle1_Regular_16
                                      .copyWith(
                                          color: Colorconstands.neutralWhite,
                                          fontWeight: FontWeight.bold),
                                ),
                                const TextSpan(text: "\n"),
                                TextSpan(
                                    text: 'PLEASEVERIFY'.tr(),
                                    style: ThemeConstands.subtitle1_Regular_16),
                              ])),
                    ),
                    /* ----------------------- body ---------------------------------- */
                    Expanded(
                      child: Container(
                          margin: const EdgeInsets.only(top: 30),
                          decoration: const BoxDecoration(
                            color: Colorconstands.neutralSecondary,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(12.0),
                                topRight: Radius.circular(12.0)),
                          ),
                          // child: BlocBuilder<ChangeChildBloc, ChangeChildState>(
                          //   builder: (context, state) {
                          //     if (state is ChangeChildLoadingState) {
                          //       return const Center(
                          //         child: CircularProgressIndicator(),
                          //       );
                          //     } else if (state is ChangeChildLoadState) {
                          //       var dataChild = state.childrenModel!.data;
                                child: BlocBuilder<GetProfileUserBloc,
                                    GetProfileUserState>(
                                  builder: (context, state) {
                                    if (state is ProfileUserLoadingState) {
                                      return const Center(
                                        child: CircularProgressIndicator(),
                                      );
                                    } else if (state is ProfileUserLoaded) {
                                      var dataProfile = state.profileusermodel.data;
                                      fullnameteController.text =
                                          "${dataProfile!.firstname.toString()} ${dataProfile.lastname.toString()}";
                                      roleGuardianteController.text =
                                          "STUDENT".tr();
                                      return ListView(
                                        shrinkWrap: true,
                                        controller: scrollController,
                                        physics: const ClampingScrollPhysics(),
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 24.0),
                                        children: [
                                          /* ----------------------- Profile Icon ---------------------------------- */
                                          GestureDetector(
                                            onTap: () {
                                              setState(
                                                () {
                                                  showModalBottomSheet(
                                                    backgroundColor:
                                                        Colors.transparent,
                                                    context: context,
                                                    builder: (BuildContext bc) {
                                                      return ShowPiker(
                                                        onPressedCamera: () {
                                                          _imgFromCamera();
                                                          Navigator.of(context)
                                                              .pop();
                                                        },
                                                        onPressedGalary: () {
                                                          _imgFromGallery();
                                                          Navigator.of(context)
                                                              .pop();
                                                        },
                                                      );
                                                    },
                                                  );
                                                },
                                              );
                                            },
                                            child: Container(
                                                width: 80,
                                                height: 80,
                                                margin: const EdgeInsets.only(
                                                    top: 25, bottom: 0),
                                                alignment: Alignment.center,
                                                child: Stack(
                                                  alignment: Alignment.center,
                                                  children: [
                                                    CircleAvatar(
                                                      radius:
                                                          40, // Image radius
                                                      child: ClipOval(
                                                        child: _image == null
                                                            ? Image.network(
                                                                dataProfile
                                                                    .profileMedia!
                                                                    .fileShow
                                                                    .toString(),
                                                                width: 80,
                                                                height: 80,
                                                                fit: BoxFit
                                                                    .cover,
                                                              )
                                                            : Image.file(
                                                                _image!,
                                                                width: 80,
                                                                height: 80,
                                                                fit: BoxFit
                                                                    .cover,
                                                              ),
                                                      ),
                                                    ),
                                                    _image == null
                                                        ? Container(
                                                            width: 80,
                                                            height: 80,
                                                            decoration:
                                                                const BoxDecoration(
                                                                    shape: BoxShape
                                                                        .circle,
                                                                    color: Colors
                                                                        .black12),
                                                          )
                                                        : const SizedBox(),
                                                    _image == null
                                                        ? const Center(
                                                            child: Icon(
                                                              Icons
                                                                  .photo_camera_outlined,
                                                              color: Colorconstands
                                                                  .neutralWhite,
                                                              size: 28,
                                                            ),
                                                          )
                                                        : const SizedBox()
                                                  ],
                                                )),
                                          ),
                                          Container(
                                            margin: const EdgeInsets.only(
                                                bottom: 10),
                                            child: TextButton(
                                              onPressed: () {
                                                setState(
                                                  () {
                                                    showModalBottomSheet(
                                                      backgroundColor:
                                                          Colors.transparent,
                                                      context: context,
                                                      builder:
                                                          (BuildContext bc) {
                                                        return ShowPiker(
                                                          onPressedCamera: () {
                                                            _imgFromCamera();
                                                            Navigator.of(
                                                                    context)
                                                                .pop();
                                                          },
                                                          onPressedGalary: () {
                                                            _imgFromGallery();
                                                            Navigator.of(
                                                                    context)
                                                                .pop();
                                                          },
                                                        );
                                                      },
                                                    );
                                                  },
                                                );
                                              },
                                              child: Text('UPLOADPHOTO'.tr(),
                                                  style: ThemeConstands
                                                      .headline6_Medium_14
                                                      .copyWith(
                                                    color: Colorconstands
                                                        .primaryColor,
                                                    decoration: TextDecoration
                                                        .underline,
                                                  )),
                                            ),
                                          ),
                                          /* ----------------------- Guardian Name ---------------------------------- */
                                          Container(
                                            padding: EdgeInsets.zero,
                                            decoration: BoxDecoration(
                                              color: Colorconstands.lightGohan,
                                              borderRadius:
                                                  BorderRadius.circular(12),
                                              border: Border.all(
                                                  color: checkfocusfullname ==
                                                          false
                                                      ? Colorconstands
                                                          .neutralGrey
                                                      : Colorconstands
                                                          .primaryColor,
                                                  width: checkfocusfullname ==
                                                          false
                                                      ? 1
                                                      : 2),
                                            ),
                                            child: Focus(
                                              onFocusChange: (hasfocus) {
                                                setState(() {
                                                  hasfocus
                                                      ? checkfocusfullname =
                                                          true
                                                      : checkfocusfullname =
                                                          false;
                                                });
                                              },
                                              child: TextFormField(
                                                enabled: false,
                                                controller:
                                                    fullnameteController,
                                                style: ThemeConstands
                                                    .subtitle1_Regular_16,
                                                decoration: InputDecoration(
                                                  border: UnderlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              16.0)),
                                                  contentPadding:
                                                      const EdgeInsets.only(
                                                          top: 8,
                                                          bottom: 5,
                                                          left: 18,
                                                          right: 18),
                                                  labelText: 'FNAME'.tr(),
                                                  labelStyle: ThemeConstands
                                                      .subtitle1_Regular_16
                                                      .copyWith(
                                                          color: Colorconstands
                                                              .lightTrunks),
                                                  enabledBorder:
                                                      const UnderlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color: Colors
                                                                  .transparent)),
                                                  focusedBorder:
                                                      const UnderlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color: Colors
                                                                  .transparent)),
                                                ),
                                              ),
                                            ),
                                          ),
                                          /* ----------------------- Guardian Position ---------------------------------- */
                                          Container(
                                            margin:
                                                const EdgeInsets.only(top: 16),
                                            padding: EdgeInsets.zero,
                                            decoration: BoxDecoration(
                                              color: Colorconstands.lightGohan,
                                              borderRadius:
                                                  BorderRadius.circular(12),
                                              border: Border.all(
                                                  color:
                                                      checkfocusroleguardian ==
                                                              false
                                                          ? Colorconstands
                                                              .neutralGrey
                                                          : Colorconstands
                                                              .primaryColor,
                                                  width:
                                                      checkfocusroleguardian ==
                                                              false
                                                          ? 1
                                                          : 2),
                                            ),
                                            child: Focus(
                                              onFocusChange: (hasfocus) {
                                                setState(() {
                                                  hasfocus
                                                      ? checkfocusroleguardian =
                                                          true
                                                      : checkfocusroleguardian =
                                                          false;
                                                });
                                              },
                                              child: TextFormField(
                                                enabled: false,
                                                controller:
                                                    roleGuardianteController,
                                                onChanged: ((value) {
                                                  setState(() {
                                                    roleGuardianteController
                                                        .text;
                                                  });
                                                }),
                                                style: ThemeConstands
                                                    .subtitle1_Regular_16,
                                                decoration: InputDecoration(
                                                  border: UnderlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              16.0)),
                                                  contentPadding:
                                                      const EdgeInsets.only(
                                                          top: 8,
                                                          bottom: 5,
                                                          left: 18,
                                                          right: 18),
                                                  labelText: 'RELATION'.tr(),
                                                  labelStyle: ThemeConstands
                                                      .subtitle1_Regular_16
                                                      .copyWith(
                                                          color: Colorconstands
                                                              .lightTrunks),
                                                  enabledBorder:
                                                      const UnderlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color: Colors
                                                                  .transparent)),
                                                  focusedBorder:
                                                      const UnderlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color: Colors
                                                                  .transparent)),
                                                ),
                                              ),
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 16.0,
                                          ),
                                          /* ----------------------- List Children ---------------------------------- */
                                          const SizedBox(
                                            height: 80,
                                          )
                                        ],
                                      );
                                    } else if (state is ProfileUserErrorState) {
                                      return const Center(
                                        child: Text("Error Data"),
                                      );
                                    } else {
                                      return const Center(
                                        child: CircularProgressIndicator(),
                                      );
                                    }
                                  },
                                ),
                        ),
                    ),
                  ],
                ),
              ),
            ),
            /* ----------------------- Button Next ---------------------------------- */
            connection == false
                ? Container(
                    height: 0,
                  )
                : Positioned(
                    top: MediaQuery.of(context).size.height - 60,
                    left: 24,
                    right: 24,
                    bottom: 10,
                    child: ButtonWidgetCustom(
                      heightButton: 60,
                      buttonColor: Colorconstands.primaryColor,
                      onTap: () {
                        if (_image != null) {
                          BlocProvider.of<UpdateProfileBloc>(context)
                              .add(UpdateProfileUserEvent(imageFile: _image));
                          _saveStorage.saveComfrimProfileSuccess(comfrimProfile: "ComfrimSecuess");
                          // if(phoneNumber !="088123123"){
                          //   updateinfo.updateUserPhoneRequestApi(
                          //     phone: phoneNumber.toString());
                          // }
                        } else {
                          // if(phoneNumber !="088123123"){
                          //   updateinfo.updateUserPhoneRequestApi(
                          //     phone: phoneNumber.toString());
                          // }
                          _saveStorage.saveComfrimProfileSuccess(comfrimProfile: "ComfrimSecuess");
                          Navigator.pushNamedAndRemoveUntil(
                              context,
                              Routes.HOMESCREEN,
                              (Route<dynamic> route) => false);
                        }
                      },
                      panddinHorButton: 22,
                      panddingVerButton: 12,
                      radiusButton: 12,
                      textStyleButton: ThemeConstands.button_SemiBold_16
                          .copyWith(color: Colorconstands.neutralWhite),
                      title: 'CONTINUE'.tr(),
                    ),
                  ),
            loadingcontinu == false
                ? Container()
                : Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    top: 0,
                    child: Container(
                      color: const Color(0x7B9C9595),
                      child: Center(
                        child: Container(
                          padding: const EdgeInsets.all(16),
                          height: 60,
                          width: 60,
                          decoration: BoxDecoration(
                            color: Colorconstands.neutralGrey,
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: const Center(
                            child: CircularProgressIndicator(),
                          ),
                        ),
                      ),
                    ),
                  ),
            AnimatedPositioned(
              bottom: connection == true ? -150 : 0,
              left: 0,
              right: 0,
              duration: const Duration(milliseconds: 500),
              child: const NoConnectWidget(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _childInformationWidget({
    required String index,
    required int indexList,
    required TextEditingController nameChildcontroller,
    required TextEditingController gradeChildController,
  }) {
    bool nameChildCheckFocus = false;
    bool gradeChildCheckFocus = false;
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Row(
        children: [
          Expanded(
            child: Container(
              padding: EdgeInsets.zero,
              decoration: BoxDecoration(
                color: Colorconstands.lightGohan,
                borderRadius: BorderRadius.circular(12),
                border: Border.all(
                    color: nameChildCheckFocus == false
                        ? Colorconstands.neutralGrey
                        : Colorconstands.primaryColor,
                    width: nameChildCheckFocus == false ? 1 : 2),
              ),
              child: Focus(
                onFocusChange: (hasfocus) {
                  setState(() {
                    hasfocus
                        ? nameChildCheckFocus = true
                        : nameChildCheckFocus = false;
                  });
                },
                child: TextFormField(
                  enabled: false,
                  controller: nameChildcontroller,
                  style: ThemeConstands.subtitle1_Regular_16,
                  decoration: InputDecoration(
                    border: UnderlineInputBorder(
                        borderRadius: BorderRadius.circular(16.0)),
                    contentPadding: const EdgeInsets.only(
                        top: 8, bottom: 5, left: 18, right: 18),
                    labelText: "${'CHILD'.tr()} $index",
                    labelStyle: ThemeConstands.subtitle1_Regular_16
                        .copyWith(color: Colorconstands.lightTrunks),
                    enabledBorder: const UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent)),
                    focusedBorder: const UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent)),
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(
            width: 16,
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.zero,
              decoration: BoxDecoration(
                color: Colorconstands.lightGohan,
                borderRadius: BorderRadius.circular(12),
                border: Border.all(
                    color: gradeChildCheckFocus == false
                        ? Colorconstands.neutralGrey
                        : Colorconstands.primaryColor,
                    width: gradeChildCheckFocus == false ? 1 : 2),
              ),
              child: Focus(
                onFocusChange: (hasfocus) {
                  setState(() {
                    hasfocus
                        ? gradeChildCheckFocus = true
                        : gradeChildCheckFocus = false;
                  });
                },
                child: TextFormField(
                  enabled: false,
                  controller: gradeChildController,
                  style: ThemeConstands.subtitle1_Regular_16,
                  decoration: InputDecoration(
                    border: UnderlineInputBorder(
                        borderRadius: BorderRadius.circular(16.0)),
                    contentPadding: const EdgeInsets.only(
                        top: 8, bottom: 5, left: 18, right: 18),
                    labelText: 'CLASS'.tr(),
                    labelStyle: ThemeConstands.subtitle1_Regular_16
                        .copyWith(color: Colorconstands.lightTrunks),
                    enabledBorder: const UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent)),
                    focusedBorder: const UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent)),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _imgFromCamera() async {
    XFile? pickedFile = await _picker.pickImage(source: ImageSource.camera);
    setState(() {
      _image = File(pickedFile!.path);
      file = pickedFile;
    });
  }

  void _imgFromGallery() async {
    XFile? pickedFile = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = File(pickedFile!.path);
      file = pickedFile;
      print("path-path " + file!.path.toString());
    });
  }
}
