import 'dart:async';
import 'package:camis_application_flutter/app/modules/time_line/school_time_line/e.schooltimeline.dart';
import 'package:camis_application_flutter/app/modules/time_line/testing_pagination/bloc/pagination_bloc.dart';
import 'package:flutter/material.dart';
import '../widget/thumbnail_doc_widget.dart';
import '../widget/thumbnail_widget.dart';
import '../widget/view_image.dart';

class PaginationScreenTesting extends StatefulWidget {
  const PaginationScreenTesting({Key? key}) : super(key: key);

  @override
  State<PaginationScreenTesting> createState() =>
      _PaginationScreenTestingState();
}

class _PaginationScreenTestingState extends State<PaginationScreenTesting> {
  late ScrollController _scrollController;
  int? isClick;
  final _refreshKey = GlobalKey<RefreshIndicatorState>();

  bool isReachedMax = false;

  bool isRefreshLoading = false;

  int maxImage = 4;
  int? remaining;
  int? numImages;
  String? reaction;
  bool connection = true;
  StreamSubscription? stsub;
  //final dataTimeLine = TimeLineModel.generate();
  String? discription;
  String reactiondefultselect =
      "assets/images/actionButton/icon_like_circle.png";

  @override
  void initState() {
    _scrollController = ScrollController(keepScrollOffset: true)
      ..addListener(onScroll);
    super.initState();
    BlocProvider.of<PaginationBloc>(context).loadedData(reset: false);
  }

  void onScroll() {
    if (_scrollController.position.atEdge) {
      if (_scrollController.position.pixels != 0 && isReachedMax == false) {
        BlocProvider.of<PaginationBloc>(context).loadedData(reset: false);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back),
        ),
        title: const Text("sdf"),
      ),
      body: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              IconButton(
                onPressed: () {
                  BlocProvider.of<PaginationBloc>(context)
                      .loadedData(reset: true);
                },
                icon: Icon(Icons.home),
              ),
              IconButton(
                onPressed: () {},
                icon: Icon(Icons.home),
              ),
            ],
          ),
          Expanded(
            child: BlocBuilder<PaginationBloc, PaginationState>(
              builder: (context, state) {
                List<Datum> data = [];
                bool isLoading = false;
                if (isRefreshLoading == false) {
                  if (state is PaginationLoadingState) {
                    data = state.oldData;
                    isLoading = true;
                  } else if (state is PaginationSuccessState) {
                    data = state.data;
                    isReachedMax = state.hasReachedMax;
                    debugPrint("hasdhafdasdfsdfFul $isReachedMax");
                  }
                  if (state is PaginationLoadingState && state.isFirstFetch) {
                    return Column(
                      children: [
                        Expanded(
                          child: Container(
                            color: Colorconstands.neutralWhite,
                            child: ListView.builder(
                              shrinkWrap: true,
                              itemBuilder: (BuildContext contex, index) {
                                return const ShimmerTimeLine();
                              },
                              itemCount: 3,
                            ),
                          ),
                        ),
                      ],
                    );
                  }
                  return RefreshIndicator(
                    key: _refreshKey,
                    onRefresh: () async {
                      // setState(() {
                      //   data = [];
                      // });
                      // BlocProvider.of<PaginationBloc>(context).page = 1;
                      BlocProvider.of<PaginationBloc>(context)
                          .loadedData(reset: true);
                    },
                    child: ListView.builder(
                      controller: _scrollController,
                      itemCount: data.length +
                          (isLoading ? 1 : 0) +
                          (isReachedMax == true ? 1 : 0),
                      itemBuilder: (context, index) {
                        if (index < data.length) {
                          String? profileusercomment;
                          String? nameusercomment;
                          String? commenttext;
                          var applications = data[index]
                              .attachments!
                              .where((element) =>
                                  element.fileType!.contains("application"))
                              .toList();
                          var audio = data[index]
                              .attachments!
                              .where((element) =>
                                  element.fileType!.contains("audio"))
                              .toList();
                          var listImageVideo =
                              data[index].attachments!.where((element) {
                            return element.fileType!.contains("image") ||
                                element.fileType!.contains("video");
                          }).toList();

                          var imageandvideo = listImageVideo.length;
                          if (data[index].comments!.isNotEmpty) {
                            profileusercomment = data[index]
                                .comments![0]
                                .user!
                                .profileMedia!
                                .fileThumbnail;
                            nameusercomment = translate == "en"
                                ? data[index].comments![0].user!.nameEn ==
                                            " " ||
                                        data[index].comments![0].user!.nameEn ==
                                            ""
                                    ? data[index].comments![0].user!.name
                                    : data[index].comments![0].user!.nameEn
                                : data[index].comments![0].user!.name;
                            commenttext = data[index].comments![0].postText;
                          }
                          discription = data[index].postText.toString();
                          int indexSelected = index;
                          int selecticonreact = 5;
                          numImages = imageandvideo;
                          return Timeline_widget(
                            commenttext: commenttext.toString(),
                            commentprofile: profileusercomment,
                            nameusercomment: nameusercomment,
                            totalReactType: data[index].totalReactType!.length,
                            comments: data[index].commentCount!,
                            reaction: "React",
                            totalLike: data[index].totalReact,
                            datePost: data[index].createdDate,
                            name: translate == "en"
                                ? data[index].user!.nameEn == " " ||
                                        data[index].user!.nameEn == " "
                                    ? data[index].user!.name
                                    : data[index].user!.nameEn
                                : data[index].user!.name,
                            //------------------------- Rect type all--------------------------
                            imageReact2: data[index].totalReactType!.isEmpty
                                ? reactiondefultselect
                                : data[index].totalReactType![0].reactionType ==
                                        0
                                    ? "assets/images/actionButton/icon_like_circle.png"
                                    : data[index]
                                                .totalReactType![0]
                                                .reactionType ==
                                            1
                                        ? "assets/images/actionButton/Love.png"
                                        : data[index]
                                                    .totalReactType![0]
                                                    .reactionType ==
                                                2
                                            ? "assets/images/actionButton/Icon_haha.png"
                                            : data[index]
                                                        .totalReactType![0]
                                                        .reactionType ==
                                                    3
                                                ? "assets/images/actionButton/wow2.png"
                                                : data[index]
                                                            .totalReactType![0]
                                                            .reactionType ==
                                                        5
                                                    ? "assets/images/actionButton/angry2.png"
                                                    : "assets/images/actionButton/icon_like_circle.png",
                            imageReact1: data[index].totalReactType!.length <= 1
                                ? "assets/images/actionButton/icon_like_circle.png"
                                : data[index].totalReactType![1].reactionType ==
                                        0
                                    ? "assets/images/actionButton/icon_like_circle.png"
                                    : data[index]
                                                .totalReactType![1]
                                                .reactionType ==
                                            1
                                        ? "assets/images/actionButton/Love.png"
                                        : data[index]
                                                    .totalReactType![1]
                                                    .reactionType ==
                                                2
                                            ? "assets/images/actionButton/Icon_haha.png"
                                            : data[index]
                                                        .totalReactType![1]
                                                        .reactionType ==
                                                    3
                                                ? "assets/images/actionButton/wow2.png"
                                                : data[index]
                                                            .totalReactType![1]
                                                            .reactionType ==
                                                        5
                                                    ? "assets/images/actionButton/angry2.png"
                                                    : "assets/images/actionButton/icon_like_circle.png",
                            // imageReactselectdefoult:selecticonreact==0?"assets/images/actionButton/icon_like_circle.png"
                            // :selecticonreact==1?"assets/images/actionButton/Love.png":"assets/images/actionButton/wow2.png",

                            //-------------------------End Rect type all--------------------------

                            //------------------------- color Label React--------------------------
                            //colorlabelreact: Colorconstands.primaryColor,
                            colorlabelreact: data[index].isReact == 0
                                ? Colorconstands.primaryColor
                                : data[index].reactionType == 0
                                    ? Colorconstands.primaryColor
                                    : data[index].reactionType == 1
                                        ? Colors.red
                                        : data[index].reactionType == 2
                                            ? Colors.yellow
                                            : data[index].reactionType == 5
                                                ? Colors.red
                                                : data[index].reactionType == 6
                                                    ? Colorconstands
                                                        .primaryColor
                                                    : Colors.yellow,
                            //-------------------------End color Label React--------------------------

                            pathImageReact: data[index].isReact == 0
                                ? "assets/icons/like_icon.png"
                                : data[index].reactionType == 0
                                    ? "assets/images/actionButton/like_full_fill.png"
                                    : data[index].reactionType == 1
                                        ? "assets/images/actionButton/Love.png"
                                        : data[index].reactionType == 2
                                            ? "assets/images/actionButton/Icon_haha.png"
                                            : data[index].reactionType == 3
                                                ? "assets/images/actionButton/wow2.png"
                                                : data[index].reactionType == 5
                                                    ? "assets/images/actionButton/angry2.png"
                                                    : "assets/icons/like_icon.png",
                            textReact: data[index].isReact == 0
                                ? "Like"
                                : data[index].reactionType == 0
                                    ? "Like"
                                    : data[index].reactionType == 1
                                        ? "Love"
                                        : data[index].reactionType == 3
                                            ? "Wow"
                                            : data[index].reactionType == 2
                                                ? "haha"
                                                : data[index].reactionType == 5
                                                    ? "angry"
                                                    : "Like",
                            selectedReaction: selecticonreact,

                            //------------------------- Profile image--------------------------
                            imageProfile:
                                data[index].user!.profileMedia!.fileThumbnail ??
                                    data[index].user!.profileMedia!.fileShow,
                            //-------------------------End Profile image--------------------------

                            //------------------------Text Post------------------
                            discriptionChild: Container(
                              alignment: Alignment.centerLeft,
                              margin: const EdgeInsets.only(
                                  left: 22.0,
                                  right: 22.0,
                                  top: 15.0,
                                  bottom: 12.0),
                              child: ExpandableText(
                                discription == null ||
                                        discription == "" ||
                                        discription!.isEmpty
                                    ? ""
                                    : discription!,
                                expandText: 'show more',
                                collapseText: 'show less',
                                maxLines: 2,
                                style: const TextStyle(fontSize: 16.0),
                                linkColor: Colorconstands.primaryColor,
                              ),
                            ),
                            //------------------------End Text Post------------------

                            //------------------------Void---------------------
                            displayVoice: Container(),
                            //------------------------End Void------------------

                            //---------------View Center Body--------------------------
                            viewChild: numImages == 1
                                ? Stack(
                                    children: List.generate(
                                        listImageVideo.length, (subindex) {
                                      var attachmentUrl =
                                          listImageVideo[subindex]
                                              .fileThumbnail;
                                      var attachmentType =
                                          listImageVideo[subindex].fileType;
                                      var url =
                                          listImageVideo[subindex].fileShow;
                                      var tilte =
                                          listImageVideo[subindex].fileName;
                                      return listImageVideo.length < 0
                                          ? Container()
                                          : GestureDetector(
                                              child: attachmentType!
                                                      .contains("image")
                                                  ? GestureDetector(
                                                      onTap: connection == false
                                                          ? null
                                                          : () {
                                                              Navigator.push(
                                                                context,
                                                                PageRouteBuilder(
                                                                  pageBuilder: (_,
                                                                          __,
                                                                          ___) =>
                                                                      ImageViewDownloads(
                                                                    listimagevide:
                                                                        listImageVideo,
                                                                    activepage:
                                                                        0,
                                                                  ),
                                                                  transitionDuration:
                                                                      const Duration(
                                                                          seconds:
                                                                              0),
                                                                ),
                                                              );
                                                            },
                                                      child: CachedImageNetwork(
                                                          urlImage:
                                                              attachmentUrl!),
                                                    )
                                                  : attachmentType
                                                          .contains("video")
                                                      ? ThumbnailVideoWidget(
                                                          image: attachmentUrl,
                                                          onPressed:
                                                              connection ==
                                                                      false
                                                                  ? null
                                                                  : () {
                                                                      setState(
                                                                          () {
                                                                        Navigator.of(context).push(MaterialPageRoute(
                                                                            builder: (
                                                                          context,
                                                                        ) =>
                                                                                VideoPlayerWidget(
                                                                                  linkVideo: url,
                                                                                )));
                                                                      });
                                                                    })
                                                      : Container());
                                    }),
                                  )
                                : numImages == 3
                                    ? Stack(
                                        children: List.generate(
                                            min(listImageVideo.length,
                                                maxImage), (subindex) {
                                          var attachmentUrl =
                                              listImageVideo[subindex]
                                                  .fileThumbnail;
                                          return Container(
                                            height: 350,
                                            child: Row(
                                              children: [
                                                Expanded(
                                                  child: GestureDetector(
                                                    child: listImageVideo[0]
                                                            .fileType!
                                                            .contains("image")
                                                        ? GestureDetector(
                                                            onTap: connection ==
                                                                    false
                                                                ? null
                                                                : () {
                                                                    // setState(() {
                                                                    //   BlocProvider.of<
                                                                    //               SchoolTimeLineDetailBloc>(
                                                                    //           context)
                                                                    //       .add(SchoolTimeLineFetchDetailEvent(
                                                                    //           idPost: data[
                                                                    //                   index]
                                                                    //               .id!));
                                                                    //   Navigator.of(context).push(
                                                                    //       MaterialPageRoute(
                                                                    //           builder: (
                                                                    //     context,
                                                                    //   ) =>
                                                                    //               ViewDetailSchoolTilemine()));
                                                                    // });
                                                                  },
                                                            child: SizedBox(
                                                                height: MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .height,
                                                                child: CachedImageNetwork(
                                                                    urlImage:
                                                                        listImageVideo[0]
                                                                            .fileThumbnail!)),
                                                          )
                                                        : listImageVideo[0]
                                                                .fileType!
                                                                .contains(
                                                                    "video")
                                                            ? SizedBox(
                                                                height: MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .height,
                                                                child: ThumbnailVideoWidget(
                                                                    image: listImageVideo[0].fileThumbnail,
                                                                    onPressed: connection == false
                                                                        ? null
                                                                        : () {
                                                                            // setState(
                                                                            //     () {
                                                                            //   BlocProvider.of<SchoolTimeLineDetailBloc>(
                                                                            //           context)
                                                                            //       .add(SchoolTimeLineFetchDetailEvent(
                                                                            //           idPost: data[index].id!));
                                                                            //   Navigator.of(context).push(MaterialPageRoute(
                                                                            //       builder: (
                                                                            //     context,
                                                                            //   ) =>
                                                                            //           ViewDetailSchoolTilemine()));
                                                                            // });
                                                                          }),
                                                              )
                                                            : Container(),
                                                    onTap: () {},
                                                  ),
                                                ),
                                                const SizedBox(
                                                  width: 2.5,
                                                ),
                                                SizedBox(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width /
                                                      2.7,
                                                  child: Column(
                                                    children: [
                                                      Expanded(
                                                        child: GestureDetector(
                                                            child: listImageVideo[
                                                                        1]
                                                                    .fileType!
                                                                    .contains(
                                                                        "image")
                                                                ? GestureDetector(
                                                                    onTap: connection ==
                                                                            false
                                                                        ? null
                                                                        : () {
                                                                            // setState(
                                                                            //     () {
                                                                            //   BlocProvider.of<SchoolTimeLineDetailBloc>(
                                                                            //           context)
                                                                            //       .add(SchoolTimeLineFetchDetailEvent(
                                                                            //           idPost: data[index].id!));
                                                                            //   Navigator.of(context).push(MaterialPageRoute(
                                                                            //       builder: (
                                                                            //     context,
                                                                            //   ) =>
                                                                            //           ViewDetailSchoolTilemine()));
                                                                            // });
                                                                          },
                                                                    child: SizedBox(
                                                                        height: MediaQuery.of(context)
                                                                            .size
                                                                            .height,
                                                                        child: CachedImageNetwork(
                                                                            urlImage:
                                                                                listImageVideo[1].fileThumbnail!)),
                                                                  )
                                                                : listImageVideo[
                                                                            1]
                                                                        .fileType!
                                                                        .contains(
                                                                            "video")
                                                                    ? SizedBox(
                                                                        height: MediaQuery.of(context)
                                                                            .size
                                                                            .height,
                                                                        child: ThumbnailVideoWidget(
                                                                            image: listImageVideo[1].fileThumbnail,
                                                                            onPressed: connection == false
                                                                                ? null
                                                                                : () {
                                                                                    // setState(() {
                                                                                    //   BlocProvider.of<SchoolTimeLineDetailBloc>(context).add(SchoolTimeLineFetchDetailEvent(idPost: data[index].id!));
                                                                                    //   Navigator.of(context).push(MaterialPageRoute(
                                                                                    //       builder: (
                                                                                    //     context,
                                                                                    //   ) =>
                                                                                    //           ViewDetailSchoolTilemine()));
                                                                                    // });
                                                                                  }),
                                                                      )
                                                                    : Container()),
                                                      ),
                                                      const SizedBox(
                                                        height: 2.6,
                                                      ),
                                                      Expanded(
                                                        child: GestureDetector(
                                                            child: listImageVideo[
                                                                        2]
                                                                    .fileType!
                                                                    .contains(
                                                                        "image")
                                                                ? GestureDetector(
                                                                    onTap: connection ==
                                                                            false
                                                                        ? null
                                                                        : () {
                                                                            // setState(
                                                                            //     () {
                                                                            //   BlocProvider.of<SchoolTimeLineDetailBloc>(
                                                                            //           context)
                                                                            //       .add(SchoolTimeLineFetchDetailEvent(
                                                                            //           idPost: data[index].id!));
                                                                            //   Navigator.of(context).push(MaterialPageRoute(
                                                                            //       builder: (
                                                                            //     context,
                                                                            //   ) =>
                                                                            //           ViewDetailSchoolTilemine()));
                                                                            // });
                                                                          },
                                                                    child: SizedBox(
                                                                        height: MediaQuery.of(context)
                                                                            .size
                                                                            .height,
                                                                        child: CachedImageNetwork(
                                                                            urlImage:
                                                                                listImageVideo[2].fileThumbnail!)),
                                                                  )
                                                                : listImageVideo[
                                                                            2]
                                                                        .fileType!
                                                                        .contains(
                                                                            "video")
                                                                    ? SizedBox(
                                                                        height: MediaQuery.of(context)
                                                                            .size
                                                                            .height,
                                                                        child: ThumbnailVideoWidget(
                                                                            image: listImageVideo[2].fileThumbnail,
                                                                            onPressed: connection == false
                                                                                ? null
                                                                                : () {
                                                                                    // setState(() {
                                                                                    //   BlocProvider.of<SchoolTimeLineDetailBloc>(context).add(SchoolTimeLineFetchDetailEvent(idPost: data[index].id!));
                                                                                    //   Navigator.of(context).push(MaterialPageRoute(
                                                                                    //       builder: (
                                                                                    //     context,
                                                                                    //   ) =>
                                                                                    //           ViewDetailSchoolTilemine()));
                                                                                    // });
                                                                                  }),
                                                                      )
                                                                    : Container()),
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          );
                                        }),
                                      )
                                    : numImages == 0
                                        ? Container()
                                        : GridView(
                                            padding: const EdgeInsets.all(0),
                                            physics: const ScrollPhysics(),
                                            shrinkWrap: true,
                                            gridDelegate:
                                                SliverGridDelegateWithMaxCrossAxisExtent(
                                                    crossAxisSpacing: 2,
                                                    mainAxisSpacing: 2,
                                                    maxCrossAxisExtent:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width /
                                                            2),
                                            children: List.generate(
                                                min(listImageVideo.length,
                                                    maxImage), (subindex) {
                                              var attachmentUrl =
                                                  listImageVideo[subindex]
                                                      .fileThumbnail;
                                              //var attachmentType = data[index].attachments![subindex].fileType;
                                              var attachmentType =
                                                  listImageVideo[subindex]
                                                      .fileType;
                                              if (subindex == maxImage - 1) {
                                                remaining =
                                                    (numImages! - maxImage);
                                                if (remaining == 0) {
                                                  return data[index]
                                                              .attachments!
                                                              .length <
                                                          0
                                                      ? Container()
                                                      : GestureDetector(
                                                          child: attachmentType!
                                                                  .contains(
                                                                      "image")
                                                              ? GestureDetector(
                                                                  onTap: connection ==
                                                                          false
                                                                      ? null
                                                                      : () {
                                                                          // setState(() {
                                                                          //   BlocProvider.of<
                                                                          //               SchoolTimeLineDetailBloc>(
                                                                          //           context)
                                                                          //       .add(SchoolTimeLineFetchDetailEvent(
                                                                          //           idPost: data[
                                                                          //                   index]
                                                                          //               .id!));
                                                                          //   Navigator.of(
                                                                          //           context)
                                                                          //       .push(MaterialPageRoute(
                                                                          //           builder: (
                                                                          //     context,
                                                                          //   ) =>
                                                                          //               ViewDetailSchoolTilemine()));
                                                                          // });
                                                                        },
                                                                  child: CachedImageNetwork(
                                                                      urlImage:
                                                                          attachmentUrl!),
                                                                )
                                                              : attachmentType
                                                                      .contains(
                                                                          "video")
                                                                  ? GestureDetector(
                                                                      onTap: connection ==
                                                                              false
                                                                          ? null
                                                                          : () {
                                                                              // setState(() {
                                                                              //   BlocProvider.of<
                                                                              //               SchoolTimeLineDetailBloc>(
                                                                              //           context)
                                                                              //       .add(SchoolTimeLineFetchDetailEvent(
                                                                              //           idPost:
                                                                              //               data[index].id!));
                                                                              //   Navigator.of(
                                                                              //           context)
                                                                              //       .push(MaterialPageRoute(
                                                                              //           builder: (
                                                                              //     context,
                                                                              //   ) =>
                                                                              //               ViewDetailSchoolTilemine()));
                                                                              // });
                                                                            },
                                                                      child:
                                                                          ThumbnailVideoWidget(
                                                                        image:
                                                                            attachmentUrl,
                                                                      ))
                                                                  : Container());
                                                } else {
                                                  return GestureDetector(
                                                    onTap: () {},
                                                    child: Stack(
                                                      fit: StackFit.expand,
                                                      children: [
                                                        attachmentType!
                                                                .contains(
                                                                    "image")
                                                            ? GestureDetector(
                                                                onTap:
                                                                    connection ==
                                                                            false
                                                                        ? null
                                                                        : () {
                                                                            // setState(() {
                                                                            //   BlocProvider.of<
                                                                            //               SchoolTimeLineDetailBloc>(
                                                                            //           context)
                                                                            //       .add(SchoolTimeLineFetchDetailEvent(
                                                                            //           idPost: data[
                                                                            //                   index]
                                                                            //               .id!));
                                                                            //   Navigator.of(context).push(
                                                                            //       MaterialPageRoute(
                                                                            //           builder: (
                                                                            //     context,
                                                                            //   ) =>
                                                                            //               ViewDetailSchoolTilemine()));
                                                                            // });
                                                                          },
                                                                child: CachedImageNetwork(
                                                                    urlImage:
                                                                        attachmentUrl!),
                                                              )
                                                            : attachmentType
                                                                    .contains(
                                                                        "video")
                                                                ? GestureDetector(
                                                                    onTap: connection ==
                                                                            false
                                                                        ? null
                                                                        : () {
                                                                            // setState(() {
                                                                            //   BlocProvider.of<
                                                                            //               SchoolTimeLineDetailBloc>(
                                                                            //           context)
                                                                            //       .add(SchoolTimeLineFetchDetailEvent(
                                                                            //           idPost: data[
                                                                            //                   index]
                                                                            //               .id!));
                                                                            //   Navigator.of(
                                                                            //           context)
                                                                            //       .push(MaterialPageRoute(
                                                                            //           builder: (
                                                                            //     context,
                                                                            //   ) =>
                                                                            //               ViewDetailSchoolTilemine()));
                                                                            // });
                                                                          },
                                                                    child:
                                                                        ThumbnailVideoWidget(
                                                                      image:
                                                                          attachmentUrl,
                                                                    ))
                                                                : Container(),
                                                        Positioned.fill(
                                                          child:
                                                              GestureDetector(
                                                            onTap: connection ==
                                                                    false
                                                                ? null
                                                                : () {
                                                                    // setState(() {
                                                                    //   BlocProvider.of<
                                                                    //               SchoolTimeLineDetailBloc>(
                                                                    //           context)
                                                                    //       .add(
                                                                    //           SchoolTimeLineFetchDetailEvent(
                                                                    //               idPost: data[
                                                                    //                       index]
                                                                    //                   .id!));
                                                                    //   Navigator.of(context).push(
                                                                    //       MaterialPageRoute(
                                                                    //           builder: (
                                                                    //     context,
                                                                    //   ) =>
                                                                    //               ViewDetailSchoolTilemine()));
                                                                    // });
                                                                  },
                                                            child: Container(
                                                              alignment:
                                                                  Alignment
                                                                      .center,
                                                              color: Colors
                                                                  .black54,
                                                              child: Text(
                                                                '+' +
                                                                    remaining
                                                                        .toString(),
                                                                style: const TextStyle(
                                                                    fontSize:
                                                                        32,
                                                                    color: Colors
                                                                        .white),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  );
                                                }
                                              } else {
                                                return attachmentType!
                                                        .contains("image")
                                                    ? GestureDetector(
                                                        onTap:
                                                            connection == false
                                                                ? null
                                                                : () {
                                                                    // setState(() {
                                                                    //   BlocProvider.of<
                                                                    //               SchoolTimeLineDetailBloc>(
                                                                    //           context)
                                                                    //       .add(
                                                                    //           SchoolTimeLineFetchDetailEvent(
                                                                    //               idPost:
                                                                    //                   data[index]
                                                                    //                       .id!));
                                                                    //   Navigator.of(context)
                                                                    //       .push(MaterialPageRoute(
                                                                    //           builder: (
                                                                    //     context,
                                                                    //   ) =>
                                                                    //               ViewDetailSchoolTilemine()));
                                                                    // });
                                                                  },
                                                        child: CachedImageNetwork(
                                                            urlImage:
                                                                attachmentUrl!),
                                                      )
                                                    : attachmentType
                                                            .contains("video")
                                                        ? GestureDetector(
                                                            onTap: connection ==
                                                                    false
                                                                ? null
                                                                : () {
                                                                    // setState(() {
                                                                    //   BlocProvider.of<
                                                                    //               SchoolTimeLineDetailBloc>(
                                                                    //           context)
                                                                    //       .add(
                                                                    //           SchoolTimeLineFetchDetailEvent(
                                                                    //               idPost: data[
                                                                    //                       index]
                                                                    //                   .id!));
                                                                    //   Navigator.of(context).push(
                                                                    //       MaterialPageRoute(
                                                                    //           builder: (
                                                                    //     context,
                                                                    //   ) =>
                                                                    //               ViewDetailSchoolTilemine()));
                                                                    // });
                                                                  },
                                                            child:
                                                                ThumbnailVideoWidget(
                                                              image:
                                                                  attachmentUrl,
                                                            ))
                                                        : Container(
                                                            color:
                                                                Colors.black);
                                              }
                                            }),
                                          ),
                            //------------------------ End Center Body ------------------

                            //------------------------File-------------------------------
                            displayFile: Container(
                              child: ListView.separated(
                                physics: const ScrollPhysics(),
                                padding: const EdgeInsets.all(0),
                                shrinkWrap: true,
                                itemCount: applications.length,
                                itemBuilder: (BuildContext contex, attindex) {
                                  var path = applications[attindex]
                                      .fileShow
                                      .toString();
                                  var filename = applications[attindex]
                                      .fileName
                                      .toString();
                                  if (attindex > 2) {
                                    return Container(color: Colors.black);
                                  } else {
                                    return Column(
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        Container(
                                          child: ThumbnailDocWidget(
                                            nameFile: applications[attindex]
                                                    .fileName ??
                                                "",
                                            sizeFile: applications[attindex]
                                                    .fileSize ??
                                                "",
                                            onPressed: connection == false
                                                ? null
                                                : () {
                                                    setState(() {
                                                      Navigator.of(context).push(
                                                          MaterialPageRoute(
                                                              builder: (
                                                        context,
                                                      ) =>
                                                                  ViewDocuments(
                                                                    path: path,
                                                                    filename:
                                                                        filename
                                                                            .toString(),
                                                                  )));
                                                    });
                                                  },
                                          ),
                                        ),
                                        attindex == 2
                                            ? TextButton(
                                                onPressed: connection == false
                                                    ? null
                                                    : () {
                                                        // setState(() {
                                                        //   BlocProvider.of<
                                                        //               SchoolTimeLineDetailBloc>(
                                                        //           context)
                                                        //       .add(
                                                        //           SchoolTimeLineFetchDetailEvent(
                                                        //               idPost: data[index].id!));
                                                        //   Navigator.of(context)
                                                        //       .push(MaterialPageRoute(
                                                        //           builder: (
                                                        //     context,
                                                        //   ) =>
                                                        //               ViewDetailSchoolTilemine()));
                                                        // });
                                                      },
                                                child: const Text(
                                                  "See More",
                                                  style: TextStyle(
                                                      color: Colorconstands
                                                          .primaryColor,
                                                      fontSize: 15.0),
                                                ))
                                            : Container()
                                      ],
                                    );
                                  }
                                },
                                separatorBuilder:
                                    (BuildContext context, int index) {
                                  return Container(
                                    height: 8.0,
                                    color: Colorconstands.gray,
                                  );
                                },

                                //},
                              ),
                            ),
                            //------------------------End File------------------

                            //------------------------Button Like------------------
                            onReactionChanged:
                                (String? values, bool isChecked) {
                              setState(() {
                                if (data[index].isReact == 0) {
                                  data[index].totalReact =
                                      data[index].totalReact!;
                                  if (data[index].reactionType == 0 ||
                                      data[index].isReact == 0) {
                                    data[index].totalReact =
                                        data[index].totalReact! + 1;
                                    isChecked = true;
                                    if (values == "Like") {
                                      selecticonreact = 0;
                                      data[index].isReact = 1;
                                      data[index].reactionType = 0;
                                    } else if (values == "Love") {
                                      selecticonreact = 1;
                                      data[index].isReact = 1;
                                      data[index].reactionType = 1;
                                    } else if (values == "haha") {
                                      selecticonreact = 2;
                                      data[index].isReact = 1;
                                      data[index].reactionType = 2;
                                    } else if (values == "Wow") {
                                      selecticonreact = 3;
                                      data[index].isReact = 1;
                                      data[index].reactionType = 3;
                                    } else if (values == "Angry") {
                                      selecticonreact = 5;
                                      data[index].isReact = 1;
                                      data[index].reactionType = 5;
                                    } else {
                                      selecticonreact = 0;
                                      data[index].isReact = 1;
                                      data[index].reactionType = 0;
                                    }
                                  } else {
                                    if (values == 'Like') {
                                      data[index].isReact = 1;
                                      data[index].reactionType = 0;
                                      // _audioCache.play('sounds/like.mp3');
                                    } else if (values == 'Love') {
                                      data[index].isReact = 1;
                                      data[index].reactionType = 1;
                                      // _audioCache.play('sounds/box_up.mp3');
                                    } else if (values == 'haha') {
                                      data[index].isReact = 1;
                                      data[index].reactionType = 2;
                                      // _audioCache.play('sounds/Laugh_Laugh.mp3');
                                    } else if (values == 'Wow') {
                                      data[index].isReact = 1;
                                      data[index].reactionType = 3;
                                      // _audioCache.play('sounds/Wow.mp3');
                                    } else if (values == 'Unselect' &&
                                        isChecked == true) {
                                      isChecked = false;
                                      selecticonreact = 0;
                                      data[index].isReact = 0;
                                      data[index].reactionType = 0;
                                      data[index].totalReact =
                                          data[index].totalReact! - 1;
                                    } else if (data[index].reactionType == 0) {
                                      isChecked = true;
                                      selecticonreact = 0;
                                      data[index].isReact = 1;
                                      data[index].reactionType = 0;
                                    } else if (values == 'Angry') {
                                      data[index].isReact = 1;
                                      data[index].reactionType = 5;
                                      // _audioCache.play('sounds/Nani_anime.mp3');
                                    } else {
                                      data[index].isReact = 1;
                                      data[index].reactionType = 5;
                                    }
                                  }
                                } else if (data[index].reactionType == 6 ||
                                    data[index].isReact == 0) {
                                  data[index].totalReact =
                                      data[index].totalReact! + 1;
                                  isChecked = true;
                                  selecticonreact = 0;
                                  data[index].isReact = 1;
                                  data[index].reactionType = 0;
                                } else {
                                  if (values == 'Like') {
                                    data[index].isReact = 1;
                                    data[index].reactionType = 0;
                                    // _audioCache.play('sounds/like.mp3');
                                  } else if (values == 'Love') {
                                    data[index].isReact = 1;
                                    data[index].reactionType = 1;
                                    // _audioCache.play('sounds/box_up.mp3');
                                  } else if (values == 'haha') {
                                    data[index].isReact = 1;
                                    data[index].reactionType = 2;
                                    // _audioCache.play('sounds/Laugh_Laugh.mp3');
                                  } else if (values == 'Wow') {
                                    data[index].isReact = 1;
                                    data[index].reactionType = 3;
                                    // _audioCache.play('sounds/Wow.mp3');
                                  } else if (isChecked == false &&
                                      values == 'Unselect' &&
                                      data[index].reactionType == 0) {
                                    selecticonreact = 5;
                                    data[index].reactionType = 5;
                                    data[index].isReact = 1;
                                  } else if (values == 'Unselect' &&
                                      isChecked == true) {
                                    isChecked = false;
                                    selecticonreact = 0;
                                    data[index].isReact = 0;
                                    data[index].reactionType = 0;
                                    data[index].totalReact =
                                        data[index].totalReact! - 1;
                                  } else {
                                    data[index].isReact = 1;
                                    data[index].reactionType = 5;
                                    // _audioCache.('sounds/Nani_anime.mp3');
                                  }
                                }
                              });
                              setState(() {
                                values == "Like"
                                    ? reactiondefultselect =
                                        "assets/images/actionButton/like_full_fill.png"
                                    : values == "Love"
                                        ? reactiondefultselect =
                                            "assets/images/actionButton/Love.png"
                                        : values == "haha"
                                            ? reactiondefultselect =
                                                "assets/images/actionButton/Icon_haha.png"
                                            : values == "Wow"
                                                ? reactiondefultselect =
                                                    "assets/images/actionButton/wow2.png"
                                                : values == "Angry"
                                                    ? reactiondefultselect =
                                                        "assets/images/actionButton/angry2.png"
                                                    : reactiondefultselect =
                                                        "assets/images/actionButton/like_full_fill.png";
                              });
                              PostReaction _postReact = PostReaction();
                              _postReact.postReact("/school-time-line/react/",
                                  data[index].reactionType!, data[index].id!);
                              print(
                                  'Selected value: $values, isChecked: $isChecked');
                            },
                            //------------------------End Button Like------------------
                            //===================== button comment ====================

                            onTapcomment: connection == false
                                ? null
                                : () {
                                    // BlocProvider.of<SchoolTimeLineDetailBloc>(context).add(
                                    //     SchoolTimeLineFetchDetailEvent(
                                    //         idPost: data[index].id!));
                                    Navigator.of(context).push(
                                      MaterialPageRoute(
                                        fullscreenDialog: true,
                                        builder: (context) {
                                          return const CommentPost();
                                        },
                                      ),
                                    );
                                  },

                            onTapBodycomment: connection == false
                                ? null
                                : () {
                                    // BlocProvider.of<SchoolTimeLineDetailBloc>(context).add(
                                    //     SchoolTimeLineFetchDetailEvent(
                                    //         idPost: data[index].id!));
                                    Navigator.of(context).push(
                                      MaterialPageRoute(
                                        fullscreenDialog: true,
                                        builder: (context) {
                                          return const CommentPost();
                                        },
                                      ),
                                    );
                                  },

                            //===================== End button comment ====================
                          );

                          // return Timeline_widget(
                          //     commenttext: "sinat",
                          //     commentprofile: "sinat",
                          //     nameusercomment: "sdf",
                          //     totalReactType: 3,
                          //     comments: 4,
                          //     reaction: "React",
                          //     totalLike: 3,
                          //     name: "sdf",
                          //     imageReact1: "sdf",
                          //     imageReact2: "sdf",
                          //     colorlabelreact: Colorconstands.primaryColor,
                          //     pathImageReact: "sdf",
                          //     selectedReaction: 1,
                          //     textReact: "Like",
                          //     imageProfile: "sdf",
                          //     discriptionChild: Container(),
                          //     displayVoice: Container(),
                          //     viewChild: Container(),
                          //     displayFile: Container(),
                          //     onReactionChanged: (String? values, bool isChecked) {},
                          //     datePost: data[index].createdDate,
                          //     onTapcomment: () {},
                          //     onTapBodycomment: () {});
                          // return Container(
                          //   padding: const EdgeInsets.all(80),
                          //   margin: const EdgeInsets.all(10),
                          //   color: isClick == index ? Colors.blue : Colors.red,
                          //   child: ListTile(
                          //     onTap: () {
                          //       setState(() {
                          //         isClick = index;
                          //       });
                          //     },
                          //     title: Text(
                          //       listData[index].id.toString(),
                          //     ),
                          //   ),
                          // );
                        } else {
                          Timer(const Duration(milliseconds: 30), () {
                            _scrollController.jumpTo(
                                _scrollController.position.maxScrollExtent);
                          });

                          return Center(
                            child: isReachedMax == true
                                ? Container(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 18),
                                    child: const Text("No More item"),
                                  )
                                : const CircularProgressIndicator(),
                          );
                        }
                      },
                    ),
                  );
                } else {
                  return Column(
                    children: [
                      Expanded(
                        child: Container(
                          color: Colorconstands.neutralWhite,
                          child: ListView.builder(
                            shrinkWrap: true,
                            itemBuilder: (BuildContext contex, index) {
                              return const ShimmerTimeLine();
                            },
                            itemCount: 3,
                          ),
                        ),
                      ),
                    ],
                  );
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
