// ignore_for_file: list_remove_unrelated_type
import 'package:bloc/bloc.dart';
import 'package:camis_application_flutter/app/modules/time_line/school_time_line/e.schooltimeline.dart';
import 'package:equatable/equatable.dart';
import '../../../../../service/apis/time_line_api/get_time_line_api.dart';

part 'pagination_event.dart';
part 'pagination_state.dart';

class PaginationBloc extends Cubit<PaginationState> {
  final GetSchoolTimeLineApi getSchooltimelinedetailapi;
  PaginationBloc({required this.getSchooltimelinedetailapi})
      : super(PaginationInitial());
  int page = 1;
  bool isReachedMax = false;
  int limited = 4;
  List<Datum> dataa = [];
  var oldData = <Datum>[];
  void loadedData({required bool reset}) async {
    if (state is PaginationLoadingState) return;
    final currentState = state;

    if (currentState is PaginationSuccessState) {
      oldData = currentState.data;
    }
    emit(PaginationLoadingState(oldData: oldData, isFirstFetch: page == 1));
    var sinat = await getSchooltimelinedetailapi.getSchoolTimelineApi(
        page: page, limit: limited);
    page++;
    dataa = (state as PaginationLoadingState).oldData;
    dataa.addAll(sinat.data!);
    if (sinat.meta!.currentPage <= sinat.meta!.lastPage) {
      isReachedMax = false;
      emit(PaginationSuccessState(data: dataa, hasReachedMax: isReachedMax));
    } else {
      isReachedMax = true;
      emit(PaginationSuccessState(data: dataa, hasReachedMax: isReachedMax));
    }

    if (reset == true) {
      oldData = [];
      dataa = [];
      page = 1;
      emit(PaginationLoadingState(oldData: oldData, isFirstFetch: page == 1));
      await getSchooltimelinedetailapi
          .getSchoolTimelineApi(page: page, limit: limited)
          .then((value) {
        oldData.addAll(value.data!);
        emit(PaginationSuccessState(data: oldData, hasReachedMax: false));
         page = 2;
      });
    }
    // .then((value)
    //  async {
    // if (reset == false) {
    //   page++;
    //   dataa = (state as PaginationLoadingState).oldData;
    //   dataa.addAll(value.data!);
    //   if (sinat.meta!.currentPage <= value.meta!.lastPage) {
    //     isReachedMax = false;
    //     emit(
    //         PaginationSuccessState(data: dataa, hasReachedMax: isReachedMax));
    //   } else {
    //     isReachedMax = true;
    //     emit(
    //         PaginationSuccessState(data: dataa, hasReachedMax: isReachedMax));
    //   }
    // } else if (reset == true) {
    //   page = 1;
    //   oldData = [];
    //   dataa = [];
    //   await getSchooltimelinedetailapi
    //       .getSchoolTimelineApi(page: page, limit: limited)
    //       .then((value) => emit(PaginationSuccessState(
    //           data: value.data!.toList(), hasReachedMax: false)));
    // }
    // });
  }
}
