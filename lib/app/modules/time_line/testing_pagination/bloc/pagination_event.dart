part of 'pagination_bloc.dart';

abstract class PaginationEvent extends Equatable {
  const PaginationEvent();

  @override
  List<Object> get props => [];
}

class FetchEvent extends PaginationEvent {
  final int page;
  const FetchEvent({required this.page});
}
