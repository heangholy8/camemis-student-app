// ignore_for_file: must_be_immutable

part of 'pagination_bloc.dart';

abstract class PaginationState {
  const PaginationState();
}

class PaginationInitial extends PaginationState {}

class PaginationLoadingState extends PaginationState {
    List<Datum> oldData;
   bool isFirstFetch;
   PaginationLoadingState({required this.oldData,this.isFirstFetch=false});
}

class PaginationSuccessState extends PaginationState {
  final List<Datum> data;
  // final int page;
  final bool hasReachedMax;

  const PaginationSuccessState({required this.data,required this.hasReachedMax});

  // PaginationSuccessState copyWith(
  //     {List<Datum>? data, int? page, bool? hasReachedMax}) {
  //   return PaginationSuccessState(
  //       data: data ?? this.data,
  //       page: page ?? this.page,
  //       hasReachedMax: hasReachedMax ?? this.hasReachedMax);
  // }
}

class PaginationErrorState extends PaginationState {
  final String error;
  const PaginationErrorState({required this.error});
}




// class PaginationLoadedState extends PaginationState {
//   final int currentPage;
//   bool hasReachedMax;
//   final List<Datum> items;
//   final int lastPage;

//   PaginationLoadedState({
//     required this.currentPage,
//     required this.items,
//     required this.hasReachedMax,
//     required this.lastPage,
//   });

//   factory PaginationLoadedState.initial() {
//     return PaginationLoadedState(
//         currentPage: 1, items: [], hasReachedMax: false, lastPage: 1);
//   }

//   bool get hasNextPage => currentPage < lastPage;
//   PaginationLoadedState copyWith(
//       {List<Datum>? dataa, int? currentPage, bool? hasMore, int? lastPage}) {
//     return PaginationLoadedState(
//         lastPage: lastPage ?? this.lastPage,
//         currentPage: currentPage ?? this.currentPage,
//         hasReachedMax: hasMore ?? hasReachedMax,
//         items: dataa ?? items);
//   }
// }

// class PaginationErrorState extends PaginationState {
//   final String? error;
//   const PaginationErrorState({this.error});
// }
