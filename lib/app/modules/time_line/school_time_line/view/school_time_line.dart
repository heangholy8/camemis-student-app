import 'dart:async';
import 'package:camis_application_flutter/app/modules/auth_screens/e_school_code.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../../../helper/route.export.dart';
import '../../widget/thumbnail_doc_widget.dart';
import '../../widget/thumbnail_widget.dart';
import '../../widget/view_image.dart';
import '../e.schooltimeline.dart';

class SchoolTimeline extends StatefulWidget {
  const SchoolTimeline({Key? key}) : super(key: key);
  @override
  State<SchoolTimeline> createState() => _SchoolTimelineState();
}

class _SchoolTimelineState extends State<SchoolTimeline> {
  late ScrollController _scrollController;
  double _lastOffset = 0.0;
  bool isReachedMax = false;
  late final AudioCache _audioCache;
  int maxImage = 4;
  int? remaining;
  int? numImages;
  String? reaction;
  bool connection = true;
  StreamSubscription? stsub;
  //final dataTimeLine = TimeLineModel.generate();
  String? discription;
  String reactiondefultselect =
      "assets/images/actionButton/icon_like_circle.png";

  @override
  void initState() {
    ScrollController(initialScrollOffset: _lastOffset);
    _scrollController = ScrollController(keepScrollOffset: true)
      ..addListener(onScroll);
    // _loadScrollOffset();
    BlocProvider.of<SchoolSocialBloc>(context)
        .add(const GetSchooltimeLineEvent(isRefresh: false));
    //=============Check internet====================
    stsub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
      });
    });
    super.initState();
    _audioCache = AudioCache(
      prefix: '',
      fixedPlayer: AudioPlayer()..setReleaseMode(ReleaseMode.STOP),
    );
  }

  void onScroll() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent - 200 &&
        !_scrollController.position.outOfRange &&
        _scrollController.position.pixels != 0 &&
        isReachedMax == false) {
      connection == true
          ? BlocProvider.of<SchoolSocialBloc>(context)
              .add(const GetSchooltimeLineEvent(isRefresh: false))
          : null;
    }
  }

  @override
  void dispose() {
    stsub!.cancel();
    _scrollController.dispose();
    _scrollController.removeListener(onScroll);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    ScrollController(initialScrollOffset: _lastOffset);
    return Container(
      color: Colors.white,
      child: BlocBuilder<SchoolSocialBloc, SchoolTimeLineState>(
        builder: (context, state) {
          List<Datum> data = [];
          bool isLoading = false;
          if (state is SchoolsocialLoadingState) {
            data = state.oldData;
            isLoading = true;
          } else if (state is SchoolsocialSuccessState) {
            data = state.data;
            isReachedMax = state.hasReachedMax;
          }
          if (state is SchoolsocialLoadingState && state.isFirstFetch) {
            return Column(
              children: [
                Expanded(
                  child: Container(
                    color: Colorconstands.neutralWhite,
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemBuilder: (BuildContext contex, index) {
                        return const ShimmerTimeLine();
                      },
                      itemCount: 3,
                    ),
                  ),
                ),
              ],
            );
          }
          return RefreshIndicator(
            onRefresh: () async {
              BlocProvider.of<SchoolSocialBloc>(context)
                  .add(const GetSchooltimeLineEvent(isRefresh: true));
            },
            child: ListView.separated(
              controller: _scrollController,
              itemCount: data.length +
                  (isLoading ? 1 : 0) +
                  (isReachedMax == true ? 1 : 0),
              itemBuilder: (context, index) {
                if (index < data.length) {
                  String? profileusercomment;
                  String? nameusercomment;
                  String? commenttext;
                  var applications = data[index]
                      .attachments!
                      .where((element) =>
                          element.fileType!.contains("application"))
                      .toList();
                  var audio = data[index]
                      .attachments!
                      .where((element) => element.fileType!.contains("audio"))
                      .toList();
                  var listImageVideo =
                      data[index].attachments!.where((element) {
                    return element.fileType!.contains("image") ||
                        element.fileType!.contains("video");
                  }).toList();

                  var imageandvideo = listImageVideo.length;
                  if (data[index].comments!.isNotEmpty) {
                    profileusercomment = data[index]
                        .comments![0]
                        .user!
                        .profileMedia!
                        .fileThumbnail;
                    nameusercomment = translate == "en"
                        ? data[index].comments![0].user!.nameEn == " " ||
                                data[index].comments![0].user!.nameEn == ""
                            ? data[index].comments![0].user!.name
                            : data[index].comments![0].user!.nameEn
                        : data[index].comments![0].user!.name;
                    commenttext = data[index].comments![0].postText;
                  }
                  discription = data[index].postText.toString();
                  int indexSelected = index;
                  int selecticonreact = 5;
                  numImages = imageandvideo;
                  return Timeline_widget(
                    commenttext: commenttext.toString(),
                    commentprofile: profileusercomment,
                    nameusercomment: nameusercomment,
                    totalReactType: data[index].totalReactType!.length,
                    comments: data[index].commentCount!,
                    reaction: "React",
                    totalLike: data[index].totalReact,
                    datePost: data[index].createdDate,
                    name: translate == "en"
                        ? data[index].user!.nameEn == " " ||
                                data[index].user!.nameEn == " "
                            ? data[index].user!.name
                            : data[index].user!.nameEn
                        : data[index].user!.name,
                    //------------------------- Rect type all--------------------------
                    imageReact2: data[index].totalReactType!.isEmpty
                        ? reactiondefultselect
                        : data[index].totalReactType![0].reactionType == 0
                            ? "assets/images/actionButton/icon_like_circle.png"
                            : data[index].totalReactType![0].reactionType == 1
                                ? "assets/images/actionButton/Love.png"
                                : data[index].totalReactType![0].reactionType ==
                                        2
                                    ? "assets/images/actionButton/Icon_haha.png"
                                    : data[index]
                                                .totalReactType![0]
                                                .reactionType ==
                                            3
                                        ? "assets/images/actionButton/wow2.png"
                                        : data[index]
                                                    .totalReactType![0]
                                                    .reactionType ==
                                                5
                                            ? "assets/images/actionButton/angry2.png"
                                            : "assets/images/actionButton/icon_like_circle.png",
                    imageReact1: data[index].totalReactType!.length <= 1
                        ? "assets/images/actionButton/icon_like_circle.png"
                        : data[index].totalReactType![1].reactionType == 0
                            ? "assets/images/actionButton/icon_like_circle.png"
                            : data[index].totalReactType![1].reactionType == 1
                                ? "assets/images/actionButton/Love.png"
                                : data[index].totalReactType![1].reactionType ==
                                        2
                                    ? "assets/images/actionButton/Icon_haha.png"
                                    : data[index]
                                                .totalReactType![1]
                                                .reactionType ==
                                            3
                                        ? "assets/images/actionButton/wow2.png"
                                        : data[index]
                                                    .totalReactType![1]
                                                    .reactionType ==
                                                5
                                            ? "assets/images/actionButton/angry2.png"
                                            : "assets/images/actionButton/icon_like_circle.png",
                    // imageReactselectdefoult:selecticonreact==0?"assets/images/actionButton/icon_like_circle.png"
                    // :selecticonreact==1?"assets/images/actionButton/Love.png":"assets/images/actionButton/wow2.png",

                    //-------------------------End Rect type all--------------------------

                    //------------------------- color Label React--------------------------
                    //colorlabelreact: Colorconstands.primaryColor,
                    colorlabelreact: data[index].isReact == 0
                        ? Colorconstands.primaryColor
                        : data[index].reactionType == 0
                            ? Colorconstands.primaryColor
                            : data[index].reactionType == 1
                                ? Colors.red
                                : data[index].reactionType == 2
                                    ? Colors.yellow
                                    : data[index].reactionType == 5
                                        ? Colors.red
                                        : data[index].reactionType == 6
                                            ? Colorconstands.primaryColor
                                            : Colors.yellow,
                    //-------------------------End color Label React--------------------------

                    pathImageReact: data[index].isReact == 0
                        ? "assets/icons/like_icon.png"
                        : data[index].reactionType == 0
                            ? "assets/images/actionButton/like_full_fill.png"
                            : data[index].reactionType == 1
                                ? "assets/images/actionButton/Love.png"
                                : data[index].reactionType == 2
                                    ? "assets/images/actionButton/Icon_haha.png"
                                    : data[index].reactionType == 3
                                        ? "assets/images/actionButton/wow2.png"
                                        : data[index].reactionType == 5
                                            ? "assets/images/actionButton/angry2.png"
                                            : "assets/icons/like_icon.png",
                    textReact: data[index].isReact == 0
                        ? "Like"
                        : data[index].reactionType == 0
                            ? "Like"
                            : data[index].reactionType == 1
                                ? "Love"
                                : data[index].reactionType == 3
                                    ? "Wow"
                                    : data[index].reactionType == 2
                                        ? "haha"
                                        : data[index].reactionType == 5
                                            ? "angry"
                                            : "Like",
                    selectedReaction: selecticonreact,

                    //------------------------- Profile image--------------------------
                    imageProfile:
                        data[index].user!.profileMedia!.fileThumbnail ??
                            data[index].user!.profileMedia!.fileShow,
                    //-------------------------End Profile image--------------------------

                    //------------------------Text Post------------------
                    discriptionChild: Container(
                      alignment: Alignment.centerLeft,
                      margin: const EdgeInsets.only(
                          left: 22.0, right: 22.0, top: 15.0, bottom: 12.0),
                      child: ExpandableText(
                        discription == null ||
                                discription == "" ||
                                discription!.isEmpty
                            ? ""
                            : discription!,
                        expandText: 'show more',
                        collapseText: 'show less',
                        maxLines: 2,
                        style: const TextStyle(fontSize: 16.0),
                        linkColor: Colorconstands.primaryColor,
                      ),
                    ),
                    //------------------------End Text Post------------------

                    //------------------------Void---------------------
                    displayVoice: Container(),
                    //------------------------End Void------------------

                    //---------------View Center Body--------------------------
                    viewChild: numImages == 1
                        ? Stack(
                            children: List.generate(
                              listImageVideo.length,
                              (subindex) {
                                var attachmentUrl =
                                    listImageVideo[subindex].fileThumbnail;
                                var attachmentType =
                                    listImageVideo[subindex].fileType;
                                var url = listImageVideo[subindex].fileShow;
                                var tilte = listImageVideo[subindex].fileName;
                                return listImageVideo.length < 0
                                    ? Container()
                                    : GestureDetector(
                                        child: attachmentType!.contains("image")
                                            ? GestureDetector(
                                                onTap: connection == false
                                                    ? null
                                                    : () {
                                                        Navigator.push(
                                                          context,
                                                          PageRouteBuilder(
                                                            pageBuilder: (_, __,
                                                                    ___) =>
                                                                ImageViewDownloads(
                                                              listimagevide:
                                                                  listImageVideo,
                                                              activepage: 0,
                                                            ),
                                                            transitionDuration:
                                                                const Duration(
                                                                    seconds: 0),
                                                          ),
                                                        );
                                                      },
                                                child: CachedImageNetwork(
                                                    urlImage: attachmentUrl!),
                                              )
                                            : attachmentType.contains("video")
                                                ? ThumbnailVideoWidget(
                                                    image: attachmentUrl,
                                                    onPressed:
                                                        connection == false
                                                            ? null
                                                            : () {
                                                                setState(() {
                                                                  Navigator.of(
                                                                          context)
                                                                      .push(
                                                                    MaterialPageRoute(
                                                                      builder: (
                                                                        context,
                                                                      ) =>
                                                                          VideoPlayerWidget(
                                                                        linkVideo:
                                                                            url,
                                                                      ),
                                                                    ),
                                                                  );
                                                                });
                                                              },
                                                  )
                                                : Container(),
                                      );
                              },
                            ),
                          )
                        : numImages == 3
                            ? Stack(
                                children: List.generate(
                                    min(listImageVideo.length, maxImage),
                                    (subindex) {
                                  var attachmentUrl =
                                      listImageVideo[subindex].fileThumbnail;
                                  return Container(
                                    height: 350,
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: GestureDetector(
                                            child:
                                                listImageVideo[0]
                                                        .fileType!
                                                        .contains("image")
                                                    ? GestureDetector(
                                                        onTap:
                                                            connection == false
                                                                ? null
                                                                : () {
                                                                    setState(
                                                                        () {
                                                                      BlocProvider.of<SchoolTimeLineDetailBloc>(
                                                                              context)
                                                                          .add(SchoolTimeLineFetchDetailEvent(
                                                                              idPost: data[index].id!));
                                                                      Navigator.of(context).push(MaterialPageRoute(
                                                                          builder: (
                                                                        context,
                                                                      ) =>
                                                                              ViewDetailSchoolTilemine()));
                                                                    });
                                                                  },
                                                        child: SizedBox(
                                                            height:
                                                                MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .height,
                                                            child: CachedImageNetwork(
                                                                urlImage:
                                                                    listImageVideo[
                                                                            0]
                                                                        .fileThumbnail!)),
                                                      )
                                                    : listImageVideo[0]
                                                            .fileType!
                                                            .contains("video")
                                                        ? SizedBox(
                                                            height:
                                                                MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .height,
                                                            child:
                                                                ThumbnailVideoWidget(
                                                                    image: listImageVideo[
                                                                            0]
                                                                        .fileThumbnail,
                                                                    onPressed: connection ==
                                                                            false
                                                                        ? null
                                                                        : () {
                                                                            setState(() {
                                                                              BlocProvider.of<SchoolTimeLineDetailBloc>(context).add(SchoolTimeLineFetchDetailEvent(idPost: data[index].id!));
                                                                              Navigator.of(context).push(MaterialPageRoute(
                                                                                  builder: (
                                                                                context,
                                                                              ) =>
                                                                                      ViewDetailSchoolTilemine()));
                                                                            });
                                                                          }),
                                                          )
                                                        : Container(),
                                            onTap: () {},
                                          ),
                                        ),
                                        const SizedBox(
                                          width: 2.5,
                                        ),
                                        SizedBox(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              2.7,
                                          child: Column(
                                            children: [
                                              Expanded(
                                                child: GestureDetector(
                                                    child:
                                                        listImageVideo[1]
                                                                .fileType!
                                                                .contains(
                                                                    "image")
                                                            ? GestureDetector(
                                                                onTap:
                                                                    connection ==
                                                                            false
                                                                        ? null
                                                                        : () {
                                                                            setState(() {
                                                                              BlocProvider.of<SchoolTimeLineDetailBloc>(context).add(SchoolTimeLineFetchDetailEvent(idPost: data[index].id!));
                                                                              Navigator.of(context).push(MaterialPageRoute(
                                                                                  builder: (
                                                                                context,
                                                                              ) =>
                                                                                      ViewDetailSchoolTilemine()));
                                                                            });
                                                                          },
                                                                child: SizedBox(
                                                                    height: MediaQuery.of(
                                                                            context)
                                                                        .size
                                                                        .height,
                                                                    child: CachedImageNetwork(
                                                                        urlImage:
                                                                            listImageVideo[1].fileThumbnail!)),
                                                              )
                                                            : listImageVideo[1]
                                                                    .fileType!
                                                                    .contains(
                                                                        "video")
                                                                ? SizedBox(
                                                                    height: MediaQuery.of(
                                                                            context)
                                                                        .size
                                                                        .height,
                                                                    child: ThumbnailVideoWidget(
                                                                        image: listImageVideo[1].fileThumbnail,
                                                                        onPressed: connection == false
                                                                            ? null
                                                                            : () {
                                                                                setState(() {
                                                                                  BlocProvider.of<SchoolTimeLineDetailBloc>(context).add(SchoolTimeLineFetchDetailEvent(idPost: data[index].id!));
                                                                                  Navigator.of(context).push(MaterialPageRoute(
                                                                                      builder: (
                                                                                    context,
                                                                                  ) =>
                                                                                          ViewDetailSchoolTilemine()));
                                                                                });
                                                                              }),
                                                                  )
                                                                : Container()),
                                              ),
                                              const SizedBox(
                                                height: 2.6,
                                              ),
                                              Expanded(
                                                child: GestureDetector(
                                                    child:
                                                        listImageVideo[2]
                                                                .fileType!
                                                                .contains(
                                                                    "image")
                                                            ? GestureDetector(
                                                                onTap:
                                                                    connection ==
                                                                            false
                                                                        ? null
                                                                        : () {
                                                                            setState(() {
                                                                              BlocProvider.of<SchoolTimeLineDetailBloc>(context).add(SchoolTimeLineFetchDetailEvent(idPost: data[index].id!));
                                                                              Navigator.of(context).push(MaterialPageRoute(
                                                                                  builder: (
                                                                                context,
                                                                              ) =>
                                                                                      ViewDetailSchoolTilemine()));
                                                                            });
                                                                          },
                                                                child: SizedBox(
                                                                    height: MediaQuery.of(
                                                                            context)
                                                                        .size
                                                                        .height,
                                                                    child: CachedImageNetwork(
                                                                        urlImage:
                                                                            listImageVideo[2].fileThumbnail!)),
                                                              )
                                                            : listImageVideo[2]
                                                                    .fileType!
                                                                    .contains(
                                                                        "video")
                                                                ? SizedBox(
                                                                    height: MediaQuery.of(
                                                                            context)
                                                                        .size
                                                                        .height,
                                                                    child: ThumbnailVideoWidget(
                                                                        image: listImageVideo[2].fileThumbnail,
                                                                        onPressed: connection == false
                                                                            ? null
                                                                            : () {
                                                                                setState(() {
                                                                                  BlocProvider.of<SchoolTimeLineDetailBloc>(context).add(SchoolTimeLineFetchDetailEvent(idPost: data[index].id!));
                                                                                  Navigator.of(context).push(MaterialPageRoute(
                                                                                      builder: (
                                                                                    context,
                                                                                  ) =>
                                                                                          ViewDetailSchoolTilemine()));
                                                                                });
                                                                              }),
                                                                  )
                                                                : Container()),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  );
                                }),
                              )
                            : numImages == 0
                                ? Container()
                                : GridView(
                                    padding: const EdgeInsets.all(0),
                                    physics: const ScrollPhysics(),
                                    shrinkWrap: true,
                                    gridDelegate:
                                        SliverGridDelegateWithMaxCrossAxisExtent(
                                            crossAxisSpacing: 2,
                                            mainAxisSpacing: 2,
                                            maxCrossAxisExtent:
                                                MediaQuery.of(context)
                                                        .size
                                                        .width /
                                                    2),
                                    children: List.generate(
                                        min(listImageVideo.length, maxImage),
                                        (subindex) {
                                      var attachmentUrl =
                                          listImageVideo[subindex]
                                              .fileThumbnail;
                                      //var attachmentType = data[index].attachments![subindex].fileType;
                                      var attachmentType =
                                          listImageVideo[subindex].fileType;
                                      if (subindex == maxImage - 1) {
                                        remaining = (numImages! - maxImage);
                                        if (remaining == 0) {
                                          return data[index]
                                                      .attachments!
                                                      .length <
                                                  0
                                              ? Container()
                                              : GestureDetector(
                                                  child: attachmentType!
                                                          .contains("image")
                                                      ? GestureDetector(
                                                          onTap:
                                                              connection ==
                                                                      false
                                                                  ? null
                                                                  : () {
                                                                      setState(
                                                                          () {
                                                                        BlocProvider.of<SchoolTimeLineDetailBloc>(context).add(SchoolTimeLineFetchDetailEvent(
                                                                            idPost:
                                                                                data[index].id!));
                                                                        Navigator.of(context).push(MaterialPageRoute(
                                                                            builder: (
                                                                          context,
                                                                        ) =>
                                                                                ViewDetailSchoolTilemine()));
                                                                      });
                                                                    },
                                                          child: CachedImageNetwork(
                                                              urlImage:
                                                                  attachmentUrl!),
                                                        )
                                                      : attachmentType
                                                              .contains("video")
                                                          ? GestureDetector(
                                                              onTap:
                                                                  connection ==
                                                                          false
                                                                      ? null
                                                                      : () {
                                                                          setState(
                                                                              () {
                                                                            BlocProvider.of<SchoolTimeLineDetailBloc>(context).add(SchoolTimeLineFetchDetailEvent(idPost: data[index].id!));
                                                                            Navigator.of(context).push(MaterialPageRoute(
                                                                                builder: (
                                                                              context,
                                                                            ) =>
                                                                                    ViewDetailSchoolTilemine()));
                                                                          });
                                                                        },
                                                              child:
                                                                  ThumbnailVideoWidget(
                                                                image:
                                                                    attachmentUrl,
                                                              ))
                                                          : Container());
                                        } else {
                                          return GestureDetector(
                                            onTap: () {},
                                            child: Stack(
                                              fit: StackFit.expand,
                                              children: [
                                                attachmentType!
                                                        .contains("image")
                                                    ? GestureDetector(
                                                        onTap:
                                                            connection == false
                                                                ? null
                                                                : () {
                                                                    setState(
                                                                        () {
                                                                      BlocProvider.of<SchoolTimeLineDetailBloc>(
                                                                              context)
                                                                          .add(SchoolTimeLineFetchDetailEvent(
                                                                              idPost: data[index].id!));
                                                                      Navigator.of(context).push(MaterialPageRoute(
                                                                          builder: (
                                                                        context,
                                                                      ) =>
                                                                              ViewDetailSchoolTilemine()));
                                                                    });
                                                                  },
                                                        child: CachedImageNetwork(
                                                            urlImage:
                                                                attachmentUrl!),
                                                      )
                                                    : attachmentType
                                                            .contains("video")
                                                        ? GestureDetector(
                                                            onTap:
                                                                connection ==
                                                                        false
                                                                    ? null
                                                                    : () {
                                                                        setState(
                                                                            () {
                                                                          BlocProvider.of<SchoolTimeLineDetailBloc>(context)
                                                                              .add(SchoolTimeLineFetchDetailEvent(idPost: data[index].id!));
                                                                          Navigator.of(context).push(MaterialPageRoute(
                                                                              builder: (
                                                                            context,
                                                                          ) =>
                                                                                  ViewDetailSchoolTilemine()));
                                                                        });
                                                                      },
                                                            child:
                                                                ThumbnailVideoWidget(
                                                              image:
                                                                  attachmentUrl,
                                                            ))
                                                        : Container(),
                                                Positioned.fill(
                                                  child: GestureDetector(
                                                    onTap: connection == false
                                                        ? null
                                                        : () {
                                                            setState(() {
                                                              BlocProvider.of<
                                                                          SchoolTimeLineDetailBloc>(
                                                                      context)
                                                                  .add(SchoolTimeLineFetchDetailEvent(
                                                                      idPost: data[
                                                                              index]
                                                                          .id!));
                                                              Navigator.of(
                                                                      context)
                                                                  .push(MaterialPageRoute(
                                                                      builder: (
                                                                context,
                                                              ) =>
                                                                          ViewDetailSchoolTilemine()));
                                                            });
                                                          },
                                                    child: Container(
                                                      alignment:
                                                          Alignment.center,
                                                      color: Colors.black54,
                                                      child: Text(
                                                        '+' +
                                                            remaining
                                                                .toString(),
                                                        style: const TextStyle(
                                                            fontSize: 32,
                                                            color:
                                                                Colors.white),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          );
                                        }
                                      } else {
                                        return attachmentType!.contains("image")
                                            ? GestureDetector(
                                                onTap: connection == false
                                                    ? null
                                                    : () {
                                                        setState(() {
                                                          BlocProvider.of<
                                                                      SchoolTimeLineDetailBloc>(
                                                                  context)
                                                              .add(SchoolTimeLineFetchDetailEvent(
                                                                  idPost: data[
                                                                          index]
                                                                      .id!));
                                                          Navigator.of(context).push(
                                                              MaterialPageRoute(
                                                                  builder: (
                                                            context,
                                                          ) =>
                                                                      ViewDetailSchoolTilemine()));
                                                        });
                                                      },
                                                child: CachedImageNetwork(
                                                    urlImage: attachmentUrl!),
                                              )
                                            : attachmentType.contains("video")
                                                ? GestureDetector(
                                                    onTap: connection == false
                                                        ? null
                                                        : () {
                                                            setState(() {
                                                              BlocProvider.of<
                                                                          SchoolTimeLineDetailBloc>(
                                                                      context)
                                                                  .add(SchoolTimeLineFetchDetailEvent(
                                                                      idPost: data[
                                                                              index]
                                                                          .id!));
                                                              Navigator.of(
                                                                      context)
                                                                  .push(MaterialPageRoute(
                                                                      builder: (
                                                                context,
                                                              ) =>
                                                                          ViewDetailSchoolTilemine()));
                                                            });
                                                          },
                                                    child: ThumbnailVideoWidget(
                                                      image: attachmentUrl,
                                                    ))
                                                : Container(
                                                    color: Colors.black);
                                      }
                                    }),
                                  ),
                    //------------------------ End Center Body ------------------

                    //------------------------File-------------------------------
                    displayFile: Container(
                      child: ListView.separated(
                        physics: const ScrollPhysics(),
                        padding: const EdgeInsets.all(0),
                        shrinkWrap: true,
                        itemCount: applications.length,
                        itemBuilder: (BuildContext contex, attindex) {
                          var path = applications[attindex].fileShow.toString();
                          var filename =
                              applications[attindex].fileName.toString();
                          if (attindex > 2) {
                            return Container(color: Colors.black);
                          } else {
                            return Column(
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Container(
                                  child: ThumbnailDocWidget(
                                    nameFile:
                                        applications[attindex].fileName ?? "",
                                    sizeFile:
                                        applications[attindex].fileSize ?? "",
                                    onPressed: connection == false
                                        ? null
                                        : () {
                                            setState(() {
                                              Navigator.of(context)
                                                  .push(MaterialPageRoute(
                                                      builder: (
                                                context,
                                              ) =>
                                                          ViewDocuments(
                                                            path: path,
                                                            filename: filename
                                                                .toString(),
                                                          )));
                                            });
                                          },
                                  ),
                                ),
                                attindex == 2
                                    ? TextButton(
                                        onPressed: connection == false
                                            ? null
                                            : () {
                                                setState(() {
                                                  BlocProvider.of<
                                                              SchoolTimeLineDetailBloc>(
                                                          context)
                                                      .add(
                                                          SchoolTimeLineFetchDetailEvent(
                                                              idPost:
                                                                  data[index]
                                                                      .id!));
                                                  Navigator.of(context)
                                                      .push(MaterialPageRoute(
                                                          builder: (
                                                    context,
                                                  ) =>
                                                              ViewDetailSchoolTilemine()));
                                                });
                                              },
                                        child: const Text(
                                          "See More",
                                          style: TextStyle(
                                              color:
                                                  Colorconstands.primaryColor,
                                              fontSize: 15.0),
                                        ))
                                    : Container()
                              ],
                            );
                          }
                        },
                        separatorBuilder: (BuildContext context, int index) {
                          return Container(
                            height: 8.0,
                            color: Colorconstands.gray,
                          );
                        },

                        //},
                      ),
                    ),
                    //------------------------End File------------------

                    //------------------------Button Like------------------
                    onReactionChanged: (String? values, bool isChecked) {
                      setState(() {
                        if (data[index].isReact == 0) {
                          data[index].totalReact = data[index].totalReact!;
                          if (data[index].reactionType == 0 ||
                              data[index].isReact == 0) {
                            data[index].totalReact =
                                data[index].totalReact! + 1;
                            isChecked = true;
                            if (values == "Like") {
                              selecticonreact = 0;
                              data[index].isReact = 1;
                              data[index].reactionType = 0;
                            } else if (values == "Love") {
                              selecticonreact = 1;
                              data[index].isReact = 1;
                              data[index].reactionType = 1;
                            } else if (values == "haha") {
                              selecticonreact = 2;
                              data[index].isReact = 1;
                              data[index].reactionType = 2;
                            } else if (values == "Wow") {
                              selecticonreact = 3;
                              data[index].isReact = 1;
                              data[index].reactionType = 3;
                            } else if (values == "Angry") {
                              selecticonreact = 5;
                              data[index].isReact = 1;
                              data[index].reactionType = 5;
                            } else {
                              selecticonreact = 0;
                              data[index].isReact = 1;
                              data[index].reactionType = 0;
                            }
                          } else {
                            if (values == 'Like') {
                              data[index].isReact = 1;
                              data[index].reactionType = 0;
                              // _audioCache.play('sounds/like.mp3');
                            } else if (values == 'Love') {
                              data[index].isReact = 1;
                              data[index].reactionType = 1;
                              // _audioCache.play('sounds/box_up.mp3');
                            } else if (values == 'haha') {
                              data[index].isReact = 1;
                              data[index].reactionType = 2;
                              // _audioCache.play('sounds/Laugh_Laugh.mp3');
                            } else if (values == 'Wow') {
                              data[index].isReact = 1;
                              data[index].reactionType = 3;
                              // _audioCache.play('sounds/Wow.mp3');
                            } else if (values == 'Unselect' &&
                                isChecked == true) {
                              isChecked = false;
                              selecticonreact = 0;
                              data[index].isReact = 0;
                              data[index].reactionType = 0;
                              data[index].totalReact =
                                  data[index].totalReact! - 1;
                            } else if (data[index].reactionType == 0) {
                              isChecked = true;
                              selecticonreact = 0;
                              data[index].isReact = 1;
                              data[index].reactionType = 0;
                            } else if (values == 'Angry') {
                              data[index].isReact = 1;
                              data[index].reactionType = 5;
                              // _audioCache.play('sounds/Nani_anime.mp3');
                            } else {
                              data[index].isReact = 1;
                              data[index].reactionType = 5;
                            }
                          }
                        } else if (data[index].reactionType == 6 ||
                            data[index].isReact == 0) {
                          data[index].totalReact = data[index].totalReact! + 1;
                          isChecked = true;
                          selecticonreact = 0;
                          data[index].isReact = 1;
                          data[index].reactionType = 0;
                        } else {
                          if (values == 'Like') {
                            data[index].isReact = 1;
                            data[index].reactionType = 0;
                            // _audioCache.play('sounds/like.mp3');
                          } else if (values == 'Love') {
                            data[index].isReact = 1;
                            data[index].reactionType = 1;
                            // _audioCache.play('sounds/box_up.mp3');
                          } else if (values == 'haha') {
                            data[index].isReact = 1;
                            data[index].reactionType = 2;
                            // _audioCache.play('sounds/Laugh_Laugh.mp3');
                          } else if (values == 'Wow') {
                            data[index].isReact = 1;
                            data[index].reactionType = 3;
                            // _audioCache.play('sounds/Wow.mp3');
                          } else if (isChecked == false &&
                              values == 'Unselect' &&
                              data[index].reactionType == 0) {
                            selecticonreact = 5;
                            data[index].reactionType = 5;
                            data[index].isReact = 1;
                          } else if (values == 'Unselect' &&
                              isChecked == true) {
                            isChecked = false;
                            selecticonreact = 0;
                            data[index].isReact = 0;
                            data[index].reactionType = 0;
                            data[index].totalReact =
                                data[index].totalReact! - 1;
                          } else {
                            data[index].isReact = 1;
                            data[index].reactionType = 5;
                            // _audioCache.('sounds/Nani_anime.mp3');
                          }
                        }
                      });
                      setState(() {
                        values == "Like"
                            ? reactiondefultselect =
                                "assets/images/actionButton/like_full_fill.png"
                            : values == "Love"
                                ? reactiondefultselect =
                                    "assets/images/actionButton/Love.png"
                                : values == "haha"
                                    ? reactiondefultselect =
                                        "assets/images/actionButton/Icon_haha.png"
                                    : values == "Wow"
                                        ? reactiondefultselect =
                                            "assets/images/actionButton/wow2.png"
                                        : values == "Angry"
                                            ? reactiondefultselect =
                                                "assets/images/actionButton/angry2.png"
                                            : reactiondefultselect =
                                                "assets/images/actionButton/like_full_fill.png";
                      });
                      PostReaction _postReact = PostReaction();
                      _postReact.postReact("/school-time-line/react/",
                          data[index].reactionType!, data[index].id!);
                    },
                    //------------------------End Button Like------------------
                    //===================== button comment ====================

                    onTapcomment: connection == false
                        ? null
                        : () {
                            BlocProvider.of<SchoolTimeLineDetailBloc>(context)
                                .add(
                              SchoolTimeLineFetchDetailEvent(
                                  idPost: data[index].id!),
                            );
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                fullscreenDialog: true,
                                builder: (context) {
                                  return const CommentPost();
                                },
                              ),
                            );
                          },

                    onTapBodycomment: connection == false
                        ? null
                        : () {
                            BlocProvider.of<SchoolTimeLineDetailBloc>(context)
                                .add(SchoolTimeLineFetchDetailEvent(
                                    idPost: data[index].id!));
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                fullscreenDialog: true,
                                builder: (context) {
                                  return const CommentPost();
                                },
                              ),
                            );
                          },

                    //===================== End button comment ====================
                  );
                } else {
                  // Timer(const Duration(milliseconds: 10), () {
                  //   _scrollController
                  //       .jumpTo(_scrollController.position.maxScrollExtent);
                  // });
                  return Container(
                    color: Colors.white,
                    child: Center(
                      child: isReachedMax == true
                          ? Container(
                              color: Colors.grey.withOpacity(.2),
                              width: MediaQuery.of(context).size.width,
                              padding: const EdgeInsets.symmetric(vertical: 30),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SvgPicture.asset(
                                    "assets/icons/carbon_message-queue.svg",
                                    width: 50,
                                    color: Colors.grey,
                                  ),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  Text(
                                    "All Data Loaded",
                                    style: ThemeConstands.button_SemiBold_16
                                        .copyWith(color: Colors.grey),
                                  ),
                                ],
                              ),
                            )
                          : const ShimmerTimeLine(),
                    ),
                  );
                }
              },
              separatorBuilder: (BuildContext context, int index) {
                return Container(
                  height: 8.0,
                  color: Colorconstands.gray,
                );
              },
            ),
          );
        },
      ),
    );
  }
}



  // void _saveScrollOffset() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   await prefs.setDouble('scrollOffset', _lastOffset);
  //   print("scrooolll ${_scrollController.offset} $_lastOffset");
  // }

  // void _loadScrollOffset() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   double offset = prefs.getDouble('scrollOffset') ?? 0.0;
  //   setState(() {
  //     _lastOffset = offset;
  //   });
  //   // _scrollController.animateTo(
  //   //   _lastOffset,
  //   //   duration: const Duration(milliseconds: 200),
  //   //   curve: Curves.easeIn,
  //   // );
  // }