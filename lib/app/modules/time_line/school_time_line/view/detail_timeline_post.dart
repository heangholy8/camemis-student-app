import 'dart:async';

import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:camis_application_flutter/widgets/profile_widget.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:expandable_text/expandable_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../widgets/cached_image.dart';
import '../../../../../widgets/error_widget/error_request.dart';
import '../../../../../widgets/no_connection_alert_widget.dart';
import '../../../../../widgets/shimmer_style.dart';
import '../../../../../widgets/view_document.dart';
import '../../widget/thumbnail_doc_widget.dart';
import '../../widget/thumbnail_widget.dart';
import '../../widget/view_image.dart';
import '../bloc/school_time_line_bloc.dart';
class ViewDetailSchoolTilemine extends StatefulWidget {
   ViewDetailSchoolTilemine({Key? key}) : super(key: key);
  @override

  State<ViewDetailSchoolTilemine> createState() => _ViewDetailSchoolTilemineState();
}
class _ViewDetailSchoolTilemineState extends State<ViewDetailSchoolTilemine> {
  ScrollController sc = ScrollController();
  bool connection = true; 
  StreamSubscription? stsub;
  @override
  void initState() {
    super.initState();
    //=============Check internet====================
    stsub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
      });
    });
   
   //=============Check internet====================
    //sc.jumpTo(2);
  }
  @override
  void dispose() {
    stsub?.cancel();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstands.neutralWhite,
      body: Stack(
        children: [
          Positioned(
            top: 0,left: 0,right: 0,bottom: 0,
            child: Container(
              padding:const EdgeInsets.only(top: 45.0),
              color: Colorconstands.white,
              child: BlocBuilder<SchoolTimeLineDetailBloc, SchoolTimeLineState>(
                builder: (context, state) {
                  if (state is SchoolTimeLineLoadingState) {
                    return  ListView.builder(
                      padding: const EdgeInsets.all(0),
                      shrinkWrap: true,
                      itemBuilder: (BuildContext contex,index){
                        return const ShimmerTimeLine();
                      },
                      itemCount: 3,
                    );
                  }
                  if (state is DetailTimeLineLoaded) {
                    var data = state.timeLineDetailmodel!.data;
                    var listImageVideo = data!.attachments!.where((element){
                                    return element.fileType!.contains("image") || element.fileType!.contains("video");
                                  }).toList();
                    var listappication = data.attachments!.where((element)=>element.fileType!.contains("application")).toList();
                    return Column(
                      children: [
                        Expanded(
                          child: SingleChildScrollView(
                            controller: sc,
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Container(
                                      margin:const EdgeInsets.only(left: 5.0,right: 6),
                                      child: IconButton(
                                        padding:const EdgeInsets.all(0),
                                        onPressed: (){Navigator.of(context).pop();},
                                        icon:const Icon(Icons.arrow_back_ios_rounded,size: 20,)
                                      ),
                                    ),
                                    Container(
                                    margin:const EdgeInsets.symmetric(horizontal: 5.0,vertical: 5),
                                    child: Row(
                                      children: [
                                        Container(
                                          margin:const EdgeInsets.only(right: 9.0),
                                          child: Profile(height: 45, imageProfile: data.user!.profileMedia!.fileShow.toString(), namechild: '', onPressed: () {  }, pandding: 0, sizetextname: 0, width: 45,),
                                        ),
                                        Container(
                                          alignment: Alignment.bottomCenter,
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.end,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Align(
                                                child: Text(translate=="en"?data.user!.nameEn==" "||data.user!.nameEn==" "?
                                                            data.user!.name.toString():data.user!.nameEn.toString():data.user!.name.toString(),style: ThemeConstands.texttheme.headline6!.copyWith(color: Colorconstands.black,fontWeight: FontWeight.w500),),
                                              ),
                                              Align(
                                                child: Text( data.createdDate.toString()+" •",style: ThemeConstands.texttheme.caption!.copyWith(color: Colorconstands.darkGray,fontWeight: FontWeight.w100),),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                              ),
                                  ],
                                ),
                              Container(
                                alignment: Alignment.centerLeft,
                                margin:const EdgeInsets.only(left: 22.0,right: 22.0,top: 15.0,bottom: 12.0),
                                child: ExpandableText(
                                  data.postText.toString(),
                                  expandText: 'show more',
                                  collapseText: 'show less',
                                  maxLines: 2,
                                  style:const TextStyle(fontSize: 16.0),
                                  linkColor: Colorconstands.primaryColor,
                                ),
                              ),
                              //------------------------File-------------------------------
                              ListView.builder(
                                physics:const ScrollPhysics(),
                                padding:const EdgeInsets.all(0),
                                shrinkWrap: true,
                                itemCount: listappication.length,
                                itemBuilder: (BuildContext contex,index){
                                  var path = listappication[index].fileShow.toString();
                                  var filename = listappication[index].fileName.toString();
                                  return ThumbnailDocWidget(
                                      nameFile: listappication[index].fileName??"",
                                      sizeFile: listappication[index].fileSize??"",
                                      onPressed:connection==false?null:(){
                                        setState(() {
                                            Navigator.of(context).push(MaterialPageRoute(builder : (context,)=> ViewDocuments(path: path,filename: filename.toString(),)));
                                        });
                                      },
                                    );
                                },
                              ),
                            //------------------------End File------------------
                              ListView.builder(
                                physics:const ScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: listImageVideo.length,
                                itemBuilder: (BuildContext contex,int index){
                                  var attachmentUrl = listImageVideo[index].fileShow;
                                  var attachmentUrlvideo = listImageVideo[index].fileThumbnail;
                                  var attachmentType = listImageVideo[index].fileType;
                                  return GestureDetector(
                                      onTap: (){
                                        Navigator.push(context,PageRouteBuilder(pageBuilder: (_, __, ___) => ImageViewDownloads(listimagevide: listImageVideo,activepage: index,),transitionDuration: const Duration(seconds: 0),),
                                        );
                                      },
                                      child: Container(
                                        margin:const EdgeInsets.only(top: 6.0,left: 6.0,right: 6.0),
                                        child: attachmentType!.contains("image")? CachedImageNetwork(urlImage: attachmentUrl!,)
                                          :attachmentType.contains("video")
                                          ?ThumbnailVideoWidget(
                                            image: attachmentUrlvideo,
                                          ):Container()
                                        ),
                                    
                                     
                                  );
                                },
                              )
                              ],
                            ),
                          ),
                        ),
                      ],
                    );
                  }
                  else {
                    return Center(
                      child: ErrorRequestData(
                          onPressed: (){
                            Navigator.of(context).pop();
                          },
                          discription: '', 
                          hidebutton: true, 
                          title: 'WE_DETECT_ERROR'.tr(),

                        ),
                    );
                  }
                }
              ),
            ),
          ),
          AnimatedPositioned(
            bottom: connection == true ? -150 : 0,
            left: 0,
            right: 0,
            duration: const Duration(milliseconds: 500),
            child: const NoConnectWidget(),
          ),
        ],
      ),
      
    );
  }
  
}