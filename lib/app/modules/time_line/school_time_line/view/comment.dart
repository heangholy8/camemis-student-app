
import 'dart:async';

import 'package:camis_application_flutter/app/modules/time_line/school_time_line/bloc/school_time_line_bloc.dart';
import 'package:camis_application_flutter/app/modules/time_line/widget/comment_widget.dart';
import 'package:camis_application_flutter/service/apis/time_line_api/post_comment.dart';
import 'package:camis_application_flutter/widgets/shimmer_style.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../../../service/apis/time_line_api/post_react.dart';
import '../../../../../widgets/no_connection_alert_widget.dart';
import '../../../auth_screens/e_school_code.dart';

class CommentPost extends StatefulWidget {
  const CommentPost({Key? key}) : super(key: key);

  @override
  State<CommentPost> createState() => _CommentPostState();
}

class _CommentPostState extends State<CommentPost> {
   GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
   final FocusNode _titleFocus = FocusNode();
   final TextEditingController _textcomment = TextEditingController();
   final ScrollController _controller = ScrollController();
    String replyName = "";
    String replyText = "";
    bool reply = false;
  int? idPost;
  int? idReplyComment;
  bool connection = true; 
  StreamSubscription? stsub;

  @override
  void initState() {
    //=============Check internet====================
    stsub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
        if(connection==false){
          FocusScope.of(context).unfocus();
        }
      });
    });
   
   //=============Check internet====================
    super.initState();
  }
  @override
  void dispose() {
    stsub!.cancel();
    _titleFocus.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
     final translate = context.locale.toString();
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            top: 0,bottom: 0,left: 0,right: 0,
            child: Container(
              color:Colorconstands.neutralWhite,
              child: Column(
                children: [
                  Container(
                    margin:const EdgeInsets.only(left:12.0,top: 35),
                    alignment: Alignment.centerLeft,
                    child: IconButton(
                      splashColor: Colors.transparent,
                      hoverColor:Colors.transparent,
                      highlightColor: Colors.transparent,
                      padding:const EdgeInsets.all(0),
                      icon:const Icon(Icons.arrow_back_ios,size: 22.0,color: Colorconstands.darkGray,),
                      onPressed: (){
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                  BlocBuilder<SchoolTimeLineDetailBloc, SchoolTimeLineState>(
                    builder: (context, state) {
                      if (state is SchoolTimeLineLoadingState) {
                         return  Expanded(
                          child: ListView.builder(
                            shrinkWrap: true,
                            itemBuilder: (BuildContext contex,index){
                              return const ShimmerComment();
                            },
                            itemCount: 2,
                          ),
                        );
                      }
                      else if (state is DetailTimeLineLoaded) {
                        var data = state.timeLineDetailmodel!.data;
                        idPost = data!.id!.toInt();
                        return  Expanded(
                          child: data.comments!.isEmpty?
                          ListView.builder(
                          padding:const EdgeInsets.all(0),
                          shrinkWrap: true,
                          itemBuilder: (BuildContext contex,index){
                            idPost = data.id!.toInt();
                            return Container(
                              child: Column(
                                children: [
                                  Image.asset("assets/images/gifs/no_comment.gif",width: MediaQuery.of(context).size.width/1.8,),
                                  const SizedBox(height: 12.0,),
                                  Text("NOT_COMMENT".tr(),style: ThemeConstands.texttheme.headline6,)
                                ],
                              ),
                            );
                          },
                          itemCount: 1,
                          )
                          :GestureDetector(
                            onTap: (() {
                              FocusScope.of(context).unfocus();
                            }),
                            child: Container(
                              margin:const EdgeInsets.symmetric(vertical: 12.0),
                              child: ListView.builder(
                                padding:const EdgeInsets.only(left: 12,right: 12,bottom: 12),
                                controller: _controller,
                                shrinkWrap: true,
                                itemBuilder: (BuildContext contex,index){
                                  int selecticonreact = 5;
                                  return Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      WidgetComment(
                                        hideReact: false,
                                        marginprofileLeft: 12.0,
                                        isCheckroot: true,
                                        lenghtreply: data.comments![index].comments!.length,
                                        margincommentbetweenprofile: 65.0,
                                        namecomment: translate=="en"?data.comments![index].user!.nameEn==" "||data.comments![index].user!.nameEn==""?
                                            data.comments![index].user!.name.toString():
                                            data.comments![index].user!.nameEn.toString():
                                            data.comments![index].user!.name.toString(),
                                        profilecomment: data.comments![index].user!.profileMedia!.fileThumbnail.toString(),
                                        textcomment: data.comments![index].postText.toString(),
                                        //------------------------- Icon React--------------------------
                                        iconReact:data.comments![index].isReact==0?"assets/icons/like_icon.png"
                                        :data.comments![index].reactionType==0?"assets/images/actionButton/like_full_fill.png"
                                        :data.comments![index].reactionType==1?"assets/images/actionButton/Love.png"
                                        :data.comments![index].reactionType==2?"assets/images/actionButton/Icon_haha.png"
                                        :data.comments![index].reactionType==3?"assets/images/actionButton/wow2.png"
                                        :data.comments![index].reactionType==5?"assets/images/actionButton/angry2.png":"assets/icons/like_icon.png",
                                        //-------------------------End Icon React--------------------------
                                        textReact:data.comments![index].isReact==0?"Like":data.comments![index].reactionType==0?"Like":data.comments![index].reactionType==1?"Love":data.comments![index].reactionType==3?"Wow":data.comments![index].reactionType==2?"haha":data.comments![index].reactionType==5?"angry":"Like",
                                        profilehight: 45, 
                                        profilewidth: 45, 
                                        style: ThemeConstands.texttheme.subtitle1!.copyWith(color: Colorconstands.black,fontWeight: FontWeight.w500), 
                                        styletextpost: ThemeConstands.texttheme.subtitle1!.copyWith(color: Colorconstands.darkGray,),
                                        //------------------------- color Label React--------------------------
                                        //colorlabelreact: Colorconstands.primaryColor,
                                        colorlabelreact:data.comments![index].isReact==0?Colorconstands.primaryColor
                                        :data.comments![index].reactionType==0?Colorconstands.primaryColor
                                        :data.comments![index].reactionType==1?Colors.red
                                        :data.comments![index].reactionType==2?Colors.yellow
                                        :data.comments![index].reactionType==5? Colors.red 
                                        :data.comments![index].reactionType==6?Colorconstands.primaryColor
                                        :Colors.yellow,   
                                        //-------------------------End color Label React--------------------------
                                        selectedReaction: selecticonreact, 
                                        reply: () {
                                          setState(() {
                                            reply=true;
                                            replyName =translate=="en"?data.comments![index].user!.nameEn==" "||data.comments![index].user!.nameEn==""?
                                            data.comments![index].user!.name.toString():
                                            data.comments![index].user!.nameEn.toString():
                                            data.comments![index].user!.name.toString();
                                            replyText = data.comments![index].postText.toString();
                                            idReplyComment = data.comments![index].id;
                                          });
                                        },
                                        onReact: (String? values, bool isChecked){
                                           setState(() {
                                          if(data.comments![index].isReact==0){
                                            data.comments![index].totalReact =data.comments![index].totalReact!;
                                            if(data.comments![index].reactionType ==0||data.comments![index].isReact==0){
                                              data.comments![index].totalReact =data.comments![index].totalReact! +1;
                                                isChecked=true;
                                                if(values=="Like"){
                                                selecticonreact=0;
                                                data.comments![index].isReact=1;
                                                data.comments![index].reactionType =0;
                                                }
                                                else if(values=="Love"){
                                                selecticonreact=1;
                                                data.comments![index].isReact=1;
                                                data.comments![index].reactionType=1;
                                                }
                                                else if(values=="haha"){
                                                selecticonreact=2;
                                                data.comments![index].isReact=1;
                                                data.comments![index].reactionType =2;
                                                }
                                                else if(values=="Wow"){
                                                selecticonreact=3;
                                                data.comments![index].isReact=1;
                                                data.comments![index].reactionType =3;
                                                }
                                                
                                                else if(values=="Angry"){
                                                selecticonreact=5;
                                                data.comments![index].isReact=1;
                                                data.comments![index].reactionType =5;
                                                }
                                                else{
                                                selecticonreact=0;
                                                data.comments![index].isReact=1;
                                                data.comments![index].reactionType =0;
                                                }
                                              }
                                            else{
                                            if(values=='Like'){
                                              data.comments![index].isReact=1;
                                                data.comments![index].reactionType =0;
                                                // _audioCache.play('sounds/like.mp3');
                                              }
                                              else if(values=='Love'){
                                                data.comments![index].isReact=1;
                                                data.comments![index].reactionType =1;
                                                // _audioCache.play('sounds/box_up.mp3');
                                              }
                                              else if(values=='haha'){
                                                data.comments![index].isReact=1;
                                                data.comments![index].reactionType =2;
                                                // _audioCache.play('sounds/Laugh_Laugh.mp3');
                                              }
                                              else if(values=='Wow'){
                                                data.comments![index].isReact=1;
                                                data.comments![index].reactionType =3;
                                                // _audioCache.play('sounds/Wow.mp3');
                                              }
                                            
                                              else if(values=='Unselect'&&isChecked==true){
                                                isChecked=false;
                                                selecticonreact=0;
                                                data.comments![index].isReact=0;
                                                data.comments![index].reactionType =0;
                                                data.comments![index].totalReact =data.comments![index].totalReact! -1;
                                              }
                                              else if(data.comments![index].reactionType ==0){
                                                isChecked=true;
                                                selecticonreact=0;
                                                data.comments![index].isReact=1;
                                                data.comments![index].reactionType =0;
                                              }
                                              else if(values=='Angry'){
                                                data.comments![index].isReact=1;
                                                data.comments![index].reactionType =5;
                                                // _audioCache.play('sounds/Nani_anime.mp3');
                                              }
                                              else{
                                                data.comments![index].isReact=1;
                                                data.comments![index].reactionType =5;
                                              }
                                            }
                                          }
                                          else 
                                            if(data.comments![index].reactionType==6||data.comments![index].isReact==0){
                                                data.comments![index].totalReact =data.comments![index].totalReact! +1;
                                                isChecked=true;
                                                selecticonreact=0;
                                                data.comments![index].isReact=1;
                                                data.comments![index].reactionType =0;
                                            }
                                            else{
                                            if(values=='Like'){
                                              data.comments![index].isReact=1;
                                                data.comments![index].reactionType =0;
                                                // _audioCache.play('sounds/like.mp3');
                                              }
                                              else if(values=='Love'){
                                                data.comments![index].isReact=1;
                                                data.comments![index].reactionType =1;
                                                // _audioCache.play('sounds/box_up.mp3');
                                              }
                                              else if(values=='haha'){
                                                data.comments![index].isReact=1;
                                                data.comments![index].reactionType =2;
                                                // _audioCache.play('sounds/Laugh_Laugh.mp3');
                                              }
                                              else if(values=='Wow'){
                                                data.comments![index].isReact=1;
                                                data.comments![index].reactionType =3;
                                                // _audioCache.play('sounds/Wow.mp3');
                                              }
                                            
                                              else if(isChecked==false&&values=='Unselect'&&data.comments![index].reactionType ==0){
                                                selecticonreact=5;
                                                data.comments![index].reactionType =5;
                                                data.comments![index].isReact=1;
                                              }
                                              else if(values=='Unselect'&&isChecked==true){
                                                isChecked=false;
                                                selecticonreact=0;
                                                data.comments![index].isReact=0;
                                                data.comments![index].reactionType =0;
                                                data.comments![index].totalReact =data.comments![index].totalReact! -1;
                                              }
                                              else{
                                                data.comments![index].isReact=1;
                                                data.comments![index].reactionType =5;
                                                // _audioCache.('sounds/Nani_anime.mp3');
                                              }
                                            }
                                          }
                                          );
                                          //=======================Event post React=======================
                                           PostReaction _postReact = PostReaction();
                                          _postReact.postReact("/school-time-line/react/",data.comments![index].reactionType!, data.comments![index].id!);
                                           //======================= End Event post React=======================
                                        }, 
                                        
                                      ),
                                      data.comments![index].comments!.isEmpty?Container():
                                          Container(
                                            margin:const EdgeInsets.only(left: 0,right: 0.0),
                                            child: Stack(
                                              children: [
                                                ListView.builder(
                                                  padding:const EdgeInsets.all(0),
                                                  physics:const ScrollPhysics(),
                                                  shrinkWrap: true,
                                                  itemCount: data.comments![index].comments!.length - 1,
                                                   itemBuilder: (BuildContext contex,subIndex){
                                                    return Stack(
                                                      children: [
                                                        Positioned(
                                                          top: 0,left: 35,bottom: 0,
                                                          child: Container(
                                                            color: Colorconstands.gray,
                                                            width: 1,
                                                          ),
                                                        ),
                                                        
                                                        Container(
                                                           margin:const EdgeInsets.only(left: 65),
                                                          child: WidgetComment(
                                                            hideReact: false,
                                                            marginprofileLeft: 5.0,
                                                            isCheckroot: false,
                                                            lenghtreply: data.comments![index].comments!.length,
                                                            margincommentbetweenprofile: 50.0,
                                                            namecomment: translate=="en"?data.comments![index].comments![subIndex].user!.nameEn==" "||data.comments![index].comments![subIndex].user!.nameEn==""?
                                                                  data.comments![index].comments![subIndex].user!.name.toString():
                                                                  data.comments![index].comments![subIndex].user!.nameEn.toString():
                                                                  data.comments![index].comments![subIndex].user!.name.toString(),
                                                            profilecomment: data.comments![index].comments![subIndex].user!.profileMedia!.fileThumbnail.toString(),
                                                            textcomment: data.comments![index].comments![subIndex].postText.toString(),
                                                            //------------------------- Icon React--------------------------
                                                            iconReact:"assets/icons/like_icon.png",
                                                            //-------------------------End Icon React--------------------------
                                                            textReact:data.comments![index].comments![subIndex].isReact==0?"Like":data.comments![index].comments![subIndex].reactionType==0?"Like":data.comments![index].comments![subIndex].reactionType==1?"Love":data.comments![index].comments![subIndex].reactionType==3?"Wow":data.comments![index].comments![subIndex].reactionType==2?"haha":data.comments![index].comments![subIndex].reactionType==5?"angry":"Like",
                                                            profilehight: 30, 
                                                            profilewidth: 30, 
                                                            style: ThemeConstands.texttheme.subtitle2!.copyWith(color: Colorconstands.black,fontWeight: FontWeight.w500), 
                                                            styletextpost: ThemeConstands.texttheme.subtitle2!.copyWith(color: Colorconstands.darkGray,),
                                                            //------------------------- color Label React--------------------------
                                                            //colorlabelreact: Colorconstands.primaryColor,
                                                            colorlabelreact:Colors.yellow,   
                                                            //-------------------------End color Label React--------------------------
                                                            selectedReaction: selecticonreact, 
                                                            reply: () {  }, 
                                                            onReact: (String? values, bool isChecked){ }, 
                                                            
                                                          ),
                                                        ),
                                                      ],
                                                    );
                                                   }
                                                ),
                                                Container(
                                                  margin:const EdgeInsets.only(left: 0),
                                                  child: ListView.builder(
                                                    padding:const EdgeInsets.all(0),
                                                    physics:const ScrollPhysics(),
                                                    shrinkWrap: true,
                                                    itemCount: data.comments![index].comments!.length,
                                                     itemBuilder: (BuildContext contex,subIndex){
                                                      return Stack(
                                                        children: [
                                                          Positioned(
                                                            top: 0,left: 35,right: 0,
                                                            child: Container(
                                                              height: 25,
                                                              decoration: BoxDecoration(
                                                                border: Border.all(
                                                                  color: Colorconstands.gray,
                                                                  width: 1.0,
                                                                  style: BorderStyle.solid),
                                                                borderRadius:const BorderRadius.only(bottomLeft: Radius.circular(16))
                                                              ),
                                                            ),
                                                          ),
                                                          Positioned(
                                                            top: 0,left: 36,right: 0,
                                                            child: Container(
                                                              height: 1,
                                                              color: Colorconstands.white,
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:const EdgeInsets.only(left: 65),
                                                            color:Colorconstands.white,
                                                            child: WidgetComment(
                                                              hideReact: false,
                                                              marginprofileLeft: 5.0,
                                                              isCheckroot: false,
                                                              lenghtreply: data.comments![index].comments!.length,
                                                              margincommentbetweenprofile: 50.0,
                                                              namecomment: translate=="en"?data.comments![index].comments![subIndex].user!.nameEn==" "||data.comments![index].comments![subIndex].user!.nameEn==""?
                                                                  data.comments![index].comments![subIndex].user!.name.toString():
                                                                  data.comments![index].comments![subIndex].user!.nameEn.toString():
                                                                  data.comments![index].comments![subIndex].user!.name.toString(),
                                                              profilecomment: data.comments![index].comments![subIndex].user!.profileMedia!.fileThumbnail.toString(),
                                                              textcomment: data.comments![index].comments![subIndex].postText.toString(),
                                                              //------------------------- Icon React--------------------------
                                                              iconReact:data.comments![index].comments![subIndex].isReact==0?"assets/icons/like_icon.png"
                                                              :data.comments![index].comments![subIndex].reactionType==0?"assets/images/actionButton/like_full_fill.png"
                                                              :data.comments![index].comments![subIndex].reactionType==1?"assets/images/actionButton/Love.png"
                                                              :data.comments![index].comments![subIndex].reactionType==2?"assets/images/actionButton/Icon_haha.png"
                                                              :data.comments![index].comments![subIndex].reactionType==3?"assets/images/actionButton/wow2.png"
                                                              :data.comments![index].comments![subIndex].reactionType==5?"assets/images/actionButton/angry2.png":"assets/icons/like_icon.png",
                                                              //-------------------------End Icon React--------------------------
                                                              textReact:data.comments![index].comments![subIndex].isReact==0?"Like":data.comments![index].comments![subIndex].reactionType==0?"Like":data.comments![index].comments![subIndex].reactionType==1?"Love":data.comments![index].comments![subIndex].reactionType==3?"Wow":data.comments![index].comments![subIndex].reactionType==2?"haha":data.comments![index].comments![subIndex].reactionType==5?"angry":"Like",
                                                              profilehight: 30, 
                                                              profilewidth: 30, 
                                                              style: ThemeConstands.texttheme.subtitle2!.copyWith(color: Colorconstands.black,fontWeight: FontWeight.w500), 
                                                              styletextpost: ThemeConstands.texttheme.subtitle2!.copyWith(color: Colorconstands.darkGray,),
                                                              //------------------------- color Label React--------------------------
                                                              //colorlabelreact: Colorconstands.primaryColor,
                                                              colorlabelreact:data.comments![index].comments![subIndex].isReact==0?Colorconstands.primaryColor
                                                              :data.comments![index].comments![subIndex].reactionType==0?Colorconstands.primaryColor
                                                              :data.comments![index].comments![subIndex].reactionType==1?Colors.red
                                                              :data.comments![index].comments![subIndex].reactionType==2?Colors.yellow
                                                              :data.comments![index].comments![subIndex].reactionType==5? Colors.red 
                                                              :data.comments![index].comments![subIndex].reactionType==6?Colorconstands.primaryColor
                                                              :Colors.yellow,   
                                                              //-------------------------End color Label React--------------------------
                                                              selectedReaction: selecticonreact, 
                                                              reply: () {
                                                                setState(() {
                                                                  reply=true;
                                                                  replyName =translate=="en"?data.comments![index].comments![subIndex].user!.nameEn==" "||data.comments![index].comments![subIndex].user!.nameEn==""?
                                                                  data.comments![index].comments![subIndex].user!.name.toString():
                                                                  data.comments![index].comments![subIndex].user!.nameEn.toString():
                                                                  data.comments![index].comments![subIndex].user!.name.toString();
                                                                  replyText = data.comments![index].comments![subIndex].postText.toString();
                                                                  idReplyComment = data.comments![index].id;
                                                                });
                                                              }, 
                                                              onReact: (String? values, bool isChecked){
                                                                setState(() {
                                                                if(data.comments![index].comments![subIndex].isReact==0){
                                                                  data.comments![index].comments![subIndex].totalReact =data.comments![index].comments![subIndex].totalReact!;
                                                                  if(data.comments![index].comments![subIndex].reactionType ==0||data.comments![index].comments![subIndex].isReact==0){
                                                                    data.comments![index].comments![subIndex].totalReact =data.comments![index].comments![subIndex].totalReact! +1;
                                                                      isChecked=true;
                                                                      if(values=="Like"){
                                                                      selecticonreact=0;
                                                                      data.comments![index].comments![subIndex].isReact=1;
                                                                      data.comments![index].comments![subIndex].reactionType =0;
                                                                      }
                                                                      else if(values=="Love"){
                                                                      selecticonreact=1;
                                                                      data.comments![index].comments![subIndex].isReact=1;
                                                                      data.comments![index].comments![subIndex].reactionType=1;
                                                                      }
                                                                      else if(values=="haha"){
                                                                      selecticonreact=2;
                                                                      data.comments![index].comments![subIndex].isReact=1;
                                                                      data.comments![index].comments![subIndex].reactionType =2;
                                                                      }
                                                                      else if(values=="Wow"){
                                                                      selecticonreact=3;
                                                                      data.comments![index].comments![subIndex].isReact=1;
                                                                      data.comments![index].comments![subIndex].reactionType =3;
                                                                      }
                                                                      
                                                                      else if(values=="Angry"){
                                                                      selecticonreact=5;
                                                                      data.comments![index].comments![subIndex].isReact=1;
                                                                      data.comments![index].comments![subIndex].reactionType =5;
                                                                      }
                                                                      else{
                                                                      selecticonreact=0;
                                                                      data.comments![index].comments![subIndex].isReact=1;
                                                                      data.comments![index].comments![subIndex].reactionType =0;
                                                                      }
                                                                    }
                                                                  else{
                                                                  if(values=='Like'){
                                                                    data.comments![index].comments![subIndex].isReact=1;
                                                                      data.comments![index].comments![subIndex].reactionType =0;
                                                                      // _audioCache.play('sounds/like.mp3');
                                                                    }
                                                                    else if(values=='Love'){
                                                                      data.comments![index].comments![subIndex].isReact=1;
                                                                      data.comments![index].comments![subIndex].reactionType =1;
                                                                      // _audioCache.play('sounds/box_up.mp3');
                                                                    }
                                                                    else if(values=='haha'){
                                                                      data.comments![index].comments![subIndex].isReact=1;
                                                                      data.comments![index].comments![subIndex].reactionType =2;
                                                                      // _audioCache.play('sounds/Laugh_Laugh.mp3');
                                                                    }
                                                                    else if(values=='Wow'){
                                                                      data.comments![index].comments![subIndex].isReact=1;
                                                                      data.comments![index].comments![subIndex].reactionType =3;
                                                                      // _audioCache.play('sounds/Wow.mp3');
                                                                    }
                                                                  
                                                                    else if(values=='Unselect'&&isChecked==true){
                                                                      isChecked=false;
                                                                      selecticonreact=0;
                                                                      data.comments![index].comments![subIndex].isReact=0;
                                                                      data.comments![index].comments![subIndex].reactionType =0;
                                                                      data.comments![index].comments![subIndex].totalReact =data.comments![index].comments![subIndex].totalReact! -1;
                                                                    }
                                                                    else if(data.comments![index].comments![subIndex].reactionType ==0){
                                                                      isChecked=true;
                                                                      selecticonreact=0;
                                                                      data.comments![index].comments![subIndex].isReact=1;
                                                                      data.comments![index].comments![subIndex].reactionType =0;
                                                                    }
                                                                    else if(values=='Angry'){
                                                                      data.comments![index].comments![subIndex].isReact=1;
                                                                      data.comments![index].comments![subIndex].reactionType =5;
                                                                      // _audioCache.play('sounds/Nani_anime.mp3');
                                                                    }
                                                                    else{
                                                                      data.comments![index].comments![subIndex].isReact=1;
                                                                      data.comments![index].comments![subIndex].reactionType =5;
                                                                    }
                                                                  }
                                                                }
                                                                else 
                                                                  if(data.comments![index].comments![subIndex].reactionType==6||data.comments![index].comments![subIndex].isReact==0){
                                                                      data.comments![index].comments![subIndex].totalReact =data.comments![index].comments![subIndex].totalReact! +1;
                                                                      isChecked=true;
                                                                      selecticonreact=0;
                                                                      data.comments![index].comments![subIndex].isReact=1;
                                                                      data.comments![index].comments![subIndex].reactionType =0;
                                                                  }
                                                                  else{
                                                                  if(values=='Like'){
                                                                    data.comments![index].comments![subIndex].isReact=1;
                                                                      data.comments![index].comments![subIndex].reactionType =0;
                                                                      // _audioCache.play('sounds/like.mp3');
                                                                    }
                                                                    else if(values=='Love'){
                                                                      data.comments![index].comments![subIndex].isReact=1;
                                                                      data.comments![index].comments![subIndex].reactionType =1;
                                                                      // _audioCache.play('sounds/box_up.mp3');
                                                                    }
                                                                    else if(values=='haha'){
                                                                      data.comments![index].comments![subIndex].isReact=1;
                                                                      data.comments![index].comments![subIndex].reactionType =2;
                                                                      // _audioCache.play('sounds/Laugh_Laugh.mp3');
                                                                    }
                                                                    else if(values=='Wow'){
                                                                      data.comments![index].comments![subIndex].isReact=1;
                                                                      data.comments![index].comments![subIndex].reactionType =3;
                                                                      // _audioCache.play('sounds/Wow.mp3');
                                                                    }
                                                                 
                                                                    else if(isChecked==false&&values=='Unselect'&&data.comments![index].comments![subIndex].reactionType ==0){
                                                                      selecticonreact=5;
                                                                      data.comments![index].comments![subIndex].reactionType =5;
                                                                      data.comments![index].comments![subIndex].isReact=1;
                                                                    }
                                                                    else if(values=='Unselect'&&isChecked==true){
                                                                      isChecked=false;
                                                                      selecticonreact=0;
                                                                      data.comments![index].comments![subIndex].isReact=0;
                                                                      data.comments![index].comments![subIndex].reactionType =0;
                                                                      data.comments![index].comments![subIndex].totalReact =data.comments![index].comments![subIndex].totalReact! -1;
                                                                    }
                                                                    else{
                                                                      data.comments![index].comments![subIndex].isReact=1;
                                                                      data.comments![index].comments![subIndex].reactionType =5;
                                                                      // _audioCache.('sounds/Nani_anime.mp3');
                                                                    }
                                                                  }
                                                                }
                                                                );
                                                                //=======================Event post React=======================
                                                                  PostReaction _postReact = PostReaction();
                                                                  _postReact.postReact("/school-time-line/react/",data.comments![index].comments![subIndex].reactionType!, data.comments![index].comments![subIndex].id!);
                                                                  //======================= End Event post React=======================
                                                              }, 
                                                              
                                                            ),
                                                          ),
                                                        ],
                                                      );
                                                     }
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      );
                                    
                                },
                                itemCount: data.comments!.length,
                              ),
                            ),
                          ),
                        );
                      }
                      else{
                        return Expanded(
                          child:Container(),
                        );
                      }
                    }
                  ),
                  Container(
                    color:Colorconstands.gray.withOpacity(0.4),
                    child: Column(
                      children: [
                        const Divider(
                          height: 1,
                          thickness: 1,
                          color: Colorconstands.gray,
                        ),
                        reply==false?Container():Container(
                          margin:const EdgeInsets.only(left: 12.0,right:12.0,top: 12.0),
                          child: Row(
                            children: [
                              Expanded(child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Replying to " + replyName,style: ThemeConstands.texttheme.subtitle2!.copyWith(color: Colorconstands.black,fontWeight: FontWeight.bold),),
                                   Text(replyText,style: ThemeConstands.texttheme.subtitle2!.copyWith(color: Colorconstands.black,),maxLines: 1,overflow: TextOverflow.ellipsis,),
                                ],
                              )),
                              InkWell(
                                onTap: (){
                                  setState(() {
                                    reply=false;
                                  });
                                },
                                child: Container(
                                  width: 50,
                                  margin: const EdgeInsets.all(4),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colorconstands.darkGray.withOpacity(0.5),
                                  ),
                                  child:const Icon(
                                    Icons.close,
                                    size: 20,color: Colorconstands.gray,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin:const EdgeInsets.only(bottom: 25,left: 12.0,right:12.0,top: 12.0),
                          child: Form(
                            key: _formKey,
                            child: TextFormField(
                              onChanged:((value) {
                                setState(() {
                                  _textcomment.text;
                                });
                              }),
                              keyboardType: TextInputType.multiline,
                              maxLines: 5,
                              minLines: 1,
                              controller: _textcomment,
                              focusNode: _titleFocus,
                              autofocus: true,
                            decoration: InputDecoration(
                                hintText: "Comment....",
                                hintStyle: const TextStyle(),
                                fillColor: Colors.white,
                                filled: true,
                                contentPadding:
                                    const EdgeInsets.symmetric(vertical: 0, horizontal: 10.0),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(12.0),
                                  borderSide: const BorderSide(),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(12.0),
                                  borderSide: const BorderSide(
                                    color: Colorconstands.gray,
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(12.0),
                                  borderSide: BorderSide(
                                    color: const Color(0xFFDADADA).withOpacity(.7),
                                  ),
                                ),
                                suffixIcon:_textcomment.text==""||_textcomment.text==null?Container(width: 45,): InkWell(
                                      onTap: () {
                                        setState(() {
                                          //=======================Event post React=======================
                                            PostComment _postComment = PostComment();
                                            _postComment.postComment("/school-time-line/post-comment/",_textcomment.text,reply==true?idReplyComment!.toInt():idPost!.toInt()).then((value){
                                              BlocProvider.of<SchoolTimeLineDetailBloc>(context).add(SchoolTimeLineFetchDetailEvent(idPost:idPost!.toInt()));
                                              setState(() {
                                              reply = false;
                                            });
                                            });
                                          //======================= End Event post React=======================
                                        });
                                        FocusScope.of(context).unfocus();
                                        _textcomment.text="";
                                        
                                      },
                                      child: Container(
                                        margin: const EdgeInsets.all(4),
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.grey.shade100,
                                        ),
                                        child:const Icon(
                                          Icons.send,
                                          size: 20,color:Colorconstands.primaryColor,
                                        ),
                                      ),
                                    ),
                              ),
                            ),
                          )
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          AnimatedPositioned(
            bottom: connection == true ? -150 : 0,
            left: 0,
            right: 0,
            duration: const Duration(milliseconds: 500),
            child: const NoConnectWidget(),
          ),
        ],
      ),
    );
  }
}