import 'package:camis_application_flutter/app/modules/auth_screens/e_school_code.dart';
import 'package:equatable/equatable.dart';
import '../../../../../model/base/base_respone_model.dart';
import '../../../../../model/timeline/detail_school_time_line_model.dart';
import '../../../../../model/timeline/school_timeline.dart';
import '../../../../../service/apis/time_line_api/get_time_line_api.dart';

part 'school_time_line_event.dart';
part 'school_time_line_state.dart';

class SchoolTimeLineDetailBloc
    extends Bloc<SchoolTimeLineEvent, SchoolTimeLineState> {
  final GetSchoolTimeLineApi getSchooltimelinedetailapi;
  SchoolTimeLineDetailBloc({required this.getSchooltimelinedetailapi})
      : super(SchoolTimeLineInitial()) {
    on<SchoolTimeLineFetchDetailEvent>((event, emit) async {
      emit(const SchoolTimeLineLoadingState(message: "Loading....."));
      try {
        var detaildata = await getSchooltimelinedetailapi
            .getSchoolTimelineDetailApi(postId: event.idPost);
        emit(DetailTimeLineLoaded(timeLineDetailmodel: detaildata));
      } catch (e) {
        emit(const SchoolTimeLineErrorState(error: "Error data"));
      }
    });
  }
}

/// New Bloc School time line Screen

class SchoolSocialBloc extends Bloc<SchoolTimeLineEvent, SchoolTimeLineState> {
  final GetSchoolTimeLineApi getSchooltimelinedetailapi;
  List<Datum> dataa = [];
  var oldData = <Datum>[];
  int page = 1;
  bool isReachedMax = false;
  int limited = 10;
  SchoolSocialBloc(this.getSchooltimelinedetailapi)
      : super(SchoolTimeLineInitial()) {
    on<GetSchooltimeLineEvent>((event, emit) async {
      if (state is SchoolsocialLoadingState) return;
      final currentState = state;

      if (currentState is SchoolsocialSuccessState) {
        oldData = currentState.data;
      }

      emit(SchoolsocialLoadingState(oldData: oldData, isFirstFetch: page == 1));
      var resultData = await getSchooltimelinedetailapi.getSchoolTimelineApi(
          page: page, limit: limited);
      page++;
      final data = (state as SchoolsocialLoadingState).oldData;
      data.addAll(resultData.data!);
      if (resultData.meta!.currentPage <= resultData.meta!.lastPage) {
        isReachedMax = false;
        emit(SchoolsocialSuccessState(data: data, hasReachedMax: isReachedMax));
      } else {
        isReachedMax = true;
        emit(SchoolsocialSuccessState(data: data, hasReachedMax: isReachedMax));
      }

      if (event.isRefresh == true) {
        oldData = [];
        dataa = [];
        page = 1;
        emit(SchoolsocialLoadingState(
            oldData: oldData, isFirstFetch: page == 1));
        await getSchooltimelinedetailapi
            .getSchoolTimelineApi(page: page, limit: limited)
            .then((value) {
          oldData.addAll(value.data!);
          if (value.meta!.currentPage <= value.meta!.lastPage) {
            isReachedMax = false;
            emit(SchoolsocialSuccessState(
                data: oldData, hasReachedMax: isReachedMax));
          } else {
            isReachedMax = true;
            emit(SchoolsocialSuccessState(
                data: oldData, hasReachedMax: isReachedMax));
          }
          page = 2;
        });
      }

      // await getSchooltimelinedetailapi
      //     .getSchoolTimelineApi(page: page, limit: 5)
      //     .then((value) {
      //   page++;
      //   final data = (state as SchoolsocialLoadingState).oldData;
      //   data.addAll(value.data!);
      //   if (value.meta!.currentPage <= value.meta!.lastPage) {
      //     isReachedMax = false;
      //     emit(SchoolsocialSuccessState(
      //         data: data, hasReachedMax: isReachedMax));
      //   } else {
      //     isReachedMax = true;
      //     emit(SchoolsocialSuccessState(
      //         data: data, hasReachedMax: isReachedMax));
      //   }
      // });
    });
  }
}
