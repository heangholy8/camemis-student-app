part of 'school_time_line_bloc.dart';

abstract class SchoolTimeLineEvent extends Equatable {
  const SchoolTimeLineEvent();

  @override
  List<Object> get props => [];
}

class SchoolTimeLineFetchEvent extends SchoolTimeLineEvent {
  const SchoolTimeLineFetchEvent();
}

class SchoolTimeLineRefreshEvent extends SchoolTimeLineEvent {
  const SchoolTimeLineRefreshEvent();
}

class SchoolTimeLineFetchDetailEvent extends SchoolTimeLineEvent {
  final int idPost;
  const SchoolTimeLineFetchDetailEvent({required this.idPost});
}

class GetSchooltimeLineEvent extends SchoolTimeLineEvent {
  // final int page;'
  final bool isRefresh;
  const GetSchooltimeLineEvent({required this.isRefresh});
}
