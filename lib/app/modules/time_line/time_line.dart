import 'dart:async';
import 'package:camis_application_flutter/app/routes/e.route.dart';
import 'package:camis_application_flutter/service/apis/profile_user/bloc/get_profile_user_bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../helper/route.export.dart';

import '../../../widgets/navigate_bottom_bar_widget/navigate_bottom_bar.dart';
import '../../../widgets/no_connection_alert_widget.dart';
import '../../core/resources/asset_resource.dart';
import 'class_time_line/view/class_time_line.dart';
import 'school_time_line/view/school_time_line.dart';
class TimeLineScreen extends StatefulWidget {
  int? selectIndex =0;
  TimeLineScreen({Key? key,this.selectIndex}) : super(key: key);

  @override
  State<TimeLineScreen> createState() => _TimeLineScreenState();
}

class _TimeLineScreenState extends State<TimeLineScreen> {
  String studentID = "";
  String classID = "";

  StreamSubscription? stsub;
  bool connection = true;
  bool firstLoad = true;
  @override
  void initState() {
    super.initState();
     //=============Check internet====================
    stsub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
      });
    });
   
   //=============Check internet====================
    setState(() {
      // BlocProvider.of<SchoolTimeLineBloc>(context).add(const SchoolTimeLineRefreshEvent());
      BlocProvider.of<SchoolSocialBloc>(context).add( const GetSchooltimeLineEvent(isRefresh: false));
      BlocProvider.of<GetProfileUserBloc>(context).add(GetProfileUser());
    });
  }

  @override
  void dispose() { 
    stsub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
  final translate = context.locale.toString();
  Future.delayed(const Duration(seconds: 1), () {
    setState(() {
        firstLoad = false;
      });
    });
  return Scaffold(
    bottomNavigationBar: connection == false
          ? Container(height: 0)
          : const BottomNavigateBar(isActive:4, paid:true),
      body: Container(
        color: Colorconstands.primaryColor,
        child: Stack(
          children: [
            Positioned(
              top: 0,
              right: 0,
              child: Image.asset("assets/images/Oval.png"),
            ),
            Positioned(
              left: 0,
              top: 50,
              child: SvgPicture.asset( ImageAssets.path20,color: Colorconstands.secondaryColor)
            ),
            Positioned(
              top: 0,left: 0,right: 0,bottom: 0,
              child: WillPopScope(
                onWillPop: ()async{
                  return false;
                },
                child: SafeArea(
                  bottom: false,
                  child: Column(
                    children:[
                      Container(
                        margin:const EdgeInsets.only(top: 0.0,bottom: 12.0,left: 12,right: 12),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children:[
                            Expanded(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    margin:const EdgeInsets.only(left: 0.0),
                                    child:TextButton(
                                      onPressed:connection == false ? null :  () {
                                        setState(() {
                                         widget.selectIndex = 0;
                                        });
                                      },
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: [
                                          Container(
                                            height: 6,width: 6,color: Colors.transparent,
                                          ),
                                          const SizedBox(height: 5.0,),
                                          Text("SCHOOL_TIMELINE".tr(),style: TextStyle(color: widget.selectIndex != 0?Colorconstands.gray.withOpacity(0.7): Colorconstands.white,fontSize: 18.0),overflow: TextOverflow.ellipsis,),
                                          const SizedBox(height: 5.0,),
                                          Container(
                                            decoration: BoxDecoration(
                                              color: widget.selectIndex==0 ? Colors.white:Colors.transparent,
                                              shape: BoxShape.circle
                                            ),
                                            height: 6,
                                            width: 6,
                                          )
                                        ],
                                      )
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    margin:const EdgeInsets.only(left: 8.0),
                                    child:TextButton(
                                      onPressed: connection == false?null: (){
                                        setState(() {
                                          widget.selectIndex =1;
                                        });
                                      },
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: [
                                          Container(height: 6,width: 6,color: Colors.transparent),
                                          const SizedBox(height: 5.0,),
                                          Text("CLASS_TIMELINE".tr(),style: TextStyle(color: widget.selectIndex==0?Colorconstands.gray.withOpacity(0.7): Colorconstands.white,fontSize: 18.0),overflow: TextOverflow.ellipsis,),
                                          const SizedBox(height: 5.0,),
                                          Container(
                                            decoration: BoxDecoration(
                                              color:widget.selectIndex==0 ? Colors.transparent:Colorconstands.white,
                                              shape: BoxShape.circle
                                            ),
                                            height: 6,
                                            width: 6,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: widget.selectIndex==0 ? const SchoolTimeline(): ClassTimeline(firstLoad: firstLoad),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            AnimatedPositioned(
              bottom: connection == true ? -150 : 0,
              left: 0,
              right: 0,
              duration: const Duration(milliseconds: 500),
              child: const NoConnectWidget(),
            ),
          ],
        ),
      ),
    );
  }
}
