import 'dart:io';

import 'package:camis_application_flutter/app/modules/time_line/widget/video_view.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:share_plus/share_plus.dart';
import 'package:video_player/video_player.dart';

import '../../../../widgets/cached_image.dart';
import '../../../../widgets/show_menu_gallary.dart';
import '../../../core/themes/color_app.dart';

class ImageViewDownloads extends StatefulWidget {
  final List listimagevide;
  late  int? activepage;
  ImageViewDownloads({Key? key, required this.listimagevide, this.activepage}) : super(key: key);

  @override
  State<ImageViewDownloads> createState() => _ImageViewDownloadsState();
}

class _ImageViewDownloadsState extends State<ImageViewDownloads> {
  List<String> imagePaths = [];
  String? linkImage;
  String? title;
  String? cotainImage;
  @override
  void initState() {
    super.initState();
    widget.listimagevide;
    cotainImage=widget.listimagevide[widget.activepage!].fileType.contains("image")?"image":"video";
  }
  @override
  Widget build(BuildContext context) {
    PageController pagec = PageController(initialPage:widget.listimagevide.length==1?0:widget.activepage!);
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        automaticallyImplyLeading: true,
        centerTitle: true,
        actions: [
          widget.listimagevide.length==1?Container(): Container(
            margin:const EdgeInsets.only(top: 15,right: 18),
            child: Text((widget.activepage! +1).toString()+" / "+widget.listimagevide.length.toString(),style:const TextStyle(color: Colorconstands.white,fontSize: 18.0),),
          ),
          cotainImage!.contains("image")?Padding(
            padding:const EdgeInsets.only(right: 20.0),
            child: GestureDetector(
              onTap: () {
                setState(() {
                  showModalBottomSheet(
                  backgroundColor: Colors.transparent,
                    context: context,
                    builder: (BuildContext bc) {
                    return ShowMenuGallary(
                      nameItem1: 'SAVE'.tr(),
                      nameItem2: 'SHARE'.tr(),
                      onPressedItem1: () {
                        setState(() {
                          GallerySaver.saveImage(linkImage!,albumName: "CAMIS");
                          Navigator.of(context).pop();
                          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(backgroundColor: Colorconstands.primaryColor,content: Text("Save to Gallarys!",style: TextStyle(fontSize: 16),textAlign:TextAlign.center)));
                        });
                       },
                      onPressedItem2: () async{ 
                        final uri = Uri.parse(linkImage!);
                        final res = await http.get(uri);
                        final bytes = res.bodyBytes;
                        final temp = await getTemporaryDirectory();
                        final path = "${temp.path}/image.jpg";
                        File(path).writeAsBytesSync(bytes);
                        await Share.shareFiles([path]);
                        Navigator.of(context).pop();
                      },
                    );
                    });
                });
              },
              child:const Icon(
                  Icons.more_vert,color: Colors.white,
              ),
            )
          ):Container(),
        ],
      ),
      body: SafeArea(
        child: Center(
          child:
           PageView.builder(
            controller: pagec,
            itemCount: widget.listimagevide.length,
            onPageChanged: (index){
              setState(() {
                cotainImage = widget.listimagevide[index].fileType;
                widget.activepage=index;
              });
            },
            itemBuilder: (context, index) {
              cotainImage = widget.listimagevide[index].fileType;
              title = widget.listimagevide[index].fileName;
              linkImage= widget.listimagevide[index].fileShow??widget.listimagevide[index].fileThumbnail;
              return Center(
                child:widget.listimagevide[index].fileType.contains("image")? InteractiveViewer(
                  clipBehavior: Clip.none,
                  child: ClipRRect(
                    child: linkImage==null||linkImage!.isEmpty||linkImage==""?const Center(child: CircularProgressIndicator(),) :CachedImageNetwork(urlImage: linkImage!,),
                  ),
                ):widget.listimagevide[index].fileType.contains("video")?VideoView(videoPlayerController: VideoPlayerController.network(widget.listimagevide[index].fileShow,), autoplay: true)
                :Container(),
              );
            },
            
          ),
        ),
      ),
    );
  }
  // void _onShareWithResult(BuildContext context) async {
  //   final box = context.findRenderObject() as RenderBox?;
  //   ShareResult result;
  //   if (linkImage!.isNotEmpty) {
  //     result = await Share.shareFilesWithResult([linkImage!],
  //         text: title,
  //         subject: "Image",
  //         mimeTypes:[linkImage!],
  //         sharePositionOrigin: box!.localToGlobal(Offset.fromDirection(1.0)) & box.size);
  //   } else {
  //     result = await Share.shareWithResult(title!,
  //         subject: "Image",
  //         sharePositionOrigin: box!.localToGlobal(Offset.fromDirection(1.0)) & box.size);
  //   }
  //   ScaffoldMessenger.of(context).showSnackBar(SnackBar(
  //     backgroundColor: Colorconstands.primaryColor,
  //     content: Text("Share Image: ${result.status}",textAlign:TextAlign.center),
  //   ));
  // }
}