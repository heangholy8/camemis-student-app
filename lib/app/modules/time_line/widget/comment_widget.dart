import 'package:camis_application_flutter/app/modules/time_line/data/example_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter_reaction_button/flutter_reaction_button.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../widgets/profile_widget.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class WidgetComment extends StatefulWidget {
  final String profilecomment;
  final String namecomment;
  final String textcomment;
  final double profilewidth;
  final double profilehight;
  final String textReact;
  final Color colorlabelreact;
  final int selectedReaction;
  final String? iconReact;
  final Function(String? values, bool isChecked) onReact;
  final VoidCallback reply;
  final TextStyle style;
  final TextStyle styletextpost;
  final double margincommentbetweenprofile;
  final int lenghtreply;
  final bool isCheckroot;
  final double marginprofileLeft;
  bool hideReact = false;
   WidgetComment(
      {Key? key,
      required this.hideReact,
      required this.profilecomment,
      required this.namecomment,
      required this.textcomment,
      required this.profilewidth,
      required this.profilehight,
      required this.textReact,
      this.iconReact,
      required this.onReact,
      required this.reply,
      required this.style,
      required this.styletextpost,
      required this.colorlabelreact,
      required this.selectedReaction,
      required this.margincommentbetweenprofile,
      required this.lenghtreply,
      required this.isCheckroot,
      required this.marginprofileLeft})
      : super(key: key);

  @override
  State<WidgetComment> createState() => _WidgetCommentState();
}

class _WidgetCommentState extends State<WidgetComment> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
      child: Stack(
        children: [
          widget.lenghtreply == 0 || widget.isCheckroot == false
              ? Container()
              : Positioned(
                  top: 8.0,
                  bottom: 0,
                  left: 35,
                  child: Container(
                    width: 1,
                    color: Colorconstands.gray,
                  ),
                ),
          Positioned(
            top: 8.0,
            left: widget.marginprofileLeft,
            child: Profile(
              imageProfile: widget.profilecomment,
              namechild: '',
              onPressed: () {},
              pandding: 0,
              sizetextname: 0,
              width: widget.profilewidth,
              height: widget.profilehight,
            ),
          ),
          Container(
            margin: EdgeInsets.only(
                left: widget.margincommentbetweenprofile,
                right: 12.0,
                top: 8.0,
                bottom: 8.0),
            padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 12.0, vertical: 3.0),
                  margin: const EdgeInsets.only(
                    bottom: 5.0,
                  ),
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(Radius.circular(12)),
                    color: Colorconstands.gray.withOpacity(0.4),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(widget.namecomment, style: widget.style),
                      Text(widget.textcomment, style: widget.styletextpost),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const SizedBox(
                      width: 8,
                    ),
                    widget.hideReact==true?Container():Container(
                      child: SizedBox(
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: ReactionButtonToggle<String>(
                            reactions: reactions,
                            initialReaction: Reaction<String>(
                              value: widget.textReact,
                              icon: Row(
                                children: <Widget>[
                                  Image.asset(widget.iconReact!, height: 13),
                                  const SizedBox(width: 5),
                                  Text(widget.textReact,
                                      style: TextStyle(
                                          color: widget.colorlabelreact,
                                          fontSize: 13))
                                ],
                              ),
                            ),
                            selectedReaction:
                                reactions[widget.selectedReaction],
                            onReactionChanged: widget.onReact,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 14,
                    ),
                    Container(
                      child: InkWell(
                        onTap: widget.reply,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              "assets/icons/fi_reply_icon.svg",
                              width: 12,
                            ),
                            const SizedBox(
                              width: 6,
                            ),
                            Text(
                              'Reply',
                              style:
                                  ThemeConstands.texttheme.bodyText2!.copyWith(
                                fontWeight: FontWeight.w600,
                                color: Colorconstands.darkGray,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
