import 'package:better_player/better_player.dart';
import 'package:camis_application_flutter/app/modules/time_line/widget/video_view.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerWidget extends StatefulWidget {
  final String? linkVideo;
  const VideoPlayerWidget({
    Key? key,
    required this.linkVideo,
  }) : super(key: key);

  @override
  State<VideoPlayerWidget> createState() => _VideoPlayerWidgetState();
}

class _VideoPlayerWidgetState extends State<VideoPlayerWidget> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.linkVideo;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        automaticallyImplyLeading: true,
        centerTitle: true,
      ),
      body: SafeArea(
        child: Center(
          child: VideoView(autoplay: true,videoPlayerController: VideoPlayerController.network(widget.linkVideo!),),
        ),
      ),
    );
  }
}