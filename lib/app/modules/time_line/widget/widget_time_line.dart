import 'package:camis_application_flutter/app/core/resources/asset_resource.dart';
import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:camis_application_flutter/app/modules/time_line/data/example_data.dart';
import 'package:camis_application_flutter/widgets/profile_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_reaction_button/flutter_reaction_button.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Timeline_widget extends StatelessWidget {
  final Widget? discriptionChild;
  final Widget? viewChild;
  final Widget? displayVoice;
  final Widget? displayFile;
  final String? imageProfile;
  final int? comments;
  final String? reaction;
  final int? totalLike;
  final String? datePost;
  final String? name;
  final String? pathImageReact;
  final String? textReact;
  final Color? colorlabelreact;
  final int? selectedReaction;
  final int? totalReactType;
  final String? imageReact1;
  final String? imageReact2;
  final String? imageReactselectdefoult;
  final String? nameusercomment;
  final String? commenttext;
  final String? commentprofile;
  final VoidCallback? onTapBodycomment;
  final VoidCallback? onTapcomment;
  final Function(String?values,bool isChecked) onReactionChanged; 
  const Timeline_widget({Key? key,this.imageReactselectdefoult,this.imageProfile, this.discriptionChild, this.viewChild, this.comments, this.reaction, this.totalLike, required this.onReactionChanged, required this.datePost, this.name, this.pathImageReact, this.textReact, this.colorlabelreact, this.selectedReaction, this.totalReactType, this.imageReact1, this.imageReact2, this.displayVoice, this.displayFile, this.nameusercomment, this.commenttext, this.commentprofile, this.onTapBodycomment, this.onTapcomment,}) : super(key: key);
    @override
  Widget build(BuildContext context) {
    return Container(
      padding:EdgeInsets.only(top: 22,bottom: comments==0?16.0 :5.0),
      color: Colorconstands.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            margin:const EdgeInsets.symmetric(horizontal: 22.0),
            child: Row(
              children: [
                Container(
                  margin:const EdgeInsets.only(right: 9.0),
                  child: Profile(height: 45, imageProfile: imageProfile!, namechild: '', onPressed: () {  }, pandding: 0, sizetextname: 0, width: 45,),
                ),
                Container(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                        child: Text(name!,style: ThemeConstands.texttheme.headline6!.copyWith(color: Colorconstands.black,fontWeight: FontWeight.w500),),
                      ),
                      Align(
                        child: Text(datePost! +" •",style: ThemeConstands.texttheme.caption!.copyWith(color: Colorconstands.darkGray,fontWeight: FontWeight.w100),),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: discriptionChild,
          ),
          Container(
            child: displayVoice,
          ),
          Container(
            child: viewChild,
          ),
          Container(
            child: displayFile,
          ),
          Container(
            child: Container(
              child: Column(
                children: [
                  comments==0&&totalLike==0?Container():Container(
                    padding:const EdgeInsets.only(left: 22.0,right: 22.0,top: 12.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        totalLike==0?Container():Row(
                          children: [
                           Container(
                             width:totalReactType! <=1? 16:28,
                             child: Stack(
                              children: [
                                Container(
                                  height: 16,width: 16,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(width: 1,color: Colors.white),
                                    color: Colors.white
                                  ),
                                  child: Image.asset(imageReact2!),
                                ),
                                // Container(
                                //   height: 16,width: 16,
                                //   decoration: BoxDecoration(
                                //     shape: BoxShape.circle,
                                //     border: Border.all(width: 1,color: Colors.white),
                                //     color: Colors.white
                                //   ),
                                //   child: Image.asset(imageReactselectdefoult!),
                                // ),
                                totalReactType! <=1?Container():Positioned(
                                  left: 12,
                                  top: 0,bottom: 0,
                                  child: Container(
                                    height: 16,width: 16,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      border: Border.all(width: 1,color: Colors.white),
                                      color: Colors.white
                                    ),
                                    child: Image.asset(imageReact1!),
                                  ),
                                ),
                              ],
                          ),
                           ),
                          
                           const SizedBox(width: 5,),
                            Text(totalLike.toString(),style:const TextStyle(fontSize: 14.0),),
                            const SizedBox(width: 5.0,),
                            Center(child: Text(reaction!,style:const TextStyle(fontSize: 14.0),))
                          ],
                        ),
                        comments==0?Container():Container(
                          child: Row(
                            children: [
                              Text(comments.toString(),style:const TextStyle(fontSize: 14,color: Colorconstands.darkGray)),
                              const SizedBox(width: 5.0,),
                              const Text('Comment',style: TextStyle(fontSize: 14,color: Colorconstands.darkGray)),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin:const EdgeInsets.only(top: 12.0),
                    padding:const EdgeInsets.symmetric(horizontal: 12.0),
                    child:const Divider(
                      height: 1,
                      color: Color.fromRGBO(0, 0, 0, 0.38),
                    ),
                  ),
                  Container(
                    padding:const EdgeInsets.symmetric(horizontal: 32.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: SizedBox(
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child: ReactionButtonToggle<String>(
                                reactions: reactions,
                                initialReaction: Reaction<String>(
                                  value: textReact,
                                  icon: Row(
                                    children: <Widget>[
                                      Image.asset(pathImageReact!, height: 20),
                                      const SizedBox(width: 5),
                                      Text(
                                        textReact!,style: TextStyle(color: colorlabelreact,fontSize: 16)
                                      )
                                    ],
                                  ),
                                ),
                                selectedReaction: reactions[selectedReaction!],
                                onReactionChanged: onReactionChanged,
                                  
                                
                              ),
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: onTapcomment,
                          child: Container(
                            padding:const EdgeInsets.symmetric(vertical: 12.0),
                            child: Row(
                              children: [
                                SvgPicture.asset(ImageAssets.comment_icon),
                                const SizedBox(
                                  width: 8.0,
                                ),
                                const Align(
                                  child: Text("Comment",style: TextStyle(color: Colorconstands.primaryColor,fontSize: 16),),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin:const EdgeInsets.only(top: 0.0),
                    padding:const EdgeInsets.symmetric(horizontal: 12.0),
                    child:const Divider(
                      height: 1,
                      color: Colors.black38,
                    ),
                  ),
                  comments==0?Container(): InkWell(
                    onTap: onTapBodycomment,
                    child: Container(
                      padding:const EdgeInsets.symmetric(vertical: 12.0,horizontal: 12.0),
                      child: Container(
                        padding:const EdgeInsets.symmetric(vertical: 3.0,horizontal: 6.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              child: Profile(
                               imageProfile: commentprofile!,
                               namechild: '',
                               onPressed: () {  },
                               pandding: 0,
                               sizetextname: 0,
                               width: 40,
                               height: 40,
                              ),
                            ),
                            Expanded(
                              child: Container(
                                margin:const EdgeInsets.only(left: 8.0,right: 18.0),
                                padding: const EdgeInsets.symmetric(horizontal: 12.0,vertical: 5.0),
                                decoration: BoxDecoration(
                                  borderRadius:const BorderRadius.all(Radius.circular(12)),
                                   color: Colorconstands.gray.withOpacity(0.4),
                                ),
                               
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(nameusercomment!,style: ThemeConstands.texttheme.subtitle1!.copyWith(color: Colorconstands.black,fontWeight: FontWeight.w500),),
                                    Text(commenttext!,style: ThemeConstands.texttheme.subtitle1!.copyWith(color: Colorconstands.darkGray,),maxLines: 3,overflow: TextOverflow.ellipsis,),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}