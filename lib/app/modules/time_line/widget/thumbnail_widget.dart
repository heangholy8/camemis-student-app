import 'package:flutter/material.dart';

import '../../../../widgets/cached_image.dart';

class ThumbnailVideoWidget extends StatelessWidget {
  final String? image;
  final VoidCallback? onPressed;
  final double? width;
  final double? height;
  const ThumbnailVideoWidget({Key? key, this.image, this.onPressed, this.width, this.height})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      padding: const EdgeInsets.all(0),
      onPressed: onPressed,
      child: Stack(
        alignment: Alignment.center,
        children: [
          CachedImageNetwork(
            urlImage: image!,
            
          ),
          Container(
            height: 50,
            width: 50,
            decoration: BoxDecoration(
              color: Colors.grey.withOpacity(0.7),
              shape: BoxShape.circle,
            ),
            child: const Icon(
              Icons.play_arrow_outlined,
              size: 30,
            ),
          )
        ],
      ),
    );
  }
}
