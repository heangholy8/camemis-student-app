import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class LoadingShimmerWidget extends StatelessWidget {
  const LoadingShimmerWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: const Color(0xFF3688F4),
      highlightColor: const Color(0xFF3BFFF5),
      child: Container(
        margin: const EdgeInsets.symmetric(
          horizontal: 18,
        ),
        child: Row(
          children: [
            Container(
              height: 65,
              width: 66,
              decoration: const BoxDecoration(
                color: Color(0x622195F3),
                shape: BoxShape.circle,
              ),
            ),
            const SizedBox(
              width: 7.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 17,
                  width: MediaQuery.of(context).size.width / 2.5,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: const Color(0x622195F3),
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Container(
                  height: 15,
                  width: MediaQuery.of(context).size.width / 3,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: const Color(0x622195F3),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
