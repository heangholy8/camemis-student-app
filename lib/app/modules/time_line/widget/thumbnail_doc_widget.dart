
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../../core/themes/color_app.dart';

class ThumbnailDocWidget extends StatelessWidget {
  final String? nameFile;
  final String? sizeFile;
  final VoidCallback? onPressed;
  const ThumbnailDocWidget({Key? key, this.nameFile, this.onPressed, this.sizeFile}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      padding:const EdgeInsets.all(0),
      onPressed: onPressed,
      child: Container(
        margin:const EdgeInsets.only(top: 0.0,bottom: 0,left: 12.0,right: 12.09),
        decoration: BoxDecoration(
          border:Border(bottom: BorderSide(color: Colorconstands.primaryColor.withOpacity(0.5),width: 0.5))
        ),
        width: double.maxFinite,
        child: Container(
           margin:const EdgeInsets.only(top: 5.0,),
          child: Row(
           mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10.0,horizontal: 0.0),
                padding:const EdgeInsets.all(8),
                decoration: BoxDecoration(
                  color: Colorconstands.gray.withOpacity(0.3),
                  shape: BoxShape.circle,
                ),
                child: Image.asset('assets/images/pdf_icon.png',height: 24,width: 24,)),
                //child: Icon(Icons.folder_outlined,size:24.0,color: Colorconstands.black.withOpacity(0.6))),
              const SizedBox(width: 12.0,),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(nameFile!,style:const TextStyle(fontSize: 15,color:Colorconstands.darkGray,fontWeight: FontWeight.w600),textAlign:TextAlign.left,maxLines: 2,overflow: TextOverflow.ellipsis,),
                    Text(sizeFile.toString(),style:const TextStyle(fontSize: 12.0,color:Colors.black),textAlign:TextAlign.left),
                  ],
                )
              ),
              Container(
                margin: const EdgeInsets.only(right: 22.0,left: 10.0),
                padding:  const EdgeInsets.symmetric(vertical: 2,horizontal: 8),
                decoration: BoxDecoration(
                  border: Border.all(color: Colorconstands.gray, width: 1),
                  borderRadius: BorderRadius.circular(2.0)
                ),
                child: Text("View".tr(),style:const TextStyle(fontSize: 12.0,color:Colorconstands.primaryColor),),
              )
            ],
          ),
        ),
      ),
    );
  }
}