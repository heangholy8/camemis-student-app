part of 'class_time_line_bloc.dart';

abstract class ClassTimeLineState extends Equatable {
  const ClassTimeLineState();
  @override
  List<Object> get props => [];
}

class ClassTimeLineInitial extends ClassTimeLineState {}


class ClassTimelineDetailLoadingState extends ClassTimeLineState{

}

class ClassTimeLineLoadingState extends ClassTimeLineState {
  final List<DetailChildInfor> oldData;
  bool? isFirstFetch;
   ClassTimeLineLoadingState({required this.oldData,this.isFirstFetch});
}

class ClassTimeLineSuccessState extends ClassTimeLineState {
  final List<DetailChildInfor> data;
  final bool hasReachedMax;
  const ClassTimeLineSuccessState({required this.data,required this.hasReachedMax});
}

class DetailClassTimeLineLoaded extends ClassTimeLineState {
  final GetDetailSchoolTimelineModel? classtimeLineDetailmodel;
  const DetailClassTimeLineLoaded({this.classtimeLineDetailmodel});
}

class ClassTimeLineErrorState extends ClassTimeLineState {
  final String error;
  const ClassTimeLineErrorState({
    required this.error,
  });
}
