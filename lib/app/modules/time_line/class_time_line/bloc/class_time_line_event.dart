part of 'class_time_line_bloc.dart';

abstract class ClassTimeLineEvent extends Equatable {
  const ClassTimeLineEvent();

  @override
  List<Object> get props => [];
}


class ClassTimeLineFetchEvent extends ClassTimeLineEvent {
  final String classId;
  final bool isRefresh;
  const ClassTimeLineFetchEvent( {required this.isRefresh, required this.classId});
}

class ClassTimeLineFetchDetailEvent extends ClassTimeLineEvent{
  final String classId;
  final String idPost;
  const ClassTimeLineFetchDetailEvent({required this.classId,required this.idPost});
}
