import 'package:camis_application_flutter/app/modules/auth_screens/e_school_code.dart';
import 'package:camis_application_flutter/model/timeline/class_timeline_model.dart';
import 'package:equatable/equatable.dart';
import '../../../../../model/timeline/detail_school_time_line_model.dart';
import '../../../../../service/apis/time_line_api/get_time_line_api.dart';

part 'class_time_line_event.dart';
part 'class_time_line_state.dart';

class ClassTimeLineBloc extends Bloc<ClassTimeLineEvent, ClassTimeLineState> {
  final GetSchoolTimeLineApi getSchooltimelineapi;

  List<DetailChildInfor> dataBloc = [];
  
  int? indexOfChild;
  int page = 1;
  bool isReachedMax = false;
  int limited = 15;
  List<DetailChildInfor> dataa = [];
  var oldData = <DetailChildInfor>[];

  ClassTimeLineBloc({required this.getSchooltimelineapi})
      : super(ClassTimeLineInitial()) {
    on<ClassTimeLineFetchEvent>((event, emit) async {
      try {
        if (event.isRefresh == true) {
          if (state is ClassTimeLineLoadingState) return;
          final currentState = state;
          if (currentState is ClassTimeLineSuccessState) {
            oldData = [];
            dataa=[];
          }
          page = 1;
          emit(ClassTimeLineLoadingState(
              oldData: oldData, isFirstFetch: page == 1));
          await getSchooltimelineapi
              .getClassTimelineApi(
                  classId: event.classId, limit: limited, page: page)
              .then((value) {
            print("datadatadata ${value.data!.length}");
            oldData.addAll(value.data!);
            if (value.meta!.currentPage <= value.meta!.lastPage) {
              isReachedMax = false;
              emit(ClassTimeLineSuccessState(data: oldData, hasReachedMax: isReachedMax));
            } else {
              isReachedMax = true;
              emit(ClassTimeLineSuccessState(data: oldData, hasReachedMax: isReachedMax));
            }
            page = 2;
          });
        }
        if (state is ClassTimeLineLoadingState) return;
        final currentState = state;
        if (currentState is ClassTimeLineSuccessState) {
          oldData = currentState.data;
        }
        emit(ClassTimeLineLoadingState(
            oldData: oldData, isFirstFetch: page == 1));
        var resultData = await getSchooltimelineapi.getClassTimelineApi(
            classId: event.classId, page: page, limit: limited);

        dataa = (state as ClassTimeLineLoadingState).oldData;
        dataa.addAll(resultData.data!);
        if (resultData.meta!.currentPage! <= resultData.meta!.lastPage!) {
          page++;
          isReachedMax = false;
          emit(ClassTimeLineSuccessState(
              data: dataa, hasReachedMax: isReachedMax));
        } else {
          isReachedMax = true;
          emit(ClassTimeLineSuccessState(
              data: dataa, hasReachedMax: isReachedMax));
        }
      } catch (e) {
        print("Errorroro $e");
        emit(ClassTimeLineErrorState(error: e.toString()));
      }
    });
  }
}

class ClassTimeLineDetailBloc
    extends Bloc<ClassTimeLineEvent, ClassTimeLineState> {
  final GetSchoolTimeLineApi getClasstimelinedetailapi;
  ClassTimeLineDetailBloc({required this.getClasstimelinedetailapi})
      : super(ClassTimeLineInitial()) {
    on<ClassTimeLineFetchDetailEvent>((event, emit) async {
      emit(ClassTimelineDetailLoadingState());
      try {
        var detaildata =
            await getClasstimelinedetailapi.getClassTimelineDetailApi(
                postId: event.idPost, classId: event.classId);
        emit(DetailClassTimeLineLoaded(classtimeLineDetailmodel: detaildata));
      } catch (e) {
        emit(const ClassTimeLineErrorState(error: "Error data"));
        print("Eroro0roror $e");
      }
    });
  }
}
