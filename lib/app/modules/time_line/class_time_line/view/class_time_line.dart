import 'dart:async';
import 'dart:math';
import 'package:audioplayers/audioplayers.dart';
import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:camis_application_flutter/app/modules/time_line/class_time_line/view/comment.dart';
import 'package:camis_application_flutter/app/modules/time_line/class_time_line/view/detail_class_timeline_post.dart';
import 'package:camis_application_flutter/app/modules/time_line/widget/video_play.dart';
import 'package:camis_application_flutter/model/timeline/class_timeline_model.dart';
import 'package:camis_application_flutter/service/apis/profile_user/bloc/get_profile_user_bloc.dart';
import 'package:camis_application_flutter/widgets/cached_image.dart';
import 'package:camis_application_flutter/widgets/view_document.dart';
import 'package:camis_application_flutter/app/modules/time_line/widget/widget_time_line.dart';
import 'package:camis_application_flutter/widgets/shimmer_style.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:expandable_text/expandable_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../../component/change_child/bloc/change_child_bloc.dart';
import '../../../../../mixins/toast.dart';
import '../../../../../service/apis/time_line_api/post_react.dart';
import '../../../../../widgets/no_dart.dart';
import '../../../../core/themes/themes.dart';
import '../../../attendance_screen.dart/widgets/schedule_empty.dart';
import '../../widget/thumbnail_doc_widget.dart';
import '../../widget/thumbnail_widget.dart';
import '../../widget/view_image.dart';
import '../bloc/class_time_line_bloc.dart';

class ClassTimeline extends StatefulWidget {
  final bool firstLoad;
  const ClassTimeline({Key? key, required this.firstLoad}) : super(key: key);
  @override
  State<ClassTimeline> createState() => _ClassTimelineState();
}

class _ClassTimelineState extends State<ClassTimeline>
    with TickerProviderStateMixin, Toast {
  late ScrollController _scrollController;
  late  AudioCache audioCache;
  bool isClick = true;
  bool isReachedMax = false;
  int maxImage = 4;
  int? remaining;
  int? numImages;
  String? reaction;
  bool isFirstLoaded =false;
  //final dataTimeLine = TimeLineModel.generate();
  String? discription;
  String reactiondefultselect ="assets/images/actionButton/icon_like_circle.png";
  bool connection = true;
  StreamSubscription? stsub;
  int activeChild = 0;
  String ClassId = "";
  
  @override
  void initState() {
     BlocProvider.of<GetProfileUserBloc>(context).add(GetProfileUser());
    //=============Check internet====================
      stsub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          connection = (event != ConnectivityResult.none);
          if (connection == false) {
            FocusScope.of(context).unfocus();
          }
        });
      });
    //============= Check internet ====================
    _scrollController = ScrollController(keepScrollOffset: true)..addListener(onScroll);

    super.initState();
    audioCache = AudioCache(
      prefix: '',
      fixedPlayer: AudioPlayer()..setReleaseMode(ReleaseMode.STOP),
    );
  }

  void onScroll() {
    if (_scrollController.offset >= _scrollController.position.maxScrollExtent - 200 &&
      !_scrollController.position.outOfRange && _scrollController.position.pixels != 0 && isReachedMax == false) {
      // isClick = false;
      connection==true?
      BlocProvider.of<ClassTimeLineBloc>(context).add(ClassTimeLineFetchEvent( classId:ClassId, isRefresh: false)):null;
    }
  }

  @override
  void dispose() {
    stsub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Column(
      children: [
        Expanded(
          child: Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(12), topRight: Radius.circular(12)),
              ),
              child: BlocListener<GetProfileUserBloc, GetProfileUserState>(
                listener: (context, state) {
                  if(state is ProfileUserLoaded){
                    if (state.profileusermodel.data != null && state.profileusermodel.data!.currentClass != null) {
                      BlocProvider.of<ClassTimeLineBloc>(context).add(ClassTimeLineFetchEvent(classId: state.profileusermodel.data!.currentClass!.classId.toString(),isRefresh: false));
                      setState(() {
                        ClassId = state.profileusermodel.data!.currentClass!.classId.toString();
                      });
                    }
                    
                  }
                },
                child: BlocBuilder<GetProfileUserBloc, GetProfileUserState>(
                  builder: (context, state) {
                    if (state is ProfileUserLoadingState) {
                      return Column(
                        children: [
                          Expanded(
                            child: Container(
                              color: Colorconstands.neutralWhite,
                              child: ListView.builder(
                                shrinkWrap: true,
                                itemBuilder:  (BuildContext contex,index) {
                                  return const ShimmerTimeLine();
                                },
                                itemCount: 3,
                              ),
                            ),
                          ),
                        ],
                      );
                    } else if (state is ProfileUserLoaded) {
                      var datachild = state.profileusermodel.data;
                      return datachild! == null
                          ? Container(
                              color: Colorconstands.neutralWhite,
                              child: DataNotFound(
                                    subtitle: "CHILD_NOT_CLASS_DES".tr(),
                                    title: "CHILD_NOT_CLASS".tr(),
                              ))
                          : Column(
                            children: [
                              Expanded(
                                child: Container(
                                  decoration: BoxDecoration(
                                    color:const Color(0xFF4B85B7).withOpacity(0.2),
                                    borderRadius: const BorderRadius.only(topLeft: Radius.circular(12),topRight: Radius.circular(12))),
                                    child:Container(
                                  child:  Container(
                                          color: Colorconstands.neutralWhite,
                                          child: BlocListener<ClassTimeLineBloc, ClassTimeLineState>(
                                            listener: (context, state) {
                                              if(state is ClassTimeLineLoadingState){
                                              setState(() {
                                                isClick = false;
                                              });
                                              } else{
                                              setState(() {
                                                  isClick=true;
                                              });
                                              }
                                            },
                                            child: BlocBuilder<ClassTimeLineBloc, ClassTimeLineState>(
                                              builder: (context, state) {
                                                List<DetailChildInfor> data = [];
                                                bool isLoading = false;
                                                if (state is ClassTimeLineLoadingState) {
                                                  data = state.oldData;
                                                  isLoading = true;
                                                } else if (state is ClassTimeLineSuccessState) {
                                                  data = state.data;
                                                  isReachedMax = state.hasReachedMax;
                                                }
                                                if (state is ClassTimeLineLoadingState && state.isFirstFetch!) {
                                                  return Column(
                                                    children: [
                                                      Expanded(
                                                        child: Container(
                                                          color: Colorconstands.neutralWhite,
                                                          child: ListView.builder(
                                                            shrinkWrap: true,
                                                            itemBuilder:  (BuildContext contex,index) {
                                                              return const ShimmerTimeLine();
                                                            },
                                                            itemCount: 3,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  );
                                                }
                                              return RefreshIndicator(
                                                onRefresh: () async {
                                                  BlocProvider.of<ClassTimeLineBloc>(context).add(ClassTimeLineFetchEvent(classId: datachild.currentClass!.classId.toString(),isRefresh: true));
                                                },
                                            // Checking Data New Feed isEmpty
                                              child: data.isEmpty?Container(
                                                color: Colorconstands.neutralWhite,
                                                child: Column(
                                                  children: [
                                                    Expanded(child: Container()),
                                                      DataEmptyWidget(
                                                      title:"EMPTHY_CONTENT".tr(),
                                                      description: "EMPTHY_CONTENT_DES".tr(),
                                                      
                                                      ),
                                                    Expanded(child: Container()),
                                                  ],
                                                )):
                                                ListView.separated(
                                                controller: _scrollController,
                                                itemCount: data.length +(isLoading?1:0)+(isReachedMax ==true ?1:0),
                                                itemBuilder: (context,index){
                                                  if(index< data.length){
                                                    var applications = data[index].attachments! .where((element) =>element.fileType!.contains("application")) .toList();
                                                    var audio = data[index].attachments!.where((element)=>element.fileType!.contains("audio")).toList();
                                                    var listImageVideo = data[index].attachments!.where((element) {
                                                        return element.fileType!.contains("image") || element.fileType!.contains("video");
                                                    }).toList();
                                                    String? profileusercomment;
                                                    String? nameusercomment;
                                                    String? commenttext;
                                                    var imageandvideo = listImageVideo.length;
                                                  if (data[index].comments!.isNotEmpty) {
                                                    profileusercomment = data[index].comments![0].user!.profileMedia!.fileThumbnail;
                                                    nameusercomment = translate == "en"
                                                      ? data[index].comments![0].user!.nameEn == " " || data[index].comments![0].user!.nameEn == ""
                                                      ? data[index].comments![0].user!.name : data[index].comments![0].user!.nameEn : data[index].comments![0].user!.name;
                                                    commenttext = data[index].comments![0].postText;
                                                  }
                                                  discription = data[index].postText.toString();
                                                  // int indexSelected = index;
                                                  int selecticonreact = 5;
                                                  numImages = imageandvideo;
                                                return Timeline_widget(
                                                  commentprofile:profileusercomment,
                                                  nameusercomment: nameusercomment,
                                                  commenttext: commenttext,
                                                  totalReactType: data[index].totalReactType!.length,
                                                  comments: data[index].commentCount,
                                                  reaction: "React",
                                                  totalLike: data[index].totalReact,
                                                  datePost: data[index].createdDate,
                                                  name: translate == "en"
                                                      ? data[index].user!.nameEn ==" " ||data[index].user!.nameEn == " "
                                                          ? data[index].user!.name
                                                          : data[index].user!.nameEn
                                                      : data[index].user!.name,
                                                  //------------------------- Rect type all--------------------------
                                                  imageReact2: data[index].totalReactType!.isEmpty
                                                      ? reactiondefultselect
                                                      : data[index].totalReactType![0].reactionType == 0
                                                          ? "assets/images/actionButton/icon_like_circle.png"
                                                          : data[index].totalReactType![0].reactionType == 1
                                                              ? "assets/images/actionButton/Love.png"
                                                              : data[index].totalReactType![0].reactionType == 2
                                                                  ? "assets/images/actionButton/Icon_haha.png"
                                                                  : data[index].totalReactType![0].reactionType == 3
                                                                      ? "assets/images/actionButton/wow2.png"
                                                                      : data[index].totalReactType![0].reactionType == 5
                                                                          ? "assets/images/actionButton/angry2.png"
                                                                          : "assets/images/actionButton/icon_like_circle.png",
                                                  imageReact1: data[index].totalReactType!.length <= 1
                                                      ? "assets/images/actionButton/icon_like_circle.png"
                                                      : data[index].totalReactType![1].reactionType ==0
                                                          ? "assets/images/actionButton/icon_like_circle.png"
                                                          : data[index].totalReactType![1].reactionType ==1
                                                              ? "assets/images/actionButton/Love.png"
                                                              : data[index].totalReactType![1].reactionType == 2
                                                                  ? "assets/images/actionButton/Icon_haha.png"
                                                                  : data[index].totalReactType![1].reactionType == 3
                                                                      ? "assets/images/actionButton/wow2.png"
                                                                      : data[index].totalReactType![1].reactionType == 5
                                                                          ? "assets/images/actionButton/angry2.png"
                                                                          : "assets/images/actionButton/icon_like_circle.png",
                                                  // imageReactselectdefoult:selecticonreact==0?"assets/images/actionButton/icon_like_circle.png"
                                                  // :selecticonreact==1?"assets/images/actionButton/Love.png":"assets/images/actionButton/wow2.png",
                                                  //-------------------------End Rect type all--------------------------
                                                  //------------------------- color Label React--------------------------
                                                  //colorlabelreact: Colorconstands.primaryColor,
                                                  colorlabelreact: data[ index].isReact == 0
                                                      ? Colorconstands .primaryColor
                                                      : data[index].reactionType == 0
                                                          ? Colorconstands .primaryColor
                                                          : data[index].reactionType ==1
                                                              ? Colors.red
                                                              : data[index].reactionType ==2
                                                                  ? Colors.yellow
                                                                  : data[index].reactionType == 5
                                                                      ? Colors.red
                                                                      : data[index].reactionType == 6
                                                                          ? Colorconstands.primaryColor
                                                                          : Colors.yellow,
                                                  //-------------------------End color Label React--------------------------
                                                  pathImageReact: data[index].isReact == 0
                                                      ? "assets/icons/like_icon.png"
                                                      : data[index] .reactionType == 0 ?"assets/images/actionButton/like_full_fill.png"
                                                          : data[index].reactionType ==1? "assets/images/actionButton/Love.png"
                                                              : data[index].reactionType == 2? "assets/images/actionButton/Icon_haha.png"
                                                                  : data[index].reactionType == 3? "assets/images/actionButton/wow2.png"
                                                                      : data[index].reactionType == 5? "assets/images/actionButton/angry2.png" : "assets/icons/like_icon.png",
                                                  textReact: data[index] .isReact == 0 ? "Like"
                                                      : data[index] .reactionType == 0 ? "Like"
                                                          : data[index] .reactionType == 1? "Love"
                                                              : data[index].reactionType ==3? "Wow"
                                                                  : data[index].reactionType == 2? "haha"
                                                                      : data[index].reactionType == 5? "angry": "Like",
                                                  selectedReaction: selecticonreact,
                                                  //------------------------- Profile image--------------------------
                                                  imageProfile: data[index].user!.profileMedia!.fileThumbnail ?? data[index] .user!.profileMedia!.fileShow,
                                                  //-------------------------End Profile image--------------------------
                                                  //------------------------Text Post------------------
                                                  discriptionChild: Container(
                                                    alignment: Alignment.centerLeft,
                                                    margin:const EdgeInsets.only(left: 22.0,right: 22.0, top: 15.0, bottom: 12.0),
                                                    child: ExpandableText(
                                                      discription == null ||discription == ""|| discription!.isEmpty? "" : discription!,
                                                      expandText: 'show more',
                                                      collapseText:'show less',
                                                      maxLines: 2,
                                                      style:const TextStyle(fontSize:16.0),
                                                      linkColor: Colorconstands .primaryColor,
                                                    ),
                                                  ),
                                                  //------------------------End Text Post------------------
                                                  //------------------------Void---------------------
                                                  displayVoice: Container(),
                                                  //------------------------End Void------------------
                                                  //---------------View Center Body--------------------------
                                                  viewChild: numImages == 1
                                                      ? Stack(
                                                          children: List.generate(listImageVideo.length, (subindex) {
                                                            var attachmentUrl = listImageVideo[subindex].fileThumbnail;
                                                            var attachmentType =listImageVideo[subindex] .fileType;
                                                            var url = listImageVideo[subindex].fileShow;
                                                            var tilte = listImageVideo[subindex].fileName;
                                                            return listImageVideo.length < 0
                                                                ? Container()
                                                                : GestureDetector(
                                                                    child: attachmentType!.contains("image")
                                                                        ? GestureDetector(
                                                                            onTap: connection == false
                                                                                ? null
                                                                                : () {
                                                                                    Navigator.push(  context,
                                                                                      PageRouteBuilder(
                                                                                        pageBuilder: (_, __, ___) => ImageViewDownloads(
                                                                                          listimagevide: listImageVideo,
                                                                                          activepage: 0,
                                                                                        ),
                                                                                        transitionDuration: const Duration(seconds: 0),
                                                                                      ),
                                                                                    );
                                                                                  },
                                                                            child: CachedImageNetwork(urlImage: attachmentUrl!),
                                                                          )
                                                                        : attachmentType.contains("video")
                                                                            ? ThumbnailVideoWidget(
                                                                                image: attachmentUrl,
                                                                                onPressed: connection == false
                                                                                    ? null
                                                                                    : () {
                                                                                        setState(() {
                                                                                          Navigator.of(context).push(MaterialPageRoute(builder:(context)=> VideoPlayerWidget(linkVideo: url)));
                                                                                        });
                                                                                      }) : Container());
                                                          }),
                                                        )
                                                      : numImages == 3
                                                          ? Stack(
                                                              children: List.generate(min(listImageVideo .length,maxImage),(subindex) {
                                                                var attachmentUrl = listImageVideo[subindex].fileThumbnail;
                                                                return Container(
                                                                  height:
                                                                      350,
                                                                  child:
                                                                      Row(
                                                                    children: [
                                                                      Expanded(
                                                                        child: GestureDetector(
                                                                          child: listImageVideo[0].fileType!.contains("image")
                                                                              ? GestureDetector(
                                                                                  onTap: connection == false
                                                                                      ? null
                                                                                      : () {
                                                                                          setState(() {
                                                                                            getlocalModel(data[index].id.toString());
                                                                                          });
                                                                                        },
                                                                                  child: SizedBox(height: MediaQuery.of(context).size.height, child: CachedImageNetwork(urlImage: listImageVideo[0].fileThumbnail!)),
                                                                                )
                                                                              : listImageVideo[0].fileType!.contains("video")
                                                                                  ? SizedBox(
                                                                                      height: MediaQuery.of(context).size.height,
                                                                                      child: ThumbnailVideoWidget(
                                                                                          image: listImageVideo[0].fileThumbnail,
                                                                                          onPressed: connection == false
                                                                                              ? null
                                                                                              : () {
                                                                                                    getlocalModel(data[index].id.toString());
                                                                                                }),
                                                                                    )
                                                                                  : Container(),
                                                                          onTap: () {},
                                                                        ),
                                                                      ),
                                                                      const SizedBox(
                                                                        width: 2.5,
                                                                      ),
                                                                      Container(
                                                                        width: MediaQuery.of(context).size.width / 2.7,
                                                                        child: Column(
                                                                          children: [
                                                                            Expanded(
                                                                              child: GestureDetector(
                                                                                  child: listImageVideo[1].fileType!.contains("image")
                                                                                      ? GestureDetector(
                                                                                          onTap: connection == false
                                                                                              ? null
                                                                                              : () {
                                                                                                  setState(() {
                                                                                                    getlocalModel(data[index].id.toString());
                                                                                                  });
                                                                                                },
                                                                                          child: SizedBox(height: MediaQuery.of(context).size.height, child: CachedImageNetwork(urlImage: listImageVideo[1].fileThumbnail!)),
                                                                                        )
                                                                                      : listImageVideo[1].fileType!.contains("video")
                                                                                          ? SizedBox(
                                                                                              height: MediaQuery.of(context).size.height,
                                                                                              child: ThumbnailVideoWidget(
                                                                                                  image: listImageVideo[1].fileThumbnail,
                                                                                                  onPressed: connection == false
                                                                                                      ? null
                                                                                                      : () {
                                                                                                          setState(() {
                                                                                                            getlocalModel(data[index].id.toString());
                                                                                                          });
                                                                                                        }),
                                                                                            )
                                                                                          : Container()),
                                                                            ),
                                                                            const SizedBox(
                                                                              height: 2.6,
                                                                            ),
                                                                            Expanded(
                                                                              child: GestureDetector(
                                                                                child: listImageVideo[2].fileType!.contains("image")
                                                                                    ? GestureDetector(
                                                                                        onTap: connection == false
                                                                                            ? null
                                                                                            : () {
                                                                                                setState(() {
                                                                                                  getlocalModel(data[index].id.toString());
                                                                                                });
                                                                                              },
                                                                                        child: SizedBox(height: MediaQuery.of(context).size.height, child: CachedImageNetwork(urlImage: listImageVideo[2].fileThumbnail!)),
                                                                                      )
                                                                                    : listImageVideo[2].fileType!.contains("video")
                                                                                        ? SizedBox(
                                                                                            height: MediaQuery.of(context).size.height,
                                                                                            child: ThumbnailVideoWidget(
                                                                                                image: listImageVideo[2].fileThumbnail,
                                                                                                onPressed: connection == false
                                                                                                    ? null
                                                                                                    : () {
                                                                                                        setState(() {
                                                                                                          getlocalModel(data[index].id.toString());
                                                                                                        });
                                                                                                      }),
                                                                                          )
                                                                                        : Container(),
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      )
                                                                    ],
                                                                  ),
                                                                );
                                                              }),
                                                            )
                                                          : numImages == 0
                                                              ? Container()
                                                              : GridView(
                                                                  padding:const EdgeInsets.all(0),
                                                                  physics:const ScrollPhysics(),
                                                                  shrinkWrap: true,
                                                                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                                                                      crossAxisSpacing: 2,
                                                                      mainAxisSpacing: 2,
                                                                      maxCrossAxisExtent: MediaQuery.of(context).size.width / 2),
                                                                  children: List.generate(
                                                                      min(listImageVideo.length, maxImage),
                                                                      (subindex) {
                                                                    var attachmentUrl =  listImageVideo[subindex].fileThumbnail;
                                                                    //var attachmentType = data[index].attachments![subindex].fileType;
                                                                    var attachmentType = listImageVideo[subindex].fileType;
                                                                    if (subindex == maxImage - 1) {
                                                                      remaining = (numImages! - maxImage);
                                                                      if (remaining == 0) {
                                                                        return data[index].attachments!.length < 0
                                                                            ? Container()
                                                                            : GestureDetector(
                                                                                child: attachmentType!.contains("image")
                                                                                    ? GestureDetector(
                                                                                        onTap: connection == false
                                                                                            ? null
                                                                                            : () {
                                                                                                setState(() {
                                                                                                  getlocalModel(data[index].id.toString());
                                                                                                });
                                                                                              },
                                                                                        child: CachedImageNetwork(urlImage: attachmentUrl!),
                                                                                      )
                                                                                    : attachmentType.contains("video")
                                                                                        ? GestureDetector(
                                                                                            onTap: connection == false
                                                                                                ? null
                                                                                                : () {
                                                                                                    setState(() {
                                                                                                      getlocalModel(data[index].id.toString());
                                                                                                    });
                                                                                                  },
                                                                                            child: ThumbnailVideoWidget(
                                                                                              image: attachmentUrl,
                                                                                            ))
                                                                                        : Container());
                                                                      } else {
                                                                        return GestureDetector(
                                                                          onTap: () {},
                                                                          child: Stack(
                                                                            fit: StackFit.expand,
                                                                            children: [
                                                                              attachmentType!.contains("image")
                                                                                  ? GestureDetector(
                                                                                      onTap: connection == false
                                                                                          ? null
                                                                                          : () {
                                                                                              setState(() {
                                                                                                getlocalModel(data[index].id.toString());
                                                                                              });
                                                                                            },
                                                                                      child: CachedImageNetwork(urlImage: attachmentUrl!),
                                                                                    )
                                                                                  : attachmentType.contains("video")
                                                                                      ? GestureDetector(
                                                                                          onTap: connection == false
                                                                                              ? null
                                                                                              : () {
                                                                                                  setState(() {
                                                                                                    getlocalModel(data[index].id.toString());
                                                                                                  });
                                                                                                },
                                                                                          child: ThumbnailVideoWidget(
                                                                                            image: attachmentUrl,
                                                                                          ))
                                                                                      : Container(),
                                                                              Positioned.fill(
                                                                                child: GestureDetector(
                                                                                  onTap: connection == false
                                                                                      ? null
                                                                                      : () {
                                                                                          setState(() {
                                                                                            getlocalModel(data[index].id.toString());
                                                                                          });
                                                                                        },
                                                                                  child: Container(
                                                                                    alignment: Alignment.center,
                                                                                    color: Colors.black54,
                                                                                    child: Text(
                                                                                      '+' + remaining.toString(),
                                                                                      style: const TextStyle(fontSize: 32, color: Colors.white),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        );
                                                                      }
                                                                    } else {
                                                                      return attachmentType!.contains("image")
                                                                          ? GestureDetector(
                                                                              onTap: connection == false
                                                                                  ? null
                                                                                  : () {
                                                                                      setState(() {
                                                                                        getlocalModel(data[index].id.toString());
                                                                                      });
                                                                                    },
                                                                              child: CachedImageNetwork(urlImage: attachmentUrl!),
                                                                            )
                                                                          : attachmentType.contains("video")
                                                                              ? GestureDetector(
                                                                                  onTap: connection == false
                                                                                      ? null
                                                                                      : () {
                                                                                          setState(() {
                                                                                            getlocalModel(data[index].id.toString());
                                                                                          });
                                                                                        },
                                                                                  child: ThumbnailVideoWidget(
                                                                                    image: attachmentUrl,
                                                                                  ))
                                                                              : Container(color: Colors.black);
                                                                    }
                                                                  }),
                                                                ),
                                                  //------------------------ End Center Body ------------------
                                                  //------------------------File-------------------------------
                                                  displayFile: ListView .builder(
                                                              physics: const ScrollPhysics(),
                                                              padding: const EdgeInsets.all( 0),
                                                              shrinkWrap: true,
                                                              itemCount:  applications .length,
                                                              itemBuilder: (BuildContext  contex, attindex) {
                                                                var path = applications[attindex] .fileShow.toString();
                                                                var filename = applications[attindex].fileName.toString();
                                                                if (attindex >2) {
                                                                  return Container(
                                                                      color:Colors.black);
                                                                } else {
                                                                  return Column(
                                                                    mainAxisSize: MainAxisSize.max,
                                                                    children: [
                                                                      Container(
                                                                        child: ThumbnailDocWidget(
                                                                          nameFile: applications[attindex].fileName ?? "",
                                                                          sizeFile: applications[attindex].fileSize ?? "",
                                                                          onPressed: () {
                                                                            setState(() {
                                                                              Navigator.of(context).push(MaterialPageRoute(builder:(context) =>ViewDocuments(path: path,filename: filename.toString(),)));
                                                                            });
                                                                          },
                                                                        ),
                                                                      ),
                                                                      attindex == 2
                                                                          ? TextButton(
                                                                              onPressed: connection == false
                                                                                  ? null
                                                                                  : () {
                                                                                      setState(() {
                                                                                        getlocalModel(data[index].id.toString());
                                                                                      });
                                                                                    },
                                                                              child: const Text(
                                                                                "See More",
                                                                                style: TextStyle(color: Colorconstands.primaryColor, fontSize: 15.0),
                                                                              ))
                                                                          : Container()
                                                                    ],
                                                                  );
                                                                }
                                                              }
                                                              //},
                                                              ),
                                                  //------------------------End File------------------
                                                  //------------------------Button Like------------------
                                                  onReactionChanged:
                                                      (String? values, bool isChecked) {
                                                    setState(() {
                                                      if (data[index] .isReact == 0) {
                                                        data[index].totalReact = data[index].totalReact!;
                                                        if (data[index].reactionType == 0 || data[index].isReact == 0) {
                                                          data[index].totalReact =data[index] .totalReact! +1;
                                                          isChecked = true;
                                                          if (values ==  "Like") {
                                                            selecticonreact = 0;
                                                            data[index].isReact = 1;
                                                            data[index] .reactionType = 0;
                                                          } else if (values == "Love") {
                                                            selecticonreact = 1;
                                                            data[index]  .isReact = 1;
                                                            data[index].reactionType = 1;
                                                          } else if (values == "haha") {
                                                            selecticonreact = 2;
                                                            data[index].isReact = 1;
                                                            data[index].reactionType = 2;
                                                          } else if (values == "Wow") {
                                                            selecticonreact = 3;
                                                            data[index].isReact = 1;
                                                            data[index].reactionType = 3;
                                                          } else if (values == "Angry") {
                                                            selecticonreact =  5;
                                                            data[index] .isReact = 1;
                                                            data[index] .reactionType = 5;
                                                          } else {
                                                            selecticonreact = 0;
                                                            data[index] .isReact = 1;
                                                            data[index] .reactionType = 0;
                                                          }
                                                        } else {
                                                          if (values == 'Like') {
                                                            data[index] .isReact = 1;
                                                            data[index].reactionType = 0;
                                                            // _audioCache.play('sounds/like.mp3');
                                                          } else if (values =='Love') {
                                                            data[index] .isReact = 1;
                                                            data[index] .reactionType = 1;
                                                            // _audioCache.play('sounds/box_up.mp3');
                                                          } else if (values == 'haha') {
                                                            data[index].isReact = 1;
                                                            data[index] .reactionType = 2;
                                                            // _audioCache.play('sounds/Laugh_Laugh.mp3');
                                                          } else if (values == 'Wow') {
                                                            data[index] .isReact = 1;
                                                            data[index].reactionType = 3;
                                                            // _audioCache.play('sounds/Wow.mp3');
                                                          } else if (values == 'Unselect' && isChecked == true) {
                                                            isChecked = false;
                                                            selecticonreact = 0;
                                                            data[index].isReact = 0;
                                                            data[index] .reactionType = 0;
                                                            data[index].totalReact =
                                                                data[index].totalReact! - 1;
                                                          } else if (data[index] .reactionType ==0) {
                                                            isChecked = true;
                                                            selecticonreact = 0;
                                                            data[index].isReact = 1;
                                                            data[index].reactionType = 0;
                                                          } else if (values =='Angry') {
                                                            data[index] .isReact = 1;
                                                            data[index] .reactionType = 5;
                                                            // _audioCache.play('sounds/Nani_anime.mp3');
                                                          } else {
                                                            data[index].isReact = 1;
                                                            data[index].reactionType = 5;
                                                          }
                                                        }
                                                      } else if (data[index] .reactionType == 6 || data[index].isReact ==0) {
                                                        data[index].totalReact = data[index].totalReact! +1;
                                                        isChecked =true;
                                                        selecticonreact =0;
                                                        data[index].isReact = 1;
                                                        data[index].reactionType = 0;
                                                      } else {
                                                        if (values == 'Like') {
                                                          data[index].isReact = 1;
                                                          data[index] .reactionType = 0;
                                                          // _audioCache.play('sounds/like.mp3');
                                                        } else if (values == 'Love') {
                                                          data[index].isReact = 1;
                                                          data[index] .reactionType = 1;
                                                          // _audioCache.play('sounds/box_up.mp3');
                                                        } else if (values == 'haha') {
                                                          data[index].isReact = 1;
                                                          data[index].reactionType = 2;
                                                          // _audioCache.play('sounds/Laugh_Laugh.mp3');
                                                        } else if (values == 'Wow') {
                                                          data[index].isReact = 1;
                                                          data[index].reactionType = 3;
                                                          // _audioCache.play('sounds/Wow.mp3');
                                                        } else if (isChecked ==false &&
                                                            values =='Unselect' && data[index].reactionType ==0) {
                                                          selecticonreact = 5;
                                                          data[index] .reactionType = 5;
                                                          data[index] .isReact = 1;
                                                        } else if (values =='Unselect' && isChecked ==true) {
                                                          isChecked =false;
                                                          selecticonreact =0;
                                                          data[index].isReact = 0;
                                                          data[index]
                                                              .reactionType = 0;
                                                          data[index]
                                                                  .totalReact =
                                                              data[index]
                                                                      .totalReact! -
                                                                  1;
                                                        } else {
                                                          data[index]
                                                              .isReact = 1;
                                                          data[index]
                                                              .reactionType = 5;
                                                          // _audioCache.('sounds/Nani_anime.mp3');
                                                        }
                                                      }
                                                    });
                                                    setState(() {
                                                      values == "Like"
                                                          ? reactiondefultselect =
                                                              "assets/images/actionButton/like_full_fill.png"
                                                          : values ==
                                                                  "Love"
                                                              ? reactiondefultselect =
                                                                  "assets/images/actionButton/Love.png"
                                                              : values ==
                                                                      "haha"
                                                                  ? reactiondefultselect =
                                                                      "assets/images/actionButton/Icon_haha.png"
                                                                  : values == "Wow"
                                                                      ? reactiondefultselect = "assets/images/actionButton/wow2.png"
                                                                      : values == "Angry"
                                                                          ? reactiondefultselect = "assets/images/actionButton/angry2.png"
                                                                          : reactiondefultselect = "assets/images/actionButton/like_full_fill.png";
                                                    });
                                                    PostReaction
                                                        _postReact =
                                                        PostReaction();
                                                    _postReact.postReact(
                                                        "/class-time-line/react/",
                                                        data[index] .reactionType!, data[index] .id!);
                                                    print(
                                                        'Selected value: $values, isChecked: $isChecked');
                                                  },
                                                  //------------------------End Button Like------------------
                                                  //===================== button comment ====================
                                                  onTapcomment:
                                                      connection ==
                                                              false
                                                          ? null
                                                          : () {
                                                            BlocProvider.of<ClassTimeLineDetailBloc>(context).add(ClassTimeLineFetchDetailEvent(idPost: data[index].id!.toString(), classId: BlocProvider.of<ChangeChildBloc>(context).classIDBloc.toString()));
                                                            Navigator.of(context).push( MaterialPageRoute( fullscreenDialog: true, builder: (context) {return const CommentPostClassTimeline();}));
                                                            },
                                                  onTapBodycomment: connection == false
                                                          ? null
                                                          : () {
                                                              BlocProvider.of<ClassTimeLineDetailBloc>(context).add(ClassTimeLineFetchDetailEvent( idPost: data[index].id!.toString(), classId: BlocProvider.of<ChangeChildBloc>(context).classIDBloc.toString()));
                                                              Navigator.of(context).push( MaterialPageRoute( fullscreenDialog: true, builder: (context) {return const CommentPostClassTimeline();}));
                                                            },
                                                  //===================== End button comment ====================
                                                );
                                            }else{
                                              return Container(
                                              color: Colors.white,
                                              child: Center(
                                                child: isReachedMax == true
                                                    ? Container(
                                                        color: Colors.grey.withOpacity(.2),
                                                        width: MediaQuery.of(context).size.width,
                                                        padding: const EdgeInsets.symmetric(vertical: 30),
                                                        child: Column(
                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                          children: [
                                                            SvgPicture.asset(
                                                              "assets/icons/carbon_message-queue.svg",
                                                              width: 50,
                                                              color: Colors.grey,
                                                            ),
                                                            const SizedBox(
                                                              height: 20,
                                                            ),
                                                            Text(
                                                              "All Data Loaded",
                                                              style: ThemeConstands.button_SemiBold_16
                                                                  .copyWith(color: Colors.grey),
                                                            ),
                                                          ],
                                                        ),
                                                      )
                                                    : const ShimmerTimeLine(),
                                              ),
                                            );
                                            }
                                        },
                                        separatorBuilder:(BuildContext context,int index) {
                                            return Container(
                                              height: 8.0,
                                              color:Colorconstands.gray,
                                            );
                                          },
                                        ),
                                                          );
                                                        }),
                                                    ),
                                        )    ),
                                          ),
                              )
          ],
        );
  } else {
    return Container();
  }
},
),
),
),
),
],
);
}

  void getlocalModel(String postID) async {
     BlocProvider.of<ClassTimeLineDetailBloc>(context).add(ClassTimeLineFetchDetailEvent( idPost:postID.toString(), classId: BlocProvider.of<ChangeChildBloc>(context).classIDBloc.toString()));
      Navigator.of(context).push(MaterialPageRoute(builder: (context) => const ViewDetailClassTilemine()));
    // setState(() {
    //   BlocProvider.of<ClassTimeLineDetailBloc>(context).add(ClassTimeLineFetchDetailEvent(idPost: postID.toString(), classId: classID.toString()));
    //   Navigator.of(context).push(MaterialPageRoute(builder: (context) => const ViewDetailClassTilemine()));
    // });
  }

}
