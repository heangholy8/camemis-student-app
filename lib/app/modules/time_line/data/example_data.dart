import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_reaction_button/flutter_reaction_button.dart';

final defaultInitialReaction = Reaction<String>(
  value: 'Like',
  icon: _buildReactionsIcon(
      'assets/icons/like_icon.png',
      const Text(
        'Like',style: TextStyle(color: Colorconstands.primaryColor,fontSize: 16)
      )
      )
);

final reactions = [
  Reaction<String>(
    value: 'Like',
    title: _buildTitle('Like'),
    previewIcon: _buildReactionsPreviewIcon('assets/images/actionButton/like.gif'),
    icon: _buildReactionsIcon(
      'assets/images/actionButton/like_full_fill.png',
      const Text(
        'Like',
        style: TextStyle(
          color: Colorconstands.primaryColor,fontSize: 16
        ),
      ),
    ),
  ),
  Reaction<String>(
    value: 'Love',
    title: _buildTitle('Love'),
    previewIcon: _buildReactionsPreviewIcon('assets/images/actionButton/love.gif'),
    icon: _buildReactionsIcon(
      'assets/images/actionButton/Love.png',
      const Text(
        'Love',
        style: TextStyle(
          color: Color(0xFFB72F23),fontSize: 16
        ),
      ),
    ),
  ),
  Reaction<String>(
    value: 'haha',
    title: _buildTitle('haha'),
    previewIcon: _buildReactionsPreviewIcon('assets/images/actionButton/haha.gif'),
    icon: _buildReactionsIcon(
      'assets/images/actionButton/Icon_haha.png',
      const Text(
        'haha',
        style: TextStyle(
          color: Color(0xFFFFCC00),fontSize: 16
        ),
      ),
    ),
  ),
  Reaction<String>(
    value: 'Wow',
    title: _buildTitle('Wow'),
    previewIcon: _buildReactionsPreviewIcon('assets/images/actionButton/wow.gif'),
    icon: _buildReactionsIcon(
      'assets/images/actionButton/icon_wow.png',
      const Text(
        'Wow',
        style: TextStyle(
          color: Color(0xFFFFCC00),fontSize: 16
        ),
      ),
    ),
  ),
  
  Reaction<String>(
    value: 'Angry',
    title: _buildTitle('Angry'),
    previewIcon: _buildReactionsPreviewIcon('assets/images/actionButton/angry.gif'),
    icon: _buildReactionsIcon(
      'assets/images/actionButton/angry2.png',
      const Text(
        'Angry',
        style: TextStyle(
          color: Color(0xFFFFCC00),fontSize: 16
        ),
      ),
    ),
  ),
  Reaction<String>(
    value: 'Unselect',
    previewIcon:const Icon(Icons.add_reaction_sharp,size: 1,),
    icon: _buildReactionsIcon(
      'assets/images/actionButton/like_icon.png',
      const Text(
        'Unselect',
        style: TextStyle(
          color: Colorconstands.primaryColor,fontSize: 16
        ),
      ),
    ),
  ),
  
  
];

Container _buildTitle(String title) {
  return Container(
    padding: const EdgeInsets.symmetric(horizontal: 7.5, vertical: 2.5),
    decoration: BoxDecoration(
      color: title=='Like'?Colorconstands.primaryColor:title=='Love'?const Color(0xFFB72F23):const Color(0xFFFFCC00),
      borderRadius: BorderRadius.circular(15),
    ),
    child: Text(
      title,
      style:const TextStyle(
        color: Colors.white,
        fontSize: 10,
        fontWeight: FontWeight.bold,
      ),
    ),
  );
}

Padding _buildReactionsPreviewIcon(String path) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 3.5, vertical: 5),
    child: Image.asset(path, height: 40),
  );
}

Image _buildIcon(String path) {
  return Image.asset(
    path,
    height: 30,
    width: 30,
  );
}

Container _buildReactionsIcon(String path, Text text) {
  return Container(
    color: Colors.transparent,
    child: Row(
      children: <Widget>[
        Image.asset(path, height: 20),
        const SizedBox(width: 5),
        text,
      ],
    ),
  );
}
