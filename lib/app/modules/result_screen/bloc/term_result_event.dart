part of 'term_result_bloc.dart';

abstract class TermResultEvent {
  const TermResultEvent();

  @override
  List<Object> get props => [];
}

class ResultTermEvent extends TermResultEvent {
  String studentId, classId, term;

  ResultTermEvent(
      {required this.studentId, required this.classId, required this.term});
}

// class ResultFirstTermEvent extends TermResultEvent {
//   String studentId, classId, term;

//   ResultFirstTermEvent(
//       {required this.studentId, required this.classId, required this.term});
// }

// class ResultSecondTermEvent extends TermResultEvent {
//   String studentId, classId, term;

//   ResultSecondTermEvent(
//       {required this.studentId, required this.classId, required this.term});
// }
