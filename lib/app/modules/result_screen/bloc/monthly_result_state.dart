part of 'monthly_result_bloc.dart';

abstract class MonthlyResultState {
  const MonthlyResultState();

  @override
  List<Object> get props => [];
}

class MonthlyResultInitial extends MonthlyResultState {}

class MonthlyResultLoadingState extends MonthlyResultState {}

class MonthlyResultLoadedState extends MonthlyResultState {
  final MonthlyResultModel? currentMonthlyResult;
  const MonthlyResultLoadedState({this.currentMonthlyResult});
}

class MonthlyResultErrorState extends MonthlyResultState {
  final String error;
  const MonthlyResultErrorState({required this.error});
}
