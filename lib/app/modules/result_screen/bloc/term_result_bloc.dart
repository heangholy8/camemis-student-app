import 'package:bloc/bloc.dart';
import 'package:camis_application_flutter/model/result/semester_result.dart';
import 'package:meta/meta.dart';

import '../../../../service/apis/result_api/get_semester_result.dart';

part 'term_result_event.dart';
part 'term_result_state.dart';

class TermResultBloc extends Bloc<TermResultEvent, TermResultState> {
  final TermResultApi currenFirstApi;
  TermResultBloc({required this.currenFirstApi}) : super(TermResultInitial()) {
    on<ResultTermEvent>((event, emit) async {
      emit(TermResultLoadingState());
      try {
        var data = await currenFirstApi.getTermResultApi(
            studenId: event.studentId,
            classId: event.classId,
            term: event.term);
        emit(TermResultLoadedState(currentTermResult: data));
      } catch (e) {
        emit(TermResultErrorState(error: e.toString()));
      }
    });
  }
}

// class FirstTermResultBloc extends Bloc<ResultFirstTermEvent, TermResultState> {
//   final TermResultApi currenFirstApi;
//   FirstTermResultBloc({required this.currenFirstApi})
//       : super(TermResultInitial()) {
//     on<ResultFirstTermEvent>((event, emit) async {
//       emit(TermResultLoadingState());
//       try {
//         var data = await currenFirstApi.getTermResultApi(
//             studenId: event.studentId,
//             classId: event.classId,
//             term: event.term);
//         emit(FirstTermResultLoadedState(currentTermResult: data));
//       } catch (e) {
//         emit(TermResultErrorState(error: e.toString()));
//       }
//     });
//   }
// }

// class SecondTermResultBloc
//     extends Bloc<ResultSecondTermEvent, TermResultState> {
//   final TermResultApi currenFirstApi;
//   SecondTermResultBloc({required this.currenFirstApi})
//       : super(TermResultInitial()) {
//     on<ResultSecondTermEvent>((event, emit) async {
//       emit(TermResultLoadingState());
//       try {
//         var data = await currenFirstApi.getTermResultApi(
//             studenId: event.studentId,
//             classId: event.classId,
//             term: "SECOND_SEMESTER");
//         emit(SecondTermResultLoadedState(currentTermResult: data));
//       } catch (e) {
//         emit(TermResultErrorState(error: e.toString()));
//       }
//     });
//   }
// }
