part of 'year_bloc.dart';

abstract class YearResultState {
  const YearResultState();

  @override
  List<Object> get props => [];
}

class YearResultInitial extends YearResultState {}

class YearResultLoadingState extends YearResultState {}

class YearResultLoadedState extends YearResultState {
  final YearResultModel? currentResult;
  const YearResultLoadedState({this.currentResult});
}

class YearResultErrorState extends YearResultState {
  final String error;
  const YearResultErrorState({required this.error});
}
