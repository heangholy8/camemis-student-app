part of 'term_result_bloc.dart';

abstract class TermResultState {
  const TermResultState();

  @override
  List<Object> get props => [];
}

class TermResultInitial extends TermResultState {}

class TermResultLoadingState extends TermResultState {}

class TermResultLoadedState extends TermResultState {
  final TermResultModel? currentTermResult;
  const TermResultLoadedState({this.currentTermResult});
}

// class FirstTermResultLoadedState extends TermResultState {
//   final TermResultModel? currentTermResult;
//   const FirstTermResultLoadedState({this.currentTermResult});
// }

// class SecondTermResultLoadedState extends TermResultState {
//   final TermResultModel? currentTermResult;
//   const SecondTermResultLoadedState({this.currentTermResult});
// }

class TermResultErrorState extends TermResultState {
  final String error;
  const TermResultErrorState({required this.error});
}
