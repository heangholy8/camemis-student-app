import 'package:bloc/bloc.dart';
import 'package:camis_application_flutter/model/result/monthly_result.dart';
import 'package:camis_application_flutter/service/apis/result_api/get_monthly_result.dart';
part 'monthly_result_event.dart';
part 'monthly_result_state.dart';

class MonthlyResultBloc extends Bloc<MonthlyResultEvent, MonthlyResultState> {
  final MonthlyResultApi currenApi;
  MonthlyResultBloc({required this.currenApi}) : super(MonthlyResultInitial()) {
    on<ResultMonthlyEvent>((event, emit) async {
      emit(MonthlyResultLoadingState());
      try {
        var data = await currenApi.getMonthlyResultApi(
            studenId: event.studentId,
            classId: event.classId,
            month: event.month,
            term: event.term);
        emit(MonthlyResultLoadedState(currentMonthlyResult: data));
      } catch (e) {
        emit(MonthlyResultErrorState(error: e.toString()));
      }
    });
  }
}
