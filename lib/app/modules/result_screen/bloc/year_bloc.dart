import 'package:bloc/bloc.dart';
import 'package:camis_application_flutter/service/apis/result_api/get_yeatly_result.dart';

import '../../../../model/result/yearly_result.dart';

part 'year_event.dart';
part 'year_state.dart';

class YearResultBloc extends Bloc<ResultYearEvent, YearResultState> {
  final YearResultApi currenApi;
  YearResultBloc({required this.currenApi}) : super(YearResultInitial()) {
    on<ResultYearEvent>((event, emit) async {
      emit(YearResultLoadingState());
      try {
        var data = await currenApi.getYearResultApi(
          studenId: event.studentId,
          classId: event.classId,
        );
        emit(YearResultLoadedState(currentResult: data));
      } catch (e) {
        emit(YearResultErrorState(error: e.toString()));
      }
    });
  }
}
