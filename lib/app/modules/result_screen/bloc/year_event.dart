part of 'year_bloc.dart';

abstract class YearResultEvent {
  const YearResultEvent();

  @override
  List<Object> get props => [];
}

class ResultYearEvent extends YearResultEvent {
  String studentId, classId;

  ResultYearEvent({required this.studentId, required this.classId});
}
