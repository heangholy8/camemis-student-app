part of 'monthly_result_bloc.dart';

abstract class MonthlyResultEvent {
  const MonthlyResultEvent();

  @override
  List<Object> get props => [];
}

class ResultMonthlyEvent extends MonthlyResultEvent {
  String studentId, classId, month, term;

  ResultMonthlyEvent(
      {required this.studentId,
      required this.classId,
      required this.month,
      required this.term});
}
