import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../../model/result/detail_result.dart';
import '../../../../../service/apis/result_api/get_detail_result.dart';

part 'detail_result_event.dart';
part 'detail_result_state.dart';

class DetailResultBloc extends Bloc<ResultDetailEvent, DetailResultState> {
  final DetailResultApi currenApi;
  DetailResultBloc({required this.currenApi}) : super(DetailResultInitial()) {
    on<ResultDetailEvent>((event, emit) async {
      emit(DetailResultLoadingState());
      try {
        var data = await currenApi.getDetailResultApi(
          studenId: event.studentId,
          classId: event.classId,
        );
        emit(DetailResultLoadedState(currentDetailResult: data));
      } catch (e) {
        emit(DetailResultErrorState(error: e.toString()));
      }
    });
  }
}
