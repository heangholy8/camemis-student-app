part of 'detail_result_bloc.dart';

abstract class DetailResultState {
  const DetailResultState();

  @override
  List<Object> get props => [];
}

class DetailResultInitial extends DetailResultState {}

class DetailResultLoadingState extends DetailResultState {}

class DetailResultLoadedState extends DetailResultState {
  final DetailResultModel? currentDetailResult;
  const DetailResultLoadedState({this.currentDetailResult});
}

class DetailResultErrorState extends DetailResultState {
  final String error;
  const DetailResultErrorState({required this.error});
}
