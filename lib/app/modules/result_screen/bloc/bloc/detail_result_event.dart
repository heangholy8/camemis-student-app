part of 'detail_result_bloc.dart';

abstract class DetailResultEvent {
  const DetailResultEvent();

  @override
  List<Object> get props => [];
}

class ResultDetailEvent extends DetailResultEvent {
  String studentId, classId;

  ResultDetailEvent({required this.studentId, required this.classId});
}
