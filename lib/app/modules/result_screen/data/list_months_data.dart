import 'package:camis_application_flutter/app/modules/result_screen/model/months_model.dart';

List<ListMonthsModel> listMonths = [
  ListMonthsModel("មករា", false, false, true, true),
  ListMonthsModel("កុម្ផះ", false, false, true, true),
  ListMonthsModel("មីនា", false, false, true, true),
  ListMonthsModel("មេសា", true, false, true, true),
  ListMonthsModel("ឧសភា", false, false, true, false),
  ListMonthsModel("លទ្ធផលប្រចាំឆមាសទី១", false, false, false, true),
  ListMonthsModel("មិថុនា", false, false, true, true),
  ListMonthsModel("កក្កដា", false, false, true, false),
  ListMonthsModel("សីហា", false, false, true, false),
  ListMonthsModel("កញ្ញា", false, false, true, false),
  ListMonthsModel("តុលា", false, false, true, false),
  ListMonthsModel("វិច្ឆិកា", false, false, true, false),
  ListMonthsModel("ធ្នូ", false, false, true, false),
  ListMonthsModel("លទ្ធផលប្រចាំឆមាសទី២", false, false, false, false),
  ListMonthsModel("លទ្ធផលប្រចាំឆ្នាំ", false, false, false, false),
];
