import 'package:camis_application_flutter/app/modules/result_screen/model/summary_result.dart';
import 'package:easy_localization/easy_localization.dart';

List<SummaryResult> summaryAbsent = [
  SummaryResult("PERMISSION".tr()+": ", "0", "0", "0"),
  SummaryResult("ABSENT".tr()+": ", "0", "0", "0")
];
List<SummaryResult> summaryValuation = [
  SummaryResult("Morality:", "ល្អ", "--", "--"),
  SummaryResult("Labor - Cultivation:", "ល្អ", "--", "--"),
  SummaryResult("Health - Hygiene:", "ល្អ", "--", "--")
];
List<SummaryResult> summaryMedal = [
  SummaryResult("Certificate of Appreciation:", "0", "0", "0"),
  SummaryResult("Letter of commendation:", "0", "0", "0"),
  SummaryResult("Incentives:", "0", "0", "0"),
];
