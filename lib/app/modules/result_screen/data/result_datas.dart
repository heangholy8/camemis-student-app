import 'package:camis_application_flutter/app/modules/result_screen/model/result_model.dart';

List<ResultData> resultDatas = [
  ResultData("1", "1", "ភាសារខ្មែរ", 80, "A", "ល្អ", 1, 40, 100),
  ResultData("2", "2", "សីលធម៌ ", 80, "A", "ល្អ", 1, 40, 100),
  ResultData("3", "3", "តែងសេចក្ដី ", 80, "C", "មធ្យម", 1, 40, 100),
  ResultData("4", "4", "គណិតវិទ្យា ", 80, "B", "ល្អបង្គួរ", 1, 40, 100),
  ResultData("5", "5", "គីមីវិទ្យា", 80, "B", "ល្អបង្គួរ", 1, 40, 100),
  ResultData("6", "6", "រូបវិទ្យា ", 80, "D", "ខ្សោយ", 1, 40, 100),
  ResultData("7", "7", "ភាសារខ្មែរ", 80, "F", "ធ្លាក់", 1, 40, 100),
  ResultData("8", "8", "ភាសារខ្មែរ", 80, "F", "ធ្លាក់", 1, 40, 100),
];
