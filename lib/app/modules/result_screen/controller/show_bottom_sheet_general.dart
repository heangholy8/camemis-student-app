import 'package:flutter/material.dart';

import '../../../core/themes/themes.dart';

Future<void> showButtomSheet(
    context, String title, String year, String subtitle) async {
  return showModalBottomSheet(
      backgroundColor: Colors.white,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(25),
        ),
      ),
      context: context,
      builder: (BuildContext context) {
        return Container(
            height: 900,
            padding: const EdgeInsets.only(top: 30),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(bottom: 20),
                  width: MediaQuery.of(context).size.width,
                  decoration:
                      const BoxDecoration(color: Colors.white, boxShadow: [
                    BoxShadow(
                        blurRadius: 4,
                        color: Color.fromRGBO(222, 222, 222, 0.25),
                        offset: Offset(0, 4))
                  ]),
                  child: Column(children: [
                    Text(
                      title,
                      style: ThemeConstands.texttheme.headline5,
                    ),
                    Text(
                      year,
                      style: ThemeConstands.texttheme.bodyLarge,
                    ),
                  ]),
                ),
                const SizedBox(
                  height: 5,
                ),
                Expanded(
                    child: Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(vertical: 10),
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  color: Colors.white,
                  child: Text(
                    subtitle,
                    style: ThemeConstands.texttheme.bodyLarge,
                  ),
                ))
              ],
            ));
      });
}
