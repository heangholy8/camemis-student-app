import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:camis_application_flutter/app/modules/result_screen/data/list_months_data.dart';
import 'package:flutter/material.dart';

Future<void> showButtomSheetMonth(context, bool isPaid) async {
  return showModalBottomSheet(
      backgroundColor: Colors.white,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(25),
        ),
      ),
      context: context,
      builder: (BuildContext context) {
        return Container(
            height: 900,
            margin: const EdgeInsets.only(top: 30),
            child: Column(
              children: [
                Container(
                  padding:const EdgeInsets.only(bottom: 20),
                  width: MediaQuery.of(context).size.width,
                  decoration:
                      const BoxDecoration(color: Colors.white, boxShadow: [
                    BoxShadow(
                        blurRadius: 4,
                        color: Color.fromRGBO(222, 222, 222, 0.25),
                        offset: Offset(0, 4))
                  ]),
                  child: Column(children: [
                    Text(
                      "ជ្រេីសរេីសខែ",
                      style: ThemeConstands.texttheme.headline5,
                    ),
                    Text(
                      "សម្រាបឆ្នាំសិក្សា ២០២២-២០២៣",
                      style: ThemeConstands.texttheme.bodyLarge,
                    ),
                  ]),
                ),
                const SizedBox(
                  height: 5,
                ),
                Expanded(
                    child: ListView.builder(
                  itemCount: listMonths.length,
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        listMonths
                            .forEach((element) => element.isSelected = false);
                        listMonths[index].selected = true;
                        if (listMonths[index].isSelected == false &&
                            listMonths[index].selected == true) {
                          listMonths[index].isSelected = true;
                          isPaid = listMonths[index].isPaid;
                        }

                        Navigator.pop(context);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          border: const Border(
                              bottom: BorderSide(
                                  width: 1.5,
                                  color: Color.fromRGBO(244, 244, 244, 1))),
                          color: listMonths[index].isSelected
                              ? const Color.fromRGBO(241, 249, 255, 1)
                              : Colors.white,
                        ),
                        padding: const EdgeInsets.symmetric(horizontal: 30),
                        child: Column(
                          children: [
                            Container(
                              padding: const EdgeInsets.symmetric(vertical: 10),
                              margin:const EdgeInsets.only(top: 5),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    listMonths[index].name,
                                    style: ThemeConstands.texttheme.headline6
                                        ?.copyWith(
                                            color: listMonths[index].isMonth
                                                ? Colorconstands.primaryColor
                                                : Colors.red),
                                  ),
                                  Icon(
                                    listMonths[index].isSelected
                                        ? Icons.check_circle
                                        : Icons.circle_outlined,
                                    color: Colorconstands.primaryColor,
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ))
              ],
            ));
      });
}
