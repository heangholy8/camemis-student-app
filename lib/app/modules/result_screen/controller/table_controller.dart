import 'package:camis_application_flutter/app/core/themes/theme_config.dart';
import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:camis_application_flutter/app/modules/result_screen/model/result_model.dart';
import 'package:flutter/material.dart';

import '../widget/custom_header_table.dart';

List<DataColumn> getColumns(List<String> columns) => columns
    .map((String column) => DataColumn(label: CustomTableHeader(title: column)))
    .toList();
List<DataRow> getRows(List<ResultData> tableDatas) =>
    tableDatas.map((ResultData tableData) {
      final cells = [
        tableData.no,
        tableData.subjectName,
        tableData.score.toString() + "/" + tableData.tottalscore.toString(),
        tableData.gradeKH + "(" + tableData.grade + ")",
        tableData.rank.toString() + "/" + tableData.totalStudent.toString()
      ];
      return DataRow(cells: getCells(cells));
    }).toList();

List<DataCell> getCells(List<dynamic> cells) => cells
    .map((data) => DataCell(Text(
          "$data",
          style: ThemeConstands.texttheme.bodyLarge?.copyWith(
              color: data != "ធ្លាក់(F)" ? Colors.black : Colors.red[300]),
        )))
    .toList();
