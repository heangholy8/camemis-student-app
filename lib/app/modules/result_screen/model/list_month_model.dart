class ListMonthModel{
  String displayName,month,displayNameEn,year,semester,classID;
  bool?isSelected,isActive,isMonth;
  int index;
  ListMonthModel(this.displayName, this.month, this.displayNameEn, this.year, this.semester, this.index,{this.isSelected=false,this.isActive=false,this.classID="",this.isMonth=true});
}