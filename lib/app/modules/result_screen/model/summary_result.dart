class SummaryResult {
  final String title;
  final String semester1;
  final String semester2;
  final String yearly;

  SummaryResult(this.title, this.semester1, this.semester2, this.yearly);
}
