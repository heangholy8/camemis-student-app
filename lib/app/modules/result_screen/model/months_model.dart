class ListMonthsModel {
  String name;
  bool isSelected;
  bool selected;
  bool isMonth;
  bool isPaid;

  ListMonthsModel(
      this.name, this.isSelected, this.selected, this.isMonth, this.isPaid);
}
