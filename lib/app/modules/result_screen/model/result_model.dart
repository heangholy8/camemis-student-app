class ResultData {
  String id;
  String no;
  String subjectName;
  int score;
  String grade;
  String gradeKH;
  int rank;
  int totalStudent;
  int tottalscore;

  ResultData(this.id, this.no, this.subjectName, this.score, this.grade,
      this.gradeKH, this.rank, this.totalStudent, this.tottalscore);
}
