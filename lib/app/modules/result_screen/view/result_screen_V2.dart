// ignore_for_file: avoid_print

import 'dart:async';
import 'package:blurrycontainer/blurrycontainer.dart';
import 'package:camis_application_flutter/app/core/resources/asset_resource.dart';
import 'package:camis_application_flutter/app/modules/PaymentAndBillScreen/bloc/bloc/check_payment_expire_bloc.dart';
import 'package:camis_application_flutter/app/modules/result_screen/model/list_month_model.dart';
import 'package:camis_application_flutter/app/modules/time_line/school_time_line/e.schooltimeline.dart';
import 'package:camis_application_flutter/service/apis/profile_user/bloc/get_profile_user_bloc.dart';
import 'package:camis_application_flutter/widgets/lock_screen_no_paid_widget/lock_screen_unpaid_widget.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import '../../../../helper/route.export.dart';
import '../../../../widgets/error_widget/error_request.dart';
import '../../../../widgets/navigate_bottom_bar_widget/navigate_bottom_bar.dart';
import '../../../../widgets/no_connection_alert_widget.dart';
import '../../../routes/e.route.dart';
import '../../attendance_screen.dart/widgets/schedule_empty.dart';

class ResultScreenV2 extends StatefulWidget {
  int? activeStudent;
  ResultScreenV2({Key? key,this.activeStudent=0}) : super(key: key);

  @override
  State<ResultScreenV2> createState() => _ResultScreenV2State();
}

class _ResultScreenV2State extends State<ResultScreenV2>
    with TickerProviderStateMixin, Toast {
  bool paid = true;
  double _height = 0;
  String monthName = "", studentId = "", classId = "";
  String monthID = "", semesterID = "";
  int? buttonSelected;
  double isActive = 0;
  double tangleaction = -1;
  int resultMonthlyIndex = 0;
  int? selectChild;
  List<ListMonthModel> _listMonth = [];
  int? monthlocal;
  int? monthlSelected;
  bool firstChildNoclass = false;
  List<String> headerlabel = [
    "NO".tr(),
    "SUBJECT".tr(),
    "SCORE".tr(),
    "GRADE".tr(),
    "RANK".tr(),
  ];
  String plateform = "";
  List<String> headerLabelYear = [
    "SUBJECT".tr(),
    "AS1".tr(),
    "AS2".tr(),
    'ASY'.tr(),
    "RANK".tr(),
  ];
  bool? isMonth, isFirstRun;
  // MonthlyData? dataResult;
  // TermData? termdataResult;
  StreamSubscription? internetconnection;
  bool isoffline = true;
  bool isPiad = true;
  String dropdownValue = "";
  bool selected = true;
  bool firstLoad = true;
  bool selectMonth = false;
  bool isShowMonthlyResult = false;
  bool isYear = false;
  @override
  void initState() {
    super.initState();
    isMonth = true;
    isFirstRun = true;
    //=============Check internet====================
    internetconnection = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      // whenevery connection status is changed.
      if (result == ConnectivityResult.none) {
        //there is no any connection
        setState(() {
          isoffline = false;
        });
      } else if (result == ConnectivityResult.mobile) {
        //connection is mobile data network
        setState(() {
          isoffline = true;
        });
      } else if (result == ConnectivityResult.wifi) {
        //connection is from wifi
        setState(() {
          isoffline = true;
        });
      }
    }); //
    monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
    //=============Eend Check internet====================

    if (isoffline == true && firstLoad == true) {
      setState(() {
        BlocProvider.of<GetProfileUserBloc>(context).add(GetProfileUser());
        BlocProvider.of<CheckPaymentExpireBloc>(context).add(CheckPaymentEvent());
      });
    }
  }

  @override
  void dispose() {
    internetconnection!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    print("tarnslate : $translate");
    return Scaffold(
      bottomNavigationBar: isoffline == false
          ? Container(
              height: 0,
            )
          : BottomNavigateBar(
              isActive: 3,
              paid: paid,
            ),
      backgroundColor: Colorconstands.neutralWhite,
      body: Container(
        color: Colorconstands.primaryColor,
        child: Stack(
          children: [
            Positioned(
              top: 0,
              right: 0,
              child: Image.asset("assets/images/Oval.png"),
            ),
            Positioned(
                left: 0,
                top: 50,
                child: SvgPicture.asset(
                  ImageAssets.path20,
                  color: Colorconstands.secondaryColor,
                )),
            Column(
              children: [
                Container(
                  margin: const EdgeInsets.only(
                      top: 50, bottom: 20, left: 22, right: 22),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "RESULT".tr(),
                        style: ThemeConstands.headline2_SemiBold_24.copyWith(
                            color: Colorconstands.lightTextsRegular,
                            fontWeight: FontWeight.bold),
                      ),
                      //SvgPicture.asset(ImageAssets.questoin_icon)
                    ],
                  ),
                ),
                Expanded(
                  child: BlocListener<CheckPaymentExpireBloc,
                      CheckPaymentExpireState>(
                    listener: (context, state) {
                      if (state is CheckPaymentExpireLoaded) {
                        var dataStatus = state.checkpayment!.status;
                        setState(() {
                          paid = dataStatus!;
                        });
                      }
                    },
                    child: BlocConsumer<GetProfileUserBloc, GetProfileUserState>(
                      listener: ((context, state) {
                        if(state is ProfileUserLoaded){
                          var data = state.profileusermodel.data;
                          setState(() {
                           if(data!.currentClass==null){
                              firstChildNoclass = true;
                            }
                            else{
                              firstChildNoclass = false;
                            }
                          },);
                        }
                      }),
                      builder: (context, state) {
                        if (state is ProfileUserLoadingState) {
                          return Container(
                              decoration: const BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(16),
                                    topRight: Radius.circular(16)),
                                color: Colorconstands.neutralWhite,
                              ),
                              child: _buildLoading());
                        }
                        else if (state is ProfileUserLoaded) {
                          var data = state.profileusermodel.data;
                          var dataMonth= data!.currentClass;
                          if (data != null) {
                            if (isoffline == true) {
                              if (firstLoad == true) {
                                if(data.currentClass==null){
                                }
                                else{
                                studentId = data.id.toString();
                                classId = data.currentClass!.classId.toString();
                                if (isFirstRun!) {
                                  isFirstRun =false;
                                  var index = 1;
                                  for (var element in dataMonth!.listMonths!) {
                                    for (var month in element.months!) {
                                      _listMonth.add(ListMonthModel(
                                          translate == "km"
                                              ? month.displayMonth!
                                              : month.displayMonthEn!,
                                          month.month!,
                                          month.displayMonthEn!,
                                          month.year!,
                                          element.semester!,
                                          int.parse(month.month.toString()),
                                          isSelected:false));
                                      if (month.isCurrent == true) {
                                        Future.delayed(
                                          const Duration(milliseconds:2),
                                          () {
                                            setState(
                                                () {
                                              monthName =  "${translate == "km" ? month.displayMonth : month.displayMonthEn} ${month.year}";
                                              monthID =  month.month.toString();
                                              semesterID = element.semester.toString();
                                              _listMonth.forEach((element) {
                                                element.month == monthID ? element.isSelected = true : element.isSelected = false;
                                                // int.parse(month.month!) >= element.index || element.year==month.year ? element.isActive = true : element.isActive = false;
                                                element.isActive = true;
                                              });
                                            });
                                          },
                                        );                                                                        
                                        month.isSelected = true;
                                        BlocProvider.of<MonthlyResultBloc>(context).add(ResultMonthlyEvent(
                                            classId: data.currentClass!.classId.toString(),
                                            month: month.month.toString(),
                                            studentId: studentId,
                                            term: element.semester.toString()));
                                        Future.delayed(const Duration(seconds:1),
                                          () {
                                            setState(() {
                                              isShowMonthlyResult = true;
                                            });
                                          },
                                        );  
                                      }
                                      index = int.parse(month.month.toString());
                                    }
                                    _listMonth.add(ListMonthModel(
                                        element.name.toString() == "លទ្ធផលប្រចាំឆ្នាំ"
                                            ? "YEAR_RESULT".tr()
                                            : translate == "km"
                                                ? element.name.toString()
                                                :element.semester == "FIRST_SEMESTER"?"SEMESTER1":"SEMESTER2",
                                        element.semester == "FIRST_SEMESTER"
                                            ? "S1"
                                            : element.semester == "SECOND_SEMESTER"
                                                ? "S2"
                                                : "Y",
                                        element.semester == "FIRST_SEMESTER"?"SEMESTER1":"SEMESTER2",
                                        "",
                                        element.semester.toString(),
                                        index,
                                        isSelected: false,
                                        isMonth: false,
                                        isActive: true));
                                  }
                                }
                                }
                              }
                            }
                          }
                          return data == null
                              ? Container(
                                child: Container(
                                    color: Colorconstands.neutralWhite,
                                    child: Column(
                                      children: [
                                        Expanded(child: Container()),
                                        DataEmptyWidget(
                                              description: "CHILD_NOT_CLASS_DES".tr(),
                                                      title: "CHILD_NOT_CLASS".tr(),
                                        ),
                                        Expanded(child: Container()),
                                      ],
                                    )))
                              : Container(
                                child: Column(
                                    children: [
                                      Expanded(
                                        child:Container(
                                          decoration:const BoxDecoration(
                                            color: Colorconstands.neutralWhite,
                                            borderRadius: BorderRadius.only(
                                                            topLeft: Radius.circular(12),
                                                            topRight: Radius.circular(12)),
                                          ),
                                              
                                              child: Column(
                                                  children: [
                                                    GestureDetector(
                                                      onTap: () {
                                                        setState(() {
                                                          selected = !selected;
                                                        });
                                                      },
                                                      child: Container(
                                                        height: 50,
                                                        decoration: BoxDecoration(
                                                          borderRadius:const BorderRadius.only(
                                                            topLeft: Radius.circular(12),
                                                            topRight: Radius.circular(12)),
                                                            boxShadow: <BoxShadow>[
                                                              BoxShadow(
                                                                color: Colorconstands.neutralGrey.withOpacity(0.3),
                                                                blurRadius: 1,
                                                                offset:const Offset(0, 0),
                                                              ),
                                                            ],
                                                        ),
                                                        child: Row(
                                                          crossAxisAlignment:CrossAxisAlignment.center,
                                                          mainAxisAlignment:MainAxisAlignment.center,
                                                          children: [
                                                            Text(
                                                              monthName,
                                                              style: const TextStyle(
                                                                  fontSize: 16,
                                                                  color: Colorconstands
                                                                      .primaryColor,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w600),
                                                            ),
                                                            const SizedBox(
                                                              width: 10,
                                                            ),
                                                            const Icon(
                                                              Icons
                                                                  .keyboard_arrow_down,
                                                              color: Colorconstands
                                                                  .primaryColor,
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: Stack(
                                                        children: [
                                                          isMonth!
                                                              ?BlocBuilder<MonthlyResultBloc,MonthlyResultState>(
                                                                  builder: (context,state) {
                                                                    if (state is MonthlyResultLoadingState) {
                                                                      return Container(
                                                                          color: Colorconstands.neutralWhite,
                                                                          child:_buildLoading());
                                                                    } else if (state is MonthlyResultLoadedState) {
                                                                      var dataResult = state.currentMonthlyResult!.data;
                                                                     // print( " teacher : ${dataResult!.subject![0].teacherComment}");
                                                                      return dataResult!.resultMonthly == null
                                                                          ? Container(
                                                                              height: MediaQuery.of(context).size.height,
                                                                              width: MediaQuery.of(context).size.width,
                                                                              color: Colorconstands.white,
                                                                              child: Column(
                                                                                children: [
                                                                                  Expanded(child: Container()),
                                                                                  DataEmptyWidget(
                                                                                      title: "WAIT_SCORE_ENTRY".tr(),
                                                                                          description: "WAIT_SCORE_ENTRY_DES".tr(),),
                                                                                  Expanded(child: Container()),
                                                                                ],
                                                                              ))
                                                                          : Container(
                                                                              decoration:
                                                                                  const BoxDecoration(
                                                                                color:
                                                                                    Colorconstands.white,
                                                                              ),
                                                                              child:
                                                                                  SingleChildScrollView(
                                                                                child:
                                                                                    Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                                  children: [
                                                                                    Padding(
                                                                                      padding: const EdgeInsets.only(top: 10, left: 15, right: 15),
                                                                                      child: Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                        children: [
                                                                                          Column(
                                                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                                                            children: [
                                                                                              Text(
                                                                                                "MONTHLYAVERAGE".tr() + monthName,
                                                                                                style: ThemeConstands.headline4_Regular_18.copyWith(color: Colors.grey.shade500),
                                                                                              ),
                                                                                              const SizedBox(
                                                                                                height: 10,
                                                                                              ),
                                                                                              Row(
                                                                                                children: [
                                                                                                  Text(
                                                                                                    //"",
                                                                                                    dataResult.resultMonthly!.avgScore.toString() == "null" || dataResult.resultMonthly!.avgScore.toString() == "" ? "- - -" : dataResult.resultMonthly!.avgScore.toString(),
                                                                                                    style: ThemeConstands.headline1_SemiBold_32,
                                                                                                  ),
                                                                                                  Text(
                                                                                                    dataResult.resultMonthly!.maxAvgScore.toString() == "null" || dataResult.resultMonthly!.maxAvgScore.toString() == "" ? "- - -" : "/${dataResult.resultMonthly!.maxAvgScore}",
                                                                                                    style: ThemeConstands.headline3_SemiBold_20.copyWith(height: 1.4),
                                                                                                  ),
                                                                                                  Container(
                                                                                                    margin: const EdgeInsets.only(left: 10),
                                                                                                    padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                                                                                                    decoration: BoxDecoration(
                                                                                                        color: dataResult.resultMonthly!.performancePercentage! > 0
                                                                                                            ? Colorconstands.controlFadeGreen
                                                                                                            : dataResult.resultMonthly!.performancePercentage! < 0
                                                                                                                ? Colorconstands.alertsRedShades
                                                                                                                : Colorconstands.gray300,
                                                                                                        borderRadius: const BorderRadius.all(Radius.circular(15))),
                                                                                                    child: Row(
                                                                                                      children: [
                                                                                                        Icon(
                                                                                                          dataResult.resultMonthly!.performancePercentage! > 0
                                                                                                              ? Icons.arrow_upward
                                                                                                              : dataResult.resultMonthly!.performancePercentage! < 0
                                                                                                                  ? Icons.arrow_downward
                                                                                                                  : null,
                                                                                                          color: dataResult.resultMonthly!.performancePercentage! > 0
                                                                                                              ? Colorconstands.controlGreen
                                                                                                              : dataResult.resultMonthly!.performancePercentage! < 0
                                                                                                                  ? Colorconstands.controlRed
                                                                                                                  : Colorconstands.gray,
                                                                                                          size: 14,
                                                                                                        ),
                                                                                                        const SizedBox(width: 3),
                                                                                                        Text(
                                                                                                          dataResult.resultMonthly!.performancePercentage.toString() == "null" || dataResult.resultMonthly!.performancePercentage.toString() == "" ? "- - -" : dataResult.resultMonthly!.performancePercentage.toString() + "%",
                                                                                                          style: ThemeConstands.headline5_SemiBold_16.copyWith(
                                                                                                              color: dataResult.resultMonthly!.performancePercentage! > 0
                                                                                                                  ? Colorconstands.controlGreen
                                                                                                                  : dataResult.resultMonthly!.performancePercentage! < 0
                                                                                                                      ? Colorconstands.controlRed
                                                                                                                      : Colorconstands.gray),
                                                                                                        ),
                                                                                                      ],
                                                                                                    ),
                                                                                                  ),
                                                                                                ],
                                                                                              )
                                                                                            ],
                                                                                          ),
                                                                                          //const Icon(Icons.share_outlined)
                                                                                        ],
                                                                                      ),
                                                                                    ),
                                                                                    Padding(
                                                                                      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 12),
                                                                                      child: Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        children: [
                                                                                          Expanded(
                                                                                            child: Column(
                                                                                              children: [
                                                                                                HeaderItemWidget(
                                                                                                  title: "GRADE".tr(),
                                                                                                  value: dataResult.resultMonthly!.grading.toString() == "null" || dataResult.resultMonthly!.grading.toString() == ""
                                                                                                      ? "- - -"
                                                                                                      : translate == "km"
                                                                                                          ? dataResult.resultMonthly!.grading.toString()
                                                                                                          : dataResult.resultMonthly!.gradingEn.toString(),
                                                                                                  color: Colorconstands.controlGreen,
                                                                                                  image: ImageAssets.award,
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 13,
                                                                                                ),
                                                                                                HeaderItemWidget(
                                                                                                  title: "TOTAL_SCORE".tr(),
                                                                                                  value: dataResult.resultMonthly!.totalScore.toString() == "null" || dataResult.resultMonthly!.totalScore.toString() == "" ? "- - -" : dataResult.resultMonthly!.totalScore.toString(),
                                                                                                  color: Colorconstands.black,
                                                                                                  image: ImageAssets.addItem,
                                                                                                )
                                                                                              ],
                                                                                            ),
                                                                                          ),
                                                                                          const SizedBox(width: 33),
                                                                                          Expanded(
                                                                                            child: Column(
                                                                                              children: [
                                                                                                HeaderItemWidget(
                                                                                                  title: "RANK".tr(),
                                                                                                  value: dataResult.resultMonthly!.rank.toString() == "null" || dataResult.resultMonthly!.rank.toString() == "" ? "- - -" : '${dataResult.resultMonthly!.rank}/${dataResult.totalStudent}',
                                                                                                  color: Colorconstands.primaryColor,
                                                                                                  image: ImageAssets.chart,
                                                                                                ),
                                                                                                const SizedBox(
                                                                                                  height: 13,
                                                                                                ),
                                                                                                HeaderItemWidget(
                                                                                                  title: "TOTALABSENT".tr(),
                                                                                                  value: dataResult.resultMonthly!.absenceTotal.toString() == "null" || dataResult.resultMonthly!.absenceTotal.toString() == "" ? "- - -" : dataResult.resultMonthly!.absenceTotal.toString(),
                                                                                                  color: Colorconstands.black,
                                                                                                  image: ImageAssets.absent,
                                                                                                )
                                                                                              ],
                                                                                            ),
                                                                                          )
                                                                                        ],
                                                                                      ),
                                                                                    ),
                                                                                    Stack(
                                                                                      children: [
                                                                                        Stack(
                                                                                          children: [
                                                                                            SizedBox(
                                                                                                width: MediaQuery.of(context).size.width,
                                                                                                child: Padding(
                                                                                                  padding: const EdgeInsets.only(top: 70),
                                                                                                  child: Column(
                                                                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                      children: List.generate(
                                                                                                        dataResult.subject!.length,
                                                                                                        (index) {
                                                                                                          return Container(
                                                                                                            margin: const EdgeInsets.symmetric(vertical: 5),
                                                                                                            child: Column(
                                                                                                              children: [
                                                                                                                Row(
                                                                                                                  children: [
                                                                                                                    Container(
                                                                                                                      width: 50,
                                                                                                                      child: Text(
                                                                                                                        "${index + 1}",
                                                                                                                        style: ThemeConstands.headline6_Medium_14,textAlign: TextAlign.center,
                                                                                                                      ),
                                                                                                                    ),
                                                                                                                    Expanded(
                                                                                                                      child: SizedBox(
                                                                                                                        child: Text(
                                                                                                                          translate == "km" ? dataResult.subject![index].name.toString() : dataResult.subject![index].nameEn.toString(),
                                                                                                                          textAlign: TextAlign.left,
                                                                                                                          style: ThemeConstands.headline6_Medium_14,
                                                                                                                        ),
                                                                                                                      ),
                                                                                                                    ),
                                                                                                                    Container(
                                                                                                                      width: 65,
                                                                                                                      child: Center(
                                                                                                                        child: Text(
                                                                                                                          dataResult.subject![index].score.toString() == "null" || dataResult.subject![index].score.toString() == "" ? "- - -" : "${dataResult.subject![index].score}/${dataResult.subject![index].maxScore}",
                                                                                                                          style: ThemeConstands.headline6_Medium_14,textAlign: TextAlign.center,
                                                                                                                        ),
                                                                                                                      ),
                                                                                                                    ),
                                                                                                                    Container(
                                                                                                                      width: 85,
                                                                                                                      margin: const EdgeInsets.only(left: 18, right: 18),
                                                                                                                      child: Center(
                                                                                                                        child: Text(dataResult.subject![index].letterGrade.toString() == "null" || dataResult.subject![index].letterGrade.toString() == "" ? "- - -" : "${translate == "km" ? dataResult.subject![index].grading : dataResult.subject![index].gradingEn}(${dataResult.subject![index].letterGrade})",
                                                                                                                            style: ThemeConstands.headline6_Medium_14.copyWith(
                                                                                                                                color: dataResult.subject![index].letterGrade.toString() == "F"
                                                                                                                                    ? Colorconstands.alertsNotifications
                                                                                                                                    : dataResult.subject![index].letterGrade.toString() == "A"
                                                                                                                                        ? Colorconstands.alertsPositive
                                                                                                                                        : dataResult.subject![index].letterGrade.toString() == "B"
                                                                                                                                            ? Colorconstands.mainColorSecondary
                                                                                                                                            : Colorconstands.alertsAwaitingText)),
                                                                                                                      ),
                                                                                                                    ),
                                                                                                                    Container(
                                                                                                                      width: 75,
                                                                                                                      margin:const EdgeInsets.only(right: 12),
                                                                                                                      child: dataResult.subject![index].teacherComment != null
                                                                                                                          ? Center(
                                                                                                                              child: Row(
                                                                                                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                                                children: [
                                                                                                                                  Expanded(
                                                                                                                                    child: Text(dataResult.subject![index].rank.toString() == "null" || dataResult.subject![index].rank.toString() == "" ? "- - -" : "${dataResult.subject![index].rank}/${dataResult.totalStudent}",
                                                                                                                                        style: ThemeConstands.headline6_Medium_14.copyWith(
                                                                                                                                            color: dataResult.subject![index].letterGrade.toString() == "F"
                                                                                                                                                ? Colorconstands.alertsNotifications
                                                                                                                                                : dataResult.subject![index].letterGrade.toString() == "A"
                                                                                                                                                    ? Colorconstands.alertsPositive
                                                                                                                                                    : dataResult.subject![index].letterGrade.toString() == "B"
                                                                                                                                                        ? Colorconstands.mainColorSecondary
                                                                                                                                                        : Colorconstands.alertsAwaitingText)),
                                                                                                                                  ),
                                                                                                                                  dataResult.subject![index].teacherComment == null
                                                                                                                                      ? Container()
                                                                                                                                      : Padding(
                                                                                                                                          padding: const EdgeInsets.only(left: 10),
                                                                                                                                          child: GestureDetector(
                                                                                                                                              onTap: () {
                                                                                                                                                setState(() {
                                                                                                                                                  for (var element in dataResult.subject!) {
                                                                                                                                                    element.isSelected = false;
                                                                                                                                                  }
                                                                                                                                                  resultMonthlyIndex = index;
                                                                                                                                                  dataResult.subject![index].isSelected=!dataResult.subject![index].isSelected;                                                                                                                                                  
                                                                                                                                                });
                                                                                                                                                                                                                                           },
                                                                                                                                              child: SvgPicture.asset(ImageAssets.comment)),
                                                                                                                                        )
                                                                                                                                ],
                                                                                                                              ),
                                                                                                                            )
                                                                                                                          : Center(
                                                                                                                              child: Text(dataResult.subject![index].rank.toString() == "null" || dataResult.subject![index].rank.toString() == "" ? "- - -" : "${dataResult.subject![index].rank}/${dataResult.totalStudent}",
                                                                                                                                  style: ThemeConstands.headline6_Medium_14.copyWith(
                                                                                                                                      color: dataResult.subject![index].letterGrade.toString() == "F"
                                                                                                                                          ? Colorconstands.alertsNotifications
                                                                                                                                          : dataResult.subject![index].letterGrade.toString() == "A"
                                                                                                                                              ? Colorconstands.alertsPositive
                                                                                                                                              : dataResult.subject![index].letterGrade.toString() == "B"
                                                                                                                                                  ? Colorconstands.mainColorSecondary
                                                                                                                                                  : Colorconstands.alertsAwaitingText)),
                                                                                                                            ),
                                                                                                                    )
                                                                                                                  ],
                                                                                                                ),
                                                                                                                 dataResult.subject![index].teacherComment != null?
                                                                                                                 AnimatedSize(
                                                                                                                  curve: Curves.easeInOutCirc,
                                                                                                                  duration: const Duration(milliseconds: 300),
                                                                                                                  child: Container(
                                                                                                                    padding: const EdgeInsets.symmetric(horizontal: 20),
                                                                                                                    height: index==resultMonthlyIndex&&dataResult.subject![index].isSelected?50:0,
                                                                                                                    child: Align(
                                                                                                                      alignment: Alignment.centerLeft,
                                                                                                                      child: Text("TEACHER_COMMENTARY".tr()+" :"+ dataResult.subject![index].teacherComment!,
                                                                                                                      style: ThemeConstands.caption_Regular_12 ,
                                                                                                                      textAlign: TextAlign.center,),
                                                                                                                    ),
                                                                                                                    
                                                                                                                  ),
                                                                                                                ):Container(),
                                                                                                                Divider()
                                                                                                              ],
                                                                                                            ),
                                                                                                          );
                                                                                                        },
                                                                                                      )),
                                                                                                )
                                                                                                //   DataTable(
                                                                                                //   columnSpacing: 0,
                                                                                                //   horizontalMargin: 0,
                                                                                                //   headingRowColor: MaterialStateColor.resolveWith((states) => Colorconstands.neutralBtnBg),
                                                                                                //   columns: headerlabel.map((e) {
                                                                                                //     return DataColumn(
                                                                                                //         label: Text(
                                                                                                //       e,
                                                                                                //       style: ThemeConstands.headline6_SemiBold_14,
                                                                                                //     ));
                                                                                                //   }).toList(),
                                                                                                //   rows:
                                                                                                //List.generate(dataResult.subject!.length, (index) {
                                                                                                //     return DataRow(

                                                                                                //       cells: [
                                                                                                //       DataCell(Container(
                                                                                                //         margin: const EdgeInsets.only(left: 25),
                                                                                                //         width: 25,
                                                                                                //         child: Text(
                                                                                                //           "${index + 1}",
                                                                                                //           style: ThemeConstands.caption_Regular_12,
                                                                                                //         ),
                                                                                                //       )),
                                                                                                //       DataCell(
                                                                                                // SizedBox(
                                                                                                //         width: 98,
                                                                                                //         child: Text(
                                                                                                //           translate=="km"?dataResult.subject![index].name.toString():
                                                                                                //           dataResult.subject![index].nameEn.toString(),
                                                                                                //           textAlign: TextAlign.left,
                                                                                                //           style: ThemeConstands.headline6_Medium_14,
                                                                                                //         ),
                                                                                                //       )
                                                                                                // ),
                                                                                                //       DataCell(
                                                                                                // SizedBox(
                                                                                                //         width: 58,
                                                                                                //         child: Center(
                                                                                                //           child: Text(
                                                                                                //             dataResult.subject![index].score.toString() == "null" || dataResult.subject![index].score.toString() == "" ? "- - -" : "${dataResult.subject![index].score}/${dataResult.subject![index].maxScore}",
                                                                                                //             style: ThemeConstands.headline6_SemiBold_14,
                                                                                                //           ),
                                                                                                //         ),
                                                                                                //       )
                                                                                                // ),
                                                                                                //       DataCell(
                                                                                                // SizedBox(
                                                                                                //         width: 88,
                                                                                                //         child:
                                                                                                // Center(
                                                                                                //           child: Text(dataResult.subject![index].letterGrade.toString() == "null" || dataResult.subject![index].letterGrade.toString() == "" ? "- - -" : "${translate=="km"? dataResult.subject![index].grading:dataResult.subject![index].gradingEn}(${dataResult.subject![index].letterGrade})",
                                                                                                //               style: ThemeConstands.caption_Regular_12.copyWith(
                                                                                                //                   color: dataResult.subject![index].letterGrade.toString() == "F"
                                                                                                //                       ? Colorconstands.alertsNotifications
                                                                                                //                       : dataResult.subject![index].letterGrade.toString() == "A"
                                                                                                //                           ? Colorconstands.alertsPositive
                                                                                                //                           : dataResult.subject![index].letterGrade.toString() == "B"
                                                                                                //                               ? Colorconstands.mainColorSecondary
                                                                                                //                               : Colorconstands.alertsAwaitingText)),
                                                                                                //         ),
                                                                                                //       )
                                                                                                // ),
                                                                                                //       DataCell(
                                                                                                // Container(
                                                                                                //         padding: const EdgeInsets.only(left: 5, right: 25),
                                                                                                //         width: 90,
                                                                                                //         child: dataResult.subject![index].teacherComment == null
                                                                                                //             ? Center(
                                                                                                //                 child: Row(
                                                                                                //                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                //                   children: [
                                                                                                //                     Expanded(
                                                                                                //                       child: Text(dataResult.subject![index].rank.toString() == "null" || dataResult.subject![index].rank.toString() == "" ? "- - -" : "${dataResult.subject![index].rank}/${dataResult.totalStudent}",
                                                                                                //                           style: ThemeConstands.caption_Regular_12.copyWith(
                                                                                                //                               color: dataResult.subject![index].letterGrade.toString() == "F"
                                                                                                //                                   ? Colorconstands.alertsNotifications
                                                                                                //                                   : dataResult.subject![index].letterGrade.toString() == "A"
                                                                                                //                                       ? Colorconstands.alertsPositive
                                                                                                //                                       : dataResult.subject![index].letterGrade.toString() == "B"
                                                                                                //                                           ? Colorconstands.mainColorSecondary
                                                                                                //                                           : Colorconstands.alertsAwaitingText)),
                                                                                                //                     ),
                                                                                                //                     dataResult.subject![index].teacherComment != null

                                                                                                //                         ? Container()
                                                                                                //                         : Padding(
                                                                                                //                             padding: const EdgeInsets.only(left: 10),
                                                                                                //                             child: SvgPicture.asset(ImageAssets.comment),
                                                                                                //                           )
                                                                                                //                   ],
                                                                                                //                 ),
                                                                                                //               )
                                                                                                //             : Center(
                                                                                                //                 child: Text(dataResult.subject![index].rank.toString() == "null" || dataResult.subject![index].rank.toString() == "" ? "- - -" : "${dataResult.subject![index].rank}/${dataResult.totalStudent}",
                                                                                                //                     style: ThemeConstands.caption_Regular_12.copyWith(
                                                                                                //                         color: dataResult.subject![index].letterGrade.toString() == "F"
                                                                                                //                             ? Colorconstands.alertsNotifications
                                                                                                //                             : dataResult.subject![index].letterGrade.toString() == "A"
                                                                                                //                                 ? Colorconstands.alertsPositive
                                                                                                //                                 : dataResult.subject![index].letterGrade.toString() == "B"
                                                                                                //                                     ? Colorconstands.mainColorSecondary
                                                                                                //                                     : Colorconstands.alertsAwaitingText)),
                                                                                                //               ),
                                                                                                //       )),
                                                                                                //     ]);
                                                                                                //   }),
                                                                                                // ),

                                                                                                ),
                                                                                            paid ? Container() : LockScreenUnpaidWidget(title: "YOU_CAN_NOT_ACCESS".tr(), subtitle: "PLEASE_PAY_THE_BILL".tr())
                                                                                          ],
                                                                                        ),
                                                                                        Positioned(
                                                                                            top: 0,
                                                                                            child: Container(
                                                                                              height: 60,
                                                                                              width: MediaQuery.of(context).size.width,
                                                                                              decoration: const BoxDecoration(color: Colorconstands.neutralBtnBg),
                                                                                              child: Row(children: [
                                                                                                Container(
                                                                                                 width: 50,
                                                                                                  child: Text(
                                                                                                    headerlabel[0],
                                                                                                    style: ThemeConstands.headline6_SemiBold_14,textAlign: TextAlign.center,
                                                                                                  ),
                                                                                                ),
                                                                                                Expanded(
                                                                                                  child: Container(
                                                                                                    child: Text(
                                                                                                      headerlabel[1],
                                                                                                      style: ThemeConstands.headline6_SemiBold_14,textAlign: TextAlign.left,
                                                                                                    ),
                                                                                                  ),
                                                                                                ),
                                                                                                Container(
                                                                                                  margin:const EdgeInsets.only(right: 12),
                                                                                                  width: 70,
                                                                                                  child: Center(
                                                                                                    child: Text(
                                                                                                      headerlabel[2],
                                                                                                      style: ThemeConstands.headline6_SemiBold_14,textAlign: TextAlign.center,
                                                                                                    ),
                                                                                                  ),
                                                                                                ),
                                                                                                Container(
                                                                                                  margin:const EdgeInsets.only(right: 18),
                                                                                                  width: 80,
                                                                                                  child: Center(
                                                                                                    child: Text(
                                                                                                      headerlabel[3],
                                                                                                      style: ThemeConstands.headline6_SemiBold_14,textAlign: TextAlign.center,
                                                                                                    ),
                                                                                                  ),
                                                                                                ),
                                                                                                Container(
                                                                                                   margin:const EdgeInsets.only(right: 12),
                                                                                                   width: 85,
                                                                                                    child: Text(
                                                                                                      headerlabel[4],
                                                                                                      style: ThemeConstands.headline6_SemiBold_14,textAlign: TextAlign.center,
                                                                                                    )),
                                                                                              ]),
                                                                                            )),
                                                                                      ],
                                                                                    ),
                                                                                    dataResult.resultMonthly!.teacherComment == null || dataResult.resultMonthly!.teacherComment == ""
                                                                                        ? Container()
                                                                                        : Column(
                                                                                            children: [
                                                                                              Container(
                                                                                                color: Colorconstands.neutralBtnBg,
                                                                                                padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                                                                                                width: MediaQuery.of(context).size.width,
                                                                                                child: Text(
                                                                                                  "TEACHER_COMMENTARY".tr(),
                                                                                                  style: ThemeConstands.headline6_Medium_14.copyWith(fontWeight: FontWeight.bold),
                                                                                                ),
                                                                                              ),
                                                                                              Padding(
                                                                                                padding: const EdgeInsets.symmetric(vertical: 20,horizontal: 20),
                                                                                                child: Align(
                                                                                                  alignment: Alignment.centerLeft,
                                                                                                  child: Text(dataResult.resultMonthly!.teacherComment!, style: ThemeConstands.headline6_Regular_14_20height)),
                                                                                              ),
                                                                                            ],
                                                                                          )
                                                                                  ],
                                                                                ),
                                                                              ));
                                                                    } else {
                                                                      return isShowMonthlyResult==true? Container(
                                                                            decoration:const BoxDecoration(
                                                                              color: Colorconstands.neutralWhite,
                                                                            ),
                                                                            child: Center(
                                                                              child: ErrorRequestData(
                                                                                onPressed: (){
                                                                                   BlocProvider.of<GetProfileUserBloc>(context).add(GetProfileUser());
                                                                                },
                                                                                discription: '', 
                                                                                hidebutton: true, 
                                                                                title: 'WE_DETECT_ERROR'.tr(),

                                                                              ),
                                                                            ),
                                                                          ):Container();
                                                                    }
                                                                  },
                                                                  ///////////////////////////////////////// End Month ////////////////////////////////
                                                                )
                                                              : !isYear
                                                                  ? BlocBuilder<TermResultBloc,TermResultState>(builder:(context,state) {
                                                                        if (state is TermResultLoadingState) {
                                                                          return Container(
                                                                              color: Colorconstands.neutralWhite,
                                                                              child: _buildLoading());
                                                                        }
                                                                        else if (state is TermResultLoadedState) {
                                                                          // Future.delayed(Duration.zero, () async {
                                                                          //   checkIsPiad(state.currentFirstTermResult!.status!);
                                                                          // });

                                                                          var dataResult = state
                                                                              .currentTermResult!
                                                                              .data;
                                                                          // print(
                                                                          //     " teacher : ${dataResult!.yearResult!.teacherComment}");
                                                                          return dataResult!.termResult ==
                                                                                  null
                                                                              ? Container(
                                                                                  height: MediaQuery.of(context).size.height,
                                                                                  width: MediaQuery.of(context).size.width,
                                                                                  color: Colorconstands.white,
                                                                                  child: Column(
                                                                                children: [
                                                                                  Expanded(child: Container()),
                                                                                  DataEmptyWidget(
                                                                                      title: "WAIT_SCORE_ENTRY".tr(),
                                                                                          description: "WAIT_SCORE_ENTRY_DES".tr(),),
                                                                                  Expanded(child: Container()),
                                                                                ],
                                                                              ))
                                                                              : Container(
                                                                                  decoration: const BoxDecoration(
                                                                                    color: Colorconstands.white,
                                                                                  ),
                                                                                  child: SingleChildScrollView(
                                                                                    child: Column(
                                                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                                                      children: [
                                                                                        Padding(
                                                                                          padding: const EdgeInsets.only(top: 10, left: 15, right: 15),
                                                                                          child: Row(
                                                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                            crossAxisAlignment: CrossAxisAlignment.center,
                                                                                            children: [
                                                                                              Column(
                                                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                                                children: [
                                                                                                  Text(
                                                                                                    "SEMESTER_TERM".tr() + monthName.trim(),
                                                                                                    style: ThemeConstands.headline4_Regular_18.copyWith(color: Colors.grey.shade500),
                                                                                                  ),
                                                                                                  const SizedBox(
                                                                                                    height: 10,
                                                                                                  ),
                                                                                                  Row(
                                                                                                    children: [
                                                                                                      Text(
                                                                                                        //"",
                                                                                                        dataResult.termExamResult!.avgScore.toString() == "null" || dataResult.termExamResult!.avgScore.toString() == "" ? "- - -" : dataResult.termExamResult!.avgScore.toString(),
                                                                                                        style: ThemeConstands.headline1_SemiBold_32,
                                                                                                      ),
                                                                                                      Text(
                                                                                                        dataResult.termExamResult!.maxAvgScore.toString() == "null" || dataResult.termExamResult!.maxAvgScore.toString() == "" ? "- - -" : "/${dataResult.termExamResult!.maxAvgScore}",
                                                                                                        style: ThemeConstands.headline3_SemiBold_20.copyWith(height: 1.4),
                                                                                                      ),
                                                                                                    ],
                                                                                                  )
                                                                                                ],
                                                                                              ),
                                                                                              //const Icon(Icons.share_outlined)
                                                                                            ],
                                                                                          ),
                                                                                        ),
                                                                                        Padding(
                                                                                          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 12),
                                                                                          child: Row(
                                                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                            children: [
                                                                                              Expanded(
                                                                                                child: Column(
                                                                                                  children: [
                                                                                                    HeaderItemWidget(
                                                                                                      title: "GRADE".tr(),
                                                                                                      value: dataResult.termExamResult!.grading.toString() == "null" || dataResult.termExamResult!.grading.toString() == ""
                                                                                                          ? "- - -"
                                                                                                          : translate == "km"
                                                                                                              ? dataResult.termExamResult!.grading.toString()
                                                                                                              : dataResult.termExamResult!.gradingEn.toString(),
                                                                                                      color: Colorconstands.controlGreen,
                                                                                                      image: ImageAssets.award,
                                                                                                    ),
                                                                                                    const SizedBox(
                                                                                                      height: 13,
                                                                                                    ),
                                                                                                    HeaderItemWidget(
                                                                                                      title: "TOTAL_SCORE".tr(),
                                                                                                      value: dataResult.termExamResult!.totalScore.toString() == "null" || dataResult.termExamResult!.totalScore.toString() == "" ? "- - -" : dataResult.termExamResult!.totalScore.toString(),
                                                                                                      color: Colorconstands.black,
                                                                                                      image: ImageAssets.addItem,
                                                                                                    )
                                                                                                  ],
                                                                                                ),
                                                                                              ),
                                                                                              const SizedBox(width: 33),
                                                                                              Expanded(
                                                                                                child: Column(
                                                                                                  children: [
                                                                                                    HeaderItemWidget(
                                                                                                      title: "RANK".tr(),
                                                                                                      value: dataResult.termExamResult!.rank.toString() == "null" || dataResult.termExamResult!.rank.toString() == "" ? "- - -" : '${dataResult.termExamResult!.rank}/${dataResult.totalStudent}',
                                                                                                      color: Colorconstands.primaryColor,
                                                                                                      image: ImageAssets.chart,
                                                                                                    ),
                                                                                                    const SizedBox(
                                                                                                      height: 13,
                                                                                                    ),
                                                                                                    HeaderItemWidget(
                                                                                                      title: "TOTALABSENT".tr(),
                                                                                                      value: dataResult.termResult!.absenceTotal.toString() == "null" || dataResult.termResult!.absenceTotal.toString() == "" ? "- - -" : dataResult.termResult!.absenceTotal.toString(),
                                                                                                      color: Colorconstands.black,
                                                                                                      image: ImageAssets.absent,
                                                                                                    )
                                                                                                  ],
                                                                                                ),
                                                                                              )
                                                                                            ],
                                                                                          ),
                                                                                        ),
                                                                                        Stack(
                                                                                          children: [
                                                                                            Column(
                                                                                              children: [
                                                                                                Stack(
                                                                                                  children: [
                                                                                                    Column(
                                                                                                      children: [
                                                                                                        SizedBox(
                                                                                                          width: MediaQuery.of(context).size.width,
                                                                                                          child: Container(
                                                                                                            margin:const EdgeInsets.only(top: 65),
                                                                                                             child: Column(
                                                                                                                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                                 crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                 children: List.generate(dataResult.termSubjects!.length, (index) {
                                                                                                                     print("Index : $index");
                                                                                                                     return Container(
                                                                                                                       margin: const EdgeInsets.symmetric(vertical: 5),
                                                                                                                       child: Column(
                                                                                                                         children: [
                                                                                                                           Row(
                                                                                                                             children: [
                                                                                                                              Container(
                                                                                                                                  margin: const EdgeInsets.only(left: 25),
                                                                                                                                  child: Text(
                                                                                                                                    "${index + 1}",
                                                                                                                                    style: ThemeConstands.headline6_Medium_14,
                                                                                                                                  ),
                                                                                                                                ),
                                                                                                                               Expanded(
                                                                                                                                 child: Container(
                                                                                                                                   padding: const EdgeInsets.only(left: 22),
                                                                                                                                  child: Text(
                                                                                                                                    translate == "km" ? dataResult.termSubjects![index].name.toString() : dataResult.termSubjects![index].nameEn.toString(),
                                                                                                                                    textAlign: TextAlign.left,
                                                                                                                                    style: ThemeConstands.headline6_Medium_14,
                                                                                                                                  ),
                                                                                                                                )
                                                                                                                               ),
                                                                                                                               Container(
                                                                                                                                margin: const EdgeInsets.only(right: 22),
                                                                                                                                  width: 75,
                                                                                                                                  child: Center(
                                                                                                                                    child: Text(
                                                                                                                                      dataResult.termSubjects![index].score.toString() == "null" || dataResult.termSubjects![index].score.toString() == "" ? "- - -" : "${dataResult.termSubjects![index].score}/${dataResult.termSubjects![index].maxScore}",
                                                                                                                                      style: ThemeConstands.headline6_Medium_14,
                                                                                                                                    ),
                                                                                                                                  ),
                                                                                                                                ),
                                                                                                                                Container(
                                                                                                                                   margin: const EdgeInsets.only(right: 12),
                                                                                                                                  width: 85,
                                                                                                                                  child: Center(
                                                                                                                                    child: Text(dataResult.termSubjects![index].letterGrade.toString() == "null" || dataResult.termSubjects![index].letterGrade.toString() == "" ? "- - -" : "${translate == "km" ? dataResult.termSubjects![index].grading : dataResult.termSubjects![index].gradingEn}(${dataResult.termSubjects![index].letterGrade})",
                                                                                                                                        style: ThemeConstands.headline6_Medium_14.copyWith(
                                                                                                                                            color: dataResult.termSubjects![index].letterGrade.toString() == "F"
                                                                                                                                                ? Colorconstands.alertsNotifications
                                                                                                                                                : dataResult.termSubjects![index].letterGrade.toString() == "A"
                                                                                                                                                    ? Colorconstands.alertsPositive
                                                                                                                                                    : dataResult.termSubjects![index].letterGrade.toString() == "B"
                                                                                                                                                        ? Colorconstands.mainColorSecondary
                                                                                                                                                        : Colorconstands.alertsAwaitingText)),
                                                                                                                                  ),
                                                                                                                                ),
                                                                                                                                Container(
                                                                                                                                  width: 80,
                                                                                                                                   margin: const EdgeInsets.only(right: 12),
                                                                                                                                  child: dataResult.termSubjects![index].teacherComment != null
                                                                                                                                      ? Center(
                                                                                                                                          child: Row(
                                                                                                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                                                            children: [
                                                                                                                                              Expanded(
                                                                                                                                                child: Text(dataResult.termSubjects![index].rank.toString() == "null" || dataResult.termSubjects![index].rank.toString() == "" ? "- - -" : "${dataResult.termSubjects![index].rank}/${dataResult.totalStudent}",
                                                                                                                                                    style: ThemeConstands.headline6_Medium_14.copyWith(
                                                                                                                                                        color: dataResult.termSubjects![index].letterGrade.toString() == "F"
                                                                                                                                                            ? Colorconstands.alertsNotifications
                                                                                                                                                            : dataResult.termSubjects![index].letterGrade.toString() == "A"
                                                                                                                                                                ? Colorconstands.alertsPositive
                                                                                                                                                                : dataResult.termSubjects![index].letterGrade.toString() == "B"
                                                                                                                                                                    ? Colorconstands.mainColorSecondary
                                                                                                                                                                    : Colorconstands.alertsAwaitingText)),
                                                                                                                                              ),
                                                                                                                                              dataResult.termSubjects![index].teacherComment == null
                                                                                                                                                  ? Container()
                                                                                                                                                  : Padding(
                                                                                                                                                      padding: const EdgeInsets.only(left: 10),
                                                                                                                                                      child: SvgPicture.asset(ImageAssets.comment),
                                                                                                                                                    )
                                                                                                                                            ],
                                                                                                                                          ),
                                                                                                                                        )
                                                                                                                                      : Center(
                                                                                                                                          child: Text(dataResult.termSubjects![index].rank.toString() == "null" || dataResult.termSubjects![index].rank.toString() == "" ? "- - -" : "${dataResult.termSubjects![index].rank}/${dataResult.totalStudent}",
                                                                                                                                              style: ThemeConstands.headline6_Medium_14.copyWith(
                                                                                                                                                  color: dataResult.termSubjects![index].letterGrade.toString() == "F"
                                                                                                                                                      ? Colorconstands.alertsNotifications
                                                                                                                                                      : dataResult.termSubjects![index].letterGrade.toString() == "A"
                                                                                                                                                          ? Colorconstands.alertsPositive
                                                                                                                                                          : dataResult.termSubjects![index].letterGrade.toString() == "B"
                                                                                                                                                              ? Colorconstands.mainColorSecondary
                                                                                                                                                              : Colorconstands.alertsAwaitingText)),
                                                                                                                                        ),
                                                                                                                                )
                                                                                                                                
                                                                                                                             ],
                                                                                                                           ),
                                                                                                                          Divider()
                                                                                                                         ],
                                                                                                                       ),
                                                                                                                     );
                                                                                                                   },
                                                                                                                 )
                                                                                                              ),
                                                                                                           )

                                                                                                        ),
                                                                                                        dataResult.termResult == null
                                                                                                            ? Container()
                                                                                                            : Column(
                                                                                                                children: [
                                                                                                                  Container(
                                                                                                                    color: Colorconstands.neutralBtnBg,
                                                                                                                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                                                                                                                    width: MediaQuery.of(context).size.width,
                                                                                                                    child: Text(
                                                                                                                      "SMESTER_EXAM_RESULTS".tr(),
                                                                                                                      style: ThemeConstands.headline6_Medium_14.copyWith(fontWeight: FontWeight.bold),
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                  Padding(
                                                                                                                    padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                                                                                                                    child: Column(
                                                                                                                      children: [
                                                                                                                        SubWidget(
                                                                                                                          title: "AVERAGE_SCORE_MONTHLY_EXAM".tr(),
                                                                                                                          subTitle: dataResult.termResult!.avgScore.toString().isEmpty ? "- - -" : dataResult.termResult!.avgScore.toString(),
                                                                                                                        ),
                                                                                                                        const Divider(),
                                                                                                                        SubWidget(
                                                                                                                          title: "SEMI_RANKING".tr(),
                                                                                                                          subTitle: dataResult.termResult!.rank.toString().isEmpty ? "- - -" : "${dataResult.termResult!.rank}/${dataResult.totalStudent}",
                                                                                                                        ),
                                                                                                                        const Divider(),
                                                                                                                        SubWidget(
                                                                                                                          isColor: true,
                                                                                                                          title: "GRADE".tr(),
                                                                                                                          subTitle: dataResult.termResult!.grading.toString().isEmpty ? "- - -" : "${dataResult.termResult!.grading}(${dataResult.termResult!.letterGrade.toString()})",
                                                                                                                        ),
                                                                                                                      ],
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                  Container(
                                                                                                                    color: Colorconstands.neutralBtnBg,
                                                                                                                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                                                                                                                    width: MediaQuery.of(context).size.width,
                                                                                                                    child: Row(
                                                                                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                                      children: [
                                                                                                                        SizedBox(
                                                                                                                          width: MediaQuery.of(context).size.width / 4,
                                                                                                                          child: Text(
                                                                                                                            "TOTALABSENT".tr(),
                                                                                                                            style: ThemeConstands.headline6_Medium_14.copyWith(fontWeight: FontWeight.bold),
                                                                                                                          ),
                                                                                                                        ),
                                                                                                                        AbsentWidget(
                                                                                                                          value: "PERMISSION".tr(),
                                                                                                                        ),
                                                                                                                        AbsentWidget(
                                                                                                                          value: "LATE".tr(),
                                                                                                                        ),
                                                                                                                        AbsentWidget(
                                                                                                                          value: "ABSENT".tr(),
                                                                                                                        ),
                                                                                                                      ],
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                  Padding(
                                                                                                                    padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                                                                                                                    child: Row(
                                                                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                                      children: [
                                                                                                                        SizedBox(
                                                                                                                          width: MediaQuery.of(context).size.width / 4,
                                                                                                                          child: Text(
                                                                                                                            "NUMBER_OF_ABSENT".tr(),
                                                                                                                            style: ThemeConstands.headline6_Medium_14.copyWith(fontWeight: FontWeight.bold),
                                                                                                                          ),
                                                                                                                        ),
                                                                                                                        AbsentWidget(
                                                                                                                          isBool: true,
                                                                                                                          value: dataResult.termResult!.absenceWithPermission!.toString().isEmpty ? "- - -" : dataResult.termResult!.absenceWithPermission!.toString(),
                                                                                                                        ),
                                                                                                                        AbsentWidget(
                                                                                                                          isBool: true,
                                                                                                                          value: dataResult.termResult!.absenceWithLate!.toString().isEmpty ? "- - -" : dataResult.termResult!.absenceWithLate!.toString(),
                                                                                                                        ),
                                                                                                                        AbsentWidget(
                                                                                                                          isBool: true,
                                                                                                                          value: dataResult.termResult!.absenceWithoutPermission!.toString().isEmpty ? "- - -" : dataResult.termResult!.absenceWithoutPermission!.toString(),
                                                                                                                        ),
                                                                                                                      ],
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                  Container(
                                                                                                                    color: Colorconstands.neutralBtnBg,
                                                                                                                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                                                                                                                    width: MediaQuery.of(context).size.width,
                                                                                                                    child: Text(
                                                                                                                      "TEACHER_COMMENTARY".tr(),
                                                                                                                      style: ThemeConstands.headline6_Medium_14.copyWith(fontWeight: FontWeight.bold),
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                  Padding(
                                                                                                                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                                                                                                                    child: Text(
                                                                                                                      dataResult.termResult!.teacherComment.toString().isEmpty ? "- - -" : dataResult.termResult!.teacherComment.toString(),
                                                                                                                      textAlign: TextAlign.left,
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                  const SizedBox(
                                                                                                                    height: 20,
                                                                                                                  )
                                                                                                                ],
                                                                                                              ),
                                                                                                      ],
                                                                                                    ),
                                                                                                    paid ? Container() : LockScreenUnpaidWidget(title: "YOU_CAN_NOT_ACCESS".tr(), subtitle: "PLEASE_PAY_THE_BILL".tr())
                                                                                                  ],
                                                                                                ),
                                                                                              ],
                                                                                            ),
                                                                                            Positioned(
                                                                                                top: 0,
                                                                                                child: Container(
                                                                                                  padding: const EdgeInsets.only(left: 19, right: 10),
                                                                                                  height: 60,
                                                                                                  width: MediaQuery.of(context).size.width,
                                                                                                  decoration: const BoxDecoration(color: Colorconstands.neutralBtnBg),
                                                                                                  child: Row(children: [
                                                                                                    Container(
                                                                                                      margin: const EdgeInsets.only(right: 12),
                                                                                                      width: 25,
                                                                                                      child: Text(
                                                                                                        headerlabel[0],
                                                                                                        style: ThemeConstands.headline6_SemiBold_14,
                                                                                                      ),
                                                                                                    ),
                                                                                                    Expanded(
                                                                                                      child: Container(
                                                                                                        child: Align(
                                                                                                          alignment: Alignment.centerLeft,
                                                                                                          child: Text(
                                                                                                            headerlabel[1],
                                                                                                            style: ThemeConstands.headline6_SemiBold_14,
                                                                                                          ),
                                                                                                        ),
                                                                                                      ),
                                                                                                    ),
                                                                                                    Container(
                                                                                                      margin:const EdgeInsets.only(right: 16),
                                                                                                      width: 75,
                                                                                                      child: Center(
                                                                                                        child: Text(
                                                                                                          headerlabel[2],
                                                                                                          style: ThemeConstands.headline6_SemiBold_14,
                                                                                                        ),
                                                                                                      ),
                                                                                                    ),
                                                                                                    Container(
                                                                                                      margin:const EdgeInsets.only(right: 21),
                                                                                                      width: 75,
                                                                                                      child: Center(
                                                                                                        child: Text(
                                                                                                          headerlabel[3],
                                                                                                          style: ThemeConstands.headline6_SemiBold_14,
                                                                                                        ),
                                                                                                      ),
                                                                                                    ),
                                                                                                    Container(
                                                                                                      margin:const EdgeInsets.only(right: 5),
                                                                                                       width: 80,
                                                                                                      child: Text(
                                                                                                        headerlabel[4],
                                                                                                        style: ThemeConstands.headline6_SemiBold_14,
                                                                                                      ),
                                                                                                    ),
                                                                                                  ]),
                                                                                                )
                                                                                              ),
                                                                                          ],
                                                                                        ),
                                                                                      ],
                                                                                    ),
                                                                                  ));
                                                                        } else {
                                                                          return Container(
                                                                            decoration:const BoxDecoration(
                                                                              color: Colorconstands.neutralWhite,
                                                                            ),
                                                                            child: Center(
                                                                              child: ErrorRequestData(
                                                                                onPressed: (){
                                                                                  BlocProvider.of<TermResultBloc>(context)
                                                                                  .add(ResultTermEvent(classId:classId,studentId:studentId,term:semesterID));
                                                                                },
                                                                                discription: '', 
                                                                                hidebutton: true, 
                                                                                title: 'WE_DETECT_ERROR'.tr(),

                                                                              ),
                                                                            ),
                                                                          );
                                                                        }
                                                                      },
                                                                    )
                                                                  : BlocBuilder<YearResultBloc,YearResultState>(
                                                                      builder:(context,state) {
                                                                        if (state is YearResultLoadingState) {
                                                                          return Container(
                                                                              color: Colorconstands.neutralWhite,
                                                                              child:_buildLoading());
                                                                        }
                                                                        else if (state is YearResultLoadedState) {
                                                                          // Future.delayed(Duration.zero, () async {
                                                                          //   checkIsPiad(state.currentFirstTermResult!.status!);
                                                                          // });
                                                                          var dataResult = state.currentResult!.data;
                                                                          return dataResult!.yearResult == null
                                                                              ? Container(
                                                                                  height: MediaQuery.of(context).size.height,
                                                                                  width: MediaQuery.of(context).size.width,
                                                                                  color: Colorconstands.white,
                                                                                  child: Column(
                                                                                children: [
                                                                                  Expanded(child: Container()),
                                                                                  DataEmptyWidget(
                                                                                      title: "WAIT_SCORE_ENTRY".tr(),
                                                                                          description: "WAIT_SCORE_ENTRY_DES".tr(),),
                                                                                  Expanded(child: Container()),
                                                                                ],
                                                                              ))
                                                                              : Container(
                                                                                  decoration: const BoxDecoration(
                                                                                    color: Colorconstands.white,
                                                                                  ),
                                                                                  child: SingleChildScrollView(
                                                                                    child: Column(
                                                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                                                      children: [
                                                                                        Padding(
                                                                                          padding: const EdgeInsets.only(top: 10, left: 15, right: 15),
                                                                                          child: Row(
                                                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                            crossAxisAlignment: CrossAxisAlignment.center,
                                                                                            children: [
                                                                                              Column(
                                                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                                                children: [
                                                                                                  Text(
                                                                                                    "ANNUAL_AVERAGE".tr(),
                                                                                                    style: ThemeConstands.headline4_Regular_18.copyWith(color: Colors.grey.shade500),
                                                                                                  ),
                                                                                                  const SizedBox(
                                                                                                    height: 10,
                                                                                                  ),
                                                                                                  Row(
                                                                                                    children: [
                                                                                                      Text(
                                                                                                        //"",
                                                                                                        dataResult.yearResult!.avgScore.toString() == "null" || dataResult.yearResult!.avgScore.toString() == "" ? "- - -" : dataResult.yearResult!.avgScore.toString(),
                                                                                                        style: ThemeConstands.headline1_SemiBold_32,
                                                                                                      ),
                                                                                                      Text(
                                                                                                        dataResult.yearResult!.maxAvgScore.toString() == "null" || dataResult.yearResult!.maxAvgScore.toString() == "" ? "- - -" : "/${dataResult.yearResult!.maxAvgScore}",
                                                                                                        style: ThemeConstands.headline3_SemiBold_20.copyWith(height: 1.4),
                                                                                                      ),
                                                                                                    ],
                                                                                                  )
                                                                                                ],
                                                                                              ),
                                                                                              //const Icon(Icons.share_outlined)
                                                                                            ],
                                                                                          ),
                                                                                        ),
                                                                                        Padding(
                                                                                          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 12),
                                                                                          child: Row(
                                                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                            children: [
                                                                                              Expanded(
                                                                                                child: Column(
                                                                                                  children: [
                                                                                                    HeaderItemWidget(
                                                                                                      title: "GRADE".tr(),
                                                                                                      value: dataResult.yearResult!.grading.toString() == "null" || dataResult.yearResult!.grading.toString() == "" ? "- - -" : "${dataResult.yearResult!.grading}(${dataResult.yearResult!.letterGrade.toString()})",
                                                                                                      color: Colorconstands.controlGreen,
                                                                                                      image: ImageAssets.award,
                                                                                                    ),
                                                                                                    const SizedBox(
                                                                                                      height: 13,
                                                                                                    ),
                                                                                                    HeaderItemWidget(
                                                                                                      title: "TOTALABSENT".tr(),
                                                                                                      value: dataResult.yearResult!.absenceTotal.toString() == "null" || dataResult.yearResult!.absenceTotal.toString() == "" ? "- - -" : dataResult.yearResult!.absenceTotal.toString(),
                                                                                                      color: Colorconstands.black,
                                                                                                      image: ImageAssets.absent,
                                                                                                    )
                                                                                                  ],
                                                                                                ),
                                                                                              ),
                                                                                              const SizedBox(width: 33),
                                                                                              Expanded(
                                                                                                child: Column(
                                                                                                  children: [
                                                                                                    HeaderItemWidget(
                                                                                                      title: "RANK".tr(),
                                                                                                      value: dataResult.yearResult!.rank.toString() == "null" || dataResult.yearResult!.rank.toString() == "" ? "- - -" : '${dataResult.yearResult!.rank}/${dataResult.totalStudent}',
                                                                                                      color: Colorconstands.primaryColor,
                                                                                                      image: ImageAssets.chart,
                                                                                                    ),
                                                                                                    const SizedBox(
                                                                                                      height: 13,
                                                                                                    ),
                                                                                                    HeaderItemWidget(
                                                                                                      title: "STATUS".tr(),
                                                                                                      value: dataResult.yearResult!.isFail.toString() == "0" ? "ជាប់" : "ធ្លាក់",
                                                                                                      color: Colorconstands.controlGreen,
                                                                                                      image: ImageAssets.status_up,
                                                                                                    )
                                                                                                  ],
                                                                                                ),
                                                                                              )
                                                                                            ],
                                                                                          ),
                                                                                        ),
                                                                                        Stack(
                                                                                          children: [
                                                                                            Column(
                                                                                              children: [
                                                                                                Stack(
                                                                                                  children: [
                                                                                                    Column(
                                                                                                      children: [
                                                                                                        SizedBox(
                                                                                                          width: MediaQuery.of(context).size.width,
                                                                                                           child: Container(
                                                                                                            margin:const EdgeInsets.only(top: 65),
                                                                                                             child: Column(
                                                                                                                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                                 crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                 children: List.generate(dataResult.yearSubjects!.length, (index) {
                                                                                                                     print("Index : $index");
                                                                                                                     return Container(
                                                                                                                       margin: const EdgeInsets.symmetric(vertical: 5),
                                                                                                                       child: Column(
                                                                                                                         children: [
                                                                                                                           Row(
                                                                                                                             children: [
                                                                                                                               Expanded(
                                                                                                                                 child: Container(
                                                                                                                                    padding: const EdgeInsets.only(left: 22),
                                                                                                                                    child: Text(
                                                                                                                                      translate == "km" ? dataResult.yearSubjects![index].name.toString() : dataResult.yearSubjects![index].nameEn.toString(),
                                                                                                                                      textAlign: TextAlign.left,
                                                                                                                                      style: ThemeConstands.headline6_Medium_14,
                                                                                                                                    ),                                                                                                                                                         
                                                                                                                                  ),
                                                                                                                               ),
                                                                                                                               Container(
                                                                                                                                  width: 75,
                                                                                                                                  child: Center(
                                                                                                                                    child: Text(
                                                                                                                                      dataResult.yearSubjects![index].firstSemesterAvgScore.toString() == "null" || dataResult.yearSubjects![index].firstSemesterAvgScore.toString() == "" ? "- - -" : dataResult.yearSubjects![index].firstSemesterAvgScore.toString(),
                                                                                                                                      style: ThemeConstands.headline6_SemiBold_14,
                                                                                                                                    ),
                                                                                                                                  ),
                                                                                                                                ),
                                                                                                                                Container(
                                                                                                                                  width: 75,
                                                                                                                                  child: Center(
                                                                                                                                    child: Text(
                                                                                                                                      dataResult.yearSubjects![index].secondSemesterAvgScore.toString() == "null" || dataResult.yearSubjects![index].secondSemesterAvgScore.toString() == "" ? "- - -" : dataResult.yearSubjects![index].secondSemesterAvgScore.toString(),
                                                                                                                                      style: ThemeConstands.headline6_SemiBold_14,textAlign: TextAlign.center,
                                                                                                                                    ),
                                                                                                                                  ),
                                                                                                                                ),
                                                                                                                                Container(
                                                                                                                                  width: 75,
                                                                                                                                  child: Center(
                                                                                                                                    child: Text(
                                                                                                                                      dataResult.yearSubjects![index].avgScore.toString() == "null" || dataResult.yearSubjects![index].avgScore.toString() == "" ? "- - -" : dataResult.yearSubjects![index].avgScore.toString(),
                                                                                                                                      style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.controlRed),
                                                                                                                                    ),
                                                                                                                                  ),
                                                                                                                                ),
                                                                                                                                Container(
                                                                                                                                  width: 85,
                                                                                                                                  child: Text(
                                                                                                                                    dataResult.yearSubjects![index].rank.toString() == "null" || dataResult.yearSubjects![index].rank.toString() == "" ? "- - -" : "${dataResult.yearSubjects![index].rank}/${dataResult.totalStudent}",
                                                                                                                                    style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.controlGreen),textAlign: TextAlign.center,
                                                                                                                                  ),
                                                                                                                                )
                                                                                                                             ],
                                                                                                                           ),
                                                                                                                          Divider()
                                                                                                                         ],
                                                                                                                       ),
                                                                                                                     );
                                                                                                                   },
                                                                                                                 )
                                                                                                              ),
                                                                                                           )
                                                                                                        ),
                                                                                                        dataResult.yearResult == null
                                                                                                            ? Container()
                                                                                                            : Column(
                                                                                                                children: [
                                                                                                                  Container(
                                                                                                                    color: Colorconstands.neutralBtnBg,
                                                                                                                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                                                                                                                    width: MediaQuery.of(context).size.width,
                                                                                                                    child: Row(
                                                                                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                                      children: [
                                                                                                                        Expanded(
                                                                                                                          child: Text(
                                                                                                                            "STATUS".tr(),
                                                                                                                            style: ThemeConstands.headline6_Medium_14.copyWith(fontWeight: FontWeight.bold),
                                                                                                                          ),
                                                                                                                        ),
                                                                                                                        SizedBox(
                                                                                                                          width: MediaQuery.of(context).size.width / 4,
                                                                                                                          child: Center(
                                                                                                                            child: Text(
                                                                                                                              "FIRST_SEMESTER".tr(),
                                                                                                                              style: ThemeConstands.headline6_Medium_14,
                                                                                                                            ),
                                                                                                                          ),
                                                                                                                        ),
                                                                                                                        SizedBox(
                                                                                                                          width: MediaQuery.of(context).size.width / 4,
                                                                                                                          child: Center(
                                                                                                                            child: Text(
                                                                                                                              "SECOND_SEMESTER".tr(),
                                                                                                                              style: ThemeConstands.headline6_Medium_14,
                                                                                                                            ),
                                                                                                                          ),
                                                                                                                        )
                                                                                                                      ],
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                  Padding(
                                                                                                                    padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                                                                                                                    child: Column(
                                                                                                                      children: [
                                                                                                                        SubWidgetThreeRow(
                                                                                                                          title: "SEMESTER_EXAM_RESULT".tr(),
                                                                                                                          subTitle1: dataResult.yearResult!.firstSemesterTotalExamScore.toString() == "null" ? "- - -" : dataResult.yearResult!.firstSemesterTotalExamScore.toString(),
                                                                                                                          subTitle2: dataResult.yearResult!.secondSemesterTotalExamScore.toString() == "null" ? "- - -" : dataResult.yearResult!.secondSemesterTotalExamScore.toString(),
                                                                                                                        ),
                                                                                                                        const Divider(),
                                                                                                                        SubWidgetThreeRow(
                                                                                                                          title: "AVERAGE_SERMESTER_EXAM_SCORE".tr(),
                                                                                                                          subTitle1: dataResult.yearResult!.firstSemesterAvgExamScore.toString() == "null" ? "- - -" : dataResult.yearResult!.firstSemesterAvgExamScore.toString(),
                                                                                                                          subTitle2: dataResult.yearResult!.secondSemesterAvgExamScore.toString() == "null" ? "- - -" : dataResult.yearResult!.secondSemesterAvgExamScore.toString(),
                                                                                                                        ),
                                                                                                                        const Divider(),
                                                                                                                        SubWidgetThreeRow(
                                                                                                                          title: "AVERAGE_MONTHLY_SCORE".tr(),
                                                                                                                          subTitle1: dataResult.yearResult!.firstSemesterMonthAvgScore.toString() == "null" ? "- - -" : dataResult.yearResult!.firstSemesterMonthAvgScore.toString(),
                                                                                                                          subTitle2: dataResult.yearResult!.secondSemesterMonthAvgScore.toString() == "null" ? "- - -" : dataResult.yearResult!.secondSemesterMonthAvgScore.toString(),
                                                                                                                        ),
                                                                                                                        const Divider(),
                                                                                                                        SubWidgetThreeRow(
                                                                                                                          title: "TOTAL_SEMSETER_AVERAGE".tr(),
                                                                                                                          subTitle1: dataResult.yearResult!.firstSemesterAvg.toString() == "null" ? "- - -" : dataResult.yearResult!.firstSemesterAvg.toString(),
                                                                                                                          subTitle2: dataResult.yearResult!.secondSemesterAvg.toString() == "null" ? "- - -" : dataResult.yearResult!.secondSemesterAvg.toString(),
                                                                                                                        ),
                                                                                                                      ],
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                  Container(
                                                                                                                    color: Colorconstands.neutralBtnBg,
                                                                                                                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                                                                                                                    width: MediaQuery.of(context).size.width,
                                                                                                                    child: Row(
                                                                                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                                      children: [
                                                                                                                        SizedBox(
                                                                                                                          width: MediaQuery.of(context).size.width / 4,
                                                                                                                          child: Text(
                                                                                                                            "TOTALABSENT".tr(),
                                                                                                                            style: ThemeConstands.headline6_Medium_14.copyWith(fontWeight: FontWeight.bold),
                                                                                                                          ),
                                                                                                                        ),
                                                                                                                        AbsentWidget(
                                                                                                                          value: "PERMISSION".tr(),
                                                                                                                        ),
                                                                                                                        AbsentWidget(
                                                                                                                          value: "LATE".tr(),
                                                                                                                        ),
                                                                                                                        AbsentWidget(
                                                                                                                          value: "ABSENT".tr(),
                                                                                                                        ),
                                                                                                                      ],
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                  Padding(
                                                                                                                    padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                                                                                                                    child: Row(
                                                                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                                      children: [
                                                                                                                        SizedBox(
                                                                                                                          width: MediaQuery.of(context).size.width / 4,
                                                                                                                          child: Text(
                                                                                                                            "NUMBER_OF_ABSENT".tr(),
                                                                                                                            style: ThemeConstands.headline6_Medium_14.copyWith(fontWeight: FontWeight.bold),
                                                                                                                          ),
                                                                                                                        ),
                                                                                                                        AbsentWidget(
                                                                                                                          isBool: true,
                                                                                                                          value: dataResult.yearResult!.absenceWithPermission!.toString().isEmpty ? "- - -" : dataResult.yearResult!.absenceWithPermission!.toString(),
                                                                                                                        ),
                                                                                                                        AbsentWidget(
                                                                                                                          isBool: true,
                                                                                                                          value: dataResult.yearResult!.absenceWithoutLate!.toString().isEmpty ? "- - -" : dataResult.yearResult!.absenceWithoutLate!.toString(),
                                                                                                                        ),
                                                                                                                        AbsentWidget(
                                                                                                                          isBool: true,
                                                                                                                          value: dataResult.yearResult!.absenceWithoutPermission!.toString().isEmpty ? "- - -" : dataResult.yearResult!.absenceWithoutPermission!.toString(),
                                                                                                                        ),
                                                                                                                      ],
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                  Container(
                                                                                                                    color: Colorconstands.neutralBtnBg,
                                                                                                                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                                                                                                                    width: MediaQuery.of(context).size.width,
                                                                                                                    child: Text(
                                                                                                                      "TEACHER_COMMENTARY".tr(),
                                                                                                                      style: ThemeConstands.headline6_Medium_14.copyWith(fontWeight: FontWeight.bold),
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                  Padding(
                                                                                                                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                                                                                                                    child: Text(
                                                                                                                      dataResult.yearResult!.teacherComment.toString().isEmpty ? "- - -" : dataResult.yearResult!.teacherComment.toString(),
                                                                                                                      textAlign: TextAlign.left,
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                  const SizedBox(
                                                                                                                    height: 20,
                                                                                                                  )
                                                                                                                ],
                                                                                                              ),
                                                                                                      ],
                                                                                                    ),
                                                                                                    paid? Container() : LockScreenUnpaidWidget(title: "YOU_CAN_NOT_ACCESS".tr(), subtitle: "PLEASE_PAY_THE_BILL".tr())
                                                                                                  ],
                                                                                                ),
                                                                                              ],
                                                                                            ),
                                                                                            Positioned(
                                                                                                top: 0,
                                                                                                child: Container(
                                                                                                  padding: const EdgeInsets.only(left: 19, right: 2),
                                                                                                  height: 60,
                                                                                                  width: MediaQuery.of(context).size.width,
                                                                                                  decoration: const BoxDecoration(color: Colorconstands.neutralBtnBg),
                                                                                                  child: Row(children: [
                                                                                                    Expanded(
                                                                                                      child: Container(
                                                                                                        child: Text(
                                                                                                          headerLabelYear[0],
                                                                                                          style: ThemeConstands.headline6_SemiBold_14,
                                                                                                        ),
                                                                                                      ),
                                                                                                    ),
                                                                                                    Container(
                                                                                                     width: 75,
                                                                                                      child: Center(
                                                                                                        child: Text(
                                                                                                          headerLabelYear[1],
                                                                                                          style: ThemeConstands.headline6_SemiBold_14,
                                                                                                        ),
                                                                                                      ),
                                                                                                    ),
                                                                                                    Container(
                                                                                                      width: 75,
                                                                                                      child: Center(
                                                                                                        child: Text(
                                                                                                          headerLabelYear[2],
                                                                                                          style: ThemeConstands.headline6_SemiBold_14,
                                                                                                        ),
                                                                                                      ),
                                                                                                    ),
                                                                                                    Container(
                                                                                                      width: 75,
                                                                                                      child: Center(
                                                                                                        child: Text(
                                                                                                          headerLabelYear[3],
                                                                                                          style: ThemeConstands.headline6_SemiBold_14,
                                                                                                        ),
                                                                                                      ),
                                                                                                    ),
                                                                                                    Container(
                                                                                                      width: 88,
                                                                                                      child: Text(
                                                                                                        headerLabelYear[4],
                                                                                                        style: ThemeConstands.headline6_SemiBold_14,textAlign: TextAlign.center,
                                                                                                      ),
                                                                                                    ),
                                                                                                  ]),
                                                                                                )),
                                                                                          ],
                                                                                        ),
                                                                                      ],
                                                                                    ),
                                                                                  ));
                                                                        } else {
                                                                          return Container(
                                                                            decoration:const BoxDecoration(
                                                                              color: Colorconstands.neutralWhite,
                                                                            ),
                                                                            child: Center(
                                                                              child: ErrorRequestData(
                                                                                onPressed: (){
                                                                                  BlocProvider.of<YearResultBloc>(context)
                                                                                   .add(ResultYearEvent(classId:classId,studentId:studentId));
                                                                                },
                                                                                discription: '', 
                                                                                hidebutton: true, 
                                                                                title: 'WE_DETECT_ERROR'.tr(),

                                                                              ),
                                                                            ),
                                                                          );
                                                                        }
                                                                      },
                                                                    ),
                                                          selected
                                                              ? Container()
                                                              : GestureDetector(
                                                                  onTap: () {
                                                                    setState(() {
                                                                      selected =
                                                                          !selected;
                                                                    });
                                                                  },
                                                                  child: Container(
                                                                    width: MediaQuery.of(
                                                                            context)
                                                                        .size
                                                                        .width,
                                                                    height: MediaQuery.of(
                                                                            context)
                                                                        .size
                                                                        .height,
                                                                    color: const Color(
                                                                        0x7B9C9595),
                                                                  )),
                                                          AnimatedPositioned(
                                                            top: selected == true
                                                                ?MediaQuery.of(context).size.width>800?-575 :-265
                                                                : 0,
                                                            left: 0,
                                                            right: 0,
                                                            duration:
                                                                const Duration(
                                                                    milliseconds:
                                                                        300),
                                                            child: Container(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .symmetric(
                                                                      vertical: 22,
                                                                      horizontal:
                                                                          8),
                                                              decoration: const BoxDecoration(
                                                                  color: Colorconstands
                                                                      .neutralWhite,
                                                                  boxShadow: [
                                                                    BoxShadow(
                                                                        blurRadius:
                                                                            5,
                                                                        offset:
                                                                            Offset(
                                                                                0,
                                                                                5),
                                                                        color:
                                                                            Colorconstands
                                                                                .gray)
                                                                  ]),
                                                              child: Container(
                                                                    height: MediaQuery.of(context).size.width>800?520:210,
                                                                    width: MediaQuery.of(context).size.width,
                                                                    padding:const EdgeInsets.only(top: 0),
                                                                    child: GridView.builder(
                                                                        itemCount: _listMonth.length,
                                                                        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                                                                            childAspectRatio: 2.4,
                                                                            crossAxisCount: 4,
                                                                           //crossAxisSpacing: 25.0, //25
                                                                            mainAxisSpacing: 15.0),
                                                                        shrinkWrap: true,
                                                                        primary: false,
                                                                        padding: const EdgeInsets.only(left: 5, right: 5),
                                                                        itemBuilder: (BuildContext context, int index) {
                                                                          return InkWell(
                                                                            onTap: _listMonth[index].isActive!
                                                                                ? () {
                                                                                    var indexer = index;
                                                                                    setState(() {
                                                                                      selected = !selected;
                                                                                    });
                                                                                    buttonSelected = index;
                                                                                    for (var element in _listMonth) {
                                                                                      element.isSelected = false;
                                                                                    }
                                                                                    _listMonth[index].isSelected = true;

                                                                                    if ((_listMonth[index].month == "S1" || _listMonth[index].month == "S2")) {
                                                                                      setState(() {
                                                                                        isActive = 0;
                                                                                        tangleaction = -1.12;
                                                                                        isMonth = false;
                                                                                        isYear = false;
                                                                                        monthName = "${translate == "km" ? _listMonth[index].displayName : _listMonth[index].displayNameEn} ${_listMonth[index-1].year}";
                                                                                        semesterID = _listMonth[index].semester.toString();
                                                                                        BlocProvider.of<TermResultBloc>(context).add(ResultTermEvent(classId: classId, studentId: studentId, term: semesterID));
                                                                                      });
                                                                                    } else if (_listMonth[index].month == "Y") {
                                                                                      setState(() {
                                                                                        isYear = true;
                                                                                        isActive = 2;
                                                                                        tangleaction = 0.35;
                                                                                        isMonth = false;
                                                                                        monthName = "YEAR_RESULT".tr() + _listMonth[index-2].year;
                                                                                      });
                                                                                      BlocProvider.of<YearResultBloc>(context).add(ResultYearEvent(classId: classId, studentId: studentId));
                                                                                    } else {
                                                                                      // print(data.listMonths![index].months![subindex].month);
                                                                                      setState(() {
                                                                                        isMonth = true;
                                                                                        monthName = "${translate == "km" ? _listMonth[index].displayName : _listMonth[index].displayNameEn} ${_listMonth[index].year}";
                                                                                        monthID = _listMonth[index].month;
                                                                                        semesterID = _listMonth[index].semester;
                                                                                        BlocProvider.of<MonthlyResultBloc>(context).add(ResultMonthlyEvent(classId: classId, month: monthID, studentId: studentId, term: semesterID));
                                                                                        print("MonthId :$monthID");
                                                                                      });
                                                                                    }
                                                                                  }
                                                                                : () {},
                                                                            child: Container(
                                                                              margin:const EdgeInsets.symmetric(horizontal: 6),
                                                                              decoration: BoxDecoration(
                                                                                borderRadius: const BorderRadius.all(Radius.circular(10)),
                                                                                color: _listMonth[index].year != ""
                                                                                    ? _listMonth[index].isSelected!
                                                                                        ? Colorconstands.primaryColor
                                                                                        : Colorconstands.white
                                                                                    : _listMonth[index].isSelected! && _listMonth[index].year == ""
                                                                                        ? Colorconstands.primaryColor
                                                                                        : Colorconstands.white,
                                                                              ),
                                                                              child: Center(
                                                                                child: Text(
                                                                                  _listMonth[index].displayName,
                                                                                  style: ThemeConstands.headline5_Medium_16.copyWith(
                                                                                      fontSize: translate == "km" ? 16 : 12,
                                                                                      decoration: _listMonth[index].isMonth!
                                                                                          ? _listMonth[index].isSelected!
                                                                                              ? TextDecoration.none
                                                                                              : TextDecoration.none
                                                                                          : TextDecoration.underline,
                                                                                      color: !_listMonth[index].isActive! && !_listMonth[index].isSelected!
                                                                                          ? Colorconstands.darkTextsPlaceholder.withOpacity(0.6)
                                                                                          : _listMonth[index].isMonth!
                                                                                              ? _listMonth[index].isSelected!
                                                                                                  ? Colorconstands.white
                                                                                                  : Colorconstands.black
                                                                                              :_listMonth[index].isSelected!?Colorconstands.white: Colors.red),
                                                                                ),
                                                                              ),
                                                                            ),
                                                                          );
                                                                        }),
                                                                  ),
                                                            
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                            ),
                                      ),
                                    ],
                                  ),
                              );
                        } else {
                          return Container(
                            decoration:const BoxDecoration(
                              color: Colorconstands.neutralWhite,
                              borderRadius: BorderRadius.only(topLeft: Radius.circular(15),topRight:  Radius.circular(15))
                            ),
                            child: ErrorRequestData(
                              onPressed: (){
                                Navigator.pushReplacement(
                                    context, 
                                    PageRouteBuilder(
                                        pageBuilder: (context, animation1, animation2) => ResultScreenV2(),
                                        transitionDuration: Duration.zero,
                                        reverseTransitionDuration: Duration.zero,
                                    ),
                                );
                              },
                              discription: '', 
                              hidebutton: true, 
                              title: 'WE_DETECT_ERROR'.tr(),

                            ),
                          );
                        }
                      },
                    ),
                  ),
                ),
              ],
            ),
            isoffline == true
                ? Container()
                : Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    top: 0,
                    child: Container(
                      color: const Color(0x7B9C9595),
                    ),
                  ),
            AnimatedPositioned(
              bottom: isoffline == true ? -150 : 0,
              left: 0,
              right: 0,
              duration: const Duration(milliseconds: 500),
              child: const NoConnectWidget(),
            ),
          ],
        ),
      ),
    );
  }

  void checkIsPiad(bool paid) {
    setState(() {
      isPiad = paid;
    });
  }

  Widget _buildLoading() => Container(
          child: const Center(
        child: CircularProgressIndicator(),
      ));
}

class AbsentWidget extends StatelessWidget {
  final String value;
  bool? isBool;
  AbsentWidget({
    Key? key,
    required this.value,
    this.isBool = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width / 7,
      child: Center(
        child: Text(
          value,
          style: isBool!
              ? ThemeConstands.headline5_SemiBold_16
              : ThemeConstands.headline6_Medium_14,
        ),
      ),
    );
  }
}

class SubWidget extends StatelessWidget {
  bool? isColor;
  final String title, subTitle;
  SubWidget(
      {Key? key,
      required this.title,
      required this.subTitle,
      this.isColor = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: Row(
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width / 2.2,
            child: Text(title,
                style: ThemeConstands.headline6_Regular_14_20height),
          ),
          Expanded(
            child: Text(subTitle,
                style: ThemeConstands.headline5_SemiBold_16.copyWith(
                    fontWeight: FontWeight.bold,
                    color: isColor!
                        ? Colorconstands.controlGreen
                        : Colorconstands.black)),
          ),
        ],
      ),
    );
  }
}

class SubWidgetThreeRow extends StatelessWidget {
  bool? isColor;
  final String title, subTitle1, subTitle2;
  SubWidgetThreeRow(
      {Key? key,
      required this.title,
      required this.subTitle1,
      this.isColor = false,
      required this.subTitle2})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: Row(
        children: [
          Expanded(
            child: Text(title,
                style: ThemeConstands.headline6_Regular_14_20height),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width / 4,
            child: Center(
              child: Text(subTitle1,
                  style: ThemeConstands.headline5_SemiBold_16.copyWith(
                      fontWeight: FontWeight.bold,
                      color: isColor!
                          ? Colorconstands.controlGreen
                          : Colorconstands.black)),
            ),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width / 4,
            child: Center(
              child: Text(subTitle2,
                  style: ThemeConstands.headline5_SemiBold_16.copyWith(
                      fontWeight: FontWeight.bold,
                      color: isColor!
                          ? Colorconstands.controlGreen
                          : Colorconstands.black)),
            ),
          ),
        ],
      ),
    );
  }
}

class UnpaidWidget extends StatelessWidget {
  const UnpaidWidget({
    Key? key,
    required this.isMonth,
  }) : super(key: key);

  final bool? isMonth;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 90,
      bottom: 0,
      left: 0,
      right: 0,
      child: Container(
        height: MediaQuery.of(context).size.height * 4,
        margin: EdgeInsets.only(top: isMonth! ? 10 : 10),
        color: Colorconstands.mainColorSecondary.withOpacity(0.1),
        child: BlurryContainer(
          blur: 4,
          elevation: 0,
          padding: const EdgeInsets.only(bottom: 50),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(ImageAssets.lock),
                Text(
                  "YOU_CAN_NOT_ACCESS".tr(),
                  style: ThemeConstands.headline3_Medium_20_26height
                      .copyWith(fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 15,
                ),
                Text(
                  "PLEASE_PAY_THE_BILL".tr(),
                  textAlign: TextAlign.center,
                  style: ThemeConstands.headline6_Regular_14_20height
                      .copyWith(fontWeight: FontWeight.bold),
                ),
                MaterialButton(
                    onPressed: () {
                      Navigator.pushNamed(context, Routes.PAYMENTOPTIONSCREEN);
                    },
                    child: Container(
                      margin:
                          const EdgeInsets.only(top: 25, left: 25, right: 25),
                      padding: const EdgeInsets.symmetric(
                          vertical: 15, horizontal: 0),
                      decoration: const BoxDecoration(
                          color: Colorconstands.primaryColor,
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      child: Center(
                          child: Text("BUY_SERVICE".tr(),
                              style: ThemeConstands.button_SemiBold_16.copyWith(
                                color: Colorconstands.white,
                              ))),
                    ))
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class HeaderItemWidget extends StatelessWidget {
  final String title, value, image;
  final Color color;

  const HeaderItemWidget({
    Key? key,
    required this.title,
    required this.value,
    required this.image,
    required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SvgPicture.asset(image),
        const SizedBox(
          width: 10,
        ),
        Expanded(
            child: Text(
          title,
          style: ThemeConstands.headline6_Regular_14_20height,
        )),
        Text(value,
            style: ThemeConstands.headline6_SemiBold_14.copyWith(color: color))
      ],
    );
  }
}
