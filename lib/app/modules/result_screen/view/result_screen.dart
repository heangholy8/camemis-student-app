

// import 'dart:async';

// import 'package:blurrycontainer/blurrycontainer.dart';
// import 'package:camis_application_flutter/app/core/resources/asset_resource.dart';
// import 'package:connectivity_plus/connectivity_plus.dart';
// import 'package:dots_indicator/dots_indicator.dart';
// import 'package:easy_localization/easy_localization.dart';
// import '../../../../component/change_child/bloc/change_child_bloc.dart';
// import '../../../../component/dropdown_month/bloc/monthly_bloc.dart';
// import '../../../../model/get_children_model/get_children_model.dart';
// import '../../../../model/result/monthly_result.dart';
// import '../../../../model/result/semester_result.dart';
// import '../../../../storages/get_storage.dart';
// import '../../../../storages/save_storage.dart';
// import '../../../../widgets/custom_alter_box.dart';
// import '../../../../widgets/custom_header_with_image.dart';
// import '../../../../widgets/custom_list_header.dart';
// import '../../../../widgets/header_change_child.dart';
// import '../../../../widgets/list_result_item.dart';
// import '../../../../widgets/list_year_result_item.dart';
// import '../../../../widgets/model_bottomSheet.dart';
// import '../../../../widgets/no_connection_alert_widget.dart';
// import '../../../../widgets/profile_widget.dart';
// import '../../../../widgets/toast_nointernet.dart';
// import '../../auth_screens/e_school_code.dart';
// import '../bloc/bloc/detail_result_bloc.dart';
// import '../bloc/monthly_result_bloc.dart';
// import '../bloc/term_result_bloc.dart';
// import '../bloc/year_bloc.dart';
// import '../widget/custom_header_recommentdetion.dart';
// import '../widget/month_body.dart';
// import '../widget/month_body_fake.dart';
// import '../widget/summary_result_body.dart';
// import '../widget/tanggle_button_result.dart';

// class ResultScreen extends StatefulWidget {
//   const ResultScreen({Key? key}) : super(key: key);
//   @override
//   State<ResultScreen> createState() => _ResultScreenState();
// }

// class _ResultScreenState extends State<ResultScreen>
//     with TickerProviderStateMixin {
//   String monthName = "", studentId = "", classId = "";
//   String monthID = "", semesterID = "";
//   int? buttonSelected;

//   double pageIndex = 0;
//   late bool isPiad;
//   bool? isMonth, isFirstRun;
//   MonthlyData? dataResult;
//   final SaveStoragePref _savePref = SaveStoragePref();
//   TermData? termdataResult;
//   int? selectChild;
//   double isActive = 0;
//   double tangleaction = -1;
//   StreamSubscription? stsub;
//   bool connection = true;
//   @override
//   void initState() {
//     super.initState();
//     //=============Check internet====================
//     stsub = Connectivity().onConnectivityChanged.listen((event) {
//       setState(() {
//         connection = (event != ConnectivityResult.none);
//       });
//     });
//     //=============Check internet====================
//     isMonth = true;
//     isFirstRun = true;
//     isPiad = true;
//     setState(() {
//       getChild();
//       BlocProvider.of<ChangeChildBloc>(context).add(GetChildrenEvent());
//       BlocProvider.of<MonthlyBloc>(context).add(GetDetailChildMonthEvent());
//     });
//   }

//   @override
//   void dispose() {
//     stsub?.cancel();
//     BlocProvider.of<ChangeChildBloc>(context).close();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     final translate = context.locale.toString();
//     return Scaffold(
//       backgroundColor: Colorconstands.primaryColor,
//       body: WillPopScope(
//         onWillPop: ()async{
//           backScreen();
//           return true;
//         },
//         child: SafeArea(
//           bottom: false,
//           child: Column(
//             mainAxisSize: MainAxisSize.max,
//             children: [
//               //---------------------------Header------------------------------
//               Container(
//                  margin: const EdgeInsets.only(top: 15.0,left: 20,right: 22),
//                  child:  Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                   Text("លទ្ធផលសិក្សា",style: ThemeConstands.headline2_SemiBold_24.copyWith(color: Colorconstands.white),),
//                   SvgPicture.asset(ImageAssets.questoin_icon)
//                  ],)),
//               Container(
//                 margin: const EdgeInsets.only(top: 15.0),
//                 child: CustomHeaderWithImage(
//                   title: "Result",
//                   changechild: BlocBuilder<ChangeChildBloc, ChangeChildState>(
//                       builder: (context, state) {
//                     if (state is ChangeChildLoadingState) {
//                       return _buildLoading();
//                     }
//                     if (state is ChangeChildLoadState) {
//                       var data = state.childrenModel!.data as List<ChildrenModel>;
//                       return Profile(
//                         height: 60,
//                         width: 60,
//                         pandding: 1.5,
//                         namechild: '',
//                         sizetextname: 17.0,
//                         onPressed:connection==false?(){showtoastInternet(context);}: () {
//                           showModalBottomSheet(
//                             backgroundColor: Colors.transparent,
//                             isScrollControlled: true,
//                             context: context,
//                             isDismissible: true,
//                             builder: (context) => BuildBottomSheet(
//                               initialChildSize: 0.5,
//                               child: const HeaderChangeChild(),
//                               expanded: Container(
//                                 color: Colorconstands.white,
//                                 child: ListView.builder(
//                                     physics: const NeverScrollableScrollPhysics(),
//                                     shrinkWrap: true,
//                                     padding: const EdgeInsets.all(0),
//                                     itemCount: data.length,
//                                     itemBuilder: (context, index) {
//                                       studentId = data[index].id.toString();
//                                       classId = data[index]
//                                           .currentClass!
//                                           .classId
//                                           .toString();
//                                       return Container(
//                                         alignment: Alignment.center,
//                                         decoration: BoxDecoration(
//                                             color: selectChild == index
//                                                 ? Colorconstands
//                                                     .primaryBackgroundColor
//                                                 : Colorconstands.white,
//                                             border: Border(
//                                                 bottom: BorderSide(
//                                               color: Colorconstands.darkGray
//                                                   .withOpacity(0.1),
//                                               width: 1,
//                                             ))),
//                                         height: 85,
//                                         child: ListTile(
//                                           leading: Container(
//                                             height: 50,
//                                             width: 50,
//                                             child: ClipOval(
//                                                 child: FadeInImage.assetNetwork(
//                                               fit: BoxFit.cover,
//                                               image: data[index]
//                                                           .profileMedia!
//                                                           .fileThumbnail ==
//                                                       null
//                                                   ? data[index]
//                                                       .profileMedia!
//                                                       .fileShow
//                                                       .toString()
//                                                   : data[index]
//                                                       .profileMedia!
//                                                       .fileThumbnail
//                                                       .toString(),
//                                               placeholder:
//                                                   'assets/images/gifs/loading.gif',
//                                             )),
//                                           ),
//                                           title: Text(translate == "en"
//                                                             ? data[selectChild!].nameEn == " " ||data[selectChild!].nameEn ==""
//                                                             ? data[selectChild!].name.toString(): data[selectChild!].nameEn.toString()
//                                                             : data[selectChild!].name.toString(),
//                                             style: ThemeConstands
//                                                 .texttheme.headline6!
//                                                 .copyWith(
//                                                     fontWeight: selectChild == index
//                                                         ? FontWeight.bold
//                                                         : FontWeight.normal),
//                                           ),
//                                           subtitle: Text(
//                                             "CLASS".tr()+": " +
//                                                translate=="en"? data[index].currentClass!.classNameEn.toString()==""? data[index].currentClass!.className.toString()
//                                                : data[index].currentClass!.classNameEn.toString(): data[index].currentClass!.className.toString(),
//                                             style: ThemeConstands
//                                                 .texttheme.subtitle1!
//                                                 .copyWith(),
//                                           ),
//                                           trailing: selectChild == index
//                                               ? const Icon(
//                                                   Icons.check_circle_outline_sharp,
//                                                   color:
//                                                       Colorconstands.secondaryColor,
//                                                 )
//                                               : Container(
//                                                   decoration: BoxDecoration(
//                                                       color: Colorconstands
//                                                           .secondaryColor,
//                                                       borderRadius:
//                                                           BorderRadius.circular(
//                                                               12)),
//                                                   padding:
//                                                       const EdgeInsets.symmetric(
//                                                           horizontal: 15,
//                                                           vertical: 5),
//                                                   child: Text(
//                                                     'Choose',
//                                                     style: ThemeConstands
//                                                         .texttheme.subtitle2!
//                                                         .copyWith(
//                                                             color: Colorconstands
//                                                                 .white),
//                                                   ),
//                                                 ),
//                                           onTap: () {
//                                             Navigator.pop(context);
//                                             setState(() {
//                                               studentId = data[index].id.toString();
//                                               classId = data[index]
//                                                   .currentClass!
//                                                   .classId
//                                                   .toString();
//                                               selectChild = index;
//                                               _savePref.saveChildId(
//                                                   childid:
//                                                       data[index].id.toString());
//                                               _savePref.saveSelectedChild(
//                                                   selectedchild: selectChild!);
//                                               _savePref.saveClassId(
//                                                   classid: data[index]
//                                                       .currentClass!
//                                                       .classId
//                                                       .toString());
//                                               BlocProvider.of<MonthlyResultBloc>(
//                                                       context)
//                                                   .add(ResultMonthlyEvent(
//                                                       classId: classId.toString(),
//                                                       month: monthID.toString(),
//                                                       studentId: studentId,
//                                                       term: semesterID.toString()));
//                                               BlocProvider.of<SecondTermResultBloc>(
//                                                       context)
//                                                   .add(ResultSecondTermEvent(
//                                                       classId: classId,
//                                                       studentId: studentId,
//                                                       term: "SECOND SEMESTER"));
//                                               BlocProvider.of<DetailResultBloc>(
//                                                       context)
//                                                   .add(ResultDetailEvent(
//                                                 classId: classId,
//                                                 studentId: studentId,
//                                               ));
//                                               BlocProvider.of<FirstTermResultBloc>(
//                                                       context)
//                                                   .add(ResultFirstTermEvent(
//                                                       classId: classId,
//                                                       studentId: studentId,
//                                                       term: "FIRST SEMESTER"));
//                                               BlocProvider.of<YearResultBloc>(
//                                                       context)
//                                                   .add(ResultYearEvent(
//                                                       classId: classId,
//                                                       studentId: studentId));
//                                             });
//                                           },
//                                         ),
//                                       );
//                                     }),
//                               ),
//                             ),
//                           );
//                         },
//                         imageProfile:
//                             data[selectChild!].profileMedia!.fileShow == null
//                                 ? data[selectChild!]
//                                     .profileMedia!
//                                     .fileThumbnail
//                                     .toString()
//                                 : data[selectChild!]
//                                     .profileMedia!
//                                     .fileShow
//                                     .toString(),
//                       );
//                     } else {
//                       return Container(
//                           margin: const EdgeInsets.only(right: 10.0),
//                           child: Center(
//                               child: Image.asset(
//                             "assets/images/gifs/loading.gif",
//                             height: 45,
//                             width: 45,
//                           )));
//                     }
//                   }),
//                 ),
//               ),
      
//               //--------------------------- End Header------------------------------
//               const SizedBox(
//                 height: 12.0,
//               ),
//               Expanded(
//                 child: Container(
//                   width: MediaQuery.of(context).size.width,
//                   padding:  EdgeInsets.only(top:connection==false? 0: 15),
//                   decoration: const BoxDecoration(
//                     borderRadius: BorderRadius.only(
//                         topLeft: Radius.circular(15),
//                         topRight: Radius.circular(15)),
//                     color: Colorconstands.white,
//                   ),
//                   child: Column(
//                     mainAxisSize: MainAxisSize.max,
//                     children: [
//                       //================== check condition internet=====================
//                         connection==true?Container():const NoConnectWidget(),
//                       //================== end check condition internet=====================
//                       Expanded(
//                         child: SingleChildScrollView(
//                           physics: !isPiad
//                               ? const NeverScrollableScrollPhysics()
//                               : const AlwaysScrollableScrollPhysics(),
//                           child: Stack(
//                             children: [
//                               Column(
//                                 children: [
//                                   Column(
//                                     mainAxisAlignment: MainAxisAlignment.start,
//                                     crossAxisAlignment:
//                                         CrossAxisAlignment.start,
//                                     children: [
                                      
//                                       Container(
//                                         height: 50,
//                                         width:
//                                             MediaQuery.of(context).size.width,
//                                         decoration: BoxDecoration(
//                                             color: Colorconstands.white,
//                                             borderRadius: BorderRadius.circular(12.0)),
//                                         child: BlocBuilder<MonthlyBloc, MonthlyState>(
//                                             builder: (context, state) {
//                                           if (state is MonthlyLoadingState) {
//                                             return _buildLoading();
//                                           }
//                                           if (state is MonthlyLoadedState) {
//                                             var data =state.currentMonthly!.data;
//                                             if (isFirstRun!) {
//                                               isFirstRun = false;
//                                               for (var element in data!.listMonths!) {
//                                                 for (var month in element.months!) {
//                                                   if (month.isCurrent == true) {
//                                                     monthName = month.displayMonthEn.toString().tr();
//                                                     month.isSelected = true;
//                                                     BlocProvider.of< MonthlyResultBloc>( context).add(ResultMonthlyEvent(
//                                                             classId: data .classId .toString(), month: month.month.toString(),
//                                                             studentId:studentId,term: element.semester.toString()));
//                                                     monthID = month.month.toString();
//                                                     semesterID = element .semester.toString();
//                                                     BlocProvider.of<SecondTermResultBloc>(context) .add(ResultSecondTermEvent(
//                                                             classId: classId,studentId:studentId,term: "SECOND SEMESTER"));
//                                                     BlocProvider.of< DetailResultBloc>( context).add(ResultDetailEvent(
//                                                       classId: classId, studentId: studentId,));
      
//                                                     BlocProvider.of<FirstTermResultBloc>( context).add(ResultFirstTermEvent(
//                                                             classId: classId,studentId: studentId,  term:"FIRST SEMESTER"));
//                                                     BlocProvider.of<YearResultBloc>( context).add(ResultYearEvent(
//                                                       classId: classId,studentId:studentId));
      
//                                                     break;
//                                                   }
//                                                 }
//                                               }
//                                             }
      
//                                             return MaterialButton(
//                                               shape: RoundedRectangleBorder(
//                                                   borderRadius: BorderRadius.circular(12.0)),
//                                               padding: const EdgeInsets.all(0.0),
//                                               onPressed: () {
//                                                 setState(() {
//                                                   showModalBottomSheet(
//                                                       backgroundColor: Colors.transparent,
//                                                       isScrollControlled: true,
//                                                       context: context,
//                                                       isDismissible: true,
//                                                       builder: (context) {
//                                                         return BuildBottomSheet(
//                                                           initialChildSize: 0.55,
//                                                           child: Column(
//                                                             mainAxisSize: MainAxisSize .min,
//                                                             children: [
//                                                               Container(
//                                                                 padding: const EdgeInsets.only(top:10.0),
//                                                                 alignment:
//                                                                     Alignment
//                                                                         .center,
//                                                                 child: Align(
//                                                                   child: Text(
//                                                                       "PLEASESELECTMONTH".tr(),
//                                                                       style: ThemeConstands
//                                                                           .texttheme
//                                                                           .headline6!
//                                                                           .copyWith(
//                                                                               color: Colorconstands.black)),
//                                                                 ),
//                                                               ),
//                                                               Container(
//                                                                 margin: const EdgeInsets
//                                                                         .symmetric(
//                                                                     horizontal:
//                                                                         30,
//                                                                     vertical:
//                                                                         20),
//                                                                 child: Align(
//                                                                   child: Text(
//                                                                     "Excepteur sint occaecat cupidatat non proident, sunt in culpa",
//                                                                     style: ThemeConstands
//                                                                         .texttheme
//                                                                         .subtitle2!
//                                                                         .copyWith(
//                                                                             color:
//                                                                                 Colorconstands.black),
//                                                                     textAlign:
//                                                                         TextAlign
//                                                                             .center,
//                                                                   ),
//                                                                 ),
//                                                               ),
//                                                               Divider(
//                                                                 height: 2,
//                                                                 thickness: 1,
//                                                                 color: Colorconstands
//                                                                     .darkGray
//                                                                     .withOpacity(
//                                                                         0.1),
//                                                               )
//                                                             ],
//                                                           ),
//                                                           expanded:
//                                                               ListView.builder(
//                                                                   physics:
//                                                                       const NeverScrollableScrollPhysics(),
//                                                                   shrinkWrap:
//                                                                       true,
//                                                                   padding:
//                                                                       const EdgeInsets
//                                                                               .all(
//                                                                           0),
//                                                                   itemCount: data!
//                                                                       .listMonths!
//                                                                       .length,
//                                                                   itemBuilder:
//                                                                       (context,
//                                                                           index) {
//                                                                     return Column(
//                                                                         children: List.generate(
//                                                                             data.listMonths![index].months!.length +
//                                                                                 1,
//                                                                             (subindex) {
//                                                                       var count = subindex !=
//                                                                           data.listMonths![index]
//                                                                               .months!
//                                                                               .length;
      
//                                                                       return Container(
//                                                                         alignment:
//                                                                             Alignment.center,
//                                                                         decoration: BoxDecoration(
//                                                                             color: count
//                                                                                 ? data.listMonths![index].months![subindex].isSelected
//                                                                                     ? Colorconstands.primaryColor.withOpacity(0.1)
//                                                                                     : Colorconstands.white
//                                                                                 : data.listMonths![index].isSelected
//                                                                                     ? Colorconstands.primaryColor.withOpacity(0.1)
//                                                                                     : Colorconstands.white,
//                                                                             border: Border(
//                                                                                 bottom: BorderSide(
//                                                                               color: Colorconstands.darkGray.withOpacity(0.1),
//                                                                               width: 1,
//                                                                             ))),
//                                                                         height:
//                                                                             50,
//                                                                         child: Container(
//                                                                             margin: const EdgeInsets.symmetric(horizontal: 15.0),
//                                                                             child: ListTile(
//                                                                               title: Text(
//                                                                                 count ? data.listMonths![index].months![subindex].displayMonthEn.toString().tr()
//                                                                                  :data.listMonths![index].semester==null?data.listMonths![index].nameEn.toString().tr():data.listMonths![index].semester.toString().tr(),
//                                                                                 style: ThemeConstands.texttheme.headline6!.copyWith(fontWeight: FontWeight.normal, color: count ? Colorconstands.primaryColor : Colors.red),
//                                                                               ),
//                                                                               trailing: Icon(
//                                                                                 count
//                                                                                     ? data.listMonths![index].months![subindex].isSelected
//                                                                                         ? Icons.check_circle_rounded
//                                                                                         : Icons.circle_outlined
//                                                                                     : data.listMonths![index].isSelected
//                                                                                         ? Icons.check_circle_rounded
//                                                                                         : Icons.circle_outlined,
//                                                                                 size: 20,
//                                                                                 color: Colorconstands.primaryColor,
//                                                                               ),
//                                                                               //trailing:const Icon(Icons.check_circle),
//                                                                               onTap:connection==false?(){
//                                                                                  Navigator.of(context).pop();
//                                                                                 showtoastInternet(context);}: () {
//                                                                                 buttonSelected = index;
//                                                                                 data.listMonths!.forEach((element) {
//                                                                                   element.isSelected = false;
//                                                                                 });
//                                                                                 data.listMonths!.forEach((element) {
//                                                                                   element.months!.forEach((element) {
//                                                                                     element.isSelected = false;
//                                                                                   });
//                                                                                 });
//                                                                                 if (!count && index == 0) {
//                                                                                   setState(() {
//                                                                                     isActive = 0;
//                                                                                     tangleaction = -1.12;
//                                                                                     isMonth = false;
//                                                                                     monthName = data.listMonths![index].nameEn==null?data.listMonths![index].semester.toString():data.listMonths![index].nameEn.toString();
//                                                                                     data.listMonths![index].isSelected = true;
//                                                                                     BlocProvider.of<FirstTermResultBloc>(context).add(ResultFirstTermEvent(classId: classId, studentId: studentId, term: data.listMonths![index].semester.toString()));
//                                                                                     BlocProvider.of<DetailResultBloc>(context).add(ResultDetailEvent(
//                                                                                       classId: classId,
//                                                                                       studentId: studentId,
//                                                                                     ));
//                                                                                     monthID = data.listMonths![index].months.toString();
//                                                                                     semesterID = data.listMonths![index].semester.toString();
//                                                                                     print("0");
//                                                                                   });
//                                                                                 } else if (!count && index == 1) {
//                                                                                   setState(() {
//                                                                                     isActive = 1;
//                                                                                     tangleaction = -0.40;
//                                                                                     isMonth = false;
//                                                                                     BlocProvider.of<SecondTermResultBloc>(context).add(ResultSecondTermEvent(classId: classId, studentId: studentId, term: data.listMonths![index].semester.toString()));
//                                                                                     BlocProvider.of<DetailResultBloc>(context).add(ResultDetailEvent(
//                                                                                       classId: classId,
//                                                                                       studentId: studentId,
//                                                                                     ));
//                                                                                     monthName = data.listMonths![index].nameEn==null?data.listMonths![index].semester.toString():data.listMonths![index].nameEn.toString();
//                                                                                     data.listMonths![index].isSelected = true;
//                                                                                     monthID = data.listMonths![index].months.toString();
//                                                                                     semesterID = data.listMonths![index].semester.toString();
//                                                                                     print("1");
//                                                                                   });
//                                                                                 } else if (index == 2) {
//                                                                                   setState(() {
//                                                                                     isActive = 2;
//                                                                                     tangleaction = 0.35;
//                                                                                     BlocProvider.of<DetailResultBloc>(context).add(ResultDetailEvent(
//                                                                                       classId: classId,
//                                                                                       studentId: studentId,
//                                                                                     ));
//                                                                                     BlocProvider.of<YearResultBloc>(context).add(ResultYearEvent(classId: classId, studentId: studentId));
//                                                                                     isMonth = false;
//                                                                                     monthName = data.listMonths![index].nameEn==null?data.listMonths![index].semester.toString():data.listMonths![index].nameEn.toString();
//                                                                                     data.listMonths![index].isSelected = true;
//                                                                                     monthID = data.listMonths![index].months.toString();
//                                                                                     semesterID = data.listMonths![index].semester.toString();
//                                                                                   });
//                                                                                 } else {
//                                                                                   // print(data.listMonths![index].months![subindex].month);
//                                                                                   setState(() {
//                                                                                     isMonth = true;
//                                                                                     monthName = data.listMonths![index].months![subindex].displayMonthEn.toString();
//                                                                                     data.listMonths![index].months![subindex].isSelected = true;
//                                                                                     BlocProvider.of<MonthlyResultBloc>(context).add(ResultMonthlyEvent(classId: classId, month: data.listMonths![index].months![subindex].month.toString(), studentId: studentId, term: data.listMonths![index].semester.toString()));
//                                                                                     monthID = data.listMonths![index].months.toString();
//                                                                                     semesterID = data.listMonths![index].semester.toString();
//                                                                                   });
//                                                                                 }
//                                                                                 Navigator.of(context).pop();
//                                                                               },
//                                                                             )),
//                                                                       );
//                                                                     }));
//                                                                   }),
                                                        
//                                                         );
//                                                       });
//                                                 });
//                                               },
//                                               child: Container(
//                                                 margin:const EdgeInsets.symmetric(horizontal: 25),
//                                                 child: Row(
//                                                   mainAxisAlignment:MainAxisAlignment.spaceBetween,
//                                                   children: [
//                                                     Text(
//                                                       monthName.tr() +" ( " + data!.schoolyearName.toString() + " )",
//                                                       style: const TextStyle(color: Colorconstands.secondaryColor,fontSize: 16.0),
//                                                     ),
//                                                     const Icon(
//                                                       Icons.keyboard_arrow_down_rounded, size: 28,
//                                                       color: Colorconstands.secondaryColor,
//                                                     )
//                                                   ],
//                                                 ),
//                                                 alignment: Alignment.centerLeft,
//                                               ),
//                                             );
//                                           } else {
//                                             return _buildLoading();
//                                           }
//                                         }),
//                                       ),
                                      
//                                       const SizedBox(height: 12,),
      
//                                       Column(
//                                         mainAxisSize: MainAxisSize.max,
//                                         children: [
//                                           isMonth!
//                                               ? BlocBuilder<MonthlyResultBloc, MonthlyResultState>(
//                                                   builder: (context, state) {
//                                                   if (state is MonthlyResultLoadingState) {
//                                                     return _buildLoading();
//                                                   }
//                                                   if (state is MonthlyResultLoadedState) {
//                                                     Future.delayed(
//                                                         Duration.zero,
//                                                         () async {
//                                                       checkIsPiad(state.currentMonthlyResult!.status!);
//                                                     });
//                                                     dataResult = state.currentMonthlyResult!.data;
//                                                     return state.currentMonthlyResult!.status !=false
//                                                         ? MonthBody(data: state.currentMonthlyResult!.data!,)
//                                                         : const MonthBodyFake();
//                                                   } else {
//                                                     return _buildLoading();
//                                                   }
//                                                 })
//                                                 :Container(
//                                                   width: MediaQuery.of(context).size.width,
//                                                   margin:const EdgeInsets.only(top: 0.0),
//                                                   padding:const EdgeInsets.all(12.0),
//                                                   color: Colorconstands.primaryBackgroundColor,
//                                                   child: ToggleButtonResult(
//                                                     toggleXAlign: tangleaction,
//                                                     isActive:isActive.toInt(),
//                                                     width: MediaQuery.of(context).size.width,
//                                                     height: 55.0,
//                                                     toggleBackgroundColor: Colors.transparent,
//                                                     toggleBorderColor: Colors.transparent,
//                                                     toggleColor: Colorconstands.primaryColor,
//                                                     activeTextColor: Colors.white,
//                                                     inactiveTextColor: Colors.grey,
//                                                     leftDescription: "FIRST_SEMESTER".tr(),
//                                                     rightDescription: "COMMENT".tr(),
//                                                     centerrightDescription: "YEAR_RESULT".tr(),
//                                                     centerleftDescription: "SECOND_SEMESTER".tr(),
//                                                     onLeftToggleActive: () {
//                                                       setState((){
//                                                         isActive = 0;
//                                                         tangleaction = -1.12;
//                                                       });
//                                                     },
//                                                     onCenterLeftToggleActive: () { 
//                                                        setState(() {
//                                                         isActive = 1;
//                                                         tangleaction = -0.40;
//                                                       });
//                                                      }, 
//                                                      onCenterRightToggleActive: () { 
//                                                        setState(() {
//                                                         isActive = 2;
//                                                         tangleaction = 0.35;
//                                                       });
//                                                      },
//                                                     onRightToggleActive: () {
//                                                       setState(() {
//                                                         isActive = 3;
//                                                         tangleaction = 1.12;
//                                                       });
//                                                     }, 
                                                    
//                                                   ),
//                                                 ),
//                                     /////================body Result =================================
//                                               isMonth! ?Container():
//                                               isActive==0?
//                                               BlocBuilder<FirstTermResultBloc,TermResultState>(
//                                                 builder:(context,state) {
//                                                   if (state is TermResultLoadingState) {
//                                                     return _buildLoading();
//                                                   }
//                                                   if (state is FirstTermResultLoadedState) {
//                                                     termdataResult = state .currentTermResult!.data;
//                                                     return state.currentTermResult!.status != false
//                                                         ? ListResultItem( data:state.currentTermResult!.data!,)
//                                                         : Container();
//                                                   } else {
//                                                     return Center( child: _buildLoading());
//                                                   }
//                                                 },
//                                               ): isActive==1?
//                                               BlocBuilder<SecondTermResultBloc,TermResultState>(
//                                                 builder:(context,state) {
//                                                   if (state is TermResultLoadingState) {
//                                                     return _buildLoading();
//                                                   }
//                                                   if (state is SecondTermResultLoadedState) {
//                                                     termdataResult = state.currentTermResult!.data;
//                                                     return state.currentTermResult!.status !=false
//                                                         ? ListResultItem(data:state.currentTermResult!.data!,)
//                                                         : Container();
//                                                   } else {
//                                                     return Center(child:_buildLoading());
//                                                   }
//                                                 },
//                                               ):isActive==2?
//                                               BlocBuilder<YearResultBloc,YearResultState>(
//                                                 builder:(context,state) {
//                                                   if (state is YearResultLoadingState) {
//                                                     return _buildLoading();
//                                                   }
//                                                   if (state is YearResultLoadedState) {
//                                                     return state.currentResult!.status !=false
//                                                         ? ListView( padding:const EdgeInsets.all(0),
//                                                             shrinkWrap:true,
//                                                             physics: const NeverScrollableScrollPhysics(),
//                                                             children: [
//                                                               CustomListHeader(isMonth: false,),
//                                                               ListYearResultItem(data: state.currentResult!.data!,),
//                                                             ],
//                                                           )
//                                                         : Container();
//                                                   } else {
//                                                     return Center(child:_buildLoading());
//                                                   }
//                                                 },
//                                               ):
//                                               BlocBuilder<YearResultBloc,YearResultState>(
//                                                 builder:(context,state) {
//                                                   if (state is YearResultLoadingState) {
//                                                     return _buildLoading();
//                                                   }
//                                                   if (state is YearResultLoadedState) {
//                                                     return state.currentResult!.status !=false
//                                                         ? Column(
//                                                             children: [
//                                                               const CustomHeaderRecommandetion(),
//                                                               ListView.separated(
//                                                                 padding: const EdgeInsets.all(0),
//                                                                 physics: const NeverScrollableScrollPhysics(),
//                                                                 shrinkWrap: true,
//                                                                 itemCount: state.currentResult!.data!.yearSubjects!.length,
//                                                                 itemBuilder: (context, index) {
//                                                                   return Container(
//                                                                     padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
//                                                                     width: MediaQuery.of(context).size.width,
//                                                                     child: Row(
//                                                                       children: [
//                                                                         Expanded(
//                                                                           flex: 1,
//                                                                           child: Center(
//                                                                             child: Text(
//                                                                               (index + 1).toString(),
//                                                                               style: ThemeConstands.texttheme.subtitle1,
//                                                                             ),
//                                                                           ),
//                                                                         ),
//                                                                         Expanded(
//                                                                           flex: 3,
//                                                                           child: Container(
//                                                                             padding: const EdgeInsets.only(left: 8),
//                                                                             child: Align(
//                                                                               alignment: Alignment.centerLeft,
//                                                                               child: Text(
//                                                                                 translate=="en"?state.currentResult!.data!.yearSubjects![index].nameEn.toString():state.currentResult!.data!.yearSubjects![index].name.toString(),
//                                                                                 textAlign: TextAlign.left,
//                                                                                 style: ThemeConstands.texttheme.subtitle1,
//                                                                               ),
//                                                                             ),
//                                                                           ),
//                                                                         ),
//                                                                         Expanded(
//                                                                           flex: 2,
//                                                                           child: Container(
//                                                                             margin: const EdgeInsets.only(right: 50),
//                                                                             child: Center(
//                                                                               child: Text(
//                                                                                 translate=="en"?state.currentResult!.data!.yearSubjects![index].letterGradeEn.toString()
//                                                                                 :state.currentResult!.data!.yearSubjects![index].letterGrade.toString() + "(" + state.currentResult!.data!.yearSubjects![index].letterGradeEn.toString() + ")",
//                                                                                 textAlign: TextAlign.left,
//                                                                                 style: ThemeConstands.texttheme.subtitle1,
//                                                                               ),
//                                                                             ),
//                                                                           ),
//                                                                         )
//                                                                       ],
//                                                                     ),
//                                                                   );
//                                                                 },
//                                                                 separatorBuilder: (BuildContext context, int index) {
//                                                                   return const Divider(
//                                                                     thickness: 0.5,
//                                                                     indent: 30,
//                                                                     endIndent: 30,
//                                                                     height: 10,
//                                                                   );
//                                                                 },
//                                                               )
//                                                             ],
//                                                           )
//                                                         : Container();
//                                                   } else {
//                                                     return Center(child: _buildLoading());
//                                                   }
//                                                 }),
                                              
//                                     /////================End body Result =================================
//                                     ///
//                                     ///================== Dot Style =========================
//                                                isMonth! ?Container():Container(
//                                                 margin:const EdgeInsets.symmetric(vertical: 18.0),
//                                                  child: DotsIndicator(
//                                                           dotsCount:4,
//                                                           position:isActive,
//                                                           decorator:
//                                                               DotsDecorator(
//                                                             shape: RoundedRectangleBorder(
//                                                                 borderRadius:
//                                                                     BorderRadius
//                                                                         .circular(
//                                                                             5.0)),
//                                                             size: const Size(
//                                                                 30.0, 3.0),
//                                                             activeSize:
//                                                                 const Size(
//                                                                     30.0, 3.0),
//                                                             activeShape: RoundedRectangleBorder(
//                                                                 borderRadius:
//                                                                     BorderRadius
//                                                                         .circular(
//                                                                             5.0)),
//                                                           ),
//                                                         ),
//                                                ),
//                                          ///================== end Dot Style =========================
//                                          ///
//                                          /// ///================== Attendance Style =========================
//                                                     isMonth! ?Container(): BlocBuilder<
//                                                           DetailResultBloc,
//                                                           DetailResultState>(
//                                                         builder:
//                                                             (context, state) {
//                                                           if (state
//                                                               is DetailResultLoadingState) {
//                                                             return _buildLoading();
//                                                           }
//                                                           if (state
//                                                               is DetailResultLoadedState) {
//                                                             return state.currentDetailResult!
//                                                                         .status !=
//                                                                     false
//                                                                 ? SummaryResultBody(
//                                                                     data: state
//                                                                         .currentDetailResult!
//                                                                         .data!)
//                                                                 : Container();
//                                                             ;
//                                                           } else {
//                                                             return Center(
//                                                                 child:
//                                                                     _buildLoading());
//                                                           }
//                                                         },
//                                                       ),
//                                       /// ///==================End Attendance Style =========================
//                                                       const SizedBox(
//                                                         height: 50,
//                                                       )
      
//                                         ],
//                                       ),
                                    
//                                     ],
//                                   ),
//                                 ],
//                               ),
//                               !isPiad
//                                   ? Container(
//                                       margin: EdgeInsets.only(
//                                           top: isMonth! ? 10 : 170),
//                                       child: BlurryContainer(
//                                         child: const CustomAlert(),
//                                         blur: 3,
//                                         height: isMonth!
//                                             ? MediaQuery.of(context)
//                                                     .size
//                                                     .height *
//                                                 0.9
//                                             : MediaQuery.of(context)
//                                                     .size
//                                                     .height *
//                                                 0.6,
//                                         elevation: 0,
//                                         padding:
//                                             const EdgeInsets.only(bottom: 50),
//                                       ),
//                                     )
//                                   : const SizedBox()
//                             ],
//                           ),
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
//   void backScreen(){
//     if(connection==true){
//         Navigator.pushReplacement(
//           context,
//           MaterialPageRoute(
//             builder: (context) => HomeScreen(isConnect: true,),
//           ),
//         );
//       }
//       else{
//         Navigator.pushReplacement(
//           context,
//           MaterialPageRoute(
//             builder: (context) => HomeScreen(isConnect: false,),
//           ),
//         );
//       }
//    }

//   void getChild() async {
//     final GetStoragePref _pref = GetStoragePref();
//     var studentID = await _pref.getChildId();
//     var classid = await _pref.getClassId();
//     var selectedChild = await _pref.getSelectedChild();
//     setState(() {
//       studentId = studentID.toString();
//       classId = classid.toString();
//       selectChild = selectedChild;
//       isFirstRun = true;
//     });
//   }

//   void checkIsPiad(bool paid) {
//     setState(() {
//       isPiad = paid;
//     });
//   }

//   Widget _buildLoading() => Container(
//       margin: const EdgeInsets.only(right: 10.0),
//       child: Center(
//           child: Image.asset(
//         "assets/images/gifs/loading.gif",
//         height: 45,
//         width: 45,
//       )));
// }
