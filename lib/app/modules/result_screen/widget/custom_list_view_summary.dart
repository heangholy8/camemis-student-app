import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../../model/result/detail_result.dart';
import '../model/summary_result.dart';

class CustomListViewSummaryAttendance extends StatefulWidget {
  final List<SummaryResult> list;
  DetailData data;

  CustomListViewSummaryAttendance({
    Key? key,
    required this.list,
    required this.data,
  }) : super(key: key);

  @override
  State<CustomListViewSummaryAttendance> createState() =>
      _CustomListViewSummaryAttendanceState();
}

class _CustomListViewSummaryAttendanceState
    extends State<CustomListViewSummaryAttendance> {
  @override
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(0),
      child: ListView.separated(
        padding: const EdgeInsets.all(0),
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: widget.list.length,
        itemBuilder: (context, index) {
          return Column(
            children: [
              Row(
                children: [
                  Expanded(
                      flex: 2,
                      child: Container(
                        margin: const EdgeInsets.only(left: 30),
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              widget.list[index].title,
                              textAlign: TextAlign.left,
                            )),
                      )),
                  Expanded(
                      child: Center(
                          child: Text(index == 0
                              ? widget.data.totalAttendance![0]
                                              .firstSemesterPermission
                                              .toString() ==
                                          "null" ||
                                      widget.data.totalAttendance![0]
                                              .firstSemesterPermission
                                              .toString() ==
                                          ""
                                  ? "- - -"
                                  : widget.data.totalAttendance![0]
                                      .firstSemesterPermission
                                      .toString()
                              : widget.data.totalAttendance![0]
                                              .firstSemesterWithoutPermission
                                              .toString() ==
                                          "null" ||
                                      widget.data.totalAttendance![0]
                                              .firstSemesterWithoutPermission
                                              .toString() ==
                                          ""
                                  ? "- - -"
                                  : widget.data.totalAttendance![0]
                                      .firstSemesterWithoutPermission
                                      .toString()))),
                  Expanded(
                      child: Center(
                          child: Text(index == 0
                              ? widget.data.totalAttendance![1]
                                              .secondSemesterPermission
                                              .toString() ==
                                          "null" ||
                                      widget.data.totalAttendance![1]
                                              .secondSemesterPermission
                                              .toString() ==
                                          ""
                                  ? "- - -"
                                  : widget.data.totalAttendance![1]
                                      .secondSemesterWithoutPermission
                                      .toString()
                              : widget.data.totalAttendance![1]
                                              .secondSemesterWithoutPermission
                                              .toString() ==
                                          "null" ||
                                      widget.data.totalAttendance![1]
                                              .secondSemesterWithoutPermission
                                              .toString() ==
                                          ""
                                  ? "- - -"
                                  : widget.data.totalAttendance![1]
                                      .secondSemesterPermission
                                      .toString()))),
                  Expanded(
                      child: Center(
                          child: Text(index == 0
                              ? widget.data.totalAttendance![2].inYearPermission
                                              .toString() ==
                                          "null" ||
                                      widget.data.totalAttendance![2]
                                              .inYearPermission
                                              .toString() ==
                                          ""
                                  ? "- - -"
                                  : widget.data.totalAttendance![2]
                                      .inYearWithoutPermission
                                      .toString()
                              : widget.data.totalAttendance![2]
                                              .inYearWithoutPermission
                                              .toString() ==
                                          "null" ||
                                      widget.data.totalAttendance![2]
                                              .inYearWithoutPermission
                                              .toString() ==
                                          ""
                                  ? "- - -"
                                  : widget.data.totalAttendance![2]
                                      .inYearWithoutPermission
                                      .toString())))
                ],
              )
            ],
          );
        },
        separatorBuilder: (BuildContext context, int index) {
          return const Divider(
            thickness: 2,
            indent: 25,
            endIndent: 25,
          );
        },
      ),
    );
  }
}

class CustomListViewEvaluation extends StatefulWidget {
  List<SummaryResult> list;
  DetailData data;

  CustomListViewEvaluation({
    Key? key,
    required this.list,
    required this.data,
  }) : super(key: key);

  @override
  _CustomListViewEvaluationState createState() =>
      _CustomListViewEvaluationState();
}

class _CustomListViewEvaluationState extends State<CustomListViewEvaluation> {
  @override
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(0),
      child: ListView.separated(
        padding: const EdgeInsets.all(0),
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: widget.list.length,
        itemBuilder: (context, index) {
          return Column(
            children: [
              Row(
                children: [
                  Expanded(
                      flex: 2,
                      child: Container(
                        margin: const EdgeInsets.only(left: 30),
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              widget.list[index].title,
                              textAlign: TextAlign.left,
                            )),
                      )),
                  Expanded(
                      child: Center(
                          child: Text(index == 0
                              ? widget.data.evaluation![0].firstSemisterMorality
                                              .toString() ==
                                          "null" ||
                                      widget.data.evaluation![0].firstSemisterMorality
                                              .toString() ==
                                          ""
                                  ? "- - -"
                                  : widget.data.evaluation![0].firstSemisterMorality
                                      .toString()
                              : index == 1
                                  ? widget.data.evaluation![0]
                                                  .firstSemisterBangkeunPhal
                                                  .toString() ==
                                              "null" ||
                                          widget.data.evaluation![0]
                                                  .firstSemisterBangkeunPhal
                                                  .toString() ==
                                              ""
                                      ? "- - -"
                                      : widget.data.evaluation![0]
                                          .firstSemisterBangkeunPhal
                                          .toString()
                                  : widget.data.evaluation![0].firstSemisterHealth
                                                  .toString() ==
                                              "null" ||
                                          widget.data.evaluation![0]
                                                  .firstSemisterHealth
                                                  .toString() ==
                                              ""
                                      ? "- - -"
                                      : widget.data.evaluation![0]
                                          .firstSemisterHealth
                                          .toString()))),
                  Expanded(
                      child: Center(
                          child: Text(index == 0
                              ? widget.data.evaluation![0].secondSemisterMorality
                                              .toString() ==
                                          "null" ||
                                      widget.data.evaluation![0]
                                              .secondSemisterMorality
                                              .toString() ==
                                          ""
                                  ? "- - -"
                                  : widget.data.evaluation![0]
                                      .secondSemisterMorality
                                      .toString()
                              : index == 1
                                  ? widget.data.evaluation![0]
                                                  .secondSemisterBangkeunPhal
                                                  .toString() ==
                                              "null" ||
                                          widget.data.evaluation![0]
                                                  .secondSemisterBangkeunPhal
                                                  .toString() ==
                                              ""
                                      ? "- - -"
                                      : widget.data.evaluation![0]
                                          .secondSemisterBangkeunPhal
                                          .toString()
                                  : widget.data.evaluation![0].secondSemisterHealth
                                                  .toString() ==
                                              "null" ||
                                          widget.data.evaluation![0]
                                                  .secondSemisterHealth
                                                  .toString() ==
                                              ""
                                      ? "- - -"
                                      : widget.data.evaluation![0].secondSemisterHealth.toString()))),
                  Expanded(
                      child: Center(
                          child: Text(index == 0
                              ? widget.data.evaluation![0].inYearMorality.toString() ==
                                          "null" ||
                                      widget.data.evaluation![0].inYearMorality
                                              .toString() ==
                                          ""
                                  ? "- - -"
                                  : widget.data.evaluation![0].inYearMorality
                                      .toString()
                              : index == 1
                                  ? widget.data.evaluation![0].inYearBangkeunPhal
                                                  .toString() ==
                                              "null" ||
                                          widget.data.evaluation![0]
                                                  .inYearBangkeunPhal
                                                  .toString() ==
                                              ""
                                      ? "- - -"
                                      : widget.data.evaluation![0].inYearBangkeunPhal
                                          .toString()
                                  : widget.data.evaluation![0].inYearHealth
                                                  .toString() ==
                                              "null" ||
                                          widget.data.evaluation![0].inYearHealth
                                                  .toString() ==
                                              ""
                                      ? "- - -"
                                      : widget.data.evaluation![0].inYearHealth.toString())))
                ],
              )
            ],
          );
        },
        separatorBuilder: (BuildContext context, int index) {
          return const Divider(
            thickness: 2,
            indent: 25,
            endIndent: 25,
          );
        },
      ),
    );
  }
}
class CustomListViewHonor extends StatefulWidget {
  List<SummaryResult> list;


  CustomListViewHonor({
    Key? key,
    required this.list,
  }) : super(key: key);

  @override
  _CustomListViewHonorState createState() =>
      _CustomListViewHonorState();
}

class _CustomListViewHonorState extends State<CustomListViewHonor> {
  @override
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(0),
      child: ListView.separated(
        padding: const EdgeInsets.all(0),
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: widget.list.length,
        itemBuilder: (context, index) {
          return Column(
            children: [
              Row(
                children: [
                  Expanded(
                      flex: 2,
                      child: Container(
                        margin: const EdgeInsets.only(left: 30),
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              widget.list[index].title,
                              textAlign: TextAlign.left,
                            )),
                      )),
                  const Expanded(
                      child: Center(
                          child:  Text("- - -"
                              ))),
                  const Expanded(
                      child: Center(
                          child:  Text("- - -"
                              ))),
                              const Expanded(
                      child: Center(
                          child:  Text("- - -"
                              ))),
                ],
              )
            ],
          );
        },
        separatorBuilder: (BuildContext context, int index) {
          return const Divider(
            thickness: 2,
            indent: 25,
            endIndent: 25,
          );
        },
      ),
    );
  }
}
