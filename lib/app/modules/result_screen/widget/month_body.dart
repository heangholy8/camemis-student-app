import 'package:camis_application_flutter/model/result/monthly_result.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';

import '../../../../widgets/custom_list_header.dart';
import '../../../../widgets/custom_list_result.dart';
import '../../../core/themes/color_app.dart';
import '../controller/show_bottom_sheet_general.dart';
import 'container_body.dart';
import 'custom_container_title_and_subTitle.dart';
import 'custom_outline_button.dart';

class MonthBody extends StatelessWidget {
  MonthlyData data;
  MonthBody({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Column(
          children: [
            CustomListHeader(),
            CustomListResult(
              data: data,
            ),
          ],
        ),
        ContainerBody(
          data: data,
        ),
        data.resultMonthly != null
            ? Column(
                children: [
                  Container(
                    margin:
                        const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                    child: DottedLine(
                      lineThickness: 1,
                      dashLength: 3,
                      dashColor: Colorconstands.darkGray.withOpacity(0.5),
                    ),
                  ),
                  TitleAndSubTitle(
                      title: "TOTALABSENT".tr(),
                      subTitle: data.resultMonthly!.absenceTotal!.toString()),
                  TitleAndSubTitle(
                      title: "PERMISSION".tr(),
                      subTitle: data.resultMonthly!.absenceWithPermission!
                          .toString()),
                  TitleAndSubTitle(
                      title: "ABSENT".tr(),
                      subTitle: data.resultMonthly!.absenceWithoutPermission!
                          .toString()),
                  Container(
                    margin:
                        const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                    child: DottedLine(
                      lineThickness: 1,
                      dashLength: 3,
                      dashColor: Colorconstands.darkGray.withOpacity(0.5),
                    ),
                  ),
                  CustomOutLineButton(
                    onPress: () {
                      showButtomSheet(
                          context,
                          "TEACHER_COMMENTARY".tr(),
                          "",
                          data.resultMonthly!.teacherComment!.toString());
                    },
                    title: "TEACHER_COMMENTARY".tr()+": ",
                  ),
                  CustomOutLineButton(
                    onPress: () {
                      showButtomSheet(
                          context,
                          "RECOMMENDATION".tr(),
                          "",
                          data.resultMonthly!.recommendation!.toString());
                    },
                    title: "RECOMMENDATION".tr()+": ",
                  ),
                  CustomOutLineButton(
                    onPress: () {
                      showButtomSheet(
                          context,
                          "BEHAVIOR".tr(),
                          "",
                          data.resultMonthly!.behavior!.toString());
                    },
                    title: "BEHAVIOR".tr()+": ",
                  ),
                ],
              )
            : Container(),
        const SizedBox(
          height: 100,
        ),
      ],
    );
  }
}
