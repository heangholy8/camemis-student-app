import 'package:camis_application_flutter/model/result/detail_result.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';

import '../data/summary_result_data.dart';
import 'custom_header_result.dart';
import 'custom_list_view_summary.dart';
import 'custom_sub_header_result.dart';

class SummaryResultBody extends StatefulWidget {
  DetailData data;
  SummaryResultBody({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  State<SummaryResultBody> createState() => _SummaryResultBodyState();
}

class _SummaryResultBodyState extends State<SummaryResultBody> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        HeaderResult(
          title: "NUMBER_ABSENCES_SCHOOL_YEAR".tr(),
        ),
        SubHeaderResult(
          title: "PERMISSION".tr(),
        ),
        CustomListViewSummaryAttendance(
          list: summaryAbsent,
          data: widget.data,
        ),
        HeaderResult(
          title: "EVALUATION".tr(),
        ),
        SubHeaderResult(
          title: "THE_THREE_PART".tr(),
        ),
        CustomListViewEvaluation(
          list: summaryValuation,
          data: widget.data,
        ),
        HeaderResult(
          title: "PRAISE".tr(),
        ),
        SubHeaderResult(
          title: "RECEVIED".tr(),
        ),
        CustomListViewHonor(
          list: summaryMedal,
        )
      ],
    );
  }
}
