import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../core/themes/themes.dart';
class SubHeaderResult extends StatelessWidget {
  final String title;
  const SubHeaderResult({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding:const EdgeInsets.all(0),
      margin: const EdgeInsets.only(top: 5, right: 5, left: 5),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                flex: 2,
                child: Container(
                  margin:const EdgeInsets.only(left: 30),
                  child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(title,
                          style: ThemeConstands.texttheme.subtitle1)),
                ),
              ),
              Expanded(
                child: Center(
                  child:
                      Text("FIRST_SEMESTER".tr(),textAlign: TextAlign.center, style: ThemeConstands.texttheme.subtitle1),
                ),
              ),
              Expanded(
                child: Center(
                  child:
                      Text("SECOND_SEMESTER".tr(),textAlign: TextAlign.center, style: ThemeConstands.texttheme.subtitle1),
                ),
              ),
              Expanded(
                child: Center(
                  child: Text("YEAR_RESULT".tr(),
                  textAlign: TextAlign.center,
                      style: ThemeConstands.texttheme.subtitle1),
                ),
              ),
            ],
          ),
          const Divider(
            indent: 25,
            endIndent: 20,
            thickness: 2,
          )
        ],
      ),
    );
  }
}
