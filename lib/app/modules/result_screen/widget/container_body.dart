import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';

import '../../../../model/result/monthly_result.dart';
import '../../../core/themes/color_app.dart';
import 'custom_container_title_and_subTitle.dart';
import 'custom_contianer.dart';

class ContainerBody extends StatefulWidget {
  MonthlyData data;
  ContainerBody({Key? key, required this.data}) : super(key: key);

  @override
  State<ContainerBody> createState() => _ContainerBodyState();
}

class _ContainerBodyState extends State<ContainerBody> {
  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return widget.data.resultMonthly != null
        ? Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: CustomContainer(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ContainTitleAndSubTitle(
                      title: "MONTHLYAVERAGE".tr()+": ",
                      subTitle: widget.data.resultMonthly!.absenceExam != 1
                          ? widget.data.resultMonthly!.avgScore!
                              .toStringAsFixed(2)
                          : "NORANK".tr()+": ",
                    ),
                    ContainTitleAndSubTitle(
                      title: "MONTHLY_TOTAL_SCORE".tr()+": ",
                      subTitle:
                          widget.data.resultMonthly!.totalScore!.toString(),
                    ),
                  ],
                )),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                child: CustomContainer(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ContainTitleAndSubTitleSmallContainer(
                      title: "GRADE".tr()+": ",
                      subTitle: translate=="en"?widget.data.resultMonthly!.gradingEn!.toString()==""||widget.data.resultMonthly!.gradingEn==null?widget.data.resultMonthly!.grading.toString()
                      :widget.data.resultMonthly!.gradingEn!.toString():widget.data.resultMonthly!.grading.toString(),
                    ),
                    ContainTitleAndSubTitleSmallContainer(
                      title: "RANK".tr()+": ",
                      subTitle: widget.data.resultMonthly!.absenceExam != 1
                          ? widget.data.resultMonthly!.rank!.toString()
                          : "NORANK".tr()+": ",
                    )
                  ],
                )),
              ),
              const SizedBox(
                width: 15,
              ),
            ],
          )
        : Container();
  }
}
