import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class CustomOutLineButton extends StatelessWidget {
  final String title;
  final VoidCallback onPress;
  const CustomOutLineButton({
    Key? key,
    required this.onPress,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      child: Column(
        children: [
          OutlinedButton(
            style: OutlinedButton.styleFrom(
              side: BorderSide.none,
            ),
            onPressed: onPress,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  title,
                  style: ThemeConstands.texttheme.titleMedium?.copyWith(
                      color: Colorconstands.secondaryColor, fontSize: 17),
                ),
                Icon(
                  Icons.arrow_forward_ios_outlined,
                  color: Colorconstands.iconColor.withOpacity(0.5),
                )
              ],
            ),
          ),
          const Divider(
            thickness: 1,
            indent: 15,
            endIndent: 15,
          )
        ],
      ),
    );
  }
}
