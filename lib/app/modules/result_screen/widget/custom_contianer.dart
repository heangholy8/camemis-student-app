import 'package:flutter/cupertino.dart';

import '../../../core/themes/color_app.dart';

class CustomContainer extends StatelessWidget {
  final Widget child;
  const CustomContainer({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 180,
      padding:const EdgeInsets.symmetric(horizontal: 8, vertical: 10),
      margin: const EdgeInsets.symmetric(vertical: 20),
      decoration: const BoxDecoration(
          color: Colorconstands.secondaryColor,
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: child,
    );
  }
}

class CustomContainer2 extends StatelessWidget {
  final Widget child;
  const CustomContainer2({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding:const EdgeInsets.symmetric(horizontal: 8, vertical: 25),
      margin: const EdgeInsets.symmetric(vertical: 20),
      decoration: const BoxDecoration(
          color: Colorconstands.secondaryColor,
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: child,
    );
  }
}
