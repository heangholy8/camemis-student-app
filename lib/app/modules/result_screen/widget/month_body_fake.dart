import 'package:camis_application_flutter/app/modules/result_screen/widget/container_body_fake.dart';
import 'package:camis_application_flutter/widgets/custom_list_result_fake.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/cupertino.dart';
import '../../../../widgets/custom_list_header.dart';
import '../../../core/themes/color_app.dart';
import '../controller/show_bottom_sheet_general.dart';
import 'custom_container_title_and_subTitle.dart';
import 'custom_outline_button.dart';

class MonthBodyFake extends StatelessWidget {
 const MonthBodyFake({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Column(
          children: [
            CustomListHeader(),
            CustomListResultFake(),
          ],
        ),
        const ContainerBodyFake(),
        Column(
          children: [
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              child: DottedLine(
                lineThickness: 1,
                dashLength: 3,
                dashColor: Colorconstands.darkGray.withOpacity(0.5),
              ),
            ),
            TitleAndSubTitle(title: "អវត្តមានសរុប", subTitle: "0".toString()),
            TitleAndSubTitle(title: "មានច្បាប់", subTitle: "0".toString()),
            TitleAndSubTitle(title: "ឥតច្បាប់", subTitle: "0".toString()),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              child: DottedLine(
                lineThickness: 1,
                dashLength: 3,
                dashColor: Colorconstands.darkGray.withOpacity(0.5),
              ),
            ),
            CustomOutLineButton(
              onPress: () {
                showButtomSheet(
                    context, "មូលវិចារណ៍គ្រូ", "សម្រាបខែមករា ២០២២-២០២៣", "");
              },
              title: "មូលវិចារណ៍គ្រូ:  ",
            ),
            CustomOutLineButton(
              onPress: () {
                showButtomSheet(
                    context, "អនុសាសន៍", "សម្រាបខែមករា ២០២២-២០២៣", "");
              },
              title: "អនុសាសន៍:  ",
            ),
            CustomOutLineButton(
              onPress: () {
                showButtomSheet(
                    context, "អាកប្បកិរិយា", "សម្រាបខែមករា ២០២២-២០២៣", "");
              },
              title: "អាកប្បកិរិយា:   ",
            ),
          ],
        ),
        const SizedBox(
          height: 100,
        ),
      ],
    );
  }
}
