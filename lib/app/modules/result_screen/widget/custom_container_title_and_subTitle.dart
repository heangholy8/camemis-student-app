import 'package:flutter/cupertino.dart';

import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class ContainTitleAndSubTitle extends StatefulWidget {
  final String title;
  final String subTitle;
  const ContainTitleAndSubTitle({
    Key? key,
    required this.title,
    required this.subTitle,
  }) : super(key: key);

  @override
  State<ContainTitleAndSubTitle> createState() =>
      _ContainTitleAndSubTitleState();
}

class _ContainTitleAndSubTitleState extends State<ContainTitleAndSubTitle> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin:const EdgeInsets.symmetric(vertical: 05),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Expanded(
              child: Text(
                widget.title,
                overflow: TextOverflow.clip,
                style: ThemeConstands.texttheme.bodyText1?.copyWith(
                    color: Colorconstands.white, fontWeight: FontWeight.w600),
              ),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.centerRight,
                child: Text(widget.subTitle,
                    overflow: TextOverflow.clip,
                    style: ThemeConstands.texttheme.headline6?.copyWith(
                        color: Colorconstands.white, fontWeight: FontWeight.w700)),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ContainTitleAndSubTitleSmallContainer extends StatelessWidget {
  final String title;
  final String subTitle;
  const ContainTitleAndSubTitleSmallContainer({
    Key? key,
    required this.title,
    required this.subTitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:const EdgeInsets.symmetric(vertical: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Expanded(
            child: Text(
              title,
              overflow: TextOverflow.clip,
              style: ThemeConstands.texttheme.titleMedium?.copyWith(
                  color: Colorconstands.white,
                  fontWeight: FontWeight.w600,
                  fontSize: 17),
            ),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.centerRight,
              child: Text(subTitle,
                  textAlign: TextAlign.start,
                  style: ThemeConstands.texttheme.headline6?.copyWith(
                    color: Colorconstands.white,
                    fontWeight: FontWeight.w800,
                    height: 1,
                  )),
            ),
          )
        ],
      ),
    );
  }
}

class TitleAndSubTitle extends StatelessWidget {
  final String title;
  final String subTitle;
  const TitleAndSubTitle({
    Key? key,
    required this.title,
    required this.subTitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            width: 200,
            child: Text(
              title,
              overflow: TextOverflow.ellipsis,
              style:
                  ThemeConstands.texttheme.titleMedium?.copyWith(fontSize: 17),
            ),
          ),
          SizedBox(
            width: 10,
            child: Text(
              ":",
              overflow: TextOverflow.ellipsis,
              style:
                  ThemeConstands.texttheme.titleMedium?.copyWith(fontSize: 17),
            ),
          ),
          Flexible(
            flex: 2,
            child: Text(subTitle,
                overflow: TextOverflow.ellipsis,
                style: ThemeConstands.texttheme.titleMedium
                    ?.copyWith(fontWeight: FontWeight.w500, fontSize: 18)),
          )
        ],
      ),
    );
  }
}
