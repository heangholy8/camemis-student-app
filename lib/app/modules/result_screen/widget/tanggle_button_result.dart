import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class ToggleButtonResult extends StatefulWidget {
  final double width;
  final double height;
  
  final int isActive;

  final String leftDescription;
  final String rightDescription;
  final String centerleftDescription;
  final String centerrightDescription;

  final Color toggleColor;
  final Color toggleBackgroundColor;
  final Color toggleBorderColor;

  final Color inactiveTextColor;
  final Color activeTextColor;
  final double toggleXAlign;

  final double _leftToggleAlign = -1.12;
  final double _rightToggleAlign = 1.12;
  final double _centerleftToggleAlign = -0.40;
  final double _centerrightToggleAlign = 0.35;

  final VoidCallback onLeftToggleActive;
  final VoidCallback onRightToggleActive;
  final VoidCallback onCenterLeftToggleActive;
  final VoidCallback onCenterRightToggleActive;

  const ToggleButtonResult(
      {Key? key,
      required this.width,
      required this.height,
      required this.toggleBackgroundColor,
      required this.toggleBorderColor,
      required this.toggleColor,
      required this.activeTextColor,
      required this.inactiveTextColor,
      required this.leftDescription,
      required this.rightDescription,
      required this.onLeftToggleActive,
      required this.onRightToggleActive,
      required this.onCenterLeftToggleActive, 
      required this.onCenterRightToggleActive, 
      required this.centerleftDescription, 
      required this.centerrightDescription, 
      required this.isActive,
      required this.toggleXAlign,
      })
      : super(key: key);

  @override
  _ToggleButtonResultState createState() => _ToggleButtonResultState();
}

class _ToggleButtonResultState extends State<ToggleButtonResult> {

  @override
  void initState() {
    super.initState();

    // setState(() {
    //   if(widget.isActive==0){
    //     _toggleXAlign = -1;
    //     _leftDescriptionColor = widget.activeTextColor;
    //     _rightDescriptionColor = widget.inactiveTextColor;
    //     _centerleftDescriptionColor = widget.inactiveTextColor;
    //     _centerrightDescriptionColor = widget.inactiveTextColor;
    //   }
    //   else if(widget.isActive==1){
    //     _toggleXAlign = -0.30;
    //     _centerleftDescriptionColor = widget.activeTextColor;
    //     _rightDescriptionColor = widget.inactiveTextColor;
    //     _leftDescriptionColor = widget.inactiveTextColor;
    //     _centerrightDescriptionColor = widget.inactiveTextColor;
    //   }
    //   else if(widget.isActive==2){
    //     _toggleXAlign = 0.40;
    //     _leftDescriptionColor = widget.inactiveTextColor;
    //     _rightDescriptionColor = widget.inactiveTextColor;
    //     _centerleftDescriptionColor = widget.inactiveTextColor;
    //     _centerrightDescriptionColor = widget.activeTextColor;
    //   }
    // });
  }

  @override
  Widget build(BuildContext context) {
     final translate = context.locale.toString();
    return Container(
      width: widget.width,
      height: widget.height,
      decoration: BoxDecoration(
        color: widget.toggleBackgroundColor,
        borderRadius:const BorderRadius.all(
          Radius.circular(14.0),
        ),
      ),
      child: Stack(
        children: [
          AnimatedAlign(
            alignment: Alignment(widget.toggleXAlign, 0),
            duration:const Duration(milliseconds: 300),
            child: Container(
              width: widget.width/4,
              height: widget.height,
              decoration: BoxDecoration(
                color: widget.toggleColor,
                borderRadius:const BorderRadius.all(
                  Radius.circular(14.0),
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: widget.onLeftToggleActive,
          
            child: Align(
              alignment:const Alignment(-1.12, 0),
              child: Container(
                width: widget.width/4,
                color: Colors.transparent,
                alignment: Alignment.center,
                child: Text(
                  widget.leftDescription,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: widget.isActive==0? widget.activeTextColor:widget.inactiveTextColor,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap:widget.onCenterLeftToggleActive,
            child: Align(
              alignment:const Alignment(-0.40, 0),
              child: Container(
               width: widget.width/4,
                color: Colors.transparent,
                alignment: Alignment.center,
                child: Text(
                  widget.centerleftDescription,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: widget.isActive==1? widget.activeTextColor:widget.inactiveTextColor,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap:widget.onCenterRightToggleActive,
            child: Align(
              alignment:const Alignment(0.35, 0),
              child: Container(
                width: widget.width/4,
                color: Colors.transparent,
                child: Text(
                  widget.centerrightDescription,
                  textAlign:TextAlign.center,
                  style: TextStyle(
                      color: widget.isActive==2? widget.activeTextColor:widget.inactiveTextColor,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap:widget.onRightToggleActive,
            child: Align(
              alignment:const Alignment(1.12, 0),
              child: Container(
                width: widget.width/4,
                color: Colors.transparent,
                alignment: Alignment.center,
                child: Text(
                  widget.rightDescription,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: widget.isActive==3? widget.activeTextColor:widget.inactiveTextColor,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}