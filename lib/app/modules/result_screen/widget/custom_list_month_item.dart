import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';
import '../data/list_months_data.dart';

class ListMonthItem extends StatelessWidget {
  final int index;
  const ListMonthItem({
    Key? key,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: const Border(
            bottom: BorderSide(
                width: 1.5, color: Color.fromRGBO(244, 244, 244, 1))),
        color: listMonths[index].isSelected
            ? const Color.fromRGBO(241, 249, 255, 1)
            : Colors.white,
      ),
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(vertical: 10),
            margin:const EdgeInsets.only(top: 5),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  listMonths[index].name,
                  style: ThemeConstands.texttheme.headline6?.copyWith(
                      color: listMonths[index].isMonth
                          ? Colorconstands.primaryColor
                          : Colors.red),
                ),
                Icon(
                  listMonths[index].isSelected
                      ? Icons.check_circle
                      : Icons.circle_outlined,
                  color: Colorconstands.primaryColor,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
