import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';

import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class CustomHeaderRecommandetion extends StatelessWidget {
  const CustomHeaderRecommandetion({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        width: MediaQuery.of(context).size.width,
        color: Colorconstands.iconColor.withOpacity(0.1),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Container(
                child: Center(
                  child: Text(
                    "NO".tr(),
                    style: ThemeConstands.texttheme.subtitle1
                        ?.copyWith(fontWeight: FontWeight.w700),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: Container(
                padding:const EdgeInsets.only(left: 5),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "SUBJECT".tr(),
                    style: ThemeConstands.texttheme.subtitle1
                        ?.copyWith(fontWeight: FontWeight.w700),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                child: Text(
                  "TEACHER_COMMENTARY".tr(),
                  style: ThemeConstands.texttheme.subtitle1
                      ?.copyWith(fontWeight: FontWeight.w700),
                ),
              ),
            ),
          ],
        ));
  }
}
