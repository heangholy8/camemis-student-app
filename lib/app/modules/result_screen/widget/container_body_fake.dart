import 'package:flutter/cupertino.dart';

import '../../../../model/result/monthly_result.dart';
import 'custom_container_title_and_subTitle.dart';
import 'custom_contianer.dart';

class ContainerBodyFake extends StatefulWidget {
 const ContainerBodyFake({Key? key}) : super(key: key);

  @override
  State<ContainerBodyFake> createState() => _ContainerBodyFakeState();
}

class _ContainerBodyFakeState extends State<ContainerBodyFake> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const SizedBox(
          width: 15,
        ),
        Flexible(
          fit: FlexFit.tight,
          flex: 2,
          child: CustomContainer(
              child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              ContainTitleAndSubTitle(
                  title: "មធ្យមភាគប្រចាំខែ: ", subTitle: "55.55"),
              ContainTitleAndSubTitle(
                  title: "ពិន្ទុសរុបប្រចាំខែ: ", subTitle: "55.55"),
            ],
          )),
        ),
        const SizedBox(
          width: 10,
        ),
        Flexible(
          fit: FlexFit.tight,
          flex: 2,
          child: CustomContainer(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              ContainTitleAndSubTitleSmallContainer(
                  title: "និទ្ទេស:   ", subTitle: "55.55"),
              ContainTitleAndSubTitleSmallContainer(
                  title: "ចំណាត់ថ្នាក់:  ", subTitle: "55.55")
            ],
          )),
        ),
        const SizedBox(
          width: 15,
        ),
      ],
    );
  }
}
