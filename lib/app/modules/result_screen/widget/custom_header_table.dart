import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../core/themes/themes.dart';

class CustomTableHeader extends StatelessWidget {
  final String title;
  const CustomTableHeader({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return title == "លរ"
        ? Container(
            padding: const EdgeInsets.only(top: 10),
            width: MediaQuery.of(context).size.width / 15,
            child: Align(
                alignment: Alignment.centerLeft,
                child: Text(title, style: ThemeConstands.texttheme.subtitle2)))
        : title == "មុខវិទ្យា"
            ? Container(
                width: MediaQuery.of(context).size.width / 5,
                padding: const EdgeInsets.only(top: 10),
                child: Align(
                    alignment: Alignment.centerLeft,
                    child:
                        Text(title, style: ThemeConstands.texttheme.subtitle2)))
            : title == "ចំណាត់ថ្នាក់"
                ? Container(
                    width: MediaQuery.of(context).size.width / 6,
                    padding: const EdgeInsets.only(top: 10, left: 0),
                    child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(title,
                            style: ThemeConstands.texttheme.subtitle2)))
                : Container(
                    padding: const EdgeInsets.only(top: 10),
                    width: MediaQuery.of(context).size.width / 8,
                    child: Center(
                        child: Text(title,
                            style: ThemeConstands.texttheme.subtitle2)));
  }
}
