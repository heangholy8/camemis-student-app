import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../core/themes/themes.dart';

class CustomBottomSheetMonthHeader extends StatelessWidget {
  final String title;
  const CustomBottomSheetMonthHeader({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding:const EdgeInsets.only(bottom: 20),
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
            blurRadius: 4,
            color: Color.fromRGBO(222, 222, 222, 0.25),
            offset: Offset(0, 4))
      ]),
      child: Column(children: [
        Text(
          title,
          style: ThemeConstands.texttheme.headline5,
        ),
        Text(
          "សម្រាបឆ្នាំសិក្សា ២០២២-២០២៣",
          style: ThemeConstands.texttheme.bodyLarge,
        ),
      ]),
    );
  }
}
