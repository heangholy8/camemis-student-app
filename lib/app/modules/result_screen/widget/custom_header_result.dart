import 'package:flutter/cupertino.dart';

import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class HeaderResult extends StatelessWidget {
  final String title;
  const HeaderResult({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:const EdgeInsets.symmetric(vertical: 15),
      padding:const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      width: MediaQuery.of(context).size.width,
      color: Colorconstands.darkGray.withOpacity(0.1),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text(
          title,
          textAlign: TextAlign.left,
          style: ThemeConstands.texttheme.titleLarge
              ?.copyWith(fontWeight: FontWeight.w600),
        ),
      ),
    );
  }
}
