// ignore_for_file: avoid_print

import 'package:camis_application_flutter/app/modules/online_screen/online_learning_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class CustomLessonCard extends StatelessWidget {
  final String? date;
  final String? tiemline;
  final String? description;
  final int? type;
  final String? category;
  final String? title;
  final GestureTapCallback? onTap;
  const CustomLessonCard({
    Key? key,
    this.date,
    this.tiemline,
    this.description,
    this.type,
    this.title,
    this.onTap, 
    this.category,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    return Container(
      width: width,
      margin: const EdgeInsets.only(bottom: 10, right: 2.0),
      decoration: BoxDecoration(
          color: const Color(0xFFF1F9FF),
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(8.0),
            bottomLeft: Radius.circular(8.0),
            topRight: Radius.circular(12),
            bottomRight: Radius.circular(12),
          ),
          boxShadow: [
            BoxShadow(
              offset: const Offset(1, 2),
              color: Colors.black.withOpacity(.1),
              blurRadius: 4,
            ),
          ]),
      child: Stack(
        children: [
          Positioned(
            top: 0,
            bottom: 0,
            right: width / 4,
            child: type == 2
                ? SvgPicture.asset(
                    "assets/icons/learning_icon.svg",
                    color: const Color(0xFF9CB6DB),
                  )
                : type == 1
                    ? SvgPicture.asset(
                        "assets/icons/lesson_icon.svg",
                        color: const Color(0xFF9CB6DB),
                      )
                    : SvgPicture.asset(
                        "assets/icons/online_icon.svg",
                        color: const Color(0xFF9CB6DB),
                      ),
          ),
          Positioned(
            top: .2,
            right: .2,
            child: Container(
              padding: const EdgeInsets.symmetric(
                vertical: 8.0,
                horizontal: 18.0,
              ),
              decoration: BoxDecoration(
                color: type == 2
                    ? const Color(0xFF1ABC9C)
                    : type == 1
                        ? const Color(0xFF4B7BEC)
                        : const Color(0xFF00A8FF),
                borderRadius: const BorderRadius.only(
                  topRight: Radius.circular(
                    8,
                  ),
                  bottomLeft: Radius.circular(
                    8,
                  ),
                ),
              ),
              child: Text(
                category.toString(),
                style: ThemeConstands.texttheme.bodyText2!.copyWith(
                  color: Colors.white,
                ),
              ),
            ),
          ),
          SizedBox(
            width: width,
            child: MaterialButton(
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8.0),
                  bottomLeft: Radius.circular(8.0),
                  topRight: Radius.circular(12),
                  bottomRight: Radius.circular(12),
                ),
              ),
              padding: const EdgeInsets.all(0.0),
              onPressed: onTap ??
                  () {
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(
                    //       builder: (context) =>  OnlineLiveScreen(idPost:  data[index].id.toString(),)),
                    // );
                  },
              child: Stack(
                children: [
                  Positioned(
                    top: 0,
                    bottom: 0,
                    left: 0,
                    child: Container(
                      width: 5,
                      padding: const EdgeInsets.symmetric(
                        horizontal: 2,
                      ),
                      decoration: BoxDecoration(
                        color: type == 2
                            ? const Color(0xFF1ABC9C)
                            : type == 1
                                ? const Color(0xFF4B7BEC)
                                : const Color(0xFF00A8FF),
                        borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(12.0),
                          bottomLeft: Radius.circular(12.0),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        left: 20, right: 10, top: 12, bottom: 12),
                    child: Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                title ?? "[Assignment title]",
                                style:
                                    ThemeConstands.texttheme.subtitle1!.copyWith(
                                  color: Colorconstands.primaryColor,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              const SizedBox(
                                height: 12.0,
                              ),
                              RichText(
                                text: TextSpan(
                                  text: "Date: ",
                                  style: ThemeConstands.texttheme.bodyText2!
                                      .copyWith(
                                    color: Colorconstands.darkGray,
                                  ),
                                  children: [
                                    TextSpan(
                                      text: date,
                                      style: ThemeConstands.texttheme.caption!
                                          .copyWith(
                                        fontWeight: FontWeight.w600,
                                        color: Colorconstands.darkGray,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(
                                height: 8.0,
                              ),
                              RichText(
                                text: TextSpan(
                                  text: "Timeline: ",
                                  style: ThemeConstands.texttheme.bodyText2!
                                      .copyWith(
                                    color: Colorconstands.darkGray,
                                  ),
                                  children: [
                                    TextSpan(
                                      text: tiemline,
                                      style: ThemeConstands.texttheme.caption!
                                          .copyWith(
                                        fontWeight: FontWeight.w600,
                                        color: Colorconstands.darkGray,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(
                                height: 8.0,
                              ),
                              RichText(
                                text: TextSpan(
                                  text: "Description: ",
                                  style: ThemeConstands.texttheme.bodyText2!
                                      .copyWith(
                                    color: Colorconstands.darkGray,
                                  ),
                                  children: [
                                    TextSpan(
                                      text: description,
                                      style: ThemeConstands.texttheme.caption!
                                          .copyWith(
                                        fontWeight: FontWeight.w600,
                                        color: Colorconstands.darkGray,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(right: 18.0),
                          child: const Icon(
                            CupertinoIcons.chevron_right,
                            size: 24,
                            color: Colorconstands.primaryColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
