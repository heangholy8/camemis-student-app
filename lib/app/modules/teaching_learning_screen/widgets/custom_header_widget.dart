// ignore_for_file: avoid_print

import 'package:flutter/material.dart';

import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class CustomHeaderWidget extends StatelessWidget {
  final String? title;
  final String? date;
  final String? timeline;
  final String? description;
  final GestureTapCallback? onPressed;
  const CustomHeaderWidget({
    Key? key,
    this.title,
    this.date,
    this.timeline,
    this.description,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 18,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title ?? "Khmer Construction",
                style: ThemeConstands.texttheme.headline6!.copyWith(
                  fontWeight: FontWeight.w600,
                  color: Colors.black,
                ),
              ),
              const SizedBox(
                height: 12.0,
              ),
              RichText(
                text: TextSpan(
                  text: "Date: ",
                  style: ThemeConstands.texttheme.bodyText2!.copyWith(
                    fontWeight: FontWeight.w600,
                    color: Colorconstands.darkGray,
                  ),
                  children: [
                    TextSpan(
                      text: date ?? "02-May-2022 ",
                      style: ThemeConstands.texttheme.caption!.copyWith(
                        color: Colorconstands.darkGray,
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 8.0,
              ),
              RichText(
                text: TextSpan(
                  text: "Timeline: ",
                  style: ThemeConstands.texttheme.bodyText2!.copyWith(
                    fontWeight: FontWeight.w600,
                    color: Colorconstands.darkGray,
                  ),
                  children: [
                    TextSpan(
                      text: timeline ?? "03:30-05:00 PM",
                      style: ThemeConstands.texttheme.caption!.copyWith(
                        color: Colorconstands.darkGray,
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 8.0,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Description: ",
                    style: ThemeConstands.texttheme.bodyText2!.copyWith(
                      fontWeight: FontWeight.w600,
                      color: Colorconstands.darkGray,
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width - 110,
                    child: Text(
                      description.toString(),
                      style: ThemeConstands.texttheme.bodyText2!.copyWith(
                        color: Colorconstands.darkGray,
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 8.0,
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.only(right: 18.0),
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(4.0),
            ),
          ),
          child: MaterialButton(
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(4.0),
              ),
            ),
            minWidth: 30,
            padding: const EdgeInsets.all(0.0),
            color: Colors.red,
            onPressed: onPressed ??
                () {
                  print("Live click");
                },
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 8.0,
                vertical: 6.0,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const Icon(
                    Icons.circle,
                    size: 8.0,
                    color: Colors.white,
                  ),
                  const SizedBox(
                    width: 4,
                  ),
                  Text(
                    "LIVE",
                    style: ThemeConstands.texttheme.bodyText2!.copyWith(
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
     
      ],
    );
  }
}
