import '../../../routes/e.route.dart';

class CustomTextFieldSenderWidget extends StatelessWidget {
  final Widget? suffixIcon;
  final bool? autofocus;
  final FocusNode? commentNode;
  final String? hintText;
  final bool isReply;
  final String mentionName;
  final Function()? onTapcancel;

  final TextEditingController? controller;
  const CustomTextFieldSenderWidget({
    Key? key,
    this.suffixIcon,
    this.autofocus,
    this.commentNode,
    this.controller,
    this.hintText,
    required this.isReply,
    required this.mentionName,
    this.onTapcancel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 18.0),
      margin: const EdgeInsets.only(top: 8.0),
      height: 45,
      child: TextFormField(
        autofocus: true,
        textAlignVertical: TextAlignVertical.center,
        controller: controller,
        enabled: true,
        enableInteractiveSelection: true,
        readOnly: false,
        toolbarOptions: const ToolbarOptions(
            paste: true, cut: true, selectAll: true, copy: true),
        focusNode: commentNode,
        scrollPhysics: const BouncingScrollPhysics(),
        keyboardType: TextInputType.text,
        decoration: isReply == true
            ? InputDecoration(
                prefixIcon: Stack(
                  alignment: Alignment.center,
                  children: [
                    Positioned(
                      right: 2,
                      top: 5,
                      child: InkWell(
                        onTap: onTapcancel,
                        child: const Icon(
                          Icons.cancel,
                          size: 14.0,
                        ),
                      ),
                    ),
                    AnimatedContainer(
                      duration: const Duration(milliseconds: 300),
                      margin: const EdgeInsets.symmetric(horizontal: 5.0),
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colorconstands.primaryColor.withOpacity(0.5),
                            borderRadius: BorderRadius.circular(4)),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 8, vertical: 2.0),
                          child: Text(
                            mentionName.toString(),
                            overflow: TextOverflow.ellipsis,
                            style: ThemeConstands.texttheme.bodyText2!.copyWith(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                hintText: hintText ?? "Comment.....",
                hintStyle: const TextStyle(),
                fillColor: Colors.white,
                filled: true,
                contentPadding:
                    const EdgeInsets.symmetric(vertical: 0, horizontal: 10.0),
                suffixIcon: suffixIcon,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12.0),
                  borderSide: const BorderSide(),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12.0),
                  borderSide: const BorderSide(
                    color: Colors.white,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12.0),
                  borderSide: BorderSide(
                    color: const Color(0xFFDADADA).withOpacity(.7),
                  ),
                ),
              )
            : InputDecoration(
                hintText: hintText ?? "Comment....",
                hintStyle: const TextStyle(),
                fillColor: Colors.white,
                filled: true,
                contentPadding:
                    const EdgeInsets.symmetric(vertical: 0, horizontal: 10.0),
                suffixIcon: suffixIcon,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12.0),
                  borderSide: const BorderSide(),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12.0),
                  borderSide: const BorderSide(
                    color: Colors.white,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12.0),
                  borderSide: BorderSide(
                    color: const Color(0xFFDADADA).withOpacity(.7),
                  ),
                ),
              ),
      ),
    );
  }
}
