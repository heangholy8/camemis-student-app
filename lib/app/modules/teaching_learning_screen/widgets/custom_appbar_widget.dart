import 'package:camis_application_flutter/component/change_child/change_child.dart';
import 'package:flutter/material.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class CustomAppbarWidget extends StatelessWidget {
  final String? title;
  final bool? avatar;
  const CustomAppbarWidget({
    Key? key,
    this.title,
    this.avatar = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return 
    Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            width: width / 1.5,
            height: 60,
            child: Row(
              children: [
                Container(
                  margin: const EdgeInsets.only(left: 8),
                  child: IconButton(
                    icon: const Icon(
                      Icons.arrow_back_ios_new_outlined,
                      size: 25,
                      color: Colorconstands.white,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
                const SizedBox(
                  width: 4.0,
                ),
                Expanded(
                  child: Text(
                    title.toString(),
                    style: ThemeConstands.texttheme.headline5!.copyWith(
                      color: Colors.white,
                      fontWeight: FontWeight.w400,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ],
            ),
          ),
          avatar == true
              ? Container(
                  margin: const EdgeInsets.symmetric(vertical: 15),
                  child: const ChangeChildren(),
                )
              : const SizedBox(),
        ],
      ),
    );
  
  }
}
