import 'package:comment_tree/data/comment.dart';
import 'package:comment_tree/widgets/comment_tree_widget.dart';
import 'package:comment_tree/widgets/tree_theme_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';
import '../../homework_screen/bloc/homework_bloc.dart';

class TabForum extends StatefulWidget {
  final GestureTapCallback? focus;
  const TabForum({
    Key? key,
    required this.focus,
  }) : super(key: key);

  @override
  State<TabForum> createState() => _TabForumState();
}

class _TabForumState extends State<TabForum> {
  bool _isLike = false;
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: BlocBuilder<HomeworkBloc, HomeworkState>(
        builder: (context, state) {
          if (state is HomeWorkLoadingState) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is OnlinelearningLoadedState) {
            var forum = state.streamingModel.data!.forums;
            return Container(
              color: Colors.white,
              padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 16),
              child: ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: forum?.length,
                itemBuilder: (context, index) {
                  return CommentTreeWidget<Comment, Comment>(
                    Comment(
                      avatar: 'null',
                      userName: 'sinat',
                      content: "${forum?[index].comment}",
                    ),
                    List.generate(
                      0,
                      (index) => Comment(
                          avatar: 'null',
                          userName: 'null',
                          content: '${forum?[index].comment}'),
                    ),
                    treeThemeData: TreeThemeData(
                        lineColor: forum!.isEmpty
                            ? const Color(0xFFDEDEDE)
                            : Colors.white,
                        lineWidth: 1.5),
                    avatarRoot: (context, data) => PreferredSize(
                      child: InkWell(
                        onTap: widget.focus ??
                            () {
                              print("Profile");
                            },
                        child: CircleAvatar(
                          radius: 24,
                          backgroundImage: NetworkImage(
                              "${forum[index].userProfile?.profileMedia?.fileThumbnail}"),
                        ),
                      ),
                      preferredSize: const Size.fromRadius(18),
                    ),
                    avatarChild: (context, data) => PreferredSize(
                      child: CircleAvatar(
                        radius: 18,
                        backgroundImage: NetworkImage(
                            '${forum[index].userProfile?.profileMedia?.fileThumbnail}'),
                      ),
                      preferredSize: const Size.fromRadius(12),
                    ),
                    contentChild: (context, data) {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 14, horizontal: 14),
                            decoration: BoxDecoration(
                                color: Colors.grey[100],
                                borderRadius: BorderRadius.circular(12)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  '${forum[index].userProfile?.nameEn}',
                                  style: ThemeConstands.texttheme.subtitle1!
                                      .copyWith(
                                    fontWeight: FontWeight.w600,
                                    color: Colors.black,
                                  ),
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  '${data.content}',
                                  style: ThemeConstands.texttheme.bodyText1
                                      ?.copyWith(
                                    fontWeight: FontWeight.w500,
                                    color: Colorconstands.darkGray,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          DefaultTextStyle(
                            style: Theme.of(context)
                                .textTheme
                                .caption!
                                .copyWith(
                                    color: Colors.grey[700],
                                    fontWeight: FontWeight.bold),
                            child: Padding(
                              padding: const EdgeInsets.only(top: 16),
                              child: Row(
                                children: [
                                  const SizedBox(
                                    width: 8,
                                  ),
                                  InkWell(
                                      onTap: () {
                                        print("Like");
                                      },
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          SvgPicture.asset(
                                            "assets/icons/fi_like_icon.svg",
                                            width: 12,
                                          ),
                                          const SizedBox(
                                            width: 6,
                                          ),
                                          Text(
                                            'Like',
                                            style: ThemeConstands
                                                .texttheme.bodyText2!
                                                .copyWith(
                                              fontWeight: FontWeight.w600,
                                              color: Colorconstands.darkGray,
                                            ),
                                          ),
                                        ],
                                      )),
                                  const SizedBox(
                                    width: 24,
                                  ),
                                  InkWell(
                                    onTap: widget.focus ??
                                        () {
                                          print("Comment");
                                        },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        SvgPicture.asset(
                                          "assets/icons/fi_reply_icon.svg",
                                          width: 12,
                                        ),
                                        const SizedBox(
                                          width: 6,
                                        ),
                                        Text(
                                          'Reply',
                                          style: ThemeConstands
                                              .texttheme.bodyText2!
                                              .copyWith(
                                            fontWeight: FontWeight.w600,
                                            color: Colorconstands.darkGray,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      );
                    },
                    contentRoot: (context, data) {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 8),
                            decoration: BoxDecoration(
                                color: Colors.grey[100],
                                borderRadius: BorderRadius.circular(12)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  '${forum[index].userProfile?.nameEn}',
                                  style: ThemeConstands.texttheme.subtitle1!
                                      .copyWith(
                                          fontWeight: FontWeight.w600,
                                          color: Colors.black),
                                ),
                                const SizedBox(
                                  height: 4,
                                ),
                                Text(
                                  '${data.content}',
                                  style: ThemeConstands.texttheme.bodyText1
                                      ?.copyWith(
                                    fontWeight: FontWeight.w500,
                                    color: Colorconstands.darkGray,
                                  ),
                                ),
                              ],
                            ),
                          ),

                          /// LIKE AND REPLY BUTTON
                          DefaultTextStyle(
                            style: Theme.of(context)
                                .textTheme
                                .caption!
                                .copyWith(
                                    color: Colors.grey[700],
                                    fontWeight: FontWeight.bold),
                            child: Padding(
                              padding: const EdgeInsets.only(top: 6),
                              child: Row(
                                children: [
                                  const SizedBox(
                                    width: 8,
                                  ),
                                  InkWell(
                                    onTap: () {
                                      print("Like");
                                      setState(() {
                                        _isLike = !_isLike;
                                      });
                                    },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        SvgPicture.asset(
                                          "assets/icons/fi_like_icon.svg",
                                          width: 12,
                                          color: !_isLike
                                              ? Colorconstands.darkGray
                                              : Colorconstands.primaryColor,
                                        ),
                                        const SizedBox(
                                          width: 6,
                                        ),
                                        Text(
                                          'Like',
                                          style: ThemeConstands
                                              .texttheme.bodyText2!
                                              .copyWith(
                                            fontWeight: FontWeight.w600,
                                            color: !_isLike
                                                ? Colorconstands.darkGray
                                                : Colorconstands.primaryColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 24,
                                  ),
                                  InkWell(
                                    onTap: widget.focus ??
                                        () {
                                          print("Comment");
                                        },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        SvgPicture.asset(
                                          "assets/icons/fi_reply_icon.svg",
                                          width: 12,
                                        ),
                                        const SizedBox(
                                          width: 6,
                                        ),
                                        Text(
                                          'Reply',
                                          style: ThemeConstands
                                              .texttheme.bodyText2!
                                              .copyWith(
                                            fontWeight: FontWeight.w600,
                                            color: Colorconstands.darkGray,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 18,
                          ),
                        ],
                      );
                    },
                  );
                },
              ),
            );
          }
          return Container();
        },
      ),
    );
  }
}
