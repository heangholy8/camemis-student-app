import 'package:camis_application_flutter/app/modules/auth_screens/e_school_code.dart';
import 'package:camis_application_flutter/app/modules/homework_screen/components/shimmer_widget.dart';
import 'package:camis_application_flutter/app/modules/teaching_learning_screen/bloc/teaching_bloc.dart';
import 'package:camis_application_flutter/widgets/shimmer_style.dart';
import '../../../../component/dropdown_month/bloc/monthly_bloc.dart';

class CustomeListSubject extends StatefulWidget {
  final int subjectLenght;
  const CustomeListSubject({Key? key, required this.subjectLenght})
      : super(key: key);

  @override
  State<CustomeListSubject> createState() => _CustomeListSubjectState();
}

class _CustomeListSubjectState extends State<CustomeListSubject>
    with TickerProviderStateMixin {
  late TabController _monthlyController;

  int seletedIndex = 0;

  @override
  void initState() {
    // getListSubjectChild();
    _monthlyController =
        TabController(length: widget.subjectLenght, vsync: this);
    _monthlyController.addListener(() {
      setState(() {
        seletedIndex = _monthlyController.index;
      });
    });
    super.initState();
  }

  // void getListSubjectChild() async {
  //   final GetStoragePref _pref = GetStoragePref();
  //   var listSubject = await _pref.getSubjectLenght();
  //   setState(() {
  //     subjectLen = listSubject!;
  //     print("sinat subject list" + subjectLen.toString());
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: widget.subjectLenght,
      initialIndex: 0,
      child: BlocBuilder<MonthlyBloc, MonthlyState>(
        builder: (context, state) {
          if (state is MonthlyLoadingState) {
            return Center(
              child: ShimmerWidget(
                child: Container(
                  decoration: const BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.all(
                      Radius.circular(18.0),
                    ),
                  ),
                  margin: const EdgeInsets.symmetric(horizontal: 8.0),
                  width: MediaQuery.of(context).size.width,
                  height: 60,
                ),
              ),
            );
          }
          if (state is SpecificChildLoadedState) {
            
            var data = state.specificChildModel?.data?.listSubjects;
            return SizedBox(
              height: 50,
              child: TabBar(
                labelPadding: const EdgeInsets.symmetric(horizontal: 8.0),
                labelColor: Colors.white,
                unselectedLabelColor: Colors.black,
                isScrollable: true,
                controller: _monthlyController,
                labelStyle: const TextStyle(
                  fontFamily: "battambang",
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                ),
                tabs: List.generate(
                  widget.subjectLenght,
                  (index) => Tab(
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 18.0, vertical: 8.0),
                      decoration: BoxDecoration(
                        color: seletedIndex == index
                            ? Colorconstands.primaryColor
                            : const Color(0xFFF1F9FF),
                        borderRadius: BorderRadius.circular(8.0),
                        boxShadow: [
                          BoxShadow(
                            offset: const Offset(0, 0.5),
                            color: Colors.black.withOpacity(0.2),
                            blurRadius: 1,
                          ),
                        ],
                      ),
                      child: Text(
                        data![index].name.toString(),
                      ),
                    ),
                  ),
                ),
                onTap: (value) {
                  BlocProvider.of<TeachingBloc>(context).add(
                    GetTeachingEvent(
                      subjectID:
                          "${state.specificChildModel!.data?.listSubjects![value].id}",
                      classID: "${state.specificChildModel?.data?.classId}",
                    ),
                  );
                },
              ),
            );
          }
          return Container();
        },
      ),
    );
  }
}
