import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:flutter/material.dart';

class TabAttendanceRecord extends StatelessWidget {
  const TabAttendanceRecord({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      color: Colors.white,
      child:
       ListView.separated(
        itemBuilder: (context, index) {
          return SizedBox(
            height: 50,
            width: width,
            child: ListTile(
              leading: Container(
                height: double.infinity,
                width: 50,
                decoration: const BoxDecoration(
                  color: Colors.black,
                  image: DecorationImage(
                    image: AssetImage(
                      "assets/images/profile/comment_profile.png",
                    ),
                  ),
                  shape: BoxShape.circle,
                ),
              ),
              title: Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Text(
                  "Keav Visal",
                  style: ThemeConstands.texttheme.subtitle1!.copyWith(
                    color: Colorconstands.darkGray,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              subtitle: Text(
                "03:30-05:00 PM",
                style: ThemeConstands.texttheme.bodyText2!.copyWith(
                  color: Colorconstands.darkGray.withOpacity(.6),
                  fontWeight: FontWeight.w500,
                ),
              ),
              trailing: Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                decoration: const BoxDecoration(
                  color: Color(0xFF68BA6B),
                  borderRadius: BorderRadius.all(
                    Radius.circular(5.0),
                  ),
                ),
                child: Text(
                  "Join",
                  style: ThemeConstands.texttheme.bodyText1!.copyWith(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          );
        },
        separatorBuilder: (context, index) {
          return Container(
            padding: const EdgeInsets.only(
              left: 60.0,
              top: 14,
              bottom: 4,
              right: 20,
            ),
            child: const Divider(),
          );
        },
        itemCount: 5,
      ),
   
    );
  }
}
