import 'package:camis_application_flutter/app/modules/homework_screen/bloc/homework_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../core/themes/themes.dart';
import '../../homework_screen/view/homework_screen.dart';
import '../../lesson_screen/lesson_screen.dart';
import '../bloc/teaching_bloc.dart';
import '../../online_screen/online_learning_screen.dart';
import '../widgets/custom_lesson_card.dart';

class TabHistoryScreen extends StatelessWidget {
  const TabHistoryScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: BlocBuilder<TeachingBloc, TeachingState>(
        builder: (context, state) {
          if (state is TeachingLoadingState) {
            return Container(
              height: MediaQuery.of(context).size.height / 3,
              width: MediaQuery.of(context).size.width,
              alignment: Alignment.center,
              child: const CircularProgressIndicator(),
            );
          }
          if (state is TeachingLoadedState) {
            var data = state.teachingModel?.data?.history;
            if (data!.isNotEmpty) {
              return Column(
                children: List.generate(
                  data.length,
                  (index) => CustomLessonCard(
                    onTap: () {
                      // Homework Screen
                      if (data[index].type == 2) {
                        print(
                            "sinataaklsjdfalsdf " + data[index].id.toString());
                        BlocProvider.of<HomeworkBloc>(context).add(
                          GetHomeWorkTeachingEvent(
                            homeWorkId: "${data[index].id}",
                          ),
                        );
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => HomeWorkScreen(
                              idPost: data[index].id.toString(),
                            ),
                          ),
                        );
                      }
                      // Lesson Screen
                      else if (data[index].type == 1) {
                        print("sinataaklsjdfalsdf" + data[index].id.toString());
                        BlocProvider.of<HomeworkBloc>(context).add(
                          GetLessonEvent(lessonId: data[index].id.toString()),
                        );
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => LessonScreen(
                              idPost: data[index].id.toString(),
                            ),
                          ),
                        );
                      }
                      // Online Learning
                      else {
                        print("sinataaklsjdfalsdf" + data[index].id.toString());
                        BlocProvider.of<HomeworkBloc>(context).add(
                          GetOnlineLearningEvent(
                            liveId: data[index].id.toString(),
                          ),
                        );
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => OnlineLiveScreen(
                              idPost: data[index].id.toString(),
                            ),
                          ),
                        );
                      }
                    },
                    title: data[index].name,
                    date: data[index].createdDate,
                    tiemline: data[index].startDate,
                    description: data[index].description,
                    category: data[index].category,
                    type: data[index].type,
                  ),
                ),
              );
            } else {
              return Container(
                height: MediaQuery.of(context).size.height / 3,
                width: MediaQuery.of(context).size.width,
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Image.asset(
                      "assets/images/gifs/empty_data.gif",
                      width: MediaQuery.of(context).size.width / 1.8,
                    ),
                    const SizedBox(
                      height: 28,
                    ),
                    Text(
                      "Empty History",
                      style: ThemeConstands.texttheme.headline6!
                          .copyWith(fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              );
            }
          } else {
            return Container(
              height: MediaQuery.of(context).size.height / 3,
              width: MediaQuery.of(context).size.width,
              alignment: Alignment.center,
              child: const Text("Somethign when wrong"),
            );
          }
        },
      ),
    );
  }
}
