
class TeachingModel {
  final String? id;
  final String? date;
  final String? tiemline;
  final String? description;
  final String? type;
  final String? title;

  TeachingModel(
      {this.id,
      this.title,
      this.date,
      this.tiemline,
      this.description,
      this.type});
}

List<TeachingModel> teachData = [
  TeachingModel(
    id: "1",
    title: "Khmer Construction",
    date: "02-May-2022",
    tiemline: "03:30-05:00 PM",
    description: "",
    type: "online",
  ),
  TeachingModel(
    id: "2",
    title: "Khmer Construction",
    date: "02-May-2022",
    tiemline: "03:30-05:00 PM",
    description: "",
    type: "lesson",
  ),
  TeachingModel(
    id: "3",
    title: "Khmer Construction",
    date: "02-May-2022",
    tiemline: "03:30-05:00 PM",
    description: "",
    type: "assignment",
  ),
];

List<String> subjectTab = const [
 'ភាសាខ្មែរ',
    'គណិតវិទ្យា',
    'រូបវិទ្យា',
    'គីមីវិទ្យា',
    'ប្រវត្តិវិទ្យា',
    'ភូមិវិទ្យា',
    'សីលធម៍',
    'ផែនដីវិទ្យា',
    'ភាសាអង់​គ្លេស',
    'ជីវវិទ្យា',
    'សេដ្ឋកិច្ច'
];
