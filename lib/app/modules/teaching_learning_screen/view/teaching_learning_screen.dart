import 'dart:async';

import 'package:camis_application_flutter/component/dropdown_month/bloc/monthly_bloc.dart';
import 'package:camis_application_flutter/storages/get_storage.dart';
import 'package:camis_application_flutter/widgets/custom_header_with_image.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../component/change_child/bloc/change_child_bloc.dart';
import '../../../../helper/timer_helper.dart';
import '../../../../storages/save_storage.dart';
import '../../../../widgets/header_change_child.dart';
import '../../../../widgets/model_bottomSheet.dart';
import '../../../../widgets/profile_widget.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';
import '../../attendance_screen.dart/model/absent_request_model.dart';
import '../../home_screen/view/home_screen.dart';
import '../../homework_screen/components/shimmer_widget.dart';
import '../bloc/teaching_bloc.dart';
import '../components/tab_history.dart';
import '../components/tab_ongoing.dart';
import 'list_month.dart';

class TeachingLearningScreen extends StatefulWidget {
  const TeachingLearningScreen({Key? key}) : super(key: key);
  @override
  State<TeachingLearningScreen> createState() => _TeachingLearningScreenState();
}

class _TeachingLearningScreenState extends State<TeachingLearningScreen>
    with TickerProviderStateMixin {
  final SaveStoragePref _savePref = SaveStoragePref();

  List<Tab> tabs = <Tab>[
    const Tab(
      text: 'onGoing',
    ),
    const Tab(text: 'History'),
  ];
  int seletedIndex = 0;
  String studentID = "";
  String classID = "";
  String firstSubject = "";
  int? selectChild;
  int? lenghtSubject;
  late TabController _tabController;
  late TabController _subjectController;
  int selectSubject = 0;
  String currentYear = TimerHepler().getCurrentYear;
  int? monthselected;
  int? monthlocal;
  String? monthname;
  StreamSubscription? stsub;
  bool connection = true;
  @override
  void initState() {
     //=============Check internet====================
    stsub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
      });
    });
    for (var element in monthdata) {
      element.isSelect = false;
    }
    monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
    monthselected = monthlocal;
    monthname = DateFormat("").format(DateTime.now());
    getlocaldataModel();
    _tabController = TabController(length: tabs.length, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    stsub?.cancel();
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: WillPopScope(
        onWillPop: ()async{
          backScreen();
          return true;
        },
        child: SizedBox(
          height: height,
          width: width,
          child: Padding(
            padding: const EdgeInsets.only(top: 26),
            child: Column(
              children: [
                Column(
                  children: [
                    //---------------------------Header------------------------------
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 15.0),
                      child: CustomHeaderWithImage(
                        title: "Teaching And Learning",
                        changechild:
                            BlocBuilder<ChangeChildBloc, ChangeChildState>(
                                builder: (context, state) {
                          if (state is ChangeChildLoadingState) {
                            return _buildLoading();
                          }
                          if (state is ChangeChildLoadState) {
                            var data = state.childrenModel!.data!;
                            return Profile(
                              height: 60,
                              width: 60,
                              pandding: 1.5,
                              namechild: '',
                              sizetextname: 17.0,
                              onPressed: () {
                                showModalBottomSheet(
                                  backgroundColor: Colors.transparent,
                                  isScrollControlled: true,
                                  context: context,
                                  isDismissible: true,
                                  builder: (context) => BuildBottomSheet(
                                    initialChildSize: 0.5,
                                    child: const HeaderChangeChild(),
                                    expanded: Container(
                                      color: Colorconstands.white,
                                      child: ListView.builder(
                                          physics:
                                              const NeverScrollableScrollPhysics(),
                                          shrinkWrap: true,
                                          padding: const EdgeInsets.all(0),
                                          itemCount: data.length,
                                          itemBuilder: (context, index) {
                                            studentID = data[index].id.toString();
                                            classID = data[index]
                                                .currentClass!
                                                .classId
                                                .toString();
                                            return Container(
                                              alignment: Alignment.center,
                                              decoration: BoxDecoration(
                                                  color: selectChild == index
                                                      ? Colorconstands
                                                          .primaryBackgroundColor
                                                      : Colorconstands.white,
                                                  border: Border(
                                                      bottom: BorderSide(
                                                    color: Colorconstands.darkGray
                                                        .withOpacity(0.1),
                                                    width: 1,
                                                  ))),
                                              height: 85,
                                              child: ListTile(
                                                leading: SizedBox(
                                                  height: 50,
                                                  width: 50,
                                                  child: ClipOval(
                                                      child: FadeInImage
                                                          .assetNetwork(
                                                    fit: BoxFit.cover,
                                                    image: data[index]
                                                                .profileMedia!
                                                                .fileThumbnail ==
                                                            null
                                                        ? data[index]
                                                            .profileMedia!
                                                            .fileShow
                                                            .toString()
                                                        : data[index]
                                                            .profileMedia!
                                                            .fileThumbnail
                                                            .toString(),
                                                    placeholder:
                                                        'assets/images/gifs/loading.gif',
                                                  )),
                                                ),
                                                title: Text(
                                                  translate=="en"?data[index].nameEn.toString()==""?data[index].name.toString()
                                                  :data[index].nameEn.toString():data[index].name.toString(),
                                                  style: ThemeConstands
                                                      .texttheme.headline6!
                                                      .copyWith(
                                                          fontWeight:
                                                              selectChild == index
                                                                  ? FontWeight
                                                                      .bold
                                                                  : FontWeight
                                                                      .normal),
                                                ),
                                                subtitle: Text(
                                                  "CLASS".tr()+": " +
                                                      data[index]
                                                          .currentClass!
                                                          .className
                                                          .toString(),
                                                  style: ThemeConstands
                                                      .texttheme.subtitle1!
                                                      .copyWith(),
                                                ),
                                                trailing: selectChild == index
                                                    ? const Icon(
                                                        Icons
                                                            .check_circle_outline_sharp,
                                                        color: Colorconstands
                                                            .secondaryColor,
                                                      )
                                                    : Container(
                                                        decoration: BoxDecoration(
                                                            color: Colorconstands
                                                                .secondaryColor,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        12)),
                                                        padding: const EdgeInsets
                                                                .symmetric(
                                                            horizontal: 15,
                                                            vertical: 5),
                                                        child: Text(
                                                          'Choose',
                                                          style: ThemeConstands
                                                              .texttheme
                                                              .subtitle2!
                                                              .copyWith(
                                                                  color:
                                                                      Colorconstands
                                                                          .white),
                                                        ),
                                                      ),
                                                onTap: () {
                                                  setState(() {
                                                    seletedIndex = 0;
                                                    studentID =
                                                        data[index].id.toString();
                                                    classID = data[index]
                                                        .currentClass!
                                                        .classId
                                                        .toString();
                                                    Navigator.of(context).pop();
                                                    firstSubject = data[index]
                                                        .currentClass!
                                                        .listSubjects![0]
                                                        .id
                                                        .toString();
                                                    classID = data[index]
                                                        .currentClass!
                                                        .classId
                                                        .toString();
                                                    selectChild = index;
                                                    lenghtSubject = data[index]
                                                        .currentClass!
                                                        .listSubjects!
                                                        .length;
                                                    _eventChangeChild();                                                   
                                                  });
                                                },
                                              ),
                                            );
                                          }),
                                    ),
                                  ),
                                );
                              },
                              imageProfile:
                                  data[selectChild!].profileMedia!.fileShow ==
                                          null
                                      ? data[selectChild!]
                                          .profileMedia!
                                          .fileThumbnail
                                          .toString()
                                      : data[selectChild!]
                                          .profileMedia!
                                          .fileShow
                                          .toString(),
                            );
                          } else {
                            return Container(
                                margin: const EdgeInsets.only(right: 10.0),
                                child: Center(
                                    child: Image.asset(
                                  "assets/images/gifs/loading.gif",
                                  height: 45,
                                  width: 45,
                                )));
                          }
                        }),
                      ),
                    ),
      
                    // --------------------------- End Header------------------------------
      
                    Container(
                      height: 200,
                      width: width,
                      decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(12.0),
                          topRight: Radius.circular(12.0),
                        ),
                      ),
                      child: Column(
                        children: [
                          Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 18.0, vertical: 20.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Align(
                                      child: Text(
                                        "PLEASESELECTMONTH".tr(),
                                        style:const TextStyle(
                                          fontSize: 18.0,
                                          color: Colorconstands.darkGray,
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ],
                                ),
      
                                //=============================Month Selection=========================
                                ListMonthWidget(
                                  monthName: monthdata[monthlocal! - 1].name,
                                  childBody: ListView.builder(
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      shrinkWrap: true,
                                      padding: const EdgeInsets.all(0),
                                      itemCount: monthdata.length,
                                      itemBuilder: (context, index) {
                                        return Container(
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                            color: Colorconstands.white,
                                            border: Border(
                                              bottom: BorderSide(
                                                color: Colorconstands.darkGray
                                                    .withOpacity(0.1),
                                                width: 1,
                                              ),
                                            ),
                                          ),
                                          height: 50,
                                          child: Container(
                                            margin: const EdgeInsets.symmetric(
                                                horizontal: 15.0),
                                            child: ListTile(
                                              title: Text(
                                                monthdata[index].name.tr(),
                                                style: ThemeConstands
                                                    .texttheme.headline6!
                                                    .copyWith(
                                                        fontWeight:
                                                            FontWeight.normal,
                                                        color: monthdata[index]
                                                                        .isSelect ==
                                                                    false ||
                                                                monthselected ==
                                                                    monthdata[
                                                                            index]
                                                                        .month
                                                            ? Colorconstands
                                                                .primaryColor
                                                            : Colors.red),
                                              ),
                                              trailing: Icon(
                                                monthdata[index].isSelect ==
                                                            true ||
                                                        monthselected ==
                                                            monthdata[index].month
                                                    ? Icons.radio_button_checked
                                                    : Icons.circle_outlined,
                                                size: 20,
                                                color: Colorconstands.primaryColor,
                                              ),
                                              //trailing:const Icon(Icons.check_circle),
                                              onTap: () {
                                                setState(() {
                                                  for (var element in monthdata) {
                                                    element.isSelect = false;
                                                  }
                                                  monthdata[index].isSelect =
                                                      true;
                                                  monthselected =
                                                      monthdata[index].month;
                                                  monthlocal =
                                                      monthdata[index].month;
                                                  Navigator.of(context).pop();
                                                  monthname = monthdata[index]
                                                      .name
                                                      .toString();
                                                  BlocProvider.of<TeachingBloc>(
                                                          context)
                                                      .add(
                                                    GetTeachingEvent(
                                                      subjectID:
                                                          firstSubject.toString(),
                                                      classID: classID.toString(),
                                                      year: currentYear,
                                                      month: monthselected
                                                          .toString(),
                                                    ),
                                                  );
                                                });
                                              },
                                            ),
                                          ),
                                        );
                                      }),
                                ),
                                //============================= End Month Selection=========================
                                const SizedBox(
                                  height: 8,
                                ),
                              ],
                            ),
                          ),
      
                          //=============================Subject Selection=========================
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: DefaultTabController(
                              length: lenghtSubject!,
                              initialIndex: 0,
                              child: BlocBuilder<MonthlyBloc, MonthlyState>(
                                builder: (context, state) {
                                  if (state is MonthlyLoadingState) {
                                    return Center(
                                      child: ShimmerWidget(
                                        child: Container(
                                          decoration: const BoxDecoration(
                                            color: Colors.grey,
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(18.0),
                                            ),
                                          ),
                                          margin: const EdgeInsets.symmetric(
                                              horizontal: 8.0),
                                          width:
                                              MediaQuery.of(context).size.width,
                                          height: 60,
                                        ),
                                      ),
                                    );
                                  }
                                  if (state is SpecificChildLoadedState) {
                                    var data = state
                                        .specificChildModel?.data?.listSubjects;
                                    return SizedBox(
                                      height: 50,
                                      child: TabBar(
                                        labelPadding: const EdgeInsets.symmetric(
                                            horizontal: 8.0),
                                        labelColor: Colors.white,
                                        unselectedLabelColor: Colors.black,
                                        isScrollable: true,
                                        controller: _subjectController,
                                        labelStyle: const TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w600,
                                        ),
                                        tabs: List.generate(
                                          lenghtSubject!,
                                          (index) => Tab(
                                            child: Container(
                                              padding: const EdgeInsets.symmetric(
                                                  horizontal: 18.0,
                                                  vertical: 8.0),
                                              decoration: BoxDecoration(
                                                color: seletedIndex == index
                                                    ? Colorconstands.primaryColor
                                                    : const Color(0xFFF1F9FF),
                                                borderRadius:
                                                    BorderRadius.circular(8.0),
                                                boxShadow: [
                                                  BoxShadow(
                                                    offset: const Offset(0, 0.5),
                                                    color: Colors.black
                                                        .withOpacity(0.2),
                                                    blurRadius: 1,
                                                  ),
                                                ],
                                              ),
                                              child: Text(translate=="en"?data![index].nameEn.toString():data![index].name.toString(),
                                              ),
                                            ),
                                          ),
                                        ),
                                        onTap: (value) {
                                          firstSubject =
                                              data![value].id.toString();
                                          BlocProvider.of<TeachingBloc>(context)
                                              .add(
                                            GetTeachingEvent(
                                              subjectID: firstSubject.toString(),
                                              classID: classID.toString(),
                                              year: currentYear,
                                              month: monthselected.toString(),
                                            ),
                                          );
                                        },
                                      ),
                                    );
                                  }
                                  return Container();
                                },
                              ),
                            ),
                          ),
      
                          //=============================Subject Selection=========================
                        ],
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: Container(
                    color: Colors.white,
                    width: width,
                    padding: const EdgeInsets.symmetric(horizontal: 14.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 40,
                          child: TabBar(
                            controller: _tabController,
                            labelColor: const Color.fromRGBO(24, 85, 171, 1),
                            indicator: const BoxDecoration(
                              border: Border(
                                bottom: BorderSide(
                                  color: Colorconstands.primaryColor,
                                  width: 2.0,
                                ),
                              ),
                            ),
                            labelStyle:
                                ThemeConstands.texttheme.headline5!.copyWith(
                              fontWeight: FontWeight.w600,
                            ),
                            indicatorSize: TabBarIndicatorSize.tab,
                            indicatorPadding:
                                const EdgeInsets.symmetric(horizontal: 14),
                            physics: const NeverScrollableScrollPhysics(),
                            isScrollable: true,
                            tabs: tabs.toList(),
                          ),
                        ),
                        const SizedBox(
                          height: 18.0,
                        ),
                        Expanded(
                          child: TabBarView(
                            controller: _tabController,
                            children: const [
                              TabonGoingScreen(),
                              TabHistoryScreen(),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void getlocaldataModel() async {
    GetStoragePref _pref = GetStoragePref();
    var childId = "";
    var classid = "";
    var selectedChild = 0;
    var subjectid = "";
    var langSubject = 0;
    setState(() {
      studentID = childId.toString();
      classID = classid.toString();
      selectChild = selectedChild;
      firstSubject = subjectid.toString();
      lenghtSubject = langSubject;
      _subjectController = TabController(length: lenghtSubject!, vsync: this);
      _subjectController.addListener(() {
        setState(() {
          seletedIndex = _subjectController.index;
        });
      });
      BlocProvider.of<TeachingBloc>(context).add(
        GetTeachingEvent(
          subjectID: firstSubject.toString(),
          classID: classID.toString(),
          year: currentYear,
          month: monthselected.toString(),
        ),
      );
      BlocProvider.of<ChangeChildBloc>(context).add(GetChildrenEvent());
      BlocProvider.of<MonthlyBloc>(context).add(
        GetSpecificChildEvent(),
      );
    });
  }

  void _eventChangeChild() {
    _subjectController = TabController(length: lenghtSubject!, vsync: this);
    _subjectController.addListener(() {
      setState(() {
        seletedIndex = _subjectController.index;
      });
    });
    BlocProvider.of<TeachingBloc>(context).add(
      GetTeachingEvent(
        subjectID: firstSubject.toString(),
        classID: classID.toString(),
        year: currentYear,
        month: monthselected.toString(),
      ),
    );
    BlocProvider.of<ChangeChildBloc>(context).add(GetChildrenEvent());
    BlocProvider.of<MonthlyBloc>(context).add(
      GetSpecificChildEvent(),
    );
  }
  void backScreen(){
    if(connection==true){
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => HomeScreen(),
          ),
        );
      }
      else{
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => HomeScreen(),
          ),
        );
      }
   }

  Widget _buildLoading() => Container(
      margin: const EdgeInsets.only(right: 10.0),
      child: Center(
          child: Image.asset(
        "assets/images/gifs/loading.gif",
        height: 45,
        width: 45,
      )));
}
