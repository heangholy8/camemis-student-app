import 'package:easy_localization/easy_localization.dart';

import '../../../../storages/get_storage.dart';
import '../../../../widgets/model_bottomSheet.dart';
import '../../../routes/e.route.dart';
import '../bloc/teaching_bloc.dart';

class ListMonthWidget extends StatefulWidget {
  final Widget childBody;
  final String monthName;
  const ListMonthWidget({Key? key, required this.childBody, required this.monthName}) : super(key: key);

  @override
  State<ListMonthWidget> createState() => _ListMonthWidgetState();
}

class _ListMonthWidgetState extends State<ListMonthWidget> {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          color: const Color(0xFFF1F9FF),
          borderRadius: BorderRadius.circular(12.0)),
      child: StatefulBuilder(
        builder:
            (BuildContext context, void Function(void Function()) setState) {
          return MaterialButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12.0)),
            padding: const EdgeInsets.all(0.0),
            onPressed: () {
              setState(() {
                showModalBottomSheet(
                    backgroundColor: Colors.transparent,
                    isScrollControlled: true,
                    context: context,
                    isDismissible: true,
                    builder: (context) {
                      return BuildBottomSheet(
                        initialChildSize: 0.55,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              padding: const EdgeInsets.only(top: 10.0),
                              alignment: Alignment.center,
                              child: Align(
                                child: Text(
                                  "PLEASESELECTMONTH".tr(),
                                  style: ThemeConstands.texttheme.headline6!
                                      .copyWith(color: Colorconstands.black),
                                ),
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.symmetric(
                                horizontal: 30,
                                vertical: 20,
                              ),
                              child: Align(
                                child: Text(
                                  "Excepteur sint occaecat cupidatat non proident, sunt in culpa",
                                  style: ThemeConstands.texttheme.subtitle2!
                                      .copyWith(color: Colorconstands.black),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            Divider(
                              height: 2,
                              thickness: 1,
                              color: Colorconstands.darkGray.withOpacity(0.1),
                            )
                          ],
                        ),
                        expanded: widget.childBody,
                      );
                    });
              });
            },
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 25),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    widget.monthName,
                    style: const TextStyle(
                        color: Colorconstands.secondaryColor,
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600),
                  ),
                  const Icon(
                    Icons.keyboard_arrow_down_rounded,
                    size: 28,
                    color: Colorconstands.secondaryColor,
                  )
                ],
              ),
              alignment: Alignment.centerLeft,
            ),
          );
        },
      ),
    );
  }
}
