import 'package:bloc/bloc.dart';
import 'package:camis_application_flutter/helper/timer_helper.dart';
import 'package:camis_application_flutter/model/base/base_respone_model.dart';
import 'package:camis_application_flutter/model/teaching_model/homework_model.dart';
import 'package:camis_application_flutter/model/teaching_model/lesson_model.dart';
import 'package:camis_application_flutter/model/teaching_model/onlinelive_model.dart';
import 'package:camis_application_flutter/model/teaching_model/teaching_model.dart';
import 'package:camis_application_flutter/service/apis/teaching_api/teaching_api.dart';
import 'package:equatable/equatable.dart';

part 'teaching_event.dart';
part 'teaching_state.dart';

class TeachingBloc extends Bloc<TeachingEvent, TeachingState> {
  final TeachingApi teachingApi;
  BaseResponseModel<TeachingAndLearningModel> teachModel =
      BaseResponseModel<TeachingAndLearningModel>();
  BaseResponseModel<HomeWorkModel> homeworkModel =
      BaseResponseModel<HomeWorkModel>();
  TeachingBloc({required this.teachingApi}) : super(TeachingInitial()) {
    //// Get Teaching Bloc
    on<GetTeachingEvent>((event, emit) async {
      emit(TeachingLoadingState());
      String currentMonth = TimerHepler().getCurrentMonth;
      String currentYear = TimerHepler().getCurrentYear;
      try {
        teachModel = await teachingApi.getTeachingApi(
            subjectId: event.subjectID,
            classId: event.classID,
            month: event.month ?? currentMonth,
            year: event.year ?? currentYear);
        emit(
          TeachingLoadedState(teachingModel: teachModel),
        );
      } catch (e) {
        emit(
          TeachingErrorState(error: e.toString()),
        );
      }
    });
  }
}
