part of 'teaching_bloc.dart';

abstract class TeachingState extends Equatable {
  const TeachingState();

  @override
  List<Object> get props => [];
}

class TeachingInitial extends TeachingState {}

class TeachingLoadingState extends TeachingState {}

class TeachingLoadedState extends TeachingState {
  final BaseResponseModel<TeachingAndLearningModel>? teachingModel;
  const TeachingLoadedState({this.teachingModel});
}


/// HomeWork State 
class HomeWorkLoadedState extends TeachingState{
    final BaseResponseModel<HomeWorkModel>? homeWorkModel;
  const HomeWorkLoadedState({this.homeWorkModel});
}


class TeachingErrorState extends TeachingState {
  final String error;
  const TeachingErrorState({required this.error});
}
