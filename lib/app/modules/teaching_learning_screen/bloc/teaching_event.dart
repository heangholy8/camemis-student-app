// ignore_for_file: must_be_immutable

part of 'teaching_bloc.dart';

abstract class TeachingEvent extends Equatable {
  const TeachingEvent();

  @override
  List<Object> get props => [];
}

class GetTeachingEvent extends TeachingEvent {
  final String subjectID;
  final String classID;
   String? month;
   String? year;
   GetTeachingEvent({this.month,this.year ,required this.subjectID, required this.classID});
}

// Get Homework Class Event
class GetHomeWorkEvent extends TeachingEvent{
  final String lessonID;
  final String studentID;
   const GetHomeWorkEvent({required this.studentID,required this.lessonID});
}
class GetCurrentClassEvent extends TeachingEvent {}
