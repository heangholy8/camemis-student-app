class ModelEdit {
  String? firstName;
  String? lastName;
  String? latinfirstName;
  String? latinlastName;
  String? email;
  String? phone;
  String? numberphone;
  String? dateofbirth;
  
  ModelEdit({this.firstName, this.lastName, this.email, this.dateofbirth,this.latinfirstName,this.latinlastName,this.numberphone,this.phone});
}