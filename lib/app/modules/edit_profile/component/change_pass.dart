// ignore_for_file: avoid_print
import 'package:camis_application_flutter/app/modules/edit_profile/component/bloc/changepassword_bloc.dart';
import 'package:camis_application_flutter/widgets/custom_alertdialog.dart';

import '../../../../helper/route.export.dart';
import '../../../routes/e.route.dart';
import '../bloc/change_info_user_bloc.dart';

class ChangePass extends StatefulWidget {
  const ChangePass({Key? key}) : super(key: key);

  @override
  State<ChangePass> createState() => _ChangePassState();
}

class _ChangePassState extends State<ChangePass> {
  final _formKey = GlobalKey<FormState>();
  late bool _passwordDisable;
  late bool _passRetypeDisable;
  late bool errorText;
  final TextEditingController _currentTextController = TextEditingController();
  final TextEditingController _newTextController = TextEditingController();
  final TextEditingController _confirmTextController = TextEditingController();
  ValueNotifier<bool>? _fieldNotifier;
  ValueNotifier<StateContext?>? _stateContextNotifier;

  @override
  void initState() {
    super.initState();
    _stateContextNotifier = ValueNotifier<StateContext?>(const StateContext());
    _fieldNotifier = ValueNotifier<bool>(false);
    errorText = false;
    _passwordDisable = false;
    _passRetypeDisable = false;
  }

  bool get _fieldIsNotEmpty {
    String schoolCode = _currentTextController.text;
    String userName = _newTextController.text;
    String password = _confirmTextController.text;
    bool isValid =
        schoolCode.isNotEmpty && userName.isNotEmpty && password.isNotEmpty;
    return isValid;
  }

  @override
  void dispose() {
    super.dispose();
    _currentTextController.dispose();
    _newTextController.dispose();
    _confirmTextController.dispose();
  }

  void validateAndSave() {
    final FormState? form = _formKey.currentState;
    if (form!.validate()) {
      print('Form is valid');
    } else {
      print('Form is invalid');
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: BlocConsumer<ChangepasswordBloc, ChangepasswordState>(
      listener: (context, state) {
        if (state is ChangepasswordLoadingState) {
          showDialog(
            barrierDismissible: false,
            barrierColor: Colors.black38,
            context: context,
            builder: (context) {
              return Dialog(
                elevation: 5,
                backgroundColor: Colorconstands.gray.withOpacity(0.8),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
                child: Container(
                  width: MediaQuery.of(context).size.width / 5,
                  height: MediaQuery.of(context).size.width / 5,
                  padding: const EdgeInsets.all(10),
                  child: const Center(
                    child: CircularProgressIndicator(),
                  ),
                ),
              );
            },
          );
        }
        if (state is ChangepasswordSuccessState ||
            BlocProvider.of<ChangepasswordBloc>(context).isValidate == true) {
          Navigator.of(context).pop();
          showDialog(
            barrierDismissible: false,
            barrierColor: Colors.black38,
            context: context,
            builder: (context) {
              return CustomAlertDialog(
                blurColor: const Color(0xFF2699FB),
                child: Center(
                  child: Icon(
                    Icons.check_circle,
                    color: Colorconstands.white,
                    size: MediaQuery.of(context).size.width / 6,
                  ),
                ),
                title: "Password Updated!",
                subtitle: "Your password has been changed successfully.",
                onPressed: () {
                  Navigator.of(context).pop();
                },
                titleButton: 'OK',
                buttonColor: Colorconstands.secondaryColor,
                titlebuttonColor: Colorconstands.white,
              );
            },
          );
        }
        if (state is ChangepasswordErrorState) {
          Navigator.of(context).pop();

          showDialog(
            barrierDismissible: false,
            barrierColor: Colors.black38,
            context: context,
            builder: (context) {
              return CustomAlertDialog(
                //  blurColor: const Color.fromARGB(255, 251, 38, 38),
                child: Center(
                  child: SvgPicture.asset(
                    "assets/icons/cross_circle.svg",
                    // color: Colorconstands.white,
                    width: MediaQuery.of(context).size.width / 6,
                  ),
                ),
                title: "Password Error!",
                subtitle: "Excepteur sint occaecat cupidatat non proident",
                onPressed: () {
                  Navigator.of(context).pop();
                },
                titleButton: 'Try Agian',
                buttonColor: Colorconstands.errorColor,
                titlebuttonColor: Colorconstands.white,
              );
            },
          );
        }
      },
      builder: (context, state) {
        return InkWell(
          focusColor: Colors.transparent,
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Container(
            margin: const EdgeInsets.only(top: 35, left: 18.0, right: 18.0),
            child: Form(
              key: _formKey,
              child: ValueListenableBuilder<StateContext?>(
                  valueListenable: _stateContextNotifier!,
                  builder: (ctx, value, child) {
                    bool? errorState = false;
                    if (value!.state != null) {
                      if (value.state is ErrorState) {
                        errorState = value.state?.handle();
                      }
                    }
                    return Column(
                      children: [
                        CustomFormWidget(
                          items: [
                            FormItem(
                              label: 'Current Password',
                              child: CustomTextField(
                                controller: _currentTextController,
                                errorState: errorState!,
                                hintText: 'Enter your current password',
                                onFieldSubmitted: (v) {},
                                onChanged: (v) {
                                  _stateContextNotifier!.value =
                                      const StateContext();
                                  _fieldNotifier!.value = _fieldIsNotEmpty;
                                },
                              ),
                            ),
                            const SizedBox(
                              height: 18.0,
                            ),
                            FormItem(
                              label: "New Password",
                              child: CustomTextField(
                                controller: _newTextController,
                                errorState: errorState,
                                hintText: 'Enter new password',
                                securetext: _passRetypeDisable,
                                suffixIcon: InkWell(
                                    onTap: () {
                                      setState(() {
                                        _passRetypeDisable =
                                            !_passRetypeDisable;
                                      });
                                    },
                                    child: _passRetypeDisable
                                        ? const Icon(
                                            Icons.visibility_off,
                                            size: 26,
                                          )
                                        : const Icon(
                                            Icons.visibility,
                                            size: 26,
                                          )),
                                onFieldSubmitted: (v) {},
                                onChanged: (v) {
                                  _stateContextNotifier!.value =
                                      const StateContext();
                                  _fieldNotifier!.value = _fieldIsNotEmpty;
                                },
                              ),
                            ),
                            const SizedBox(
                              height: 18.0,
                            ),
                            FormItem(
                              label: 'Retype New Password',
                              child: CustomTextField(
                                  controller: _confirmTextController,
                                  errorState: errorState,
                                  hintText: 'Confirm new password',
                                  securetext: _passwordDisable,
                                  suffixIcon: InkWell(
                                      onTap: () {
                                        setState(() {
                                          // disable_btn = !disable_btn;
                                          _passwordDisable = !_passwordDisable;
                                        });
                                      },
                                      child: _passwordDisable
                                          ? const Icon(
                                              Icons.visibility_off,
                                              size: 26,
                                            )
                                          : const Icon(
                                              Icons.visibility,
                                              size: 26,
                                            )),
                                  onFieldSubmitted: (v) {},
                                  onChanged: (v) {
                                    _stateContextNotifier!.value =
                                        const StateContext();
                                    _fieldNotifier!.value = _fieldIsNotEmpty;
                                  }),
                            ),
                          ],
                          error: errorText,
                          state: value,
                          buttonCallback: ValueListenableBuilder<bool>(
                            valueListenable: _fieldNotifier!,
                            child: const Hero(
                              tag: 'btnc',
                              child: ButtonScanQRWidget(
                                backgroundColor: Colorconstands.primaryColor,
                                title: 'Change Password',
                                onpressed: null,
                              ),
                            ),
                            builder: (ctx, value, child) {
                              if (value) {
                                return ButtonScanQRWidget(
                                  backgroundColor: Colorconstands.primaryColor,
                                  title: 'Change Password',
                                  onpressed: () async {
                                    print(_currentTextController.text);
                                    print(_newTextController.text);
                                    print(_confirmTextController.text);
                                    BlocProvider.of<ChangepasswordBloc>(context)
                                        .add(
                                      ChangePassEvent(
                                          currentpassword:
                                              _currentTextController.text
                                                  .toString(),
                                          newPassword: _newTextController.text
                                              .toString(),
                                          confirmPassword:
                                              _confirmTextController.text
                                                  .toString()),
                                    );
                                    // ChangeUserInformationApi()
                                    //     .changePasswordApi(
                                    //         currentPassword:
                                    //             _currentTextController.text
                                    //                 .toString(),
                                    //         newPassword: _newTextController.text
                                    //             .toString(),
                                    //         confirmPassword:
                                    //             _confirmTextController.text
                                    //                 .toString());
                                  },
                                );
                              } else {
                                return child!;
                              }
                            },
                          ),
                        ),
                        // SizedBox(
                        //   child: ButtonScanQRWidget(
                        //     backgroundColor: Colors.white,
                        //     title: 'Cancel',
                        //     textColor: Colors.black,
                        //     borderColor: Colors.white,
                        //     onpressed: () async {},
                        //   ),
                        // ),
                        // const SizedBox(
                        //   height: 50,
                        // )
                      ],
                    );
                  }),
            ),
          ),
        );
      },
    ));
  }
}
