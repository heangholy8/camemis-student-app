import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../../service/apis/change_info_apis/change_password_api.dart';

part 'changepassword_event.dart';
part 'changepassword_state.dart';

class ChangepasswordBloc
    extends Bloc<ChangepasswordEvent, ChangepasswordState> {
  final ChangeUserInformationApi api;
  final isValidate = false;
  ChangepasswordBloc({required this.api}) : super(ChangepasswordInitial()) {
    on<ChangePassEvent>((event, emit) async {
      emit(ChangepasswordLoadingState());
      try {
        var data = await api
            .changePasswordApi(
                currentPassword: event.currentpassword,
                newPassword: event.newPassword,
                confirmPassword: event.confirmPassword)
            .then((value) {
          if (value != null) {
            isValidate == true;
            emit(ChangepasswordSuccessState());
          } else {
            isValidate == false;
            emit(const ChangepasswordErrorState(
                message: "Something when wrong!"));
          }
        });

        //  print(data);
      } catch (e) {
        emit(ChangepasswordErrorState(message: e.toString()));
      }
    });
  }
}
