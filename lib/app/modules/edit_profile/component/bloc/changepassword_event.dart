part of 'changepassword_bloc.dart';

abstract class ChangepasswordEvent extends Equatable {
  const ChangepasswordEvent();

  @override
  List<Object> get props => [];
}

class ChangePassEvent extends ChangepasswordEvent {
  final String currentpassword;
  final String newPassword;
  final String confirmPassword;

  const ChangePassEvent({
    required this.currentpassword,
    required this.newPassword,
    required this.confirmPassword,
  });
}
