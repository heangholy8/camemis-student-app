part of 'changepassword_bloc.dart';

abstract class ChangepasswordState extends Equatable {
  const ChangepasswordState();

  @override
  List<Object> get props => [];
}

class ChangepasswordInitial extends ChangepasswordState {}

class ChangepasswordLoadingState extends ChangepasswordState {}

class ChangepasswordSuccessState extends ChangepasswordState {}

class ChangepasswordErrorState extends ChangepasswordState {
  final String message;
  const ChangepasswordErrorState({required this.message});
}
