import 'package:camis_application_flutter/app/routes/e.route.dart';
class CustomeFormField extends StatefulWidget {
  final TextEditingController firstnameKhController;
  final TextEditingController lastnameKhController;
  final TextEditingController firstnameEnController;
  final TextEditingController lastnameEnController;
  final TextEditingController emailController;
  final TextEditingController phoneController;
  final TextEditingController dobController;
  const CustomeFormField(
      {Key? key,
      required this.firstnameKhController,
      required this.lastnameKhController,
      required this.firstnameEnController,
      required this.lastnameEnController,
      required this.emailController,
      required this.phoneController,
      required this.dobController})
      : super(key: key);

  @override
  State<CustomeFormField> createState() => _CustomeFormFieldState();
}

class _CustomeFormFieldState extends State<CustomeFormField> with Toast {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    widget.firstnameEnController.dispose();
    widget.lastnameEnController.dispose();
    widget.firstnameKhController.dispose();
    widget.lastnameKhController.dispose();
    widget.emailController.dispose();
    widget.dobController.dispose();
    widget.phoneController.dispose();
    super.dispose();
  }

  void clearTextfield() {
    widget.firstnameEnController.clear();
    widget.lastnameEnController.clear();
    widget.firstnameKhController.clear();
    widget.lastnameKhController.clear();
    widget.emailController.clear();
    widget.dobController.clear();
    widget.phoneController.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 22.0),
      child: Column(
        children: [
          FormItem(
            label: "Firstname",
            child: CustomTextField(
              controller: widget.firstnameKhController,
              errorState: false,
              hintText: 'Enter your firstname in khmer',
              onFieldSubmitted: (v) {},
              onChanged: (v) {
                // _stateContextNotifier!.value = const StateContext();
                // _fieldNotifier!.value = _fieldIsNotEmpty;
              },
            ),
          ),
          const SizedBox(
            height: 18.0,
          ),
          FormItem(
            label: "Lastname",
            child: CustomTextField(
              controller: widget.lastnameKhController,
              errorState: false,
              hintText: 'Enter your lastname in khmer',
              onFieldSubmitted: (v) {},
              onChanged: (v) {
                // _stateContextNotifier!.value = const StateContext();
                // _fieldNotifier!.value = _fieldIsNotEmpty;
              },
            ),
          ),
          const SizedBox(
            height: 18.0,
          ),
          FormItem(
            label: "Firstname En",
            child: CustomTextField(
              controller: widget.firstnameEnController,
              errorState: false,
              hintText: 'Enter your firstname in english',
              onFieldSubmitted: (v) {},
              onChanged: (v) {
                // _stateContextNotifier!.value = const StateContext();
                // _fieldNotifier!.value = _fieldIsNotEmpty;
              },
            ),
          ),
          const SizedBox(
            height: 18.0,
          ),
          FormItem(
            label: "Lastname En",
            child: CustomTextField(
              controller: widget.lastnameEnController,
              errorState: false,
              hintText: 'Enter your lastname in english',
              onFieldSubmitted: (v) {},
              onChanged: (v) {
                // _stateContextNotifier!.value = const StateContext();
                // _fieldNotifier!.value = _fieldIsNotEmpty;
              },
            ),
          ),
          const SizedBox(
            height: 18.0,
          ),
          FormItem(
            label: "Email",
            child: CustomTextField(
              controller: widget.emailController,
              errorState: false,
              hintText: 'Enter your Email',
              onFieldSubmitted: (v) {},
              onChanged: (v) {
                // _stateContextNotifier!.value = const StateContext();
                // _fieldNotifier!.value = _fieldIsNotEmpty;
              },
            ),
          ),
          const SizedBox(
            height: 18.0,
          ),
          FormItem(
            label: "Phone Number",
            child: CustomTextField(
              controller: widget.phoneController,
              errorState: false,
              hintText: 'Enter your phone number',
              onFieldSubmitted: (v) {},
              onChanged: (v) {
                // _stateContextNotifier!.value = const StateContext();
                // _fieldNotifier!.value = _fieldIsNotEmpty;
              },
            ),
          ),
          const SizedBox(
            height: 18.0,
          ),
          FormItem(
            label: "Date of Birth",
            child: CustomTextField(
              controller: widget.dobController,
              errorState: false,
              hintText: 'dd/mm/yyyy',
              onFieldSubmitted: (v) {},
              onChanged: (v) {
                // _stateContextNotifier!.value = const StateContext();
                // _fieldNotifier!.value = _fieldIsNotEmpty;
              },
            ),
          ),
          const SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }
}
