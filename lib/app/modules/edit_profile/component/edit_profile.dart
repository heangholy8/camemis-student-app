import 'dart:async';
import 'dart:io';
import 'package:camis_application_flutter/widgets/take_upload_image.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:image_picker/image_picker.dart';
import '../../../../service/apis/profile_user/bloc/get_profile_user_bloc.dart';
import '../../../../widgets/custom_alertdialog.dart';
import '../../../../widgets/toast_nointernet.dart';
import '../../../routes/e.route.dart';
import '../bloc/change_info_user_bloc.dart';
import 'custom_formfield.dart';

class EditProfile extends StatefulWidget {
  const EditProfile({
    Key? key,
  }) : super(key: key);

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  final _formKey = GlobalKey<FormState>();
  final ImagePicker _picker = ImagePicker();
  File? _image;
  XFile? file;

  String? profileImage;
  StreamSubscription? stsub;
  bool connection = true;
  @override
  void initState() {
    super.initState();
    //=============Check internet====================
    stsub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
      });
    });

    //=============Check internet====================
  }

  @override
  void dispose() {
    stsub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: BlocListener<ChangeInfoUserBloc, ChangeInfoUserState>(
        listener: (context, state) {
          if (state is ChangeLoading) {
            showDialog(
              barrierDismissible: false,
              barrierColor: Colors.black38,
              context: context,
              builder: (context) {
                return Dialog(
                  elevation: 5,
                  backgroundColor: Colorconstands.gray.withOpacity(0.8),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: Container(
                    width: MediaQuery.of(context).size.width / 5,
                    height: MediaQuery.of(context).size.width / 5,
                    padding: const EdgeInsets.all(10),
                    child: const Center(
                      child: CircularProgressIndicator(),
                    ),
                  ),
                );
              },
            );
          }
          else if (state is ChangeInforLoaded) {
            Navigator.of(context).pop();
            BlocProvider.of<GetProfileUserBloc>(context).add(GetProfileUser());
            showDialog(
              barrierDismissible: false,
              barrierColor: Colors.black38,
              context: context,
              builder: (context) {
                return CustomAlertDialog(
                  blurColor: const Color(0xFF2699FB),
                  child: Center(
                    child: Icon(
                      Icons.check_circle,
                      color: Colorconstands.white,
                      size: MediaQuery.of(context).size.width / 6,
                    ),
                  ),
                  title: "Profile Updated!",
                  subtitle: "Your profile has been changed successfully.",
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  titleButton: 'OK',
                  buttonColor: Colorconstands.secondaryColor,
                  titlebuttonColor: Colorconstands.white,
                );
              },
            );
          }
          else if (state is ChangeError) {
            Navigator.of(context).pop();
            BlocProvider.of<GetProfileUserBloc>(context).add(GetProfileUser());
            showDialog(
              barrierDismissible: false,
              barrierColor: Colors.black38,
              context: context,
              builder: (context) {
                return CustomAlertDialog(
                  blurColor: const Color(0xFF2699FB),
                  child: Center(
                    child: Icon(
                      Icons.check_circle,
                      color: Colorconstands.white,
                      size: MediaQuery.of(context).size.width / 6,
                    ),
                  ),
                  title: "Profile Updated!",
                  subtitle: "Your profile has been changed successfully.",
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  titleButton: 'OK',
                  buttonColor: Colorconstands.secondaryColor,
                  titlebuttonColor: Colorconstands.white,
                );
              },
            );
          } else {}
        },
        child: InkWell(
          // splashColor: Colors.white,
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child:
              //=================Profile User===============
              BlocBuilder<GetProfileUserBloc, GetProfileUserState>(
                  builder: (context, state) {
            final TextEditingController _firstnameKhController =
                TextEditingController();
            final TextEditingController _lastnameKhController =
                TextEditingController();
            final TextEditingController _firstnameEnController =
                TextEditingController();
            final TextEditingController _lastnameEnController =
                TextEditingController();
            final TextEditingController _emailController =
                TextEditingController();
            final TextEditingController _phoneController =
                TextEditingController();
            final TextEditingController _dobController =
                TextEditingController();
            if (state is ProfileUserLoadingState) {
              return CustomeFormField(
                dobController: _dobController,
                emailController: _emailController,
                firstnameEnController: _firstnameEnController,
                firstnameKhController: _firstnameKhController,
                lastnameEnController: _lastnameEnController,
                lastnameKhController: _lastnameKhController,
                phoneController: _phoneController,
              );
            } else if (state is ProfileUserLoaded) {
              var data = state.profileusermodel.data;
              _firstnameKhController.text = data!.firstname.toString();
              _lastnameKhController.text = data.lastname.toString();
              _firstnameEnController.text = data.firstnameEn.toString();
              _lastnameEnController.text = data.lastnameEn.toString();
              _emailController.text = data.email.toString();
              _phoneController.text = data.phone.toString();
              _dobController.text = data.dob.toString();
              return Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(top: 35.0),
                    child: Stack(
                      children: [
                        Container(
                          decoration: const BoxDecoration(
                            color: Colorconstands.white,
                            shape: BoxShape.circle,
                          ),
                          child: MaterialButton(
                            shape: const CircleBorder(),
                            padding: const EdgeInsets.all(0),
                            onPressed: () {
                              setState(
                                () {
                                  showModalBottomSheet(
                                    backgroundColor: Colors.transparent,
                                    context: context,
                                    builder: (BuildContext bc) {
                                      return ShowPiker(
                                        onPressedCamera: () {
                                          _imgFromCamera();
                                          Navigator.of(context).pop();
                                        },
                                        onPressedGalary: () {
                                          _imgFromGallery();
                                          Navigator.of(context).pop();
                                        },
                                      );
                                    },
                                  );
                                },
                              );
                            },
                            child: Container(
                              margin: const EdgeInsets.all(5),
                              decoration: const BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colorconstands.secondaryColor),
                              height: MediaQuery.of(context).size.width / 4,
                              width: MediaQuery.of(context).size.width / 4,
                              child: CircleAvatar(
                                backgroundColor: Colorconstands.secondaryColor,
                                radius: 56,
                                child: Padding(
                                  padding:
                                      const EdgeInsets.all(0), // Border radius
                                  child: ClipOval(
                                      child: _image == null
                                          ? Image.network(
                                              data.profileMedia!.fileShow
                                                  .toString(),
                                              fit: BoxFit.cover,
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  4,
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  4,
                                            )
                                          : Image.file(
                                              _image!,
                                              fit: BoxFit.cover,
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  4,
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  4,
                                            )),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          right: 0,
                          bottom: 10,
                          child: Container(
                              height: 25,
                              width: 25,
                              decoration: BoxDecoration(
                                color: Colorconstands.primaryColor,
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              child: IconButton(
                                padding: const EdgeInsets.all(0),
                                color: Colorconstands.white,
                                iconSize: 16,
                                icon: const Icon(Icons.camera_alt),
                                onPressed: () {
                                  setState(
                                    () {
                                      showModalBottomSheet(
                                        backgroundColor: Colors.transparent,
                                        context: context,
                                        builder: (BuildContext bc) {
                                          return ShowPiker(
                                            onPressedCamera: () {
                                              _imgFromCamera();
                                            },
                                            onPressedGalary: () {
                                              _imgFromGallery();
                                            },
                                          );
                                        },
                                      );
                                    },
                                  );
                                },
                              )),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 5, bottom: 35),
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.center,
                          margin: const EdgeInsets.only(
                              top: 8, left: 17.0, right: 17.0),
                          child: Align(
                            child: Text(
                              "Guardian Name",
                              style:
                                  ThemeConstands.texttheme.headline6!.copyWith(
                                color: Colorconstands.black,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          margin: const EdgeInsets.only(
                              top: 8, left: 17.0, right: 17.0),
                          child: Align(
                            child: Text(
                              "Parents/Guardain",
                              style:
                                  ThemeConstands.texttheme.subtitle2!.copyWith(
                                color: Colorconstands.darkGray,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        CustomeFormField(
                          dobController: _dobController,
                          emailController: _emailController,
                          firstnameEnController: _firstnameEnController,
                          firstnameKhController: _firstnameKhController,
                          lastnameEnController: _lastnameEnController,
                          lastnameKhController: _lastnameKhController,
                          phoneController: _phoneController,
                        ),
                        ButtonScanQRWidget(
                            backgroundColor: Colorconstands.primaryColor,
                            title: 'Save',
                            onpressed: connection == false
                                ? () {
                                    showtoastInternet(context);
                                  }
                                : () {
                                    if (_firstnameKhController.text == "" ||
                                        _phoneController.text == "" ||
                                        _lastnameKhController.text == "" ||
                                        _lastnameEnController.text == "" ||
                                        _firstnameEnController.text == "" ||
                                        _emailController.text == "" ||
                                        _dobController.text == "") {
                                      showDialog(
                                        barrierDismissible: false,
                                        barrierColor: Colors.black38,
                                        context: context,
                                        builder: (context) {
                                          return CustomAlertDialog(
                                            child: Padding(
                                              padding:
                                                   EdgeInsets.all(4.0),
                                              child: SvgPicture.asset(
                                                "assets/icons/cross_circle.svg",
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width /
                                                    6,
                                              ),
                                            ),
                                            title: "Information Invalid!",
                                            subtitle:
                                                "Excepteur sint occaecat cupidatat non proident, ",
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                            titleButton: 'Try Again!',
                                            buttonColor:
                                                const Color(0xFFEB4D4B),
                                            titlebuttonColor:
                                                Colorconstands.white,
                                          );
                                        },
                                      );
                                    } else {
                                      if (_image == null) {
                                        BlocProvider.of<ChangeInfoUserBloc>(
                                                context)
                                            .add(
                                          ChangeInforNotimageData(
                                            fristname: _firstnameKhController
                                                .text
                                                .trim(),
                                            lastname: _lastnameKhController.text
                                                .trim(),
                                            fristnameen: _firstnameEnController
                                                .text
                                                .trim(),
                                            lastnameen: _lastnameEnController
                                                .text
                                                .trim(),
                                            gmail: _emailController.text.trim(),
                                            dob: _dobController.text.trim(),
                                            phone: _phoneController.text.trim(),
                                          ),
                                        );
                                      } else {
                                        BlocProvider.of<ChangeInfoUserBloc>(
                                                context)
                                            .add(
                                          ChangeInforData(
                                            fristname: _firstnameKhController
                                                .text
                                                .trim(),
                                            lastname: _lastnameKhController.text
                                                .trim(),
                                            fristnameen: _firstnameEnController
                                                .text
                                                .trim(),
                                            lastnameen: _lastnameEnController
                                                .text
                                                .trim(),
                                            gmail: _emailController.text.trim(),
                                            dob: _dobController.text.trim(),
                                            phone: _phoneController.text.trim(),
                                            file: _image!,
                                          ),
                                        );
                                      }
                                    }
                                  }),
                        const SizedBox(
                          height: 50,
                        ),
                      ],
                    ),
                  ),
                ],
              );
            } else {
              return const Center(
                child: CircularProgressIndicator(
                  strokeWidth: 10,
                ),
              );
            }
          }),
          //=================End Profile User===============
        ),
      ),
    );
  }

  void _imgFromCamera() async {
    XFile? pickedFile = await _picker.pickImage(source: ImageSource.camera);
    setState(() {
      _image = File(pickedFile!.path);
      file = pickedFile;
    });
  }

  void _imgFromGallery() async {
    XFile? pickedFile = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = File(pickedFile!.path);
      file = pickedFile;
      print("path-path " + file!.path.toString());
    });
  }
}
