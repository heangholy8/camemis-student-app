part of 'change_info_user_bloc.dart';

abstract class ChangeInfoUserState extends Equatable {
  const ChangeInfoUserState();

  @override
  List<Object> get props => [];
}

class ChangeInfoUserInitial extends ChangeInfoUserState {}

class ChangeLoading extends ChangeInfoUserState {}

class ChangeInforLoaded extends ChangeInfoUserState {
  BaseResponseModel<UserInfoModel>? dataInfor;
  ChangeInforLoaded({required this.dataInfor});
}

class ChangePassLoaded extends ChangeInfoUserState {
  BaseResponseModel<ChangePassModel>? changePassword;
  
  ChangePassLoaded({required this.changePassword});
}

class ChangeError extends ChangeInfoUserState {}
