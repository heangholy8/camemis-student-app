part of 'change_info_user_bloc.dart';

abstract class ChangeInfoUserEvent extends Equatable {
  const ChangeInfoUserEvent();

  @override
  List<Object> get props => [];
}

class ChangeInforData extends ChangeInfoUserEvent {
  final String? fristname;
  final String? lastname;
  final String? fristnameen;
  final String? lastnameen;
  final String? dob;
  final String? gmail;
  final String? gender;
  final String? phone;
  final String? address;
  final File? file;
 const ChangeInforData({this.gender, this.address, this.file,this.fristname, this.lastname, this.fristnameen, this.lastnameen, this.dob, this.gmail, this.phone});
}

class ChangeInforNotimageData extends ChangeInfoUserEvent {
  final String? fristname;
  final String? lastname;
  final String? fristnameen;
  final String? lastnameen;
  final String? dob;
  final String? gmail;
  final String? gender;
  final String? address;
  final String? phone;
 const ChangeInforNotimageData({this.gender,  this.address,this.fristname, this.lastname, this.fristnameen, this.lastnameen, this.dob, this.gmail, this.phone});
}
class ChangePassDataEvent extends ChangeInfoUserEvent {
  final String? currentPass;
  final String? newPass;
  final String? comfirmPass;
 const ChangePassDataEvent({this.currentPass, this.newPass, this.comfirmPass});
}
