import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:camis_application_flutter/model/change_password_model.dart';
import 'package:equatable/equatable.dart';
import '../../../../model/base/base_respone_model.dart';
import '../../../../model/user_infor_model.dart';
import '../../../../model/user_profile/user_profile.dart';
import '../../../../service/apis/change_info_apis/change_password_api.dart';
import '../../../../service/apis/change_info_apis/update_info_user_api.dart';

part 'change_info_user_event.dart';
part 'change_info_user_state.dart';

class ChangeInfoUserBloc
    extends Bloc<ChangeInfoUserEvent, ChangeInfoUserState> {
  final UpdateUserInfoApi changeInforUser;
  final ChangeUserInformationApi changePassword;
  bool isSuccess = false;
  ChangeInfoUserBloc(
      {required this.changePassword, required this.changeInforUser})
      : super(ChangeInfoUserInitial()) {
    on<ChangeInforData>((event, emit) async {
      emit(ChangeLoading());
      try {
        var data = await changeInforUser.updateInfoGuardApi(
          dob: event.dob.toString(),
          lastname: event.lastname.toString(),
          firstname: event.fristname.toString(),
          phone: event.phone.toString(),
          email: event.gmail.toString(),
          gender: event.gender.toString(),
          address: event.address.toString(),
          file: event.file!,
          firstnameEn: event.fristnameen.toString(),
          lastnameEn: event.lastnameen.toString(),
        );
        emit(ChangeInforLoaded(dataInfor: data));
      } catch (e) {
        emit(ChangeError());
      }
    });
    on<ChangeInforNotimageData>((event, emit) async {
      emit(ChangeLoading());
      try {
        var data = await changeInforUser.updateInfoGuardApiNotimage(
          dob: event.dob.toString(),
          lastname: event.lastname.toString(),
          lastnameEn: event.lastnameen.toString(),
          firstname: event.fristname.toString(),
          firstnameEn: event.fristnameen.toString(),
          phone: event.phone.toString(),
          email: event.gmail.toString(),
          address: event.address.toString(),
          gender: event.gender.toString()
        );
        emit(ChangeInforLoaded(dataInfor: data));
      } catch (e) {
        emit(ChangeError());
      }
    });

    on<ChangePassDataEvent>((event, emit) async {
      emit(ChangeLoading());
      try {
        var data = await changePassword.changePassword(
            confirmPassword: event.comfirmPass.toString(),
            newPassword: event.newPass.toString(),
            currentPassword: event.currentPass.toString());
        print("sinasadf" + data.isSuccess.toString());
        // if (data.isSuccess == true) {
        //   print("sfavadfdsf${data.isSuccess}");
        //   isSuccess = true;
        //   print("success$isSuccess");
        emit(ChangePassLoaded(changePassword: data));
        // } else {
        //   isSuccess = false;
        //   print("fail$isSuccess");
        //   emit(ChangeError());
        // }
      } catch (e) {
        emit(ChangeError());
      }
    });
  }
}
