import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:camis_application_flutter/app/modules/edit_profile/component/change_pass.dart';
import 'package:camis_application_flutter/app/modules/edit_profile/component/edit_profile.dart';
import 'package:flutter/material.dart';

import '../../../../storages/get_storage.dart';

class TabView extends StatefulWidget {
  const TabView({Key? key,}) : super(key: key);
  @override
  State<TabView> createState() => _TabViewState();
}

class _TabViewState extends State<TabView> {

  final List<Tab> myTabs = <Tab>[
    const Tab(text: 'User Profile'),
    const Tab(text: 'Password'),
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Stack(
        children: [
          Scaffold(
            backgroundColor: Colors.transparent,
            appBar: PreferredSize(
              preferredSize: const Size.fromHeight(67),
              child: AppBar(
                automaticallyImplyLeading: false,
                toolbarHeight: 67,
                excludeHeaderSemantics: true,
                backgroundColor: Colors.transparent,
                elevation: 0,
                title: Stack(
                  children: [
                    Positioned(
                      top: 45.5,
                      left: 0,
                      right: 0,
                      child: Container(
                        color: Colorconstands.darkGray.withOpacity(0.08),
                        height: 4,
                        width: MediaQuery.of(context).size.width,
                      ),
                    ),
                    Container(
                      color: Colors.transparent,
                      alignment: Alignment.bottomCenter,
                      margin: const EdgeInsets.symmetric(horizontal: 35),
                      padding: const EdgeInsets.only(bottom: 0),
                      child: TabBar(
                        indicatorColor:
                            Colorconstands.primaryColor.withOpacity(0.9),
                        indicatorWeight: 2,
                        indicatorSize: TabBarIndicatorSize.tab,
                        labelColor: Colorconstands.primaryColor,
                        labelStyle: ThemeConstands.texttheme.headline6!.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                        tabs: myTabs,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            body:const TabBarView(
              children: [
                EditProfile(),
                ChangePass(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
