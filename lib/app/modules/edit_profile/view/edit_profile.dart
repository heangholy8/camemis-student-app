import 'dart:async';

import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:camis_application_flutter/app/modules/edit_profile/view/tabview.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../service/apis/profile_user/bloc/get_profile_user_bloc.dart';
import '../../home_screen/view/home_screen.dart';

class EditProfileScreen extends StatefulWidget {
  const EditProfileScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<EditProfileScreen> createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen>
    with SingleTickerProviderStateMixin {
  final List<Tab> myTabs = <Tab>[
    const Tab(text: 'User Profile'),
    const Tab(text: 'Password'),
  ];

  StreamSubscription? stsub;
  bool connection = true;
  @override
  void initState() {
    super.initState();
    //=============Check internet====================
    stsub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
      });
    });

    //=============Check internet====================
    setState(() {
      BlocProvider.of<GetProfileUserBloc>(context).add(GetProfileUser());
    });
  }

  @override
  void dispose() {
    stsub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: Column(
        children: [
          Container(
            margin: const EdgeInsets.only(top: 55, bottom: 0),
            child: Row(
              children: [
                Container(
                  margin: const EdgeInsets.only(left: 8),
                  child: IconButton(
                    icon: const Icon(
                      Icons.arrow_back_ios_new_outlined,
                      size: 25,
                      color: Colorconstands.white,
                    ),
                    onPressed: () {
                      if (connection == true) {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => HomeScreen(
                            ),
                          ),
                        );
                      } else {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => HomeScreen(
                            ),
                          ),
                        );
                      }
                    },
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 8),
                  child: Align(
                    child: Text(
                      "Edit Profile",
                      style: ThemeConstands.texttheme.headline5!.copyWith(
                        color: Colorconstands.white,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          Expanded(
            child: Stack(
              children: [
                Positioned(
                  top: 35,
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    decoration: const BoxDecoration(
                      color: Colorconstands.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15.0),
                        topRight: Radius.circular(15.0),
                      ),
                    ),
                  ),
                ),
                const Positioned(
                  child: TabView(),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
