import 'dart:async';
import 'dart:io';
import 'package:camis_application_flutter/app/modules/PaymentAndBillScreen/bloc/bloc/check_payment_expire_bloc.dart';
import 'package:camis_application_flutter/app/modules/attendance_screen.dart/bloc/get_schedule_bloc.dart';
import 'package:camis_application_flutter/app/modules/attendance_screen.dart/view/schedule_screen.dart';
import 'package:camis_application_flutter/app/modules/auth_screens/e_school_code.dart';
import 'package:camis_application_flutter/app/modules/home_screen/widget/button_quick_action_widget.dart';
import 'package:camis_application_flutter/app/modules/home_screen/widget/card_schedule_subject_widget.dart';
import 'package:camis_application_flutter/app/modules/home_screen/widget/empty_widget.dart';
import 'package:camis_application_flutter/app/modules/home_screen/widget/widge_update.dart';
import 'package:camis_application_flutter/app/modules/teacher_list_screen/view/teacher_list_screen.dart';
import 'package:camis_application_flutter/app/modules/time_line/time_line.dart';
import 'package:camis_application_flutter/helper/check_platform_device.dart';
import 'package:camis_application_flutter/service/apis/profile_user/bloc/get_profile_user_bloc.dart';
import 'package:camis_application_flutter/state_bloc/check_version_update/bloc/check_version_update_bloc.dart';
import 'package:camis_application_flutter/storages/get_storage.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../../component/change_child/bloc/change_child_bloc.dart';
import '../../../../storages/key_storage.dart';
import '../../../../widgets/error_widget/error_request.dart';
import '../../../../widgets/lock_screen_no_paid_widget/lock_home_screen.dart';
import '../../../../widgets/navigate_bottom_bar_widget/navigate_bottom_bar.dart';
import '../../../../widgets/no_connection_alert_widget.dart';
import '../../../core/resources/asset_resource.dart';
import '../../guide_line/bloc/user_guideline_bloc.dart';
import '../../result_screen/bloc/monthly_result_bloc.dart';
import '../../result_screen/view/result_screen_V2.dart';
import '../../time_line/school_time_line/e.schooltimeline.dart';
import '../../video_guide/display_video_guide.dart';
import '../widget/card_summary_result.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key,}) : super(key: key);
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with Toast {

  ///=========== Update Version ==============
    bool updateVersion = false;
    String currentVersionIos = "1.0.0";
    String currentVersionAndroid = "1.0.0";
    String releaseDateVersionIos = "04/Apr/2023";
    String releaseDateVersionAndroid = "04/Apr/2023";
  ///=========== Update Version ==============
  ///
  final keyStoragePref keyPref = keyStoragePref();
  GetStoragePref _pref = GetStoragePref();
  bool paid = true;
  PageController? pagecNotification;
  double activeindexNotification = 0;
  double activeindexMyclass = 0;
  int? monthlocal;
  int? daylocal;
  int? yearchecklocal;
  int activeChild = 0;
  String studentId = "0";
  String classId = "0";
  List homeList1 = [];
  List homeList2 = [];
  bool firstLoad = true;
  String? date;
  String? schoolName;
  String? videoGuideStorage;
  final random =  Random();
  int lengthDataSchedule = 0;


  StreamSubscription? internetconnection;
  bool isoffline = true;
  void checkLogin() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
     videoGuideStorage = pref.getString("videoGuide") ?? "";
  }

  void getLocalData() async {
    var schoolname = await _pref.getJsonToken;
    setState(() {
      schoolName = schoolname.schoolName.toString();
    });
  }
  @override
  void initState() {
    setState(() {      
      //=============Check internet====================
      internetconnection = Connectivity()
          .onConnectivityChanged
          .listen((ConnectivityResult result) {
        // whenevery connection status is changed.
        if (result == ConnectivityResult.none) {
          //there is no any connection
          setState(() {
            isoffline = false;
          });
        } else if (result == ConnectivityResult.mobile) {
          //connection is mobile data network
          setState(() {
            isoffline = true;
          });
        } else if (result == ConnectivityResult.wifi) {
          //connection is from wifi
          setState(() {
            isoffline = true;
          });
        }
      });
      //=============Eend Check internet====================
      getLocalData();
      monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
      daylocal = int.parse(DateFormat("dd").format(DateTime.now()));
      yearchecklocal = int.parse(DateFormat("yyyy").format(DateTime.now()));
      date = "$daylocal/$monthlocal/$yearchecklocal";
      if(isoffline==true && firstLoad==true){
        BlocProvider.of<GetProfileUserBloc>(context).add(GetProfileUser());
        BlocProvider.of<CheckPaymentExpireBloc>(context).add(CheckPaymentEvent());
        BlocProvider.of<CheckVersionUpdateBloc>(context).add(CheckVersionUpdate());
      }
      checkLogin();
    });
    super.initState();
  }

  @override
  void dispose() {
    internetconnection!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    pagecNotification = PageController(viewportFraction: 0.92);
    return Scaffold(
      bottomNavigationBar: isoffline == false || updateVersion == true
          ? Container(
              height: 0,
            )
          : BottomNavigateBar(
              isActive: 1,
              paid: paid,
            ),
      backgroundColor: const Color.fromARGB(255, 247, 248, 249),
      body: WillPopScope(
        onWillPop: () {
          return exit(0);
        },
        child: Container(
          color: Colorconstands.primaryColor,
          child: Stack(
            children: [
              Positioned(
                right: 0,
                top: 0,
                child: Container(
                 child: SvgPicture.asset("assets/images/Path_home_1.svg"),
                ),
              ),
              Positioned(
                  left: 0,
                  top: 50,
                  child: SvgPicture.asset(
                    ImageAssets.path20,
                    color: Colorconstands.secondaryColor,
                  )),
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                child: Container(
                  margin: const EdgeInsets.only(top: 35),
                  child: Column(
                    children: [
                      Container(
                        margin: const EdgeInsets.symmetric(
                            horizontal: 24, vertical: 14),
                        child: Row(
                          children: [
                            Expanded(
                              child: Container(
                                margin: const EdgeInsets.only(left: 10),
                                alignment: Alignment.centerLeft,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "$schoolName",
                                      style: ThemeConstands
                                          .subtitle1_Regular_16
                                          .copyWith(
                                              color: Colorconstands
                                                  .darkBackgroundsDisabled),
                                      textAlign: TextAlign.left,
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    BlocBuilder<GetProfileUserBloc, GetProfileUserState>(
                                      builder: (context, state) {
                                        if (state is ProfileUserLoadingState) {
                                          return Container(
                                            child: Text(
                                              "${"HELLO".tr()}........",
                                              style: ThemeConstands
                                                  .headline3_SemiBold_20
                                                  .copyWith(
                                                      color: Colorconstands
                                                          .lightTextsRegular),
                                              textAlign: TextAlign.left,
                                            ),
                                          );
                                        } else if (state is ProfileUserLoaded) {
                                          var data = state.profileusermodel.data;
                                          return Container(
                                            child: Text(
                                              "${"HELLO".tr()} ${translate=="km"?data!.firstname:data!.firstnameEn == "null"? data.firstname:data.firstnameEn}",
                                              style: ThemeConstands
                                                  .headline3_SemiBold_20
                                                  .copyWith(
                                                      color: Colorconstands
                                                          .lightTextsRegular),
                                              textAlign: TextAlign.left,
                                            ),
                                          );
                                        } else if (state is ProfileUserErrorState) {
                                          return Container(
                                            child: Text(
                                              "HELLO".tr(),
                                              style: ThemeConstands
                                                  .headline3_SemiBold_20
                                                  .copyWith(
                                                      color: Colorconstands
                                                          .lightTextsRegular),
                                              textAlign: TextAlign.left,
                                            ),
                                          );
                                        } else {
                                          return Container();
                                        }
                                      },
                                    )
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(
                              width: 15,
                            ),
                            Container(
                              width: 64,
                              decoration: BoxDecoration(
                                  color: Colorconstands.neutralWhite
                                      .withOpacity(0.2),
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(10))),
                              child: Container(
                                padding: const EdgeInsets.only(
                                    top: 3, bottom: 12, left: 5, right: 5),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      child: Text(daylocal.toString(),
                                          style: ThemeConstands
                                              .headline1_SemiBold_32
                                              .copyWith(
                                                  color: Colorconstands
                                                      .neutralWhite,
                                                  height: 1.3),
                                          textAlign: TextAlign.center),
                                    ),
                                    Center(
                                        child: Text(
                                      "${(monthlocal == 1 ? "JANUARY" : monthlocal == 2 ? "FEBRUARY" : monthlocal == 3 ? "MARCH" : monthlocal == 4 ? "APRIL" : monthlocal == 5 ? "MAY" : monthlocal == 6 ? "JUNE" : monthlocal == 7 ? "JULY" : monthlocal == 8 ? "AUGUST" : monthlocal == 9 ? "SEPTEMBER" : monthlocal == 10 ? "OCTOBER" : monthlocal == 11 ? "NOVEMBER" : "DECEMBER").tr()}  ",
                                      style: ThemeConstands.subtitle1_Regular_16
                                          .copyWith(
                                              color:
                                                  Colorconstands.neutralWhite,
                                              height: 1),
                                      textAlign: TextAlign.center,
                                    ))
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: BlocListener<CheckVersionUpdateBloc, CheckVersionUpdateState>(
                          listener: (context, state) {
                            if(state is CheckVersionUpdateLoaded){
                                var data = state.checkUpdateModel.data;
                                var plateform = checkPlatformDevice();
                                if(plateform=="Android"){
                                  if(data!.versionAndroid.toString() == currentVersionAndroid || data.releaseDateAndroid == releaseDateVersionAndroid){
                                      setState(() {
                                        updateVersion = false;
                                      });
                                  }
                                  else{
                                     setState(() {
                                        updateVersion = true;
                                      });
                                  }
                                }
                                if(plateform=="ios"){
                                  if(data!.versionIos.toString() == currentVersionIos || data.releaseDateIos == releaseDateVersionIos){
                                      setState(() {
                                        updateVersion = false;
                                      });
                                  }
                                  else{
                                     setState(() {
                                        updateVersion = true;
                                      });
                                  }
                                }
                                //  Future.delayed(const Duration(milliseconds: 1200), () {
                                //     if(videoGuideStorage == "SeenVideoDaskBoard"){
                                //     }
                                //     else{
                                //       BlocProvider.of<UserGuidelineBloc>(context).add(const UserGuidelineGetdataEvent());
                                //     }
                                //  });
                              }
                          },
                          child: BlocListener<CheckPaymentExpireBloc, CheckPaymentExpireState>(
                            listener: (context, state) {
                              if(state is CheckPaymentExpireLoaded){
                                var dataStatus = state.checkpayment!.status;
                                setState(() {
                                  paid = dataStatus!;
                                });
                              }
                            },
                            child: Column(
                              children: [
                                Expanded(
                                  child: Stack(
                                    children: [
                                      Container(
                                        decoration: const BoxDecoration(
                                          color: Colorconstands.alertsAwaitingBg,
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(12),
                                                topRight: Radius.circular(12))),
                                        child: BlocBuilder<GetProfileUserBloc, GetProfileUserState>(
                                          builder: (context, state) {
                                            if (state is ProfileUserLoadingState) {
                                              return Container(
                                                color: Colorconstands.neutralWhite,
                                                child: const ShimmerDask());
                                            } else if (state is ProfileUserLoaded) {
                                              var datachild = state.profileusermodel.data;
                                            if(datachild!.currentClass != null){
                                              if (isoffline == true &&  firstLoad == true) {
                                                  studentId = datachild.id.toString();
                                                  classId = datachild.currentClass!.classId.toString();
                                                  BlocProvider.of<GetScheduleBloc>(context).add(GetSchedule(date: date,classId: classId,childId: studentId));
                                                  BlocProvider.of<MonthlyResultBloc>(context).add(ResultMonthlyEvent(classId: classId, month: monthlocal.toString(),studentId: studentId,term: ""));
                                              }
                                            }
                                              return datachild.currentClass == null
                                                  ? Container(
                                                    height: MediaQuery.of(context).size.height,
                                                    color: Colorconstands.neutralWhite,
                                                      child: DataNotFound(
                                                      subtitle: "CHILD_NOT_CLASS_DES".tr(),
                                                      title: "CHILD_NOT_CLASS".tr(),
                                                    ))
                                                  : Column(
                                                    children: [
                                                      Expanded(
                                                        child: Container(
                                                          decoration:const BoxDecoration(
                                                                    color: Colorconstands
                                                                .neutralWhite,
                                                                    borderRadius:  BorderRadius.only(topLeft: Radius.circular(12),topRight: Radius.circular(12))),
                                                          child: Stack(
                                                            children: [
                                                              SingleChildScrollView(
                                                                child: Container(
                                                                  alignment: Alignment .bottomCenter,
                                                                  child: Container(
                                                                    decoration: const BoxDecoration(),
                                                                    child: Column(
                                                                      children: [
                                                                        Container(
                                                                          padding: const EdgeInsets.only( top: 22,bottom: 12,left: 18,right: 18,),
                                                                          child:Row(
                                                                            mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                                                            children: [
                                                                              Text("SCHEDULE".tr(), style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.lightBlack)),
                                                                              GestureDetector(
                                                                                onTap: () {
                                                                                  Navigator.pushReplacement(context, PageRouteBuilder( pageBuilder: (context, animation1, animation2) 
                                                                                  => ScheduleScreen(
                                                                                        activeChild: activeChild,
                                                                                      ),
                                                                                      transitionDuration: Duration.zero,
                                                                                      reverseTransitionDuration: Duration.zero,
                                                                                    ),
                                                                                  );
                                                                                },
                                                                                child: Row(
                                                                                  children: [
                                                                                    Text("SEEALL".tr(), style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.primaryColor)),
                                                                                    const Icon( Icons.arrow_forward_ios_rounded, size: 18, color: Colorconstands.primaryColor,
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                        BlocConsumer<GetScheduleBloc,GetScheduleState>(
                                                                          listener: (context, state) {
                                                                            if(state is GetMyScheduleLoaded){
                                                                              setState(() {
                                                                                firstLoad = false;
                                                                              });
                                                                            }
                                                                          },
                                                                          builder:(context, state) {
                                                                            if (state is GetScheduleLoading) {
                                                                              return Container(
                                                                                decoration: const BoxDecoration(
                                                                                  color: Color(0xFFF1F9FF),
                                                                                  borderRadius: BorderRadius.only(
                                                                                    topLeft: Radius.circular(12),
                                                                                    topRight: Radius.circular(12),
                                                                                  ),
                                                                                ),
                                                                                child: Column(
                                                                                  children: [
                                                                                    ListView.builder(
                                                                                      padding: const EdgeInsets.all(0),
                                                                                      shrinkWrap: true,
                                                                                      itemBuilder: (contex, index) {
                                                                                        return const ShimmerTimeTableDask();
                                                                                      },
                                                                                      itemCount: 5,
                                                                                    ),
                                                                                  ],
                                                                                ),
                                                                              );
                                                                            } else if (state is GetMyScheduleLoaded) {
                                                                              var dataMySchedule = state.myScheduleModel!.data;
                                                                              var messageResporn = state.myScheduleModel!.message;
                                                                              var dataSchedulePage1;
                                                                              var dataSchedulePage2;
                                                                              var ranDomListIndex1;
                                                                              var ranDomListIndex2;
                                                                              var heightPageView;
                                                        
                                                                              if(dataMySchedule!.isNotEmpty){
                                                                                dataSchedulePage1 = dataMySchedule.take(4).toList();
                                                                                dataSchedulePage2 = dataMySchedule.length<=4?dataMySchedule.skip(0).toList():dataMySchedule.skip(4).toList();
                                                                                //======== random dataSchedulePage1 ===============
                                                                                ranDomListIndex1 = random.nextInt(dataSchedulePage1.length);
                                                                                //========End random dataSchedulePage1 ===============
                                                                                //======== random dataSchedulePage2 ===============
                                                                                  ranDomListIndex2 = random.nextInt(dataSchedulePage2.length);
                                                                                //========End random dataSchedulePage2 ===============
                                                                                heightPageView = 79 * dataSchedulePage1.length;
                                                                                lengthDataSchedule = dataMySchedule.length;
                                                                                //=======================
                                                                              }
                                                                              return dataMySchedule.isEmpty
                                                                                  ? EmptyWidget(
                                                                                    title: "$messageResporn".tr(),
                                                                                    subtitle: messageResporn=="HOLIDAY"?"HOLIDAY_DES".tr():messageResporn=="DATE_NOT_VALID_ACADEMIC" ? "DATE_NOT_VALID_ACADEMIC_DES".tr():"SCHEDULE_NOT_SET_DES".tr(),
                                                                                  ) 
                                                                                  : Column(
                                                                                    children: [
                                                                                      Container(
                                                                                          alignment: Alignment.center,
                                                                                          child: SizedBox(
                                                                                            height:heightPageView.toDouble(),
                                                                                            child: PageView.builder(
                                                                                              onPageChanged: (value) {
                                                                                                setState(() {
                                                                                                  firstLoad = false;
                                                                                                  activeindexMyclass = value.toDouble();
                                                                                                });
                                                                                              },
                                                                                              itemCount:dataMySchedule.length<=4?1:2,
                                                                                              itemBuilder: (context,indexPageView) {
                                                                                                return ListView.separated(
                                                                                                  shrinkWrap: true,
                                                                                                  physics: const NeverScrollableScrollPhysics(),
                                                                                                  itemCount: indexPageView==0?dataSchedulePage1.length:dataSchedulePage2.length,
                                                                                                  padding: const EdgeInsets.only(top: 0),
                                                                                                  itemBuilder: (context, indexschedule) {
                                                                                                    if (dataSchedulePage1.isNotEmpty) {
                                                                                                        homeList1 = dataSchedulePage1[indexschedule].activities!.where((element) {
                                                                                                          return element.activityType == 2;
                                                                                                        }).toList();
                                                                                                    }
                                                                                                    else if(dataSchedulePage2.isNotEmpty){
                                                                                                      homeList2 = dataSchedulePage2[indexschedule].activities!.where((element) {
                                                                                                          return element.activityType == 2;
                                                                                                        }).toList();
                                                                                                    }
                                                                                                    return indexPageView == 0 ? CardScheduleSubjectDaskWidget(
                                                                                                            studentId: datachild.id,
                                                                                                            classId: dataSchedulePage1[indexschedule].classId.toString(),
                                                                                                            date: dataSchedulePage1[indexschedule].date.toString(),
                                                                                                            scheduleId: dataSchedulePage1[indexschedule].id.toString(),
                                                                                                            leadership: datachild.currentClass!.leadership,
                                                                                                            index: indexschedule,
                                                                                                            randomIndex: ranDomListIndex1,
                                                                                                            isTakeAttendance: dataSchedulePage1[indexschedule].isTakeAttendance,
                                                                                                            paid: paid,
                                                                                                            homeList: homeList1,
                                                                                                            attStatus: dataSchedulePage1[indexschedule].attendanceStatus,
                                                                                                            titleSubject: dataSchedulePage1[indexschedule].subjectName==""||dataSchedulePage1[indexschedule].subjectNameEn==""?dataSchedulePage1[indexschedule].event
                                                                                                                        : translate=="km"? dataSchedulePage1[indexschedule].subjectName.toString():dataSchedulePage1[indexschedule].subjectNameEn.toString(),
                                                                                                            isActive: dataSchedulePage1[indexschedule].isCurrent,
                                                                                                            subjectColor: dataSchedulePage1[indexschedule].isExam == false ? "0xFF${dataSchedulePage1[indexschedule].subjectColor}" : "0xFFEE964B",
                                                                                                            scheduleExamType: dataSchedulePage1[indexschedule].isExam,
                                                                                                            examDate: dataSchedulePage1[indexschedule].isExam == true ? dataSchedulePage1[indexschedule].examItem!.examDate.toString() : "",
                                                                                                            timeSubject: "${dataSchedulePage1[indexschedule].startTime} - ${dataSchedulePage1[indexschedule].endTime}",
                                                                                                          ):
                                                                                                          CardScheduleSubjectDaskWidget(
                                                                                                            studentId: datachild.id,
                                                                                                            date: dataSchedulePage2[indexschedule].date.toString(),
                                                                                                            classId: dataSchedulePage2[indexschedule].classId.toString(),
                                                                                                            scheduleId: dataSchedulePage2[indexschedule].id.toString(),
                                                                                                            leadership: datachild.currentClass!.leadership,
                                                                                                            isTakeAttendance: dataSchedulePage1[indexschedule].isTakeAttendance,
                                                                                                            index: indexschedule,
                                                                                                            randomIndex: ranDomListIndex2,
                                                                                                            paid: paid,
                                                                                                            homeList: homeList2,
                                                                                                            attStatus: dataSchedulePage2[indexschedule].attendanceStatus,
                                                                                                            titleSubject:dataSchedulePage2[indexschedule].subjectName==""||dataSchedulePage2[indexschedule].subjectNameEn==""?dataSchedulePage2[indexschedule].event
                                                                                                                        :translate=="km"? dataSchedulePage2[indexschedule].subjectName.toString():dataSchedulePage2[indexschedule].subjectNameEn.toString(),
                                                                                                            isActive: dataSchedulePage2[indexschedule].isCurrent,
                                                                                                            subjectColor: dataSchedulePage2[indexschedule].isExam == false ? "0xFF${dataSchedulePage2[indexschedule].subjectColor}" : "0xFFEE964B",
                                                                                                            scheduleExamType: dataSchedulePage2[indexschedule].isExam,
                                                                                                            examDate: dataSchedulePage2[indexschedule].isExam == true ? dataSchedulePage2[indexschedule].examItem!.examDate.toString() : "",
                                                                                                            timeSubject: "${dataSchedulePage2[indexschedule].startTime} - ${dataSchedulePage2[indexschedule].endTime}",
                                                                                                          );
                                                                                                  },
                                                                                                  separatorBuilder: (context, index) {
                                                                                                    return Container(
                                                                                                      margin: const EdgeInsets.only(left: 80, right: 8),
                                                                                                      child: const Divider(
                                                                                                        height: 1,
                                                                                                        thickness: 0.7,
                                                                                                      ),
                                                                                                    );
                                                                                                  },
                                                                                                );
                                                                                              }
                                                                                            ),
                                                                                          ),
                                                                                        ),
                                                                                        dataMySchedule.length<=4?Container():Container(
                                                                                        margin:const EdgeInsets.only(bottom: 5,top: 20),
                                                                                        child: DotsIndicator(
                                                                                          dotsCount: 2,
                                                                                          position: activeindexMyclass,
                                                                                          decorator: DotsDecorator(
                                                                                            color: Colorconstands.neutralGrey,
                                                                                            activeColor: Colorconstands.mainColorForecolor,
                                                                                            size: const Size(16.0,6),
                                                                                            activeSize: const Size(35.0, 6.0),
                                                                                            activeShape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                                                                                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                                                                                          ),
                                                                                        ),
                                                                                      ),
                                                        
                                                                                    ],
                                                                                  );
                                                                            } else {
                                                                              return Center(
                                                                                child: Text("WE_DETECT".tr(),style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.lightBlack),textAlign: TextAlign.center,),
                                                                              );
                                                                            }
                                                                          },
                                                                        ),
                                                      
                                                                        // Quick Action 
                                                                        Column(
                                                                          children: [
                                                                            Container(
                                                                              margin: const EdgeInsets.only(left: 14, bottom: 30, top: 22),
                                                                              alignment: Alignment.centerLeft,
                                                                              child: Text(
                                                                                "QUICKACTION".tr(),
                                                                                style: ThemeConstands.button_SemiBold_16.copyWith(color: Colorconstands.lightBlack, height: 1),
                                                                              ),
                                                                            ),
                                                                            Container(
                                                                              margin: const EdgeInsets.only(left: 14),
                                                                              child: Row(
                                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                                children: [
                                                                                  Expanded( 
                                                                                    child: ButttonQuickAction(
                                                                                      onTap: () {
                                                                                        Navigator.pushReplacement(context,
                                                                                          PageRouteBuilder(
                                                                                            pageBuilder: (context, animation1, animation2) => TimeLineScreen(selectIndex: 1,),
                                                                                            transitionDuration: Duration.zero,
                                                                                            reverseTransitionDuration: Duration.zero,
                                                                                          ),
                                                                                        );
                                                                                      },
                                                                                      namebutton: "TIMELINE".tr(),
                                                                                      imageIcon: ImageAssets.tilemline_outline_icon,
                                                                                    ),
                                                                                  ),
                                                                                  Expanded(
                                                                                    child: ButttonQuickAction(
                                                                                      onTap: () { 
                                                                                          Navigator.push(context,
                                                                                          PageRouteBuilder(
                                                                                            pageBuilder: (context, animation1, animation2) =>  TeacherListScreen(
                                                                                              activeChild: activeChild ,
                                                                                            ),
                                                                                            transitionDuration: Duration.zero,
                                                                                            reverseTransitionDuration: Duration.zero,
                                                                                          ),
                                                                                        );
                                                                                      },
                                                                                      namebutton: "TEACHER_LIST".tr(),
                                                                                      imageIcon: ImageAssets.profile2user_outline,
                                                                                    ),
                                                                                  ),
                                                                                  Expanded(
                                                                                    child: ButttonQuickAction(
                                                                                      onTap: () async{ 
                                                                                        var url = Uri.parse("https://t.me");
                                                                                        if (await canLaunchUrl(url)) {
                                                                                          await launchUrl(url);
                                                                                        }
                                                                                      },
                                                                                      namebutton: "TELEGRAM".tr(),
                                                                                      imageIcon: ImageAssets.telegram_icon,
                                                                         
                                                                                    ),
                                                                                  ),
                                                                                ],
                                                                              ),
                                                                            )
                                                                          ],
                                                                        ),
                                                        
                                                                        Row(
                                                                          children: [
                                                                            Expanded(
                                                                              child: Container(
                                                                                padding: const EdgeInsets.only(top: 12,bottom: 22, left: 18,right: 18),
                                                                                child: Text(
                                                                                  "RESULT".tr(),
                                                                                  style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.lightBlack),
                                                                                  textAlign: TextAlign.left,
                                                                                ),
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                        BlocBuilder< MonthlyResultBloc, MonthlyResultState>(
                                                                          builder: (context, state) {
                                                                            if (state is MonthlyResultLoadingState) {
                                                                              return Container();
                                                                            } else if (state is MonthlyResultLoadedState) {
                                                                              var data = state.currentMonthlyResult!.data;
                                                                              return data!.resultMonthly == null
                                                                                  ? EmptyWidget(
                                                                                    title: "WAIT_SCORE_ENTRY".tr(),
                                                                                    subtitle: "WAIT_SCORE_ENTRY_DES".tr(),
                                                                                  )
                                                                                  : CardSummaryResult(
                                                                                    onPressed:(() {
                                                                                      Navigator.pushAndRemoveUntil( context, PageRouteBuilder( pageBuilder: (context, animation1, animation2) => ResultScreenV2(activeStudent: activeChild,),
                                                                                          transitionDuration: Duration.zero,
                                                                                          reverseTransitionDuration: Duration.zero,
                                                                                      ),
                                                                                          (route) => false);
                                                                                    }),
                                                                                      total_average: data.resultMonthly!.maxAvgScore.toString(),
                                                                                      rank: "${data.resultMonthly!.rank.toString()}/${data.totalStudent}",
                                                                                      attendance: data.resultMonthly!.absenceTotal.toString(),
                                                                                      average: data.resultMonthly!.avgScore.toDouble(),
                                                                                      grade: translate=="km"?data.resultMonthly!.grading.toString():data.resultMonthly!.gradingEn.toString(),
                                                                                      lenght_subject: data.subject!.length.toString(),
                                                                                      month: (monthlocal == 1
                                                                                              ? "JANUARY"
                                                                                              : monthlocal == 2
                                                                                                  ? "FEBRUARY"
                                                                                                  : monthlocal == 3
                                                                                                      ? "MARCH"
                                                                                                      : monthlocal == 4
                                                                                                          ? "APRIL"
                                                                                                          : monthlocal == 5
                                                                                                              ? "MAY"
                                                                                                              : monthlocal == 6
                                                                                                                  ? "JUNE"
                                                                                                                  : monthlocal == 7
                                                                                                                      ? "JULY"
                                                                                                                      : monthlocal == 8
                                                                                                                          ? "AUGUST"
                                                                                                                          : monthlocal == 9
                                                                                                                              ? "SEPTEMBER"
                                                                                                                              : monthlocal == 10
                                                                                                                                  ? "OCTOBER"
                                                                                                                                  : monthlocal == 11
                                                                                                                                      ? "NOVEMBER"
                                                                                                                                      : "DECEMBER").tr(),
                                                                                      performance: data.resultMonthly!.performancePercentage.toDouble(),
                                                                              );
                                                                            } else {
                                                                              return Center(
                                                                                child: Text("WE_DETECT".tr(),style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.lightBlack),textAlign: TextAlign.center,),
                                                                              );
                                                                            }
                                                                          },
                                                                        ),
                                                                       
                                                                        const SizedBox(
                                                                          height:
                                                                              25,
                                                                        ),
                                                                        // Row(
                                                                        //   children: [
                                                                        //     Expanded(
                                                                        //       child: Container(
                                                                        //         padding: const EdgeInsets.only(top: 12,bottom: 22, left: 18,right: 18),
                                                                        //         child: Text(
                                                                        //           "TIMELINE".tr(),
                                                                        //           style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.lightBlack),
                                                                        //           textAlign: TextAlign.left,
                                                                        //         ),
                                                                        //       ),
                                                                        //     ),
                                                                        //   ],
                                                                        // ),
                                                                        // Container(
                                                                        //   height: 300,
                                                                        //   padding: const EdgeInsets.all(9),
                                                                        //   child: IconButton(onPressed: () {
                                                                        //   print("1234567610 ${BlocProvider.of<ChangeChildBloc>(context).classIDBloc} && ${BlocProvider.of<ChangeChildBloc>(context).activeChild }");
                                                                        //   Navigator.pushReplacement(context,
                                                                        //       PageRouteBuilder(
                                                                        //         pageBuilder: (context, animation1, animation2) => TimeLineScreen(selectIndex: 1,),
                                                                        //         transitionDuration: Duration.zero,
                                                                        //         reverseTransitionDuration: Duration.zero,
                                                                        //       ),
                                                                        //     );
                                                                        //   }, 
                                                                        // icon:const Icon(Icons.home),),
                                                                      
                                                                        // ),
                                                                      
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  );
                                            } else if (state
                                                is ChangeChildErrorState) {
                                              return Container(
                                               decoration:const BoxDecoration(
                                                  color: Colorconstands.neutralWhite,
                                                  borderRadius: BorderRadius.only(topLeft: Radius.circular(15),topRight:  Radius.circular(15))
                                                ),
                                                child: ErrorRequestData(
                                                  onPressed: (){
                                                    Navigator.pushReplacement(
                                                        context, 
                                                        PageRouteBuilder(
                                                            pageBuilder: (context, animation1, animation2) => const HomeScreen(),
                                                            transitionDuration: Duration.zero,
                                                            reverseTransitionDuration: Duration.zero,
                                                        ),
                                                    );
                                                  },
                                                  discription: '', 
                                                  hidebutton: true, 
                                                  title: 'WE_DETECT_ERROR'.tr(),

                                                ),
                                              );
                                            } else {
                                              return Container(
                                                decoration:const BoxDecoration(
                                                  color: Colorconstands.neutralWhite,
                                                  borderRadius: BorderRadius.only(topLeft: Radius.circular(15),topRight:  Radius.circular(15))
                                                ),
                                                child: ErrorRequestData(
                                                  onPressed: (){
                                                    Navigator.pushReplacement(
                                                        context, 
                                                        PageRouteBuilder(
                                                            pageBuilder: (context, animation1, animation2) => const HomeScreen(),
                                                            transitionDuration: Duration.zero,
                                                            reverseTransitionDuration: Duration.zero,
                                                        ),
                                                    );
                                                  },
                                                  discription: '', 
                                                  hidebutton: true, 
                                                  title: 'WE_DETECT_ERROR'.tr(),

                                                ),
                                              );
                                            }
                                          },
                                        ),
                                      ),
                                      paid == false
                                          ? activeChild != 0
                                              ? LockScreenUnpaidHomeScreenWidget(
                                                  subtitle:
                                                      'PLEASE_PAY_THE_BILL'.tr(),
                                                  title:
                                                      'YOU_CAN_NOT_ACCESS'.tr(),
                                                )
                                              : Container()
                                          : Container()
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              isoffline == true
                  ? Container()
                  : Positioned(
                      bottom: 0,
                      left: 0,
                      right: 0,
                      top: 0,
                      child: Container(
                        color: const Color(0x7B9C9595),
                      ),
                    ),
              AnimatedPositioned(
                bottom: isoffline == true ? -150 : 0,
                left: 0,
                right: 0,
                duration: const Duration(milliseconds: 500),
                child: const NoConnectWidget(),
              ),
              updateVersion==false?Container(): const Positioned(
              top: 0,left: 0,right: 0,bottom: 0,
              child: WidgetUpdate(),

            ),
        // ================= check video guide ==============
            // videoGuideStorage == "SeenVideoDaskBoard"?Container(height: 0,width: 0,):BlocListener<UserGuidelineBloc, UserGuidelineState>(
            //   listener: (context, state) {
            //     if(state is UserGuidelineSuccessState){
            //       String linkVideo = "";
            //       var data = state.userGuidelinrmodel.data;
            //       for (var i = 0; i < data!.length; i++) {
            //         if (data[i].feature == 1) {
            //           setState(() {
            //             linkVideo = data[i].file.toString();
            //           });
            //         }
            //       }
            //         showModalBottomSheet(
            //           isScrollControlled:true,
            //           enableDrag:false,
            //           context: context,
            //           builder: (context) {
            //             return StatefulBuilder(builder:(BuildContext context,StateSetter stateSetter) {
            //               return VideoGuideScreen(linkVideo: linkVideo.toString());
            //             });});
                  
            //     }
            //   },
            //   child: Container(height: 0,width: 0,),
            // )
            ],
          ),
        ),
      ),
    );
  }




}
