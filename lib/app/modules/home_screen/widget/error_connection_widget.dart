import 'dart:async';
import 'dart:io';
import 'package:camis_application_flutter/app/modules/home_screen/view/home_screen.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../component/change_child/bloc/change_child_bloc.dart';
import '../../../../service/apis/profile_user/bloc/get_profile_user_bloc.dart';
import '../../../../storages/get_storage.dart';
import '../../../../widgets/no_connection_alert_widget.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/themes/themes.dart';
import '../../PaymentAndBillScreen/bloc/bloc/check_payment_expire_bloc.dart';
import '../../time_line/school_time_line/e.schooltimeline.dart';

class ErroreWidgetConnection extends StatefulWidget {
  const ErroreWidgetConnection({Key? key}) : super(key: key);

  @override
  State<ErroreWidgetConnection> createState() => _ErroreWidgetConnectionState();
}

class _ErroreWidgetConnectionState extends State<ErroreWidgetConnection> {
  GetStoragePref _pref = GetStoragePref();
  String? schoolName;
  int? monthlocal;
  int? daylocal;
  StreamSubscription? internetconnection;
  bool? isoffline;
  void getLocalData() async {
    var schoolname = await _pref.getJsonToken;
    setState(() {
      schoolName = schoolname.schoolName.toString();
    });
  }

  Future checkInterNet() async{
    try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              isoffline = true;
            });
          }
        } on SocketException catch (_) {
          setState(() {
            isoffline = false;
          });
        }
  }
  @override
  void initState()  {
    getLocalData();
    setState((){
      checkInterNet();
      //=============Check internet====================
      internetconnection = Connectivity()
          .onConnectivityChanged
          .listen((ConnectivityResult result) {
        if (result == ConnectivityResult.none) {
          setState(() {
            isoffline = false;
          });
        } else if (result == ConnectivityResult.mobile) {
          setState(() {
            isoffline = true;
            BlocProvider.of<ChangeChildBloc>(context).add(GetChildrenEvent());
            BlocProvider.of<GetProfileUserBloc>(context).add(GetProfileUser());
            BlocProvider.of<CheckPaymentExpireBloc>(context).add(CheckPaymentEvent());
            Navigator.pushAndRemoveUntil(context,
                              PageRouteBuilder(
                              pageBuilder: (context, animation1, animation2) => const HomeScreen(),transitionDuration: Duration.zero,reverseTransitionDuration: Duration.zero,),(route) => false);
          });
        } else if (result == ConnectivityResult.wifi) {
          setState(() {
            isoffline = true;
            BlocProvider.of<ChangeChildBloc>(context).add(GetChildrenEvent());
            BlocProvider.of<GetProfileUserBloc>(context).add(GetProfileUser());
            BlocProvider.of<CheckPaymentExpireBloc>(context).add(CheckPaymentEvent());
            Navigator.pushAndRemoveUntil(context,
                              PageRouteBuilder(
                              pageBuilder: (context, animation1, animation2) => const HomeScreen(),
                              transitionDuration: Duration.zero,
                              reverseTransitionDuration: Duration.zero,
                          ),(route) => false);
        });
        }
      });
      //=============Eend Check internet====================
    });
    monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
    daylocal = int.parse(DateFormat("dd").format(DateTime.now()));
    super.initState();
  }
  @override
  void deactivate() {
    internetconnection!.cancel();
    super.deactivate();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 247, 248, 249),
      body: WillPopScope(
        onWillPop: () {
          return exit(0);
        },
        child: Container(
          color: Colorconstands.primaryColor,
          child: Stack(
            children: [
              Positioned(
                right: 0,
                top: 0,
                child: Container(
                  child: SvgPicture.asset("assets/images/Path_home_1.svg"),
                ),
              ),
              Positioned(
                  left: 0,
                  top: 50,
                  child: SvgPicture.asset(
                    ImageAssets.path20,
                    color: Colorconstands.secondaryColor,
                  )),
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                child: Container(
                  margin: const EdgeInsets.only(top: 37),
                  child: Column(
                    children: [
                      Container(
                        margin: const EdgeInsets.symmetric(
                            horizontal: 24, vertical: 14),
                        child: Row(
                          children: [
                            Expanded(
                              child: Container(
                                margin: const EdgeInsets.only(left: 10),
                                alignment: Alignment.centerLeft,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      child: Text(
                                        "$schoolName",
                                        style: ThemeConstands
                                            .subtitle1_Regular_16
                                            .copyWith(
                                                color: Colorconstands
                                                    .darkBackgroundsDisabled),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Container(
                                      child: Text(
                                        "សួស្តី .......",
                                        style: ThemeConstands
                                            .headline3_SemiBold_20
                                            .copyWith(
                                                color: Colorconstands
                                                    .lightTextsRegular),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(
                              width: 15,
                            ),
                            Container(
                              width: 64,
                              decoration: BoxDecoration(
                                  color: Colorconstands.neutralWhite
                                      .withOpacity(0.2),
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(10))),
                              child: Container(
                                padding: const EdgeInsets.only(
                                    top: 3, bottom: 12, left: 5, right: 5),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      child: Text(daylocal.toString(),
                                          style: ThemeConstands
                                              .headline1_SemiBold_32
                                              .copyWith(
                                                  color: Colorconstands
                                                      .neutralWhite,
                                                  height: 1.3),
                                          textAlign: TextAlign.center),
                                    ),
                                    Center(
                                        child: Text(
                                      "${(monthlocal == 1 ? "JANUARY" : monthlocal == 2 ? "FEBRUARY" : monthlocal == 3 ? "MARCH" : monthlocal == 4 ? "APRIL" : monthlocal == 5 ? "MAY" : monthlocal == 6 ? "JUNE" : monthlocal == 7 ? "JULY" : monthlocal == 8 ? "AUGUST" : monthlocal == 9 ? "SEPTEMBER" : monthlocal == 10 ? "OCTOBER" : monthlocal == 11 ? "NOVEMBER" : "DECEMBER").tr()}  ",
                                      style: ThemeConstands.subtitle1_Regular_16
                                          .copyWith(
                                              color:
                                                  Colorconstands.neutralWhite,
                                              height: 1),
                                      textAlign: TextAlign.center,
                                    ))
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Container(
                          child: Column(
                            children: [
                              Expanded(
                                child: Container(
                                  color: Colorconstands.neutralWhite,
                                  child: Stack(
                                    children: [
                                      Container(
                                        decoration: const BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(12),
                                                topRight: Radius.circular(12))),
                                        child: const ShimmerDask()
                                      ),
                                      AnimatedPositioned(
                                        bottom: isoffline == true ? -150 : 0,
                                        left: 0,
                                        right: 0,
                                        duration: const Duration(milliseconds: 500),
                                        child: const NoConnectWidget(),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );

  }
}