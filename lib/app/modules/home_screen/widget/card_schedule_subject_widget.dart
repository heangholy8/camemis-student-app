import 'package:blurrycontainer/blurrycontainer.dart';
import 'package:camis_application_flutter/app/modules/auth_screens/e_school_code.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../../widgets/lock_widget/lock_widget.dart';
import '../../../core/resources/asset_resource.dart';
import '../../check_attendance_screen/bloc/check_attendance_bloc.dart';
import '../../check_attendance_screen/view/attendance_entry_screen.dart';

class CardScheduleSubjectDaskWidget extends StatelessWidget {
  final String? subjectColor;
  final bool? scheduleExamType;
  final String? titleSubject;
  final String? timeSubject;
  final String? examDate;
  final bool? isActive;
  final List? homeList;
  final String? attStatus;
  final bool? paid;
  final int?index;
  final int? isTakeAttendance;
  final int?randomIndex;
  final int? leadership;
  final String? classId;
  final String? scheduleId;
  final String?date;
  final String?studentId;
  const CardScheduleSubjectDaskWidget({
    Key? key,
    this.examDate,
    required this.classId,
    required this.studentId,
    required this.date,
    required this.scheduleId,
    required this.leadership,
    this.isTakeAttendance,
    this.subjectColor,
    this.titleSubject,
    this.scheduleExamType,
    this.isActive,
    this.timeSubject,
    this.homeList,
    this.attStatus,
    this.paid,
    this.index,
    this.randomIndex,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Row(
        children: [
          isActive == true
              ? Container(
                  margin: const EdgeInsets.only(right: 5.0),
                  width: 15.0,
                  height: 13.0,
                  decoration:const BoxDecoration(
                    color: Colorconstands.primaryColor,
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(10.0),
                      bottomRight: Radius.circular(10.0),
                    ),
                  ),
                )
              : const SizedBox(width: 20.0),
          Expanded(
            child: Container(
            padding: const EdgeInsets.symmetric(
                vertical: 15.0, horizontal: 14.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  alignment: Alignment.center,
                  width: 30,
                  height: 30,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color(int.parse(subjectColor!))
                  ),
                  child: SvgPicture.asset(scheduleExamType==true?ImageAssets.doucment_check_icon:ImageAssets.book,width: 20,height: 20,color: Colorconstands.neutralWhite,),
                ),
                const SizedBox(
                  width: 9.0,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            timeSubject!,
                            style: isActive==true?ThemeConstands.headline5_SemiBold_16:ThemeConstands.button_SemiBold_16
                                .copyWith(
                                    height: 1,
                                    color: Colorconstands.lightBlack),
                          ),
                          homeList!.isEmpty?Container(
                            height: 25,
                          ): Container(
                            height: 25,
                            padding:const EdgeInsets.all(4),
                            decoration: BoxDecoration(
                              color: Color(int.parse(subjectColor!)).withOpacity(0.1),
                              shape: BoxShape.circle
                            ),
                            child: Center(
                              child: IconTitleWidget(
                                icon: ImageAssets.HOMEWORK_ICON,
                                subjectColor: Color(int.parse(subjectColor!))),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 5,),
                      Text(
                        titleSubject!,
                        style: ThemeConstands.headline6_Regular_14_20height
                            .copyWith(
                                height: 1,
                                color: Colorconstands.darkTextsPlaceholder),
                      ),
                    ],
                  ),
                ),
                leadership == 1 && isTakeAttendance==0? Container(): isTakeAttendance == 1 || isActive ==true || attStatus == "UNQUOTED" ?  paid==true? GestureDetector(
                  onTap: leadership == 1?(){
                      BlocProvider.of<CheckAttendanceBloc>(context).add(
                      GetCheckStudentAttendanceEvent(classId.toString(),scheduleId.toString(),date.toString()),
                    );
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => AttendanceEntryScreen(studentId:studentId.toString()),
                      ),
                    );
                  }:(){},
                  child: Container(
                    padding:const EdgeInsets.symmetric(horizontal: 15,vertical: 6),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color:attStatus =="PRESENT"? Colorconstands.alertsPositive:attStatus=="ABSENT"?Colorconstands.alertsDecline:attStatus =="LATE"?Colorconstands.alertsAwaitingText:Colorconstands.neutralGrey,
                    ),
                    child: Row(
                      children: [
                        attStatus =="ABSENT"?const Icon(Icons.close_rounded,size: 18,color: Colorconstands.neutralWhite,):SvgPicture.asset(attStatus =="PRESENT"? ImageAssets.checkIcon:attStatus =="LATE"? ImageAssets.clockIcon:ImageAssets.qnquotedIcon, color:attStatus=="UNQUOTED"||attStatus=="PERMISSION"?Colorconstands.neutralDarkGrey:Colorconstands.neutralWhite,),
                        const SizedBox(width: 6,),
                        Text(attStatus=="PRESENT"?"PRESENT".tr(): attStatus=="ABSENT"?"ABSENT".tr() :attStatus=="LATE"?"LATE".tr():attStatus=="PERMISSION"?"PERMISSION".tr():"UNQUOTED".tr(),style: ThemeConstands.headline6_SemiBold_14.copyWith(color:attStatus=="UNQUOTED"||attStatus=="PERMISSION"?Colorconstands.neutralDarkGrey:Colorconstands.neutralWhite),)
                      ],
                    ),
                  ),
                ):index==randomIndex?GestureDetector(
                  onTap: leadership == 1?(){
                      BlocProvider.of<CheckAttendanceBloc>(context).add(
                      GetCheckStudentAttendanceEvent(classId.toString(),scheduleId.toString(),date.toString()),
                    );
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => AttendanceEntryScreen(studentId:studentId.toString()),
                      ),
                    );
                  }:(){},
                  child: Container(
                    padding:const EdgeInsets.symmetric(horizontal: 15,vertical: 6),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color:attStatus =="PRESENT"? Colorconstands.alertsPositive:attStatus=="ABSENT"?Colorconstands.alertsDecline:attStatus =="LATE"?Colorconstands.alertsAwaitingText:Colorconstands.neutralGrey,
                    ),
                    child: Row(
                      children: [
                        attStatus =="ABSENT"?const Icon(Icons.close_rounded,size: 18,color: Colorconstands.neutralWhite,): SvgPicture.asset(attStatus =="PRESENT"? ImageAssets.checkIcon:attStatus =="LATE"? ImageAssets.clockIcon:ImageAssets.qnquotedIcon, color:attStatus=="UNQUOTED"?Colorconstands.neutralDarkGrey:Colorconstands.neutralWhite,),
                        const SizedBox(width: 6,),
                        Text(attStatus=="PRESENT"?"PRESENT".tr():attStatus=="ABSENT"?"ABSENT".tr():attStatus=="LATE"?"LATE".tr():attStatus=="PERMISSION"?"PERMISSION".tr():"UNQUOTED".tr(),style: ThemeConstands.headline6_SemiBold_14.copyWith(color:attStatus=="UNQUOTED"?Colorconstands.neutralDarkGrey:Colorconstands.neutralWhite),)
                      ],
                    ),
                  ),
                ):const LockWidget():Container(),

            leadership == 1 && isTakeAttendance==0?  
            Container(
              child: MaterialButton(
                elevation: 2,
                color: Colorconstands.neutralGrey,
                shape:const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                ),
                padding:const EdgeInsets.all(0),
                onPressed: (){
                    BlocProvider.of<CheckAttendanceBloc>(context).add(
                      GetCheckStudentAttendanceEvent(classId.toString(),scheduleId.toString(),date.toString()),
                    );
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => AttendanceEntryScreen(studentId:studentId.toString()),
                      ),
                    );
                },
                child: Container(
                  padding:const EdgeInsets.symmetric(horizontal: 15,vertical: 2),
                  child: Row(
                    children: [
                      SvgPicture.asset(ImageAssets.qnquotedIcon, color:Colorconstands.neutralDarkGrey),
                      const SizedBox(width: 5,),
                      Text("NOT_QUOTED".tr(),style: ThemeConstands.headline6_SemiBold_14.copyWith(color:Colorconstands.neutralDarkGrey),),
                    ],
                  ),
                ),
              )
            )
            :isTakeAttendance == 0 && isActive == false && attStatus != "UNQUOTED"?   
             SizedBox(
                  width: 160,
                  child: Stack(
                    children: [
                    Positioned(
                      child: BlurryContainer(
                          padding: const EdgeInsets.all(0),
                          borderRadius : const BorderRadius.only(topRight:Radius.circular(0)),
                          blur: 4,
                          child: Container(   
                            height: 50,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(color:const Color(0xFF6163F5).withOpacity(0.2),
                              borderRadius: BorderRadius.circular(11)
                            ),
                            child: Text("CHECKINGATTEN".tr(),style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.primaryColor),           
                            textAlign: TextAlign.center,),       
                          ),
                        ),
                      )     
                    ],
                  ),
                )     
                :Container(),
              ],
            )
            ),
          ),
        ],
      ),
    );
  }
}
class IconTitleWidget extends StatelessWidget {
  final String icon;
  const IconTitleWidget({ Key? key,
    required this.icon,
    required this.subjectColor,
  });

  final Color? subjectColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SvgPicture.asset(
        icon,
        height: 18,
        color: subjectColor ?? Colorconstands.primaryColor,
      ),
    );
  }
}
