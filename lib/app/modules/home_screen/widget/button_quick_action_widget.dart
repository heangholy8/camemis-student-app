
import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:flutter/material.dart';import 'package:flutter_svg/svg.dart';

import '../../../core/themes/themes.dart';

class ButttonQuickAction extends StatelessWidget {
  final String namebutton;
  final String imageIcon;
  final VoidCallback onTap;
   Color? iconColor;
   ButttonQuickAction({Key? key, required this.namebutton, required this.imageIcon, required this.onTap,this.iconColor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        MaterialButton(
          elevation: 1,
          color: Colorconstands.neutralWhite,
          onPressed: onTap,
          padding:const EdgeInsets.all(16),
          shape: const CircleBorder(),
          child: SvgPicture.asset(imageIcon,width: 32,color:iconColor?? Colorconstands.primaryColor,fit: BoxFit.fill,),
        ),
        const SizedBox(height: 5,),
        Container(
          margin:const EdgeInsets.symmetric(vertical: 8,horizontal: 5),
          child: Text(namebutton,textAlign: TextAlign.center,style: ThemeConstands.headline6_SemiBold_14.copyWith(color:Colorconstands.neutralDarkGrey,)),
        )
      ],
    );
  }
}