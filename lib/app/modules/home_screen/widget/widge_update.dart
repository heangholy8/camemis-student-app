import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../helper/check_platform_device.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/themes/themes.dart';

class WidgetUpdate extends StatefulWidget {
  const WidgetUpdate({Key? key}) : super(key: key);

  @override
  State<WidgetUpdate> createState() => _WidgetUpdateState();
}

class _WidgetUpdateState extends State<WidgetUpdate> {
  var platform = checkPlatformDevice();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colorconstands.neutralDarkGrey.withOpacity(0.6),
      child: Center(
        child: Container(
               margin:const EdgeInsets.all(42),
                height: 380,
                 decoration: BoxDecoration(
                        color: Colorconstands.neutralWhite,
                        borderRadius: BorderRadius.circular(15)
                      ),
                child: Column(
                  children: [
                    Container(
                      height: 150,
                      decoration:const BoxDecoration(
                        color: Colorconstands.neutralWhite,
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(15),topRight: Radius.circular(15))
                      ),
                      child: Center(
                        child: Container(
                          height: 80,width: 80,
                          decoration:const BoxDecoration(
                            color: Colorconstands.neutralGrey,
                            shape: BoxShape.circle
                          ),
                          padding:const EdgeInsets.all(10),
                          child: SvgPicture.asset(ImageAssets.logoCamis,width: 45,height: 45,color: Colorconstands.primaryColor,),
                        ),
                      ),
                    ),
                    Container(
                      child: Text("Update New!",style: ThemeConstands.headline3_SemiBold_20.copyWith(color: Colorconstands.lightBlack),),
                    ),
                    const SizedBox(height: 22,),
                    Container(
                      margin:const EdgeInsets.symmetric(horizontal: 18),
                      child: Text("UPDATE_VERSION_DISCRIPTION".tr(),style: ThemeConstands.headline4_Regular_18.copyWith(color: Colorconstands.lightBlack),textAlign: TextAlign.center,),
                    ),
                    Expanded(
                      child: Container(),
                    ),
                    Container(
                      margin:const EdgeInsets.symmetric(vertical: 28,horizontal: 28),
                      height: 60,
                       child: MaterialButton(
                        color: Colorconstands.secondaryColor,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15),
                          ),
                        padding:const EdgeInsets.all(0),
                        onPressed: (){
                          setState(() {
                            if(platform=="Android"){
                              launchUrl(
                                Uri.parse("market://details?id=com.camis.camemis_student_application",),
                                mode: LaunchMode.externalApplication,
                              );
                            }
                            else{
                              launchUrl(
                                Uri.parse("https://apps.apple.com/app/id6443797596",),
                                mode: LaunchMode.externalApplication,
                              );
                            }
                            
                          });
                        },
                        child: Center(
                          child: Text("Update Now",style: ThemeConstands.headline3_SemiBold_20.copyWith(color: Colorconstands.neutralWhite),),
                        ),
                      ),
                    )
                  ],
                ),
              ),
      ),
    );
  }
}