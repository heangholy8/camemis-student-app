import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../core/resources/asset_resource.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class CardAnounment extends StatelessWidget {
  const CardAnounment({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width-40,
      margin:const EdgeInsets.only(right: 8),
      decoration: BoxDecoration(
            color: Colorconstands.neutralWhite,
            borderRadius: BorderRadius.circular(12),
          ),
      child: MaterialButton(
        onPressed: (){},
         shape:const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12))),
          padding:const EdgeInsets.all(0),
        child: Container(
          
          child: Stack(
            fit: StackFit.passthrough,
            clipBehavior: Clip.none,
            children: [
              Positioned(
                bottom: 0,right: 0,
                child:ClipRRect(
                  borderRadius: BorderRadius.circular(12.0), //add border radius
                  child: SvgPicture.asset(
                      "assets/icons/svg/Red_anounment_back.svg",
                      fit:BoxFit.cover,
                  ),
                )
              ),
              Positioned(
                top: 0,left: 0,right: 0,bottom: 0,
                child: Container(
                  margin:const EdgeInsets.all(14),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding:const EdgeInsets.only(bottom: 4,top: 9,left: 12,right: 12),
                        decoration: BoxDecoration(
                          color: Colorconstands.alertsDecline,
                          borderRadius: BorderRadius.circular(12)
                        ),
                        child: Text("ដំណឹងបន្ទាន់របស់សាលា",style: ThemeConstands.overline_Semibold_12.copyWith(color: Colorconstands.neutralWhite),),
                      ),
                      Container(
                        padding:const EdgeInsets.only(top: 8),
                        child: Text("ប្រកាសបិទសាលាបណ្ដោះអាសន្ន ទប់ស្កាត់ជំងឺ",style: ThemeConstands.button_SemiBold_16.copyWith(color: Colorconstands.primaryColor),maxLines: 1,overflow: TextOverflow.ellipsis,),
                      ),
                      Container(
                        child: Text("តាមរយៈលិខិត បានឱ្យដឹងថា ដោយសារមានការឆ្លងជំងឺកូវីដ-១៩ ក្នុងសហគមន៍ នៅក្នុងស្រុកគិរីសាគរ ខេត្តកោះកុង និងដើម្បី ចៀសវាង",style: ThemeConstands.caption_Regular_12.copyWith(color: Colorconstands.neutralDarkGrey,height: 1.89),maxLines: 2,overflow: TextOverflow.ellipsis,),
                      ),
                      Container(
                        padding:const EdgeInsets.only(top: 12),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                               padding:const EdgeInsets.only(bottom: 6,right: 7),
                              child: SvgPicture.asset(ImageAssets.current_date,height: 14,color: Colorconstands.darkTextsPlaceholder,),
                            ),
                            Expanded(child: Text("០៨ តុលា ២០២២",style: ThemeConstands.caption_Regular_12.copyWith(color: Colorconstands.darkTextsPlaceholder),maxLines: 1,overflow: TextOverflow.ellipsis,)),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                right: 12,
                top: 12,
                child: GestureDetector(
                  onTap: (){},
                   child:const Icon(Icons.close)),
              )
            ],
          ),
        ),
      ),
    );
  }
}