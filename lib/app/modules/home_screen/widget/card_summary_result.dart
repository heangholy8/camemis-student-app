import 'package:dotted_line/dotted_line.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../core/resources/asset_resource.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class CardSummaryResult extends StatelessWidget {
  final double? average;
  final String? total_average;
  final String? month;
  final String? lenght_subject;
  final String? grade;
  final String? rank;
  final String? attendance;
  final double performance;
  final VoidCallback onPressed;
  const CardSummaryResult({Key? key,required this.onPressed,required this.average,required this.total_average,required this.month,required this.lenght_subject,required this.grade,required this.rank,required this.attendance,required this.performance}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal:18),
      decoration: BoxDecoration(
        color: Colorconstands.neutralWhite,
        borderRadius:BorderRadius.circular(16),
        boxShadow: const [
          BoxShadow(
            color: Color.fromARGB(47, 24, 85, 171),
            blurRadius: 20,
            spreadRadius: 2,
            offset: Offset(8, 8),
          ),
          BoxShadow(
            color: Colorconstands.neutralWhite,
            blurRadius: 20,
            spreadRadius: 2,
            offset: Offset(-12, -12),
          ),
        ]
      ),
      child:MaterialButton(
        padding:const EdgeInsets.all(0),
        onPressed: onPressed,
        child: Container(
          decoration: const BoxDecoration(
                    gradient: LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [
                      Colorconstands.primaryColor,
                      Color(0xDB1654AA),
                      Color(0xDB4E92F1),
                    ]),
                    borderRadius: BorderRadius.all(Radius.circular(16))),
          child: Row(
            children: [
              Container(
                padding:const EdgeInsets.symmetric(vertical: 12,horizontal: 18),
                child:Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset(ImageAssets.doucment_check_icon,color: Colorconstands.neutralWhite,width: 18,height: 18,),
                          const SizedBox(width: 8,),
                          Text("AVERAGE_SCORE".tr(),style: ThemeConstands.overline_Semibold_12.copyWith(color: Colorconstands.neutralWhite))
                        ],
                      ),
                    ),
                    const SizedBox(height: 22,),
                    Container(
                      child: Text("${"MONTH".tr()} $month",style: ThemeConstands.caption_Regular_12.copyWith(color: Colorconstands.neutralWhite),),
                    ),
                    const SizedBox(height: 22,),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text("$average",style: ThemeConstands.headline2_SemiBold_24.copyWith(color: Colorconstands.neutralWhite),),
                          Container(margin:const EdgeInsets.only(top: 4),child: Text("/",style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.neutralGrey),)),
                          Container(margin:const EdgeInsets.only(top: 4),child: Text("$total_average",style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.neutralGrey),)),
                        ],
                      ),
                    ),
                    const SizedBox(height: 22,),
                    Container(
                      child: Text("$lenght_subject ${"SUBJECT".tr()}",style: ThemeConstands.caption_Regular_12.copyWith(color: Colorconstands.neutralWhite),),
                    ),
                  ],
                ),
              ),
              Expanded(
                child:Container(
                  padding:const EdgeInsets.symmetric(vertical: 0,horizontal: 18),
                  decoration:const BoxDecoration(color: Colorconstands.white, borderRadius: BorderRadius.only(topRight: Radius.circular(16), bottomRight: Radius.circular(16))),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        margin:const EdgeInsets.symmetric(vertical: 12),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Container(
                                    child: SvgPicture.asset(ImageAssets.CHART_ICON,color: Colorconstands.lightBlack,),
                                  ),
                                  const SizedBox(width: 5,),
                                  Text("RANK".tr(),style: ThemeConstands.headline6_Regular_14_20height.copyWith(color:Colorconstands.neutralDarkGrey),)
                                ],
                              ),
                            ),
                            Text("$rank",style: ThemeConstands.headline6_SemiBold_14.copyWith(color:Colorconstands.mainColorSecondary),)
                          ],
                        ),
                      ),
                      const DottedLine(dashColor: Colorconstands.neutralGrey,),
                      Container(
                        margin:const EdgeInsets.symmetric(vertical: 12),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Container(
                                    child: SvgPicture.asset(ImageAssets.AWARD_ICON,color: Colorconstands.lightBlack,),
                                  ),
                                  const SizedBox(width: 5,),
                                  Text("GRADE".tr(),style: ThemeConstands.headline6_Regular_14_20height.copyWith(color:Colorconstands.neutralDarkGrey),)
                                ],
                              ),
                            ),
                            Container(
                              padding:const EdgeInsets.symmetric(vertical: 4,horizontal: 12,),
                              decoration: BoxDecoration(
                                color: Colorconstands.alertsPositive,
                                borderRadius: BorderRadius.circular(12)
                              ),
                            child: Text(" $grade",style: ThemeConstands.headline6_SemiBold_14.copyWith(color:Colorconstands.neutralWhite),))
                          ],
                        ),
                      ),
                      const DottedLine(dashColor: Colorconstands.neutralGrey,),
                      Container(
                        margin:const EdgeInsets.symmetric(vertical: 12),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Container(
                                    child: SvgPicture.asset(ImageAssets.CALENDER_REMOVE_ICON,color: Colorconstands.lightBlack,),
                                  ),
                                  const SizedBox(width: 5,),
                                  Text("ABSENT".tr(),style: ThemeConstands.headline6_Regular_14_20height.copyWith(color:Colorconstands.neutralDarkGrey),)
                                ],
                              ),
                            ),
                            Text("$attendance ${"TIME".tr()}",style: ThemeConstands.headline6_SemiBold_14.copyWith(color:Colorconstands.neutralDarkGrey),)
                          ],
                        ),
                      ),
                      const DottedLine(dashColor: Colorconstands.neutralGrey,),
                      Container(
                        margin:const EdgeInsets.symmetric(vertical: 12),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: Container(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Container(
                                      child: SvgPicture.asset(ImageAssets.TREND_UP_ICON,color: Colorconstands.lightBlack,),
                                    ),
                                    const SizedBox(width: 5,),
                                    Expanded(child: Text("PERFORMANCE".tr(),style: ThemeConstands.headline6_Regular_14_20height.copyWith(color:Colorconstands.neutralDarkGrey),))
                                  ],
                                ),
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Icon(performance<0?Icons.arrow_downward_outlined:performance==0?Icons.arrow_back:Icons.arrow_upward_outlined,size: 18,color:performance <0 || performance==0?Colorconstands.alertsDecline:Colorconstands.alertsPositive),
                                Text(" $performance%",style: ThemeConstands.headline6_SemiBold_14.copyWith(color:performance <0 || performance==0?Colorconstands.alertsDecline:Colorconstands.alertsPositive),),
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}