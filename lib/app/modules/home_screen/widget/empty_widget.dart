

import 'package:flutter/material.dart';

import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class EmptyWidget extends StatelessWidget {
  final String title;
  final String subtitle;
  const EmptyWidget({Key? key, required this.title, required this.subtitle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 22, vertical: 12),
      child: Column(
        children: [
          Text(
            title,
            style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.lightBlack),
            textAlign: TextAlign.center,
          ),
          const SizedBox(
            height: 22,
          ),
          Text(
            subtitle,
            style: ThemeConstands.headline6_Regular_14_20height.copyWith(color: Colorconstands.darkTextsPlaceholder),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}