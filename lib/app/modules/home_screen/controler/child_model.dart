class Child {
  final String image;
  final String name;
  final String nameKh;
  final String grade;
  const Child(
      {required this.image,
      required this.name,
      required this.grade,
      required this.nameKh});
}

const allChilds = [
  Child(
      image:"http://www.ericlafforgue.com/imagecache/f-albums-d/cambodia/camboweb0029.jpg",
      name: 'Nirath',
      grade: '7 D',
      nameKh: ""),
  Child(
      image:
          'https://media.istockphoto.com/photos/cambodian-schoolboy-doing-homework-near-tonle-sap-cambodia-picture-id865135890?k=20&m=865135890&s=612x612&w=0&h=Lw9apwy6iTwPs2LROLpYsFshr2fElClLDEz73ScNoF0=',
      name: 'Phasith',
      grade: '7 A',
      nameKh: "ធ"),
  Child(
      image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQHgS3FGmkIGyL0M9Rfib0EefHkhncU3ZacENC6twuXzdaFWiIySn_BlfLFeeuswwj4uXo&usqp=CAU',
      name: 'Chakriya',
      grade: '4 B',
      nameKh: ""),
];
