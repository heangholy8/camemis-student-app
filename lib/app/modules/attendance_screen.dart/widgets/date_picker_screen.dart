import 'package:flutter/material.dart';

import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class DatePickerWidget extends StatefulWidget {
  final String weekday;
  final String day;
  final bool selectedday;
  final bool eveninday;
  final bool disable;
  final VoidCallback onPressed;
  const DatePickerWidget({Key? key, required this.selectedday, required this.onPressed, required this.weekday, required this.day, required this.eveninday, required this.disable}) : super(key: key);

  @override
  State<DatePickerWidget> createState() => _DatePickerWidgetState();
}

class _DatePickerWidgetState extends State<DatePickerWidget> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  widget.selectedday;
  }
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return InkWell(
              onTap: widget.onPressed,
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 2.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12.0),
                  color: widget.selectedday == true
                      ? Colorconstands.primaryColor
                      : Colors.transparent,
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 0.0, vertical: 8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        widget.day,
                        style:widget.selectedday == true?ThemeConstands.headline6_SemiBold_14.copyWith(
                          color: widget.selectedday == true
                              ? const Color(0xFFFFFFFF)
                              :widget.day=="អាទិត្យ"||widget.day=="SU" || widget.disable==true ?Colorconstands.lightTrunks : Colorconstands.neutralDarkGrey,
                        ):ThemeConstands.headline6_Regular_14_20height.copyWith(
                          color: widget.selectedday == true
                              ? const Color(0xFFFFFFFF)
                              :widget.day=="អាទិត្យ"||widget.day=="SU" || widget.disable==true ?Colorconstands.lightTrunks : Colorconstands.neutralDarkGrey,
                        ),
                      ),
                      Text(
                        widget.weekday,
                        style: ThemeConstands.headline5_SemiBold_16.copyWith(
                          color: widget.selectedday == true
                              ? const Color(0xFFFFFFFF)
                              :widget.day=="អាទិត្យ" ||widget.day=="SU" || widget.disable==true ?Colorconstands.lightTrunks :const Color(0xFF000000),
                        ),
                      ),
                      Container(
                        height: 6,width: 6,
                        decoration:BoxDecoration(
                          color: widget.day=="អា" ||widget.day=="SU"? Colorconstands.alertsDecline:Colors.transparent,
                          shape: BoxShape.circle
                        ),
                      )
                      
                    ],
                  ),
                ),
              ),
    );
  }
}
