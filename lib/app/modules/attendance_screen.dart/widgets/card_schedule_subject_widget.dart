import 'package:blurrycontainer/blurrycontainer.dart';
import 'package:camis_application_flutter/app/modules/auth_screens/e_school_code.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../../widgets/lock_widget/lock_widget.dart';
import '../../../core/resources/asset_resource.dart';
import '../../check_attendance_screen/bloc/check_attendance_bloc.dart';
import '../../check_attendance_screen/view/attendance_entry_screen.dart';

class CardScheduleSubjectWidget extends StatelessWidget {
  final String? subjectColor;
  final int? scheduleType;
  final int? examApprov;
  final String? titleSubject;
  final String? timeSubject;
  final bool? subjectEvent;
  final String? durationSubject;
  final String? examDate;
  final String? homeworkExDate;
  final String? lessonAbout;
  final bool? isActive;
  final List? homeList;
  final List? lessList;
  final String? attStatus;
  final bool? paid;
  final VoidCallback? onpressed;
  final int? isTakeAttandance;
  final int? leadership;
  final String? classId;
  final String? scheduleId;
  final String?date;
  final String?studentId;
  const CardScheduleSubjectWidget({
    Key? key,
    this.onpressed,
    required this.classId,
    required this.studentId,
    required this.date,
    required this.scheduleId,
    this.examDate,
    required this.leadership,
    this.homeworkExDate,
    this.lessonAbout,
    this.subjectColor,
    this.titleSubject,
    this.scheduleType,
    this.isActive,
    this.timeSubject,
    this.durationSubject,
    this.lessList,
    this.homeList,
    this.attStatus,
    this.paid,
    this.subjectEvent,
    this.examApprov,
    this.isTakeAttandance,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            isActive == true
                ? Container(
                    margin: const EdgeInsets.only(right: 12.0),
                    width: 20.0,
                    height: 13.0,
                    decoration:const BoxDecoration(
                      color: Colorconstands.primaryColor,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(10.0),
                        bottomRight: Radius.circular(10.0),
                      ),
                    ),
                  )
                : const SizedBox(width: 30.0),
            Text(
              timeSubject!.toString(),
              style: ThemeConstands.button_SemiBold_16
                  .copyWith(color:  isActive == true ? Colorconstands.primaryColor : Colorconstands.lightBulma),
            ),
            const Spacer(),
            Text(
              // durationSubject!,
              convertMinutes(durationSubject!),
              style: ThemeConstands.headline6_SemiBold_14
                  .copyWith(color: Colorconstands.lightTrunks),
            ),
            const SizedBox(width: 20.0),
          ],
        ),
        Container(
          margin: isActive ==true
              ? const EdgeInsets.only(
                  left: 18.0, top: 10, right: 16.0, bottom: 16.0)
              : const EdgeInsets.only(
                  left: 32, top: 14, bottom: 12.0, right: 16),
          decoration: BoxDecoration(
              color: Color(int.parse(subjectColor!)),
              borderRadius: BorderRadius.circular(12),
              boxShadow: [
                isActive == true
                    ? BoxShadow(
                        offset: const Offset(0, 10),
                        blurRadius: 19,
                        spreadRadius: 1,
                        color: Color(int.parse(subjectColor!)).withOpacity(0.2))
                    : const BoxShadow()
              ]),
          child: Container(
              margin:
                  const EdgeInsets.only(left: 6, right: 1, top: 1, bottom: 1),
              decoration: BoxDecoration(
                color: Colorconstands.neutralWhite,
                borderRadius: BorderRadius.circular(12),
              ),
              child: MaterialButton(
                onPressed: onpressed,
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(12))),
                padding: const EdgeInsets.all(0),
                child: Container(
                    color:  Color(int.parse(subjectColor!)).withOpacity(0.09),
                    padding: const EdgeInsets.only(
                        top: 2.0,bottom: 2.0, left: 14.0,right: 1.0),
                    child: Column(
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              margin:const EdgeInsets.only(top: 12,bottom: 14),
                              padding:const EdgeInsets.all(1),
                              decoration:const BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colorconstands.neutralWhite
                                ),
                              child: Container(
                                alignment: Alignment.center,
                                width: 50,
                                height: 50,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Color(int.parse(subjectColor!))
                                ),
                                child: SvgPicture.asset(ImageAssets.book,width: 30,height: 30,),
                              ),
                            ),
                            const SizedBox(
                              width: 9.0,
                            ),
                            Expanded(
                              child: Text(
                                titleSubject!,
                                style: ThemeConstands.headline4_SemiBold_18.copyWith(
                                        height: 1,
                                        color: Color(int.parse(subjectColor!)
                                    ),
                                ),
                              ),
                            ),
                          leadership == 1 && isTakeAttandance==0? Container():isTakeAttandance == 1 || isActive ==true || attStatus == "UNQUOTED"?  paid==true? GestureDetector(
                            onTap: leadership == 1?(){
                              BlocProvider.of<CheckAttendanceBloc>(context).add(
                                  GetCheckStudentAttendanceEvent(classId.toString(),scheduleId.toString(),date.toString()),
                                );
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => AttendanceEntryScreen(studentId:studentId.toString()),
                                  ),
                                );
                            }:(){},
                             child: Container(
                              margin: const EdgeInsets.only(right: 10),
                              padding:const EdgeInsets.symmetric(horizontal: 15,vertical: 8),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                color:attStatus =="ABSENT"?Colorconstands.alertsDecline:attStatus =="PRESENT"? Colorconstands.alertsPositive:attStatus =="LATE"?Colorconstands.alertsAwaitingText:Colorconstands.neutralGrey,
                              ),
                              child: Row(
                                children: [
                                  attStatus =="ABSENT"?const Icon(Icons.close_rounded,size: 18,color: Colorconstands.neutralWhite,):SvgPicture.asset(attStatus =="PRESENT"? ImageAssets.checkIcon:attStatus =="LATE"? ImageAssets.clockIcon:ImageAssets.qnquotedIcon, color:attStatus=="UNQUOTED"||attStatus=="PERMISSION"?Colorconstands.neutralDarkGrey:Colorconstands.neutralWhite,),
                                  const SizedBox(width: 6,),
                                  Text(attStatus =="ABSENT"?"ABSENT".tr():attStatus=="PRESENT"?"PRESENT".tr():attStatus=="LATE"?"LATE".tr():attStatus=="PERMISSION"?"PERMISSION".tr():"UNQUOTED".tr(),style: ThemeConstands.headline6_SemiBold_14.copyWith(color:attStatus=="UNQUOTED"||attStatus=="PERMISSION"?Colorconstands.neutralDarkGrey:Colorconstands.neutralWhite),)
                                ],
                              ),
                                                     ),
                           ):const LockWidget():Container(),
                          leadership == 1 && isTakeAttandance==0?  
                            Container(
                              child: MaterialButton(
                                elevation: 2,
                                color: Colorconstands.neutralGrey,
                                shape:const RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(8)),
                                ),
                                padding:const EdgeInsets.all(0),
                                onPressed: (){
                                    BlocProvider.of<CheckAttendanceBloc>(context).add(
                                      GetCheckStudentAttendanceEvent(classId.toString(),scheduleId.toString(),date.toString()),
                                    );
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => AttendanceEntryScreen(studentId:studentId.toString()),
                                      ),
                                    );
                                },
                                child: Container(
                                  padding:const EdgeInsets.symmetric(horizontal: 15,vertical: 6),
                                  child: Row(
                                    children: [
                                      SvgPicture.asset(ImageAssets.qnquotedIcon, color:Colorconstands.neutralDarkGrey),
                                      const SizedBox(width: 5,),
                                      Text("NOT_QUOTED".tr(),style: ThemeConstands.headline6_SemiBold_14.copyWith(color:Colorconstands.neutralDarkGrey),),
                                    ],
                                  ),
                                ),
                              )
                             ): isTakeAttandance == 0 && isActive == false && attStatus != "UNQUOTED" ? SizedBox(
                              width: 150,
                              height: 80,
                              child: Stack(
                                children: [
                                Positioned(
                                  top: 5,
                                  right: 5,
                                 // bottom: 2,
                                  left: 0,
                                  child: BlurryContainer(
                                      padding: const EdgeInsets.all(0),
                                      borderRadius : const BorderRadius.only(topRight:Radius.circular(8)),
                                      blur: 4,
                                      child: Container(   
                                       padding:const EdgeInsets.symmetric(vertical: 2),
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration( color:const Color(0xFF6163F5).withOpacity(0.2),                                
                                          borderRadius: BorderRadius.circular(11)
                                        ),
                                        child: Text("CHECKINGATTEN".tr(),style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.primaryColor),           
                                        textAlign: TextAlign.center,),       
                                      ),
                                    ),
                                  )     
                                 ],
                              ),
                            )
                            :Container()
                          ],
                        ),
                      
                        subjectEvent==true?Container(): Padding(
                          padding: const EdgeInsets.only(bottom: 16.0,top: 0),
                          child: Divider(
                            height: 1.2,
                            thickness: 1,
                            color:  Color(int.parse(subjectColor!)).withOpacity(0.2),
                          ),
                        ),
                        subjectEvent==true?Container(): scheduleType == 2 ? studySubject() : examSubject(),
                      ],
                    )),
              )),
        ),
      ],
    );
  }

  Widget studySubject() {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                   mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconTitleWidget(
                        icon: ImageAssets.book_notebook,
                        title: "LESSION_SUMMARY".tr(),
                        subjectColor: Color(int.parse(subjectColor!))),
                    const SizedBox(height: 8.0),
                    Container(
                      margin:const EdgeInsets.only(left: 22),
                      child: Text(
                        lessList!.isEmpty?"-----":lessonAbout!,
                        textAlign: TextAlign.center,
                        style: ThemeConstands.headline6_SemiBold_14
                            .copyWith(color: Colorconstands.lightBulma),
                      ),
                    ),
                  ],
                )),
            Expanded(
                child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconTitleWidget(
                      icon: ImageAssets.HOMEWORK_ICON,
                      title: homeList!.isEmpty?"HOMEWORK".tr():"HOMEWORK_DATELINE".tr(),
                      subjectColor: Color(int.parse(subjectColor!))),
                  const SizedBox(height: 8.0),
                  Container(
                    margin:const EdgeInsets.only(left: 22),
                    child: Text(
                      homeList!.isEmpty?"-----":homeworkExDate!,
                      textAlign: TextAlign.center,
                      style: ThemeConstands.headline6_SemiBold_14
                          .copyWith(color: Colorconstands.lightBulma),
                    ),
                  ),
                ],
              )),
          ],
        ),
        isActive ==true
            ? const SizedBox(
                height: 16,
              )
            : const SizedBox(),
      ],
    );
  }

  Widget examSubject() {
    return Row(children: [
      Expanded(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
             IconTitleWidget(
                icon: ImageAssets.current_date,
                title: "EXAM_DATE".tr(),
                subjectColor: Colorconstands.exam),
            const SizedBox(
              height: 8.0,
            ),
            Container(
              margin:const EdgeInsets.only(left: 18),
              child: Text(
                examDate!,
                style: ThemeConstands.headline6_SemiBold_14
                    .copyWith(color: Colorconstands.lightBulma),
              ),
            ),
          ],
        ),
      ),
      Expanded(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
             IconTitleWidget(
                icon: ImageAssets.EXAM_ICON,
                title: "EXAM_RESULT".tr(),
                subjectColor: Colorconstands.exam),
            const SizedBox(
              height: 8.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Expanded(
                  child: Text(
                    examApprov==0?"AWWAIT_APPROV".tr():"IS_APPROV".tr(),
                    textAlign: TextAlign.right,
                    overflow: TextOverflow.ellipsis,
                    style: ThemeConstands.headline6_SemiBold_14
                        .copyWith(color:examApprov==0?Colorconstands.alertsAwaitingText:Colorconstands.alertsPositive,),
                  ),
                ),
                const SizedBox(width: 8),
                Icon(
                  Icons.access_time_filled_rounded,
                  size: 18.0,
                  color:examApprov==0?Colorconstands.alertsAwaitingText:Colorconstands.alertsPositive,
                )
              ],
            ),
          ],
        ),
      )
    ]);
  }

  String convertMinutes(String minutesString,) {
  int minutes = int.parse(minutesString.replaceAll('min','').trim());
  int hours = minutes ~/ 60;
  int remainingMinutes = minutes % 60;

  if(minutes < 60){
    return "$minutes${"MINUTE".tr()}";
  }
  else if(minutes == 60) {
    return '${hours.toString()} ${"HOUR".tr()}';
  }else{
    return '${hours.toString()} ${"HOUR".tr()} :${remainingMinutes.toString().padLeft(2, '0')} ${"MINUTE".tr()}';
  }

  // return '${hours.toString().padLeft(2, '0')}:${remainingMinutes.toString().padLeft(2, '0')}';
 }
}

class IconTitleWidget extends StatelessWidget {
  final String icon;
  final String title;
  const IconTitleWidget({ Key? key,
    required this.icon,
    required this.title,
    required this.subjectColor,
  });

  final Color? subjectColor;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          margin:const EdgeInsets.only(bottom: 5),
          child: SvgPicture.asset(
            icon,
            height: 18,
            color: subjectColor ?? Colorconstands.primaryColor,
          ),
        ),
        const SizedBox(
          width: 5.0,
        ),
        Text(
          title,
          overflow: TextOverflow.ellipsis,
          style: ThemeConstands.overline_Semibold_12
              .copyWith(color: subjectColor ?? Colorconstands.primaryColor),
        ),
      ],
    );
  }

}
