import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:flutter/material.dart';

class WidgetLessonDetailCard extends StatelessWidget {
  final Widget child;
  const WidgetLessonDetailCard({Key? key,required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(12)),
        color: Colorconstands.neutralWhite,
        boxShadow: [
          BoxShadow(
            color: Color.fromARGB(255, 238, 233, 233),
            blurRadius: 15.0, // soften the shadow
            spreadRadius: 5.0, //extend the shadow
            offset: Offset(
              5.0, // Move to right 5  horizontally
              5.0, // Move to bottom 5 Vertically
            ),
          )
        ],
      ),
      child: child,
    );
  }
}