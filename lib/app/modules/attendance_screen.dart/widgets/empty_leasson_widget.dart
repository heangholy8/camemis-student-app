import 'package:camis_application_flutter/app/core/resources/asset_resource.dart';
import 'package:camis_application_flutter/app/modules/home_screen/e_home.dart';
import 'package:flutter/material.dart';

class EmptyLessonWidget extends StatelessWidget {
  final String title;
  final String icon;
  final String subtitle;
  const EmptyLessonWidget({Key? key, required this.title, required this.icon, required this.subtitle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset(icon,width: 20,height: 20,color: Colorconstands.lightBlack,),
                const SizedBox(width: 8,),
                Container(
                    alignment: Alignment.centerLeft,
                    child: Text(title,style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),),
                  ),
              ],
            ),
          ),
          const SizedBox(height: 18,),
          Container(
            alignment: Alignment.center,
            child: Text(subtitle,style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.darkTextsPlaceholder),textAlign: TextAlign.center,),
          ),
          ],
      ),
    );
  }
}