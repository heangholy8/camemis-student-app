import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../../core/themes/themes.dart';

class DataEmptyWidget extends StatelessWidget {
  final String title;
  final String description;
  const DataEmptyWidget({Key? key, required this.title, required this.description}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        child: Column(
          children: [
            Container(
              child: Image.asset("assets/images/no_schedule_image.png",width: 200,height: 200,),
            ),
            Container(
              child: Text(title,style: ThemeConstands.headline3_Medium_20_26height.copyWith(color:const Color(0xff1e2123),),),
            ),
            Container(
              margin:const EdgeInsets.only(top: 8,left: 32,right: 32),
              child: Text(description,style: ThemeConstands.headline6_Regular_14_20height.copyWith(color:const Color(0xff828c95),),textAlign: TextAlign.center,),
            )
          ],
        ),
      ),
    );
  }
}