part of 'get_date_bloc.dart';

abstract class GetDateState extends Equatable {
  const GetDateState();
  
  @override
  List<Object> get props => [];
}

class GetDateInitial extends GetDateState {}
class GetDateListLoading extends GetDateState{}
class GetDateListLoaded extends GetDateState{
  final ListDateInMonthModel? listDateModel;
  const GetDateListLoaded({required this.listDateModel,});
}
class GetDateListError extends GetDateState{}
