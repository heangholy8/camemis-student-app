part of 'get_schedule_bloc.dart';

abstract class GetScheduleEvent extends Equatable {
  const GetScheduleEvent();

  @override
  List<Object> get props => [];
}

class GetSchedule extends GetScheduleEvent {
  final String? date;
  final String? classId;
  final String? childId;
 const GetSchedule({this.date,this.classId,this.childId});
}


