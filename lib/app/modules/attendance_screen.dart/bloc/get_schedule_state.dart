part of 'get_schedule_bloc.dart';

abstract class GetScheduleState extends Equatable {
  const GetScheduleState();
  
  @override
  List<Object> get props => [];
}

class GetScheduleInitial extends GetScheduleState {}

class GetScheduleLoading extends GetScheduleState{
  final String? loading;
  const GetScheduleLoading({required this.loading});
}
class GetMyScheduleLoaded extends GetScheduleState{
  final ScheduleModel? myScheduleModel;
  const GetMyScheduleLoaded({required this.myScheduleModel,});
}
class GetScheduleError extends GetScheduleState{}