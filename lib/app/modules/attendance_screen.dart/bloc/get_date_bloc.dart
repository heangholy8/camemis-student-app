import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../model/attendance_schedule/get_List_date_by_month.dart';
import '../../../../service/apis/attendance_schedule/get_date_in_month_api.dart';
part 'get_date_event.dart';
part 'get_date_state.dart';

class GetDateBloc extends Bloc<GetDateEvent, GetDateState> {
  final GetListDateApi listDate;
  GetDateBloc({required this.listDate}) : super(GetDateInitial()) {
    on<GetListDateEvent>((event, emit)async{
      emit(GetDateListLoading());
      try {
        var data = await listDate.getListDateRequestApi(
          month: event.month.toString(),
          year: event.year.toString(),
        );
        emit(GetDateListLoaded(listDateModel: data));
      } catch (e) {
        print(e);
        emit(GetDateListError());
      }
    });
  }
}
