import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../model/attendance_schedule/get_attendance_schedule_by_day_model.dart';
import '../../../../service/apis/attendance_schedule/get_schedule_api.dart';
part 'get_schedule_event.dart';
part 'get_schedule_state.dart';

class GetScheduleBloc extends Bloc<GetScheduleEvent, GetScheduleState> {
  final GetScheduleApi mySchedule;
  GetScheduleBloc({required this.mySchedule}) : super(GetScheduleInitial()) {
    on<GetSchedule>((event, emit) async {
      emit(const GetScheduleLoading(loading: "Loading............."));
      try {
        var dataMySchedule = await mySchedule.getScheduleRequestApi(
          date: event.date.toString(),cladsId: event.classId.toString(),childId: event.childId.toString(),
        );
        emit(GetMyScheduleLoaded(myScheduleModel: dataMySchedule));
      } catch (e) {
        print(e);
        emit(GetScheduleError());
      }
    });
  }
}
