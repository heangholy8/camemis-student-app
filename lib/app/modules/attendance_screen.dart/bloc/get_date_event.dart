part of 'get_date_bloc.dart';

abstract class GetDateEvent extends Equatable {
  const GetDateEvent();

  @override
  List<Object> get props => [];
}
class GetListDateEvent extends GetDateEvent {
  final String? month;
  final String? year;
 const GetListDateEvent({this.month, this.year,});
}