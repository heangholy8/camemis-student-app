
import 'package:blurrycontainer/blurrycontainer.dart';
import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:page_transition/page_transition.dart';
import '../../../../model/attendance_schedule/get_attendance_schedule_by_day_model.dart';
import '../../../../widgets/lock_widget/lock_widget.dart';
import '../../../../widgets/view_document.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/themes/color_app.dart';
import '../../time_line/widget/thumbnail_doc_widget.dart';
import '../../time_line/widget/view_image.dart';
import '../widgets/container_lesson_detail.dart';
import '../widgets/empty_leasson_widget.dart';

class LessonDetailScreen extends StatefulWidget {
  final DataSchedule dataSchedule;
  final String nameChild;
  final String profileChild;
  final bool paid;
  const LessonDetailScreen({Key? key,required this.dataSchedule, required this.nameChild, required this.profileChild, required this.paid,}) : super(key: key);

  @override
  State<LessonDetailScreen> createState() => _LessonDetailScreenState();
}

class _LessonDetailScreenState extends State<LessonDetailScreen> {
  List listImage = [];
  List listappication = [];
  String? month;

  @override
  void initState() {
    setState((){
      if(widget.dataSchedule.isExam==true){
        checkMonth();
      }
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    var homework = widget.dataSchedule.activities!.where((element) {
      return element.activityType == 2;
    }).toList();
    var lesson = widget.dataSchedule.activities!.where((element) {
      return element.activityType == 1;
    }).toList();
    if(homework.isNotEmpty){
      listappication = homework[0].file!.where((element) => element.fileType!.contains("application")).toList();
      listImage = homework[0].file!.where((element) => element.fileType!.contains("image")).toList();
    }
    return Scaffold(
      backgroundColor: Colorconstands.neutralSecondBackground,
      body: Container(
        margin: const EdgeInsets.only(top: 45,left: 0,right: 0),
        child: Column(
          children: [
            Container(
               margin: const EdgeInsets.only(left: 6,),
              alignment: Alignment.centerLeft,
              child: IconButton(
                onPressed: () {
                  Navigator.of(context).pop();
                }, 
                icon:const Icon(Icons.close_rounded,color: Colorconstands.black,size: 28,)
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                   margin: const EdgeInsets.only(left: 18,right: 18,top: 10,bottom: 25),
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(translate=="km"?widget.dataSchedule.subjectName.toString():widget.dataSchedule.subjectNameEn.toString(),style: ThemeConstands.headline2_SemiBold_24.copyWith(color: Color(int.parse("0xFF${widget.dataSchedule.subjectColor}"))),),
                      ),
                      Container(
                         margin: const EdgeInsets.only(top: 22),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: Row(
                                children: [
                                  Container(
                                    child:Icon(
                                      Icons.location_on_outlined,size: 22,color:Color(int.parse("0xFF${widget.dataSchedule.subjectColor}")),
                                    ),
                                  ),
                                  Container(
                                    child: Text(widget.dataSchedule.className.toString(),style: ThemeConstands.button_SemiBold_16.copyWith(color: Colorconstands.lightBlack),),
                                  )
                                ],
                              ),
                            ),
                            Expanded(
                              child: Container(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    SvgPicture.asset(ImageAssets.current_date,color:Color(int.parse("0xFF${widget.dataSchedule.subjectColor}")),),
                                    const SizedBox(width: 5,),
                                    Container(
                                    child: Text(widget.dataSchedule.date.toString(),style: ThemeConstands.button_SemiBold_16.copyWith(color: Colorconstands.lightBlack),),
                                    ),
                                    Container(
                                      child: Text(" (${widget.dataSchedule.startTime}",style: ThemeConstands.button_SemiBold_16.copyWith(color: Colorconstands.lightBlack),),
                                    ),
                                    Container(
                                      child: Text("-${widget.dataSchedule.endTime})",style: ThemeConstands.button_SemiBold_16.copyWith(color: Colorconstands.lightBlack),),
                                    )
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      widget.dataSchedule.isExam==false?Container():Container(
                        margin: const EdgeInsets.only(top: 28),
                        child: WidgetLessonDetailCard(
                          child: Container(
                            padding: const EdgeInsets.all(16),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      alignment: Alignment.centerLeft,
                                      child: Text("TYPE_EXAM".tr(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.neutralDarkGrey),),
                                    ),
                                    widget.dataSchedule.examItem!.type==1?Container(
                                      alignment: Alignment.centerLeft,
                                      child: Text("${"REGULAR".tr()}${translate=="en"?"":"MONTH".tr()}$month",style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.primaryColor),),
                                    ):Container(
                                      alignment: Alignment.centerLeft,
                                      child: Text("${"REGULAR".tr()}${widget.dataSchedule.examItem!.semester=="FIRST_SEMESTER"?"FIRST_SEMESTER".tr():"SECOND_SEMESTER".tr()}",style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.primaryColor),),
                                    ),
                                 ],
                                ),
                                Container(
                                  margin: const EdgeInsets.only(top: 8,bottom: 8),
                                  child:const Divider(),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      alignment: Alignment.centerLeft,
                                      child: Text("ABOUT".tr(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.neutralDarkGrey),),
                                    ),
                                    Container(
                                      alignment: Alignment.centerLeft,
                                      child: Text(widget.dataSchedule.examItem!.description.toString(),style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.primaryColor),),
                                    )
                                 ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 22),
                        child: WidgetLessonDetailCard(
                          child: Container(
                            padding: const EdgeInsets.all(16),
                            child: Column(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  child: Text("STUDENT_ATTENDANCE".tr(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.neutralDarkGrey),),
                                ),
                                Container(
                                   margin: const EdgeInsets.only(top: 22),
                                  alignment: Alignment.centerLeft,
                                  child: Container(
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          child:CircleAvatar(radius:20, // Image radius
                                          backgroundImage: NetworkImage(widget.profileChild),
                                        )),
                                        const SizedBox(
                                          width: 9.0,
                                        ),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                children: [
                                                  Text(widget.dataSchedule.startTime.toString(),
                                                    style: ThemeConstands.button_SemiBold_16
                                                        .copyWith(
                                                            height: 1,
                                                            color: Colorconstands.lightBlack),
                                                  ),
                                                  Text("-${widget.dataSchedule.startTime.toString()}",
                                                    style: ThemeConstands.button_SemiBold_16
                                                        .copyWith(
                                                            height: 1,
                                                            color: Colorconstands.lightBlack),
                                                  ),
                                                ],
                                              ),
                                              const SizedBox(height: 5,),
                                              Text(
                                                widget.nameChild,
                                                style: ThemeConstands.headline6_Regular_14_20height
                                                    .copyWith(
                                                        height: 1,
                                                        color: Colorconstands.darkTextsPlaceholder),
                                              ),
                                            ],
                                          ),
                                        ),
                                        widget.dataSchedule.isTakeAttendance == 1 || widget.dataSchedule.isActive ==true || widget.dataSchedule.attendanceStatus == "UNQUOTED"?  widget.paid==true? Container(
                                          padding:const EdgeInsets.symmetric(horizontal: 15,vertical: 6),
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(8),
                                            color:widget.dataSchedule.attendanceStatus =="ABSENT"?Colorconstands.alertsDecline:widget.dataSchedule.attendanceStatus =="PRESENT"? Colorconstands.alertsPositive:widget.dataSchedule.attendanceStatus =="LATE"?Colorconstands.alertsAwaitingText:Colorconstands.neutralGrey,
                                          ),
                                          child: Row(
                                            children: [
                                              widget.dataSchedule.attendanceStatus =="ABSENT"?const Icon(Icons.close_rounded,size: 18,color: Colorconstands.neutralWhite,):SvgPicture.asset(widget.dataSchedule.attendanceStatus =="PRESENT"? ImageAssets.checkIcon:widget.dataSchedule.attendanceStatus =="LATE"? ImageAssets.clockIcon:ImageAssets.qnquotedIcon, color:widget.dataSchedule.attendanceStatus=="UNQUOTED"||widget.dataSchedule.attendanceStatus=="PERMISSION"?Colorconstands.neutralDarkGrey:Colorconstands.neutralWhite,),
                                              const SizedBox(width: 6,),
                                              Text(widget.dataSchedule.attendanceStatus =="ABSENT"?"ABSENT".tr():widget.dataSchedule.attendanceStatus=="PRESENT"?"PRESENT".tr():widget.dataSchedule.attendanceStatus=="LATE"?"LATE".tr():widget.dataSchedule.attendanceStatus=="PERMISSION"?"PERMISSION".tr():"UNQUOTED".tr(),style: ThemeConstands.headline6_SemiBold_14.copyWith(color:widget.dataSchedule.attendanceStatus=="UNQUOTED"||widget.dataSchedule.attendanceStatus=="PERMISSION"?Colorconstands.neutralDarkGrey:Colorconstands.neutralWhite),)
                                            ],
                                          ),
                                        ):const LockWidget():Container(),
                                        widget.dataSchedule.isTakeAttendance == 0 && widget.dataSchedule.isActive == false && widget.dataSchedule.attendanceStatus != "UNQUOTED" ? SizedBox(
                                          width: 150,
                                          height: 60,
                                          child: Stack(
                                            children: [
                                            Positioned(
                                              top: 0,
                                              right: 1,
                                              bottom: 2,
                                              left: 0,
                                              child: BlurryContainer(
                                                  padding: const EdgeInsets.all(0),
                                                  borderRadius : const BorderRadius.only(topRight:Radius.circular(8)),
                                                  blur: 4,
                                                  child: Container(   
                                                    height: 100,
                                                    alignment: Alignment.center,
                                                    decoration: BoxDecoration( color:const Color(0xFF6163F5).withOpacity(0.2),                                     
                                                      borderRadius: BorderRadius.circular(11)
                                                    ),
                                                    child: Text("CHECKINGATTEN".tr(),style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.primaryColor),           
                                                    textAlign: TextAlign.center,),       
                                                  ),
                                                ),
                                              )     
                                            ],
                                          ),
                                        )
                                        :Container()
                                      ],
                                    ),
                                  )
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      widget.dataSchedule.isExam==true?Container():Container(
                        margin: const EdgeInsets.only(top: 22),
                        child: WidgetLessonDetailCard(
                          child: Container(
                            padding: const EdgeInsets.all(16),
                            child:lesson.isEmpty?EmptyLessonWidget(
                              icon: ImageAssets.book_notebook,
                              title: "NO_EXCERPT".tr(),
                              subtitle: "TEACHER_HAVE_NOT_LESSON_TODAY".tr(),
                            ) :Column(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  child: Text("EXCERPT".tr(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.neutralDarkGrey),),
                                ),
                                const SizedBox(height: 12,),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  child: Text(lesson[0].title.toString(),style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.neutralDarkGrey),),
                                ),
                                const SizedBox(height: 8,),
                                Container(
                                  child: Divider(),
                                ),
                                const SizedBox(height: 8,),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  child: Text(lesson[0].description.toString(),style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.neutralDarkGrey),),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      widget.dataSchedule.isExam==true?Container():Container(
                        margin: const EdgeInsets.only(top: 22),
                        child: WidgetLessonDetailCard(
                          child: Container(
                            padding: const EdgeInsets.all(16),
                            child:homework.isEmpty?EmptyLessonWidget(
                              icon: ImageAssets.HOMEWORK_ICON,
                              title: "NO_HOMEWORK".tr(),
                              subtitle: "NO_HOMEWORK_TODAY".tr(),
                            ) :Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      alignment: Alignment.centerLeft,
                                      child: Text("HOMEWORK_DATELINE".tr(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.neutralDarkGrey),),
                                    ),
                                    Container(
                                        padding:const EdgeInsets.symmetric(horizontal: 15,vertical: 6),
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(8),
                                          color: Colorconstands.primaryColor.withOpacity(0.16),
                                        ),
                                      child: Row(
                                        children: [
                                          SvgPicture.asset(ImageAssets.current_date,color: Colorconstands.primaryColor,),
                                          const SizedBox(width: 8,),
                                          Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text(homework[0].expiredAt.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.primaryColor),),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                                const SizedBox(height: 12,),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  child: Text(homework[0].title.toString(),style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.neutralDarkGrey),),
                                ),
                                const SizedBox(height: 8,),
                                Container(
                                  child: Divider(),
                                ),
                                const SizedBox(height: 8,),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  child: Text(homework[0].description.toString(),style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.neutralDarkGrey),),
                                ),
                                const SizedBox(height: 8,),
                                Container(
                                  child: Divider(),
                                ),
                                listImage.isEmpty?Container():Column(
                                  children: [
                                    Container(
                                      alignment: Alignment.centerLeft,
                                      child: Text("PHOTO".tr(),style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.neutralDarkGrey),),
                                    ),
                                    Container(
                                      margin:const EdgeInsets.only(top: 16,bottom: 16),
                                      height: 150,
                                      child: Row(
                                        children: [
                                          Expanded(
                                            child: GestureDetector(
                                              onTap: (() {
                                                Navigator.push(context,
                                                  PageTransition(type:PageTransitionType.fade,child:ImageViewDownloads(
                                                      listimagevide: listImage,
                                                      activepage: 0,
                                                    ),
                                                ),);
                                              }),
                                              child: Container(
                                                padding:const EdgeInsets.all(0.7),
                                                decoration:const BoxDecoration(
                                                  color: Colorconstands.gray300,
                                                  borderRadius: BorderRadius.only(
                                                    topLeft: Radius.circular(10.0),
                                                    bottomLeft: Radius.circular(10.0),
                                                  ),
                                                ),
                                                child: Container(
                                                  decoration:const BoxDecoration(
                                                  color: Colorconstands.neutralWhite,
                                                  borderRadius: BorderRadius.only(
                                                    topLeft: Radius.circular(10.0),
                                                    bottomLeft: Radius.circular(10.0),
                                                  ),
                                                ),
                                                  child: Image.network(listImage[0].fileShow.toString(),fit: BoxFit.cover,))
                                              ),
                                            ),
                                          ),
                                          const SizedBox(width: 5,),
                                          Expanded(
                                            child: listImage.length<=1?Container():GestureDetector(
                                              onTap: (() {
                                                Navigator.push(context,
                                                  PageTransition(type:PageTransitionType.fade,child:ImageViewDownloads(
                                                      listimagevide: listImage,
                                                      activepage: 1,
                                                    ),
                                                ),);
                                              }),
                                              child: Stack(
                                                children: [
                                                  Positioned(
                                                    left: 0,right: 0,
                                                    bottom: 0,top: 0,
                                                    child: Container(
                                                      padding:const EdgeInsets.all(0.7),
                                                      decoration:const BoxDecoration(
                                                        color: Colorconstands.gray300,
                                                        borderRadius: BorderRadius.only(
                                                          topRight: Radius.circular(10.0),
                                                          bottomRight: Radius.circular(10.0),
                                                        ),
                                                      ),
                                                      child: Container(
                                                        decoration:const BoxDecoration(
                                                        color: Colorconstands.neutralWhite,
                                                        borderRadius: BorderRadius.only(
                                                          topRight: Radius.circular(10.0),
                                                          bottomRight: Radius.circular(10.0),
                                                        ),
                                                      ),
                                                        child: Image.network(listImage[1].fileShow.toString(),fit: BoxFit.cover,))
                                                    ),
                                                  ),
                                                  Container(
                                                    decoration: BoxDecoration(
                                                      color:listImage.length<=2?Colors.transparent:Colorconstands.neutralDarkGrey.withOpacity(0.4),
                                                      borderRadius: const BorderRadius.only(
                                                        topRight: Radius.circular(10.0),
                                                        bottomRight: Radius.circular(10.0),
                                                      ),
                                                    ),
                                                  ),
                                                  listImage.length>2?Container(
                                                    child: Center(
                                                      child: Text("${listImage.length - 2}+",style: ThemeConstands.headline2_SemiBold_242.copyWith(color: Colorconstands.neutralWhite),),
                                                    ),
                                                  ):Container(height: 0,width: 0,)
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                                listappication.isEmpty?Container():Column(
                                  children: [
                                    Container(
                                      alignment: Alignment.centerLeft,
                                      child: Text("APPLICATION".tr(),style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.neutralDarkGrey),),
                                    ),
                                    ListView.builder(
                                      physics: const ScrollPhysics(),
                                      padding: const EdgeInsets.all(0),
                                      shrinkWrap: true,
                                      itemCount: listappication.length,
                                      itemBuilder: (BuildContext contex, index) {
                                        var path = listappication[index].fileShow.toString();
                                        var filename =listappication[index].fileName.toString();
                                        return ThumbnailDocWidget(
                                          nameFile:listappication[index].fileName ?? "",
                                          sizeFile:listappication[index].fileSize ?? "",
                                          onPressed: () {
                                            setState(() {
                                              Navigator.of(context).push(MaterialPageRoute( builder: (
                                                context,) => ViewDocuments(path: path,filename:filename.toString(),
                                                          )));
                                            });
                                          },
                                        );
                                      },
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
  checkMonth() {
    if (widget.dataSchedule.examItem!.month=="01") {
      return  month="JANUARY".tr();
    } else if (widget.dataSchedule.examItem!.month=="02") {
      return month="FEBRUARY".tr();
    } else if (widget.dataSchedule.examItem!.month=="03") {
      return month="MARCH".tr();
    } else if (widget.dataSchedule.examItem!.month=="04") {
      return month="APRIL".tr();
    } else if (widget.dataSchedule.examItem!.month=="05") {
      return month="MAY".tr();
    } else if (widget.dataSchedule.examItem!.month=="06") {
      return month="JUNE".tr();
    } else if (widget.dataSchedule.examItem!.month=="07") {
      return month="JULY".tr();
    } else if (widget.dataSchedule.examItem!.month=="08") {
      return month="AUGUST".tr();
    } else if (widget.dataSchedule.examItem!.month=="09") {
      return month="SEPTEMBER".tr();
    } else if (widget.dataSchedule.examItem!.month=="10") {
      return month="OCTOBER".tr();
    } else if (widget.dataSchedule.examItem!.month=="11") {
      return month="NOVEMBER".tr();
    } else {
      return "DECEMBER".tr();
    }
  }
}