import 'dart:async';

import 'package:camis_application_flutter/app/modules/attendance_screen.dart/bloc/get_date_bloc.dart';
import 'package:camis_application_flutter/app/modules/attendance_screen.dart/bloc/get_schedule_bloc.dart';
import 'package:camis_application_flutter/app/modules/attendance_screen.dart/view/lesson_detail.dart';
import 'package:camis_application_flutter/service/apis/profile_user/bloc/get_profile_user_bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:page_transition/page_transition.dart';
import '../../../../mixins/toast.dart';
import '../../../../widgets/error_widget/error_request.dart';
import '../../../../widgets/navigate_bottom_bar_widget/navigate_bottom_bar.dart';
import '../../../../widgets/no_connection_alert_widget.dart';
import '../../../../widgets/no_dart.dart';
import '../../../../widgets/shimmer_style.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';
import '../../PaymentAndBillScreen/bloc/bloc/check_payment_expire_bloc.dart';
import '../widgets/card_schedule_subject_widget.dart';
import '../widgets/date_picker_screen.dart';
import '../widgets/schedule_empty.dart';

class ScheduleScreen extends StatefulWidget {
  late int activeChild;
  ScheduleScreen({Key? key, this.activeChild = 0});
  @override
  State<ScheduleScreen> createState() => _ScheduleScreenState();
}

class _ScheduleScreenState extends State<ScheduleScreen>
    with TickerProviderStateMixin, Toast {
  bool paid = true;
  int myIndex = 0;
  PageController? pagec;
  int? monthlocal;
  int? daylocal;
  int? yearchecklocal;
  int? monthlocalfirst;
   int? daylocalfirst;
  int? yearchecklocalfirst;
  bool? selectedday;
  List? lessList;
  List? homeList;
  bool showListDay = false;
  StreamSubscription? internetconnection;
  bool isoffline = true;
  String childId = "0";
  String classId = "0";
  bool firstLoad = true;
  String? date;
  double offset = 0.0;
  ScrollController? controller;
  int daySelectNoCurrentMonth = 1;
  int activeIndexDay=0;
  bool firstLoadOnlyIndexDay = true;
  bool isLoading = true;

  //==================== drop month verible ===============
  int? monthDrop;
  int? yearDrop;

  @override
  void initState() {
    setState(() {
      //=============Check internet====================
      internetconnection = Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
        // whenevery connection status is changed.
        if(result == ConnectivityResult.none){
             //there is no any connection
             setState(() {
                 isoffline = false;
             }); 
        }else if(result == ConnectivityResult.mobile){
             //connection is mobile data network
             setState(() {
                isoffline = true;
             });
        }else if(result == ConnectivityResult.wifi){
            //connection is from wifi
            setState(() {
               isoffline = true;
            });
        }
    }); //
      //=============Eend Check internet====================
    });
    monthlocal = int.parse(DateFormat("MM").format(DateTime.now()));
      daylocal = int.parse(DateFormat("dd").format(DateTime.now()));
      yearchecklocal = int.parse(DateFormat("yyyy").format(DateTime.now()));
      date = "$daylocal/$monthlocal/$yearchecklocal";
      if(isoffline==true && firstLoad==true){
        BlocProvider.of<GetProfileUserBloc>(context).add(GetProfileUser());
        BlocProvider.of<CheckPaymentExpireBloc>(context).add(CheckPaymentEvent());
        BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent( month: monthlocal.toString(), year: yearchecklocal.toString()));
      }
     monthlocalfirst = monthlocal;
     daylocalfirst = daylocal;
     yearchecklocalfirst = yearchecklocal;
    super.initState();
  }

  @override
  void dispose() {
    internetconnection!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    controller = ScrollController(initialScrollOffset: offset);
    controller!.addListener(() {
      setState(() {
        firstLoad = false;
        offset = controller!.offset;
      });
    });
    return Scaffold(
            backgroundColor: Colorconstands.neutralWhite,
            bottomNavigationBar: isoffline == false
                ? Container(
                    height: 0,
                  )
                : BottomNavigateBar(
                    isActive: 2,
                    paid: paid,
                  ),
            body: Container(
              color: Colorconstands.primaryColor,
              child: Stack(
                children: [
                  Positioned(
                    top: 0,
                    right: 0,
                    child: Image.asset("assets/images/Oval.png"),
                  ),
                  Positioned(
                      left: 0,
                      top: 50,
                      child: SvgPicture.asset(
                        ImageAssets.path20,
                        color: Colorconstands.secondaryColor,
                      )),
                  Container(
                    child: Column(
                      children: [
                        Container(
                          child: Container(
                            margin: const EdgeInsets.only(
                                top: 50, left: 22, right: 22, bottom: 20),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      "SCHEDULE".tr(),
                                      style: ThemeConstands.headline2_SemiBold_24
                                          .copyWith(
                                        color: Colorconstands.neutralWhite,
                                      ),
                                      textAlign: TextAlign.center,
                                    )),
                                Container()
                                // GestureDetector(
                                //   onTap: () {},
                                //   child: Container(
                                //     child: SvgPicture.asset(
                                //       ImageAssets.questoin_icon,
                                //       color: Colorconstands.neutralWhite,
                                //     ),
                                //   ),
                                // ),
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: BlocListener<CheckPaymentExpireBloc, CheckPaymentExpireState>(
                            listener: (context, state) {
                              if(state is CheckPaymentExpireLoaded){
                                var dataStatus = state.checkpayment!.status;
                                setState(() {
                                  paid = dataStatus!;
                                });
                              }
                            },
                          child: Container(
                            decoration: const BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(12),
                                    topRight: Radius.circular(12))),
                            child: BlocBuilder<GetProfileUserBloc, GetProfileUserState>(
                              builder: (context, state) {
                                if (state is ProfileUserLoadingState) {
                                  return const ShimmerSchedule();
                                } else if (state is ProfileUserLoaded) {
                                  var datachild = state.profileusermodel.data;  
                                 if(datachild !=null){
                                  if(isoffline ==true && firstLoad==true){
                                    if(datachild.currentClass ==null){}
                                    else{
                                      childId = datachild.id.toString();
                                      classId = datachild.currentClass!.classId.toString();
                                          BlocProvider.of<GetScheduleBloc>(context).add(
                                        GetSchedule(date: date,classId: datachild.currentClass!.classId.toString(),
                                            childId: datachild.id.toString()));
                                    }
                                  }
                                 }
                                    return datachild == null
                                      ? Container(
                                          child: Container(
                                            decoration:const BoxDecoration(
                                              color: Colorconstands.neutralWhite,
                                              borderRadius:  BorderRadius.only(topLeft: Radius.circular(12),topRight: Radius.circular(12))
                                            ),
                                              child: DataNotFound(
                                                    subtitle: "CHILD_NOT_CLASS_DES".tr(),
                                                      title: "CHILD_NOT_CLASS".tr(),
                                              )))
                                      : Column(
                                          children: [
                                            Expanded(
                                              child:Container(
                                                decoration:const BoxDecoration(
                                                  color: Colorconstands.neutralWhite,
                                                  borderRadius:  BorderRadius.only(topLeft: Radius.circular(12),topRight: Radius.circular(12))
                                                ),
                                                child: BlocBuilder<GetDateBloc,GetDateState>(
                                                  builder: (context, state) {
                                                    if (state is GetDateListLoading) {
                                                      return const ShimmerSchedule();
                                                    } else if (state is GetDateListLoaded) {
                                                      var selectDay;
                                                      var datamonth = state.listDateModel!.data;
                                                      var listMonthData = datamonth!.monthData;
                                                      if(datamonth.month.toString() == monthlocalfirst.toString() && yearchecklocal==yearchecklocalfirst){
                                                        daySelectNoCurrentMonth = 100;
                                                      }
                                                  //===================== loop list ======================
                                                      var listDayWeek1 = listMonthData!.where((element) {
                                                        return element.index! < 7;
                                                      },).toList();
                                                      var listDayWeek2 = listMonthData.where((element) {
                                                        return element.index! >= 7 && element.index! <= 13;
                                                      },).toList();
                                                       var listDayWeek3 = listMonthData.where((element) {
                                                        return element.index! >= 14 && element.index! <=20;
                                                      },).toList();
                                                       var listDayWeek4 = listMonthData.where((element) {
                                                        return element.index! >= 21 && element.index! <=27;
                                                      },).toList();
                                                      var listDayWeek6 = listMonthData.where((element) {
                                                        return element.index! > listMonthData.length-8;
                                                      },).toList();
                                                      var listDayWeek5 = listMonthData.length<=35?listDayWeek6:listMonthData.where((element) {
                                                          return element.index! >= 28 && element.index! <= 34;
                                                        },).toList();
                                                  //===================== End loop list ======================
                                                      return Stack(
                                                        children: [
                                                          Positioned(
                                                            top: 0,left: 0,right: 0,bottom: 0,
                                                            child: Column(
                                                              children: [
                                                                Container(
                                                                  height: 0,
                                                                  child: Stack(
                                                                    children:List.generate(listMonthData.length,(subindex) {
                                                                      if(firstLoadOnlyIndexDay==true){
                                                                        if(listMonthData[subindex].isActive==true){
                                                                          activeIndexDay = listMonthData[subindex].index!;
                                                                          firstLoadOnlyIndexDay = false;
                                                                        }
                                                                      }
                                                                        return Container();
                                                                      }
                                                                    )
                                                                  ),
                                                                ),
                                                                Container(
                                                                      width: double.maxFinite,
                                                                      decoration: BoxDecoration(
                                                                          borderRadius:BorderRadius.circular(8.0),
                                                                          color: Colorconstands.neutralSecondBackground,
                                                                      ),
                                                                      child: Column(
                                                                        mainAxisSize: MainAxisSize.min,
                                                                        children: [
                                                                          GestureDetector(
                                                                            onTap:() {
                                                                              setState(() {
                                                                                  if(showListDay==true){
                                                                                      showListDay =false;
                                                                                  }
                                                                                  else{
                                                                                    showListDay = true;
                                                                                  }
                                                                              });
                                                                            },
                                                                            child:Container(
                                                                              width: MediaQuery.of(context).size.width,
                                                                              height: 45,
                                                                                child: Row(
                                                                                  children: [
                                                                                    showListDay==false ?Container():Container(
                                                                                      margin:const EdgeInsets.only(left: 8),
                                                                                      child: IconButton(
                                                                                        onPressed: (){
                                                                                          setState(() {
                                                                                            if(yearchecklocal == (yearchecklocalfirst! -1)){}
                                                                                            else{
                                                                                              offset = 0.0;
                                                                                              daylocal = 1;
                                                                                              monthlocal = 1;
                                                                                              daySelectNoCurrentMonth = 1;
                                                                                              yearchecklocal = yearchecklocal! - 1;
                                                                                              showListDay = false;
                                                                                              date = "1/1/$yearchecklocal";
                                                                                              BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent( month: monthlocal.toString(), year: yearchecklocal.toString()));
                                                                                              BlocProvider.of<GetScheduleBloc>(context).add(
                                                                                                GetSchedule(date: date,classId: datachild.currentClass!.classId.toString(),
                                                                                                    childId: datachild.id.toString()));
                                                                                            }
                                                                                          });
                                                                                        }, 
                                                                                        icon: Icon(Icons.arrow_back_ios_new_rounded,size: 18,color: yearchecklocal == yearchecklocalfirst! - 1?Colors.transparent:Colorconstands.primaryColor,))
                                                                                    ),
                                                                                    Expanded(
                                                                                      child: Row(
                                                                                      mainAxisAlignment:MainAxisAlignment.center,
                                                                                      children: [
                                                                                        offset > 130
                                                                                            ? Container(
                                                                                                height: 25,width: 25,
                                                                                                margin: const EdgeInsets.only(right: 5),
                                                                                                padding: const EdgeInsets.all(3),
                                                                                                decoration: BoxDecoration(color: Colorconstands.primaryColor, borderRadius: BorderRadius.circular(6)),
                                                                                                child: Center(
                                                                                                  child: Text(
                                                                                                    daylocal.toString(),
                                                                                                    style: ThemeConstands.caption_Regular_12.copyWith(color: Colorconstands.neutralWhite),
                                                                                                  ),
                                                                                                ),
                                                                                              )
                                                                                            : Container(),
                                                                                        Text(
                                                                                          "${(datamonth.month == "1" ? "JANUARY" : datamonth.month == "2" ? "FEBRUARY" : datamonth.month == "3" ? "MARCH" : datamonth.month == "4" ? "APRIL" : datamonth.month == "5" ? "MAY" : datamonth.month == "6" ? "JUNE" : datamonth.month == "7" ? "JULY" : datamonth.month == "8" ? "AUGUST" : datamonth.month == "9" ? "SEPTEMBER" : datamonth.month == "10" ? "OCTOBER" : datamonth.month == "11" ? "NOVEMBER" : "DECEMBER").tr()}  ",
                                                                                          style:
                                                                                              ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.primaryColor),
                                                                                        ),
                                                                                        Text(
                                                                                            datamonth.year.toString(),
                                                                                            style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.primaryColor)),
                                                                                        const SizedBox(
                                                                                            width: 8),
                                                                                      showListDay==true?Container(): Icon(
                                                                                           showListDay ==false?Icons.expand_more:Icons.expand_less_rounded,
                                                                                            size: 24,
                                                                                            color: Colorconstands.primaryColor),
                                                                                                                                                              ],
                                                                                                                                                            ),
                                                                                    ),
                                                                                    showListDay==false?Container():Container(
                                                                                      margin:const EdgeInsets.only(right: 8),
                                                                                      child: IconButton(
                                                                                        onPressed: (){
                                                                                          setState(() {
                                                                                            if(yearchecklocal == yearchecklocalfirst!){}
                                                                                            else{
                                                                                              offset = 0.0;
                                                                                              yearchecklocal = yearchecklocalfirst;
                                                                                              daySelectNoCurrentMonth = 100;
                                                                                              daylocal = daylocalfirst;
                                                                                              monthlocal = monthlocalfirst;
                                                                                              showListDay = false;
                                                                                              BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent( month: monthlocalfirst.toString(), year: yearchecklocalfirst.toString()));
                                                                                              BlocProvider.of<GetScheduleBloc>(context).add(GetSchedule(date:"$daylocalfirst/$monthlocalfirst/$yearchecklocalfirst",childId: childId.toString(),classId: classId.toString()));
                                                                                            }
                                                                                          });
                                                                                        }, 
                                                                                        icon:Icon(Icons.arrow_forward_ios_rounded,size: 18,color:yearchecklocal==yearchecklocalfirst?Colors.transparent: Colorconstands.primaryColor,))
                                                                                    ),
                                                                                  ],
                                                                                ),
                                                                                ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                Expanded(
                                                                  child: Stack(
                                                                    children: [
                                                                      Column(
                                                                        children: [
                                                                          Expanded(
                                                                            child:BlocConsumer<GetScheduleBloc, GetScheduleState>(
                                                                              listener: (context, state) {
                                                                                if(state is GetMyScheduleLoaded){
                                                                                  setState(() {
                                                                                    firstLoad = false;
                                                                                  });
                                                                                }
                                                                              },
                                                                              builder:(context,state) {
                                                                                if (state is GetScheduleLoading) {
                                                                                  return Container(
                                                                                    decoration: const BoxDecoration(
                                                                                      color: Color(0xFFF1F9FF),
                                                                                      borderRadius: BorderRadius.only(
                                                                                        topLeft: Radius.circular(12),
                                                                                        topRight: Radius.circular(12),
                                                                                      ),
                                                                                    ),
                                                                                    child: ListView.builder(
                                                                                      padding: const EdgeInsets.all(0),
                                                                                      shrinkWrap: true,
                                                                                      itemBuilder: (BuildContextcontex, index) {
                                                                                        return const ShimmerTimeTable();
                                                                                      },
                                                                                      itemCount: 5,
                                                                                    ),
                                                                                  );
                                                                                } else if (state is GetMyScheduleLoaded) {
                                                                                  var dataMySchedule =  state.myScheduleModel!.data;
                                                                                  var messageResporn = state.myScheduleModel!.message;
                                                                                  return dataMySchedule!.isEmpty
                                                                                      ? Column(
                                                                                        children: [
                                                                                          Expanded(child:Container()),
                                                                                          DataEmptyWidget(
                                                                                            title: "$messageResporn".tr(),
                                                                                            description: messageResporn=="HOLIDAY"?"HOLIDAY_DES".tr():messageResporn=="DATE_NOT_VALID_ACADEMIC"?"DATE_NOT_VALID_ACADEMIC_DES".tr():"SCHEDULE_NOT_SET_DES".tr(),
                                                                                          ),
                                                                                          Expanded(child:Container()),
                                                                                        ],
                                                                                      )
                                                                                      : Container(
                                                                                          alignment: Alignment.center,
                                                                                          child: ListView.builder(
                                                                                            shrinkWrap: true,
                                                                                            controller: controller,
                                                                                            itemCount: dataMySchedule.length,
                                                                                            padding: EdgeInsets.only(top: MediaQuery.of(context).size.width>800? 210:115),
                                                                                            itemBuilder: (context, indexmyschedule) {
                                                                                              if (dataMySchedule.isNotEmpty) {
                                                                                                lessList = dataMySchedule[indexmyschedule].activities!.where((element) {
                                                                                                  return element.activityType == 1;
                                                                                                }).toList();
                                                                
                                                                                                homeList = dataMySchedule[indexmyschedule].activities!.where((element) {
                                                                                                  return element.activityType == 2;
                                                                                                }).toList();
                                                                                              }
                                                                                              return CardScheduleSubjectWidget(
                                                                                                studentId: datachild.id,
                                                                                                date: dataMySchedule[indexmyschedule].date.toString(),
                                                                                                classId: dataMySchedule[indexmyschedule].classId.toString(),
                                                                                                scheduleId: dataMySchedule[indexmyschedule].id.toString(),
                                                                                                leadership: datachild.currentClass!.leadership,
                                                                                                onpressed:paid==false? null:() {
                                                                                                   Navigator.push(context,
                                                                                                      PageTransition(type:PageTransitionType.bottomToTop,child:LessonDetailScreen(
                                                                                                        paid: paid,
                                                                                                        dataSchedule: dataMySchedule[indexmyschedule],
                                                                                                        profileChild: datachild.profileMedia!.fileShow.toString(),
                                                                                                        nameChild:translate=="km"?datachild.name.toString():datachild.nameEn.toString(),
                                                                                                      ),
                                                                                                    ),
                                                                                                  );
                                                                                                },
                                                                                                isTakeAttandance: dataMySchedule[indexmyschedule].isTakeAttendance,
                                                                                                paid: paid,
                                                                                                attStatus: dataMySchedule[indexmyschedule].attendanceStatus,
                                                                                                titleSubject: dataMySchedule[indexmyschedule].scheduleType==2?dataMySchedule[indexmyschedule].event
                                                                                                            : translate=="km"?dataMySchedule[indexmyschedule].subjectName.toString():dataMySchedule[indexmyschedule].subjectNameEn.toString(),
                                                                                                isActive: dataMySchedule[indexmyschedule].isCurrent,
                                                                                                subjectEvent: dataMySchedule[indexmyschedule].scheduleType==2?true:false,
                                                                                                subjectColor: dataMySchedule[indexmyschedule].isExam == false ? "0xFF${dataMySchedule[indexmyschedule].subjectColor}" : "0xFFEE964B",
                                                                                                scheduleType: dataMySchedule[indexmyschedule].isExam == true ? 1 : 2,
                                                                                                examDate: dataMySchedule[indexmyschedule].isExam == true ? dataMySchedule[indexmyschedule].examItem!.examDate.toString() : "",
                                                                                                homeworkExDate: homeList!.isEmpty ? "" : homeList![0].expiredAt.toString(),
                                                                                                lessonAbout: lessList!.isEmpty ? "" : lessList![0].title.toString(),
                                                                                                homeList: homeList,
                                                                                                lessList: lessList,
                                                                                                examApprov: dataMySchedule[indexmyschedule].isExam == true?dataMySchedule[indexmyschedule].examItem!.isApprove:0,
                                                                                                durationSubject: dataMySchedule[indexmyschedule].duration.toString(),
                                                                                                timeSubject: "${dataMySchedule[indexmyschedule].startTime} - ${dataMySchedule[indexmyschedule].endTime}",
                                                                                              );
                                                                                            },
                                                                                          ),
                                                                                        );
                                                                                } else if (state
                                                                                    is GetScheduleError) {
                                                                                  return  Center(
                                                                                    child: Text("WE_DETECT".tr(),style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.lightBlack),textAlign: TextAlign.center,),
                                                                                  );
                                                                                } else {
                                                                                  return Center(
                                                                                    child: Text("WE_DETECT".tr(),style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.lightBlack),textAlign: TextAlign.center,),
                                                                                  );
                                                                                }
                                                                              },
                                                                            ),
                                                                         )
                                                                        ],
                                                                      ),
                                                                      //======================= Date List =======================================
                                                                        AnimatedPositioned(
                                                                            top: offset>130?-110:0,
                                                                            left: 0,
                                                                            right: 0,
                                                                            duration: const Duration(milliseconds:300),
                                                                            child: Container(
                                                                                decoration:
                                                                                    const BoxDecoration(boxShadow: <BoxShadow>[
                                                                                  BoxShadow(
                                                                                      color: Colorconstands.neutralGrey,
                                                                                      blurRadius: 10.0,
                                                                                      offset: Offset(5.0, 5.7))
                                                                                ], color: Colorconstands.neutralSecondBackground),
                                                                                width: MediaQuery.of(context).size.width,
                                                                                height:MediaQuery.of(context).size.width>800? 190:95,
                                                                                padding: const EdgeInsets.symmetric(vertical:10.0,horizontal:0.0),
                                                                                child: PageView.builder(
                                                                                    controller: PageController(initialPage:monthlocalfirst != monthlocal || yearchecklocal != yearchecklocalfirst ?0:activeIndexDay <=6? 0:activeIndexDay >=7&&activeIndexDay <=13?1:activeIndexDay >=14&&activeIndexDay <=20?2:activeIndexDay >=21&&activeIndexDay <=27?3:activeIndexDay >=28&&activeIndexDay <=34?4:5),
                                                                                    scrollDirection: Axis.horizontal,
                                                                                    itemCount:listMonthData.length<=35?5:6,
                                                                                    itemBuilder: (context, indexDate) {
                                                                                      return GridView.builder(
                                                                                        physics:const NeverScrollableScrollPhysics(),
                                                                                        padding:const EdgeInsets.all(0),
                                                                                        itemCount: indexDate==0?listDayWeek1.length:indexDate==1?listDayWeek2.length:indexDate==2?listDayWeek3.length:indexDate==3?listDayWeek4.length:indexDate==4?listDayWeek5.length:listDayWeek6.length,
                                                                                        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 7,childAspectRatio: 0.8),
                                                                                        itemBuilder: (context, index) {
                                                                                          if(listDayWeek1[index].day!.toInt() == daySelectNoCurrentMonth){
                                                                                            selectDay = true;
                                                                                          }
                                                                                        else{
                                                                                            selectDay = false;
                                                                                          }
                                                                                          return indexDate==0? DatePickerWidget(
                                                                                            disable: listDayWeek1[index].disable!,
                                                                                            eveninday: false,
                                                                                            selectedday:listDayWeek1[index].day!.toInt() == daySelectNoCurrentMonth? selectDay:listDayWeek1[index].isActive!,
                                                                                            day:listDayWeek1[index].shortName.toString().tr(),
                                                                                            weekday:listDayWeek1[index].day.toString().tr(),
                                                                                            onPressed: () {
                                                                                              if(listDayWeek1[index].disable==true){}
                                                                                              else{
                                                                                                setState(() {
  
                                                                                                  if(listDayWeek1[index].day!.toInt() == daySelectNoCurrentMonth){}
                                                                                                  else{
                                                                                                    daySelectNoCurrentMonth = 100;
                                                                                                  }
                                                                                                  daylocal = listDayWeek1[index].day;
                                                                                                  for (var element in listMonthData) {
                                                                                                    element.isActive = false;
                                                                                                  }
                                                                                                  date = listDayWeek1[index].date;
                                                                                                  listMonthData[listDayWeek1[index].index!.toInt()].isActive = true;                                                                                                date = "$daylocal/$monthlocal/$yearchecklocal";
                                                                                                  BlocProvider.of<GetScheduleBloc>(context).add(GetSchedule(date: date, childId: childId, classId: classId));
                                                                                                                                
                                                                                                });
                                                                                              }
                                                                                            },
                                                                                          ):indexDate==1? DatePickerWidget(
                                                                                            disable: listDayWeek2[index].disable!,
                                                                                            eveninday: false,
                                                                                            selectedday: listDayWeek2[index].isActive!,
                                                                                            day:listDayWeek2[index].shortName.toString().tr(),
                                                                                            weekday:listDayWeek2[index].day.toString(),
                                                                                            onPressed: () {
                                                                                              setState(() {
                                                                                                daySelectNoCurrentMonth = 100;
                                                                                                daylocal = listDayWeek2[index].day;
                                                                                                for (var element in listMonthData) {
                                                                                                  element.isActive = false;
                                                                                                }
                                                                                                date = listDayWeek2[index].date;
                                                                                                listMonthData[listDayWeek2[index].index!.toInt()].isActive = true;                                                                                                date = "$daylocal/$monthlocal/$yearchecklocal";
                                                                                                BlocProvider.of<GetScheduleBloc>(context).add(GetSchedule(date: date, childId: childId, classId: classId));
                                                                                                                                
                                                                                              });
                                                                                            },
                                                                                          ):indexDate==2? DatePickerWidget(
                                                                                            disable: listDayWeek3[index].disable!,
                                                                                            eveninday: false,
                                                                                            selectedday: listDayWeek3[index].isActive!,
                                                                                            day:listDayWeek3[index].shortName.toString().tr(),
                                                                                            weekday:listDayWeek3[index].day.toString(),
                                                                                            onPressed: () {
                                                                                              setState(() {
                                                                                                daySelectNoCurrentMonth = 100;
                                                                                                daylocal = listDayWeek3[index].day;
                                                                                                for (var element in listMonthData) {
                                                                                                  element.isActive = false;
                                                                                                }
                                                                                                date = listDayWeek3[index].date;
                                                                                                listMonthData[listDayWeek3[index].index!.toInt()].isActive = true;                                                                                                date = "$daylocal/$monthlocal/$yearchecklocal";
                                                                                                BlocProvider.of<GetScheduleBloc>(context).add(GetSchedule(date: date, childId: childId, classId: classId));
                                                                                                                                
                                                                                              });
                                                                                            },
                                                                                          ):indexDate==3? DatePickerWidget(
                                                                                            disable: listDayWeek4[index].disable!,
                                                                                            eveninday: false,
                                                                                            selectedday: listDayWeek4[index].isActive!,
                                                                                            day:listDayWeek4[index].shortName.toString().tr(),
                                                                                            weekday:listDayWeek4[index].day.toString(),
                                                                                            onPressed: () {
                                                                                              setState(() {
                                                                                                daySelectNoCurrentMonth = 100;
                                                                                                daylocal = listDayWeek4[index].day;
                                                                                                for (var element in listMonthData) {
                                                                                                  element.isActive = false;
                                                                                                }
                                                                                                date = listDayWeek4[index].date;
                                                                                                listMonthData[listDayWeek4[index].index!.toInt()].isActive = true;                                                                                                date = "$daylocal/$monthlocal/$yearchecklocal";
                                                                                                BlocProvider.of<GetScheduleBloc>(context).add(GetSchedule(date: date, childId: childId, classId: classId));
                                                                                                                                
                                                                                              });
                                                                                            },
                                                                                          ):indexDate==4? DatePickerWidget(
                                                                                            disable: listDayWeek5[index].disable!,
                                                                                            eveninday: false,
                                                                                            selectedday: listDayWeek5[index].isActive!,
                                                                                            day:listDayWeek5[index].shortName.toString().tr(),
                                                                                            weekday:listDayWeek5[index].day.toString(),
                                                                                            onPressed: () {
                                                                                              setState(() {
                                                                                                daySelectNoCurrentMonth = 100;
                                                                                                daylocal = listDayWeek5[index].day;
                                                                                                for (var element in listMonthData) {
                                                                                                  element.isActive = false;
                                                                                                }
                                                                                                date = listDayWeek5[index].date;
                                                                                                listMonthData[listDayWeek5[index].index!.toInt()].isActive = true;                                                                                                date = "$daylocal/$monthlocal/$yearchecklocal";
                                                                                                BlocProvider.of<GetScheduleBloc>(context).add(GetSchedule(date: date, childId: childId, classId: classId));
                                                                                              });
                                                                                            },
                                                                                          ): DatePickerWidget(
                                                                                            disable: listDayWeek6[index].disable!,
                                                                                            eveninday: false,
                                                                                            selectedday: listDayWeek6[index].isActive!,
                                                                                            day:listDayWeek6[index].shortName.toString().tr(),
                                                                                            weekday:listDayWeek6[index].day.toString(),
                                                                                            onPressed: () {
                                                                                              setState(() {
                                                                                                daySelectNoCurrentMonth = 100;
                                                                                                daylocal = listDayWeek6[index].day;
                                                                                                for (var element in listMonthData) {
                                                                                                  element.isActive = false;
                                                                                                }
                                                                                                date = listDayWeek6[index].date;
                                                                                                listMonthData[listDayWeek6[index].index!.toInt()].isActive = true;                                                                                                date = "$daylocal/$monthlocal/$yearchecklocal";
                                                                                                BlocProvider.of<GetScheduleBloc>(context).add(GetSchedule(date: date, childId: childId, classId: classId));
                                                                                                                                
                                                                                              });
                                                                                            },
                                                                                          );
                                                                                        },);
                                                                                    }),
                                                                              ),
                                                                          ),
                                                                      //======================= End Date List =======================================
                                                                      showListDay == false
                                                                          ? Container()
                                                                          : Positioned(
                                                                              bottom: 0,
                                                                              left: 0,
                                                                              right: 0,
                                                                              top: 0,
                                                                              child:
                                                                                  GestureDetector(
                                                                                onTap: (() {
                                                                                  setState(
                                                                                      () {
                                                                                    showListDay =
                                                                                        false;
                                                                                  });
                                                                                }),
                                                                                child:
                                                                                    Container(
                                                                                  color: const Color(
                                                                                      0x7B9C9595),
                                                                                ),
                                                                              ),
                                                                            ),
                                                                      AnimatedPositioned(
                                                                              top: showListDay ==
                                                                                      false
                                                                                  ?MediaQuery.of(context).size.width> 800?-300: -170
                                                                                  : 0,
                                                                              left: 0,
                                                                              right: 0,
                                                                              duration: const Duration(milliseconds:300),
                                                                              child:Container(
                                                                                color: Colorconstands.neutralWhite,
                                                                                child:
                                                                                    GestureDetector(
                                                                                  onTap:() {
                                                                                    setState(() {
                                                                                      showListDay = false;
                                                                                    });
                                                                                  },
                                                                                  child:Container(
                                                                                    height: MediaQuery.of(context).size.width> 800?280:150,
                                                                                    margin:const EdgeInsets.only(top: 15,bottom: 5,left: 12,right: 12),
                                                                                    child: GridView.builder(
                                                                                      physics:const ScrollPhysics(),
                                                                                      padding:const EdgeInsets.all(.0),
                                                                                        itemCount: 12,
                                                                                        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 6),
                                                                                        itemBuilder: (BuildContext context, int index) {
                                                                                          return GestureDetector(
                                                                                            onTap: (() {
                                                                                              setState(() {
                                                                                                if(monthlocal == index+1){}
                                                                                                else{
                                                                                                  setState(() {
                                                                                                    offset = 0.0;
                                                                                                  });
                                                                                                  if(index+1==monthlocalfirst){
                                                                                                    setState(() {
                                                                                                      daylocal = daylocalfirst;
                                                                                                    });
                                                                                                  }
                                                                                                  else{
                                                                                                    setState(() {
                                                                                                      daylocal = 1;
                                                                                                    });
                                                                                                  }
                                                                                                  daySelectNoCurrentMonth = 1;
                                                                                                  showListDay = false;
                                                                                                  monthlocal = (index+1);
                                                                                                  date = "$daylocal/$monthlocal/$yearchecklocal";
                                                                                                  BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent( month: (index+1).toString(), year: yearchecklocal.toString()));
                                                                                                  BlocProvider.of<GetScheduleBloc>(context).add(GetSchedule(date:date,childId: datachild.id.toString(),
                                                                                                  classId: datachild.currentClass!.classId.toString()));
                                                                                                }
                                                                                              });
                                                                                            }),
                                                                                            child: Container(
                                                                                              margin:const EdgeInsets.all(6),
                                                                                              decoration:BoxDecoration(
                                                                                                borderRadius: BorderRadius.circular(12),
                                                                                                color: monthlocal == (index+1)? Colorconstands.primaryColor:Colors.transparent,
                                                                                              ),
                                                                                              height: 20,width: 20,
                                                                                              child:  Center(child: Text("${(index+1) == 1 ? "JANUARY" : (index+1) == 2 ? "FEBRUARY" : (index+1) == 3 ? "MARCH" : (index+1) == 4 ? "APRIL" : (index+1) == 5 ? "MAY" : (index+1) == 6 ? "JUNE" : (index+1) == 7 ? "JULY" : (index+1) == 8 ? "AUGUST" : (index+1) == 9 ? "SEPTEMBER" : (index+1) == 10 ? "OCTOBER" : (index+1) == 11 ? "NOVEMBER" : "DECEMBER"}".tr(),style: ThemeConstands.headline6_SemiBold_14.copyWith(color: monthlocal==(index+1)?Colorconstands.neutralWhite:Colorconstands.neutralDarkGrey),textAlign: TextAlign.center,)),
                                                                                            ),
                                                                                          );
                                                                                        },
                                                                                      ),
                                                                                  )
                                                                                ),
                                                                              ),
                                                                            ),
                                                                        monthlocal == monthlocalfirst && yearchecklocal == yearchecklocalfirst && daylocal == daylocalfirst ?Container():
                                                                        AnimatedPositioned(
                                                                          bottom: 30,right: 30,
                                                                          duration: const Duration(milliseconds:500),
                                                                          child: Container(
                                                                            height: 55,width: 55,
                                                                            decoration: BoxDecoration(
                                                                              boxShadow: [
                                                                                BoxShadow(
                                                                                  color: Colorconstands.neutralGrey.withOpacity(0.7),
                                                                                  spreadRadius: 4,
                                                                                  blurRadius: 4,
                                                                                  offset:const Offset(0, 3), // changes position of shadow
                                                                                ),
                                                                              ],
                                                                              color: Colorconstands.neutralWhite,
                                                                              shape: BoxShape.circle
                                                                            ),
                                                                            child: MaterialButton(
                                                                              elevation: 8,
                                                                              onPressed: (){
                                                                                setState(() {
                                                                                  offset = 0.0;
                                                                                   yearchecklocal = yearchecklocalfirst;
                                                                                    daySelectNoCurrentMonth = 100;
                                                                                    daylocal = daylocalfirst;
                                                                                    monthlocal = monthlocalfirst;
                                                                                    showListDay = false;
                                                                                });
                                                                                BlocProvider.of<GetDateBloc>(context).add(GetListDateEvent( month: monthlocalfirst.toString(), year: yearchecklocalfirst.toString()));
                                                                                BlocProvider.of<GetScheduleBloc>(context).add(GetSchedule(date:"$daylocalfirst/$monthlocalfirst/$yearchecklocalfirst",childId: childId.toString(),classId: classId.toString()));
                                                                              },
                                                                              shape:const CircleBorder(),
                                                                              child: SvgPicture.asset(ImageAssets.current_date,width: 35,height: 35,),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        
                                                                    ],
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      );
                                                    } else if (state
                                                        is GetDateListError) {
                                                      return  Center(
                                                        child: Text("WE_DETECT".tr(),style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.lightBlack),textAlign: TextAlign.center,),
                                                      );
                                                    } else {
                                                      return Container();
                                                    }
                                                  },
                                                ),
                                              ),
                                            ),
                                          ],
                                        );
                                } else if (state is ProfileUserErrorState) {
                                  return Container(
                                    decoration:const BoxDecoration(
                                      color: Colorconstands.neutralWhite,
                                      borderRadius: BorderRadius.only(topLeft: Radius.circular(15),topRight:  Radius.circular(15))
                                    ),
                                    child: ErrorRequestData(
                                      onPressed: (){
                                        Navigator.pushReplacement(
                                            context, 
                                            PageRouteBuilder(
                                                pageBuilder: (context, animation1, animation2) => ScheduleScreen(),
                                                transitionDuration: Duration.zero,
                                                reverseTransitionDuration: Duration.zero,
                                            ),
                                        );
                                      },
                                      discription: '', 
                                      hidebutton: true, 
                                      title: 'WE_DETECT_ERROR'.tr(),

                                    ),
                                  );
                                } else {
                                  return Container(
                                    decoration:const BoxDecoration(
                                      color: Colorconstands.neutralWhite,
                                      borderRadius: BorderRadius.only(topLeft: Radius.circular(15),topRight:  Radius.circular(15))
                                    ),
                                    child: ErrorRequestData(
                                      onPressed: (){
                                        Navigator.pushReplacement(
                                            context, 
                                            PageRouteBuilder(
                                                pageBuilder: (context, animation1, animation2) => ScheduleScreen(),
                                                transitionDuration: Duration.zero,
                                                reverseTransitionDuration: Duration.zero,
                                            ),
                                        );
                                      },
                                      discription: '', 
                                      hidebutton: true, 
                                      title: 'WE_DETECT_ERROR'.tr(),

                                    ),
                                  );
                                }
                              },
                            ),
                          ),
                         ),
                        ),
                      ],
                    ),
                  ),
                isoffline == true
                ? Container()
                : Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    top: 0,
                    child: Container(
                      color: const Color(0x7B9C9595),
                    ),
                  ),
                  AnimatedPositioned(
                    bottom: isoffline == true ? -150 : 0,
                    left: 0,
                    right: 0,
                    duration: const Duration(milliseconds: 500),
                    child: const NoConnectWidget(),
                  ),
                ],
              ),
            ),
          );
  }
}
