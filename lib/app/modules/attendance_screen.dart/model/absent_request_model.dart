class MonthData {
  final int month;
  final String name;
  final String shortname;
  late  bool isSelect;
  MonthData( 
      {required this.isSelect,
        required this.month,
      required this.name,
      required this.shortname,
      });
}

List<MonthData> monthdata =[
   MonthData(month:1,name: 'JANUARY',shortname: "Jan",isSelect: false),
   MonthData(month:2,name: 'FEBRUARY',shortname: "Feb",isSelect: false),
   MonthData(month:3,name: 'MARCH',shortname: "Mar",isSelect: false),
   MonthData(month:4,name: 'APRIL',shortname: "Apr",isSelect: false),
   MonthData(month:5,name: 'MAY',shortname: "May",isSelect: false),
   MonthData(month:6,name: 'JUNE',shortname: "Jun",isSelect: false),
   MonthData(month:7,name: 'JULY',shortname: "Jul",isSelect: false),
   MonthData(month:8,name: 'AUGUST',shortname: "Aug",isSelect: false),
   MonthData(month:9,name: 'SEPTEMBER',shortname: "Sep",isSelect: false),
   MonthData(month:10,name: 'OCTOBER',shortname: "Oct",isSelect: false),
   MonthData(month:11,name: 'NOVEMBER',shortname: "Nov",isSelect: false),
   MonthData(month:12,name: 'DECEMBER',shortname: "Dec",isSelect: false),
];
