import 'dart:async';
import 'dart:io';
import 'dart:ui';
import 'package:camis_application_flutter/app/core/resources/asset_resource.dart';
import 'package:camis_application_flutter/app/modules/auth_screens/e_school_code.dart';
import 'package:camis_application_flutter/widgets/take_upload_image.dart';
import 'package:camis_application_flutter/widgets/top_bar_widget/top_bar_one_line_widget.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:image_picker/image_picker.dart';

import '../../../../service/apis/profile_user/bloc/get_profile_user_bloc.dart';
import '../../../../widgets/no_connection_alert_widget.dart';
import '../../../../widgets/textFormfiled_custom/textformfiled_custom.dart';
import '../../edit_profile/bloc/change_info_user_bloc.dart';

class MyProfileScreen extends StatefulWidget {
  const MyProfileScreen({Key? key}) : super(key: key);

  @override
  State<MyProfileScreen> createState() => _MyProfileScreenState();
}

class _MyProfileScreenState extends State<MyProfileScreen> with Toast {
  final ImagePicker _picker = ImagePicker();
  File? _image;
  XFile? file;
  bool fristLoadata = true;
  bool checkFourcusGender = false;
  bool isLoading = false;
  bool connection = true;
  StreamSubscription? sub;
  String errorSelectGender = "";
  int genderNumber = 0;
  final TextEditingController _firstnameKhController = TextEditingController();
  final TextEditingController _lastnameKhController = TextEditingController();
  final TextEditingController _firstnameEnController = TextEditingController();
  final TextEditingController _lastnameEnController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _dobController = TextEditingController();
  final TextEditingController _addressController = TextEditingController();

  @override
  void initState() {
     setState(() {
      //=============Check internet====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          connection = (event != ConnectivityResult.none);
        });
      });
      //=============Eend Check internet====================
    });
   // BlocProvider.of<GetProfileUserBloc>(context).add(GetProfileUser());
    super.initState();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
        bottomNavigationBar: connection==true?Container(
            child: MaterialButton(
          padding: const EdgeInsets.all(0),
          onPressed: genderNumber==0?() {
            setState(() {
              errorSelectGender = "PLEASE_SELECT_GENDER".tr();
            });
          }:() {
            setState(() {
              errorSelectGender = "";
            });
            if (_image == null) {
              BlocProvider.of<ChangeInfoUserBloc>(context).add(
                ChangeInforNotimageData(
                  fristname: _firstnameKhController.text.trim(),
                  lastname: _lastnameKhController.text.trim(),
                  gmail: _emailController.text.trim(),
                  dob: _dobController.text.trim(),
                  phone: _phoneController.text.trim(),
                  gender:genderNumber.toString(),
                  address: _addressController.text.trim(),
                  fristnameen: _firstnameEnController.text.trim(),
                  lastnameen: _lastnameEnController.text.trim(),
                ),
              );
            } else {
              BlocProvider.of<ChangeInfoUserBloc>(context).add(
                ChangeInforData(
                  fristname: _firstnameKhController.text.trim(),
                  lastname: _lastnameKhController.text.trim(),
                  gmail: _emailController.text.trim(),
                  address: _addressController.text.trim(),
                  dob: _dobController.text.trim(),
                  gender:genderNumber.toString(),
                  phone: _phoneController.text.trim(),
                  file: _image!,
                  fristnameen: _firstnameEnController.text.trim(),
                  lastnameen: _lastnameEnController.text.trim(),
                ),
              );
            }
          },
          height: 65,
          color: Colorconstands.primaryColor,
          minWidth: double.maxFinite,
          child: Text(
            "SAVE".tr(),
            style: ThemeConstands.headline3_Medium_20
                .copyWith(color: Colors.white),
          ),
        )):Container(height: 0,),
        body: BlocListener<ChangeInfoUserBloc, ChangeInfoUserState>(
          listener: (context, state) {
            if(state is ChangeLoading){
              setState(() {
                isLoading = true;
              });
            }
            else if(state is ChangeInforLoaded){
              setState(() {
                showSuccessDialog(title: "SUCCESSFULLY".tr(),discription: "", callback: () { 
                  BlocProvider.of<GetProfileUserBloc>(context).add(GetProfileUser());
                    Navigator.of(context).pop();
                 },);
                isLoading = false;
              });
            }
            else if(state is ChangeError){
              setState(() {
                showErrorDialog(() {
                    Navigator.of(context).pop();
                  },context);
              });
              setState(() {
                isLoading = false;
              });
            }
          },
          child: Stack(
            children: [
              Positioned(
                top: MediaQuery.of(context).size.height - 300,
                left: 0,
                child: Image.asset("assets/images/Path_21.png"),
              ),
              Positioned(
                top: 0,
                right: 0,
                child: Image.asset("assets/images/Path_22.png"),
              ),
              Positioned(
                top: 0,
                right: 0,
                left: 0,
                bottom: 0,
                child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 50, sigmaY: 50),
                  child: Column(
                    children: [
                      const SizedBox(height: 35,),
                      Container(
                          margin: const EdgeInsets.symmetric(
                              vertical: 16, horizontal: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                child: Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  child: TopBarOneLineWidget(
                                      titleTopBar: "MY_ACCOUNT".tr()),
                                ),
                              ),
                              const Icon(
                                Icons.more_vert,
                                size: 24,
                                color: Colors.transparent,
                              ),
                            ],
                          )),
                      Expanded(
                        child: Container(
                          child: BlocBuilder<GetProfileUserBloc,
                              GetProfileUserState>(
                            builder: (context, state) {
                              if (state is ProfileUserLoadingState) {
                                return const Center(
                                  child: CircularProgressIndicator(),
                                );
                              } else if (state is ProfileUserLoaded) {
                                var data = state.profileusermodel.data;
                                if (fristLoadata == true) {
                                  _firstnameEnController.text = data!.firstnameEn.toString();
                                  _lastnameEnController.text = data.lastnameEn.toString();
                                  _firstnameKhController.text = data.firstname.toString();
                                  _lastnameKhController.text = data.lastname.toString();
                                  _emailController.text = data.email.toString();
                                  _phoneController.text = data.phone.toString();
                                  _dobController.text = data.dob.toString();
                                  _addressController.text = data.address.toString();
                                  genderNumber = data.gender!;
                                  fristLoadata = false;
                                } else {
                                  _firstnameKhController.text;
                                  _lastnameKhController.text;
                                  _emailController.text;
                                  _phoneController.text;
                                  _dobController.text;
                                  _addressController.text;
                                  _firstnameEnController.text;
                                  _lastnameEnController.text;
                                }
                                return ListView(
                                  padding: const EdgeInsets.all(0),
                                  shrinkWrap: true,
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        setState(
                                          () {
                                            showModalBottomSheet(
                                              backgroundColor:
                                                  Colors.transparent,
                                              context: context,
                                              builder: (BuildContext bc) {
                                                return ShowPiker(
                                                  onPressedCamera: () {
                                                    _imgFromCamera();
                                                    Navigator.of(context)
                                                        .pop();
                                                  },
                                                  onPressedGalary: () {
                                                    _imgFromGallery();
                                                    Navigator.of(context)
                                                        .pop();
                                                  },
                                                );
                                              },
                                            );
                                          },
                                        );
                                      },
                                      child: Container(
                                          width: 108,
                                          height: 108,
                                          margin: const EdgeInsets.only(
                                              top: 25, bottom: 0),
                                          alignment: Alignment.center,
                                          child: Stack(
                                            alignment: Alignment.center,
                                            children: [
                                              CircleAvatar(
                                                radius: 54, // Image radius
                                                child: ClipOval(
                                                  child: _image == null
                                                      ? Image.network(
                                                          data!.profileMedia!
                                                              .fileShow
                                                              .toString(),
                                                          width: 108,
                                                          height: 108,
                                                          fit: BoxFit.cover,
                                                        )
                                                      : Image.file(
                                                          _image!,
                                                          width: 108,
                                                          height: 108,
                                                          fit: BoxFit.cover,
                                                        ),
                                                ),
                                              ),
                                              _image == null
                                                  ? Container(
                                                      width: 108,
                                                      height: 108,
                                                      decoration:
                                                          const BoxDecoration(
                                                              shape: BoxShape
                                                                  .circle,
                                                              color: Colors
                                                                  .black12),
                                                    )
                                                  : const SizedBox(),
                                              _image == null
                                                  ? const Center(
                                                      child: Icon(
                                                        Icons
                                                            .photo_camera_outlined,
                                                        color: Colorconstands
                                                            .neutralWhite,
                                                        size: 28,
                                                      ),
                                                    )
                                                  : const SizedBox()
                                            ],
                                          )),
                                    ),
                                    Container(
                                      margin: const EdgeInsets.only(
                                          bottom: 0, top: 5),
                                      padding: const EdgeInsets.all(10),
                                      child: GestureDetector(
                                        onTap: () {
                                          setState(
                                            () {
                                              showModalBottomSheet(
                                                backgroundColor:
                                                    Colors.transparent,
                                                context: context,
                                                builder: (BuildContext bc) {
                                                  return ShowPiker(
                                                    onPressedCamera: () {
                                                      _imgFromCamera();
                                                      Navigator.of(context)
                                                          .pop();
                                                    },
                                                    onPressedGalary: () {
                                                      _imgFromGallery();
                                                      Navigator.of(context)
                                                          .pop();
                                                    },
                                                  );
                                                },
                                              );
                                            },
                                          );
                                        },
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            SvgPicture.asset(
                                              ImageAssets.CAMERA_icon,
                                              height: 24,
                                              color:
                                                  Colorconstands.primaryColor,
                                            ),
                                            const SizedBox(width: 8.0),
                                            Text("CHANGE_PROFILE".tr(),
                                                style: ThemeConstands
                                                    .headline6_Medium_14
                                                    .copyWith(
                                                  color: Colorconstands
                                                      .primaryColor,
                                                  decoration: TextDecoration
                                                      .underline,
                                                )),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: const EdgeInsets.symmetric(
                                          horizontal: 22, vertical: 10),
                                      child: Column(
                                        children: [
                                          TextFormFiledCustom(
                                            keyboardType: TextInputType.name,
                                            enable: true,
                                            checkFourcus: false,
                                            controller: translate=="km"?_firstnameKhController:_firstnameEnController,
                                            onChanged: (String? value) {
                                              Future.delayed(
                                                  const Duration(seconds: 1),
                                                  () {
                                                fristLoadata = false;
                                              });
                                            },
                                            onFocusChange: () {},
                                            title: 'FIRST_NAME'.tr(),
                                          ),
                                          TextFormFiledCustom(
                                            keyboardType: TextInputType.name,
                                            enable: true,
                                            checkFourcus: false,
                                            controller: translate=="km"? _lastnameKhController : _lastnameEnController,
                                            onChanged: (String? value) {},
                                            onFocusChange: () {},
                                            title: 'LAST_NAME'.tr(),
                                          ),
                                          Row(
                                            children: [
                                              Container(
                                                margin:const EdgeInsets.only(top: 12,left: 12),
                                                alignment: Alignment.centerLeft,
                                                child: Text('GENDER'.tr(),style: ThemeConstands.subtitle1_Regular_16.copyWith( color: Colorconstands.darkGray),),
                                              ),
                                              Container(
                                                margin:const EdgeInsets.only(top: 12,left: 12),
                                                alignment: Alignment.centerLeft,
                                                child: Text(errorSelectGender,style: ThemeConstands.subtitle1_Regular_16.copyWith( color: Colorconstands.alertsDecline),),
                                              ),
                                            ],
                                          ),
                                          Container(
                                            child: Row(
                                              children: [
                                                Container(
                                                  child: Row(
                                                    children: [
                                                      Radio(
                                                        value:1,
                                                        groupValue: genderNumber,
                                                        onChanged: (int? value) {
                                                          setState(() {
                                                            errorSelectGender = "";
                                                          genderNumber = value!;
                                                          });
                                                        },
                                                      ),
                                                      Container(
                                                        child: Text('MALE'.tr(),style: ThemeConstands.subtitle1_Regular_16.copyWith(color: Colorconstands.darkGray),),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                const SizedBox(width: 20,),
                                                Container(
                                                  child: Row(
                                                    children: [
                                                      Radio(
                                                        value:2,
                                                        groupValue: genderNumber,
                                                        onChanged: (int? value) {
                                                          setState(() {
                                                            errorSelectGender = "";
                                                          genderNumber = value!;
                                                          });
                                                        },
                                                      ),
                                                      Container(
                                                        child: Text('FEMALE'.tr(),style: ThemeConstands.subtitle1_Regular_16.copyWith(color: Colorconstands.darkGray),),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          TextFormFiledCustom(
                                            keyboardType: TextInputType.datetime,
                                            enable: true,
                                            checkFourcus: false,
                                            controller: _dobController,
                                            onChanged: (String? value) {
                                            },
                                            onFocusChange: () {},
                                            title: 'DOB'.tr(),
                                          ),
                                          TextFormFiledCustom(
                                            keyboardType: TextInputType.phone,
                                            enable: true,
                                            checkFourcus: false,
                                            controller: _phoneController,
                                            onChanged: (String? value) {},
                                            onFocusChange: () {},
                                            title: 'NUMBER_PHONE'.tr(),
                                          ),
                                          TextFormFiledCustom(
                                            keyboardType: TextInputType.emailAddress,
                                            enable: true,
                                            checkFourcus: false,
                                            controller: _emailController,
                                            onChanged: (String? value) {},
                                            onFocusChange: () {},
                                            title: 'EMAIL'.tr(),
                                          ),
                                          TextFormFiledCustom(
                                            keyboardType: TextInputType.emailAddress,
                                            enable: true,
                                            checkFourcus: false,
                                            controller: _addressController,
                                            onChanged: (String? value) {},
                                            onFocusChange: () {},
                                            title: 'ADDRESS'.tr(),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                );
                              } else {
                                return Center(
                                  child: Column(
                                    children: const [
                                      CircularProgressIndicator(),
                                    ],
                                  ),
                                );
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              isLoading == false
                  ? Container()
                  : Positioned(
                      bottom: 0,
                      left: 0,
                      right: 0,
                      top: 0,
                      child: Container(
                        color: const Color(0x7B9C9595),
                        child: Center(
                          child: Container(
                            padding: const EdgeInsets.all(16),
                            height: 60,
                            width: 60,
                            decoration: BoxDecoration(
                              color: Colorconstands.neutralGrey,
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: const Center(
                              child: CircularProgressIndicator(),
                            ),
                          ),
                        ),
                      ),
                    ),
                AnimatedPositioned(
                  bottom: connection == true ? -150 : 0,
                  left: 0,
                  right: 0,
                  duration: const Duration(milliseconds: 500),
                  child: const NoConnectWidget(),
                ),
            ],
          ),
        ),
      ),
    );
  }

  void _imgFromCamera() async {
    XFile? pickedFile = await _picker.pickImage(source: ImageSource.camera);
    setState(() {
      _image = File(pickedFile!.path);
      file = pickedFile;
    });
  }

  void _imgFromGallery() async {
    XFile? pickedFile = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = File(pickedFile!.path);
      file = pickedFile;
      print("path-path " + file!.path.toString());
    });
  }
}
