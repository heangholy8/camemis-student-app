import 'dart:math' as math;
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class AboutScreen extends StatefulWidget {
  const AboutScreen({Key? key}) : super(key: key);

  @override
  State<AboutScreen> createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor.withOpacity(0.8),
      body: Container(
        child: Column(
          children: [
            Container(
                color: Colorconstands.primaryColor.withOpacity(0.8),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          margin: const EdgeInsets.only(
                              top: 50, left: 26, right: 26, bottom: 20),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: const Icon(
                                  Icons.arrow_back_ios_new,
                                  size: 24,
                                  color: Colorconstands.lightBackgroundsWhite,
                                ),
                              ),
                              Expanded(
                                  child: Text(
                                "អំពី CAMEMIS",
                                style: ThemeConstands.headline3_SemiBold_20
                                    .copyWith(
                                  color: Colorconstands.lightBackgroundsWhite,
                                ),
                                textAlign: TextAlign.center,
                              )),
                            ],
                          ))
                    ])),
            Expanded(
              child: Container(
                color: Colorconstands.primaryColor,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      margin:const EdgeInsets.only(top: 5,left: 15),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                            Container(
                              margin:const EdgeInsets.only(top: 0,right: 12),
                              child: SvgPicture.asset(ImageAssets.logoCamis,width: 45,height: 45,),
                            ),
                              Container(child: Text("CAMEMIS",style: ThemeConstands.headline1_SemiBold_32.copyWith(color: Colorconstands.neutralWhite,fontWeight: FontWeight.w600,fontSize: 40),textAlign: TextAlign.center,)),
                            ],
                          ),
                          Container(
                            margin:const EdgeInsets.only(left: 18),
                            child:Text("Recommended for Student",style: ThemeConstands.caption_Regular_12.copyWith(color: Colorconstands.neutralWhite,),),
                          )
                        ], 
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(bottom: 16.25),
              width: MediaQuery.of(context).size.width,
              color: Colorconstands.primaryColor,
              child: Text(
                "ver. 2.0.1\n©️2022 CAMIS Solutions. \nAll Rights Reserved.",
                textAlign: TextAlign.center,
                style: ThemeConstands.headline6_Regular_14_20height
                    .copyWith(color: Colorconstands.neutralWhite),
              ),
            ),
            Container(
              height: 70,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(ImageAssets.globe_icon),
                  const SizedBox(width: 16.38),
                  InkWell(
                    onTap: _launchURL,
                    child: const Text(
                      "Learn more",
                      style: TextStyle(
                        color: Colorconstands.neutralWhite,
                        decoration: TextDecoration.underline,
                        decorationThickness: 1,
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  _launchURL() async {
    const url = 'https://camis-info.camemis-learn.com/';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}

class AboutList extends StatelessWidget {
  final String title;
  void Function()? onPressed;
  final String subtitle;
  AboutList({Key? key, required this.title, this.onPressed, this.subtitle = ""})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: onPressed,
      padding: const EdgeInsets.all(0),
      child: Container(
        margin: const EdgeInsets.only(
          left: 20,
          right: 20,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 12),
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      title,
                      textAlign: TextAlign.justify,
                      style: ThemeConstands.headline4_Medium_18
                          .copyWith(color: Colorconstands.darkTextsRegular),
                    ),
                  ),
                  const Icon(
                    Icons.arrow_forward_ios,
                    size: 17,
                    color: Colorconstands.darkTextsRegular,
                  )
                ],
              ),
            ),
            subtitle == null
                ? const SizedBox(
                    height: 2.0,
                  )
                : const SizedBox(),
            subtitle == null
                ? Text(
                    subtitle,
                    style: ThemeConstands.caption_Regular_12
                        .copyWith(color: Colorconstands.neutralDarkGrey),
                  )
                : const SizedBox(),
            const SizedBox(
              height: 12.0,
            )
          ],
        ),
      ),
    );
  }
}
