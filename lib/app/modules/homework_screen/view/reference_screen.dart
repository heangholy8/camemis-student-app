import 'package:camis_application_flutter/app/modules/homework_screen/widgets/custom_viewgallery_widget.dart';
import 'package:camis_application_flutter/widgets/view_document.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../routes/e.route.dart';
import '../../teaching_learning_screen/widgets/custom_appbar_widget.dart';
import '../../time_line/widget/thumbnail_widget.dart';
import '../../time_line/widget/view_image.dart';
import '../bloc/homework_bloc.dart';

//sinat

class RefereneceScreen extends StatefulWidget {
  const RefereneceScreen({Key? key}) : super(key: key);
  @override
  State<RefereneceScreen> createState() => _RefereneceScreenState();
}

class _RefereneceScreenState extends State<RefereneceScreen>
    with TickerProviderStateMixin {
  final PageController _controller =
      PageController(initialPage: 0, keepPage: true);
  late AnimationController controller;
  bool _isClick = false;
  @override
  void initState() {
    // controller = AnimationController(vsync: this);
    // _controller.addListener(() {});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: BlocBuilder<HomeworkBloc, HomeworkState>(
        builder: (context, state) {
          if (state is HomeWorkLoadingState) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is HomeWorkLoadedState) {
            return SizedBox(
              height: height,
              width: width,
              child: Stack(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 56,
                    ),
                    child: Column(
                      children: [
                        CustomAppbarWidget(
                          title: translate == "en" ? "Document" : "ឯកសារ",
                          avatar: false,
                        ),
                        Expanded(
                          child: Container(
                            padding: const EdgeInsets.symmetric(vertical: 14.0),
                            width: width,
                            height: double.infinity,
                            decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                                topRight: Radius.circular(10),
                              ),
                            ),
                            child: SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: BlocBuilder<HomeworkBloc, HomeworkState>(
                                builder: (context, state) {
                                  if (state is HomeWorkLoadingState) {
                                    return const Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  }
                                  if (state is HomeWorkLoadedState) {
                                    var data = state
                                        .detailTeachingModel.data!.attachments;

                                    var listOfImage = data!
                                        .where((element) =>
                                            element.fileType!.contains("image"))
                                        .toList();
                                    var listOfVdo = data.where((element) =>
                                        element.fileType!.contains("video"));
                                    var listOfPdf = data
                                        .where((element) => element.fileType!
                                            .contains("application"))
                                        .toList();

                                    var listOfImageAndVideos = data
                                        .where(
                                          (element) =>
                                              element.fileType!
                                                  .contains("image") ||
                                              element.fileType!
                                                  .contains("video"),
                                        )
                                        .toList();
                                    return Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 18.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          // Videos and Images

                                          ListView.builder(
                                            padding: const EdgeInsets.all(0),
                                            physics:
                                                const NeverScrollableScrollPhysics(),
                                            shrinkWrap: true,
                                            itemCount:
                                                listOfImageAndVideos.length,
                                            itemBuilder:
                                                (context, index) =>
                                                    listOfImageAndVideos[index]
                                                            .fileType!
                                                            .contains("video")
                                                        ? Column(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              index == 0
                                                                  ? Text(
                                                                      translate ==
                                                                              "en"
                                                                          ? "Video"
                                                                          : "វីដេអូ",
                                                                      style: ThemeConstands
                                                                          .texttheme
                                                                          .subtitle1!
                                                                          .copyWith(
                                                                        color: Colorconstands
                                                                            .darkGray,
                                                                        fontWeight:
                                                                            FontWeight.w700,
                                                                      ),
                                                                    )
                                                                  : Container(),
                                                              index == 0
                                                                  ? const Divider()
                                                                  : Container(),
                                                              Column(
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  Padding(
                                                                    padding:
                                                                        const EdgeInsets
                                                                            .only(
                                                                      bottom:
                                                                          8.0,
                                                                    ),
                                                                    child: Text(
                                                                      listOfImageAndVideos[
                                                                              index]
                                                                          .fileName
                                                                          .toString(),
                                                                      style: ThemeConstands
                                                                          .texttheme
                                                                          .headline6!
                                                                          .copyWith(
                                                                        color: Colorconstands
                                                                            .primaryColor,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  ThumbnailVideoWidget(
                                                                    onPressed:
                                                                        () {
                                                                      Navigator
                                                                          .push(
                                                                        context,
                                                                        MaterialPageRoute(
                                                                          builder: (context) =>
                                                                              ImageViewDownloads(
                                                                            listimagevide:
                                                                                listOfImageAndVideos.toList(),
                                                                            activepage:
                                                                                index,
                                                                          ),
                                                                        ),
                                                                      );
                                                                    },
                                                                    image: listOfImageAndVideos[index].fileThumbnail ==
                                                                                '' ||
                                                                            listOfImageAndVideos[index].fileThumbnail ==
                                                                                null
                                                                        ? "https://1.bp.blogspot.com/-4ilA6TFD1nU/XbbgJXE1CLI/AAAAAAAAA3M/GwK-nwRZJ189IyiXIKSG-FROclbz_d9aQCLcBGAsYHQ/s1600/file-MrylO8jADD.png"
                                                                        : listOfImageAndVideos[index]
                                                                            .fileThumbnail
                                                                            .toString(),
                                                                  ),
                                                                ],
                                                              ),
                                                            ],
                                                          )
                                                        : InkWell(
                                                            onTap: () {
                                                              Navigator.push(
                                                                context,
                                                                MaterialPageRoute(
                                                                  builder:
                                                                      (context) =>
                                                                          ImageViewDownloads(
                                                                    listimagevide:
                                                                        listOfImageAndVideos
                                                                            .toList(),
                                                                    activepage:
                                                                        index,
                                                                  ),
                                                                ),
                                                              );
                                                            },
                                                            child: Column(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .center,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              children: [
                                                                index == 0
                                                                    ? Text(
                                                                        translate ==
                                                                                "en"
                                                                            ? "Image"
                                                                            : "រូបភាព",
                                                                        style: ThemeConstands
                                                                            .texttheme
                                                                            .subtitle1!
                                                                            .copyWith(
                                                                          color:
                                                                              Colorconstands.darkGray,
                                                                          fontWeight:
                                                                              FontWeight.w700,
                                                                        ),
                                                                      )
                                                                    : Container(),
                                                                index == 0
                                                                    ? const Divider()
                                                                    : Container(),
                                                                Container(
                                                                  width: width,
                                                                  padding:
                                                                      const EdgeInsets
                                                                          .symmetric(
                                                                    vertical:
                                                                        18.0,
                                                                  ),
                                                                  decoration:
                                                                      const BoxDecoration(
                                                                    border:
                                                                        Border(
                                                                      bottom:
                                                                          BorderSide(
                                                                        color: Color(
                                                                            0XFFDEDEDE),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  child: listOfImage
                                                                              .length ==
                                                                          1
                                                                      ? Hero(
                                                                          tag:
                                                                              "s",
                                                                          child:
                                                                              Image(
                                                                            image:
                                                                                NetworkImage("${listOfImageAndVideos[index].fileThumbnail}"),
                                                                            fit:
                                                                                BoxFit.contain,
                                                                            width:
                                                                                width / 2.2,
                                                                            height:
                                                                                MediaQuery.of(context).size.height / 2.2,
                                                                          ),
                                                                        )
                                                                      : Align(
                                                                          alignment:
                                                                              Alignment.center,
                                                                          child:
                                                                              Wrap(
                                                                            children:
                                                                                List.generate(
                                                                              listOfImage.length,
                                                                              (index) => InkWell(
                                                                                onTap: () {
                                                                                  print("object");
                                                                                },
                                                                                child: Container(
                                                                                  width: 160,
                                                                                  height: 150,
                                                                                  margin: const EdgeInsets.all(8.0),
                                                                                  decoration: BoxDecoration(
                                                                                    borderRadius: BorderRadius.circular(
                                                                                      8.0,
                                                                                    ),
                                                                                    boxShadow: [
                                                                                      BoxShadow(
                                                                                        offset: const Offset(0, 1),
                                                                                        blurRadius: 8,
                                                                                        color: Colors.black.withOpacity(.2),
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                  child: FadeInImage(
                                                                                    placeholder: const AssetImage(
                                                                                      "assets/images/placeholder_image.png",
                                                                                    ),
                                                                                    image: NetworkImage("${listOfImage[index].fileThumbnail}"),
                                                                                    fit: BoxFit.cover,
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                          ),
                                          listOfPdf.isNotEmpty
                                              ? Align(
                                                  alignment: Alignment.center,
                                                  child: ListView.separated(
                                                    shrinkWrap: true,
                                                    padding:
                                                        const EdgeInsets.all(0),
                                                    physics:
                                                        const NeverScrollableScrollPhysics(),
                                                    itemBuilder:
                                                        (context, index) {
                                                      return Column(
                                                        children: [
                                                          index == 0
                                                              ? Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  children: [
                                                                    Padding(
                                                                      padding:
                                                                          const EdgeInsets
                                                                              .only(
                                                                        top:
                                                                            18.0,
                                                                      ),
                                                                      child:
                                                                          Text(
                                                                        translate ==
                                                                                'en'
                                                                            ? "Documents: "
                                                                            : "ឯកសារ: ",
                                                                        style: ThemeConstands
                                                                            .texttheme
                                                                            .subtitle1!
                                                                            .copyWith(
                                                                          color:
                                                                              Colorconstands.darkGray,
                                                                          fontWeight:
                                                                              FontWeight.w700,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    const Divider(),
                                                                  ],
                                                                )
                                                              : Container(),
                                                          MaterialButton(
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(0),
                                                            onPressed: () {
                                                              // Navigator.push(
                                                              //   context,
                                                              //   MaterialPageRoute(
                                                              //     builder:
                                                              //         (context) =>
                                                              //             ViewDocuments(
                                                              //       path: listOfPdf[
                                                              //               index]
                                                              //           .fileShow
                                                              //           .toString(),
                                                              //       filename: listOfPdf[
                                                              //               index]
                                                              //           .fileName
                                                              //           .toString(),
                                                              //     ),
                                                              //   ),
                                                              // );
                                                            },
                                                            child: Container(
                                                              width: width,
                                                              padding:
                                                                  const EdgeInsets
                                                                      .symmetric(
                                                                vertical: 8.0,
                                                              ),
                                                              decoration:
                                                                  BoxDecoration(
                                                                color: Colors
                                                                    .grey
                                                                    .shade100,
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            2),
                                                              ),
                                                              child: Row(
                                                                children: [
                                                                  Image.asset(
                                                                    "assets/images/pdf_icon.png",
                                                                  ),
                                                                  Column(
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .start,
                                                                    children: [
                                                                      SizedBox(
                                                                        width: width /
                                                                            1.5,
                                                                        child:
                                                                            Text(
                                                                          listOfPdf[index]
                                                                              .fileName
                                                                              .toString(),
                                                                          style:
                                                                              const TextStyle(
                                                                            fontWeight:
                                                                                FontWeight.w600,
                                                                            color:
                                                                                Colorconstands.darkGray,
                                                                          ),
                                                                          maxLines:
                                                                              1,
                                                                          overflow:
                                                                              TextOverflow.ellipsis,
                                                                        ),
                                                                      ),
                                                                      const SizedBox(
                                                                        height:
                                                                            4.0,
                                                                      ),
                                                                      Text(
                                                                        "9.4MB",
                                                                        style: ThemeConstands
                                                                            .texttheme
                                                                            .subtitle1!
                                                                            .copyWith(
                                                                          fontWeight:
                                                                              FontWeight.w500,
                                                                          color:
                                                                              Colorconstands.darkGray,
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      );
                                                    },
                                                    separatorBuilder:
                                                        (context, index) {
                                                      return const Padding(
                                                        padding: EdgeInsets
                                                            .symmetric(
                                                          horizontal: 0.0,
                                                        ),
                                                        child: Divider(),
                                                      );
                                                    },
                                                    itemCount: listOfPdf.length,
                                                  ),
                                                )
                                              : Container(),
                                          const SizedBox(
                                            height: 30,
                                          ),
                                        ],
                                      ),
                                    );
                                  }
                                  return Container();
                                },
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  _isClick
                      ? CustomViewGalleryWidget(
                          // pageController: _controller,
                          onTap: () {
                          setState(() {
                            _isClick = !_isClick;
                          });
                        })
                      : const SizedBox(),
                ],
              ),
            );
          }
          return const Center(
            child: Text("Something when wrong!"),
          );
        },
      ),
    );
  }
}
