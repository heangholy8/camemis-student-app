import 'package:camis_application_flutter/app/modules/homework_screen/view/reference_screen.dart';
import 'package:camis_application_flutter/app/modules/homework_screen/widgets/toggle_button.dart';
import 'package:camis_application_flutter/helper/route.export.dart';
import 'package:camis_application_flutter/widgets/shimmer_style.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../routes/e.route.dart';
import '../../time_line/widget/comment_widget.dart';
import '../components/flexible_header_shimmer.dart';
import '../widgets/tab_homework.dart';

class HomeWorkScreen extends StatefulWidget {
  final String idPost;
  const HomeWorkScreen({Key? key, required this.idPost}) : super(key: key);
  @override
  State<HomeWorkScreen> createState() => _HomeWorkScreenState();
}

class _HomeWorkScreenState extends State<HomeWorkScreen>
    with TickerProviderStateMixin {
  List<Tab> tabs = [
    const Tab(
      text: "កិច្ចការ",
    ),
    const Tab(
      text: "វេទិការ",
    ),
  ];

  late TabController _tabController;
  bool isClicked = false;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final FocusNode _titleFocus = FocusNode();
  final TextEditingController _textcomment = TextEditingController();
  final ScrollController scrollcontroller = ScrollController();
  bool isReply = false;
  String replyName = "";
  String replyText = "";
  bool reply = false;
  String idComment = '';
  String parentID = '';

  @override
  void initState() {
    _tabController = TabController(length: tabs.length, vsync: this);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();

    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            SizedBox(
              height: 68,
              child: Row(
                children: [
                  Container(
                    margin: const EdgeInsets.only(left: 12.0),
                    child: IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: const Icon(Icons.arrow_back_ios,
                          size: 23, color: Colorconstands.white),
                    ),
                  ),
                  Text(
                    "Homework",
                    style: ThemeConstands.texttheme.headline5!
                        .copyWith(color: Colorconstands.white),
                  )
                ],
              ),
            ),
            Expanded(
              child: Stack(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.only(top: 14.0),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(18.0),
                        topRight: Radius.circular(18.0),
                      ),
                    ),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(
                        children: [
                          BlocBuilder<HomeworkBloc, HomeworkState>(
                            builder: (context, state) {
                              if (state is HomeWorkLoadingState) {
                                return const FlexibleHeaderShimmer();
                              }
                              if (state is HomeWorkLoadedState) {
                                var data = state.detailTeachingModel.data;

                                print("sinatsinat${data.toString()}");
                                return Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 18.0,
                                        vertical: 18.0,
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                1.8,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  data!.name.toString(),
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: ThemeConstands
                                                      .texttheme.headline5!
                                                      .copyWith(
                                                    color: Colors.black,
                                                    fontWeight: FontWeight.w700,
                                                  ),
                                                ),
                                                const SizedBox(
                                                  height: 18.0,
                                                ),
                                                RichText(
                                                  text: TextSpan(
                                                    text: "Date: ",
                                                    style: ThemeConstands
                                                        .texttheme.subtitle1!
                                                        .copyWith(
                                                      color: Colorconstands
                                                          .darkGray,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                    ),
                                                    children: [
                                                      TextSpan(
                                                        text: data.startDate
                                                            .toString(),
                                                        style: ThemeConstands
                                                            .texttheme
                                                            .subtitle1!
                                                            .copyWith(
                                                          color: Colorconstands
                                                              .darkGray,
                                                          fontWeight:
                                                              FontWeight.w500,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                const SizedBox(
                                                  height: 8.0,
                                                ),
                                                RichText(
                                                  text: TextSpan(
                                                    text: "Deadline: ",
                                                    style: ThemeConstands
                                                        .texttheme.subtitle1!
                                                        .copyWith(
                                                      color: Colorconstands
                                                          .darkGray,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                    ),
                                                    children: [
                                                      TextSpan(
                                                        text: data.endDate
                                                            .toString(),
                                                        style: ThemeConstands
                                                            .texttheme
                                                            .subtitle1!
                                                            .copyWith(
                                                          color: Colorconstands
                                                              .darkGray,
                                                          fontWeight:
                                                              FontWeight.w500,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            decoration: const BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                Radius.circular(4.0),
                                              ),
                                            ),
                                            child: MaterialButton(
                                              shape:
                                                  const RoundedRectangleBorder(
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(4.0),
                                                ),
                                              ),
                                              minWidth: 30,
                                              color: Colorconstands.primaryColor,
                                              onPressed: () {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: ((context) =>
                                                        const RefereneceScreen()),
                                                  ),
                                                );
                                              },
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                  horizontal: 10.0,
                                                  vertical: 10.0,
                                                ),
                                                child: Text(
                                                  "Reference",
                                                  style: ThemeConstands
                                                      .texttheme.bodyText2!
                                                      .copyWith(
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.w700,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 18.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "Description: ",
                                            style: ThemeConstands
                                                .texttheme.subtitle1!
                                                .copyWith(
                                              color: Colorconstands.darkGray,
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                          Text(
                                            "${data.description}",
                                            style: ThemeConstands
                                                .texttheme.headline6,
                                            maxLines: 4,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                );
                              }
                              if (state is HomeWorkErrorState) {
                                return Center(
                                  child: Text(state.error.toString()),
                                );
                              }
                              return const Center(
                                child: Text("Please try again!!"),
                              );
                            },
                          ),
                          Container(
                            width: width,
                            margin: const EdgeInsets.only(top: 12.0),
                            padding: const EdgeInsets.all(12.0),
                            color: Colorconstands.primaryBackgroundColor,
                            child: ToggleButton(
                              width: width,
                              height: 55.0,
                              toggleBackgroundColor: Colors.transparent,
                              toggleBorderColor: Colors.transparent,
                              toggleColor: Colorconstands.primaryColor,
                              activeTextColor: Colors.white,
                              inactiveTextColor: Colors.grey,
                              leftDescription: 'កិច្ចការ',
                              rightDescription: 'វេទិការ',
                              onLeftToggleActive: () {
                                setState(() {
                                  isClicked = true;
                                });
                              },
                              onRightToggleActive: () {
                                setState(() {
                                  isClicked = false;
                                });
                              },
                            ),
                          ),
                          isClicked == false
                              ? TabHomeWork(
                                  width: width,
                                )
                              : InkWell(
                                  onTap: () {
                                    FocusScope.of(context).unfocus();
                                  },
                                  child: Container(
                                    padding: const EdgeInsets.only(bottom: 50),
                                    child: BlocBuilder<HomeworkBloc,
                                        HomeworkState>(
                                      builder: (context, state) {
                                        if (state is HomeWorkLoadingState) {
                                          return Expanded(
                                            child: ListView.builder(
                                              shrinkWrap: true,
                                              physics: const ScrollPhysics(),
                                              controller: scrollcontroller,
                                              itemBuilder:
                                                  (BuildContext contex, index) {
                                                return const SizedBox(
                                                  child: ShimmerComment(),
                                                );
                                              },
                                              itemCount: 2,
                                            ),
                                          );
                                        }
                                        if (state is HomeWorkLoadedState) {
                                          /// Initial idPost
                                          var data = state
                                              .detailTeachingModel.data!.forums;
                                          idComment = state
                                              .detailTeachingModel.data!.id
                                              .toString();
                                          return Container(
                                            margin: const EdgeInsets.symmetric(
                                              vertical: 12.0,
                                              horizontal: 0.0,
                                            ),
                                            child: data!.isEmpty
                                                ? ListView.builder(
                                                    shrinkWrap: true,
                                                    itemBuilder:
                                                        (BuildContext contex,
                                                            index) {
                                                      return Column(
                                                        children: [
                                                          Image.asset(
                                                            "assets/images/gifs/no_comment.gif",
                                                            width: MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width /
                                                                1.8,
                                                          ),
                                                          const SizedBox(
                                                            height: 12.0,
                                                          ),
                                                          Text(
                                                            "No comments yet",
                                                            style: ThemeConstands
                                                                .texttheme
                                                                .headline6,
                                                          )
                                                        ],
                                                      );
                                                    },
                                                    itemCount: 1,
                                                  )
                                                : ListView.builder(
                                                    shrinkWrap: true,
                                                    physics:
                                                        const ScrollPhysics(),
                                                    itemCount: data.length,
                                                    itemBuilder:
                                                        (BuildContext contex,
                                                            index) {
                                                      int selecticonreact = 5;
                                                      return Column(
                                                        mainAxisSize:
                                                            MainAxisSize.min,
                                                        children: [
                                                          WidgetComment(
                                                            hideReact: true,
                                                            onReact: (values,
                                                                isChecked) {},
                                                            marginprofileLeft:
                                                                12.0,
                                                            isCheckroot: true,
                                                            lenghtreply:
                                                                data[index]
                                                                    .children!
                                                                    .length,
                                                            margincommentbetweenprofile:
                                                                65.0,
                                                            namecomment: translate ==
                                                                    "en"
                                                                ? data[index].userProfile!.nameEn ==
                                                                            " " ||
                                                                        data[index].userProfile!.nameEn ==
                                                                            ""
                                                                    ? data[index]
                                                                        .userProfile!
                                                                        .name
                                                                        .toString()
                                                                    : data[index]
                                                                        .userProfile!
                                                                        .nameEn
                                                                        .toString()
                                                                : data[index]
                                                                    .userProfile!
                                                                    .name
                                                                    .toString(),
                                                            profilecomment: data[
                                                                    index]
                                                                .userProfile!
                                                                .profileMedia!
                                                                .fileShow
                                                                .toString(),
                                                            textcomment:
                                                                data[index]
                                                                    .comment
                                                                    .toString(),
                                                            iconReact:
                                                                "assets/icons/like_icon.png",
                                                            textReact: "",
                                                            profilehight: 45,
                                                            profilewidth: 45,
                                                            style: ThemeConstands
                                                                .texttheme
                                                                .subtitle1!
                                                                .copyWith(
                                                                    color: Colorconstands
                                                                        .black,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w500),
                                                            styletextpost:
                                                                ThemeConstands
                                                                    .texttheme
                                                                    .subtitle1!
                                                                    .copyWith(
                                                              color:
                                                                  Colorconstands
                                                                      .darkGray,
                                                            ),
                                                            colorlabelreact:
                                                                Colors.yellow,
                                                            selectedReaction:
                                                                selecticonreact,
                                                            reply: () {
                                                              FocusScope.of(
                                                                      context)
                                                                  .requestFocus();
                                                              setState(() {
                                                                reply = true;
                                                                replyName = translate ==
                                                                        "en"
                                                                    ? data[index].userProfile!.nameEn ==
                                                                                " " ||
                                                                            data[index].userProfile!.nameEn ==
                                                                                ""
                                                                        ? data[index]
                                                                            .userProfile!
                                                                            .name
                                                                            .toString()
                                                                        : data[index]
                                                                            .userProfile!
                                                                            .nameEn
                                                                            .toString()
                                                                    : data[index]
                                                                        .userProfile!
                                                                        .name
                                                                        .toString();
                                                                replyText = data[
                                                                        index]
                                                                    .comment
                                                                    .toString();
                                                                parentID = data[
                                                                        index]
                                                                    .id
                                                                    .toString();
                                                              });
                                                            },
                                                          ),
                                                          data[index]
                                                                  .children!
                                                                  .isEmpty
                                                              ? Container()
                                                              : Container(
                                                                  margin: const EdgeInsets
                                                                          .only(
                                                                      left: 0,
                                                                      right:
                                                                          0.0),
                                                                  child: Stack(
                                                                    children: [
                                                                      ListView.builder(
                                                                          padding: const EdgeInsets.all(0),
                                                                          physics: const ScrollPhysics(),
                                                                          shrinkWrap: true,
                                                                          itemCount: data[index].children!.length - 1,
                                                                          itemBuilder: (BuildContext contex, subIndex) {
                                                                            return Stack(
                                                                              children: [
                                                                                Positioned(
                                                                                  top: 0,
                                                                                  left: 35,
                                                                                  bottom: 0,
                                                                                  child: Container(
                                                                                    color: Colorconstands.gray,
                                                                                    width: 1,
                                                                                  ),
                                                                                ),
                                                                                Container(
                                                                                  margin: const EdgeInsets.only(left: 65),
                                                                                  child: WidgetComment(
                                                                                    hideReact: true,
                                                                                    marginprofileLeft: 5.0,
                                                                                    isCheckroot: false,
                                                                                    lenghtreply: data.length,
                                                                                    margincommentbetweenprofile: 50.0,
                                                                                    namecomment: translate == "en"
                                                                                        ? data[index].children![subIndex].userProfile!.nameEn == " " || data[index].children![subIndex].userProfile!.nameEn == ""
                                                                                            ? data[index].children![subIndex].userProfile!.name.toString()
                                                                                            : data[index].children![subIndex].userProfile!.nameEn.toString()
                                                                                        : data[index].children![subIndex].userProfile!.name.toString(),
                                                                                    profilecomment: data[index].children![subIndex].userProfile!.profileMedia!.fileShow.toString(),
                                                                                    textcomment: data[index].children![subIndex].comment.toString(),
                                                                                    //------------------------- Icon React--------------------------
                                                                                    iconReact: "assets/icons/like_icon.png",
                                                                                    //-------------------------End Icon React--------------------------
                                                                                    textReact: "",
                                                                                    profilehight: 30,
                                                                                    profilewidth: 30,
                                                                                    style: ThemeConstands.texttheme.subtitle2!.copyWith(color: Colorconstands.black, fontWeight: FontWeight.w500),
                                                                                    styletextpost: ThemeConstands.texttheme.subtitle2!.copyWith(
                                                                                      color: Colorconstands.darkGray,
                                                                                    ),
                                                                                    //------------------------- color Label React--------------------------
                                                                                    //colorlabelreact: Colorconstands.primaryColor,
                                                                                    colorlabelreact: Colors.yellow,
                                                                                    //-------------------------End color Label React--------------------------
                                                                                    selectedReaction: selecticonreact,
                                                                                    reply: () {},
                                                                                    onReact: (String? values, bool isChecked) {},
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            );
                                                                          }),
                                                                      Container(
                                                                        margin: const EdgeInsets.only(
                                                                            left:
                                                                                0),
                                                                        child: ListView
                                                                            .builder(
                                                                          padding:
                                                                              const EdgeInsets.all(0),
                                                                          physics:
                                                                              const ScrollPhysics(),
                                                                          shrinkWrap:
                                                                              true,
                                                                          itemCount: data[index]
                                                                              .children!
                                                                              .length,
                                                                          itemBuilder:
                                                                              (BuildContext contex, subIndex) {
                                                                            return Stack(
                                                                              children: [
                                                                                Positioned(
                                                                                  top: 0,
                                                                                  left: 35,
                                                                                  right: 0,
                                                                                  child: Container(
                                                                                    height: 25,
                                                                                    decoration: BoxDecoration(border: Border.all(color: Colorconstands.gray, width: 1.0, style: BorderStyle.solid), borderRadius: const BorderRadius.only(bottomLeft: Radius.circular(16))),
                                                                                  ),
                                                                                ),
                                                                                Positioned(
                                                                                  top: 0,
                                                                                  left: 36,
                                                                                  right: 0,
                                                                                  child: Container(
                                                                                    height: 1,
                                                                                    color: Colorconstands.white,
                                                                                  ),
                                                                                ),
                                                                                Container(
                                                                                  margin: const EdgeInsets.only(left: 65),
                                                                                  color: Colorconstands.white,
                                                                                  child: WidgetComment(
                                                                                    hideReact: true,
                                                                                    onReact: ((values, isChecked) {}),
                                                                                    marginprofileLeft: 5.0,
                                                                                    isCheckroot: false,
                                                                                    lenghtreply: data.length,
                                                                                    margincommentbetweenprofile: 50.0,
                                                                                    namecomment: translate == "en"
                                                                                        ? data[index].children![subIndex].userProfile!.nameEn == " " || data[index].children![subIndex].userProfile!.nameEn == ""
                                                                                            ? data[index].children![subIndex].userProfile!.name.toString()
                                                                                            : data[index].children![subIndex].userProfile!.nameEn.toString()
                                                                                        : data[index].children![subIndex].userProfile!.name.toString(),
                                                                                    profilecomment: data[index].children![subIndex].userProfile!.profileMedia!.fileShow.toString(),
                                                                                    textcomment: data[index].children![subIndex].comment.toString(),
                                                                                    //------------------------- Icon React--------------------------
                                                                                    iconReact: "assets/icons/like_icon.png",
                                                                                    //-------------------------End Icon React--------------------------
                                                                                    textReact: "",
                                                                                    profilehight: 30,
                                                                                    profilewidth: 30,
                                                                                    style: ThemeConstands.texttheme.subtitle2!.copyWith(color: Colorconstands.black, fontWeight: FontWeight.w500),
                                                                                    styletextpost: ThemeConstands.texttheme.subtitle2!.copyWith(
                                                                                      color: Colorconstands.darkGray,
                                                                                    ),
                                                                                    //------------------------- color Label React--------------------------
                                                                                    //colorlabelreact: Colorconstands.primaryColor,
                                                                                    colorlabelreact: Colors.yellow,
                                                                                    //-------------------------End color Label React--------------------------
                                                                                    selectedReaction: selecticonreact,
                                                                                    reply: () {
                                                                                      FocusScope.of(context).requestFocus();
                                                                                      setState(
                                                                                        () {
                                                                                          reply = true;
                                                                                          replyName = translate == "en"
                                                                                              ? data[index].children![subIndex].userProfile!.nameEn == " " || data[index].children![subIndex].userProfile!.nameEn == ""
                                                                                                  ? data[index].children![subIndex].userProfile!.name.toString()
                                                                                                  : data[index].children![subIndex].userProfile!.nameEn.toString()
                                                                                              : data[index].children![subIndex].userProfile!.name.toString();
                                                                                          replyText = data[index].children![subIndex].comment.toString();
                                                                                          parentID = "${data[index].id}";
                                                                                        },
                                                                                      );
                                                                                    },
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            );
                                                                          },
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                        ],
                                                      );
                                                    },
                                                  ),
                                          );
                                        } else {
                                          return Container();
                                        }
                                      },
                                    ),
                                  ),
                                )
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: isClicked == false
                        ? Container(
                            height: 0,
                          )
                        : Container(
                            color: Colorconstands.gray,
                            child: Column(
                              children: [
                                const Divider(
                                  height: 1,
                                  thickness: 1,
                                  color: Colorconstands.gray,
                                ),
                                reply == false
                                    ? Container()
                                    : Container(
                                        margin: const EdgeInsets.only(
                                            left: 12.0, right: 12.0, top: 12.0),
                                        child: Row(
                                          children: [
                                            Expanded(
                                                child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "Replying to " + replyName,
                                                  style: ThemeConstands
                                                      .texttheme.subtitle2!
                                                      .copyWith(
                                                          color: Colorconstands
                                                              .black,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                ),
                                                Text(
                                                  replyText,
                                                  style: ThemeConstands
                                                      .texttheme.subtitle2!
                                                      .copyWith(
                                                    color: Colorconstands.black,
                                                  ),
                                                  maxLines: 1,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            )),
                                            InkWell(
                                              onTap: () {
                                                setState(() {
                                                  reply = false;
                                                });
                                              },
                                              child: AnimatedContainer(
                                                duration: const Duration(
                                                  milliseconds: 500,
                                                ),
                                                width: 50,
                                                margin: const EdgeInsets.all(4),
                                                decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  color: Colorconstands.darkGray
                                                      .withOpacity(0.5),
                                                ),
                                                child: const Icon(
                                                  Icons.close,
                                                  size: 20,
                                                  color: Colorconstands.gray,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                Container(
                                  margin: const EdgeInsets.only(
                                      bottom: 25,
                                      left: 12.0,
                                      right: 12.0,
                                      top: 12.0),
                                  child: Form(
                                    key: _formKey,
                                    child: TextFormField(
                                      onChanged: ((value) {
                                        setState(() {
                                          _textcomment.text;
                                        });
                                      }),
                                      keyboardType: TextInputType.multiline,
                                      maxLines: 5,
                                      minLines: 1,
                                      controller: _textcomment,
                                      focusNode: _titleFocus,
                                      autofocus: false,
                                      decoration: InputDecoration(
                                        hintText: "Comment....",
                                        hintStyle: const TextStyle(),
                                        fillColor: Colors.white,
                                        filled: true,
                                        contentPadding:
                                            const EdgeInsets.symmetric(
                                                vertical: 0, horizontal: 10.0),
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(12.0),
                                          borderSide: const BorderSide(),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(12.0),
                                          borderSide: const BorderSide(
                                            color: Colorconstands.gray,
                                          ),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(12.0),
                                          borderSide: BorderSide(
                                            color: const Color(0xFFDADADA)
                                                .withOpacity(.7),
                                          ),
                                        ),
                                        suffixIcon: _textcomment.text == "" ||
                                                _textcomment.text == null
                                            ? Container(
                                                width: 45,
                                              )
                                            : InkWell(
                                                onTap: () {
                                                  BlocProvider.of<HomeworkBloc>(
                                                      context)
                                                    ..add(
                                                      AddNewReplyCommentEvent(
                                                        commentText:
                                                            _textcomment.text,
                                                        parentId: reply == false
                                                            ? ""
                                                            : parentID,
                                                        replyId: idComment == ''
                                                            ? widget.idPost
                                                            : idComment,
                                                      ),
                                                    )
                                                    ..add(
                                                      GetHomeWorkTeachingEvent(
                                                        homeWorkId: widget
                                                            .idPost
                                                            .toString(),
                                                      ),
                                                    );
                                                  Future.delayed(
                                                      const Duration(
                                                          seconds: 2), () {
                                                    setState(() {
                                                      reply = false;
                                                    });
                                                  });

                                                  FocusScope.of(context)
                                                      .unfocus();
                                                  _textcomment.clear();
                                                },
                                                child: Container(
                                                  margin:
                                                      const EdgeInsets.all(4),
                                                  decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: Colors.grey.shade100,
                                                  ),
                                                  child: const Icon(
                                                    Icons.send,
                                                    size: 20,
                                                    color: Colorconstands
                                                        .primaryColor,
                                                  ),
                                                ),
                                              ),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
