import 'package:camis_application_flutter/app/modules/homework_screen/widgets/tab_forum.dart';
import 'package:flutter/material.dart';
import 'tab_homework.dart';

class CustomTabbarViewWidget extends StatelessWidget {
  const CustomTabbarViewWidget({
    Key? key,
    required this.width,
    required this.height,
    required TabController tabController,
    required this.index,
  })  : _tabController = tabController,
        super(key: key);

  final int index;
  final double width;
  final double height;
  final TabController _tabController;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 1.0),
      width: width,
      height: height / 1.2,
      alignment: Alignment.topCenter,
      child: TabBarView(
        controller: _tabController,
        children: [
          TabHomeWork(
            width: width,
          
          ),
          TabForumWork(width: width),
        ],
      ),
    );
  }
}
