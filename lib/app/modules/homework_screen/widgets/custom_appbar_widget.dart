import 'package:flutter/material.dart';

import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class CustomAppbarWidget extends StatelessWidget {
  const CustomAppbarWidget({
    Key? key,
    required this.width,
    required this.avatar,
    required this.title,
  }) : super(key: key);

  final double width;
  final bool avatar;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            width: width / 1.5,
            height: 110,
            child: Row(
              children: [
                Container(
                  margin: const EdgeInsets.only(left: 8),
                  child: IconButton(
                    icon: const Icon(
                      Icons.arrow_back_ios_new_outlined,
                      size: 25,
                      color: Colorconstands.white,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
                const SizedBox(
                  width: 4.0,
                ),
                Expanded(
                  child: Text(
                    title,
                    style: ThemeConstands.texttheme.headline5!.copyWith(
                      color: Colors.white,
                      fontWeight: FontWeight.w400,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
