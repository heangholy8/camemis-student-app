import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

import '../../../core/themes/themes.dart';

class PdfViewerWidget extends StatefulWidget {
  final String urlPDF;
  final String fileName;
  const PdfViewerWidget(
      {Key? key, required this.urlPDF, required this.fileName})
      : super(key: key);

  @override
  State<PdfViewerWidget> createState() => _PdfViewerWidgetState();
}

class _PdfViewerWidgetState extends State<PdfViewerWidget> {
  final GlobalKey<SfPdfViewerState> _pdfViewStateKey = GlobalKey();
  late PdfViewerController controller;
  OverlayEntry? _overlayEntry;
  @override
  void initState() {
    super.initState();
    controller = PdfViewerController();
    controller.addListener(({property}) {
      if (controller.scrollOffset.dy > 0) {}
    });
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: SizedBox(
        width: width,
        height: height,
        child: Padding(
          padding: const EdgeInsets.only(top: 26.0),
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.only(right: 0.0),
                height: 120,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: width / 1.5,
                      height: 60,
                      child: Row(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(left: 8),
                            child: IconButton(
                              icon: const Icon(
                                Icons.arrow_back_ios_new_outlined,
                                size: 25,
                                color: Colorconstands.white,
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                          const SizedBox(
                            width: 4.0,
                          ),
                          Expanded(
                            child: Text(
                              widget.fileName,
                              style:
                                  ThemeConstands.texttheme.headline5!.copyWith(
                                color: Colors.white,
                                fontWeight: FontWeight.w400,
                              ),
                              textAlign: TextAlign.left,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 15),
                      child: Row(
                        children: [
                          IconButton(
                            onPressed: () {
                              _pdfViewStateKey.currentState!.openBookmarkView();
                            },
                            icon: const Icon(
                              Icons.bookmark_add_outlined,
                              color: Colors.white,
                            ),
                          ),
                          IconButton(
                            onPressed: () {
                              controller.nextPage();
                            },
                            icon: const Icon(
                              Icons.more_vert_outlined,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: SfPdfViewer.network(
                  widget.urlPDF,
                  controller: controller,
                  key: _pdfViewStateKey,
                  enableDocumentLinkAnnotation: true,
                  enableTextSelection: true,
                  onPageChanged: (value) {},
                  onTextSelectionChanged:
                      (PdfTextSelectionChangedDetails details) {
                    if (details.selectedText == null && _overlayEntry != null) {
                      _overlayEntry?.remove();

                      _overlayEntry = null;
                    } else if (details.selectedText != null &&
                        _overlayEntry == null) {
                      _showContextMenu(context, details);
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _showContextMenu(
      BuildContext context, PdfTextSelectionChangedDetails details) {
    final OverlayState? _overlayState = Overlay.of(context);
    _overlayEntry = OverlayEntry(
      builder: (context) => Positioned(
        top: details.globalSelectedRegion!.center.dy - 55,
        left: details.globalSelectedRegion?.bottomLeft.dx,
        child: ElevatedButton(
          child: const Text(
            'Copy',
            style: TextStyle(fontSize: 17),
          ),
          onPressed: () {
            Clipboard.setData(
              ClipboardData(
                text: details.selectedText,
              ),
            );
            controller.clearSelection();
          },
          // color: Colors.white,
          // elevation: 10,
        ),
      ),
    );
    _overlayState?.insert(_overlayEntry!);
  }

}
