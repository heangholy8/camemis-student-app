import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class YoutubePlayerWidget extends StatefulWidget {
 
  const YoutubePlayerWidget({Key? key, })
      : super(key: key);

  @override
  State<YoutubePlayerWidget> createState() => _YoutubePlayerWidgetState();
}

class _YoutubePlayerWidgetState extends State<YoutubePlayerWidget> {
 late YoutubePlayerController _controller;

 @override
  void initState() {
    _controller = YoutubePlayerController(
      initialVideoId: 'v0RWej7Sqg4',
      params: const YoutubePlayerParams(
        playlist: [
          'MJF6_EqWggQ', //Default playlist
          'K18cpp_-gP8',
          'iLnmTe5Q2Qw',
          '_WoCV4c6XOE',
          'KmzdUe0RSJo',
          '6jZDSSZZxjQ',
          'p2lYr3vM_1w',
          '7QUtEmBT_-w',
          '34_PXCzGw1M',
        ],
        startAt:  Duration(minutes: 0, seconds: 00),
        showControls: true,
        showFullscreenButton: true,
        desktopMode: true,
        privacyEnhanced: true,
        useHybridComposition: true,
      ),
    );
    _controller.onEnterFullscreen = () {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight,
      ]);
      log('Entered Fullscreen');
    };
    _controller.onExitFullscreen = () {
      log('Exited Fullscreen');
    };
    super.initState();
  }
 
  @override
  Widget build(BuildContext context) {
    return YoutubePlayerControllerProvider(
      controller: _controller,
      child: LayoutBuilder(
        builder: (context, constraints) {
          return YoutubePlayerIFrame(
            aspectRatio: 16 / 9,
            controller: _controller,
          );
        },
      ),
    );
  }
}
