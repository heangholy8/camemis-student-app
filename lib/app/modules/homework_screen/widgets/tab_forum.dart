import 'package:camis_application_flutter/model/teaching_model/homework_model.dart'
    as home;
import 'package:comment_tree/comment_tree.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../../service/apis/time_line_api/post_react.dart';
import '../../../../widgets/shimmer_style.dart';
import '../../../routes/e.route.dart';
import '../../teaching_learning_screen/widgets/custom_textfield_sender_widget.dart';
import '../../time_line/widget/comment_widget.dart';
import '../bloc/homework_bloc.dart';

class TabForumWork extends StatefulWidget {
  final double width;
  const TabForumWork({
    Key? key,
    required this.width,
  }) : super(key: key);

  @override
  State<TabForumWork> createState() => _TabForumWorkState();
}

class _TabForumWorkState extends State<TabForumWork> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final FocusNode _titleFocus = FocusNode();
  final TextEditingController _textcomment = TextEditingController();
  bool isReply = false;
  List<home.Forum>? _listComment;

  String replyName = "";
  String replyText = "";
  bool reply = false;
  int? idPost;
  int? idReplyComment;
  String? classID;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Container(
      color: Colorconstands.white,
      child: Column(
        children: [
          BlocBuilder<HomeworkBloc, HomeworkState>(
              builder: (context, state) {
            if (state is HomeWorkLoadingState) {
              return Expanded(
                child: ListView.builder(
                  shrinkWrap: true,
                  physics: const ScrollPhysics(),
                  itemBuilder: (BuildContext contex, index) {
                    return const SizedBox(
                      child: ShimmerComment(),
                    );
                  },
                  itemCount: 2,
                ),
              );
            }
            if (state is HomeWorkLoadedState) {
              idPost = state.detailTeachingModel.data!.id!.toInt();
              var data = state.detailTeachingModel.data!.forums;
              return Container(
                margin: const EdgeInsets.symmetric(
                    vertical: 12.0, horizontal: 0.0),
                child: ListView.builder(
                  shrinkWrap: true,
                  physics: const ScrollPhysics(),
                  itemBuilder: (BuildContext contex, index) {
                    int selecticonreact = 5;
                    return Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        WidgetComment(
                          hideReact: true,
                          onReact: (values, isChecked) {},
                          marginprofileLeft: 12.0,
                          isCheckroot: true,
                          lenghtreply: data![index].children!.length,
                          margincommentbetweenprofile: 65.0,
                          namecomment: translate == "en"
                              ? data[index].userProfile!.nameEn == " " ||
                                      data[index].userProfile!.nameEn == ""
                                  ? data[index].userProfile!.name.toString()
                                  : data[index]
                                      .userProfile!
                                      .nameEn
                                      .toString()
                              : data[index].userProfile!.name.toString(),
                          profilecomment: data[index]
                              .userProfile!
                              .profileMedia!
                              .fileShow
                              .toString(),
                          textcomment: data[index].comment.toString(),
                          //------------------------- Icon React--------------------------
                          iconReact: "assets/icons/like_icon.png",
                          //-------------------------End Icon React--------------------------
                          textReact: "",
                          profilehight: 45,
                          profilewidth: 45,
                          style: ThemeConstands.texttheme.subtitle1!
                              .copyWith(
                                  color: Colorconstands.black,
                                  fontWeight: FontWeight.w500),
                          styletextpost:
                              ThemeConstands.texttheme.subtitle1!.copyWith(
                            color: Colorconstands.darkGray,
                          ),
                          //------------------------- color Label React--------------------------
                          //colorlabelreact: Colorconstands.primaryColor,
                          colorlabelreact: Colors.yellow,
                          //-------------------------End color Label React--------------------------
                          selectedReaction: selecticonreact,
                          reply: () {
                            setState(() {
                              reply = true;
                              replyName = translate == "en"
                                  ? data[index].userProfile!.nameEn ==
                                              " " ||
                                          data[index].userProfile!.nameEn ==
                                              ""
                                      ? data[index]
                                          .userProfile!
                                          .name
                                          .toString()
                                      : data[index]
                                          .userProfile!
                                          .nameEn
                                          .toString()
                                  : data[index]
                                      .userProfile!
                                      .name
                                      .toString();
                              replyText = data[index].comment.toString();
                              idReplyComment = data[index].id;
                            });
                          },
                        ),
                        data[index].children!.isEmpty
                            ? Container()
                            : Container(
                                margin: const EdgeInsets.only(
                                    left: 0, right: 0.0),
                                child: Stack(
                                  children: [
                                    ListView.builder(
                                        padding: const EdgeInsets.all(0),
                                        physics: const ScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount:
                                            data[index].children!.length - 1,
                                        itemBuilder: (BuildContext contex,
                                            subIndex) {
                                          return Stack(
                                            children: [
                                              Positioned(
                                                top: 0,
                                                left: 35,
                                                bottom: 0,
                                                child: Container(
                                                  color: Colorconstands.gray,
                                                  width: 1,
                                                ),
                                              ),
                                              Container(
                                                margin:
                                                    const EdgeInsets.only(
                                                        left: 65),
                                                child: WidgetComment(
                                                  hideReact: true,
                                                  marginprofileLeft: 5.0,
                                                  isCheckroot: false,
                                                  lenghtreply: data.length,
                                                  margincommentbetweenprofile:
                                                      50.0,
                                                  namecomment: translate ==
                                                            "en"
                                                        ? data[index]
                                                                        .children![
                                                                            subIndex]
                                                                        .userProfile!
                                                                        .nameEn ==
                                                                    " " ||
                                                                data[index]
                                                                        .children![
                                                                            subIndex]
                                                                        .userProfile!
                                                                        .nameEn ==
                                                                    ""
                                                            ? data[index]
                                                                .children![
                                                                    subIndex]
                                                                .userProfile!
                                                                .name
                                                                .toString()
                                                            : data[index]
                                                                .children![
                                                                    subIndex]
                                                                .userProfile!
                                                                .nameEn
                                                                .toString()
                                                        : data[index]
                                                            .children![
                                                                subIndex]
                                                            .userProfile!
                                                            .name
                                                            .toString(),
                                                  profilecomment: data[
                                                          index]
                                                      .children![subIndex]
                                                      .userProfile!
                                                      .profileMedia!
                                                      .fileShow
                                                      .toString(),
                                                  textcomment: data[index]
                                                      .children![subIndex]
                                                      .comment
                                                      .toString(),
                                                  //------------------------- Icon React--------------------------
                                                  iconReact:
                                                      "assets/icons/like_icon.png",
                                                  //-------------------------End Icon React--------------------------
                                                  textReact: "",
                                                  profilehight: 30,
                                                  profilewidth: 30,
                                                  style: ThemeConstands
                                                      .texttheme.subtitle2!
                                                      .copyWith(
                                                          color:
                                                              Colorconstands
                                                                  .black,
                                                          fontWeight:
                                                              FontWeight
                                                                  .w500),
                                                  styletextpost:
                                                      ThemeConstands
                                                          .texttheme
                                                          .subtitle2!
                                                          .copyWith(
                                                    color: Colorconstands
                                                        .darkGray,
                                                  ),
                                                  //------------------------- color Label React--------------------------
                                                  //colorlabelreact: Colorconstands.primaryColor,
                                                  colorlabelreact:
                                                      Colors.yellow,
                                                  //-------------------------End color Label React--------------------------
                                                  selectedReaction:
                                                      selecticonreact,
                                                  reply: () {},
                                                  onReact: (String? values,
                                                      bool isChecked) {},
                                                ),
                                              ),
                                            ],
                                          );
                                        }),
                                    Container(
                                      margin:
                                          const EdgeInsets.only(left: 0),
                                      child: ListView.builder(
                                          padding: const EdgeInsets.all(0),
                                          physics: const ScrollPhysics(),
                                          shrinkWrap: true,
                                          itemCount:data[index].children!.length,
                                          itemBuilder: (BuildContext contex,
                                              subIndex) {
                                            return Stack(
                                              children: [
                                                Positioned(
                                                  top: 0,
                                                  left: 35,
                                                  right: 0,
                                                  child: Container(
                                                    height: 25,
                                                    decoration: BoxDecoration(
                                                        border: Border.all(
                                                            color:
                                                                Colorconstands
                                                                    .gray,
                                                            width: 1.0,
                                                            style:
                                                                BorderStyle
                                                                    .solid),
                                                        borderRadius:
                                                            const BorderRadius
                                                                    .only(
                                                                bottomLeft:
                                                                    Radius.circular(
                                                                        16))),
                                                  ),
                                                ),
                                                Positioned(
                                                  top: 0,
                                                  left: 36,
                                                  right: 0,
                                                  child: Container(
                                                    height: 1,
                                                    color:
                                                        Colorconstands.white,
                                                  ),
                                                ),
                                                Container(
                                                  margin:
                                                      const EdgeInsets.only(
                                                          left: 65),
                                                  color:
                                                      Colorconstands.white,
                                                  child: WidgetComment(
                                                    hideReact: true,
                                                    onReact: ((values,
                                                        isChecked) {}),
                                                    marginprofileLeft: 5.0,
                                                    isCheckroot: false,
                                                    lenghtreply:
                                                        data.length,
                                                    margincommentbetweenprofile:
                                                        50.0,
                                                    namecomment: translate ==
                                                            "en"
                                                        ? data[index]
                                                                        .children![
                                                                            subIndex]
                                                                        .userProfile!
                                                                        .nameEn ==
                                                                    " " ||
                                                                data[index]
                                                                        .children![
                                                                            subIndex]
                                                                        .userProfile!
                                                                        .nameEn ==
                                                                    ""
                                                            ? data[index]
                                                                .children![
                                                                    subIndex]
                                                                .userProfile!
                                                                .name
                                                                .toString()
                                                            : data[index]
                                                                .children![
                                                                    subIndex]
                                                                .userProfile!
                                                                .nameEn
                                                                .toString()
                                                        : data[index]
                                                            .children![
                                                                subIndex]
                                                            .userProfile!
                                                            .name
                                                            .toString(),
                                                    profilecomment: data[
                                                            index]
                                                        .children![subIndex]
                                                        .userProfile!
                                                        .profileMedia!
                                                        .fileShow
                                                        .toString(),
                                                    textcomment: data[index]
                                                        .children![subIndex]
                                                        .comment
                                                        .toString(),
                                                    //------------------------- Icon React--------------------------
                                                    iconReact:
                                                        "assets/icons/like_icon.png",
                                                    //-------------------------End Icon React--------------------------
                                                    textReact: "",
                                                    profilehight: 30,
                                                    profilewidth: 30,
                                                    style: ThemeConstands
                                                        .texttheme
                                                        .subtitle2!
                                                        .copyWith(
                                                            color:
                                                                Colorconstands
                                                                    .black,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w500),
                                                    styletextpost:
                                                        ThemeConstands
                                                            .texttheme
                                                            .subtitle2!
                                                            .copyWith(
                                                      color: Colorconstands
                                                          .darkGray,
                                                    ),
                                                    //------------------------- color Label React--------------------------
                                                    //colorlabelreact: Colorconstands.primaryColor,
                                                    colorlabelreact:
                                                        Colors.yellow,
                                                    //-------------------------End color Label React--------------------------
                                                    selectedReaction:
                                                        selecticonreact,
                                                    reply: () {
                                                      setState(() {
                                                        reply = true;
                                                        replyName = translate ==
                                                                "en"
                                                            ? data[index].children![subIndex].userProfile!.nameEn ==
                                                                        " " ||
                                                                    data[index].children![subIndex].userProfile!.nameEn ==
                                                                        ""
                                                                ? data[index]
                                                                    .children![
                                                                        subIndex]
                                                                    .userProfile!
                                                                    .name
                                                                    .toString()
                                                                : data[index]
                                                                    .children![
                                                                        subIndex]
                                                                    .userProfile!
                                                                    .nameEn
                                                                    .toString()
                                                            : data[index]
                                                                .children![
                                                                    subIndex]
                                                                .userProfile!
                                                                .name
                                                                .toString();
                                                        replyText = data[
                                                                index]
                                                            .children![
                                                                subIndex]
                                                            .comment
                                                            .toString();
                                                        idReplyComment =
                                                            data[index].id;
                                                      });
                                                    },
                                                  ),
                                                ),
                                              ],
                                            );
                                          }),
                                    ),
                                  ],
                                ),
                              ),
                      ],
                    );
                  },
                  itemCount: data!.length,
                ),
              );
            } else {
              return Container();
            }
          }),
          Container(
            color: Colorconstands.gray.withOpacity(0.4),
            child: Column(
              children: [
                const Divider(
                  height: 1,
                  thickness: 1,
                  color: Colorconstands.gray,
                ),
                reply == false
                    ? Container()
                    : Container(
                        margin: const EdgeInsets.only(
                            left: 12.0, right: 12.0, top: 12.0),
                        child: Row(
                          children: [
                            Expanded(
                                child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Replying to " + replyName,
                                  style: ThemeConstands.texttheme.subtitle2!
                                      .copyWith(
                                          color: Colorconstands.black,
                                          fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  replyText,
                                  style: ThemeConstands.texttheme.subtitle2!
                                      .copyWith(
                                    color: Colorconstands.black,
                                  ),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ],
                            )),
                            InkWell(
                              onTap: () {
                                setState(() {
                                  reply = false;
                                });
                              },
                              child: Container(
                                width: 50,
                                margin: const EdgeInsets.all(4),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color:
                                      Colorconstands.darkGray.withOpacity(0.5),
                                ),
                                child: const Icon(
                                  Icons.close,
                                  size: 20,
                                  color: Colorconstands.gray,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                Container(
                    margin: const EdgeInsets.only(
                        bottom: 25, left: 12.0, right: 12.0, top: 12.0),
                    child: Form(
                      key: _formKey,
                      child: TextFormField(
                        onChanged: ((value) {
                          setState(() {
                            _textcomment.text;
                          });
                        }),
                        keyboardType: TextInputType.multiline,
                        maxLines: 5,
                        minLines: 1,
                        controller: _textcomment,
                        focusNode: _titleFocus,
                        autofocus: false,
                        decoration: InputDecoration(
                          hintText: "Comment....",
                          hintStyle: const TextStyle(),
                          fillColor: Colors.white,
                          filled: true,
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 0, horizontal: 10.0),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12.0),
                            borderSide: const BorderSide(),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12.0),
                            borderSide: const BorderSide(
                              color: Colorconstands.gray,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12.0),
                            borderSide: BorderSide(
                              color: const Color(0xFFDADADA).withOpacity(.7),
                            ),
                          ),
                          suffixIcon: _textcomment.text == "" ||
                                  _textcomment.text == null
                              ? Container(
                                  width: 45,
                                )
                              : InkWell(
                                  onTap: () {
                                    setState(() {
                                      //=======================Event post React=======================

                                      //======================= End Event post React=======================
                                    });
                                    FocusScope.of(context).unfocus();
                                    _textcomment.text = "";
                                  },
                                  child: Container(
                                    margin: const EdgeInsets.all(4),
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.grey.shade100,
                                    ),
                                    child: const Icon(
                                      Icons.send,
                                      size: 20,
                                      color: Colorconstands.primaryColor,
                                    ),
                                  ),
                                ),
                        ),
                      ),
                    ))
              ],
            ),
          ),
        ],
      ),
    );

    // return BlocBuilder<HomeworkBloc, HomeworkState>(
    //   builder: (context, state) {
    //     if (state is HomeWorkLoadingState) {
    //       return const Center(
    //         child: CircularProgressIndicator(),
    //       );
    //     }
    //     if (state is HomeWorkLoadedState) {
    //       var forumm = state.detailTeachingModel.data!.forums;
    //       return Stack(
    //         children: [
    //           InkWell(
    //             onTap: () {
    //               FocusScope.of(context).unfocus();
    //             },
    //             child: Container(
    //               color: Colors.white,
    //               padding:
    //                   const EdgeInsets.only(bottom: 50, left: 16, right: 16),
    //               child: ListView.builder(
    //                 shrinkWrap: true,
    //                 physics: const NeverScrollableScrollPhysics(),
    //                 itemCount: forumm?.length,
    //                 itemBuilder: (context, index) {
    //                   _listComment = forumm!;
    //                   return CommentTreeWidget<Comment, Comment>(
    //                     Comment(
    //                       avatar:
    //                           '${_listComment?[index].userProfile?.profileMedia?.fileThumbnail}',
    //                       userName: '${_listComment?[index].userProfile?.name}',
    //                       content: "${_listComment?[index].comment}",
    //                     ),
    //                     List.generate(
    //                       _listComment![index].children!.length,
    //                       (subindex) => Comment(
    //                         avatar:
    //                             '${_listComment?[index].children![subindex].userProfile?.profileMedia?.fileThumbnail}',
    //                         userName:
    //                             '${_listComment?[index].children![subindex].userProfile?.name}',
    //                         content:
    //                             '${_listComment?[index].children![subindex].comment}',
    //                       ),
    //                     ),

    //                     treeThemeData: TreeThemeData(
    //                         lineColor: _listComment![index].children!.isEmpty
    //                             ? Colors.white
    //                             : Colors.grey.shade100,
    //                         lineWidth: 1.5),
    //                     avatarRoot: (context, data) => PreferredSize(
    //                       child: InkWell(
    //                         onTap: () {
    //                           print("Main Profile Clicked");
    //                         },
    //                         child: CircleAvatar(
    //                           radius: 24,
    //                           backgroundImage: NetworkImage(
    //                               "${_listComment?[index].userProfile!.profileMedia!.fileThumbnail}"),
    //                         ),
    //                       ),
    //                       preferredSize: const Size.fromRadius(18),
    //                     ),
    //                     avatarChild: (context, data) => PreferredSize(
    //                       child: InkWell(
    //                         onTap: () {
    //                           print("Child Profile Clicked");
    //                         },
    //                         child: CircleAvatar(
    //                           radius: 18,
    //                           backgroundImage: NetworkImage('${data.avatar}'),
    //                         ),
    //                       ),
    //                       preferredSize: const Size.fromRadius(12),
    //                     ),
    //                     // Reply Container Box
    //                     contentChild: (context, data) {
    //                       return Column(
    //                         crossAxisAlignment: CrossAxisAlignment.start,
    //                         children: [
    //                           Container(
    //                             padding: const EdgeInsets.symmetric(
    //                                 vertical: 14, horizontal: 14),
    //                             decoration: BoxDecoration(
    //                                 color: Colors.grey[100],
    //                                 borderRadius: BorderRadius.circular(12)),
    //                             child: Column(
    //                               crossAxisAlignment: CrossAxisAlignment.start,
    //                               children: [
    //                                 Text(
    //                                   '${data.userName}',
    //                                   style: ThemeConstands.texttheme.subtitle1!
    //                                       .copyWith(
    //                                     fontWeight: FontWeight.w600,
    //                                     color: Colors.black,
    //                                   ),
    //                                 ),
    //                                 const SizedBox(
    //                                   height: 8,
    //                                 ),
    //                                 Text(
    //                                   ' ${data.content}',
    //                                   style: ThemeConstands.texttheme.bodyText1
    //                                       ?.copyWith(
    //                                     fontWeight: FontWeight.w500,
    //                                     color: Colorconstands.darkGray,
    //                                   ),
    //                                 ),
    //                               ],
    //                             ),
    //                           ),
    //                           DefaultTextStyle(
    //                             style: Theme.of(context)
    //                                 .textTheme
    //                                 .caption!
    //                                 .copyWith(
    //                                     color: Colors.grey[700],
    //                                     fontWeight: FontWeight.bold),
    //                             child: Padding(
    //                               padding: const EdgeInsets.only(top: 16),
    //                               child: Row(
    //                                 children: [
    //                                   const SizedBox(
    //                                     width: 8,
    //                                   ),
    //                                   InkWell(
    //                                       onTap: () {
    //                                         print("Likess");
    //                                         print(data.content);
    //                                       },
    //                                       child: Row(
    //                                         mainAxisAlignment:
    //                                             MainAxisAlignment.center,
    //                                         crossAxisAlignment:
    //                                             CrossAxisAlignment.center,
    //                                         children: [
    //                                           SvgPicture.asset(
    //                                             "assets/icons/fi_like_icon.svg",
    //                                             width: 12,
    //                                           ),
    //                                           const SizedBox(
    //                                             width: 6,
    //                                           ),
    //                                           Text(
    //                                             'Like',
    //                                             style: ThemeConstands
    //                                                 .texttheme.bodyText2!
    //                                                 .copyWith(
    //                                               fontWeight: FontWeight.w600,
    //                                               color: Colorconstands.darkGray,
    //                                             ),
    //                                           ),
    //                                         ],
    //                                       )),
    //                                   const SizedBox(
    //                                     width: 24,
    //                                   ),
    //                                   InkWell(
    //                                     onTap: () {
    //                                       print("object");
    //                                       setState(() {
    //                                         isReply == true;
    //                                       });
    //                                       print("a" +
    //                                           forumm[index]
    //                                               .children![2]
    //                                               .id
    //                                               .toString());
    //                                       print("a" + data.content.toString());

    //                                       // for (int i = 0;
    //                                       //         forumm[index]
    //                                       //             .children!
    //                                       //             .length;
    //                                       //     i++) {
    //                                       //   print(
    //                                       //       forumm[index].children![0].id);
    //                                       // }

    //                                       // else {
    //                                       //   setState(() {
    //                                       //     isReply == true;
    //                                       //   });
    //                                       // }
    //                                     },
    //                                     child: Row(
    //                                       mainAxisAlignment:
    //                                           MainAxisAlignment.center,
    //                                       crossAxisAlignment:
    //                                           CrossAxisAlignment.center,
    //                                       children: [
    //                                         SvgPicture.asset(
    //                                           "assets/icons/fi_reply_icon.svg",
    //                                           width: 12,
    //                                         ),
    //                                         const SizedBox(
    //                                           width: 6,
    //                                         ),
    //                                         Text(
    //                                           'Reply',
    //                                           style: ThemeConstands
    //                                               .texttheme.bodyText2!
    //                                               .copyWith(
    //                                             fontWeight: FontWeight.w600,
    //                                             color: Colorconstands.darkGray,
    //                                           ),
    //                                         ),
    //                                       ],
    //                                     ),
    //                                   ),
    //                                 ],
    //                               ),
    //                             ),
    //                           )
    //                         ],
    //                       );
    //                     },
    //                     contentRoot: (context, data) {
    //                       return Column(
    //                         crossAxisAlignment: CrossAxisAlignment.start,
    //                         children: [
    //                           Container(
    //                             padding: const EdgeInsets.symmetric(
    //                                 vertical: 8, horizontal: 8),
    //                             decoration: BoxDecoration(
    //                               color: Colors.grey[100],
    //                               borderRadius: BorderRadius.circular(12),
    //                             ),
    //                             child: Column(
    //                               crossAxisAlignment: CrossAxisAlignment.start,
    //                               children: [
    //                                 Text(
    //                                   '${data.userName}',
    //                                   style: ThemeConstands.texttheme.subtitle1!
    //                                       .copyWith(
    //                                     fontWeight: FontWeight.w600,
    //                                     color: Colors.black,
    //                                   ),
    //                                 ),
    //                                 const SizedBox(
    //                                   height: 4,
    //                                 ),
    //                                 Text(
    //                                   '${data.content}',
    //                                   style: ThemeConstands.texttheme.bodyText1
    //                                       ?.copyWith(
    //                                     fontWeight: FontWeight.w500,
    //                                     color: Colorconstands.darkGray,
    //                                   ),
    //                                 ),
    //                               ],
    //                             ),
    //                           ),

    //                           /// LIKE AND REPLY BUTTON
    //                           ///
    //                           DefaultTextStyle(
    //                             style: Theme.of(context)
    //                                 .textTheme
    //                                 .caption!
    //                                 .copyWith(
    //                                     color: Colors.grey[700],
    //                                     fontWeight: FontWeight.bold),
    //                             child: Padding(
    //                               padding: const EdgeInsets.only(top: 6),
    //                               child: Row(
    //                                 children: [
    //                                   const SizedBox(
    //                                     width: 8,
    //                                   ),
    //                                   InkWell(
    //                                     onTap: () {
    //                                       print("Like");
    //                                       // setState(() {
    //                                       //   _isLike = !_isLike;
    //                                       // });
    //                                     },
    //                                     child: Row(
    //                                       mainAxisAlignment:
    //                                           MainAxisAlignment.center,
    //                                       crossAxisAlignment:
    //                                           CrossAxisAlignment.center,
    //                                       children: [
    //                                         SvgPicture.asset(
    //                                           "assets/icons/fi_like_icon.svg",
    //                                           width: 12,
    //                                           color:
    //                                               //  !_isLike
    //                                               //     ? Colorconstands.darkGray
    //                                               //     :
    //                                               Colorconstands.primaryColor,
    //                                         ),
    //                                         const SizedBox(
    //                                           width: 6,
    //                                         ),
    //                                         Text(
    //                                           'Like',
    //                                           style: ThemeConstands
    //                                               .texttheme.bodyText2!
    //                                               .copyWith(
    //                                             fontWeight: FontWeight.w600,
    //                                             color:
    //                                                 //  !_isLike
    //                                                 //     ? Colorconstands.darkGray
    //                                                 //     :
    //                                                 Colorconstands.primaryColor,
    //                                           ),
    //                                         ),
    //                                       ],
    //                                     ),
    //                                   ),
    //                                   const SizedBox(
    //                                     width: 24,
    //                                   ),
    //                                   InkWell(
    //                                     onTap: () {
    //                                       print("Commentasda");
    //                                       setState(() {
    //                                         isReply == true;
    //                                       });
    //                                     },
    //                                     child: Row(
    //                                       mainAxisAlignment:
    //                                           MainAxisAlignment.center,
    //                                       crossAxisAlignment:
    //                                           CrossAxisAlignment.center,
    //                                       children: [
    //                                         SvgPicture.asset(
    //                                           "assets/icons/fi_reply_icon.svg",
    //                                           width: 12,
    //                                         ),
    //                                         const SizedBox(
    //                                           width: 6,
    //                                         ),
    //                                         Text(
    //                                           'Reply',
    //                                           style: ThemeConstands
    //                                               .texttheme.bodyText2!
    //                                               .copyWith(
    //                                             fontWeight: FontWeight.w600,
    //                                             color: Colorconstands.darkGray,
    //                                           ),
    //                                         ),
    //                                       ],
    //                                     ),
    //                                   ),
    //                                 ],
    //                               ),
    //                             ),
    //                           ),
    //                           const SizedBox(
    //                             height: 18,
    //                           ),
    //                         ],
    //                       );
    //                     },
    //                   );
    //                 },
    //               ),
    //             ),
    //           ),
    //           AnimatedPositioned(
    //             duration: const Duration(milliseconds: 200),
    //             bottom: 0,
    //             left: 0,
    //             right: 0,
    //             child: AnimatedContainer(
    //               duration: const Duration(milliseconds: 200),
    //               alignment: Alignment.topCenter,
    //               width: MediaQuery.of(context).size.width,
    //               height: 100,
    //               color: Colors.grey.shade100,
    //               child: CustomTextFieldSenderWidget(
    //                 controller: textEditController,
    //                 isReply: isReply,
    //                 mentionName: textEditController.text,
    //                 suffixIcon: InkWell(
    //                   onTap: () {
    //                     textEditController.clear();
    //                     FocusScope.of(context).unfocus();
    //                   },
    //                   child: Container(
    //                     margin: const EdgeInsets.all(4),
    //                     decoration: BoxDecoration(
    //                       shape: BoxShape.circle,
    //                       color: Colors.grey.shade100,
    //                     ),
    //                     child: const Icon(
    //                       Icons.send,
    //                       size: 20,
    //                     ),
    //                   ),
    //                 ),
    //               ),
    //             ),
    //           ),
    //         ],
    //       );
    //     }
    //     return Container();
    //   },
    // );
  }
}
