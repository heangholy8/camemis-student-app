import 'package:flutter/material.dart';
import '../../../core/themes/color_app.dart';

class CustomTabbarWidget extends StatelessWidget {
  const CustomTabbarWidget({
    Key? key,
    required this.width,
    required TabController tabController,
    required this.tabs,
  })  : _tabController = tabController,
        super(key: key);

  final double width;
  final TabController _tabController;
  final List<Tab> tabs;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,

      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(
        vertical: 8,
        horizontal: 20,
      ),
      decoration: const BoxDecoration(
        color: Color(0xFFF1F9FF),
        
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 2),
            blurRadius: 4,
            color: Color(0xFFF4F4F4),
          ),
        ],
      ),
      child: TabBar(
        indicator: BoxDecoration(
          color: Colorconstands.primaryColor,
          borderRadius: BorderRadius.circular(8.0),
        ),
        labelColor: Colors.white,
        unselectedLabelColor: const Color(0xFF9CB6DB),
        isScrollable: false,
        controller: _tabController,
        labelStyle: const TextStyle(
          fontSize: 18,
          fontFamily: "battambang",
          fontWeight: FontWeight.w600,
        ),
        tabs: tabs,
        onTap: (value) {
          print(_tabController.index);
        },
      ),
    );
  }
}
