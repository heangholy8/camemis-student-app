import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import '../../../core/themes/themes.dart';

class CustomViewGalleryWidget extends StatefulWidget {
  final GestureTapCallback onTap;
  const CustomViewGalleryWidget({
    Key? key,
    required this.onTap,
  }) : super(key: key);

  @override
  State<CustomViewGalleryWidget> createState() =>
      _CustomViewGalleryWidgetState();
}

class _CustomViewGalleryWidgetState extends State<CustomViewGalleryWidget>
    with TickerProviderStateMixin {
  late PageController pageController;
  int subindex = 0;
  @override
  void initState() {
    pageController = PageController();
    pageController.addListener(() {});

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<String> listImage = [
      "assets/images/answer_image.png",
      "assets/images/answer2_image.png",
      "assets/images/answer2_image.png",
      "assets/images/answer_image.png"
    ];
    double width = MediaQuery.of(context).size.width;
    return Positioned.fill(
      bottom: 0,
      top: 0,
      child: Container(
        color: Colors.black.withOpacity(1.0),
        padding: const EdgeInsets.only(top: 36),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: width,
              height: 60,
              // color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  IconButton(
                    onPressed: widget.onTap,
                    icon: const Icon(
                      Icons.cancel,
                      color: Colors.white,
                    ),
                  ),
                  Text(
                    "${subindex + 1} of ${listImage.length}",
                    style: ThemeConstands.texttheme.subtitle1!.copyWith(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.more_horiz,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                color: Colors.black,
                margin: const EdgeInsets.symmetric(horizontal: 2.0),
                child: PhotoViewGallery.builder(
                  pageController: pageController,
                  onPageChanged: (value) {
                    setState(() {
                      subindex = value;
                    });
                  },
                  itemCount: listImage.length,
                  builder: (context, index) {
                  
                    return PhotoViewGalleryPageOptions(
                      imageProvider: AssetImage(
                        listImage[subindex],
                      ),
                      minScale: PhotoViewComputedScale.contained * 0.8,
                      maxScale: PhotoViewComputedScale.covered * 2,
                    );
                  },
                  scrollPhysics: const BouncingScrollPhysics(),
                  backgroundDecoration: const BoxDecoration(
                    // borderRadius: BorderRadius.all(Radius.circular(20)),
                    color: Colors.black,
                  ),
                  enableRotation: true,
                  loadingBuilder: (context, event) => const Center(
                    child: SizedBox(
                      width: 30.0,
                      height: 30.0,
                      child: CircularProgressIndicator(
                        backgroundColor: Colorconstands.primaryColor,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
