part of 'homework_bloc.dart';

abstract class HomeworkState extends Equatable {
  const HomeworkState();

  @override
  List<Object> get props => [];
}

class HomeworkInitial extends HomeworkState {}

class HomeWorkLoadingState extends HomeworkState {}

class HomeWorkLoadedState extends HomeworkState {
  final BaseResponseModel<HomeWorkModel> detailTeachingModel;
  const HomeWorkLoadedState({required this.detailTeachingModel});
}

/// Lesson Model Loaded
class LessonLoadedState extends HomeworkState {
  final BaseResponseModel<LessonModel> lessonModel;
  const LessonLoadedState({required this.lessonModel});
}

class ReplyCommentState extends HomeworkState {
 
  const ReplyCommentState();
}

/// Online Learning Model Loaded
class OnlinelearningLoadedState extends HomeworkState {
  final BaseResponseModel<OnlineLiveModel> streamingModel;
  const OnlinelearningLoadedState({required this.streamingModel});
}



class HomeWorkErrorState extends HomeworkState {
  final String error;
  const HomeWorkErrorState({required this.error});
}
