import 'package:bloc/bloc.dart';
import 'package:camis_application_flutter/model/base/base_respone_model.dart';
import 'package:camis_application_flutter/model/teaching_model/homework_model.dart';
import 'package:camis_application_flutter/model/teaching_model/onlinelive_model.dart';
import 'package:equatable/equatable.dart';
import '../../../../model/teaching_model/lesson_model.dart';
import '../../../../service/apis/reply_comment_api/reply_api.dart';
import '../../../../service/apis/teaching_api/teaching_api.dart';
import '../../../../storages/get_storage.dart';

part 'homework_event.dart';
part 'homework_state.dart';

class HomeworkBloc extends Bloc<HomeworkEvent, HomeworkState> {
  final TeachingApi teachingApi;
  final ReplyCommentApi replyCommentApi = ReplyCommentApi();
  final GetStoragePref _pref = GetStoragePref();
  BaseResponseModel<HomeWorkModel>? data = BaseResponseModel<HomeWorkModel>();
  HomeworkBloc({required this.teachingApi}) : super(HomeworkInitial()) {
    on<GetHomeWorkTeachingEvent>((event, emit) async {
      emit(HomeWorkLoadingState());
      var idChild = "";
      try {
        data = await teachingApi.getHomeWorkApi(
            studentId: "$idChild", lessonId: event.homeWorkId);

        print("Holyholy ${data.toString()}");
        emit(HomeWorkLoadedState(detailTeachingModel: data!));
      } catch (e) {
        emit(HomeWorkErrorState(error: e.toString()));
      }
    });

    on<GetLessonEvent>((event, emit) async {
      emit(HomeWorkLoadingState());
      var idChild = "";
      try {
        var data = await teachingApi.getLessonApi(
            studentId: idChild.toString(), lessonId: event.lessonId);
        emit(LessonLoadedState(lessonModel: data));
      } catch (e) {
        emit(HomeWorkErrorState(error: e.toString()));
      }
    });

    on<GetOnlineLearningEvent>((event, emit) async {
      emit(HomeWorkLoadingState());
      var idChild = "";
      try {
        var data = await teachingApi.getOnlineLearningApi(
            studentId: idChild.toString(), lessonId: event.liveId);
        emit(OnlinelearningLoadedState(streamingModel: data));
      } catch (e) {
        emit(HomeWorkErrorState(error: e.toString()));
      }
    });

    on<AddNewReplyCommentEvent>((event, emit) async {
      try {
        await replyCommentApi.postReplyCommentApi(
            id: event.replyId,
            parentId: event.parentId,
            reply: event.commentText);
        print(event.replyId.toString() +
            "m" +
            event.parentId +
            event.commentText);
      } catch (e) {}
    });
  }
}
