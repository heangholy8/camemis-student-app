part of 'homework_bloc.dart';

abstract class HomeworkEvent extends Equatable {
  const HomeworkEvent();
  @override
  List<Object> get props => [];
}

class GetHomeWorkTeachingEvent extends HomeworkEvent {
  final String homeWorkId;
  const GetHomeWorkTeachingEvent({required this.homeWorkId});
}

class GetLessonEvent extends HomeworkEvent {
  final String lessonId;
  const GetLessonEvent({required this.lessonId});
}

class AddNewReplyCommentEvent extends HomeworkEvent {
  final String commentText;
  final String parentId;
  final String replyId;

  const AddNewReplyCommentEvent(
      {required this.parentId,
      required this.commentText,
      required this.replyId});
}

class GetOnlineLearningEvent extends HomeworkEvent {
  final String liveId;
  const GetOnlineLearningEvent({required this.liveId});
}
