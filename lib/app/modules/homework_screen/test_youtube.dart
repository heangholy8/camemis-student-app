import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class YoutubeApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Youtube Player IFrame Demo',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        iconTheme: const IconThemeData(color: Colors.deepPurpleAccent),
      ),
      debugShowCheckedModeBanner: false,
      home: YoutubeAppDemo(),
    );
  }
}

///
class YoutubeAppDemo extends StatefulWidget {
  @override
  _YoutubeAppDemoState createState() => _YoutubeAppDemoState();
}

class _YoutubeAppDemoState extends State<YoutubeAppDemo> {
  late YoutubePlayerController _controller;
  String urlVideoFromYouTube =
      'v0RWej7Sqg4'; 
      //this is the last part of the YouTube url https://www.youtube.com/watch?v=v0RWej7Sqg4 copy-pasted by me

  @override
  void initState() {
    super.initState();
    _controller = YoutubePlayerController(
      initialVideoId: urlVideoFromYouTube,
      params: const YoutubePlayerParams(
        playlist: [
          'nPt8bK2gbaU', //Default playlist
          'K18cpp_-gP8',
          'iLnmTe5Q2Qw',
          '_WoCV4c6XOE',
          'KmzdUe0RSJo',
          '6jZDSSZZxjQ',
          'p2lYr3vM_1w',
          '7QUtEmBT_-w',
          '34_PXCzGw1M',
        ],
        startAt:  Duration(minutes: 0, seconds: 00),
        showControls: true,
        showFullscreenButton: true,
        desktopMode: true,
        privacyEnhanced: true,
        useHybridComposition: true,
      ),
    );
    _controller.onEnterFullscreen = () {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight,
      ]);
      log('Entered Fullscreen');
    };
    _controller.onExitFullscreen = () {
      log('Exited Fullscreen');
    };
  }

  @override
  Widget build(BuildContext context) {
    const player = YoutubePlayerIFrame();

    return Scaffold(
      body: Container(
        child: Column(
          children: [
          const  Text("data"),
            Expanded(
              child:
              YoutubePlayerControllerProvider(
                controller: _controller,
                child: LayoutBuilder(
                  builder: (context, constraints) {
                    if (kIsWeb && constraints.maxWidth > 700) {
                      return Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children:const [
                           Expanded(child: player),
                           SizedBox(
                            width: 500,
                            child: SingleChildScrollView(
                              child: Controls(),
                            ),
                          ),
            
                        ],
                      );
                    }
                    return ListView(
                      children: const [
                        player,
                         Controls(),
                      ],
                    );
                  },
                ),
              ),
           
            ),
          ],
        ),
      ),
    );

    // return YoutubePlayerControllerProvider(
    //   // Passing controller to widgets below.
    //   controller: _controller,
    //   child: Scaffold(
    //     appBar: AppBar(
    //       title: const Text('Youtube Player IFrame'),
    //     ),
    //     body: LayoutBuilder(
    //       builder: (context, constraints) {
    //         if (kIsWeb && constraints.maxWidth > 800) {
    //           return Row(
    //             crossAxisAlignment: CrossAxisAlignment.start,
    //             children:  [
    //               const Expanded(child: player),
    //               const SizedBox(
    //                 width: 500,
    //                 child: SingleChildScrollView(
    //                   child: Controls(),
    //                 ),
    //               ),
    //             ],
    //           );
    //         }
    //         return ListView(
    //           children: [
    //             player,
    //             const Controls(),
    //           ],
    //         );
    //       },
    //     ),
    //   ),
    // );
  }

  @override
  void dispose() {
    _controller.close();
    super.dispose();
  }
}

///
class Controls extends StatelessWidget {
  ///
  const Controls();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // _space,
          // MetaDataSection(),
          // _space,
          // SourceInputSection(),
          // _space,
          // PlayPauseButtonBar(),
          // _space,
          // VolumeSlider(),
          // _space,
          // PlayerStateSection(),
        ],
      ),
    );
  }

  Widget get _space => const SizedBox(height: 10);
}
