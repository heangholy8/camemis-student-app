// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerWidget extends StatelessWidget {
  final Widget child;
  Color? baseColor;
  Color? highlightColor;
  ShimmerWidget(
      {Key? key, required this.child, this.baseColor, this.highlightColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      child: child,
      baseColor: baseColor ?? Colors.white,
      highlightColor: highlightColor ?? Colors.grey.shade300,
    );
  }
}
