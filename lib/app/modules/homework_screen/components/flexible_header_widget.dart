import '../../../routes/e.route.dart';
import '../bloc/homework_bloc.dart';
import '../view/reference_screen.dart';
import '../widgets/custom_appbar_widget.dart';
import 'flexible_header_shimmer.dart';

class FlexibleHeaderWidget extends StatelessWidget {
  const FlexibleHeaderWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 18.0,
        ),
        CustomAppbarWidget(
          width: MediaQuery.of(context).size.width,
          avatar: false,
          title: "HomeWork",
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          height: 350 - 84,
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(18.0),
              topRight: Radius.circular(18.0),
            ),
          ),
          child: BlocBuilder<HomeworkBloc, HomeworkState>(
            builder: (context, state) {
              if (state is HomeWorkLoadingState) {
                return const FlexibleHeaderShimmer();
              }
              if (state is HomeWorkLoadedState) {
                var data = state.detailTeachingModel.data;
                print("sinadsf" + data.toString());
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 18.0,
                        vertical: 18.0,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: MediaQuery.of(context).size.width / 1.8,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  data!.name.toString(),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  style: ThemeConstands.texttheme.headline5!
                                      .copyWith(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                const SizedBox(
                                  height: 18.0,
                                ),
                                RichText(
                                  text: TextSpan(
                                    text: "Date: ",
                                    style: ThemeConstands.texttheme.subtitle1!
                                        .copyWith(
                                      color: Colorconstands.darkGray,
                                      fontWeight: FontWeight.w600,
                                    ),
                                    children: [
                                      TextSpan(
                                        text: "${data.startDate}",
                                        style: ThemeConstands
                                            .texttheme.subtitle1!
                                            .copyWith(
                                          color: Colorconstands.darkGray,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 8.0,
                                ),
                                RichText(
                                  text: TextSpan(
                                    text: "Deadline: ",
                                    style: ThemeConstands.texttheme.subtitle1!
                                        .copyWith(
                                      color: Colorconstands.darkGray,
                                      fontWeight: FontWeight.w600,
                                    ),
                                    children: [
                                      TextSpan(
                                        text: "${data.endDate}",
                                        style: ThemeConstands
                                            .texttheme.subtitle1!
                                            .copyWith(
                                          color: Colorconstands.darkGray,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(
                                Radius.circular(4.0),
                              ),
                            ),
                            child: MaterialButton(
                              shape: const RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(4.0),
                                ),
                              ),
                              minWidth: 30,
                              color: Colorconstands.primaryColor,
                              onPressed: () {
                                print("Reference");
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: ((context) =>
                                        const RefereneceScreen()),
                                  ),
                                );
                              },
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 10.0,
                                  vertical: 10.0,
                                ),
                                child: Text(
                                  "Reference",
                                  style: ThemeConstands.texttheme.bodyText2!
                                      .copyWith(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 18.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Description: ",
                            style: ThemeConstands.texttheme.subtitle1!.copyWith(
                              color: Colorconstands.darkGray,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          Text(
                            "${data.description}",
                            style: ThemeConstands.texttheme.headline6,
                            maxLines: 4,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                  ],
                );
              }
              if (state is HomeWorkErrorState) {
                return Center(
                  child: Text(state.error.toString()),
                );
              }
              return const Center(
                child: Text("Please try again!!"),
              );
            },
          ),
        ),
      ],
    );
  }
}
