import 'package:camis_application_flutter/app/modules/homework_screen/components/shimmer_widget.dart';
import '../../auth_screens/e_school_code.dart';
import '../bloc/homework_bloc.dart';
import '../widgets/custom_tabbar_widget.dart';

// class MyDelegate extends SliverPersistentHeaderDelegate {
//   MyDelegate({required this.tabController});
//   final TabController tabController;
//   @override
//   Widget build(
//       BuildContext context, double shrinkOffset, bool overlapsContent) {
//     return BlocBuilder<HomeworkBloc, HomeworkState>(
//       builder: (context, state) {
//         if (state is HomeWorkLoadingState) {
//           return ShimmerWidget(
//             child: CustomTabbarWidget(
//               width: MediaQuery.of(context).size.width,
//               tabController: tabController,
//               tabs: [
//                 Tab(
//                   child: Text(
//                     "HomeWork",
//                     style: ThemeConstands.texttheme.subtitle1!
//                         .copyWith(fontWeight: FontWeight.w600),
//                   ),
//                 ),
//                 Tab(
//                   child: Text(
//                     "Form",
//                     style: ThemeConstands.texttheme.subtitle1!
//                         .copyWith(fontWeight: FontWeight.w600),
//                   ),
//                 ),
//               ],
//             ),
//           );
//         }
//         if (state is HomeWorkLoadedState) {
//           return CustomTabbarWidget(
//             width: MediaQuery.of(context).size.width,
//             tabController: tabController,
//             tabs: [
//               Tab(
//                 child: Text(
//                   "HomeWork",
//                   style: ThemeConstands.texttheme.subtitle1!
//                       .copyWith(fontWeight: FontWeight.w600),
//                 ),
//               ),
//               Tab(
//                 child: Text(
//                   "Form",
//                   style: ThemeConstands.texttheme.subtitle1!
//                       .copyWith(fontWeight: FontWeight.w600),
//                 ),
//               ),
//             ],
//           );
//         }
//         return const Center(
//           child: Text("Please try again!"),
//         );
//       },
//     );
//   }

//   @override
//   double get maxExtent => 80;

//   @override
//   double get minExtent => 70;

//   @override
//   bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
//     return false;
//   }
// }
