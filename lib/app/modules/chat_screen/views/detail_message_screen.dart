import 'package:camis_application_flutter/app/modules/chat_screen/controllers/data.dart';
import 'package:flutter/material.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';
import '../widgets/bottom_widget.dart';

class DetailMessageScreen extends StatefulWidget {
  const DetailMessageScreen({Key? key}) : super(key: key);

  @override
  State<DetailMessageScreen> createState() => _DetailMessageScreenState();
}

class _DetailMessageScreenState extends State<DetailMessageScreen> {
  late TextEditingController _textController;
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    _textController = TextEditingController();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: SizedBox(
          width: width,
          height: height,
          child: Column(
            children: [
              Container(
                height: 145,
                padding: const EdgeInsets.only(top: 27),
                child: Row(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(left: 8),
                      child: IconButton(
                        icon: const Icon(
                          Icons.arrow_back_ios_new_outlined,
                          size: 25,
                          color: Colorconstands.white,
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    const CircleAvatar(
                      radius: 30,
                      backgroundImage: NetworkImage(
                        "https://image.shutterstock.com/image-photo/head-shot-portrait-close-smiling-260nw-1714666150.jpg",
                      ),
                    ),
                    const SizedBox(
                      width: 18.0,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Smith Mathew",
                          style:
                              ThemeConstands.texttheme.headlineSmall?.copyWith(
                            color: Colorconstands.white,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        Text(
                          "Teacher",
                          style: ThemeConstands.texttheme.subtitle2?.copyWith(
                            color: const Color(0xFFDEDEDE),
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  color: Colors.white,
                  padding: const EdgeInsets.symmetric(horizontal: 18.0),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    physics: const BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        const SizedBox(
                          height: 18,
                        ),
                        ListView.builder(
                            controller: _scrollController,
                            shrinkWrap: true,
                            itemCount: dataMessage.length,
                            reverse: true,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.only(bottom: 20.0),
                                child: Row(
                                  mainAxisAlignment:
                                      dataMessage[index].fromto == false
                                          ? MainAxisAlignment.start
                                          : MainAxisAlignment.end,
                                  children: [
                                    const CircleAvatar(
                                      radius: 25,
                                      backgroundImage: NetworkImage(
                                        "http://lavinephotography.com.au/wp-content/uploads/2017/01/PROFILE-Photography-112.jpg",
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 14.0,
                                    ),
                                    Container(
                                      width: width / 2,
                                      decoration: BoxDecoration(
                                        color: const Color(0xFFE4E4E4)
                                            .withOpacity(0.7),
                                        borderRadius: const BorderRadius.only(
                                          topLeft: Radius.circular(28.0),
                                          topRight: Radius.circular(28.0),
                                          bottomRight: Radius.circular(28.0),
                                        ),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(14.0),
                                        child: Text(
                                          dataMessage[dataMessage.length -
                                                  1 -
                                                  index]
                                              .text
                                              .toString(),
                                          style: ThemeConstands
                                              .texttheme.headline6!
                                              .copyWith(
                                            fontWeight: FontWeight.normal,
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              );
                            }),
                      ],
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 80,
              ),
            ],
          ),
        ),
      ),
      bottomSheet: BottomWidget(
        controller: _textController,
        onChanged: (value) {},
        btnTap: () {
          if (_textController.text.isNotEmpty) {
            _scrollController.animateTo(
                _scrollController.position.maxScrollExtent,
                duration: const Duration(seconds: 1),
                curve: Curves.ease);
            dataMessage.insert(
              dataMessage.length,
              MessageDate(text: _textController.text, fromto: true),
            );

            setState(() {
              _textController.clear();
            });
          }
        },
      ),
    );
  }
}
