import 'package:camis_application_flutter/app/modules/chat_screen/data.dart';
import 'package:camis_application_flutter/app/modules/chat_screen/views/detail_message_screen.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import '../../../../widgets/custom_header.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';
import '../widgets/listtile_box_widget.dart';
import '../widgets/search_widget.dart';

class MessageScreen extends StatefulWidget {
  const MessageScreen({Key? key}) : super(key: key);
  @override
  State<MessageScreen> createState() => _MessageScreenState();
}

class _MessageScreenState extends State<MessageScreen>
    with TickerProviderStateMixin {
  List<Tab> tabs = [
    const Tab(text: "Message"),
    const Tab(text: "Guardians"),
    const Tab(text: "Teachers"),
  ];
  late TabController _tabController;
  @override
  void initState() {
    _tabController = TabController(length: tabs.length, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: SafeArea(
        bottom: false,
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: SizedBox(
            width: width,
            height: height,
            child: Column(
              children: [
                Container(
                margin: const EdgeInsets.only(left: 8.0),
                child: CustomHeader(title: "Chat")),
                Expanded(
                  child: Container(
                    width: width,
                    height: double.infinity,
                    decoration: const BoxDecoration(
                      color: Colors.white,
                    ),
                    child: Column(
                      children: [
                        const SizedBox(
                          height: 18,
                        ),
                        SizedBox(
                          // padding: const EdgeInsets.symmetric(horizontal: 2.0),
                          height: 40,
                          child: TabBar(
                            controller: _tabController,
                            labelColor: const Color.fromRGBO(24, 85, 171, 1),
                            indicator: const BoxDecoration(
                              border: Border(
                                bottom: BorderSide(
                                  color: Colorconstands.primaryColor,
                                  width: 2.0,
                                ),
                              ),
                            ),
                            labelStyle:
                                ThemeConstands.texttheme.headline5!.copyWith(
                              fontWeight: FontWeight.w600,
                            ),
                            indicatorSize: TabBarIndicatorSize.tab,
                            indicatorPadding:
                                const EdgeInsets.symmetric(horizontal: 20),
                            physics: const NeverScrollableScrollPhysics(),
                            isScrollable: false,
                            tabs: tabs.toList(),
                          ),
                        ),
                        const SizedBox(
                          height: 18,
                        ),
                        const SearchTextFieldWidget(),
                        Container(
                          margin: const EdgeInsets.only(top: 8.0),
                          height: 4,
                          width: width,
                          color: Colorconstands.darkGray.withOpacity(0.1),
                        ),
                        Expanded(
                          child: TabBarView(
                            controller: _tabController,
                            children: List.generate(
                              tabs.length,
                              (index) => SizedBox(
                                width: width,
                                child: ListView.builder(
                                  itemCount: listChat.length,
                                  itemBuilder: (context, index) {
                                    return ListTileBoxWidget(
                                      onTap: () {
                                        FocusScope.of(context).unfocus();
                                        Navigator.push(
                                          context,
                                          PageTransition(
                                            type:
                                                PageTransitionType.rightToLeft,
                                            child: const DetailMessageScreen(),
                                          ),
                                        );
                                      },
                                      profile: listChat[index].profile,
                                      title: listChat[index].userName,
                                      subtitle: listChat[index].preChat,
                                      countMessage:
                                          listChat[index].countMessage,
                                      datetime: listChat[index].date,
                                    );
                                  },
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
