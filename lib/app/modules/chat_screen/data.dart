class MessageData {
  final String? profile;
  final String? userName;
  final String? date;
  final int? countMessage;
  final String? preChat;

  MessageData(
      {this.preChat,
      this.profile,
      this.userName,
      this.date,
      this.countMessage});
}

List<MessageData> listChat = [
  MessageData(
      profile:
          "https://image.shutterstock.com/image-photo/close-side-profile-photo-beautiful-260nw-1408926620.jpg",
      userName: "Sokraksa Panha",
      date: "29 mar",
      countMessage: 10,
      preChat: "Hi, David. Hope you're doingsdfsdfsdfasdfasdfsadfs"),
  MessageData(
      profile:
          "https://image.shutterstock.com/image-photo/head-shot-portrait-close-smiling-260nw-1714666150.jpg",
      userName: "Teachers' Name",
      date: "29 mar",
      countMessage: 0,
      preChat: "Are you ready for today's part session"),
  MessageData(
      profile:
          "http://lavinephotography.com.au/wp-content/uploads/2017/01/PROFILE-Photography-112.jpg",
      userName: "Unknow User",
      date: "29 mar",
      countMessage: 9,
      preChat: "Hehe, Come to play game with me in our class Lol"),
  MessageData(
      profile:
          "https://image.shutterstock.com/image-photo/close-side-profile-photo-beautiful-260nw-1408926620.jpg",
      userName: "Guardians' name",
      date: "29 mar",
      countMessage: 4,
      preChat: "Hi, David. Hope you're doingsdfsdfsdfasdfasdfsadfs"),
  MessageData(
      profile:
          "https://image.shutterstock.com/image-photo/close-side-profile-photo-beautiful-260nw-1408926620.jpg",
      userName: "Guardians' name",
      date: "29 mar",
      countMessage: 0,
      preChat: "Hi, David. Hope you're doingsdfsdfsdfasdfasdfsadfs"),
];
