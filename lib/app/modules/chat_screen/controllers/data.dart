class MessageDate {
  final String? text;
  final bool? fromto;
  DateTime? timestamp = DateTime.now();
  MessageDate({this.text, this.fromto, this.timestamp});
}

List<MessageDate> dataMessage = <MessageDate>[
  MessageDate(
    text: "Hello mommy ah Muy",
    fromto: false,
  ),
  MessageDate(
    text: "I have some to tell u",
    fromto: false,
  ),
  MessageDate(
    text: "Hello, mommy ah tinh",
    fromto: true,
  ),
  MessageDate(
    text: "What's up?",
    fromto: true,
  ),
  MessageDate(
    text: "Hello mommy ah Muy",
    fromto: true,
  ),
  MessageDate(
    text: "Hello mommy ah Muy",
    fromto: false,
  ),
  MessageDate(
    text: "Hello mommy ah Muy",
    fromto: true,
  ),
  MessageDate(
    text: "Hello mommy ah Muy",
    fromto: false,
  ),
  MessageDate(
    text: "Hello mommy ah Muy",
    fromto: true,
  ),
  MessageDate(
    text: "Hello mommy ah Muy",
    fromto: false,
  ),
  MessageDate(
    text: "Hello mommy ah Muy",
    fromto: true,
  ),
  MessageDate(
    text: "Hello mommy ah Muy",
    fromto: false,
  ),
];
