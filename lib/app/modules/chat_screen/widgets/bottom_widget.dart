import 'package:flutter/material.dart';

import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class BottomWidget extends StatelessWidget {
  final TextEditingController? controller;
  final GestureTapCallback? btnTap;
  final Function(String)? onChanged;

  const BottomWidget({Key? key, this.controller, this.btnTap, this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Container(
      height: 80,
      padding: const EdgeInsets.only(left: 12.0),
      width: width,
      color: Colors.white,
      child: Row(
        children: [
          Expanded(
            child: SizedBox(
              height: 56,
              child: TextFormField(
                controller: controller,
                autocorrect: true,
                decoration: InputDecoration(
                  hintText: "write here...",
                  hintStyle: ThemeConstands.texttheme.subtitle1!.copyWith(
                      fontWeight: FontWeight.w600,
                      color: const Color(0xFF8B8B8B)),
                  fillColor: Colors.white,
                  filled: true,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    borderSide: const BorderSide(),
                  ),
                ),
                onChanged: onChanged,
                keyboardType: TextInputType.text,
                style: ThemeConstands.texttheme.subtitle1!.copyWith(
                  fontWeight: FontWeight.w600,
                  color: Colorconstands.primaryColor,
                ),
              ),
            ),
          ),
          const SizedBox(
            width: 0.0,
          ),
          MaterialButton(
            shape: const CircleBorder(),
            color: Colorconstands.primaryColor,
            onPressed: btnTap,
            child: const Padding(
              padding: EdgeInsets.all(14.0),
              child: Icon(
                Icons.send,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
