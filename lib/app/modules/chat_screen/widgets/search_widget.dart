import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class SearchTextFieldWidget extends StatelessWidget {
  const SearchTextFieldWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
          horizontal: 12, vertical: 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 52,
            child: TextFormField(
              autocorrect: true,
              decoration: InputDecoration(
                prefixIcon: const Icon(
                  CupertinoIcons.search,
                  color: Colorconstands.primaryColor,
                ),
                hintText: "Search here...",
                hintStyle: ThemeConstands.texttheme.subtitle1!
                    .copyWith(
                        fontWeight: FontWeight.w600,
                        color: const Color(0xFF8B8B8B)),
                fillColor: Colors.white,
                filled: true,
                // suffixIcon: suffixIcon,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12.0),
                  borderSide: const BorderSide(),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12.0),
                  borderSide: const BorderSide(
                    color: Color(0xFF8B8B8B),
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12.0),
                  borderSide: const BorderSide(
                      color: Color(0xFF8B8B8B)),
                ),
              ),
              onChanged: (value) {},
              keyboardType: TextInputType.text,
              style:
                  ThemeConstands.texttheme.subtitle1!.copyWith(
                fontWeight: FontWeight.w600,
                //  color: Colorconstands.primaryColor,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
