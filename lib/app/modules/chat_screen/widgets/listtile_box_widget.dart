import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:flutter/material.dart';

class ListTileBoxWidget extends StatelessWidget {
  final GestureTapCallback? onTap;
  final String? profile;
  final String? title;
  final String? subtitle;
  final String? datetime;
  final int? countMessage;
  final TextStyle? subStyle;

  const ListTileBoxWidget(
      {Key? key,
      this.onTap,
      this.profile,
      this.title,
      this.subtitle,
      this.datetime,
      this.countMessage,
      this.subStyle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      color: countMessage == 0 ? Colors.white : const Color(0xFFF1F9FF),
      onPressed: onTap,
      shape: Border(
        bottom: BorderSide(
          color: Colors.grey.withOpacity(0.1),
        ),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.only(top: 18.0, bottom: 24.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width - 104,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 56,
                    width: 56,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage(
                          profile.toString(),
                        ),
                      ),
                      shape: BoxShape.circle,
                    ),
                  ),
                  const SizedBox(
                    width: 18.0,
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width - 180,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "$title",
                          style: ThemeConstands.texttheme.headline6!.copyWith(
                            color: Colors.black,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        const SizedBox(
                          height: 8.0,
                        ),
                        Text(
                          "$subtitle",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: countMessage != 0
                              ? ThemeConstands.texttheme.subtitle1!.copyWith(
                                  color: Colors.black.withOpacity(.7),
                                  fontWeight: FontWeight.w600,
                                )
                              : ThemeConstands.texttheme.subtitle1!.copyWith(
                                  fontWeight: FontWeight.w500,
                                  color: Colors.grey.withOpacity(0.9),
                                ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "$datetime",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: ThemeConstands.texttheme.subtitle1!.copyWith(
                      color: countMessage != 0
                          ? Colorconstands.primaryColor
                          : const Color(0xFFC5BDBD),
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  const SizedBox(
                    height: 8.0,
                  ),
                  countMessage != 0
                      ? CircleAvatar(
                          radius: 12,
                          child: Text(
                            "$countMessage",
                            textAlign: TextAlign.center,
                            style: ThemeConstands.texttheme.bodyText2!.copyWith(
                              fontSize: 12,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        )
                      : const SizedBox(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
