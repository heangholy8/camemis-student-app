import 'dart:async';
import 'dart:io';
import 'package:camis_application_flutter/app/modules/home_screen/view/home_screen.dart';
import 'package:camis_application_flutter/app/modules/home_screen/widget/error_connection_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';
import '../../../routes/app_routes.dart';
import '../../select_lang/view/select_language.dart';
class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool? isoffline;
  Future checkLogin() async {
      final SharedPreferences _pref = await SharedPreferences.getInstance();   
      var token = _pref.getString("jsonToken") ?? "";
      var phoneNumber = _pref.getString("phoneNumber")??"";
      var comfrimAccount = _pref.getString("comfrimProfileSecuess")??"";
      var selectLang = _pref.getString("LangSuccess")??"";
      if(selectLang==""){
          setState(() {
            Future.delayed(const Duration(milliseconds: 1500), () async{
            Navigator.pushAndRemoveUntil(context,PageRouteBuilder(
                              pageBuilder: (context, animation1, animation2) => const SelectLanguageScreen(),
                              transitionDuration: Duration.zero,reverseTransitionDuration: Duration.zero,),(route) => false);
            });
          });
        }
      else{
        if(token!=""){
          // if(phoneNumber!=""){
           if(comfrimAccount!=""){
            try {
                final result = await InternetAddress.lookup('google.com');
                if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
                  setState(() {
                    isoffline = true;
                    Future.delayed(const Duration(milliseconds: 1500), () async{
                      Navigator.pushAndRemoveUntil(context,PageRouteBuilder(
                                pageBuilder: (context, animation1, animation2) => const HomeScreen(),
                                transitionDuration: Duration.zero,
                                reverseTransitionDuration: Duration.zero,
                            ),(route) => false);
                      });
                  });
                }
              } on SocketException catch (_) {
                setState(() {
                  isoffline = false;
                  Future.delayed(const Duration(milliseconds: 1500), () async{
                      Navigator.pushAndRemoveUntil( context,PageRouteBuilder(
                                pageBuilder: (context, animation1, animation2) => const ErroreWidgetConnection(),
                                transitionDuration: Duration.zero,
                                reverseTransitionDuration: Duration.zero,
                            ),(route) => false);
                      });
                });
              }
             }
              else{
                Future.delayed(const Duration(milliseconds: 1500), () async{
                  Navigator.pushReplacementNamed(context, Routes.ACCOUNTCONFIRM);
                });
              }
          // }
          // else{
          //   Future.delayed(const Duration(milliseconds: 1500), () async{
          //     Navigator.pushReplacementNamed(context, Routes.LOGINOTP);
          //   });
          // }
        }
        else{
          Future.delayed(const Duration(milliseconds: 1500), () async{
            Navigator.pushNamed(context, Routes.OPTIONLOGINSCREEN);
          });
          
        }
    }
  }
  @override
  void initState() {
    setState(() {
     checkLogin();
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: Stack(
        children: [
          Positioned(
            top: 0,right: 0,
            child: Image.asset("assets/images/Oval.png",width: MediaQuery.of(context).size.width/1.7,),
          ),
          Positioned(
            top: 150,left: 0,
            child: Image.asset("assets/images/Oval (1).png",),
          ),
          Positioned(
            bottom: 35,left: 0,
            child: Image.asset("assets/images/Oval (3).png",),
          ),
          Positioned(
            bottom: MediaQuery.of(context).size.height/5,right: MediaQuery.of(context).size.width/4,
            child: Center(child: Image.asset("assets/images/Oval (2).png",)),
          ),
          Container(
            margin:const EdgeInsets.symmetric(vertical: 75),
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  child: Column(
                    children: [
                      Text("Powered by:",style: ThemeConstands.caption_Regular_12.copyWith(color: Colors.transparent),),
                      Text("CAMIS Solutions",style: ThemeConstands.caption_Regular_12.copyWith(color: Colors.transparent),),
                    ],
                  ),
                ),
                Container(
                  margin:const EdgeInsets.only(top: 5,left: 15),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                        Container(
                          margin:const EdgeInsets.only(top: 0,right: 12),
                          child: SvgPicture.asset(ImageAssets.logoCamis,width: 45,height: 45,),
                        ),
                          Container(child: Text("CAMEMIS",style: ThemeConstands.headline1_SemiBold_32.copyWith(color: Colorconstands.neutralWhite,fontWeight: FontWeight.w600,fontSize: 40),textAlign: TextAlign.center,)),
                        ],
                      ),
                      Container(
                        margin:const EdgeInsets.only(left: 18),
                        child:Text("Recommended for Student",style: ThemeConstands.caption_Regular_12.copyWith(color: Colorconstands.neutralWhite,),),
                      )
                    ], 
                  ),
                ),
                Container(
                  child: Column(
                    children: [
                      Text("Powered by:",style: ThemeConstands.caption_Regular_12.copyWith(color: Colorconstands.neutralWhite,),),
                      Text("CAMIS Solutions",style: ThemeConstands.caption_Regular_12.copyWith(color: Colorconstands.neutralWhite),),
                      Container(
                        margin:const EdgeInsets.only(top: 8),
                        height: 30,width: 30,
                        child:const Center(
                          child: CircularProgressIndicator(color: Colorconstands.neutralWhite,),
                        ),
                      )
                    ],
                  ),
                ),
                
              ],
            ),
          ),
        ],
      ),
    );
  }
}