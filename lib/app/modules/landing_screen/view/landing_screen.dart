import 'dart:math';
import 'package:camis_application_flutter/app/modules/auth_screens/views/option_login_screen.dart';
import 'package:camis_application_flutter/widgets/button_widget/button_customwidget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:camis_application_flutter/app/modules/landing_screen/local_data.dart/OnboardaingModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../storages/save_storage.dart';
class LandingScreen extends StatefulWidget {
  const LandingScreen({Key? key}) : super(key: key);

  @override
  State<LandingScreen> createState() => _LandingScreenState();
}

class _LandingScreenState extends State<LandingScreen> {

  final SaveStoragePref _savePref = SaveStoragePref();

int _currentIndex = 0;
  final PageController _pageController = PageController();

  String leasindsuccess = 'Leading-Success';

  void _navigate() async {
      _savePref.saveLeadingSuccess(leadingSuccess: leasindsuccess.toString());
      Navigator.of(context)
          .pushReplacement(PageRouteBuilder(pageBuilder: (ctx, anim1, anim2) {
        return  OptionLoginScreen();
      }, transitionsBuilder: (ctx, anim1, anim2, child) {
        return FadeTransition(opacity: anim1, child: child);
      }));
  }

  @override
  Widget build(BuildContext context) {
   final height = MediaQuery.of(context).size.height;
   final width = MediaQuery.of(context).size.width;
    var screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colorconstands.primaryBackgroundColor,
      body: Stack(
        children: [
          Positioned(
            top: 85,
            left: 0,
            child: Container(
              alignment: Alignment.centerLeft,
              child: SizedBox(
                height: height/1.7,
                width: width,
                child: SvgPicture.asset("assets/images/svg/background_onboarding.svg",fit: BoxFit.fill,)),),
          ),
          Positioned(
            top: 65,
            left: 0,
            right: 0,
            child: Container(
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 5),
                    child: SizedBox(
                      child: Image.asset('assets/images/svg/logo.png')
                    ),
                  ),
                    const SizedBox(width: 5.0,),
                  Align(alignment: Alignment.center,child: Text("CAMEMIS",style: ThemeConstands.texttheme.headline5!.copyWith(fontWeight: FontWeight.bold,color: Colorconstands.thirdColor))),
                  
                ],
              ),),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              decoration:const BoxDecoration(
                color: Colorconstands.white,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0),topRight: Radius.circular(15.0))
              ),
              height: height/2.2,
            )
          ),
          Positioned(
            bottom: height/2.2+15,
            left: 0,
            right: 0,
            child: SvgPicture.asset(
              contents[_currentIndex].image,
              key: Key('${Random().nextInt(999999999)}'),
              width: width/1,
              alignment: Alignment.topCenter,
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              padding:const EdgeInsets.only(top: 18.0 ,left: 17.0,right: 17.0),
              height: height/2.2,
              width: width,
              child: Column(
                children: [
                  Flexible(
                    child: PageView.builder(
                      controller: _pageController,
                      itemCount: contents.length,
                      itemBuilder: (BuildContext context, int index) {
                        OnboardingModel tab = contents[index];
                        return Column(
                          children: [
                            Text(
                              tab.title,
                              style: ThemeConstands.texttheme.headline5!.copyWith(fontWeight: FontWeight.bold,color: Colorconstands.darkGray,)
                            ),
                            const SizedBox(height: 5),
                            Text(
                              tab.subtitle,
                              style: ThemeConstands.texttheme.subtitle2!.copyWith(color: Colorconstands.darkGray,)
                            ),
                          ],
                        );
                      },
                      onPageChanged: (value) {
                        _currentIndex = value;
                        setState(() {});
                      },
                    ),
                  ),
                  
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      for (int index = 0; index < contents.length; index++)
                        _DotIndicator(isSelected: index == _currentIndex),
                    ],
                  ),
                  Container(
                    margin:const EdgeInsets.only(top: 18,bottom: 0),
                    child: Button_Custom(
                      hightButton: 45,
                      buttonColor: Colorconstands.primaryColor,
                      radiusButton: 13,
                      titleButton: _currentIndex==2?'GETSTART'.tr():'Next'.tr(),
                      titlebuttonColor: Colorconstands.white,
                      onPressed: (){
                        if (_currentIndex == 2) {
                          _navigate();
                        } else {
                          _pageController.nextPage(
                            duration: const Duration(milliseconds: 300),
                            curve: Curves.linear,
                          );
                        }
                      },
                    ),
                  ),
                 _currentIndex==2?Container():Container(
                    child: CustomTextbutton(
                      color: Colorconstands.darkGray,
                      title: "SKIP",
                      onPressed: (){
                        _navigate();
                      }, size: 15,
                    ),
                  ),
                  const SizedBox(height: 35)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _DotIndicator extends StatelessWidget {
  final bool isSelected;

  const _DotIndicator({Key? key, required this.isSelected}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:const EdgeInsets.all(3),
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 300),
        height: 8.0,
        width: 8.0,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: isSelected ? Colorconstands.primaryColor : Colors.grey[400],
        ),
      ),
    );
  }
}
