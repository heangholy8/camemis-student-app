class OnboardingModel {
  final String image;
  final String title;
  final String subtitle;

  OnboardingModel(this.image, this.title, this.subtitle);
}

List<OnboardingModel> contents = [
  OnboardingModel(
    'assets/images/onboarding/image_leading1.svg',
    'Feature 1',
    'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia Excep sint occaecat cupidatat non proident, sunt in culpa qui officia ',
  ),
  OnboardingModel(
    'assets/images/onboarding/Teaching and learning in school 1.svg',
    'Feature 2',
    'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia Excep sint occaecat cupidatat non proident, sunt in culpa qui officia '
  ),
  OnboardingModel(
    'assets/images/onboarding/Document collaboration 1.svg',
    'Feature 3',
    'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia Excep sint occaecat cupidatat non proident, sunt in culpa qui officia '
  ),
];