import 'package:camis_application_flutter/app/modules/PaymentAndBillScreen/view/selected_bank_screen.dart';
import 'package:camis_application_flutter/app/modules/PaymentAndBillScreen/widget/card_free_and_premium.dart';
import 'package:camis_application_flutter/app/modules/home_screen/e_home.dart';
import 'package:camis_application_flutter/widgets/shimmer_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../helper/route.export.dart';
import '../../../routes/app_routes.dart';
class SelectedPaymentOption extends StatefulWidget {
  const SelectedPaymentOption({Key? key}) : super(key: key);

  @override
  State<SelectedPaymentOption> createState() => _SelectedPaymentOptionState();
}

class _SelectedPaymentOptionState extends State<SelectedPaymentOption> {
  bool isFrist = true;
  @override
  void initState() {
    setState(() {
      BlocProvider.of<PaymentOptionBloc>(context).add(GetPaymentOptionEvent());
      BlocProvider.of<PaymentBlocBloc>(context).add(GetPaymentEvent());
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: WillPopScope(
        onWillPop: ()async{
          return true;
        },
        child: Container(
          decoration:const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
               end: Alignment.bottomCenter,
              colors: [Colorconstands.primaryColor, Color(0xFF0A2B58)])
          ),
          child: Stack(
            children: [
              Positioned(
                top: 0,right: 0,
                child: Image.asset("assets/images/Path_back_Upgrade_service.png"),
              ),
              Container(
                margin:const EdgeInsets.only(top: 30),
                child: Column(
                  children: [
                    Container(
                      margin:const EdgeInsets.symmetric(vertical: 10,horizontal: 8),
                      child: Row(
                        children: [
                          Container(
                            height: 45,width: 45,
                            child: MaterialButton(
                              padding:const EdgeInsets.all(0),
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              onPressed:(){
                                Navigator.of(context).pop();
                              },
                              child:const Icon(Icons.arrow_back_ios_new_rounded,size: 23,color: Colorconstands.neutralWhite,),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              child: Text("UPGRADE_PLAN".tr(),style: ThemeConstands.headline3_SemiBold_20.copyWith(color: Colorconstands.neutralWhite),textAlign: TextAlign.center,),
                            ),
                          ),
                          BlocBuilder<PaymentBlocBloc, PaymentBlocState>(
                              builder: (context, state) {
                                if(state is PaymentBlocLoadingState){
                                  return Container();
                                }
                                else if(state is PaymentBlocLoadedState){
                                  var data = state.currentPaymentBloc!.data;
                                  return data!.isEmpty? Container()
                                   :IconButton(
                                    onPressed: () {
                                      Navigator.pushNamed(
                                          context, Routes.PAYMENT);
                                    },
                                    icon: const Icon(
                                      Icons.timer_sharp,
                                      color: Colorconstands.white,
                                      size: 25,
                                    ));
                                }
                                else{
                                  return Container();
                                }
                                
                              },
                            ),
                        ],
                      ),
                    ),
                    const CardFreeAndPremium(),
                    Expanded(
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        padding: const EdgeInsets.only(
                            top: 18, right: 0, left: 0, bottom: 0),
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(15),
                              topRight: Radius.circular(15)),
                          color: Colorconstands.white,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin:const EdgeInsets.symmetric(horizontal: 15),
                            child: Text(
                              "CHOOSE_PAYMENT_OPTION".tr(),
                              style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.black),
                            ),
                          ),
                          const SizedBox(height: 5,),
                          Expanded(
                            child:SingleChildScrollView(
                              child: BlocBuilder<PaymentOptionBloc, PaymentOptionState>(
                              builder: (context, state) {
                                if (state is PaymentOptionLoadingState) {
                                  return ListView.builder(
                                    padding:const EdgeInsets.all(0),
                                    shrinkWrap: true,
                                    itemBuilder: (BuildContext contex, index) {
                                      return Container(
                                        margin:const EdgeInsets.only(top: 15,left: 15,right: 15),
                                        child: const ShimmerAbsentRequest(),
                                      );
                                    },
                                    itemCount: 3,
                                  );
                                }
                                if (state is PaymentOptionLoadedState) {
                                  var paymentOptionDate = state.currentPaymentOption.data!;
                                  paymentOptionDate.sort((b, a) => a.amount!.compareTo(b.amount!));                                  
                                  return ListView.builder(
                                    physics:const ScrollPhysics(),
                                    padding: const EdgeInsets.all(0),
                                    shrinkWrap: true,
                                    itemCount: paymentOptionDate.length,
                                    itemBuilder: (context, index) {
                                      return Container(
                                         margin:const EdgeInsets.symmetric(horizontal: 15),
                                        child: Stack(
                                          children: [
                                            Container(
                                              margin:const EdgeInsets.only(top: 15,bottom: 5),
                                              child: MaterialButton(
                                                elevation: 0,
                                                color:paymentOptionDate[index].type==3? Colorconstands.primaryBackgroundColor:Colorconstands.neutralWhite,
                                                padding:const EdgeInsets.all(0),
                                                onPressed: () {
                                                  setState(() {
                                                     Navigator.push(context, MaterialPageRoute(builder: (context)=> SelectedBankScreen(typePayment: paymentOptionDate[index].type!.toInt(),)));
                                                  });
                                                },
                                                shape:const RoundedRectangleBorder(
                                                   borderRadius: BorderRadius.all(Radius.circular(15))),
                                                child: Container(
                                                  height: 84,
                                                  decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.circular(15),
                                                      border: Border.all(color: Colorconstands.primaryColor,width: 2),
                                                  ),
                                                  child: Container(
                                                    margin:const EdgeInsets.symmetric(horizontal: 18,vertical: 6),
                                                    child: Row(mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        Text(translate=="km"? paymentOptionDate[index].name!:paymentOptionDate[index].nameEn!,
                                                            style: ThemeConstands.headline3_SemiBold_20.copyWith(
                                                              color: Colorconstands.primaryColor,
                                                            )),
                                                        Text(paymentOptionDate[index].amount!.toString() +
                                                                (paymentOptionDate[index].currency!.toString() == "KHR"? " ៛": " \$"),
                                                            style: ThemeConstands.headline3_SemiBold_20.copyWith(color:  Colorconstands.primaryColor
                                                         )),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            paymentOptionDate[index].type==3? Positioned(
                                                top: 0,
                                                right: 30,
                                                child: Container(
                                                    padding: const EdgeInsets.symmetric(
                                                        horizontal: 20, vertical: 8),
                                                    decoration: BoxDecoration(
                                                      borderRadius:BorderRadius.circular(8),
                                                      color:Colorconstands.primaryColor,
                                                          
                                                    ),
                                                    child: Text( "SAVE_TWO_MONTH".tr(),
                                                        style: ThemeConstands
                                                            .headline6_Medium_14.copyWith(color: Colorconstands.white,))),
                                              ):Container(),
                                          ],
                                        ),
                                      );
                                    },
                                  );
                                } else {
                                  return  Text("DATA_ERROR".tr());
                                }
                              },
                                                  ),
                            )),
                        ],
                      ),
                    )),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
