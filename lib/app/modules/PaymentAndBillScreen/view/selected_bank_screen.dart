import 'dart:async';
import 'package:camis_application_flutter/app/modules/PaymentAndBillScreen/bloc/bloc/create_khqr_bloc.dart';
import 'package:camis_application_flutter/app/modules/PaymentAndBillScreen/bloc/check_transaction_payment/bloc/check_transaction_bloc.dart';
import 'package:camis_application_flutter/app/modules/PaymentAndBillScreen/view/current_payment_screen.dart';
import 'package:camis_application_flutter/app/routes/e.route.dart';
import 'package:camis_application_flutter/helper/check_platform_device.dart';
import 'package:camis_application_flutter/storages/save_storage.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';
import '../../../../service/apis/profile_user/bloc/get_profile_user_bloc.dart';
import '../../../../storages/get_storage.dart';
import '../../../../widgets/no_connection_alert_widget.dart';
import '../bloc/bloc/aba_deep_link_bloc.dart';
import '../widget/button_select_bank.dart';
import 'create_payment_detail.dart';
class SelectedBankScreen extends StatefulWidget {
  final int? typePayment;
  const SelectedBankScreen({Key? key, required this.typePayment})
      : super(key: key);

  @override
  State<SelectedBankScreen> createState() => _SelectedBankScreenState();
}

class _SelectedBankScreenState extends State<SelectedBankScreen> with Toast{
  SaveStoragePref saveStoragePref = SaveStoragePref();
  GetStoragePref getStoragePref = GetStoragePref();
  StreamSubscription? sub;
  bool connection = true;
  bool isLoading = false;
  bool customLoadingKhqr = false;
  String? checkRedirectApp;
  bool showKHQR = false;
  bool showPayWay = false;
  String location ="";
  String guardianId="";
  bool paymentfailed = false;
  void _launchURL(String deepLink,String uriIos,String uriAnd) async {
    String pletform = checkPlatformDevice();
   if(pletform == "ios"){
   final url = Uri.parse(deepLink);
    launchUrl(
        url,
        mode: LaunchMode.externalApplication,
      ).then((value) {
        if(value){
          launchUrl(
            url,
            mode: LaunchMode.externalApplication,
          );
        }
        else{
          final url = Uri.parse(uriIos,);
          launchUrl(
            url,
            mode: LaunchMode.externalApplication,
          );
        }
      });
   }
   else{
      final url = Uri.parse(deepLink);
      launchUrl(
          url,
          mode: LaunchMode.externalApplication,
        ).then((value) {
          if(value){
            launchUrl(
              url,
              mode: LaunchMode.externalApplication,
            );
          }
          else{
            final url = Uri.parse(uriAnd,);
            launchUrl(
              url,
              mode: LaunchMode.externalApplication,
            );
          }
        });
   }
  }
  void getLocalData() async {
    var rediractApp = await getStoragePref.getRediractApp();
    setState(() {
      checkRedirectApp = rediractApp;
    });
  }

  late Timer _every10MinutesKHQR;
  late Timer _every10MinutesPayWay;

  @override
  void initState() {
    setState(() {
      getLocalData();
      //=============Check internet====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          connection = (event != ConnectivityResult.none);
        });
      });
    });
    //=============Eend Check internet====================
    BlocProvider.of<GetProfileUserBloc>(context).add(GetProfileUser());
    super.initState();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    Future.delayed(const Duration(minutes: 4), () {
      setState(() {
        _every10MinutesPayWay.cancel();
        _every10MinutesKHQR.cancel();
      });
    });
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: WillPopScope(
        onWillPop: () async{
          return false;
        },
        child: BlocListener<GetProfileUserBloc, GetProfileUserState>(
          listener: (context, state) {
            if(state is ProfileUserLoaded){
              setState(() {
                guardianId = state.profileusermodel.data!.guardian!.id.toString();
              });
            }
          },
          child: BlocListener<AbaDeepLinkBloc, AbaDeepLinkState>(
                  listener: (context, state) {
                    if (state is CreatePaymentABADeepLinkLoadingState) {
                      setState(() {
                        isLoading = true;
                      });
                    } else if (state is CreatePaymentABADeepLinkLoadedState) {
                      var data = state.abaDeepLinkPayment!.data;
                      String deepLink = data!.abapayDeeplink.toString();
                      String deepLinkDownAnd = data.playStore.toString();
                      String deepLinkDownIos = data.appStore.toString();
                      setState(() {
                        isLoading = false;
                        _launchURL(deepLink,deepLinkDownIos,deepLinkDownAnd);
                        saveStoragePref.saveRedirectApp(saveredirectapp: "saveredirectapp");
                      });
                      _every10MinutesPayWay = Timer.periodic(const Duration(seconds: 5), (Timer t) {
                        setState(() {
                              BlocProvider.of<CheckTransactionBloc>(context) .add(TransactionEvent(tranId:data.status!.tranId.toString(),guardianId:guardianId ));
                        });
                      });
                    } else {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  },
                  child: BlocListener<CreateKhqrBloc, CreateKhqrState>(
                    listener: (context, state) {
                      if(state is CreatePaymentKHQRLoadingState){
                        setState(() {
                          isLoading = true;
                        });
                      }
                      else if(state is CreatePaymentKHQRLoadedState){
                        var data = state.abaKHQRPayment!.data;
                        setState(() {
                          isLoading = false;
                          showKHQR = true;
                        });
                        String transactionId = data!.transactionid.toString();
                        location = data.location.toString();
                         _every10MinutesKHQR = Timer.periodic(const Duration(seconds: 4), (Timer t) {
                            setState(() {
                              BlocProvider.of<CheckTransactionBloc>(context) .add(TransactionEvent(tranId:transactionId,guardianId: guardianId));
                            });
                          });
                      }
                      else {
                        setState(() {
                          isLoading = false;
                        });
                      }
                    },
                    child: BlocListener<CheckTransactionBloc, CheckTransactionState>(
                      listener: (context, state) {
                        if(state is TransactionLoading){
                        }
                        else if(state is TransactionLoaded){
                          var data = state.transactionModel.data;
                          var status = state.transactionModel.status;
                          setState(() {
                            if(data !=null && data.status=="paid"){
                              Navigator.pushAndRemoveUntil(context,PageRouteBuilder(
                                        pageBuilder: (context, animation1, animation2) => PaymentCurrentScreen(monthPay: data.choosePayOptionName.toString(),expairDatePay: data.expiredAt.toString(),),
                                        transitionDuration: Duration.zero,
                                        reverseTransitionDuration: Duration.zero,),(route) => false);
                              _every10MinutesKHQR.cancel();
                              _every10MinutesPayWay.cancel();
                            }
                            else{
                              setState(() {
                                paymentfailed=true;
                              });
                            }
                          });
                        }
                        else{
                          setState(() {
                            _every10MinutesKHQR.cancel();
                            _every10MinutesPayWay.cancel();
                          });
                        }
                      },
                      child: Container(
                                decoration: const BoxDecoration(
                                    gradient: LinearGradient(
                                        begin: Alignment.topCenter,
                                        end: Alignment.bottomCenter,
                                        colors: [Colorconstands.primaryColor, Color(0xFF0A2B58)])),
                                child: Stack(
                                  children: [
                                    Positioned(
                                      top: 0,
                                      right: 0,
                                      child: Image.asset(
                                          "assets/images/Path_back_Upgrade_service.png"),
                                    ),
                                    Container(
                                      margin: const EdgeInsets.only(top: 32),
                                      child: Column(
                                        children: [
                                          Container(
                                            margin: const EdgeInsets.symmetric(
                                                vertical: 10, horizontal: 8),
                                            child: Row(
                                              children: [
                                                Container(
                                                  height: 45,
                                                  width: 45,
                                                  child: MaterialButton(
                                                    padding:const EdgeInsets.all(0),
                                                    splashColor: Colors.transparent,
                                                    highlightColor: Colors.transparent,
                                                    onPressed:(){
                                                      Navigator.of(context).pop();
                                                    },
                                                    child: const Icon(
                                                      Icons.arrow_back_ios_new_rounded,
                                                      size: 23,
                                                      color: Colorconstands.neutralWhite,
                                                    ),
                                                  ),
                                                ),
                                                Expanded(
                                                  child: Container(
                                                    child: Text(
                                                      "PAYMENT".tr(),
                                                      style: ThemeConstands.headline3_SemiBold_20
                                                          .copyWith(
                                                              color: Colorconstands.neutralWhite),
                                                      textAlign: TextAlign.center,
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  height: 45,
                                                  width: 45,
                                                  child: const Icon(
                                                    Icons.arrow_back_ios_new_rounded,
                                                    color: Colors.transparent,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 8,
                                          ),
                                          Expanded(
                                              child: Container(
                                            width: MediaQuery.of(context).size.width,
                                            padding: const EdgeInsets.only(
                                                top: 22, right: 0, left: 0, bottom: 0),
                                            decoration: const BoxDecoration(
                                              borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(15),
                                                  topRight: Radius.circular(15)),
                                              color: Colorconstands.white,
                                            ),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  margin:
                                                      const EdgeInsets.symmetric(horizontal: 18,),
                                                  child: Text(
                                                    "SELECTSERVICEPAYMENT".tr(),
                                                    style: ThemeConstands.headline4_SemiBold_18
                                                        .copyWith(color: Colorconstands.lightBlack),
                                                  ),
                                                ),
                                                Expanded(
                                                  child: SingleChildScrollView(
                                                    child: Container(
                                                      margin: const EdgeInsets.only(top: 22),
                                                      child: Column(
                                                        children: [
                                                          SelectBankButtonWidget(
                                                            onPressed: () async{
                                                              checkRedirectApp = await getStoragePref.getRediractApp();
                                                              setState(() {
                                                                showPayWay = true;
                                                                if(checkRedirectApp=="saveredirectapp"){
                                                                  BlocProvider.of<AbaDeepLinkBloc>(context) .add(
                                                                          GetCreatePaymentABADeepLinkEvent(
                                                                              typePayment:widget.typePayment!,guardianId: guardianId));
                                                                }
                                                                else{
                                                                  RedirectApp(
                                                                    context,(){
                                                                      saveStoragePref.saveRedirectApp(saveredirectapp: "saveredirectapp");
                                                                      Navigator.of(context).pop();
                                                                      BlocProvider.of<AbaDeepLinkBloc>(context) .add(
                                                                          GetCreatePaymentABADeepLinkEvent(
                                                                              typePayment: widget.typePayment!,guardianId: guardianId));
                                                                    }
                                                                  );
                                                                }
                                                              });
                                                              
                                                              Future.delayed(const Duration(seconds: 1), () async{
                                                                setState(() {
                                                                  _every10MinutesKHQR.cancel();
                                                                });
                                                              });
                                                            },
                                                            imageIcon:
                                                                "assets/icons/bank_icon/ic_ABA PAY.svg",
                                                            subTitle: "TAPTOPAYBANK".tr(),
                                                            title: "ABA PAY",
                                                          ),
                                                          Container(
                                                            margin: const EdgeInsets.symmetric(
                                                                horizontal: 18),
                                                            child: const Divider(
                                                              color: Colorconstands.gray,
                                                              height: 1,
                                                              thickness: 0.7,
                                                            ),
                                                          ),
                                                          SelectBankButtonWidget(
                                                            onPressed: () {
                                                              setState(() {
                                                                showPayWay= false;
                                                              });
                                                              BlocProvider.of<CreateKhqrBloc>(
                                                                      context)
                                                                  .add(GetCreatePaymentKHQREvent(
                                                                      typePayment:widget.typePayment!,guardianId: guardianId));
                                                              Future.delayed(const Duration(seconds: 1), () async{
                                                                setState(() {
                                                                  _every10MinutesPayWay.cancel();
                                                                });
                                                              });
                                                            },
                                                            imageIcon:
                                                                'assets/icons/bank_icon/KHQR.svg',
                                                            subTitle:
                                                                'SCANETOPAYBANK'.tr(),
                                                            title: 'KHQR',
                                                          ),
                                                          Container(
                                                            margin: const EdgeInsets.symmetric(
                                                                horizontal: 18),
                                                            child: const Divider(
                                                              color: Colorconstands.gray,
                                                              height: 1,
                                                              thickness: 0.7,
                                                            ),
                                                          ),
                                                          // SelectBankButtonWidget(
                                                          //   onPressed: () {},
                                                          //   imageIcon:
                                                          //       "assets/icons/bank_icon/dollar-circle.svg",
                                                          //   subTitle: "Apply redeem code",
                                                          //   title: "Redeem Code",
                                                          // ),
                                                          // Container(
                                                          //   margin: const EdgeInsets.symmetric(
                                                          //       horizontal: 18),
                                                          //   child: const Divider(
                                                          //     color: Colorconstands.gray,
                                                          //     height: 1,
                                                          //     thickness: 0.7,
                                                          //   ),
                                                          // ),
                                                          SelectBankButtonWidget(
                                                            onPressed: () {
                                                              Future.delayed(const Duration(seconds: 1), () async{
                                                                setState(() {
                                                                  _every10MinutesKHQR.cancel();
                                                                  _every10MinutesPayWay.cancel();
                                                                });
                                                              });
                                                              Navigator.push(
                                                                  context,
                                                                  MaterialPageRoute(
                                                                      builder: (context) =>
                                                                          CreatePaymentDetailScreen(
                                                                            id: widget.typePayment!,
                                                                          )));
                                                              
                                                            },
                                                            imageIcon:
                                                                'assets/icons/bank_icon/issue_invoice.svg',
                                                            subTitle:
                                                                'GENARATEINVOICE'.tr(),
                                                            title: 'ISSUEINVOICE'.tr(),
                                                          ),
                                                          Container(
                                                            margin: const EdgeInsets.symmetric(
                                                                horizontal: 18),
                                                            child: const Divider(
                                                              color: Colorconstands.gray,
                                                              height: 1,
                                                              thickness: 0.7,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ))
                                        ],
                                      ),
                                    ),
                                    connection == true
                                        ? Container()
                                        : Positioned(
                                            bottom: 0,
                                            left: 0,
                                            right: 0,
                                            top: 0,
                                            child: Container(
                                              color: const Color(0x7B9C9595),
                                            ),
                                          ),
                                    AnimatedPositioned(
                                      bottom: connection == true ? -150 : 0,
                                      left: 0,
                                      right: 0,
                                      duration: const Duration(milliseconds: 500),
                                      child: const NoConnectWidget(),
                                    ),
                                    isLoading == false
                                        ? Container()
                                        : Positioned(
                                            bottom: 0,
                                            left: 0,
                                            right: 0,
                                            top: 0,
                                            child: Container(
                                              color: const Color(0x7B9C9595),
                                              child: Center(
                                                child: Container(
                                                  padding: const EdgeInsets.all(16),
                                                  height: 60,
                                                  width: 60,
                                                  decoration: BoxDecoration(
                                                    color: Colorconstands.neutralGrey,
                                                    borderRadius: BorderRadius.circular(8),
                                                  ),
                                                  child: const Center(
                                                    child: CircularProgressIndicator(),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                    paymentfailed==false?Container():Positioned(
                                      top: 0,left: 0,right: 0,bottom: 0,
                                      child: Container(
                                        color: Colorconstands.neutralGrey.withOpacity(0.5),
                                        child: Center(
                                          child: Container(
                                            height: 160,
                                            width: 270,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(15),
                                              color: Colorconstands.neutralWhite
                                            ),
                                            child: Column(
                                              children: [
                                                Container(
                                                  padding:const EdgeInsets.only(top: 18,left: 18,right: 18),
                                                  child: Text("PAYMENTFAIL".tr(),style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.lightBlack),),
                                                ),
                                                Container(
                                                  padding:const EdgeInsets.only(top: 8,left: 18,right: 18),
                                                  child: Text("CANNOTTRANSFER".tr(),style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.lightBlack),textAlign: TextAlign.center,),
                                                ),
                                                Expanded(child: Container()),
                                                const Divider(height: 1,thickness: 1,),
                                                MaterialButton(
                                                   shape:const RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(15),bottomRight: Radius.circular(15)),
                                                    ),
                                                  height: 55,
                                                  padding:const EdgeInsets.all(0),
                                                  onPressed: (){
                                                    setState(() {
                                                      if(showPayWay==true){
                                                         _every10MinutesPayWay.cancel();
                                                      }
                                                      paymentfailed=false;
                                                    });
                                                  },
                                                    child: Center(child: Text("OK".tr(),style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.secondaryColor),)),
                                                )
        
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    showKHQR == true?Container(
                                      color: Colorconstands.neutralWhite,
                                        child: Column(
                                          children: [
                                            Container(
                                              padding:const EdgeInsets.only(top: 15),
                                              height: 100,
                                              width: MediaQuery.of(context).size.width,
                                              color:const Color(0xDFBDBDBD),
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    margin:const EdgeInsets.only(left: 18),
                                                    child: IconButton(
                                                      onPressed: () {
                                                        setState((){
                                                          _every10MinutesKHQR.cancel();
                                                          showKHQR=false;
                                                        });
                                                      },
                                                      icon:const Icon(Icons.close_rounded,color: Colorconstands.neutralWhite,size: 28,),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Expanded(
                                              child: WebView(
                                                initialUrl: location,
                                                backgroundColor: Colorconstands.neutralWhite,
                                                zoomEnabled:false,
                                                javascriptMode: JavascriptMode.unrestricted,
                                                onWebViewCreated: (WebViewController webViewController) {
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                        
                                    ):Container(height: 0,),
                                  ],
                                ),
                              ),
                    ),
                  ),
                ),
        ),
      ),
    );
  }
}
