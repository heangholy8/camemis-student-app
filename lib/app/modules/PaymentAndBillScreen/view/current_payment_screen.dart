import 'dart:async';
import 'package:animate_icons/animate_icons.dart';
import 'package:camis_application_flutter/app/core/resources/asset_resource.dart';
import 'package:camis_application_flutter/app/modules/PaymentAndBillScreen/bloc/current_payment_bloc.dart';
import 'package:camis_application_flutter/app/modules/PaymentAndBillScreen/view/payment_info_screen.dart';
import 'package:camis_application_flutter/app/modules/time_line/school_time_line/e.schooltimeline.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import '../../../../helper/route.export.dart';
import '../../../../widgets/button_widget/button_customwidget.dart';
import '../../../../widgets/no_connection_alert_widget.dart';
import '../../../routes/e.route.dart';
import '../widget/custom_card_title.dart';
class PaymentCurrentScreen extends StatefulWidget {
  final String monthPay;
  final String expairDatePay;
  const PaymentCurrentScreen({Key? key, this.monthPay = "", this.expairDatePay = ""}) : super(key: key);
  @override
  State<PaymentCurrentScreen> createState() => _PaymentCurrentScreenState();
}

class _PaymentCurrentScreenState extends State<PaymentCurrentScreen> with Toast {

  AnimateIconController? controller;

  double _currentOpacity = 1;
  StreamSubscription? internetconnection;
  bool isonline = true;
  double offset = 0;
  @override
  void initState() {
    setState(() {
      if(widget.monthPay!=""){
        paymentSecuess(context,widget.monthPay,widget.expairDatePay);
      }
    });
    internetconnection = Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
        // whenevery connection status is changed.
        if(result == ConnectivityResult.none){
             //there is no any connection
             setState(() {
                 isonline = false;
             }); 
        }else if(result == ConnectivityResult.mobile){
             //connection is mobile data network
             setState(() {
                isonline = true;
             });
        }else if(result == ConnectivityResult.wifi){
            //connection is from wifi
            setState(() {
               isonline = true;
            });
        }
    }); //
    BlocProvider.of<CurrentPaymentBloc>(context).add(GetCurrentPaymentEvent());
    super.initState();
  }
  @override
  dispose() {
    super.dispose();
    internetconnection!.cancel();
    //cancel internent connection subscription after you are done
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: WillPopScope(
        onWillPop: ()async{
          Navigator.pushNamedAndRemoveUntil(context,
                        Routes.MORE, (Route<dynamic> route) => false);
          return true;
        },
        child: Stack(
          children: [
             Positioned(
                top: 0,
                right: 0,
                child:
                    Image.asset("assets/images/Path_back_Upgrade_service.png"),
              ),
            Column(children: [
              Container(
                margin: const EdgeInsets.only(top: 40.0, bottom: 22.0,left: 8,right: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                        onPressed: () {
                            Navigator.pushNamedAndRemoveUntil(context,
                             Routes.MORE, (Route<dynamic> route) => false);
                        },
                        icon: const Icon(
                          Icons.arrow_back_ios_new,
                          color: Colorconstands.white,
                          size: 25,
                        )),
                    Text(
                      "PAYMENT".tr(),
                      style: ThemeConstands.headline3_SemiBold_20
                                    .copyWith(
                                        color: Colorconstands.neutralWhite),
                                textAlign: TextAlign.center,
                    ),
                    IconButton(
                        onPressed: () {
                          Navigator.pushNamed(context,
                                                  Routes.PAYMENT);
                        },
                        icon: const Icon(
                          Icons.timer_sharp,
                          color: Colorconstands.white,
                          size: 25,
                        )),
                  ],
                ),
              ),
              Expanded(
                  child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(15), topRight: Radius.circular(15)),
                  color: Colorconstands.neutralWhite,
                ),
                child: SingleChildScrollView(
                  physics: _currentOpacity == 1
                      ? const AlwaysScrollableScrollPhysics()
                      : const NeverScrollableScrollPhysics(),
                  child: Container(
                    padding: const EdgeInsets.only(top: 22, right: 18, left: 18),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        BlocBuilder<CurrentPaymentBloc, CurrentPaymentState>(
                          builder: (context, state) {
                            if (state is CurrentPaymentLoading) {
                              return ListView.builder(
                                shrinkWrap: true,
                                itemBuilder: (BuildContext contex, index) {
                                  return Container(
                                    child: const ShimmerAbsentRequest(),
                                  );
                                },
                                itemCount: 2,
                              );
                            }
                            if (state is CurrentPaymentLoaded) {
                              var currentPay = state.currentPayment!.data!.currentPayment;
                              var newPlan = state.currentPayment!.data!.newPlan;
                              return newPlan==null && currentPay==null
                                  ?  Center(
                                    child: DataNotFound(subtitle: '', title: 'EMPTY_PAYMETN'.tr(),),
                                    )
                                    :Column(
                                      children: [
                                        currentPay==null?Container(): Container(
                                          child: Container(
                                            decoration: BoxDecoration(
                                              gradient: const LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [
                                                Color.fromARGB(219, 97, 153, 231),
                                                Color.fromARGB(219, 57, 118, 203),
                                                 Color(0xFF3476D2),
                                              ]),
                                              borderRadius: BorderRadius.circular(16)
                                            ),
                                            width: MediaQuery.of(context).size.width,
                                            child: MaterialButton(
                                              padding:const EdgeInsets.all(0),
                                              onPressed: (){
                                                   BlocProvider.of<PaymentDetailBloc>(
                                                            context)
                                                        .add(GetPaymentDetailEvent(
                                                            paymentDetailId:currentPay.id!));
                                                    Navigator.of(context)
                                                        .push(_createPaymentDetialRoute());
                                              },
                                              child: Container(
                                                height: 130,
                                                width: MediaQuery.of(context).size.width,
                                                child: Stack(
                                                  children: [
                                                      Positioned(
                                                        right: -20,bottom: -30,
                                                        child: Container(
                                                          height: 139,width: 139,
                                                          decoration: BoxDecoration(
                                                            shape: BoxShape.circle,
                                                            color: Colorconstands.neutralWhite.withOpacity(0.2)
                                                          ),
                                                          child: Container(
                                                            padding:const EdgeInsets.only(top: 20,left: 35,right: 35,bottom: 30),
                                                            child: SvgPicture.asset(ImageAssets.card_tick_outline,),
                                                          ),
                                                        ),
                                                      ),
                                                      Container(
                                                        margin: const EdgeInsets.only(top: 18,left: 18,bottom: 18),
                                                        child: Column(
                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                          children: [
                                                            Container(
                                                              margin: const EdgeInsets.only(bottom: 13),
                                                              child: Text("CURRENT_PLAN".tr(),style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.neutralWhite),),
                                                            ),
                                                            Container(
                                                              margin: const EdgeInsets.only(bottom: 6),
                                                              child: Text("EXPIRED_DATE".tr(),style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralGrey),),
                                                            ),
                                                            Container(
                                                              child: Text(currentPay.expiredAt.toString(),style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.neutralWhite),),
                                                            ),
                                                          ],
                                                        ),
                                                      )
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        newPlan==null?Container(): Container(
                                          margin: const EdgeInsets.only(bottom: 13,top: 22),
                                          child: Row(
                                            children: [
                                              Expanded(child: Text("PAYMENT_HIS".tr(),style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.neutralDarkGrey),)),
                                            ],
                                          ),
                                        ),
                                        newPlan==null?Container(): Container(
                                          child: Container(
                                            margin: const EdgeInsets.symmetric(vertical: 10),
                                            decoration: BoxDecoration(
                                              color: newPlan.status == "paid"
                                                  ? Colorconstands.alertsPositive
                                                  : Colorconstands.alertsDecline,
                                              borderRadius: const BorderRadius.all(
                                                  Radius.circular(15)),
                                            ),
                                            child: Container(
                                              margin:
                                                  const EdgeInsets.only(left: 8),
                                              decoration: const BoxDecoration(
                                                  color: Colorconstands
                                                      .neutralSecondBackground,
                                                  borderRadius: BorderRadius.only(
                                                      topRight: Radius.circular(15),
                                                      bottomRight:
                                                          Radius.circular(15))),
                                              child: Stack(
                                                children: [
                                                Positioned(
                                                  right: 0,
                                                  child: Container(
                                                    padding:const EdgeInsets.symmetric(vertical: 6,horizontal: 12),
                                                    decoration: BoxDecoration(
                                                        color:
                                                            newPlan.status ==
                                                                    "paid"
                                                                ? Colorconstands.alertsPositive
                                                                : Colorconstands.alertsDecline,
                                                        borderRadius:
                                                            const BorderRadius.only(
                                                                topRight:
                                                                    Radius.circular(
                                                                        15),
                                                                bottomLeft:
                                                                    Radius.circular(
                                                                        15))),
                                                    alignment: Alignment.center,
                                                    child: Text(
                                                      newPlan.status ==
                                                              "paid"
                                                          ? "PAID".tr()
                                                          : "UNPAID".tr(),
                                                      style: ThemeConstands
                                                          .headline6_Medium_14.copyWith(
                                                              color: Colorconstands
                                                                  .neutralWhite,),
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  margin:
                                                      const EdgeInsets.symmetric(
                                                          vertical: 25,
                                                          horizontal: 12),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.start,
                                                    children: [
                                                      Expanded(
                                                          child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          customCardTile(
                                                            title:
                                                                "SERVICE_FEE".tr(),
                                                            value:translate=="km"? newPlan.choosePayOptionName.toString():newPlan.choosePayOptionNameEn.toString(),
                                                          ),
                                                          customCardTile(
                                                            title:
                                                                "PAYMENT_CODE".tr(),
                                                            value: newPlan
                                                                .invoiceNumber
                                                                .toString(),
                                                            isBold: true,
                                                          ),
                                                          newPlan.status.toString() !="null"
                                                            ? customCardTile(
                                                                title:
                                                                    "EXPIRED_DATE".tr(),
                                                                value: newPlan.expiredAt
                                                                    .toString(),
                                                              )
                                                            : Container(),

                                                          Row(
                                                            children: [
                                                              Container(
                                                                margin: const EdgeInsets.only(top: 12),
                                                                child: ButtonOutlineCustom(
                                                                  titleButton: "CHANGE_PLAN".tr(),
                                                                  buttonColor: Colorconstands.white,
                                                                  radiusButton: 10,
                                                                  onPressed: () {
                                                                   BlocProvider.of<PaymentOptionBloc>(context).add(GetPaymentOptionEvent());
                                                                    Navigator.pushNamed(
                                                                            context, Routes.PAYMENTOPTIONSCREEN);
                                                                  },
                                                                  hightButton: 45,
                                                                  maginRight: 10,
                                                                  titlebuttonColor: Colorconstands.primaryColor,
                                                                  borderColor: Colorconstands.primaryColor,
                                                                  sizeText: 16,
                                                                  child: const SizedBox(),
                                                                ),
                                                              ),
                                                              newPlan.status=="Paid"?Container(): Expanded(
                                                                child: Container(
                                                                  margin: const EdgeInsets.only(top: 12),
                                                                  child: Button_Custom(
                                                                      titleButton: "PAIDED".tr(),
                                                                      onPressed: () {
                                                                        BlocProvider.of<PaymentDetailBloc>(context).add(GetPaymentDetailEvent(paymentDetailId:newPlan.id!));
                                                                      Navigator.of(context)
                                                                          .push(_createPaymentDetialRoute());
                                                                      },
                                                                      buttonColor: Colorconstands.primaryColor,
                                                                      hightButton: 45,
                                                                      radiusButton: 12,
                                                                      titlebuttonColor: Colorconstands.neutralWhite,
                                                                    ),
                                                                ),
                                                              )
                                                            ],
                                                          )
                                                        ],
                                                      )),
                                                    ],
                                                  ),
                                                ),
                                               ])
                                            )
                                          )
                                        )
                                      ],
                                    );
                            } else {
                              return _buildLoading();
                            }
                          },
                        )
                      ],
                    ),
                  ),
                ),
              ))
            ]),
            Positioned(
              bottom: 25,left: 22,right: 22,
              child: Container(
                  child: Button_Custom(
                    titleButton: "BUY_SERVICE".tr(),
                    buttonColor: Colorconstands.primaryColor,
                    radiusButton: 10,
                    onPressed: () {
                    BlocProvider.of<PaymentOptionBloc>(context).add(GetPaymentOptionEvent());
                      Navigator.pushNamed(
                              context, Routes.PAYMENTOPTIONSCREEN);
                    },
                    hightButton: 55,
                    maginRight: 10,
                    titlebuttonColor: Colorconstands.neutralWhite,
                  ),
                ),
            ),
            isonline == true
                ? Container()
                : Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    top: 0,
                    child: Container(
                      color: const Color(0x7B9C9595),
                    ),
                  ),
            AnimatedPositioned(
              bottom: isonline == true ? -150 : 0,
              left: 0,
              right: 0,
              duration: const Duration(milliseconds: 500),
              child: const NoConnectWidget(),
            ),

          ],
        ),
      ),
    );
  }

  Widget _buildLoading() => Container(
      margin: const EdgeInsets.only(right: 10.0),
      child: Container(
        child: const Center(
               child: CircularProgressIndicator(),
            ),
      ));
}

Route _createPaymentDetialRoute() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) =>
        const PaymentInfoScreen(),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = const Offset(0.0, 1.0);
      var end = Offset.zero;
      var curve = Curves.ease;

      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}
