import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'package:camis_application_flutter/app/modules/PaymentAndBillScreen/bloc/bloc/create_payment_bloc.dart';
import 'package:camis_application_flutter/app/modules/PaymentAndBillScreen/widget/create_payment_info.dart';
import 'package:camis_application_flutter/model/create_payment_model/create_payment_model.dart';
import 'package:camis_application_flutter/service/apis/profile_user/bloc/get_profile_user_bloc.dart';
import 'package:camis_application_flutter/widgets/button_widget/button_customwidget.dart';
import 'package:camis_application_flutter/widgets/shimmer_style.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:path_provider/path_provider.dart';
import 'package:screenshot/screenshot.dart';
import 'package:share_plus/share_plus.dart';
import '../../../../storages/get_storage.dart';
import '../../../../widgets/no_connection_alert_widget.dart';
import '../../../routes/e.route.dart';
import '../controller/save_image_controller.dart';
import '../widget/custom_bottom_noted.dart';
import '../widget/custom_content_title.dart';

class CreatePaymentDetailScreen extends StatefulWidget {
  final int id;
  const CreatePaymentDetailScreen({
    Key? key,
    required this.id,
  }) : super(key: key);

  @override
  State<CreatePaymentDetailScreen> createState() =>
      _CreatePaymentDetailScreenState();
}

class _CreatePaymentDetailScreenState extends State<CreatePaymentDetailScreen> {
  final screenShotColler = ScreenshotController();
  bool isWaiting = false;
  CreatePaymentModel payment = CreatePaymentModel();
  String? schoolName;
  StreamSubscription? sub;
  bool connection = true;
  
  @override
  void initState() {
    super.initState();
    setState(() {
      //=============Check internet====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
           connection = (event != ConnectivityResult.none);
        });
      });
    });
     //=============Eend Check internet====================
    getAuthModel();
    BlocProvider.of<GetProfileUserBloc>(context).add(GetProfileUser());
  }
  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    var translate = context.locale.toString();
    return Scaffold(
      backgroundColor:const Color(0xFF0B2A56),
      body: Stack(
        children: [
          Positioned(
            top: 0,
            right: 0,
            child: Image.asset("assets/images/Path_back_Upgrade_service.png"),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Image.asset("assets/images/Oval_detail_payment.png"),
          ),
          Container(
            margin: const EdgeInsets.only(
              top: 28,
            ),
            padding: const EdgeInsets.only(bottom: 60),
            child: BlocListener<GetProfileUserBloc, GetProfileUserState>(
              listener: (context, state) {
                if(state is ProfileUserLoaded){
                     BlocProvider.of<CreatePaymentBloc>(context).add(
                      GetCreatePaymentEvent(
                        chooseOptionPayment: widget.id,
                        guardianId: state.profileusermodel.data!.guardian!.id.toString()
                      ),
                    );
                }
              },
              child: Column(
                          children: [
                            Row(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  margin: const EdgeInsets.symmetric(
                                      vertical: 10, horizontal: 18),
                                  height: 45,
                                  width: 45,
                                  child: GestureDetector(
                                    onTap: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: const Icon(
                                      Icons.close_rounded,
                                      size: 25,
                                      color: Colorconstands.neutralWhite,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            BlocBuilder<CreatePaymentBloc, CreatePaymentState>(
                              builder: (context, state) {
                                if (state is CreatePaymentLoadingState) {
                                   return const ShimmerDetailPayment();
                                }
                                if (state is CreatePaymentLoadedState) {
                                  var detial = state.currentCreatePayment.data!;
                                return Expanded(
                                  child: SingleChildScrollView(
                                    child: Column(
                                      children: [
                                        Container(
                                          margin: const EdgeInsets.symmetric(
                                              vertical: 10, horizontal: 22),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: Container(
                                                  child: Text(
                                                    "PAYMENT_INFORMATION".tr(),
                                                    style: ThemeConstands
                                                        .headline3_SemiBold_20
                                                        .copyWith(
                                                            color: Colorconstands
                                                                .neutralWhite),
                                                    textAlign: TextAlign.left,
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                padding:const EdgeInsets.symmetric(horizontal:12,vertical: 5),
                                                decoration: BoxDecoration(
                                                  color: Colorconstands.alertsNotifications,
                                                  borderRadius: BorderRadius.circular(8)
                                                ),
                                                child: Row(
                                                  children: [
                                                    Text("UNPAID".tr(),style: ThemeConstands.headline6_Medium_14.copyWith(color: Colorconstands.neutralWhite),)
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        const SizedBox(height: 12,),
                                        SingleChildScrollView(
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      width: MediaQuery.of(context)
                                                          .size
                                                          .width,
                                                      padding: const EdgeInsets.only(
                                                          top: 15, right: 15, left: 15),
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius.circular(15),
                                                        color: Colorconstands
                                                            .primaryBackgroundColor,
                                                      ),
                                                      child: CreatePaymentInfoBody(
                                                        detial: state.currentCreatePayment.data!,
                                                        isWait: isWaiting,
                                                        isScreenShot: true,
                                                      ),
                                                    ),
                                                    Container(
                                                      margin:const EdgeInsets.symmetric(vertical: 22),
                                                      child: Row(
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        children: [
                                                          Container(
                                                            child: Column(
                                                              children: [
                                                                MaterialButton(
                                                                  color:const Color(0x245F90BB).withOpacity(0.5),
                                                                  padding:const EdgeInsets.all(12),
                                                                  shape: const CircleBorder(),
                                                                    onPressed: isWaiting
                                                                        ? null
                                                                        : () async {
                                                                            try {
                                                                              setState(() {
                                                                                isWaiting = true;
                                                                              });
                                                                              final image = await screenShotColler.captureFromWidget(
                                                                                  Container(color: Colorconstands.backgroundColor,
                                                                                padding: EdgeInsets.only(
                                                                                    top: MediaQuery.of(context).size.height /100,
                                                                                    right: MediaQuery.of(context).size.width /80,
                                                                                    left: MediaQuery.of(context).size.width /80),
                                                                                child: Stack(
                                                                                  alignment: AlignmentDirectional.center,
                                                                                  children: [
                                                                                    Container(
                                                                                      padding: const EdgeInsets.symmetric(vertical:10,horizontal: 12),
                                                                                      child: Column(
                                                                                        mainAxisSize:MainAxisSize.min,
                                                                                        crossAxisAlignment:CrossAxisAlignment.start,
                                                                                        children: [
                                                                                          detial.paidAt.toString() != "null"
                                                                                              ? CustomContenTitle(
                                                                                                  titile: "DATE".tr(),
                                                                                                  value: detial.paidAt.toString() != "null" ? detial.paidAt.toString() : "",
                                                                                                )
                                                                                              : Container(),
                                                                                          
                                                                                          const Divider(
                                                                                            thickness:1,
                                                                                            height: 40,
                                                                                          ),
                                                                                          CustomContenTitle(
                                                                                              titile: "SCHOOL".tr(),
                                                                                              value:schoolName.toString()),
                                                                                          Container(
                                                                                            padding: const EdgeInsets.symmetric(vertical:5, horizontal:10),
                                                                                            decoration: const BoxDecoration(
                                                                                                borderRadius: BorderRadius.all(Radius.circular(10)),
                                                                                                gradient: LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [
                                                                                                  Colorconstands.primaryColor,
                                                                                                  Colorconstands.secondaryColor,
                                                                                                ])),
                                                                                            child: Column(
                                                                                              children: [
                                                                                                CustomContenTitle(
                                                                                                  titile: "Price".tr(),
                                                                                                  value: detial.amount.toString() != "null" ? detial.amount.toString() + (detial.currency.toString() == "KHR" ? " រៀល" : " USD") : "- - -",
                                                                                                  isBlack: false,
                                                                                                  isbold: true,
                                                                                                ),
                                                                                                CustomContenTitle(
                                                                                                  titile: "SERVICE_FEE".tr(),
                                                                                                  value: detial.choosePayOptionName.toString() != "null" ?translate=="km"? detial.choosePayOptionName.toString():detial.choosePayOption.toString() : "- - -",
                                                                                                  isBlack: false,
                                                                                                  isbold: true,
                                                                                                ),
                                                                                                CustomContenTitle(
                                                                                                  titile: "EXPIRED_DATE".tr(),
                                                                                                  value: detial.expiredAt.toString() != "null" ? detial.expiredAt.toString() : "- - -",
                                                                                                  isBlack: false,
                                                                                                  isbold: true,
                                                                                                ),
                                                                                                CustomContenTitle(
                                                                                                  titile: "STATUS_PAYMENT".tr(),
                                                                                                  value: detial.status.toString() == "paid" ? "PAID".tr() : "UNPAID".tr(),
                                                                                                  isBlack: false,
                                                                                                  isbold: true,
                                                                                                ),
                                                                                                CustomContenTitle(
                                                                                                  titile: "PAYMENT_CODE".tr(),
                                                                                                  value: detial.invoiceNumber.toString() != "null" ? detial.invoiceNumber.toString() : "- - -",
                                                                                                  isBlack: false,
                                                                                                  isbold: true,
                                                                                                ),
                                                                                              ],
                                                                                            ),
                                                                                          ),
                                                                                          const SizedBox(
                                                                                            height:10,
                                                                                          ),
                                                                                          const CustomButtomNoted()
                                                                                        ],
                                                                                      ),
                                                                                    ),
                                                                                  ],
                                                                                ),
                                                                              ));
                                                                              setState(() {
                                                                                isWaiting = false;
                                                                              });
                                                                              try {
                                                                                await saveImageToLocal(image);
                                                                                ScaffoldMessenger.of(context).showSnackBar( SnackBar(content:Text("SAVE_IMAGE_SUCCESS".tr())));
                                                                              } catch (e) {
                                                                                ScaffoldMessenger.of(context).showSnackBar( SnackBar(content:Text("SAVE_IMAGE_UNSUCCESS".tr())));
                                                                              }
                                                                            } catch (e) {
                                                                              ScaffoldMessenger.of(context).showSnackBar( SnackBar(content: Text("SAVE_IMAGE_UNSUCCESS".tr())));
                                                                              print(e);
                                                                            }
                                                                          },
                                                                    child:const Icon(Icons.download_for_offline_outlined,size: 25,color:Colorconstands.neutralWhite)
                                                                  ),
                                                                  const SizedBox(height: 12,),
                                                                  Text("SAVE".tr(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.neutralWhite),),
                                                              ],
                                                            ),
                                                          ),
                                                          Container(
                                                            child: Column(
                                                              children: [
                                                                MaterialButton(
                                                                  color:const Color(0x245F90BB).withOpacity(0.5),
                                                                  padding:const EdgeInsets.all(12),
                                                                  shape: const CircleBorder(),
                                                                    onPressed: isWaiting
                                                                        ? null
                                                                        : () async {
                                                                            try {
                                                                              setState(() {
                                                                                isWaiting = true;
                                                                              });
                                                                              final image = await screenShotColler.captureFromWidget(
                                                                                  Container(color: Colorconstands.backgroundColor,
                                                                                padding: EdgeInsets.only(
                                                                                    top: MediaQuery.of(context).size.height /100,
                                                                                    right: MediaQuery.of(context).size.width /80,
                                                                                    left: MediaQuery.of(context).size.width /80),
                                                                                child: Stack(
                                                                                  alignment: AlignmentDirectional.center,
                                                                                  children: [
                                                                                    Container(
                                                                                      padding: const EdgeInsets.symmetric(vertical:10,horizontal: 12),
                                                                                      child: Column(
                                                                                        mainAxisSize:MainAxisSize.min,
                                                                                        crossAxisAlignment:CrossAxisAlignment.start,
                                                                                        children: [
                                                                                          detial.paidAt.toString() != "null"
                                                                                              ? CustomContenTitle(
                                                                                                  titile: "DATE".tr(),
                                                                                                  value: detial.paidAt.toString() != "null" ? detial.paidAt.toString() : "",
                                                                                                )
                                                                                              : Container(),
                                                                                          
                                                                                          const Divider(
                                                                                            thickness:1,
                                                                                            height: 40,
                                                                                          ),
                                                                                          CustomContenTitle(
                                                                                              titile: "SCHOOL".tr(),
                                                                                              value:schoolName.toString()),
                                                                                          Container(
                                                                                            padding: const EdgeInsets.symmetric(vertical:5, horizontal:10),
                                                                                            decoration: const BoxDecoration(
                                                                                                borderRadius: BorderRadius.all(Radius.circular(10)),
                                                                                                gradient: LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [
                                                                                                  Colorconstands.primaryColor,
                                                                                                  Colorconstands.secondaryColor,
                                                                                                ])),
                                                                                            child: Column(
                                                                                              children: [
                                                                                                CustomContenTitle(
                                                                                                  titile: "Price".tr(),
                                                                                                  value: detial.amount.toString() != "null" ? detial.amount.toString() + (detial.currency.toString() == "KHR" ? " រៀល" : " USD") : "- - -",
                                                                                                  isBlack: false,
                                                                                                  isbold: true,
                                                                                                ),
                                                                                                CustomContenTitle(
                                                                                                  titile: "SERVICE_FEE".tr(),
                                                                                                  value: detial.choosePayOptionName.toString() != "null" ?translate=="km"? detial.choosePayOptionName.toString():detial.choosePayOption.toString() : "- - -",
                                                                                                  isBlack: false,
                                                                                                  isbold: true,
                                                                                                ),
                                                                                                CustomContenTitle(
                                                                                                  titile: "EXPIRED_DATE".tr(),
                                                                                                  value: detial.expiredAt.toString() != "null" ? detial.expiredAt.toString() : "- - -",
                                                                                                  isBlack: false,
                                                                                                  isbold: true,
                                                                                                ),
                                                                                                CustomContenTitle(
                                                                                                  titile: "STATUS_PAYMENT".tr(),
                                                                                                  value: detial.status.toString() == "paid" ? "PAID".tr() : "UNPAID".tr(),
                                                                                                  isBlack: false,
                                                                                                  isbold: true,
                                                                                                ),
                                                                                                CustomContenTitle(
                                                                                                  titile: "PAYMENT_CODE".tr(),
                                                                                                  value: detial.invoiceNumber.toString() != "null" ? detial.invoiceNumber.toString() : "- - -",
                                                                                                  isBlack: false,
                                                                                                  isbold: true,
                                                                                                ),
                                                                                              ],
                                                                                            ),
                                                                                          ),
                                                                                          const SizedBox(
                                                                                            height:10,
                                                                                          ),
                                                                                          const CustomButtomNoted()
                                                                                        ],
                                                                                      ),
                                                                                    ),
                                                                                  ],
                                                                                ),
                                                                              ));
                                                                              setState(() {
                                                                                isWaiting = false;
                                                                              });
                                                                              await shareImage(image);
                                                                            } catch (e) {
                                                                            }
                                                                          },
                                                                    child:const Icon(Icons.share_outlined,size: 25,color:Colorconstands.neutralWhite)
                                                                  ),
                                                                  const SizedBox(height: 12,),
                                                                  Text("SHARE_PIC".tr(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.neutralWhite),),
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                      ],
                                    ),
                                  ),
                                );
                               }
                               else {
                                return _buildLoading();
                              }
                              },
                            )
                          ],
                        ),
            ),
          ),
          BlocBuilder<CreatePaymentBloc, CreatePaymentState>(
          builder: (context, state) {
            if (state is CreatePaymentLoadingState) {
              return Container();
            }
            if (state is CreatePaymentLoadedState) {
              return Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Button_Custom(
                  titleButton: "FINISH".tr(),
                  onPressed: () {
                     Navigator.pushNamedAndRemoveUntil(context,
                        Routes.CURRENTPAYMENTSCREEN, (Route<dynamic> route) => false);
                  },
                  buttonColor: Colorconstands.primaryColor,
                  hightButton: 60,
                  radiusButton: 0,
                  titlebuttonColor: Colorconstands.neutralWhite,
                ),
              );
            }
            else{
               return Container();
            }
           }
          ),
          connection==true?Container():Positioned(
                bottom: 0,left: 0,right: 0,top: 0,
                child: Container(
                  color:const Color(0x7B9C9595),
                ),
              ),
          AnimatedPositioned(
                bottom:connection==true? -150:0,left: 0,right: 0,
                duration:const Duration(milliseconds: 500), 
                child:const NoConnectWidget(),
              ),
        ],
      ),
    );
  }

  void _onChangeData(CreatePaymentModel data) {
    setState(() {
      payment = data;
    });
  }

  Future shareImage(Uint8List bytes) async {
    final directory = await getApplicationDocumentsDirectory();
    final image = File('${directory.path}/flutter.png');
    image.writeAsBytesSync(bytes);

    await Share.shareFiles([image.path]);
  }

  Widget _buildLoading() => Container(
      margin: const EdgeInsets.only(right: 10.0),
      child: Center(
          child: Image.asset(
        "assets/images/gifs/loading.gif",
        height: 45,
        width: 45,
      )));
  void getAuthModel() async {
    GetStoragePref _pref = GetStoragePref();
    var authToken = await _pref.getJsonToken;
    setState(() {
      schoolName = authToken.schoolName;
    });
  }
}
