import 'dart:async';
import 'package:animate_icons/animate_icons.dart';
import 'package:camis_application_flutter/app/modules/PaymentAndBillScreen/view/payment_info_screen.dart';
import 'package:camis_application_flutter/app/modules/PaymentAndBillScreen/view/selected_payment_option.dart';
import 'package:camis_application_flutter/widgets/shimmer_style.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../routes/e.route.dart';
import '../bloc/payment_bloc_bloc.dart';
import '../widget/custom_card_title.dart';

class PaymentAndBillScreen extends StatefulWidget {
  const PaymentAndBillScreen({Key? key}) : super(key: key);

  @override
  State<PaymentAndBillScreen> createState() => _PaymentAndBillScreenState();
}

class _PaymentAndBillScreenState extends State<PaymentAndBillScreen> {
  AnimateIconController? controller;

  double _currentOpacity = 1;

  StreamSubscription? stsub;
  bool connection = true;

  @override
  void initState() {
    //=============Check internet====================
    stsub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
      });
    });

    //=============Check internet====================
    controller = AnimateIconController();
    BlocProvider.of<PaymentBlocBloc>(context).add(GetPaymentEvent());
    super.initState();
  }

  @override
  void dispose() {
    stsub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: Stack(
        children: [
          Positioned(
            top: 0,
            right: 0,
            child: Image.asset("assets/images/Path_back_Upgrade_service.png"),
          ),
          Column(children: [
            Container(
              margin: const EdgeInsets.only(
                  top: 40.0, bottom: 22.0, left: 8, right: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: const Icon(
                        Icons.arrow_back_ios_new,
                        color: Colorconstands.white,
                        size: 25,
                      )),
                  Text(
                    "PAYMENT_HIS".tr(),
                    style: ThemeConstands.headline3_SemiBold_20
                        .copyWith(color: Colorconstands.neutralWhite),
                    textAlign: TextAlign.center,
                  ),
                  const Icon(
                    Icons.arrow_back_ios_new,
                    color: Colors.transparent,
                    size: 25,
                  )
                ],
              ),
            ),
            Expanded(
                child: Container(
              width: MediaQuery.of(context).size.width,
              padding: const EdgeInsets.only(top: 15, right: 15, left: 15),
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15)),
                color: Colorconstands.neutralWhite,
              ),
              child: SingleChildScrollView(
                physics: _currentOpacity == 1
                    ? const AlwaysScrollableScrollPhysics()
                    : const NeverScrollableScrollPhysics(),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    BlocBuilder<PaymentBlocBloc, PaymentBlocState>(
                      builder: (context, state) {
                        if (state is PaymentBlocLoadingState) {
                          return ListView.builder(
                            shrinkWrap: true,
                            itemBuilder: (BuildContext contex, index) {
                              return Container(
                                child: const ShimmerAbsentRequest(),
                              );
                            },
                            itemCount: 4,
                          );
                        }
                        if (state is PaymentBlocLoadedState) {
                          var history = state.currentPaymentBloc!.data!;
                          return history.isNotEmpty
                              ? ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  padding: const EdgeInsets.all(0),
                                  shrinkWrap: true,
                                  itemCount: history.length,
                                  itemBuilder: (context, index) {
                                    return InkWell(
                                      // onTap: () {
                                      //   BlocProvider.of<PaymentDetailBloc>(
                                      //           context)
                                      //       .add(GetPaymentDetailEvent(
                                      //           paymentDetailId:
                                      //               history[index].id!));
                                      //   Navigator.of(context)
                                      //       .push(_createPaymentDetialRoute());
                                      // },
                                      child: Container(
                                        margin: const EdgeInsets.symmetric(
                                            vertical: 10),
                                        decoration: BoxDecoration(
                                          color: history[index].status == "paid"
                                              ? Colorconstands.alertsPositive
                                              : Colorconstands.alertsDecline,
                                          borderRadius: const BorderRadius.all(
                                              Radius.circular(15)),
                                        ),
                                        child: Container(
                                          margin:
                                              const EdgeInsets.only(left: 8),
                                          decoration: const BoxDecoration(
                                              color: Colorconstands
                                                  .neutralSecondBackground,
                                              borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(15),
                                                  bottomRight:
                                                      Radius.circular(15))),
                                          child: Stack(children: [
                                            Positioned(
                                              right: 0,
                                              child: Container(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        vertical: 6,
                                                        horizontal: 12),
                                                decoration: BoxDecoration(
                                                    color:
                                                        history[index].status ==
                                                                "paid"
                                                            ? Colorconstands
                                                                .alertsPositive
                                                            : Colorconstands
                                                                .alertsDecline,
                                                    borderRadius:
                                                        const BorderRadius.only(
                                                            topRight:
                                                                Radius.circular(
                                                                    15),
                                                            bottomLeft:
                                                                Radius.circular(
                                                                    15))),
                                                // ignore: sort_child_properties_last
                                                child: Text(
                                                  history[index].status ==
                                                          "paid"
                                                      ? "PAID".tr()
                                                      : "UNPAID".tr(),
                                                  style: ThemeConstands
                                                      .headline6_Medium_14
                                                      .copyWith(
                                                    color: Colorconstands
                                                        .neutralWhite,
                                                  ),
                                                ),
                                                alignment: Alignment.center,
                                              ),
                                            ),
                                            Container(
                                              margin:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 25,
                                                      horizontal: 12),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Expanded(
                                                      child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      customCardTile(
                                                        title:
                                                            "SERVICE_FEE".tr(),
                                                        value: translate == "en"
                                                            ? history[index]
                                                                .choosePayOptionNameEn
                                                                .toString()
                                                            : history[index]
                                                                .choosePayOptionName
                                                                .toString(),
                                                      ),
                                                      customCardTile(
                                                        title: translate == "km"
                                                            ? "តម្លៃ"
                                                            : "Service Fee",
                                                        value: history[index]
                                                                .amount
                                                                .toString() +
                                                            (history[index]
                                                                        .currency
                                                                        .toString() ==
                                                                    "KHR"
                                                                ? " រៀល"
                                                                : " USD"),
                                                      ),
                                                      history[index]
                                                                  .expiredAt
                                                                  .toString() !=
                                                              "null"
                                                          ? customCardTile(
                                                              title:
                                                                  "EXPIRED_DATE"
                                                                      .tr(),
                                                              value: history[
                                                                      index]
                                                                  .expiredAt
                                                                  .toString(),
                                                            )
                                                          : Container(),
                                                      customCardTile(
                                                        title:
                                                            "PAYMENT_CODE".tr(),
                                                        value: history[index]
                                                            .invoiceNumber
                                                            .toString(),
                                                        isBold: true,
                                                      ),
                                                    ],
                                                  )),
                                                ],
                                              ),
                                            )
                                          ]),
                                        ),
                                      ),
                                    );
                                  },
                                )
                              : Center(
                                  child: Column(
                                    children: [
                                      Image.asset("assets/images/no_schedule_image.png",
                                        height:
                                            MediaQuery.of(context).size.width /
                                                1.7,
                                        width:
                                            MediaQuery.of(context).size.width /
                                                1.7,
                                      ),
                                      Text(
                                        "Empty Payment History",
                                        style: ThemeConstands
                                           .headline6_Regular_14_20height
                                            .copyWith(
                                                fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                );
                        } else {
                          return _buildLoading();
                        }
                      },
                    )
                  ],
                ),
              ),
            ))
          ]),
        ],
      ),
    );
  }

  Widget _buildLoading() => Container(
      margin: const EdgeInsets.only(right: 10.0),
      child: Container(
        child: const Center(
          child: CircularProgressIndicator(),
        ),
      ));
}

Route _createPaymentDetialRoute() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) =>
        const PaymentInfoScreen(),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = const Offset(0.0, 1.0);
      var end = Offset.zero;
      var curve = Curves.ease;

      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}
