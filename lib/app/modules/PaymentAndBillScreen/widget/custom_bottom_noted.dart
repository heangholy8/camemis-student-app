import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';

import '../../../core/resources/asset_resource.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class CustomButtomNoted extends StatelessWidget {
  const CustomButtomNoted({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            SvgPicture.asset(ImageAssets.error_line,width: 22,),
            const SizedBox(
              width: 5,
            ),
            Text(
              "MARK_FOR".tr(),
              style: ThemeConstands.texttheme.subtitle2?.copyWith(
                  fontWeight: FontWeight.bold, color: Colorconstands.black),
            )
          ],
        ),
        const SizedBox(height: 18,),
        Container(
          padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
          decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(10)),
              color: Colorconstands.gray.withOpacity(0.3)),
          child: Column(
            children: [
              Text(
                "IN_FOR_PAYMENT_PLEACE".tr(),
                style: ThemeConstands.texttheme.subtitle1
                    ?.copyWith(height: 2, color: Colorconstands.black),
              ),
              const SizedBox(
                height: 10,
              ),
              // Row(
              //   children: [
              //     Image.asset(ImageAssets.emoney_png),
              //     const SizedBox(
              //       width: 15,
              //     ),
              //     Image.asset(ImageAssets.wing_png),
              //     const SizedBox(
              //       width: 15,
              //     ),
              //     Image.asset(ImageAssets.true_money_png)
              //   ],
              // )
            ],
          ),
        ),
      ],
    );
  }
}
