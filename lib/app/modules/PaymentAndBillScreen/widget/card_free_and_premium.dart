import 'package:camis_application_flutter/app/modules/PaymentAndBillScreen/bloc/bloc/check_payment_expire_bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';

import '../../../core/resources/asset_resource.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class CardFreeAndPremium extends StatefulWidget {
  const CardFreeAndPremium({Key? key}) : super(key: key);

  @override
  State<CardFreeAndPremium> createState() => _CardFreeAndPremiumState();
}

class _CardFreeAndPremiumState extends State<CardFreeAndPremium> {
  int pageViewCheck = 1;
  bool paymentExpair = false;
  PageController? pagecInfomationFree;
  @override
  void initState() {
    BlocProvider.of<CheckPaymentExpireBloc>(context).add(CheckPaymentEvent());
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
     pagecInfomationFree = PageController(viewportFraction: 0.92,initialPage: 1);
    return BlocListener<CheckPaymentExpireBloc, CheckPaymentExpireState>(
      listener: (context, state) {
        if(state is CheckPaymentExpireLoaded){
          var data = state.checkpayment!.status;
          setState(() {
            paymentExpair = data!;
          });
        }
      },
      child: Container(
          margin:const EdgeInsets.only(top: 8,bottom: 22,),
          height: 255,
          child: PageView.builder(
            controller: pagecInfomationFree,
            scrollDirection: Axis.horizontal,
            onPageChanged: (value){
              setState(() {
                pageViewCheck = value;
              });
            },
            itemCount: 2,
            itemBuilder: (context,index){
              return Container(
                margin:EdgeInsets.only(right: pageViewCheck==0?12:0,left: pageViewCheck==1?12:0),
                decoration: BoxDecoration(
                  color: index==0? Colorconstands.neutralWhite:Colorconstands.mainColorForecolor,
                  borderRadius: BorderRadius.circular(16)
                ),
                child: Container(
                  padding:const EdgeInsets.only(top:22.0,left: 18,right: 18,bottom: 22),
                  child: Column(
                    children: [
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: Text(index==0?"FREE".tr():"PREMIUM".tr(),style: ThemeConstands.headline3_SemiBold_20.copyWith(color: index==0?Colorconstands.primaryColor:Colorconstands.neutralWhite),),
                            ),
                            paymentExpair==false?index==1?Container():Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  border: Border.all(color: Colorconstands.primaryColor,width: 2),
                              ),
                              child: Container(
                                padding:const EdgeInsets.symmetric(vertical: 4,horizontal: 10),
                                child: Row(
                                  children: [
                                    const Icon(Icons.check_circle_rounded,color: Colorconstands.primaryColor,size: 18,),
                                    const SizedBox(width: 8,),
                                    Text("CURRENTPLAN".tr(),style: ThemeConstands.headline6_Medium_14.copyWith(color: Colorconstands.primaryColor),)
                                  ],
                                ),
                              ),
                            ):index==0?Container():Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  border: Border.all(color: Colorconstands.neutralWhite,width: 2),
                              ),
                              child: Container(
                                padding:const EdgeInsets.symmetric(vertical: 4,horizontal: 10),
                                child: Row(
                                  children: [
                                    const Icon(Icons.check_circle_rounded,color: Colorconstands.neutralWhite,size: 18,),
                                    const SizedBox(width: 8,),
                                    Text("CURRENTPLAN".tr(),style: ThemeConstands.headline6_Medium_14.copyWith(color: Colorconstands.neutralWhite),)
                                  ],
                                ),
                              )),
                          ],
                        ),
                      ),
                      const SizedBox(height: 12,),
                      Container(
                        margin:const EdgeInsets.symmetric(vertical: 8),
                        child: Row(
                          children: [
                            Container(
                              child: SvgPicture.asset(index==0?ImageAssets.one_peaple_icon:ImageAssets.people_icon,width: 25,color: index==1?Colorconstands.neutralWhite:Colorconstands.primaryColor,),
                            ),
                            const SizedBox(width: 10,),
                            Expanded(
                              child: Container(
                                child: Text(index==0?"ONLY_CHILD".tr():"UNLIMITTED_CHILD".tr(),style: ThemeConstands.headline5_Medium_16.copyWith(color: index==1?Colorconstands.neutralWhite:Colorconstands.primaryColor),textAlign: TextAlign.left,),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin:const EdgeInsets.symmetric(vertical: 8),
                        child: Row(
                          children: [
                            Container(
                              child: SvgPicture.asset(ImageAssets.one_peaple_check_icon,color: index==0?Colorconstands.primaryColor:Colorconstands.neutralWhite,),
                            ),
                            const SizedBox(width: 10,),
                            Expanded(
                              child: Container(
                                child: Text(index==0?"LIMITED_CHILD_ATTENDANCE".tr():"UNLIMITED_CHILD_ATTENDANCE".tr(),style: ThemeConstands.headline5_Medium_16.copyWith(color: index==1?Colorconstands.neutralWhite:Colorconstands.primaryColor),textAlign: TextAlign.left,),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin:const EdgeInsets.symmetric(vertical: 8),
                        child: Row(
                          children: [
                            Container(
                              child: SvgPicture.asset(index==0?ImageAssets.doucment_icon:ImageAssets.doucment_check_icon),
                            ),
                            const SizedBox(width: 10,),
                            Expanded(
                              child: Container(
                                child: Text(index==0?"LIMITED_CHILD_RESULT".tr():"UNLIMITED_CHILD_RESULT".tr(),style: ThemeConstands.headline5_Medium_16.copyWith(color: index==1?Colorconstands.neutralWhite:Colorconstands.primaryColor),textAlign: TextAlign.left,),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin:const EdgeInsets.symmetric(vertical: 8),
                        child: Row(
                          children: [
                            Container(
                              child: SvgPicture.asset(index==0?ImageAssets.no_access_icon:ImageAssets.access_icon),
                            ),
                            const SizedBox(width: 10,),
                            Expanded(
                              child: Container(
                                child: Text(index==0?"LIMITED_CHILD_SUBJECT".tr():"UNLIMITED_CHILD_SUBJECT".tr(),style: ThemeConstands.headline5_Medium_16.copyWith(color: index==1?Colorconstands.neutralWhite:Colorconstands.primaryColor),textAlign: TextAlign.left,),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }
          ),
        ),
    );
  }
}