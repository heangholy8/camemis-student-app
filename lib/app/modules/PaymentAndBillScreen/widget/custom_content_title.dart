import 'package:flutter/cupertino.dart';

import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class CustomContenTitle extends StatelessWidget {
  final String titile;
  final String value;
  bool isBlack, isbold;
  CustomContenTitle(
      {Key? key,
      required this.titile,
      required this.value,
      this.isBlack = true,
      this.isbold = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Text(
              titile,
              style: ThemeConstands.subtitle1_Regular_16.copyWith(
                color: isBlack ? Colorconstands.neutralDarkGrey : Colorconstands.neutralWhite,
              ),
            ),
          ),
          Expanded(
            child: Text(
              value,
              textAlign: TextAlign.right,
              style: ThemeConstands.headline6_SemiBold_14.copyWith(
                color: isBlack ? Colorconstands.lightBlack : Colorconstands.neutralWhite,
                fontWeight: isbold ? FontWeight.bold : FontWeight.normal,
              ),
            ),
          )
        ],
      ),
    );
  }
}
