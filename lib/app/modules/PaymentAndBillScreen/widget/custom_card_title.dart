import 'package:flutter/cupertino.dart';

import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class customCardTile extends StatelessWidget {
  final String title;
  final String value;
  bool isBold = false;
  customCardTile({
    Key? key,
    required this.title,
    required this.value,
    this.isBold = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(0),
      margin: const EdgeInsets.symmetric(vertical: 5),
      child: Row(
        children: [
          Expanded(
            child: Text(title,
                style: ThemeConstands.headline6_Regular_14_20height.copyWith(
                  color: Colorconstands.neutralDarkGrey,
                )),
          ),
          Text(":    ",
              style: ThemeConstands.headline6_Medium_14.copyWith(color: Colorconstands.neutralDarkGrey,)),
          Expanded(
            child: Text(value,
                style: ThemeConstands.headline6_Medium_14.copyWith(
                  color: Colorconstands.neutralDarkGrey,)),
          ),
        ],
      ),
    );
  }
}
