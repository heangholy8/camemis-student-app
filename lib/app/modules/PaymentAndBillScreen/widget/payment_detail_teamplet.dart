import 'package:camis_application_flutter/app/modules/time_line/school_time_line/e.schooltimeline.dart';
import 'package:flutter/material.dart';
import '../../../../model/create_payment_model/create_payment_model.dart';
import '../../../../storages/get_storage.dart';
import '../../../core/themes/themes.dart';
import 'custom_bottom_noted.dart';
import 'custom_content_title.dart';

class PaymentDetail extends StatefulWidget {
  final CreatePaymentModel detail;
  bool isScreenShot;
  bool isWait;
  PaymentDetail(
      {Key? key,
      // required this.widget.detail,
      this.isScreenShot = false,
      this.isWait = false,
      required this.detail})
      : super(key: key);

  @override
  State<PaymentDetail> createState() => _PaymentDetailState();
}

class _PaymentDetailState extends State<PaymentDetail> {
  String schoolName = '';
  @override
  void initState() {
    getAuthModel();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Stack(
      alignment: AlignmentDirectional.center,
      children: [
        Container(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Transcation Summary",
                style: ThemeConstands.texttheme.headline6?.copyWith(
                    fontWeight: FontWeight.w600,
                    color: !widget.isScreenShot
                        ? Colors.white
                        : Colorconstands.black),
              ),
              const Divider(
                thickness: 1,
                height: 40,
              ),
              CustomContenTitle(
                titile: "សាលា",
                value: schoolName,
              ),
              widget.detail.paidAt.toString() != "null"
                  ? CustomContenTitle(
                      titile: "Day",
                      value: widget.detail.paidAt.toString() != "null"
                          ? widget.detail.paidAt.toString()
                          : "",
                    )
                  : Container(),
              widget.detail.transactionId.toString() != "null"
                  ? CustomContenTitle(
                      titile: "PAYMENT_CODE".tr(),
                      value: widget.detail.invoiceNumber.toString() != "null"
                          ? widget.detail.invoiceNumber!
                          : "",
                    )
                  : Container(),
              Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Colorconstands.primaryColor,
                          Colorconstands.secondaryColor,
                        ])),
                child: Column(
                  children: [
                    CustomContenTitle(
                      titile: "PRICE".tr(),
                      value: widget.detail.amount.toString() != "null"
                          ? widget.detail.amount.toString() +
                              (widget.detail.currency.toString() == "KHR"
                                  ? " រៀល"
                                  : " USD")
                          : "- - -",
                      isBlack: false,
                      isbold: true,
                    ),
                    CustomContenTitle(
                      titile: "TYPE_OF_PAYMENT".tr(),
                      value:
                          widget.detail.choosePayOptionName.toString() != "null"
                              ? widget.detail.choosePayOptionName.toString()
                              : "- - -",
                      isBlack: false,
                      isbold: true,
                    ),
                    CustomContenTitle(
                      titile: "PAYMENT_DATE".tr(),
                      value: widget.detail.paidAt.toString() != "null"
                          ? widget.detail.paidAt.toString()
                          : "- - -",
                      isBlack: false,
                      isbold: true,
                    ),
                    CustomContenTitle(
                      titile: "STATUS_PAYMENT".tr(),
                      value: widget.detail.status.toString() == "paid"
                          ? "PAID".tr()
                          : "UNPAID".tr(),
                      isBlack: false,
                      isbold: true,
                    ),
                    CustomContenTitle(
                      titile: "PAYMENT_CODE".tr(),
                      value: widget.detail.invoiceNumber.toString() != "null"
                          ? widget.detail.invoiceNumber.toString()
                          : "- - -",
                      isBlack: false,
                      isbold: true,
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              const CustomButtomNoted()
            ],
          ),
        ),
        widget.isWait
            ? Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                width: 80,
                height: 80,
                decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.7),
                    borderRadius: const BorderRadius.all(Radius.circular(15))),
                child: const Center(child: CircularProgressIndicator()))
            : const SizedBox(),
      ],
    );
  }

  void getAuthModel() async {
    GetStoragePref _pref = GetStoragePref();
    var authToken = await _pref.getJsonToken;
    setState(() {
      schoolName = authToken.schoolName.toString();
    });
  }

  Widget _buildLoading() => Container(
      margin: const EdgeInsets.only(right: 10.0),
      child: Center(
          child: Image.asset(
        "assets/images/gifs/loading.gif",
        height: 45,
        width: 45,
      )));
}
