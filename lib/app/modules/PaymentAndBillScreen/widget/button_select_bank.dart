import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class SelectBankButtonWidget extends StatefulWidget {
  final String? imageIcon;
  final String? title;
  final String? subTitle;
  final VoidCallback? onPressed;
  const SelectBankButtonWidget({Key? key,required this.imageIcon,required this.onPressed,required this.title,required this.subTitle}) : super(key: key);

  @override
  State<SelectBankButtonWidget> createState() => _SelectBankButtonWidgetState();
}

class _SelectBankButtonWidgetState extends State<SelectBankButtonWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: MaterialButton(
        padding:const EdgeInsets.symmetric(horizontal: 18,vertical: 22),
        onPressed: widget.onPressed,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      child: SvgPicture.asset(widget.imageIcon!,width: 50,height: 50,),
                    ),
                    Expanded(
                      child: Container(
                        margin:const EdgeInsets.only(left: 12),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(widget.title!,style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.lightBlack),textAlign: TextAlign.left,),
                            const SizedBox(height: 5,),
                            Text(widget.subTitle!,style: ThemeConstands .headline6_Regular_14_20height.copyWith(color: Colorconstands.darkTextsPlaceholder,),textAlign: TextAlign.left,)
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Container(
              child:const Icon(Icons.arrow_forward_ios_rounded,size: 21,color: Color(0xFFADAEB9),),
            ),
          ],
        ),
      ),
    );
  }
}