import 'package:camis_application_flutter/model/create_payment_model/create_payment_model.dart';
import 'package:camis_application_flutter/storages/get_storage.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../core/themes/color_app.dart';
import 'custom_bottom_noted.dart';
import 'custom_content_title.dart';

class CreatePaymentInfoBody extends StatefulWidget {
  final CreatePaymentModel detial;
  bool isScreenShot;
  bool isWait;
  CreatePaymentInfoBody({
    this.isScreenShot = false,
    this.isWait = false,
    required this.detial,
    //  required this.detial,
    Key? key,
  }) : super(key: key);

  @override
  State<CreatePaymentInfoBody> createState() => _CreatePaymentInfoBodyState();
}

class _CreatePaymentInfoBodyState extends State<CreatePaymentInfoBody> {
  String schoolName = '';
  @override
  void initState() {
    getAuthModel();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Stack(
      alignment: AlignmentDirectional.center,
      children: [
        Container(
          padding: const EdgeInsets.symmetric(vertical: 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              widget.detial.paidAt.toString() != "null"
                  ? CustomContenTitle(
                      titile: "DATE".tr(),
                      value: widget.detial.paidAt.toString(),
                    )
                  : Container(),
              const SizedBox(height: 8,),
              const Divider(
                thickness: 1,
                height: 1,
              ),
              const SizedBox(height: 8,),
              CustomContenTitle(
                titile: "SCHOOL".tr(),
                value: schoolName,
              ),
              const SizedBox(height: 8,),
              widget.detial.invoiceNumber.toString() != "null"
                  ? CustomContenTitle(
                      titile: "INVOICE_NUMBER".tr(),
                      value: widget.detial.invoiceNumber.toString(),
                    )
                  : Container(),
              Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Colorconstands.primaryColor,
                          Colorconstands.secondaryColor,
                        ])),
                child: Column(
                  children: [
                    CustomContenTitle(
                      titile: "Price".tr(),
                      value: widget.detial.amount.toString() != "null"
                          ? widget.detial.amount.toString() +
                              (widget.detial.currency.toString() == "KHR"
                                  ? translate == "en"
                                      ? " KHM"
                                      : " រៀល"
                                  : " USD")
                          : "- - -",
                      isBlack: false,
                      isbold: true,
                    ),
                    CustomContenTitle(
                      titile: "SERVICE_FEE".tr(),
                      value:
                          widget.detial.choosePayOptionName.toString() != "null"
                              ?translate=="km"? widget.detial.choosePayOptionName.toString():widget.detial.choosePayOptionName.toString()
                              : "- - -",
                      isBlack: false,
                      isbold: true,
                    ),
                    CustomContenTitle(
                      titile: "EXPIRED_DATE".tr(),
                      value: widget.detial.expiredAt.toString() != "null"
                          ? widget.detial.expiredAt.toString()
                          : "- - -",
                      isBlack: false,
                      isbold: true,
                    ),
                    // CustomContenTitle(
                    //   titile: "STATUS_PAYMENT".tr(),
                    //   value: widget.detial.status.toString() == "paid"
                    //       ? "PAID".tr()
                    //       : "UNPAID".tr(),
                    //   isBlack: false,
                    //   isbold: true,
                    // ),
                    CustomContenTitle(
                      titile: "INVOICE_NUMBER".tr(),
                      value: widget.detial.invoiceNumber.toString() != "null"
                          ? widget.detial.invoiceNumber.toString()
                          : "- - -",
                      isBlack: false,
                      isbold: true,
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 18,),
              const CustomButtomNoted(),
              const SizedBox(height: 18,),
            ],
          ),
        ),
        widget.isWait
            ? Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                width: 80,
                height: 80,
                decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.7),
                    borderRadius: const BorderRadius.all(Radius.circular(15))),
                child: const Center(child: CircularProgressIndicator()))
            : const SizedBox(),
      ],
    );
  }

  void getAuthModel() async {
    GetStoragePref _pref = GetStoragePref();
    var authToken = await _pref.getJsonToken;
    setState(() {
      schoolName = authToken.schoolName.toString();
    });
  }
}
