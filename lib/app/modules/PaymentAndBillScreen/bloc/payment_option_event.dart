part of 'payment_option_bloc.dart';

abstract class PaymentOptionEvent {
  const PaymentOptionEvent();

  @override
  List<Object> get props => [];
}

class GetPaymentOptionEvent extends PaymentOptionEvent {}
