import 'package:bloc/bloc.dart';
import 'package:camis_application_flutter/model/payment_option_model/payment_option_model.dart';
import 'package:meta/meta.dart';

import '../../../../model/base/base_respone_model.dart';
import '../../../../model/payment/payment_history_model.dart';
import '../../../../service/apis/payment_api/get_payment_option.dart';

part 'payment_option_event.dart';
part 'payment_option_state.dart';

class PaymentOptionBloc extends Bloc<PaymentOptionEvent, PaymentOptionState> {
  final PaymentOptionApi currenApi;
  PaymentOptionBloc({required this.currenApi}) : super(PaymentOptionInitial()) {
    on<GetPaymentOptionEvent>((event, emit) async {
      emit(PaymentOptionLoadingState());
      try {
        var data = await currenApi.getPaymentOptionApi();
        emit(PaymentOptionLoadedState(currentPaymentOption: data));
        print("Data : " + data.toString());
      } catch (e) {
        emit(PaymentOptionErrorState(error: e.toString()));
      }
    });
  }
}
