part of 'payment_detail_bloc.dart';

abstract class PaymentDetailEvent {
  const PaymentDetailEvent();

  @override
  List<Object> get props => [];
}

class GetPaymentDetailEvent extends PaymentDetailEvent {
  int paymentDetailId;

  GetPaymentDetailEvent({required this.paymentDetailId});
}
