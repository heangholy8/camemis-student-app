import 'package:bloc/bloc.dart';
import 'package:camis_application_flutter/model/payment_detail_model/payment_detail.dart';
import 'package:camis_application_flutter/service/apis/payment_api/get_payment_detail.api.dart';
import 'package:meta/meta.dart';

import '../../../../model/base/base_respone_model.dart';

part 'payment_detail_event.dart';
part 'payment_detail_state.dart';

class PaymentDetailBloc extends Bloc<PaymentDetailEvent, PaymentDetailState> {
  final PaymentDetailApi currenApi;
  PaymentDetailBloc({required this.currenApi}) : super(PaymentDetailInitial()) {
    on<GetPaymentDetailEvent>((event, emit) async {
      emit(PaymentDetailLoadingState());
      try {
        var data = await currenApi.getPaymentDetailApi(event.paymentDetailId);
        emit(PaymentDetailLoadedState(currentPaymentDetail: data));
        print("Data : " + data.toString());
      } catch (e) {
        emit(PaymentDetailErrorState(error: e.toString()));
      }
    });
  }
}
