import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../../../model/payment_option_model/get_payment_transaction_model.dart';
import '../../../../../../service/apis/check_payment_transection/get_transaction_api.dart';
part 'check_transaction_event.dart';
part 'check_transaction_state.dart';

class CheckTransactionBloc extends Bloc<CheckTransactionEvent, CheckTransactionState> {
  final GetTransactionApi transactionApi;
  CheckTransactionBloc({required this.transactionApi}) : super(CheckTransactionInitial()) {
    on<TransactionEvent>((event, emit) async {
      emit(TransactionLoading());
      try {
        var data = await transactionApi.getTransactionRequestApi(tranId: event.tranId,guardianId: event.guardianId);
        emit(TransactionLoaded(transactionModel: data));
      } catch (e) {
        print(e);
        emit(TransactionError());
      }
    });
  }
}
