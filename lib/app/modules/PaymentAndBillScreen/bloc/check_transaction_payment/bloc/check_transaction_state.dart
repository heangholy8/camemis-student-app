part of 'check_transaction_bloc.dart';

abstract class CheckTransactionState extends Equatable {
  const CheckTransactionState();
  
  @override
  List<Object> get props => [];
}

class CheckTransactionInitial extends CheckTransactionState {}

class TransactionLoading extends CheckTransactionState{}
class TransactionLoaded extends CheckTransactionState{
  final TransactionPaymentModel transactionModel;
  const TransactionLoaded({required this.transactionModel});
} 
class TransactionError extends CheckTransactionState{}
