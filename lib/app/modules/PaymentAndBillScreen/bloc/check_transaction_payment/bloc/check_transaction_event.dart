part of 'check_transaction_bloc.dart';

abstract class CheckTransactionEvent extends Equatable {
  const CheckTransactionEvent();

  @override
  List<Object> get props => [];
}

class TransactionEvent extends CheckTransactionEvent{
  final String tranId;
   final String guardianId;
  const TransactionEvent({required this.tranId,required this.guardianId});
}
