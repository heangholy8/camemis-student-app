part of 'payment_detail_bloc.dart';

abstract class PaymentDetailState {
  const PaymentDetailState();

  @override
  List<Object> get props => [];
}

class PaymentDetailInitial extends PaymentDetailState {}

class PaymentDetailLoadingState extends PaymentDetailState {}

class PaymentDetailLoadedState extends PaymentDetailState {
  final BaseResponseModel<PaymentDetailData> currentPaymentDetail;
  const PaymentDetailLoadedState({required this.currentPaymentDetail});
}

class PaymentDetailErrorState extends PaymentDetailState {
  final String error;
  const PaymentDetailErrorState({required this.error});
}
