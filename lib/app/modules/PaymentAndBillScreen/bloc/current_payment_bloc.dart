import 'package:bloc/bloc.dart';
import 'package:camis_application_flutter/model/payment_option_model/current_payment_model.dart';
import 'package:equatable/equatable.dart';

import '../../../../service/apis/payment_api/get_current_payment.dart';

part 'current_payment_event.dart';
part 'current_payment_state.dart';

class CurrentPaymentBloc extends Bloc<CurrentPaymentEvent, CurrentPaymentState> {
  final PaymentCurrentApi currentPayment;
  CurrentPaymentBloc({required this.currentPayment}) : super(CurrentPaymentInitial()) {
    on<GetCurrentPaymentEvent>((event, emit) async {
      emit(CurrentPaymentLoading());
      try {
        var data = await currentPayment.getPaymentCurrentApi();
        emit(CurrentPaymentLoaded(currentPayment: data));
      } catch (e) {
        print(e);
        emit(CurrentPaymentError());
      }
    });
  }
}
