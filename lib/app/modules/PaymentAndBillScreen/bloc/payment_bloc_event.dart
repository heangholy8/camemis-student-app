part of 'payment_bloc_bloc.dart';

abstract class PaymentBlocEvent {
  const PaymentBlocEvent();

  @override
  List<Object> get props => [];
}

class GetPaymentEvent extends PaymentBlocEvent {}
