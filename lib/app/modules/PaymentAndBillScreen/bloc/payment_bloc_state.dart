part of 'payment_bloc_bloc.dart';

abstract class PaymentBlocState {
  const PaymentBlocState();

  @override
  List<Object> get props => [];
}

class PaymentBlocInitial extends PaymentBlocState {}

class PaymentBlocLoadingState extends PaymentBlocState {}

class PaymentBlocLoadedState extends PaymentBlocState {
  final PaymentHostoryModel? currentPaymentBloc;
  const PaymentBlocLoadedState({required this.currentPaymentBloc});
}

class PaymentBlocErrorState extends PaymentBlocState {
  final String error;
  const PaymentBlocErrorState({required this.error});
}
