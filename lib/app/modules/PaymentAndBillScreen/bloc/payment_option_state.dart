part of 'payment_option_bloc.dart';

abstract class PaymentOptionState {
  const PaymentOptionState();

  @override
  List<Object> get props => [];
}

class PaymentOptionInitial extends PaymentOptionState {}

class PaymentOptionLoadingState extends PaymentOptionState {}

class PaymentOptionLoadedState extends PaymentOptionState {
  final BaseResponseModel<List<PaymentOptionData>> currentPaymentOption;
  const PaymentOptionLoadedState({required this.currentPaymentOption});
}

class PaymentOptionErrorState extends PaymentOptionState {
  final String error;
  const PaymentOptionErrorState({required this.error});
}
