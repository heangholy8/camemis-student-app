part of 'current_payment_bloc.dart';

abstract class CurrentPaymentState extends Equatable {
  const CurrentPaymentState();
  
  @override
  List<Object> get props => [];
}

class CurrentPaymentInitial extends CurrentPaymentState {}

class CurrentPaymentLoading extends CurrentPaymentState {}

class CurrentPaymentLoaded extends CurrentPaymentState {
  final PaymentCurrentModel? currentPayment;
  const CurrentPaymentLoaded({required this.currentPayment});
}

class CurrentPaymentError extends CurrentPaymentState {}