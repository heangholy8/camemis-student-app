import 'package:bloc/bloc.dart';
import '../../../../model/base/base_respone_model.dart';
import '../../../../model/payment/payment_history_model.dart';
import '../../../../service/apis/payment_api/get_payment_api.dart';

part 'payment_bloc_event.dart';
part 'payment_bloc_state.dart';

class PaymentBlocBloc extends Bloc<PaymentBlocEvent, PaymentBlocState> {
  final PaymentHistoryApi currenApi;
  PaymentBlocBloc({required this.currenApi}) : super(PaymentBlocInitial()) {
    on<GetPaymentEvent>((event, emit) async {
      emit(PaymentBlocLoadingState());
      try {
        var data = await currenApi.getPaymentHistoryApi();
        emit(PaymentBlocLoadedState(currentPaymentBloc: data));
      } catch (e) {
        print("sakfjdl;fk$e");
        emit(PaymentBlocErrorState(error: e.toString()));
      }
    });
  }
}
