part of 'aba_deep_link_bloc.dart';

abstract class AbaDeepLinkEvent extends Equatable {
  const AbaDeepLinkEvent();

  @override
  List<Object> get props => [];
}


//======================ABA Deep Link ===============================
class GetCreatePaymentABADeepLinkEvent extends AbaDeepLinkEvent {
  final int typePayment;
  final String guardianId;
  const GetCreatePaymentABADeepLinkEvent({required this.typePayment,required this.guardianId});
}