part of 'check_payment_expire_bloc.dart';

abstract class CheckPaymentExpireEvent extends Equatable {
  const CheckPaymentExpireEvent();

  @override
  List<Object> get props => [];
}

class CheckPaymentEvent extends CheckPaymentExpireEvent{}
