import 'package:bloc/bloc.dart';
import 'package:camis_application_flutter/model/create_payment_model/create_payment_model.dart';
import '../../../../../model/base/base_respone_model.dart';
import '../../../../../service/apis/create_payment/create_payment_api.dart';
part 'create_payment_event.dart';
part 'create_payment_state.dart';

class CreatePaymentBloc extends Bloc<CreatePaymentEvent, CreatePaymentState> {
  final CreatePaymentApi currenApi;
  BaseResponseModel<CreatePaymentModel> currentCreatePayment =
      BaseResponseModel<CreatePaymentModel>();
  CreatePaymentBloc({required this.currenApi}) : super(CreatePaymentInitial()) {
    on<GetCreatePaymentEvent>((event, emit) async {
      emit(CreatePaymentLoadingState());
      try {
        currentCreatePayment = await currenApi.getPaymentData(
            choosepaymentoption: event.chooseOptionPayment.toString(), guardinaId:event.guardianId.toString());
        emit(CreatePaymentLoadedState(
            currentCreatePayment: currentCreatePayment));
      } catch (e) {
        emit(CreatePaymentErrorState(error: e.toString()));
      }
    });
  }
}
