part of 'check_payment_expire_bloc.dart';

abstract class CheckPaymentExpireState extends Equatable {
  const CheckPaymentExpireState();
  
  @override
  List<Object> get props => [];
}

class CheckPaymentExpireInitial extends CheckPaymentExpireState {}

class CheckPaymentExpireLoading extends CheckPaymentExpireState{}

class CheckPaymentExpireLoaded extends CheckPaymentExpireState{
  final CheckPaymentModel? checkpayment;
  const CheckPaymentExpireLoaded({required this.checkpayment});
}
class CheckPaymentExpireError extends CheckPaymentExpireState{}
