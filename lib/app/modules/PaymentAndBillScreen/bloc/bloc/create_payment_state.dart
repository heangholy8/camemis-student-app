part of 'create_payment_bloc.dart';

abstract class CreatePaymentState {
  const CreatePaymentState();

  @override
  List<Object> get props => [];
}

class CreatePaymentInitial extends CreatePaymentState {}

class CreatePaymentLoadingState extends CreatePaymentState {}

class CreatePaymentLoadedState extends CreatePaymentState {
  final BaseResponseModel<CreatePaymentModel> currentCreatePayment;
  const CreatePaymentLoadedState({required this.currentCreatePayment});
}

class CreatePaymentErrorState extends CreatePaymentState {
  final String error;
  const CreatePaymentErrorState({required this.error});
}
