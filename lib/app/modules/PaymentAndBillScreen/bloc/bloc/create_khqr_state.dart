part of 'create_khqr_bloc.dart';

abstract class CreateKhqrState extends Equatable {
  const CreateKhqrState();
  
  @override
  List<Object> get props => [];
}

class CreateKhqrInitial extends CreateKhqrState {}


//======================ABA KHQR ===============================
class CreatePaymentKHQRLoadingState extends CreateKhqrState {}

class CreatePaymentKHQRLoadedState extends CreateKhqrState {
  final KhqrModel? abaKHQRPayment;
  const CreatePaymentKHQRLoadedState({required this.abaKHQRPayment});
}

class CreatePaymentKHQRErrorState extends CreateKhqrState {
  const CreatePaymentKHQRErrorState();
}