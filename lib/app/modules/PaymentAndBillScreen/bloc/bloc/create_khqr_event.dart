part of 'create_khqr_bloc.dart';

abstract class CreateKhqrEvent extends Equatable {
  const CreateKhqrEvent();

  @override
  List<Object> get props => [];
}

//======================ABA Deep Link ===============================
class GetCreatePaymentKHQREvent extends CreateKhqrEvent {
  final int typePayment;
  final String guardianId;
  const GetCreatePaymentKHQREvent({required this.typePayment,required this.guardianId});
}