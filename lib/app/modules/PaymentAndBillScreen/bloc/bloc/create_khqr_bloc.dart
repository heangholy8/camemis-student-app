import 'package:bloc/bloc.dart';
import 'package:camis_application_flutter/model/payment_option_model/khrq_payment_model.dart';
import 'package:camis_application_flutter/service/apis/payment_api/get_payment_khqr_api.dart';
import 'package:equatable/equatable.dart';

part 'create_khqr_event.dart';
part 'create_khqr_state.dart';

class CreateKhqrBloc extends Bloc<CreateKhqrEvent, CreateKhqrState> {
  final GetKHQRApi khqrApi;
  CreateKhqrBloc({required this.khqrApi}) : super(CreateKhqrInitial()) {
    on<GetCreatePaymentKHQREvent>((event, emit) async {
      emit(CreatePaymentKHQRLoadingState());
        try {
          var data = await khqrApi.getKHQRRequestApi(typePayment: event.typePayment,guardianId: event.guardianId);
          emit(CreatePaymentKHQRLoadedState(abaKHQRPayment: data));
        } catch (e) {
          emit(const CreatePaymentKHQRErrorState());
          print(e);
        }
    });
  }
}
