part of 'aba_deep_link_bloc.dart';

abstract class AbaDeepLinkState extends Equatable {
  const AbaDeepLinkState();
  
  @override
  List<Object> get props => [];
}

class AbaDeepLinkInitial extends AbaDeepLinkState {}

//======================ABA Deep Link ===============================
class CreatePaymentABADeepLinkLoadingState extends AbaDeepLinkState {}

class CreatePaymentABADeepLinkLoadedState extends AbaDeepLinkState {
  final AbaDeepLinkModel? abaDeepLinkPayment;
  const CreatePaymentABADeepLinkLoadedState({required this.abaDeepLinkPayment});
}

class CreatePaymentABADeepLinkErrorState extends AbaDeepLinkState {
  const CreatePaymentABADeepLinkErrorState();
}
