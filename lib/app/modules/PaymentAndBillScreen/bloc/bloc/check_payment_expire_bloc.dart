import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../../model/payment/check_payment.dart';
import '../../../../../service/apis/payment_api/check_payment_expire.dart';
part 'check_payment_expire_event.dart';
part 'check_payment_expire_state.dart';

class CheckPaymentExpireBloc extends Bloc<CheckPaymentExpireEvent, CheckPaymentExpireState> {
  final PaymentExpireApi paymentCheckApi;
  CheckPaymentExpireBloc({required this.paymentCheckApi}) : super(CheckPaymentExpireInitial()) {
    on<CheckPaymentEvent>((event, emit) async{
      emit(CheckPaymentExpireLoading());
      try {
        var data = await paymentCheckApi.checkPaymentExpireApi();
        emit(CheckPaymentExpireLoaded(checkpayment: data));
      } catch (e) {
        emit(CheckPaymentExpireError());
        print(e);
      }
    });
  }
}
