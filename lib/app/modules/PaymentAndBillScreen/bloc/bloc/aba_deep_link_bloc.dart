import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../../model/payment_option_model/aba_deep_link_model.dart';
import '../../../../../service/apis/payment_api/get_payment_deep_link_api.dart';
part 'aba_deep_link_event.dart';
part 'aba_deep_link_state.dart';

class AbaDeepLinkBloc extends Bloc<AbaDeepLinkEvent, AbaDeepLinkState> {
  final GetABADeepLinkApi deepLinkApi;
  AbaDeepLinkBloc({required this.deepLinkApi}) : super(AbaDeepLinkInitial()) {
    on<GetCreatePaymentABADeepLinkEvent>((event, emit) async {
      emit(CreatePaymentABADeepLinkLoadingState());
      try {
        var data = await deepLinkApi.getABADeepLinkRequestApi(typePayment: event.typePayment,guardianId: event.guardianId);
        emit(CreatePaymentABADeepLinkLoadedState(abaDeepLinkPayment: data));
      } catch (e) {
        emit(const CreatePaymentABADeepLinkErrorState());
      }
    });
  }
}
