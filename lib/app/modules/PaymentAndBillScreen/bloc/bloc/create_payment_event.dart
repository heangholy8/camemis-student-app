part of 'create_payment_bloc.dart';

abstract class CreatePaymentEvent {
  const CreatePaymentEvent();

  @override
  List<Object> get props => [];
}

class GetCreatePaymentEvent extends CreatePaymentEvent {
  final int chooseOptionPayment;
  final String guardianId;
  const GetCreatePaymentEvent({required this.guardianId,required this.chooseOptionPayment});
}
