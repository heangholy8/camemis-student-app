part of 'current_payment_bloc.dart';

abstract class CurrentPaymentEvent extends Equatable {
  const CurrentPaymentEvent();

  @override
  List<Object> get props => [];
}

class GetCurrentPaymentEvent extends CurrentPaymentEvent {}