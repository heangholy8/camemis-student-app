import 'dart:io';
import 'dart:typed_data';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

Future<String> saveImageToLocal(
  Uint8List btye,
) async {
  await [Permission.storage].request();
  // final result = await ImageGallerySaver.saveImage(image);

  // print("File Path: ${result}");
  // return result['filePath']!;
  final directory = await getApplicationDocumentsDirectory();
  final image = File('${directory.path}/${DateTime.now().toString()}.png');
  image.writeAsBytesSync(btye);
  GallerySaver.saveImage(image.path, albumName: "CAMIS");
  return image.path;
}
