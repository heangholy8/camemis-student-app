// ignore_for_file: prefer_interpolation_to_compose_strings

import 'dart:async';
import 'dart:ui';
import 'package:camis_application_flutter/app/modules/auth_screens/e_school_code.dart';
import 'package:camis_application_flutter/widgets/button_widget/button_icon_left_with_title.dart';
import 'package:camis_application_flutter/widgets/button_widget/button_setting_widget.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:shimmer/shimmer.dart';
import '../../../../helper/remove_storage_helper.dart';
import '../../../../service/apis/profile_user/bloc/get_profile_user_bloc.dart';
import '../../../../widgets/button_widget/button_widget_custom.dart';
import '../../../../widgets/card/card_feature_more.dart';
import '../../../../widgets/navigate_bottom_bar_widget/navigate_bottom_bar.dart';
import '../../../../widgets/no_connection_alert_widget.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../routes/app_routes.dart';
import '../../PaymentAndBillScreen/bloc/bloc/check_payment_expire_bloc.dart';
import '../../PaymentAndBillScreen/bloc/payment_option_bloc.dart';
import '../../select_lang/local_data/local_langModel.dart';

class MoreScreen extends StatefulWidget {
  const MoreScreen({Key? key}) : super(key: key);

  @override
  State<MoreScreen> createState() => _MoreScreenState();
}

class _MoreScreenState extends State<MoreScreen> with Toast {
  List<Lang> langs = allLangs;
  void updateLanguage(Locale locale, BuildContext context) {
    context.setLocale(locale);
  }
  bool paid = true;
  bool connection = true;
  StreamSubscription? sub;
  bool closeNotificate = false;
  final RemoveStorageHelper _removeStorage = RemoveStorageHelper();
  @override
  void initState() {
    setState(() {
      //=============Check internet====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          connection = (event != ConnectivityResult.none);
        });
      });
      //=============Eend Check internet====================
    });
    BlocProvider.of<CheckPaymentExpireBloc>(context).add(CheckPaymentEvent());
    //BlocProvider.of<GetProfileUserBloc>(context).add(GetProfileUser());
    super.initState();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      bottomNavigationBar: connection == false
          ? Container(
              height: 0,
            )
          : BottomNavigateBar(
              isActive: 5,
              paid: paid,
            ),
      body: Stack(
        children: [
          Positioned(
            top: 0,
            left: 0,
            child: Image.asset("assets/images/Path_20.png"),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            child: Image.asset("assets/images/Path_19.png"),
          ),
          Positioned(
            bottom: 30,
            right: 0,
            child: Image.asset("assets/images/Path_18.png"),
          ),
          Positioned(
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 50, sigmaY: 50),
              child: Container(
                color: Colorconstands.neutralWhite.withOpacity(0.3),
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(
                          top: 50, left: 26, right: 26, bottom: 10),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              alignment: Alignment.center,
                              child: Text(
                                "MORE_SCREEN".tr(),
                                style: ThemeConstands.headline2_SemiBold_24
                                    .copyWith(
                                  color: Colorconstands.neutralDarkGrey,
                                ),
                                textAlign: TextAlign.center,
                              )),
                          GestureDetector(
                            onTap: () {
                              ///====================== Show Bottom Sheet ====================================
                              showModalBottomSheet(
                                  shape: const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(16),
                                        topRight: Radius.circular(16)),
                                  ),
                                  context: context,
                                  builder: (context) {
                                    return StatefulBuilder(builder:
                                        (BuildContext context,StateSetter setState) {
                                      return Container(
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            Container(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 16,
                                                      horizontal: 28),
                                              child: Text("OTHER_OPTION".tr(),
                                                  style: ThemeConstands
                                                      .headline4_SemiBold_18
                                                      .copyWith(
                                                    color: Colorconstands
                                                        .neutralDarkGrey,
                                                  )),
                                            ),
                                            const Divider(
                                              height: 1,
                                              thickness: 0.5,
                                            ),
                                            ListView.builder(
                                              shrinkWrap: true,
                                              physics:const ScrollPhysics(),
                                              padding:const EdgeInsets.all(0),
                                              itemCount: langs.length,
                                              itemBuilder: (context, index) {
                                                final lang= langs[index];
                                                return Container(
                                                  alignment: Alignment.center,
                                                  decoration: BoxDecoration(
                                                  //color:translate==lang.sublang?Colorconstands.primaryBackgroundColor:Colorconstands.white,
                                                  border: Border(
                                                      bottom: BorderSide(
                                                      color: Colorconstands.darkGray.withOpacity(0.1),
                                                      width: 1,
                                                      )
                                                    )
                                                  ),
                                                  height: 85,
                                                  child: ListTile(
                                                    onTap: (){
                                                      setState(() {
                                                        Navigator.of(context).pop();
                                                        updateLanguage(Locale(langs[index].sublang), context);
                                                      });
                                                    },
                                                    leading: Container(
                                                      height: 50,
                                                      width: 50,
                                                      child: ClipOval(child: SvgPicture.asset(lang.image,fit: BoxFit.cover,)),
                                                    ),
                                                    title: Text(lang.title,style: translate==langs[index].sublang? ThemeConstands.headline5_SemiBold_16:ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.lightBlack),),
                                                    trailing: translate==langs[index].sublang?const Icon(Icons.check_circle_outline_sharp,color: Colorconstands.secondaryColor,):const Icon(null),
                                                    // Container(
                                                    // child: CupertinoSwitch(
                                                    //     activeColor:Colorconstands.primaryColor,
                                                    //     value: translate==langs[index].sublang?true:false,
                                                    //     onChanged: (val) {
                                                    //       setState(() {
                                                    //         Navigator.of(context).pop();
                                                    //         updateLanguage(Locale(langs[index].sublang), context);
                                                    //       });
                                                    //     },
                                                    //   ),
                                                    // ),
                                                  ),
                                                );
                                              }
                                            ),
                                            // Container(
                                            //   margin: const EdgeInsets.only(
                                            //       left: 22,
                                            //       right: 22,
                                            //       top: 12,
                                            //       bottom: 12),
                                            //   child: Row(
                                            //     children: [
                                            //       Expanded(
                                            //         child: Column(
                                            //           crossAxisAlignment:
                                            //               CrossAxisAlignment
                                            //                   .start,
                                            //           children: [
                                            //             Text(
                                            //                 "NOTIFICATION_TEACHER"
                                            //                     .tr(),
                                            //                 style: ThemeConstands
                                            //                     .headline4_Medium_18
                                            //                     .copyWith(
                                            //                   color: Colorconstands
                                            //                       .neutralDarkGrey,
                                            //                 )),
                                            //             Text(
                                            //                 "SUBTITLE_NOTIFICATION"
                                            //                     .tr(),
                                            //                 style: ThemeConstands
                                            //                     .headline6_Regular_14_24height
                                            //                     .copyWith(
                                            //                   color:
                                            //                       Colorconstands
                                            //                           .lightBulma,
                                            //                 )),
                                            //           ],
                                            //         ),
                                            //       ),
                                            //       Container(
                                            //         child: CupertinoSwitch(
                                            //           activeColor:
                                            //               Colorconstands
                                            //                   .primaryColor,
                                            //           value: closeNotificate,
                                            //           onChanged: (val) {
                                            //             setState(() {
                                            //               closeNotificate = val;
                                            //             });
                                            //           },
                                            //         ),
                                            //       ),
                                            //     ],
                                            //   ),
                                            // ),
                                            const SizedBox(height: 15,)

                                          ],
                                        ),
                                      );
                                    });
                                  });

                              ///====================== End Show Bottom Sheet ====================================
                            },
                            child: Container(
                              child: SvgPicture.asset(
                                ImageAssets.setting_outlinr_icon,
                                width: 24,
                                height: 24,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                        child: BlocListener<CheckPaymentExpireBloc,
                            CheckPaymentExpireState>(
                      listener: (context, state) {
                        if (state is CheckPaymentExpireLoaded) {
                          var dataStatus = state.checkpayment!.status;
                          setState(() {
                            paid = dataStatus!;
                          });
                        }
                      },
                      child: SingleChildScrollView(
                        child: Container(
                          margin: const EdgeInsets.only(
                            top: 15,
                            left: 26,
                            right: 26,
                          ),
                          child: Column(
                            children: [
                              BlocBuilder<GetProfileUserBloc,
                                  GetProfileUserState>(
                                builder: (context, state) {
                                  if (state is ProfileUserLoadingState) {
                                    return Container(
                                      child: Column(
                                        children: [
                                          Container(
                                            child: Shimmer.fromColors(
                                              baseColor:
                                                  const Color(0xFF3688F4),
                                              highlightColor:
                                                  const Color(0xFF3BFFF5),
                                              child: Container(
                                                height: 75,
                                                width: 75,
                                                decoration: const BoxDecoration(
                                                    color: Color(0x622195F3),
                                                    shape: BoxShape.circle),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            margin: const EdgeInsets.only(
                                                top: 19, bottom: 10),
                                            alignment: Alignment.center,
                                            child: Shimmer.fromColors(
                                              baseColor:
                                                  const Color(0xFF3688F4),
                                              highlightColor:
                                                  const Color(0xFF3BFFF5),
                                              child: Container(
                                                height: 40,
                                                width: 175,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(16),
                                                  color:
                                                      const Color(0x622195F3),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            alignment: Alignment.center,
                                            child: Shimmer.fromColors(
                                              baseColor:
                                                  const Color(0xFF3688F4),
                                              highlightColor:
                                                  const Color(0xFF3BFFF5),
                                              child: Container(
                                                height: 30,
                                                width: 275,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(16),
                                                  color:
                                                      const Color(0x622195F3),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                  } else if (state is ProfileUserLoaded) {
                                    var data = state.profileusermodel.data;
                                    return Container(
                                      child: Column(
                                        children: [
                                          Container(
                                              child: CircleAvatar(
                                            radius: 37, // Image radius
                                            backgroundImage: NetworkImage(data!
                                                .profileMedia!.fileShow
                                                .toString()),
                                          )),
                                          Container(
                                              margin: const EdgeInsets.only(
                                                  top: 19, bottom: 10),
                                              alignment: Alignment.center,
                                              child: Text( translate == "km"?"${data.firstname} ${data.lastname}":"${data.firstnameEn} ${data.lastnameEn}",
                                                style: ThemeConstands
                                                    .headline2_SemiBold_24
                                                    .copyWith(
                                                  color: Colorconstands
                                                      .neutralDarkGrey,
                                                ),
                                                textAlign: TextAlign.center,
                                              )),
                                          // Container(
                                          //   alignment: Alignment.center,
                                          //   child: Text(
                                          //     data.gender == 2
                                          //         ? "GUARDIAN".tr() +
                                          //             ": ${data.phone}"
                                          //         : "GUARDIAN".tr() +
                                          //             ": ${data.phone}",
                                          //     style: ThemeConstands
                                          //         .headline6_Regular_14_20height
                                          //         .copyWith(
                                          //       color: Colorconstands
                                          //           .darkTextsPlaceholder,
                                          //     ),
                                          //     textAlign: TextAlign.center,
                                          //   ),
                                          // ),
                                        ],
                                      ),
                                    );
                                  } else if (State is ProfileUserErrorState) {
                                    return Column(
                                      children: [
                                        Shimmer.fromColors(
                                          baseColor: const Color(0xFF3688F4),
                                          highlightColor:
                                              const Color(0xFF3BFFF5),
                                          child: Container(
                                            height: 75,
                                            width: 75,
                                            decoration: const BoxDecoration(
                                                color: Color(0x622195F3),
                                                shape: BoxShape.circle),
                                          ),
                                        ),
                                        Container(
                                          margin: const EdgeInsets.only(
                                              top: 19, bottom: 10),
                                          alignment: Alignment.center,
                                          child: Shimmer.fromColors(
                                            baseColor: const Color(0xFF3688F4),
                                            highlightColor:
                                                const Color(0xFF3BFFF5),
                                            child: Container(
                                              height: 40,
                                              width: 175,
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(16),
                                                color: const Color(0x622195F3),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          alignment: Alignment.center,
                                          child: Shimmer.fromColors(
                                            baseColor: const Color(0xFF3688F4),
                                            highlightColor:
                                                const Color(0xFF3BFFF5),
                                            child: Container(
                                              height: 30,
                                              width: 275,
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(16),
                                                color: const Color(0x622195F3),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    );
                                  } else {
                                    return Column(
                                      children: [
                                        Shimmer.fromColors(
                                          baseColor: const Color(0xFF3688F4),
                                          highlightColor:
                                              const Color(0xFF3BFFF5),
                                          child: Container(
                                            height: 75,
                                            width: 75,
                                            decoration: const BoxDecoration(
                                                color: Color(0x622195F3),
                                                shape: BoxShape.circle),
                                          ),
                                        ),
                                        Container(
                                          margin: const EdgeInsets.only(
                                              top: 19, bottom: 10),
                                          alignment: Alignment.center,
                                          child: Shimmer.fromColors(
                                            baseColor: const Color(0xFF3688F4),
                                            highlightColor:
                                                const Color(0xFF3BFFF5),
                                            child: Container(
                                              height: 40,
                                              width: 175,
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(16),
                                                color: const Color(0x622195F3),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          alignment: Alignment.center,
                                          child: Shimmer.fromColors(
                                            baseColor: const Color(0xFF3688F4),
                                            highlightColor:
                                                const Color(0xFF3BFFF5),
                                            child: Container(
                                              height: 30,
                                              width: 275,
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(16),
                                                color: const Color(0x622195F3),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    );
                                  }
                                },
                              ),
                              Container(
                                margin: const EdgeInsets.only(
                                  top: 12,
                                ),
                                child: ButtonIconLeftWithTitle(
                                  iconImage: ImageAssets.user_edit,
                                  onTap: (() {
                                    Navigator.pushNamed(
                                        context, Routes.MYPROFILE);
                                  }),
                                  title: "MY_ACCOUNT".tr(),
                                  textStyleButton: ThemeConstands
                                      .headline6_SemiBold_14
                                      .copyWith(
                                    color: Colorconstands.neutralWhite,
                                  ),
                                  buttonColor: Colorconstands.primaryColor,
                                  radiusButton: 8,
                                  weightButton: 170,
                                  panddinHorButton: 16,
                                  panddingVerButton: 6,
                                  iconColor: Colorconstands.neutralWhite,
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.only(
                                  top: 15,
                                ),
                                child: Row(
                                  children: [
                                    // Expanded(
                                    //   child: CardFeatureMore(
                                    //     iconImage: ImageAssets.people_icon,
                                    //     onTap: () {
                                    //       Navigator.pushNamed(
                                    //           context, Routes.CHLIDLISTSCREEN);
                                    //     },
                                    //     title: "CHILD_INFOR".tr(),
                                    //     textStyleButton: ThemeConstands
                                    //         .headline6_SemiBold_14
                                    //         .copyWith(
                                    //       color:
                                    //           Colorconstands.darkTextsRegular,
                                    //     ),
                                    //     buttonColor:
                                    //         Colorconstands.neutralWhite,
                                    //     radiusButton: 12,
                                    //     heightButton: 79,
                                    //     panddinHorButton: 16,
                                    //     panddingVerButton: 6,
                                    //   ),
                                    // ),
                                    // const SizedBox(
                                    //   width: 18,
                                    // ),
                                    Expanded(
                                      child: CardFeatureMore(
                                        iconImage: ImageAssets.profile_2users,
                                        onTap: () {
                                          Navigator.pushNamed(context,
                                              Routes.TEACHERLISTSCREEN);
                                        },
                                        title: "TEACHER_LIST".tr(),
                                        textStyleButton: ThemeConstands
                                            .headline6_SemiBold_14
                                            .copyWith(
                                          color:
                                              Colorconstands.darkTextsRegular,
                                        ),
                                        buttonColor:
                                            Colorconstands.neutralWhite,
                                        radiusButton: 12,
                                        heightButton: 79,
                                        panddinHorButton: 16,
                                        panddingVerButton: 6,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(
                                height: 15,
                              ),
                              CardFeatureMore(
                                heightCircle: 70,
                                weightCircle: 70,
                                iconImage: ImageAssets.card_tick,
                                onTap: () {
                                  if (paid == true) {
                                    BlocProvider.of<PaymentOptionBloc>(context)
                                        .add(GetPaymentOptionEvent());
                                    Navigator.pushNamed(
                                        context, Routes.CURRENTPAYMENTSCREEN);
                                  } else {
                                    BlocProvider.of<PaymentOptionBloc>(context)
                                        .add(GetPaymentOptionEvent());
                                    Navigator.pushNamed(
                                        context, Routes.PAYMENTOPTIONSCREEN);
                                  }
                                },
                                title: "PAYMENT_SERVICE".tr(),
                                textStyleButton: ThemeConstands
                                    .headline6_SemiBold_14
                                    .copyWith(
                                  color: Colorconstands.darkTextsRegular,
                                ),
                                buttonColor: Colorconstands.neutralWhite,
                                radiusButton: 12,
                                heightButton: 79,
                                panddinHorButton: 16,
                                panddingVerButton: 6,
                              ),
                              const Divider(
                                height: 35,
                                thickness: 0.5,
                              ),
                              Container(
                                margin: const EdgeInsets.only(bottom: 16),
                                child: ButtonSettingWidget(
                                    iconImageright:
                                        ImageAssets.chevron_right_icon,
                                    iconImageleft: ImageAssets.phone_icon,
                                    onTap: (() {
                                      Navigator.pushNamed(
                                          context, Routes.HELPSCREEN);
                                    }),
                                    title: "SUPPORT_CENTRE".tr(),
                                    textStyleButton: ThemeConstands
                                        .headline6_SemiBold_14
                                        .copyWith(
                                      color: Colorconstands.darkTextsRegular,
                                    ),
                                    buttonColor: Colorconstands.neutralWhite,
                                    radiusButton: 8,
                                    panddinHorButton: 16,
                                    panddingVerButton: 10),
                              ),
                              Container(
                                child: ButtonSettingWidget(
                                  iconImageright:
                                      ImageAssets.chevron_right_icon,
                                  iconImageleft: ImageAssets.logoCamis,
                                  onTap: (() {
                                    Navigator.pushNamed(
                                        context, Routes.ABOUTSCREEN);
                                  }),
                                  title: "ABOUT_CAMEMIS".tr(),
                                  textStyleButton: ThemeConstands
                                      .headline6_SemiBold_14
                                      .copyWith(
                                    color: Colorconstands.darkTextsRegular,
                                  ),
                                  buttonColor: Colorconstands.neutralWhite,
                                  radiusButton: 8,
                                  panddinHorButton: 16,
                                  panddingVerButton: 10,
                                ),
                              ),
                              Container(
                                margin:
                                    const EdgeInsets.only(top: 30, bottom: 5),
                                child: ButtonWidgetCustom(
                                  buttonColor: Colorconstands.primaryColor
                                      .withOpacity(0.1),
                                  title: "LOGOUT".tr(),
                                  textStyleButton: ThemeConstands
                                      .headline6_SemiBold_14
                                      .copyWith(
                                          color:
                                              Colorconstands.neutralDarkGrey),
                                  panddingVerButton: 5,
                                  onTap: () {
                                    showAlertLogout(
                                            onPressed: () {
                                              _removeStorage
                                                  .removeToken(context);
                                            },
                                            title: "DO_YOU_WANT_TO_LEAVE".tr(),
                                            context: context);
                                  },
                                  weightButton: 140,
                                  panddinHorButton: 12,
                                  radiusButton: 10,
                                ),
                              ),
                              Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    "VERSION".tr() + ": 2.0.1",
                                    style: ThemeConstands
                                        .headline6_Regular_14_20height
                                        .copyWith(
                                      color:
                                          Colorconstands.darkTextsPlaceholder,
                                    ),
                                    textAlign: TextAlign.center,
                                  )),
                              const SizedBox(
                                height: 25,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ))
                  ],
                ),
              ),
            ),
          ),
          connection == true
              ? Container()
              : Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  top: 0,
                  child: Container(
                    color: const Color(0x7B9C9595),
                  ),
                ),
          AnimatedPositioned(
            bottom: connection == true ? -150 : 0,
            left: 0,
            right: 0,
            duration: const Duration(milliseconds: 500),
            child: const NoConnectWidget(),
          ),
        ],
      ),
    );
  }
}
