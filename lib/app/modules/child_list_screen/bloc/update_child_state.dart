part of 'update_child_bloc.dart';

abstract class UpdateChildState extends Equatable {
  const UpdateChildState();
  
  @override
  List<Object> get props => [];
}

class UpdateChildInitial extends UpdateChildState {}

class UpdateChildLoading extends UpdateChildState{}

class UpdateChildLoaded extends UpdateChildState{
  final UpdateChildModel? updateChildModel;
  const UpdateChildLoaded({required this.updateChildModel});
}

class UpdateChildImageLoaded extends UpdateChildState{
  final UpdateChildModel? updateChildModel;
  const UpdateChildImageLoaded({required this.updateChildModel});
}
class UpdateChildError extends UpdateChildState{}