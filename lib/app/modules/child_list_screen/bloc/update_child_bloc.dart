import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:camis_application_flutter/service/apis/profile_user/update_profile_child.dart';
import 'package:equatable/equatable.dart';

import '../../../../model/user_profile/update_child_profile.dart';

part 'update_child_event.dart';
part 'update_child_state.dart';

class UpdateChildBloc extends Bloc<UpdateChildEvent, UpdateChildState> {
  final UpdateProfileChildApi upDateChildProfifle;
  UpdateChildBloc({required this.upDateChildProfifle}) : super(UpdateChildInitial()) {
    on<UpdateChildProfileEvent>((event, emit) async{
      emit(UpdateChildLoading());
      try {
        var upDateChild = await upDateChildProfifle.updateProfileChildRequestApi(fristname: event.fristname, lastname: event.lastname, gender: event.gender, dob: event.dob, idChild: event.idChild,fristnameEn: event.fristnameEn,lastnameEn: event.lastnameEn);
        emit(UpdateChildLoaded(updateChildModel: upDateChild));
      } catch (e) {
        print(e);
        emit(UpdateChildError());
      }
    });

    on<UpdateChildProfileImageEvent>((event, emit) async{
      emit(UpdateChildLoading());
      try {
        var upDateChild = await upDateChildProfifle.updateChildProfileImageRequestApi(idChild: event.idChild, image: event.image, dob: event.dob, fristname: event.fristname, gender: event.gender, lastname: event.lastname,fristnameEn: event.fristnameEn,lastnameEn: event.lastnameEn);
        emit(UpdateChildLoaded(updateChildModel: upDateChild));
      } catch (e) {
        print(e);
        emit(UpdateChildError());
      }
    });
  }
}
