part of 'update_child_bloc.dart';

abstract class UpdateChildEvent extends Equatable {
  const UpdateChildEvent();

  @override
  List<Object> get props => [];
}

class UpdateChildProfileEvent extends UpdateChildEvent{
 final String fristname; 
 final String lastname;
 final String fristnameEn; 
 final String lastnameEn;
 final String gender;
 final String dob; 
 final String idChild;
 const UpdateChildProfileEvent( {
  required this.fristnameEn, 
  required this.lastnameEn,
  required this.fristname, 
  required this.lastname, 
  required this.gender, 
  required this.dob, 
  required this.idChild,});
}

class UpdateChildProfileImageEvent extends UpdateChildEvent{
 final File image; 
 final String idChild;
 final String fristname; 
 final String lastname;
 final String fristnameEn; 
 final String lastnameEn;
 final String gender;
 final String dob;
 const UpdateChildProfileImageEvent({
  required this.fristnameEn,
  required this.lastnameEn,
  required this.fristname, 
  required this.lastname, 
  required this.gender, 
  required this.dob,
  required this.image, 
  required this.idChild,});
}