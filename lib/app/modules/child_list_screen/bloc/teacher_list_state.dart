part of 'teacher_list_bloc.dart';

abstract class TeacherListState extends Equatable {
  const TeacherListState();
  
  @override
  List<Object> get props => [];
}

class TeacherListInitial extends TeacherListState {}

class GetTeacherLoading extends TeacherListState{}

class GetTeacherLoaded extends TeacherListState{
  final TeacherListModel? teacherListModel;
  const GetTeacherLoaded({required this.teacherListModel});
}
class GetTeacherListError extends TeacherListState{}
