import 'dart:async';
import 'dart:io';
import 'package:camis_application_flutter/app/modules/child_list_screen/bloc/update_child_bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import '../../../../component/change_child/bloc/change_child_bloc.dart';
import '../../../../mixins/toast.dart';
import '../../../../widgets/error_widget/error_request.dart';
import '../../../../widgets/no_connection_alert_widget.dart';
import '../../../../widgets/no_dart.dart';
import '../../../../widgets/take_upload_image.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class ChildListScreen extends StatefulWidget {
  const ChildListScreen({Key? key}) : super(key: key);

  @override
  State<ChildListScreen> createState() => _ChildListScreenState();
}

class _ChildListScreenState extends State<ChildListScreen> with Toast {
  bool? classmonitor = false;
  bool connection = true;
  StreamSubscription? sub;
  String? idClass;
  int activeChild = 0;
  PageController? pagecontrol;
  bool editInformation = false;
  File? _image;
  String? childId;
  String errorSelectGender = "";
  int genderNumber = 0;

  final ImagePicker _picker = ImagePicker();
  XFile? file;
  bool fristLoadata = true;

  bool? checkfocusfristname;
  bool? checkfocuslastname;
  bool? checkfocusgender;
  bool? checkfocuswork;
  bool? checkfocusdob;

  TextEditingController fristnameController = TextEditingController();
  TextEditingController lastnameController = TextEditingController();
  TextEditingController fristnameEnController = TextEditingController();
  TextEditingController lastnameEnController = TextEditingController();
  TextEditingController workController = TextEditingController();
  TextEditingController className = TextEditingController();
  TextEditingController dateofbirth = TextEditingController();

  String gender = "0";
  @override
  void initState() {
    setState(() {
      //=============Check internet====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          connection = (event != ConnectivityResult.none);
          if (connection == true) {
          } else {}
        });
      });
      //=============Eend Check internet====================
    });
    BlocProvider.of<ChangeChildBloc>(context).add(GetChildrenEvent());
    pagecontrol = PageController(keepPage: true);
    super.initState();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      bottomNavigationBar: editInformation == false
          ? Container(
              height: 0,
            )
          : Container(
              height: 55,
              child: MaterialButton(
                color: Colorconstands.mainColorForecolor,
                padding: const EdgeInsets.all(0),
                onPressed:genderNumber==0?() {
                  setState(() {
                    errorSelectGender = "PLEASE_SELECT_GENDER".tr();
                  });
                }: () {
                  setState(() {
                    errorSelectGender = "";
                  });
                  if (_image == null) {
                    BlocProvider.of<UpdateChildBloc>(context).add(
                        UpdateChildProfileEvent(
                            dob: dateofbirth.text,
                            fristname: fristnameController.text,
                            gender: genderNumber.toString(),
                            idChild: childId!,
                            lastnameEn: lastnameEnController.text,
                            fristnameEn: fristnameEnController.text,
                            lastname: lastnameController.text));
                  } else {
                    BlocProvider.of<UpdateChildBloc>(context).add(
                        UpdateChildProfileImageEvent(
                            idChild: childId!,
                            image: _image!,
                            dob: dateofbirth.text,
                            fristname: fristnameController.text,
                            lastnameEn: lastnameEnController.text,
                            fristnameEn: fristnameEnController.text,
                            gender: genderNumber.toString(),
                            lastname: lastnameController.text));
                  }
                },
                child: Center(
                  child: Text(
                    "SAVE".tr(),
                    style: ThemeConstands.headline3_Medium_20_26height
                        .copyWith(color: Colorconstands.neutralWhite),
                  ),
                ),
              ),
            ),
      body: Stack(
        children: [
          Positioned(
            top: 0,
            right: 0,
            child: Image.asset("assets/images/Oval.png"),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            child: BlocListener<UpdateChildBloc, UpdateChildState>(
              listener: (context, state) {
                if (state is UpdateChildLoaded) {
                  setState(() {
                    showSuccessDialog( title: "SUCCESSFULLY".tr(),discription: "", callback: () { 
                       Navigator.of(context).pop();
                     });
                    BlocProvider.of<ChangeChildBloc>(context)
                        .add(GetChildrenEvent());
                    fristLoadata = true;
                  });
                }
              },
              child: Container(
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(
                          top: 50, left: 22, right: 22, bottom: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                            child: Container(
                              child: const Icon(
                                Icons.arrow_back_ios_new_rounded,
                                size: 25,
                                color: Colorconstands.neutralWhite,
                              ),
                            ),
                          ),
                          Container(
                              alignment: Alignment.center,
                              child: Text(
                                "CHILD_INFOR".tr(),
                                style: ThemeConstands.headline3_SemiBold_20
                                    .copyWith(
                                  color: Colorconstands.neutralWhite,
                                ),
                                textAlign: TextAlign.center,
                              )),
                          Container(
                            width: 32, height: 32,
                            //child: SvgPicture.asset(ImageAssets.question_circle,color: Colorconstands.neutralWhite,width: 28,height: 28,),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.only(top: 10),
                        width: MediaQuery.of(context).size.width,
                        child: Container(
                          decoration: const BoxDecoration(
                              color: Colorconstands.neutralWhite,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(12),
                                  topRight: Radius.circular(12))),
                          child: BlocBuilder<ChangeChildBloc, ChangeChildState>(
                            builder: (context, state) {
                              if (state is ChangeChildLoadingState) {
                                return const Center(
                                  child: CircularProgressIndicator(),
                                );
                              } else if (state is ChangeChildLoadState) {
                                var datachild = state.childrenModel!.data;
                                pagecontrol =
                                    PageController(initialPage: activeChild);
                                return datachild!.isEmpty
                                    ? Container(
                                        color: Colorconstands.neutralWhite,
                                        child: DataNotFound(
                                          subtitle: "NONE_CHILD_DES".tr(),
                                          title: "NONE_CHILD".tr(),
                                        ),
                                      )
                                    : Column(
                                        children: [
                                          Container(
                                            decoration: BoxDecoration(
                                              color: Colorconstands.primaryColor
                                                  .withOpacity(0.9),
                                              borderRadius:
                                                  const BorderRadius.only(
                                                topLeft: Radius.circular(12),
                                                topRight: Radius.circular(12),
                                              ),
                                            ),
                                            height: 55,
                                            child: ListView.separated(
                                              separatorBuilder:
                                                  (context, index) {
                                                return Container(
                                                  width: 8,
                                                );
                                              },
                                              scrollDirection: Axis.horizontal,
                                              itemCount: datachild.length,
                                              itemBuilder:
                                                  (context, indexChild) {
                                                return SizedBox(
                                                  width: datachild.length > 3
                                                      ? null
                                                      : MediaQuery.of(context)
                                                              .size
                                                              .width /
                                                          datachild.length,
                                                  child: MaterialButton(
                                                    textColor: Colorconstands
                                                        .primaryColor,
                                                    elevation: 0,
                                                    color: activeChild ==
                                                            indexChild
                                                        ? Colorconstands
                                                            .neutralWhite
                                                        : Colors.transparent,
                                                    shape: const RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius.only(
                                                                topLeft: Radius
                                                                    .circular(
                                                                        12),
                                                                topRight: Radius
                                                                    .circular(
                                                                        12))),
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        vertical: 4,
                                                        horizontal: 6),
                                                    onPressed:
                                                        connection == false
                                                            ? () {
                                                                showMessage(
                                                                    context,
                                                                    "NO_CONNECTIVITY"
                                                                        .tr(),
                                                                    250.0,
                                                                    25.0);
                                                              }
                                                            : () {
                                                                setState(() {
                                                                  activeChild =  indexChild;
                                                                  childId = datachild[indexChild].id.toString();
                                                                  pagecontrol!.animateToPage(
                                                                      indexChild,
                                                                      duration: const Duration( milliseconds: 500), 
                                                                      curve: Curves.ease);
                                                                  fristnameEnController = TextEditingController( text: datachild[indexChild].firstnameEn);
                                                                  fristnameController = TextEditingController( text: datachild[indexChild].firstname);
                                                                  lastnameController = TextEditingController(text: datachild[indexChild].lastname);
                                                                  lastnameEnController = TextEditingController(text: datachild[indexChild].lastnameEn);
                                                                  genderNumber = datachild[indexChild].gender!;
                                                                  workController = TextEditingController(text: "STUDENT".tr());
                                                                  className = TextEditingController(text: datachild[indexChild].currentClass ==
                                                                              null? "NOCLASS".tr()
                                                                          : datachild[indexChild].currentClass!.className.toString());
                                                                  dateofbirth = TextEditingController(text: datachild[indexChild].dob.toString());
                                                                  _image = datachild[indexChild].profileMedia!.imageprofile;                                                                });
                                                              },
                                                    child: Row(
                                                      mainAxisAlignment:MainAxisAlignment.center,
                                                      children: [
                                                        activeChild == indexChild
                                                            ? Container(
                                                                child:CircleAvatar(
                                                                radius: 20, // Image radius
                                                                backgroundImage: NetworkImage(datachild[indexChild].profileMedia!.fileShow.toString()),
                                                              ))
                                                            : Container(),
                                                        const SizedBox(
                                                          width: 12,
                                                        ),
                                                        Text(
                                                          translate == "km"
                                                              ? datachild[
                                                                      indexChild]
                                                                  .firstname
                                                                  .toString()
                                                              : datachild[
                                                                      indexChild]
                                                                  .firstnameEn
                                                                  .toString(),
                                                          style: activeChild ==
                                                                  indexChild
                                                              ? ThemeConstands
                                                                  .button_SemiBold_16
                                                              : ThemeConstands
                                                                  .headline5_Medium_16
                                                                  .copyWith(
                                                                      color: Colorconstands
                                                                          .neutralWhite),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                );
                                              },
                                            ),
                                          ),
                                          Expanded(
                                            child: Container(
                                              child: PageView.builder(
                                                controller: pagecontrol,
                                                onPageChanged: ((value) {
                                                  setState(() {
                                                    activeChild = value;
                                                    childId = datachild[value]
                                                        .id
                                                        .toString();
                                                    fristnameController = TextEditingController(text:datachild[value].firstname);
                                                    lastnameController = TextEditingController(text:datachild[value].lastname);
                                                    fristnameEnController = TextEditingController(text:datachild[value].firstnameEn);
                                                    lastnameEnController = TextEditingController(text:datachild[value].lastnameEn);
                                                    genderNumber = datachild[value].gender!;
                                                    workController =
                                                        TextEditingController(
                                                            text: "STUDENT".tr());
                                                    className = TextEditingController(
                                                        text: datachild[value]
                                                                    .currentClass ==
                                                                null
                                                            ? "NOCLASS".tr()
                                                            : datachild[value]
                                                                .currentClass!
                                                                .className
                                                                .toString());
                                                    dateofbirth =
                                                        TextEditingController(
                                                            text: datachild[
                                                                    value]
                                                                .dob
                                                                .toString());
                                                    _image = datachild[value]
                                                        .profileMedia!
                                                        .imageprofile;
                                                  });
                                                }),
                                                itemCount: datachild.length,
                                                itemBuilder: ((context, index) {
                                                  if (fristLoadata == true) {
                                                    checkfocusfristname = false;
                                                    checkfocuslastname = false;
                                                    checkfocusgender = false;
                                                    checkfocusdob = false;
                                                    checkfocuswork = false;
                                                    fristnameController = TextEditingController(text: datachild[index].firstname);
                                                    lastnameController = TextEditingController(text: datachild[index] .lastname);
                                                    fristnameEnController = TextEditingController(text: datachild[index].firstnameEn);
                                                    lastnameEnController = TextEditingController(text: datachild[index] .lastnameEn);
                                                    genderNumber = datachild[index].gender!;
                                                    workController =
                                                        TextEditingController(
                                                            text: "STUDENT".tr());
                                                    className =
                                                        TextEditingController(
                                                            text: datachild[index].currentClass==null?"NOCLASS".tr(): datachild[index].currentClass!
                                                                .className
                                                                .toString());
                                                    dateofbirth =
                                                        TextEditingController(
                                                            text: datachild[
                                                                    index]
                                                                .dob
                                                                .toString());
                                                    _image = datachild[index]
                                                        .profileMedia!
                                                        .imageprofile;
                                                    childId = datachild[index]
                                                        .id
                                                        .toString();
                                                    fristLoadata = false;
                                                  }
                                                  return SingleChildScrollView(
                                                    child: Container(
                                                      margin: const EdgeInsets
                                                              .symmetric(
                                                          horizontal: 26),
                                                      child: Column(
                                                        children: [
                                                          Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [
                                                              Container(
                                                                  decoration: const BoxDecoration(
                                                                      shape: BoxShape
                                                                          .circle),
                                                                  child:
                                                                      PopupMenuButton(
                                                                    shape: const RoundedRectangleBorder(
                                                                        borderRadius:
                                                                            BorderRadius.all(Radius.circular(15.0))),
                                                                    icon:
                                                                        const Icon(
                                                                      Icons
                                                                          .more_vert_outlined,
                                                                      size: 22,
                                                                      color: Colors
                                                                          .transparent,
                                                                    ),
                                                                    itemBuilder:
                                                                        (ctx) =>
                                                                            [],
                                                                  )),
                                                              Expanded(
                                                                child:
                                                                    Container(
                                                                  margin: const EdgeInsets
                                                                          .only(
                                                                      top: 25),
                                                                  child:
                                                                      CircleAvatar(
                                                                    radius:
                                                                        35, // Image radius
                                                                    child:
                                                                        ClipOval(
                                                                      child: datachild[index].profileMedia!.imageprofile ==
                                                                              null
                                                                          ? Image
                                                                              .network(
                                                                              datachild[index].profileMedia!.fileShow.toString(),
                                                                              width: 80,
                                                                              height: 80,
                                                                              fit: BoxFit.cover,
                                                                            )
                                                                          : Image
                                                                              .file(
                                                                              datachild[index].profileMedia!.imageprofile!,
                                                                              width: 80,
                                                                              height: 80,
                                                                              fit: BoxFit.cover,
                                                                            ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                              Container(
                                                                  decoration: const BoxDecoration(
                                                                      shape: BoxShape
                                                                          .circle),
                                                                  child:
                                                                      PopupMenuButton(
                                                                    shape: const RoundedRectangleBorder(
                                                                        borderRadius:
                                                                            BorderRadius.all(Radius.circular(15.0))),
                                                                    icon:
                                                                        const Icon(
                                                                      Icons
                                                                          .more_vert_outlined,
                                                                      size: 28,
                                                                      color: Colorconstands
                                                                          .neutralDarkGrey,
                                                                    ),
                                                                    itemBuilder:
                                                                        (ctx) =>
                                                                            [
                                                                      _buildPopupMenuItem(
                                                                        "EDIT"
                                                                            .tr(),
                                                                        Icons
                                                                            .edit_outlined,
                                                                      ),
                                                                    ],
                                                                  ))
                                                            ],
                                                          ),
                                                          Container(
                                                            margin:
                                                                const EdgeInsets
                                                                        .only(
                                                                    bottom: 20),
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .center,
                                                              children: [
                                                                const Icon(
                                                                  Icons
                                                                      .camera_alt_outlined,
                                                                  size: 18,
                                                                  color: Colorconstands
                                                                      .primaryColor,
                                                                ),
                                                                TextButton(
                                                                  onPressed:
                                                                      () {
                                                                    setState(
                                                                      () {
                                                                        showModalBottomSheet(
                                                                          backgroundColor:
                                                                              Colors.transparent,
                                                                          context:
                                                                              context,
                                                                          builder:
                                                                              (BuildContext bc) {
                                                                            return ShowPiker(
                                                                              onPressedCamera: () async {
                                                                                XFile? pickedFile = await _picker.pickImage(source: ImageSource.camera);
                                                                                setState(() {
                                                                                  datachild[index].profileMedia!.imageprofile = File(pickedFile!.path);
                                                                                  file = pickedFile;
                                                                                  _image = datachild[index].profileMedia!.imageprofile;
                                                                                });
                                                                                Navigator.of(context).pop();
                                                                              },
                                                                              onPressedGalary: () async {
                                                                                XFile? pickedFile = await _picker.pickImage(source: ImageSource.gallery);
                                                                                setState(() {
                                                                                  datachild[index].profileMedia!.imageprofile = File(pickedFile!.path);
                                                                                  file = pickedFile;
                                                                                  _image = datachild[index].profileMedia!.imageprofile;
                                                                                });
                                                                                Navigator.of(context).pop();
                                                                              },
                                                                            );
                                                                          },
                                                                        );
                                                                      },
                                                                    );
                                                                  },
                                                                  child: Text(
                                                                    "CHANGE_PROFILE".tr(),
                                                                    style: ThemeConstands
                                                                        .headline6_Medium_14
                                                                        .copyWith(color:Colorconstands.primaryColor,
                                                                            decoration: TextDecoration.underline),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                          Container(
                                                            padding:EdgeInsets.zero,
                                                            decoration:BoxDecoration(
                                                              color:Colorconstands .lightGohan,
                                                              borderRadius:BorderRadius .circular( 12),
                                                              border: Border.all(
                                                                  color: checkfocusfristname == false
                                                                      ? Colorconstands
                                                                          .neutralGrey
                                                                      : Colorconstands
                                                                          .primaryColor,
                                                                  width: checkfocusfristname ==
                                                                          false
                                                                      ? 1
                                                                      : 2),
                                                            ),
                                                            child: Focus(
                                                              onFocusChange:
                                                                  (hasfocus) {
                                                                // setState(() {
                                                                //   if(hasfocus){
                                                                //     checkfocusfristname = true;
                                                                //   }
                                                                //   else{
                                                                //     checkfocuslastname = false;
                                                                //     checkfocusdob = false;
                                                                //     checkfocusgender = false;
                                                                //   }
                                                                // });
                                                              },
                                                              child:
                                                                  TextFormField(
                                                                    keyboardType: TextInputType.name,
                                                                enabled: true,
                                                                controller:translate=="km"?fristnameController:fristnameEnController,
                                                                onChanged:
                                                                    ((value) {}),
                                                                style: ThemeConstands
                                                                    .headline5_Medium_16,
                                                                decoration:
                                                                    InputDecoration(
                                                                  border: UnderlineInputBorder(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              16.0)),
                                                                  contentPadding:
                                                                      const EdgeInsets.only( top:8, bottom: 5,left:18,right:18),
                                                                  labelText: "FIRST_NAME".tr(),
                                                                  labelStyle: ThemeConstands
                                                                      .subtitle1_Regular_16
                                                                      .copyWith( color:Colorconstands.lightTrunks),
                                                                  enabledBorder:
                                                                      const UnderlineInputBorder(
                                                                          borderSide:BorderSide(color: Colors.transparent)),
                                                                  focusedBorder:
                                                                      const UnderlineInputBorder(
                                                                          borderSide: BorderSide(color: Colors.transparent)),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:const EdgeInsets.only(top: 16),
                                                            padding: EdgeInsets.zero,
                                                            decoration:BoxDecoration(
                                                              color:Colorconstands.lightGohan,
                                                              borderRadius: BorderRadius .circular(12),
                                                              border: Border.all(
                                                                  color: checkfocuslastname == false
                                                                      ? Colorconstands
                                                                          .neutralGrey
                                                                      : Colorconstands
                                                                          .primaryColor,
                                                                  width: checkfocuslastname ==false
                                                                      ? 1
                                                                      : 2),
                                                            ),
                                                            child: Focus(
                                                              onFocusChange:
                                                                  (hasfocus) {
                                                                // setState(() {
                                                                //   if(hasfocus){
                                                                //     checkfocuslastname = true;
                                                                //   }
                                                                //   else{
                                                                //     checkfocusfristname = false;
                                                                //     checkfocusdob = false;
                                                                //     checkfocusgender = false;
                                                                //   }

                                                                // });
                                                              },
                                                              child:
                                                                  TextFormField(
                                                                    keyboardType: TextInputType.name,
                                                                enabled: true,
                                                                controller: translate=="km"?lastnameController:lastnameEnController,
                                                                onChanged:((value) {}),
                                                                style: ThemeConstands.headline5_Medium_16,
                                                                decoration:InputDecoration(
                                                                  border: UnderlineInputBorder(borderRadius:BorderRadius.circular( 16.0)),
                                                                  contentPadding:const EdgeInsets.only(top: 8,bottom:5, left:18, right:18),
                                                                  labelText: "LAST_NAME" .tr(),
                                                                  labelStyle: ThemeConstands
                                                                      .subtitle1_Regular_16
                                                                      .copyWith( color: Colorconstands.lightTrunks),
                                                                  enabledBorder:
                                                                      const UnderlineInputBorder(
                                                                          borderSide:BorderSide(color: Colors.transparent)),
                                                                  focusedBorder:
                                                                      const UnderlineInputBorder(
                                                                          borderSide:
                                                                              BorderSide(color: Colors.transparent)),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          Row(
                                                            children: [
                                                              Container(
                                                                margin:const EdgeInsets.only(top: 12,left: 12),
                                                                alignment: Alignment.centerLeft,
                                                                child: Text('GENDER'.tr(),style: ThemeConstands.subtitle1_Regular_16.copyWith( color: Colorconstands.darkGray),),
                                                              ),
                                                              Container(
                                                                margin:const EdgeInsets.only(top: 12,left: 12),
                                                                alignment: Alignment.centerLeft,
                                                                child: Text(errorSelectGender,style: ThemeConstands.subtitle1_Regular_16.copyWith( color: Colorconstands.alertsDecline),),
                                                              ),
                                                            ],
                                                          ),
                                                          Container(
                                                            child: Row(
                                                              children: [
                                                                Container(
                                                                  child: Row(
                                                                    children: [
                                                                      Radio(
                                                                        value:1,
                                                                        groupValue: genderNumber,
                                                                        onChanged: (int? value) {
                                                                          setState(() {
                                                                            errorSelectGender = "";
                                                                          genderNumber = value!;
                                                                          });
                                                                        },
                                                                      ),
                                                                      Container(
                                                                        child: Text('MALE'.tr(),style: ThemeConstands.subtitle1_Regular_16.copyWith(color: Colorconstands.darkGray),),
                                                                      )
                                                                    ],
                                                                  ),
                                                                ),
                                                                const SizedBox(width: 20,),
                                                                Container(
                                                                  child: Row(
                                                                    children: [
                                                                      Radio(
                                                                        value:2,
                                                                        groupValue: genderNumber,
                                                                        onChanged: (int? value) {
                                                                          setState(() {
                                                                            errorSelectGender = "";
                                                                          genderNumber = value!;
                                                                          });
                                                                        },
                                                                      ),
                                                                      Container(
                                                                        child: Text('FEMALE'.tr(),style: ThemeConstands.subtitle1_Regular_16.copyWith(color: Colorconstands.darkGray),),
                                                                      )
                                                                    ],
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:
                                                                const EdgeInsets
                                                                        .only(
                                                                    top: 16),
                                                            padding:
                                                                EdgeInsets.zero,
                                                            decoration:
                                                                BoxDecoration(
                                                              color:
                                                                  Colorconstands
                                                                      .lightGohan,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          12),
                                                              border: Border.all(
                                                                  color: checkfocusdob == false
                                                                      ? Colorconstands
                                                                          .neutralGrey
                                                                      : Colorconstands
                                                                          .primaryColor,
                                                                  width: checkfocusdob ==
                                                                          false
                                                                      ? 1
                                                                      : 2),
                                                            ),
                                                            child: Focus(
                                                              onFocusChange:
                                                                  (hasfocus) {
                                                                // setState(() {
                                                                //   if(hasfocus){
                                                                //       checkfocusdob = true;
                                                                //     }
                                                                //     else{
                                                                //       checkfocusfristname = false;
                                                                //       checkfocuslastname = false;
                                                                //       checkfocusgender = false;
                                                                //     }
                                                                // });
                                                              },
                                                              child:
                                                                  TextFormField(
                                                                    keyboardType: TextInputType.datetime,
                                                                enabled: true,
                                                                controller:
                                                                    dateofbirth,
                                                                onChanged:
                                                                    ((value) {
                                                                  setState(() {
                                                                    dateofbirth
                                                                        .text;
                                                                  });
                                                                }),
                                                                style: ThemeConstands
                                                                    .headline5_Medium_16,
                                                                decoration:
                                                                    InputDecoration(
                                                                  border: UnderlineInputBorder(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              16.0)),
                                                                  contentPadding:
                                                                      const EdgeInsets
                                                                              .only(
                                                                          top:
                                                                              8,
                                                                          bottom:
                                                                              5,
                                                                          left:
                                                                              18,
                                                                          right:
                                                                              18),
                                                                  labelText:
                                                                      "DOB".tr(),
                                                                  labelStyle: ThemeConstands
                                                                      .subtitle1_Regular_16
                                                                      .copyWith(
                                                                          color:
                                                                              Colorconstands.lightTrunks),
                                                                  enabledBorder:
                                                                      const UnderlineInputBorder(
                                                                          borderSide:
                                                                              BorderSide(color: Colors.transparent)),
                                                                  focusedBorder:
                                                                      const UnderlineInputBorder(
                                                                          borderSide:
                                                                              BorderSide(color: Colors.transparent)),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:
                                                                const EdgeInsets
                                                                        .only(
                                                                    top: 16),
                                                            padding:
                                                                EdgeInsets.zero,
                                                            decoration:
                                                                BoxDecoration(
                                                              color:
                                                                  Colorconstands
                                                                      .lightGohan,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          12),
                                                              border: Border.all(
                                                                  color: Colorconstands
                                                                      .neutralGrey,
                                                                  width: 1),
                                                            ),
                                                            child: Focus(
                                                              onFocusChange:
                                                                  (hasfocus) {},
                                                              child:
                                                                  TextFormField(
                                                                enabled: false,
                                                                controller:
                                                                    className,
                                                                onChanged:
                                                                    ((value) {
                                                                  setState(() {
                                                                    className
                                                                        .text;
                                                                  });
                                                                }),
                                                                style: ThemeConstands
                                                                    .headline5_Medium_16,
                                                                decoration:
                                                                    InputDecoration(
                                                                  border: UnderlineInputBorder(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              16.0)),
                                                                  contentPadding:
                                                                      const EdgeInsets
                                                                              .only(
                                                                          top:
                                                                              8,
                                                                          bottom:
                                                                              5,
                                                                          left:
                                                                              18,
                                                                          right:
                                                                              18),
                                                                  labelText:
                                                                      "CLASS"
                                                                          .tr(),
                                                                  labelStyle: ThemeConstands
                                                                      .subtitle1_Regular_16
                                                                      .copyWith(
                                                                          color:
                                                                              Colorconstands.lightTrunks),
                                                                  enabledBorder:
                                                                      const UnderlineInputBorder(
                                                                          borderSide:
                                                                              BorderSide(color: Colors.transparent)),
                                                                  focusedBorder:
                                                                      const UnderlineInputBorder(
                                                                          borderSide:
                                                                              BorderSide(color: Colors.transparent)),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:
                                                                const EdgeInsets
                                                                        .only(
                                                                    top: 16),
                                                            padding:
                                                                EdgeInsets.zero,
                                                            decoration:
                                                                BoxDecoration(
                                                              color:
                                                                  Colorconstands
                                                                      .lightGohan,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          12),
                                                              border: Border.all(
                                                                  color: checkfocuswork == false
                                                                      ? Colorconstands
                                                                          .neutralGrey
                                                                      : Colorconstands
                                                                          .primaryColor,
                                                                  width: checkfocuswork ==
                                                                          false
                                                                      ? 1
                                                                      : 2),
                                                            ),
                                                            child: Focus(
                                                              onFocusChange:
                                                                  (hasfocus) {
                                                                setState(() {
                                                                  hasfocus
                                                                      ? checkfocuswork =
                                                                          true
                                                                      : checkfocuswork =
                                                                          false;
                                                                });
                                                              },
                                                              child:
                                                                  TextFormField(
                                                                enabled: false,
                                                                controller:
                                                                    workController,
                                                                onChanged:
                                                                    ((value) {
                                                                  setState(() {
                                                                    workController
                                                                        .text;
                                                                  });
                                                                }),
                                                                style: ThemeConstands
                                                                    .headline5_Medium_16,
                                                                decoration:
                                                                    InputDecoration(
                                                                  border: UnderlineInputBorder(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              16.0)),
                                                                  contentPadding:
                                                                      const EdgeInsets
                                                                              .only(
                                                                          top:
                                                                              8,
                                                                          bottom:
                                                                              5,
                                                                          left:
                                                                              18,
                                                                          right:
                                                                              18),
                                                                  labelText:
                                                                      "ROLE"
                                                                          .tr(),
                                                                  labelStyle: ThemeConstands
                                                                      .subtitle1_Regular_16
                                                                      .copyWith(
                                                                          color:
                                                                              Colorconstands.lightTrunks),
                                                                  enabledBorder:
                                                                      const UnderlineInputBorder(
                                                                          borderSide:
                                                                              BorderSide(color: Colors.transparent)),
                                                                  focusedBorder:
                                                                      const UnderlineInputBorder(
                                                                          borderSide:
                                                                              BorderSide(color: Colors.transparent)),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  );
                                                }),
                                              ),
                                            ),
                                          )
                                        ],
                                      );
                              } else {
                                return Container(
                                  decoration:const BoxDecoration(
                                    color: Colorconstands.neutralWhite,
                                    borderRadius: BorderRadius.only(topLeft: Radius.circular(15),topRight:  Radius.circular(15))
                                  ),
                                  child: ErrorRequestData(
                                    onPressed: (){
                                      Navigator.pushReplacement(
                                          context, 
                                          PageRouteBuilder(
                                              pageBuilder: (context, animation1, animation2) =>const ChildListScreen(),
                                              transitionDuration: Duration.zero,
                                              reverseTransitionDuration: Duration.zero,
                                          ),
                                      );
                                    },
                                    discription: '', 
                                    hidebutton: true, 
                                    title: 'WE_DETECT_ERROR'.tr(),

                                  ),
                                );
                              }
                            },
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          connection == true
              ? Container()
              : Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  top: 0,
                  child: Container(
                    color: const Color(0x7B9C9595),
                  ),
                ),
          AnimatedPositioned(
            bottom: connection == true ? -150 : 0,
            left: 0,
            right: 0,
            duration: const Duration(milliseconds: 500),
            child: const NoConnectWidget(),
          ),
        ],
      ),
    );
  }

  PopupMenuItem _buildPopupMenuItem(String title, IconData iconData) {
    return PopupMenuItem(
      onTap: (() {
        setState(() {
          editInformation = true;
        });
      }),
      mouseCursor: MouseCursor.uncontrolled,
      padding: const EdgeInsets.all(0),
      child: Container(
        padding: const EdgeInsets.only(bottom: 5),
        child: Container(
          height: 50,
          decoration: BoxDecoration(
              color: Colorconstands.gray300.withOpacity(0.4),
              borderRadius: const BorderRadius.all(Radius.circular(12))),
          margin: const EdgeInsets.symmetric(horizontal: 7),
          padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
          child: Row(
            children: [
              Icon(
                iconData,
                color: Colorconstands.primaryColor,
                size: 20,
              ),
              const SizedBox(
                width: 8.0,
              ),
              Text(
                title,
                style: ThemeConstands.headline6_Regular_14_20height,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
