// ignore_for_file: public_member_api_docs, sort_constructors_first
class ExamVideoModel {
  final String title;
  final int timeInMinute;
  final String videoUrl;
  ExamVideoModel({
    required this.title,
    required this.timeInMinute,
    required this.videoUrl,
  });
}

final examVIdeoModelList = <ExamVideoModel>[
  ExamVideoModel(
      title: "របៀបបញ្ចូលពិន្ទុសិស្ស", timeInMinute: 2, videoUrl: "https://www.youtube.com/watch?v=cSR34CNXLvo"),
  ExamVideoModel(
      title: "របៀបបញ្ចូលពិន្ទុសិស្ស", timeInMinute: 2, videoUrl: "https://www.youtube.com/watch?v=cSR34CNXLvo"),
  ExamVideoModel(
      title: "របៀបបញ្ចូលពិន្ទុសិស្ស", timeInMinute: 2, videoUrl: "https://www.youtube.com/watch?v=cSR34CNXLvo"),
  ExamVideoModel(
      title: "របៀបបញ្ចូលពិន្ទុសិស្ស", timeInMinute: 4, videoUrl: "https://www.youtube.com/watch?v=cSR34CNXLvo"),
  ExamVideoModel(
      title: "របៀបបញ្ចូលពិន្ទុសិស្ស", timeInMinute: 2, videoUrl: "https://youtu.be/eKinhlKMVCI"),
];
