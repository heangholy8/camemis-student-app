import 'package:flutter/material.dart';

import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class HeadTitleBig extends StatelessWidget {
  final String title;
  final double? top;
  const HeadTitleBig({Key? key, required this.title, this.top = 0.0}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:  EdgeInsets.only(left: 17.0, bottom: 16.92, top: top!),
      child: Text(
        title,
        style: ThemeConstands.headline3_SemiBold_20
            .copyWith(color: Colorconstands.lightBlack),
      ),
    );
  }
}
