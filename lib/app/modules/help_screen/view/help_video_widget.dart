import 'package:flutter/material.dart';

import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class HelpVideoWidget extends StatelessWidget {
  final String titleVideo;
  final int timeVideo;
  void Function()? onTap;
  HelpVideoWidget({
    Key? key,
    required this.titleVideo,
    required this.timeVideo,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: onTap,
      padding: const EdgeInsets.all(0),
      child: Container(
        padding: const EdgeInsets.only(
            left: 14.0, top: 10.0, bottom: 10.0, right: 23.0),
        child: Row(
          children: [
            Container(
              margin: const EdgeInsets.only(right: 14.0),
              constraints: const BoxConstraints(maxHeight: 70, maxWidth: 76),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16.0),
                color: Colorconstands.gray300,
              ),
            ),
            Expanded(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                  Text(
                    titleVideo,
                    style: ThemeConstands.button_SemiBold_16
                        .copyWith(color: Colorconstands.lightBulma),
                  ),
                  const SizedBox(
                    height: 12.0,
                  ),
                  Text(
                    "$timeVideo mn",
                    style: ThemeConstands.caption_Regular_12
                        .copyWith(color: Colorconstands.neutralDarkGrey),
                  ),
                ])),
            const Icon(
              Icons.play_circle_fill,
              size: 50,
              color: Colorconstands.primaryColor,
            ),
          ],
        ),
      ),
    );
  }
}
