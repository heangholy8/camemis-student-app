import 'dart:ui';
import 'dart:math' as math;
import 'package:camis_application_flutter/app/modules/help_screen/view/custom_expansion.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../../../widgets/top_bar_widget/top_bar_one_line_widget.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';
import '../../guide_line/view/guid_line_screen.dart';
import '../widget/head_title.dart';
import 'help_list_modal_bottom_sheet.dart';

class HelpScreen extends StatefulWidget {
  const HelpScreen({Key? key}) : super(key: key);

  @override
  State<HelpScreen> createState() => _HelpScreenState();
}

class _HelpScreenState extends State<HelpScreen> {
  final searchHelpController = TextEditingController();
  bool isExpanded = false;
  ScrollController? controller;

  @override
  void initState() {
    controller = ScrollController();
    super.initState();
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            top: -220,
            left: 0,
            child: Transform.rotate(
                angle: math.pi / 2,
                child: Image.asset(
                  ImageAssets.Path_20,
                )),
          ),
          Positioned(top: 0, right: 0, child: Image.asset(ImageAssets.Path_22)),
          Positioned(
              bottom: 0, left: 0, child: Image.asset(ImageAssets.Path_21_help)),
          Positioned(
              bottom: 0,
              right: 0,
              child: Image.asset(ImageAssets.Path_19_help)),
          Positioned(
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 50, sigmaY: 50),
              // filter: ImageFilter.blur(sigmaX: 0, sigmaY: 0),

              child: Container(
                child: ListView(
                  controller: controller,
                  shrinkWrap: true,
                  padding: const EdgeInsets.only(
                      top: 50, bottom: 19, left: 0, right: 0),
                  scrollDirection: Axis.vertical,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(
                          left: 26, right: 26, bottom: 20),
                      child: TopBarOneLineWidget(
                          titleTopBar: "SUPPORT_CENTRE".tr()),
                    ),
                    Container(
                      alignment: Alignment.center,
                      margin: const EdgeInsets.only(
                          left: 16.0, top: 15.0, right: 16, bottom: 16.85),
                      padding: const EdgeInsets.only(left: 16, right: 16.0),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colorconstands.neutralGrey),
                          borderRadius: BorderRadius.circular(50)),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                            ImageAssets.SEARCH_ICON,
                            height: 16.0,
                            alignment: Alignment.center,
                            color: Colorconstands.darkTextsRegular,
                          ),
                          const SizedBox(
                            width: 15,
                          ),
                          Expanded(
                            child: TextField(
                              cursorRadius: const Radius.circular(50.0),
                              controller: searchHelpController,
                              decoration: InputDecoration(
                                contentPadding: const EdgeInsets.all(0),
                                border: InputBorder.none,
                                hintText: "HINT_SEARCH_HELP".tr(),
                                hintStyle: ThemeConstands.headline6_Medium_14,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    HeadTitleBig(
                        title: "CONTACT".tr()),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                              child: headOfficeWidget(
                            context: context,
                            onPressed: () {},
                          )),
                          const SizedBox(
                            width: 18,
                          ),
                          Expanded(
                              child: supportTeamWidget(
                            context: context,
                            onPressed: () {
                              setState(() {
                                showModalBottomSheet(
                                  shape: const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(16.0),
                                        topRight: Radius.circular(16.0)),
                                  ),
                                  context: context,
                                  builder: (context) {
                                    return HelpListModalBottomSheet();
                                  },
                                );
                              });
                            },
                          )),
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        HeadTitleBig(
                          title: "HOW_TO_USE".tr(),
                          top: 22,
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 12.0,
                    ),
                    CustomHelpExpand(
                      titleHead:
                         "FEATURES".tr(),
                      isExpand: isExpanded,
                      widget: Container(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          children: const [
                            GuidLineScreen(),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget cusDivider() => Container(
        height: 1.1,
        color: Colorconstands.neutralGrey,
      );

  Widget supportTeamWidget(
      {required void Function()? onPressed, required BuildContext context}) {
    final translate = context.locale.toString();
    return Container(
      //height: 193,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.2),
            spreadRadius: 5,
            blurRadius: 7,
            offset: const Offset(0, 3),
          ),
        ],
      ),
      child: MaterialButton(
        onPressed: onPressed,
        highlightColor: Colors.transparent,
        padding:
            const EdgeInsets.only(left: 16, right: 22, top: 18.5, bottom: 16),
        color: Colorconstands.lightBackgroundsWhite,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(8.0),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SvgPicture.asset(
              ImageAssets.profile_2users,
              height: 25.38,
              color: Colorconstands.primaryColor,
            ),
            const SizedBox(height: 9.1),
            Text(
               "CUSTOMER_SERVICE".tr(),
                style: ThemeConstands.headline5_SemiBold_16
                    .copyWith(color: Colorconstands.lightBulma)),
            const SizedBox(height: 10),
            Text("CLICK_HERE".tr(),
                style: ThemeConstands.subtitle1_Regular_16.copyWith(
                    color: Colorconstands.darkTextsRegular,
                    overflow: TextOverflow.clip)),
          ],
        ),
      ),
    );
  }

  Widget headOfficeWidget(
      {required void Function()? onPressed, required BuildContext context}) {
    final translate = context.locale.toString();
    return Container(
      //height: 163,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.2),
            spreadRadius: 5,
            blurRadius: 7,
            offset: const Offset(0, 3),
          ),
        ],
      ),
      child: MaterialButton(
        onPressed: onPressed,
        highlightColor: Colors.transparent,
        padding:
            const EdgeInsets.only(left: 16, right: 16, top: 18.5, bottom: 16),
        color: Colorconstands.lightBackgroundsWhite,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(8.0),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SvgPicture.asset(
              ImageAssets.logoCamis,color: Colorconstands.primaryColor,
              height: 25.38,
            ),
            const SizedBox(
              height: 9.1,
            ),
            Text( "HEAD_OFFICE".tr(),
                style: ThemeConstands.headline5_SemiBold_16
                    .copyWith(color: Colorconstands.lightBulma)),
            const SizedBox(height: 10),
            Text("WEEKDAY".tr(),
                style: ThemeConstands.subtitle1_Regular_16
                    .copyWith(color: Colorconstands.neutralDarkGrey)),
            const SizedBox(
              height: 5,
            ),
            Text(
                "DAYTIME".tr(),
                style: ThemeConstands.subtitle1_Regular_16.copyWith(
                    color: Colorconstands.neutralDarkGrey, fontSize: 14))
          ],
        ),
      ),
    );
  }
}
