import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class HelpContact {
  final String imgUrl;
  final String title;
  HelpContact({required this.title, required this.imgUrl});
}

class HelpListModalBottomSheet extends StatelessWidget {
  final String? imagUrl;
  final String? title;
  List<HelpContact> listHelpContact = [
    HelpContact(
        title: "Telegram",
        imgUrl: "https://www.freepnglogos.com/uploads/telegram-logo-png-0.png"),
    HelpContact(
        title: "Celcard",
        imgUrl:
            "https://images.khmer24.co/fields/21-10-25/m-cellcard-1635152775.png"),
    HelpContact(title: "Smart", imgUrl: ""),
    HelpContact(
        title: "Metfone",
        imgUrl:
            "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw0NEA8NDQ0ODQ0NEBAODQ0NDw8NDg0NFRIXFhURFRcYKC4sGhomHRUfITEiKCksLi42Fx8zODMtQyk5LisBCgoKDg0OFQ8QFi0mHR0rLS4vLSsrKy0rLSsrKy0tLSstLS4tKy0rLi4rKy0rMisrKy0rLS0rLy0tKysuLSstLf/AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEBAQADAQEAAAAAAAAAAAAAAQIFBgcEA//EAEAQAAIBAwEECAMEBgoDAAAAAAABAgMEERIFBiFREzEyQWFykbEHInEUgaGzNDVSYnSCFSMkQnODssPR8DOSov/EABoBAAMBAQEBAAAAAAAAAAAAAAECAwAEBQb/xAA3EQACAgEDAgQDBQUJAAAAAAAAAQIRAwQSITFBBVFhcYGx8BMiocHRMjM0kbIUFTVCcoKSouH/2gAMAwEAAhEDEQA/AOq1ZvVLi+0+98zGt836stXtS8z9zJwn2yXBrU+b9WNb5v1ZDQApDU+b9WXU+b9WQotjqI1Pm/Vmtb5v1ZkGsZRNa5c36sanzfqzJRbHURqfN+rGp836sYLg1h2+g1Pm/VjW+b9WTBQWHb6DW+b9WNb5v1YGDWbb6DU+b9WTVLm/VlwTAQbfQanzfqxrlzfqwDWDaXW+b9WZ1Pm/VgBsRxGp836smp836spA2K4k1vm/VjW+b9WUyMI0Nb5v1Y1vm/VggbBRdb5v1ZTICaj9Kval5n7mDVXtS8z9wKFdAAUUokAAAdIpC4KKOkMAFBY9EBvBMC2GiYGC4Lg1jbTOBg1gmDWbaZBrBcGsWjAwUg1gogKMBFoyABibQIUBEaMENmRhCAoMA3V7UvM/cyfpV7UvM/cyKx4rgAABRIGkRFFZRIFBRbHSLghQJZRRANDALH2gDAwCw7QBgYNZtpkGsANg2mSYNmQ2I4mSGiDCOJkhog9k2QADCMgKQJNmQaIEQ3V7UvM/cha3al5n7kAykUUIhoQqkCgqFZRIFBoRsrGIGClFsqokwXAAlj7SkKAWNRCgGs1EwTBQGxdpnAKBkxGjBDTIOmSlEyyGiDE5IyyGiMcmyAAZEpEIaARTVXtS8z9zKNVe1LzP3MispEqKCoVlUVFIik2VijRURFQrLRRUUAmVSAAMEAAxgADGAAMYgKQwGiMhWRjpk5IyRmzI6ISRlmWaZB0SkiEKyDomyAoCTN1e1LzP3Mo1V7UvM/ciAykQaRlGkTZVFKiG0Iy8UVAIE2WiigAA4ARZRlHtRlHzLBjepAPBd56Fu38Oekpxq306lJzScaFNRjUhF4xqck8P93HDn3J4Y5TdRRzarWYdLFTyypPp5v2R56D1La+4Gz6NvcVYdPqpUatWOucHHXGDksrHVlHlr7w5MUsbqQmj12HVxcsV0uOVQABM7AADGIRmiMKFkjDIaZkoiEkRmTTIx0RkRmWVgckyAAYU1V7UvM/ciNVu1LzP3IKxoK6PosrOtXkqdClOrN8VCnCc5Y5tLqXj1HK3G6O1Kcdc7KrjrbprpGl5Yt+x6nsm0tth2HSTWJRpxqXM0lrq1nwUE/q9MV4/VnF7B+ItK6rwt6ltOh0slClPpFU+d8IqXBYy+GVni19Sv2MFSnKmzyf7z1GTfPT4VKEOrfV13XK9+E6R5ns3ZN1dScbahOs1wemLcY8tT7vvZ9u0N19o2sHUrWs1BLMpRxUjFc5OLeF4s9i27tS32ZQncSgkpTbjCmoxdavLj68G2+SfWcNutv1S2hW+zyoSt6rUnTTnGpGbXFxzhYljL6scHxM9PjT2ynywx8W1U4PPjwL7OPW3z691df6Wl3PIEbpU5SemMZNt4jGKcpN8kl1s7Z8Qtgxt7uH2aDULtaowhHqramnBJdzbTS5yZ3bYOxbXYts69bT0qhquK0sNxbSzSh4Zwklxk8fdzx07lOUXxt6s9LN4tix6fHmhFyeT9mPdvvfs+OOr49V5xS3O2rKOpWlTHX87jGf/AKvD/A4q+2fXtnpr0alGXHEakZxbS71nrXij0ep8ToSnoo2NSpGUlGDlWUZybeEtMYvj4ZO3VLalfUNNzbyjGpx6Kuo9JTfc1pb0y5NPKz3dRSOnx5LWOfK81/4cmTxfV6VxeqwJRl0prd83ftSPINwf1la+aX5cju3xQ2ZcXStFb0alZwdZy0Rc9OejxnH0fodb2LsxWW26VtGopxp1JaXFxy4OEmk8dTWcNeHid+3s3njsxUXKjKr07mlpko6dGnn5vwGxRX2M4zdc/oS1+ab8Q02TTx3NwtLpae/1Xb5HmWyNg3VG7tJXNtUp0p3VGGakJRjKTqJ4480n6HpG/wA7x2yhZRqyqVaiU+hUtSoqMnJZXVngvw7zrdbfOG0q9jbxt50nG+tquqUlNNKTjjCX734Hb96t4I7NpQrOlKrrqdHpjJRx8rlnj9BsSgoZEpceZDXZNTPUaaU8K381Hqnz7nktfY21YwnOrQu4wgpSnJqppUUsybz3YPj2XsitdtqmlGEONStUkoUaMP2pTfBe/DqO/S37jfxlZUrSSqXcZW8JSqqUYupFx1PC6lnL8EzhpzhUbt7eMpWlpNxp04LV9prx7d1US7Tb7PcscO7HNLHDjbK1+Z60NbqtrWXEoy69bSXS3z58JcX16JnwUNh2SXzXF1cvvdnat0f5alVpSXikYq7AtptRoXcqVWTxCjf0XadJL9mNTjGUvDgc7Uo3MVmVCvFc5UqiXq0fHO5jJOMkpRksSjJKUWuTRuO8fn+pOOfUXuU7/wCNf0/ozqd5aVbecqdanOnOLxJSWlrx8V4rgz8YpvguLfBJdbfI7bKlG8hOzk9dajSnXsKk25TlCCbnaNvjJY+aPL37D8MNgwjS+31YqVWo5RoZSkoQi0nNeLkseCXiwRwOc1GL6nRm8ThgwSy5FyuKXdvpTfZ03zbVPrSvptDc/alSLkrOoo9yqYpzz5ZcfwOKvrGvbS6OvSnRn16ZqVNtc1nrXiuB6XtX4lUaNWVOjbu4hCTjKr0qpptPi4pJ5Xuc1/Y9u2b4PTLMU5JOrbV8d3isp8mnyZZYMc7WOfK+vrqcMvFtXh25NVgSxvuuqvzVv+TSs8QZ9Wz9lXN03G3oVKzXa0wk4x80u772c3u5utO6vqlpVzCFs5K4nT5Qlp0p833eGX3Hou3N4LLYtOFCFP5lHNO2o6Y4jxxObfUm118W3nxFxYtycpOkX13iP2c44cEd+SXNdkvX4c9kly2eX3O6G1aUdUrKq49/RYrP0jl/gcFOLjlSWGm00+DTXWmewbs78z2hW6GOz6sYrGqpTqxqqEX1SllJJffl4eEz9N/92qF1QqXKjClc0YOp0rcYRqRiuMKjfDq6m+p444LfYxcXLG7rzOJeKZcWZYdXjUW65i7q+lq5fO/RnjLMs2ZZJHqyIAAiG6val5n7mX1f95Fq9qXmfuR9QJdymL9qPw/I9v8AiX+rLj/J/NieT7r/AKbZ/wAVQ/OiesfEv9WXH+T+bE8m3X/TbP8Airf86JXUfvF8Pmzx/Bf4LJ7y/piemfFz9Co/xUPyap0b4c/rO1+lX8qZ3n4ufoVH+Kj+TVOjfDr9Z2v0q/lTBm/fr3RTw3/C8ntk+TPQt5aMJ7S2QppYUriSf70YRlD/AOkjO/uyru++zW1uv6tznOvOTcYQikknJ/zvCXHh6cX8T72VtX2bcQ7VKVWcV1JtOD0vwaWPvO6bN2jTu6Ebi3alGpHMU32amOxPHU0+DLVGcsmN96fwpfoeS5ZdPh0mqirSUkr6KW6fL+Dtexw2xd37DY9Ppqko9JBfPdVdMcZ7oL+7nqwst9WWcLV3tqbTuobPsZztqVRzUrlRzXahGTen9lcPN1dXUcVvNsTb15W/rqfSQTfRqjUirdR7nFNrH83E7HuLujKxzc3Di7iUdMYReqNKLxnj3yeO7q8ck4ucpKEI7Yrr9fT9TsnDT4cUtTqMyyZmvupNNJ9uPTrzS4pLoziaO7EdmbS2c1XnX+0Tr5coqDi4U1yfHOr8D9PjH1WXmr+1M+fae8VKvti0cZLoLafQxm5JwlOWVKpF8uKWeSz3nO/EXd642hCg7ZRlKhKo3CUoxclJLim+GVp72usVxUseSOPzX5foVjlyY9Zo8urlTcHbfHXfSfRLiUb6VZ5tur+nWf8AEUfzYnoXxc/RKP8Aj/7czqmy9176yubKtc0o04O8t4RaqU5PU55xiOe5M7X8W/0Sj/j/AO3MSCaw5Ezr1WWGTxDSShJNc8p2u/dHStwNCvYTn1U6VefDi8KjJPC7+tnbY7/UMq3sbWNNYxB15wtqan3RUY8OPjKKPP8AYF/9luaNdrVCEvniuLnSkmpxXjpbPv23sj7NWai9VGaVS3qR6p0JcYzT+nAgs8scPu+fP4UdWr0eDPnvMr+7xy6tOV8JrzVc9G+nU7dd7z7bg3mxdPHfG1ryT+/LT+5nyVrq+vcK42TKbfVWo0attVT5uo8p/Ro4TZe37+0ShQryVNcFTlicIx5JPOlfTBzEt/NotY00Pr0c8/6hlq8c196T/BnFLRzxO8OHHfncov4rr/2fuY2Lu5eU7y3uammkqVWOmEmnOUXLj2c4ynzO67HhGns3TT4RjRuFDHclKeDpVltq7aqX9zWfRW6apQWIQrXbi+jhhdpLOt5zjCZ3Dd552RTfXm1qe0jq07g+YX0b569v0+J5/iSzvHeZxdSivu3t6Sdc8tq/ZWeJrv8A+956j8IW+huV3dJB48XF/wDB5dz/AO956j8IP/Ddf4lP/Szl0n7yJ73j/wDB5fdf1HP7s0aarbSqJLXK9lCb79MacXH8Zy9WdYq7mV9o31xc3cpUbdVZxpxWOlq04y0Rwu6LUU8v7lxyfpsrb0bXa99bVnppXNXEZN8IVl1fRPOM88Hbd5f6Q6F/0cqXTf3ulaU0sf3M/Lq83A7Uozhz/lb4XufNTyZ9NqPutJ5YQqT7Ko9+1VXp18jjtp7X2dsSiqMYxg0swtaOOkm3wU5t9S4dqXXh9Z12zs7jeSlOvcXUrejTrunTtqVPNL5Yp6nl/NL5ut/djODr1Dcja9zVbrUnBylmpWuKsZce+WVltno/9l2FYYzmNFPGdMZ3Nw+OMc219yXJGjuyP76qK7FMscWlilgnvzyf7SqVfNK+ndvrfNHiu1rRW1xXt1LUqNWpSUmsOSjNrVjxwfGz9birKpOVSbzKpOTm+c5NuT9WfkzmR9FTSSfUAAIpur2peZ+5l/8AJqr2peZ+5lAaDB1TO47wb+1r+3qWsranTjU0apwcpSWmWpcGvA61s26dvWpV0lKVGpCoovgpOEk0n6HyGkCcnJ22DDp8WKDhCNJ9ue/He+yO17z761dpUY0J28KUY1Y1VODlJtqMljiv3vwOH2BtSVjcU7qEFN09eIybjGWuLjxa+ufuONKhJSk3ub5LYdPihjeKMai7Vc9+vds7DvTvPU2m6OujGj0OvCg5S16sdefp+J82w94Luwk5UJ4i+1SmnOnPzLn6PxOJRScpy3br5LQ02KOL7BQWzy6rrff1dnolH4pzSxUsFKX7VOrKMM/Rp+5wW3d+L29i6S029J5U40tWqUeUpPi/osJ+J1kDSz5JKnL6+BLF4Xo8U1OGJWvd/NtfgPHvO4bJ+Id7QgqdSMLlRXyzqalUx3Jtdf3rPidPAkJyg7i6OjPpsWojtywUl6/Vo7jtPf8Aq3XQaranH7PcU7mOmcnqlTziL4cE89Z8e8++FTaVKNGdCFFQqdJqi5SbelxxxXidaAzzTaab6ksfh+mxuMoY0nG65fF9e4Ob2Rt2MIfZbun09q5OUHHEa9CT65UpPn3xfB+HHPCAROjoyY45FtkvrzT7P66Wjtkbewq8aO0KUF16bqnVozguTcU45+hiX9HUcupdu6ks/wBTaQlFylydWphJeVZOrARQxrnYvxa/k3X4EP7J55JV/t+e35U/U5Ha+2Kl24x0xo0KScaFtTyoU4t8frJ98nxfh1HPWG/lahaxs429OUY05UtblJSaefmx951AFY5JRdpjZNHgywjjnBNR5S5/J/MnP1Oxbr72VNmRqwhQhW6WUZNyco6cLGFhHXQwQk4u0x9Rihmi4ZFafbn8mj6tsX7u69W5lFQlWfSSgnqjHqWMv6HP7E3/AL60iqU1C5prsqq5KpFclNd31TOpshWE5J7kzmzafFkgsc4JxXRPt29+nqeiXHxVqNPorKEJft1KsqkV/Koxz6nS9ubcur+fSXNRyxlQpr5YQT61Fd3178LLZxzMso8kpftM5cWi0+B3igk/Plv+bbf1zZGQ0zLMVYAAwpqr2peZ+5DVXtS8z9zIGaJSoyjSFZVGjRhFJsrE0iohUI0WTKUgEKplABggAGMAAYwABjAgBgNhmWGRjpEpMpkEY5KTIyFZljojIMhSDomwQAJM3V7UvM/cyWr2peZ+5DM0WUpAhSyNIqMlQhSLNGjBRWikZGwQZEaKqRopnJci0U3FBCgDYAIY1lIMjIaBuBCZAyRNyIyADJE5SBlhkHJNgjDIOTYAAyRKTIAQIp+lXtS8z9zIq9qXmfuZMwRfBoAAKJlRTJRWUs0UyUQopGjRjJRaHUi5LkyAUOpGsjIAKG3DIyAajbhkGQGgbjRkEyERyBMggxNsEYAxNsEAGQjYMmiDE2CEARDVXtS8z9wKval5n7mQgXQ2CFFKJgAAHs1kEApSylM5KLQykayMmS5BQ241kGcjIKDuNAzkZNRtxcjJMkDQNxcghMhoXcUEyQYSykADQrYAIMTbsGQAiFBkBMaq9qXmfuQ1V7UvM/cwZiroU0YKYY0UhBR7NAAFDbiggBQ9mskyQADZoGQYO40DIMbcXJcmQYFlBAGgWAAFIRyAMlCLYMgBEBAAgAAGAbq9qXmfuYAMZdECgChBogAEGgADkDAAEoAMOAAAwABjAABMRAAwgIAEBTLACIymWUBMQADCgAAMf//Z"),
  ];

  HelpListModalBottomSheet({Key? key, this.title, this.imagUrl})
      : super(key: key);

  _launchURL({required String url}) async {
    url = url;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.only(
              left: 26.0, top: 18, bottom: 8.5, right: 24),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "NEED_HELP".tr(),
                style: ThemeConstands.headline4_Medium_18
                    .copyWith(color: Colorconstands.neutralDarkGrey),
              ),
              Text(
                "CAMEMIS Support",
                style: ThemeConstands.headline4_Medium_18
                    .copyWith(color: Colorconstands.lightBulma),
              )
            ],
          ),
        ),
        const Divider(
          color: Colorconstands.neutralDarkGrey,
        ),
        Wrap(
          children: [
            CustomHelpWidget(
              title: "Telegram",
              imgUrl:"assets/icons/png-transparent-telegram-logo-computer-icons-telegram-logo-blue-angle-triangle-thumbnail.png",
              onPressed: () {
                _launchURL(url: 'https://t.me/camemissolution');
              },
              colorback: const Color(0xff2ba1da),
            ),
            cus_divider(context),
            CustomHelpWidget(
              title: "Cellcard",
              imgUrl:
                  "assets/icons/cellcard_icon.png",
              onPressed: () {
                _launchURL(url: 'tel://077926316');
              },
              colorback: const Color(0xfffaa31d),
            ),
            cus_divider(context),
            CustomHelpWidget(
              title: "Smart",
              imgUrl:
                  "assets/icons/smartnas_logo.png",
              onPressed: () {
                _launchURL(url: 'tel://010926316');
              },
              colorback: const Color(0xff04ab53),
            ),
            cus_divider(context),
            CustomHelpWidget(
              title: "Metfone",
              imgUrl:
                  "assets/icons/metfone_icon.png",
              onPressed: () {
                _launchURL(url: 'tel://0718664316');
              },
              colorback: const Color(0xffe30414),
            ),
          ],
        ),
        const SizedBox(height: 20.0),
      ],
    );
  }

  Widget cus_divider(BuildContext context) => Divider(
        height: 1,
        indent: MediaQuery.of(context).size.width * 0.22,
        color: Colorconstands.neutralGrey,
      );
}

class CustomHelpWidget extends StatelessWidget {
  final String imgUrl;
  final String title;
  Color? colorback;
  void Function()? onPressed;

  CustomHelpWidget(
      {Key? key,
      required this.title,
      required this.imgUrl,
      this.onPressed,
      this.colorback})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: onPressed,
      padding: const EdgeInsets.only(left: 17, top: 8, bottom: 8, right: 30),
      child: Row(
        children: [
          Container(
            margin: const EdgeInsets.only(right: 20.79),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: colorback ?? Colors.white,
            ),
            // color: colorback,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(27.5),
              child: Image.asset(
                imgUrl,
                fit: BoxFit.cover,
                width: 55,
                height: 55,
              ),
            ),
          ),
          Expanded(
              child: Text(
            title,
            style: ThemeConstands.button_SemiBold_16,
          )),
          const Icon(
            Icons.arrow_forward_ios,
            size: 24,
            color: Colorconstands.neutralDarkGrey,
          )
        ],
      ),
    );
  }
}
