import 'package:flutter/material.dart';

import '../../../../widgets/expansion/expand_section.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class CustomHelpExpand extends StatefulWidget {
  final String titleHead;
  bool isExpand;
  Widget widget;
  CustomHelpExpand({
    Key? key,
    required this.titleHead,
    required this.widget,
    required this.isExpand,
  }) : super(key: key);

  @override
  State<CustomHelpExpand> createState() => _CustomHelpExpandState();
}

class _CustomHelpExpandState extends State<CustomHelpExpand>
    with TickerProviderStateMixin {
  late Animation _arrowAnimation;
  late AnimationController _arrowAnimationController;
  final GlobalKey expansionTileKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    _arrowAnimationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 300));
    _arrowAnimation =
        Tween(begin: 0.0, end: 3.14).animate(_arrowAnimationController);
  }

  void _scrollToSelectedContent({required GlobalKey expansionTileKey}) {
    final keyContext = expansionTileKey.currentContext;
    if (keyContext != null) {
      Future.delayed(const Duration(milliseconds: 200)).then((value) {
        Scrollable.ensureVisible(keyContext,
            duration: const Duration(milliseconds: 200));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      key: expansionTileKey,
      margin: const EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
          border: Border.all(color: Colorconstands.neutralGrey, width: 1.2),
          color: Colorconstands.lightBackgroundsWhite,
          borderRadius: BorderRadius.circular(16.0)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          MaterialButton(
            highlightColor: Colors.transparent,
            focusColor: Colors.transparent,
            splashColor: Colors.transparent,
            padding: const EdgeInsets.all(0),
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16.0),
              ),
            ),
            onPressed: () {
              setState(() {
                widget.isExpand = !widget.isExpand;
                _arrowAnimationController.isCompleted
                    ? _arrowAnimationController.reverse()
                    : _arrowAnimationController.forward();
                _scrollToSelectedContent(expansionTileKey: expansionTileKey);
              });
            },
            child: Container(
              padding: const EdgeInsets.only(
                  left: 16, top: 16, bottom: 16, right: 21),
              child: Row(
                children: [
                  Text(
                    widget.titleHead,
                    style: widget.isExpand
                        ? ThemeConstands.headline5_SemiBold_16.copyWith(
                            color: Colorconstands.lightBulma,
                            fontWeight: FontWeight.bold)
                        : ThemeConstands.button_SemiBold_16
                            .copyWith(color: Colorconstands.lightBulma),
                  ),
                  const Spacer(),
                  Align(
                    alignment: Alignment.centerRight,
                    child: AnimatedBuilder(
                      animation: _arrowAnimationController,
                      builder: (context, child) => Transform.rotate(
                        angle: _arrowAnimation.value,
                        child: const Icon(
                          Icons.expand_more_outlined,
                          size: 24.0,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          AnimatedCrossFade(
            firstChild: Container(height: 1, color: Colorconstands.neutralGrey),
            secondChild: const SizedBox(),
            crossFadeState: widget.isExpand
                ? CrossFadeState.showFirst
                : CrossFadeState.showSecond,
            duration: const Duration(milliseconds: 400),
          ),
          ExpandedSection(expand: widget.isExpand, child: widget.widget),
        ],
      ),
    );
  }
}
