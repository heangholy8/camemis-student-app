class AnnouncementNotificationModel {
  final String? announcementStatus;
  final String? announcementTitle;
  final String? dateTime;
  final int? notificationType;
  final int? historyNotification;
  final String? profileNotification;
  final bool? seen;

  AnnouncementNotificationModel({
    required this.announcementStatus,
    required this.announcementTitle,
    required this.dateTime,
    required this.notificationType,
    required this.historyNotification,
    required this.profileNotification,
    required this.seen,
  });
}

final announcementNotificationModelList = <AnnouncementNotificationModel>[
  AnnouncementNotificationModel(
    seen: false,
    announcementStatus: "កិច្ចការផ្ទះ",
    announcementTitle: "[បេន តារារស្មី] មានកិច្ចការផ្ទះ គណិតវិទ្យា",
    dateTime: "១១ តុលា ២០២២",
    notificationType:1,//home work
    historyNotification:1,//today
    profileNotification: "https://progress.scalingupnutrition.org/wp-content/uploads/2021/01/honduras-1.jpg"
  ),
  AnnouncementNotificationModel(
    seen: true,
    announcementStatus: "លទ្ធផលប្រឡង",
    announcementTitle: "[ដំណឹងលទ្ធផលប្រឡង] ភាសាខ្មែរ 7:30 - 9:00",
    dateTime: "១១ តុលា ២០២២",
    notificationType:2,//result
    historyNotification:1,//today
    profileNotification: "https://www.rfa.org/english/news/cambodia/schools-03042022195010.html/@@images/d3ec8d58-efb6-458b-9907-b845c36b70c3.jpeg"
  ),
  AnnouncementNotificationModel(
    seen: true,
    announcementStatus: "ដំណឹងវត្តមាន",
    announcementTitle: "[ដំណឹងអវត្តមាន] ភាសាខ្មែរ 7:30 - 9:00",
    dateTime: "១១ តុលា ២០២២",
    notificationType:3,//attendance
    historyNotification:2,// before
    profileNotification: "https://progress.scalingupnutrition.org/wp-content/uploads/2021/01/honduras-1.jpg"
  ),
  AnnouncementNotificationModel(
    seen: true,
    announcementStatus: "សំបុត្រថ្នាក់រៀន",
    announcementTitle: "គម្រោងកែលម្អគុណភាពអប់រំចំណេះទូទៅ",
    dateTime: "១១ តុលា ២០២២",
    notificationType:4,//ionbox in class
    historyNotification:2,// before
    profileNotification: "https://www.rfa.org/english/news/cambodia/schools-03042022195010.html/@@images/d3ec8d58-efb6-458b-9907-b845c36b70c3.jpeg"
  ),
  AnnouncementNotificationModel(
    seen: true,
    announcementStatus: "សេច្តីជូនដំណឹង",
    announcementTitle: "គម្រោងកែលម្អគុណភាពអប់រំចំណេះទូទៅ",
    dateTime: "១១ តុលា ២០២២",
    notificationType:5,//information school
    historyNotification:2,// before
    profileNotification: "https://progress.scalingupnutrition.org/wp-content/uploads/2021/01/honduras-1.jpg"
  ),
  AnnouncementNotificationModel(
    seen: true,
    announcementStatus: "ដំណឹងបន្ទាន់",
    announcementTitle: "គម្រោងកែលម្អគុណភាពអប់រំចំណេះទូទៅ",
    dateTime: "១១ តុលា ២០២២",
    notificationType:6, // info emergecy
    historyNotification:2,// before
    profileNotification: "https://www.rfa.org/english/news/cambodia/schools-03042022195010.html/@@images/d3ec8d58-efb6-458b-9907-b845c36b70c3.jpeg"
  ),
];
