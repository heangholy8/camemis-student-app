import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../core/resources/asset_resource.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class AnnouncementWidget extends StatelessWidget {
  String statusAnnouncement;
  String titleAnnouncement;
  String dateTime;
  String profileImage;
  int notificationType;
  bool seen;
  AnnouncementWidget({
    Key? key,
    required this.statusAnnouncement,
    required this.titleAnnouncement,
    required this.dateTime,
    required this.notificationType,
    required this.seen,
    required this.profileImage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: seen==true? Colorconstands.neutralWhite :Colorconstands.neutralSecondary,
          border:const Border(bottom: BorderSide(color: Colorconstands.neutralGrey))),
      padding: const EdgeInsets.only(left: 20.0, top: 12.0, bottom: 12),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 60,width: 60,
            child: Stack(
              children: [
                Container(
                    child:  CircleAvatar(
                  radius: 25, // Image radius
                  backgroundImage: NetworkImage(profileImage),
                )),
                Positioned(
                  bottom: 2,right: 5,
                  child: Container(
                    height: 25,width: 25,
                    decoration: BoxDecoration(
                      color: notificationType == 6
                          ? Colorconstands.alertsDecline
                          : notificationType == 2 || notificationType == 3?Colorconstands.alertsAwaitingText:Colorconstands.primaryColor,
                      shape: BoxShape.circle
                    ),
                  ),
                )
              ],
            ),
          ),
          const SizedBox(width: 12,),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(16.0),
                      color: notificationType == 6
                          ? Colorconstands.alertsDecline
                          : notificationType == 2 || notificationType == 3?Colorconstands.alertsAwaitingText:Colorconstands.primaryColor),
                  padding:
                      const EdgeInsets.only(left: 10, right: 10, top: 6,bottom: 6),
                  child: Text(
                    statusAnnouncement,
                    style: ThemeConstands.caption_Regular_12
                        .copyWith(color: Colorconstands.neutralWhite),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 10.0, bottom: 14.0, right: 25.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Text(
                          titleAnnouncement,
                          style: ThemeConstands.headline5_Medium_16
                              .copyWith(color: Colorconstands.neutralDarkGrey),
                        ),
                      ),
                    ],
                  ),
                ),
                Row(
                  children: [
                    SvgPicture.asset(
                      ImageAssets.doucment_icon,
                      height: 18,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Text(
                        dateTime,
                        style: ThemeConstands.headline6_Regular_14_24height.copyWith(
                          color: Colorconstands.neutralDarkGrey,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
