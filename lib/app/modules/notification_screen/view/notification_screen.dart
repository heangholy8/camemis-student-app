import 'package:flutter/material.dart';
import '../../../../widgets/navigate_bottom_bar_widget/navigate_bottom_bar.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';
import '../data/announcement_notification_model.dart';
import '../widgets/announcement_list_widget.dart';

class NotifictaionScreen extends StatefulWidget {
  const NotifictaionScreen({Key? key});

  @override
  State<NotifictaionScreen> createState() => _NotifictaionScreenState();
}

class _NotifictaionScreenState extends State<NotifictaionScreen>with SingleTickerProviderStateMixin {
      List<AnnouncementNotificationModel> data = announcementNotificationModelList;

   bool paid = true;
  @override
  void initState() {
    super.initState();
  }
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var datatoday = data.where((element) {
      return element.historyNotification==1;
    },).toList();
    var databefor = data.where((element) {
      return element.historyNotification==2;
    },).toList();
    return Scaffold(
      backgroundColor: Colorconstands.neutralWhite,
      bottomNavigationBar: BottomNavigateBar(isActive: 4,paid: paid,),
      body: Container(
        color: Colorconstands.primaryColor,
        child: Stack(
          children: [
            Positioned(
              top: 0.0,
              left: 0.0,
              bottom: 0,
              right: 0,
              child: Container(
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Container(
                            margin:const EdgeInsets.only(top: 65,bottom: 25,left: 18,right: 18),
                            child: Text(
                              'ការជូនដំណឹង',
                              style: ThemeConstands.headline2_SemiBold_24.copyWith(
                                color: Colorconstands.neutralWhite,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Expanded(
                      child: Container(
                        child: ListView(
                            shrinkWrap: true,
                            padding: const EdgeInsets.all(0),
                            children: [
                              Container(
                                color: Colorconstands.neutralGrey,
                                padding:const EdgeInsets.symmetric(horizontal: 18,vertical: 12),
                                child: Text(
                                  'ថ្ងៃនេះ',style: ThemeConstands.headline6_Medium_14.copyWith(color: Colorconstands.neutralDarkGrey),
                                ),
                              ),
                              Container(
                                child: ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: datatoday.length,
                                  padding: const EdgeInsets.all(0),
                                  shrinkWrap: true,
                                  itemBuilder: (context, index) {
                                     return Container(
                                      child: AnnouncementWidget(
                                        profileImage: datatoday[index].profileNotification!,
                                        seen: datatoday[index].seen!,
                                        notificationType: datatoday[index].notificationType!,
                                        dateTime: datatoday[index].dateTime.toString(), 
                                        statusAnnouncement: datatoday[index].announcementStatus.toString(), 
                                        titleAnnouncement: datatoday[index].announcementTitle.toString(),
                                      ),
                                     );
                                  },
                                ),
                              ),
                              Container(
                                color: Colorconstands.neutralGrey,
                                padding:const EdgeInsets.symmetric(horizontal: 18,vertical: 12),
                                child: Text(
                                  'Earlier',style: ThemeConstands.headline6_Medium_14.copyWith(color: Colorconstands.neutralDarkGrey),
                                ),
                              ),
                              Container(
                                child: ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: databefor.length,
                                  padding: const EdgeInsets.all(0),
                                  shrinkWrap: true,
                                  itemBuilder: (context, index) {
                                     return Container(
                                      child: AnnouncementWidget(
                                        profileImage: databefor[index].profileNotification!,
                                        seen: databefor[index].seen!,
                                        notificationType: databefor[index].notificationType!,
                                        dateTime: databefor[index].dateTime.toString(), 
                                        statusAnnouncement: databefor[index].announcementStatus.toString(), 
                                        titleAnnouncement: databefor[index].announcementTitle.toString(),
                                      ),
                                     );
                                  },
                                ),
                              ),
                            ]),
                      )
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
