import 'package:comment_tree/comment_tree.dart';
import '../../../routes/e.route.dart';
import '../../homework_screen/bloc/homework_bloc.dart';

class TabForumLesson extends StatelessWidget {
  final double width;
  const TabForumLesson({
    Key? key,
    required this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeworkBloc, HomeworkState>(
      builder: (context, state) {
        if (state is HomeWorkLoadingState) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        if (state is LessonLoadedState) {
          var forum = state.lessonModel.data!.forums;
          return Container(
            color: Colors.white,
            padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 16),
            child: forum?.length != 0
                ? ListView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: forum?.length,
                    itemBuilder: (context, index) {
                      return CommentTreeWidget<Comment, Comment>(
                        Comment(
                          avatar:
                              '${forum?[index].userProfile?.profileMedia?.fileThumbnail}',
                          userName: '${forum?[index].userProfile?.name}',
                          content: "${forum?[index].comment}",
                        ),
                        List.generate(
                          forum![index].children!.length,
                          (subindex) => Comment(
                              avatar:
                                  '${forum[index].children![subindex].userProfile?.profileMedia?.fileThumbnail}',
                              userName:
                                  '${forum[index].children![subindex].userProfile?.name}',
                              content:
                                  '${forum[index].children![subindex].comment}'),
                        ),
                        treeThemeData: TreeThemeData(
                            lineColor: forum.isEmpty
                                ? const Color(0xFFDEDEDE)
                                : Colors.grey.shade100,
                            lineWidth: 1.5),
                        avatarRoot: (context, data) => PreferredSize(
                          child: InkWell(
                            onTap: () {
                              print("Main Profile Clicked");
                            },
                            child: CircleAvatar(
                              radius: 24,
                              backgroundImage: NetworkImage(
                                  "${forum[index].userProfile!.profileMedia!.fileThumbnail}"),
                            ),
                          ),
                          preferredSize: const Size.fromRadius(18),
                        ),
                        avatarChild: (context, data) => PreferredSize(
                          child: InkWell(
                            onTap: () {
                              print("Child Profile Clicked");
                            },
                            child: CircleAvatar(
                              radius: 18,
                              backgroundImage: NetworkImage('${data.avatar}'),
                            ),
                          ),
                          preferredSize: const Size.fromRadius(12),
                        ),
                        // Reply Container Box
                        contentChild: (context, data) {
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 14, horizontal: 14),
                                decoration: BoxDecoration(
                                    color: Colors.grey[100],
                                    borderRadius: BorderRadius.circular(12)),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      '${data.userName}',
                                      style: ThemeConstands.texttheme.subtitle1!
                                          .copyWith(
                                        fontWeight: FontWeight.w600,
                                        color: Colors.black,
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 8,
                                    ),
                                    Text(
                                      ' ${data.content}',
                                      style: ThemeConstands.texttheme.bodyText1
                                          ?.copyWith(
                                        fontWeight: FontWeight.w500,
                                        color: Colorconstands.darkGray,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              DefaultTextStyle(
                                style: Theme.of(context)
                                    .textTheme
                                    .caption!
                                    .copyWith(
                                        color: Colors.grey[700],
                                        fontWeight: FontWeight.bold),
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 16),
                                  child: Row(
                                    children: [
                                      const SizedBox(
                                        width: 8,
                                      ),
                                      InkWell(
                                          onTap: () {
                                            print("Like");
                                          },
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              SvgPicture.asset(
                                                "assets/icons/fi_like_icon.svg",
                                                width: 12,
                                              ),
                                              const SizedBox(
                                                width: 6,
                                              ),
                                              Text(
                                                'Like',
                                                style: ThemeConstands
                                                    .texttheme.bodyText2!
                                                    .copyWith(
                                                  fontWeight: FontWeight.w600,
                                                  color: Colorconstands.darkGray,
                                                ),
                                              ),
                                            ],
                                          )),
                                      const SizedBox(
                                        width: 24,
                                      ),
                                      InkWell(
                                        onTap: () {
                                          print("Comment");
                                        },
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            SvgPicture.asset(
                                              "assets/icons/fi_reply_icon.svg",
                                              width: 12,
                                            ),
                                            const SizedBox(
                                              width: 6,
                                            ),
                                            Text(
                                              'Reply',
                                              style: ThemeConstands
                                                  .texttheme.bodyText2!
                                                  .copyWith(
                                                fontWeight: FontWeight.w600,
                                                color: Colorconstands.darkGray,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          );
                        },
                        contentRoot: (context, data) {
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 8, horizontal: 8),
                                decoration: BoxDecoration(
                                    color: Colors.grey[100],
                                    borderRadius: BorderRadius.circular(12)),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      '${data.userName}',
                                      style: ThemeConstands.texttheme.subtitle1!
                                          .copyWith(
                                              fontWeight: FontWeight.w600,
                                              color: Colors.black),
                                    ),
                                    const SizedBox(
                                      height: 4,
                                    ),
                                    Text(
                                      '${data.content}',
                                      style: ThemeConstands.texttheme.bodyText1
                                          ?.copyWith(
                                        fontWeight: FontWeight.w500,
                                        color: Colorconstands.darkGray,
                                      ),
                                    ),
                                  ],
                                ),
                              ),

                              /// LIKE AND REPLY BUTTON
                              DefaultTextStyle(
                                style: Theme.of(context)
                                    .textTheme
                                    .caption!
                                    .copyWith(
                                        color: Colors.grey[700],
                                        fontWeight: FontWeight.bold),
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 6),
                                  child: Row(
                                    children: [
                                      const SizedBox(
                                        width: 8,
                                      ),
                                      InkWell(
                                        onTap: () {
                                          print("Like");
                                          // setState(() {
                                          //   _isLike = !_isLike;
                                          // });
                                        },
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            SvgPicture.asset(
                                              "assets/icons/fi_like_icon.svg",
                                              width: 12,
                                              color:
                                                  //  !_isLike
                                                  //     ? Colorconstands.darkGray
                                                  //     :
                                                  Colorconstands.primaryColor,
                                            ),
                                            const SizedBox(
                                              width: 6,
                                            ),
                                            Text(
                                              'Like',
                                              style: ThemeConstands
                                                  .texttheme.bodyText2!
                                                  .copyWith(
                                                fontWeight: FontWeight.w600,
                                                color:
                                                    //  !_isLike
                                                    //     ? Colorconstands.darkGray
                                                    //     :
                                                    Colorconstands.primaryColor,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      const SizedBox(
                                        width: 24,
                                      ),
                                      InkWell(
                                        onTap: () {
                                          print("Comment");
                                        },
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            SvgPicture.asset(
                                              "assets/icons/fi_reply_icon.svg",
                                              width: 12,
                                            ),
                                            const SizedBox(
                                              width: 6,
                                            ),
                                            Text(
                                              'Reply',
                                              style: ThemeConstands
                                                  .texttheme.bodyText2!
                                                  .copyWith(
                                                fontWeight: FontWeight.w600,
                                                color: Colorconstands.darkGray,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 18,
                              ),
                            ],
                          );
                        },
                      );
                    },
                  )
                : Container(
                    height: MediaQuery.of(context).size.height / 3,
                    width: MediaQuery.of(context).size.width,
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        Image.asset(
                          "assets/images/gifs/empty_data.gif",
                          width: MediaQuery.of(context).size.width / 1.8,
                        ),
                        Text(
                          "No Entry Available",
                          style: ThemeConstands.texttheme.headline6!
                              .copyWith(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
          );
        }
        return Container();
      },
    );
  }
}
