import 'package:camis_application_flutter/app/modules/homework_screen/components/shimmer_widget.dart';
import 'package:camis_application_flutter/app/modules/homework_screen/widgets/pdf_viewer_widget.dart';
import 'package:camis_application_flutter/widgets/view_document.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';
import '../../homework_screen/bloc/homework_bloc.dart';
import '../../time_line/widget/thumbnail_widget.dart';
import '../../time_line/widget/view_image.dart';

class TabAttachment extends StatelessWidget {
  const TabAttachment({
    Key? key,
    required this.width,
  }) : super(key: key);

  final double width;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeworkBloc, HomeworkState>(
      builder: (context, state) {
        if (state is HomeWorkLoadingState) {
          return Center(
            child: ShimmerWidget(
              child: Container(
                width: width,
                color: Colors.grey,
              ),
            ),
          );
        }
        if (state is LessonLoadedState) {
          var attachment = state.lessonModel.data!.attachments;

          // Group of Image
          var listOfImage = attachment!
              .where((element) => element.fileType!.contains("image"))
              .toList();

          // Group of Video
          var listOfVideo = attachment
              .where((element) => element.fileType!.contains("video"))
              .toList();

          var listOfPDF = attachment
              .where((element) => element.fileType!.contains("application"))
              .toList();

          // Group both of Image and Video
          var listOfImageAndVideo = attachment
              .where((element) =>
                  element.fileType!.contains("image") ||
                  element.fileType!.contains("video"))
              .toList();

          return attachment.isEmpty
              ? ListView.builder(
                  shrinkWrap: true,
                  itemBuilder: (BuildContext contex, index) {
                    return Column(
                      children: [
                        Image.asset(
                          "assets/images/gifs/empty_data.gif",
                          width: MediaQuery.of(context).size.width / 1.8,
                        ),
                        const SizedBox(
                          height: 12.0,
                        ),
                        Text(
                          "No Lesson available",
                          style: ThemeConstands.texttheme.headline6,
                        )
                      ],
                    );
                  },
                  itemCount: 1,
                )
              : Column(
                  children: [
                    const SizedBox(
                      height: 15,
                    ),
                    //PDF
                    ListView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemCount: listOfPDF.length,
                        itemBuilder: (context, index) {
                          return MaterialButton(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 18, vertical: 2),
                            onPressed: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (
                                    context,
                                  ) =>
                                      ViewDocuments(
                                    path: listOfPDF[index].fileShow,
                                    filename:
                                        listOfPDF[index].fileName.toString(),
                                  ),
                                ),
                              );
                            },
                            child: Container(
                              width: width,
                              padding: const EdgeInsets.symmetric(
                                vertical: 8.0,
                              ),
                              decoration: const BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                    color: Color(0XFFDEDEDE),
                                  ),
                                ),
                              ),
                              child: Row(
                                children: [
                                  Image.asset(
                                    "assets/images/pdf_icon.png",
                                  ),
                                  const SizedBox(
                                    width: 8.0,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        width: width - 85,
                                        child: Text(
                                          "${listOfPDF[index].fileName}",
                                          style: const TextStyle(
                                            fontWeight: FontWeight.w500,
                                            color: Colorconstands.darkGray,
                                          ),
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 1.0,
                                      ),
                                      Text(
                                        "${listOfPDF[index].fileSize}",
                                        style: ThemeConstands
                                            .texttheme.subtitle2!
                                            .copyWith(
                                          fontWeight: FontWeight.w500,
                                          color: Colorconstands.darkGray,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          );
                        }),

                    //Image and Video
                    ListView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: listOfImageAndVideo.length,
                      itemBuilder: (context, index) {
                        return MaterialButton(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 18, vertical: 2),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ImageViewDownloads(
                                  listimagevide: listOfImageAndVideo.toList(),
                                  activepage: index,
                                ),
                              ),
                            );
                          },
                          child: Container(
                            width: width,
                            padding: const EdgeInsets.symmetric(
                              vertical: 8.0,
                            ),
                            child: listOfImageAndVideo[index]
                                    .fileType!
                                    .contains("video")
                                ? ThumbnailVideoWidget(
                                    image: listOfImageAndVideo[index]
                                                    .fileThumbnail ==
                                                '' ||
                                            listOfImageAndVideo[index]
                                                    .fileThumbnail ==
                                                null
                                        ? "https://1.bp.blogspot.com/-4ilA6TFD1nU/XbbgJXE1CLI/AAAAAAAAA3M/GwK-nwRZJ189IyiXIKSG-FROclbz_d9aQCLcBGAsYHQ/s1600/file-MrylO8jADD.png"
                                        : listOfImageAndVideo[index]
                                            .fileShow
                                            .toString(),
                                  )
                                : Container(
                                    width: width,
                                    padding: const EdgeInsets.symmetric(
                                      vertical: 8.0,
                                    ),
                                    decoration: const BoxDecoration(
                                      border: Border(
                                        bottom: BorderSide(
                                          color: Color(0XFFDEDEDE),
                                        ),
                                      ),
                                    ),
                                    child: Image(
                                      image: NetworkImage(
                                        listOfImageAndVideo[index]
                                            .fileThumbnail
                                            .toString(),
                                      ),
                                      fit: BoxFit.contain,
                                      width: width / 2.2,
                                      height:
                                          MediaQuery.of(context).size.height /
                                              2.2,
                                    ),
                                  ),
                          ),
                        );
                      },
                    ),
                  ],
                );
        } else {
          return Container();
        }
      },
    );
  }
}

class CustomAnswerImageWidget extends StatelessWidget {
  final String image;
  final double width;

  const CustomAnswerImageWidget({
    Key? key,
    required this.width,
    required this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: width,
          height: 300,
          padding: const EdgeInsets.symmetric(horizontal: 18.0),
          child: FadeInImage(
            placeholder: const AssetImage(
              "assets/images/child_profile.png",
            ),
            image: NetworkImage(
              image.toString(),
            ),
            fit: BoxFit.contain,
          ),
        ),
      ],
    );
  }
}
