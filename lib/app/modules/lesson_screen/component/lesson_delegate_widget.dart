import 'package:camis_application_flutter/app/modules/homework_screen/components/shimmer_widget.dart';
import 'package:flutter/material.dart';

import '../../../core/themes/themes.dart';
import '../../../routes/e.route.dart';
import '../../homework_screen/bloc/homework_bloc.dart';
import '../../homework_screen/widgets/custom_tabbar_widget.dart';

class LessonDelegateWidget extends SliverPersistentHeaderDelegate {
  LessonDelegateWidget({required this.tabController});
  final TabController tabController;
  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return BlocBuilder<HomeworkBloc, HomeworkState>(
      builder: (context, state) {
        if (state is HomeWorkLoadingState) {
          return ShimmerWidget(
            child: Container(
              height: 54,
              width: MediaQuery.of(context).size.width,
              color: Colors.grey.shade400,
            ),
          );
        }
        if (state is LessonLoadedState) {
          return CustomTabbarWidget(
            width: MediaQuery.of(context).size.width,
            tabController: tabController,
            tabs: [
              Tab(
                child: Text(
                  "Attachment",
                  style: ThemeConstands.texttheme.subtitle1!
                      .copyWith(fontWeight: FontWeight.w600),
                ),
              ),
              Tab(
                child: Text(
                  "Forum",
                  style: ThemeConstands.texttheme.subtitle1!
                      .copyWith(fontWeight: FontWeight.w600),
                ),
              ),
            ],
          );
        }
        return const Center(
          child: Text("Please try again!"),
        );
      },
    );
  }

  @override
  double get maxExtent => 80;

  @override
  double get minExtent => 70;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}
