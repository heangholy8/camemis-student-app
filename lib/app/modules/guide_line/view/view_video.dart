import 'package:camis_application_flutter/app/modules/time_line/school_time_line/e.schooltimeline.dart';
import 'package:camis_application_flutter/widgets/custom_youtube_player.dart';
import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';
class ViewVideo extends StatefulWidget {
  final List listVideo;
  late  int? activepage;
  ViewVideo({Key? key, required this.listVideo,required this.activepage}) : super(key: key);

  @override
  State<ViewVideo> createState() => _ViewVideoState();
}

class _ViewVideoState extends State<ViewVideo> {

  YoutubePlayerController? _controler;
  @override
  void initState() {
    widget.activepage;
    widget.listVideo;
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    PageController pagec = PageController(initialPage:widget.activepage!);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colorconstands.black,
        automaticallyImplyLeading:false,
        title: Container(
          margin:const EdgeInsets.only(top: 20),
          child: Row(
            children: [
              Container(
                margin:const EdgeInsets.only(left: 0.0,),
                child: IconButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  icon:const Icon(
                    Icons.arrow_back_ios,
                    size: 23,
                    color: Colorconstands.white
                  ),
                ),
              ),
              Container(
                child: Text("VIDEO_GUIDE".tr(),style: ThemeConstands.texttheme.headline5!.copyWith(color: Colorconstands.white),),
              )
            ],
          ),
        ),
      ),
      backgroundColor: Colorconstands.black,
          body: SafeArea(
            bottom: false,
            child: PageView.builder(
              controller: pagec,
              itemCount: widget.listVideo.length,
              itemBuilder: (context, index) {
                return Center(child: CustomYoutubePlayer(youtubeUrl: widget.listVideo[index].file.toString()));
              },
            ),
          ),
        );
  }
}