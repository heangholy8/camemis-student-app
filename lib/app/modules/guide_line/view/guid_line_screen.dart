import 'package:camis_application_flutter/app/modules/guide_line/bloc/user_guideline_bloc.dart';
import 'package:camis_application_flutter/app/modules/guide_line/view/view_video.dart';
import 'package:camis_application_flutter/app/routes/e.route.dart';

import '../../../../model/user_profile/user_guideline.dart';

class GuidLineScreen extends StatefulWidget {
  const GuidLineScreen({Key? key}) : super(key: key);

  @override
  State<GuidLineScreen> createState() => _GuidLineScreenState();
}

class _GuidLineScreenState extends State<GuidLineScreen> {
  TextEditingController _textEditingController =  TextEditingController();
   List<UserGuidelineModel>? data;
  @override
  void initState() {
    BlocProvider.of<UserGuidelineBloc>(context).add(const UserGuidelineGetdataEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10),
      child: BlocBuilder<UserGuidelineBloc, UserGuidelineState>(
        builder: (context, state) {
          if(state is UserGuidelineLoadingState){
            return Container();
          }
          if(state is UserGuidelineSuccessState){
            var data = state.userGuidelinrmodel.data!;
            return data.isEmpty?Container(): ListView.separated(
              physics:
                  const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: (){
                    setState(() {
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>ViewVideo(listVideo: data, activepage: index)));
                    });
                  },
                  child: Row(
                    mainAxisAlignment:
                        MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 70,
                        width: 130,
                        decoration: const BoxDecoration(
                            color: Colorconstands.neutralGrey,
                            borderRadius: BorderRadius.all(
                                Radius.circular(10))),
                              child: const Icon(Icons.video_library,size: 45,color: Colorconstands.primaryColor,),
                      ),
                      const SizedBox(width: 12.0,),
                      Expanded(
                        child: Text(
                          data[index].title!,
                          style: ThemeConstands.button_SemiBold_16.copyWith(
                            fontWeight: FontWeight.bold,
                            color: Colorconstands.primaryColor,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                      const SizedBox(width: 12.0,),
                      const Icon(
                        Icons.arrow_forward_ios_rounded,
                        color: Colorconstands.primaryColor,
                      )
                    ],
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return const Divider();
              },
              itemCount: data.length);
          }
          else{
            return const Center(child: Text("pleas again"),);
          }
          
        },
      ),
    );
  }
}
