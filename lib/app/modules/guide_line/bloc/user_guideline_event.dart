part of 'user_guideline_bloc.dart';

abstract class UserGuidelineEvent extends Equatable {
  const UserGuidelineEvent();

  @override
  List<Object> get props => [];
}
class UserGuidelineGetdataEvent extends UserGuidelineEvent {
  const UserGuidelineGetdataEvent();
}
