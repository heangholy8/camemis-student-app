import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../model/base/base_respone_model.dart';
import '../../../../model/user_profile/user_guideline.dart';
import '../../../../service/apis/profile_user/get_profile_user.dart';

part 'user_guideline_event.dart';
part 'user_guideline_state.dart';

class UserGuidelineBloc extends Bloc<UserGuidelineEvent, UserGuidelineState> {
  final GetProfileUserApi getUserGuidelineapi;
  UserGuidelineBloc({required this.getUserGuidelineapi}) : super(UserGuidelineInitial()) {
    on<UserGuidelineEvent>((event, emit) async{
      emit(const UserGuidelineLoadingState(message: "Loading....."));
      try{
        var data = await getUserGuidelineapi.getUserguidelineApi();
        emit(UserGuidelineSuccessState(userGuidelinrmodel: data));
      }catch(e){
        emit(const UserGuidelineErrorState(error: "Error data"));
      }
    });
  }
}
