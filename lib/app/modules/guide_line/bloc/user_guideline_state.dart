part of 'user_guideline_bloc.dart';

abstract class UserGuidelineState extends Equatable {
  const UserGuidelineState();
  
  @override
  List<Object> get props => [];
}

class UserGuidelineInitial extends UserGuidelineState {}
class UserGuidelineLoadingState extends UserGuidelineState {
  final String message;

  const UserGuidelineLoadingState({
    required this.message,
  });
}

class UserGuidelineSuccessState extends UserGuidelineState {
  final UserGuidelineModel userGuidelinrmodel;

  const UserGuidelineSuccessState({required this.userGuidelinrmodel});
}

class UserGuidelineErrorState extends UserGuidelineState {
  final String error;

  const UserGuidelineErrorState({
    required this.error,
  });
}