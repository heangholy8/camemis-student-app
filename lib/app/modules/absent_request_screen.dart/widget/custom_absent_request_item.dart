import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';
import 'custom_row_title.dart';

class AbsentRequestItem extends StatelessWidget {
  final String nameChild;
  final String nameGuardian;
  final String reasons;
  final String dateRequest;
  final String enddateRequest;
  final String status;
  final Widget childScedule;
  const AbsentRequestItem({
    Key? key,
    required this.nameChild,
    required this.nameGuardian,
    required this.reasons,
    required this.dateRequest,
    required this.status,
    required this.enddateRequest,
    required this.childScedule,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      decoration: BoxDecoration(
          color: Colorconstands.secondaryColor.withOpacity(0.1),
          borderRadius: const BorderRadius.all(Radius.circular(10))),
      margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      width: MediaQuery.of(context).size.width,
      child: Stack(
        children: [
          Positioned(
            child: Column(
              children: [
                Row(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        dateRequest,
                        style: ThemeConstands.texttheme.subtitle1
                            ?.copyWith(fontWeight: FontWeight.w700),
                      ),
                    ),
                    Text(" - "),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        enddateRequest,
                        style: ThemeConstands.texttheme.subtitle1
                            ?.copyWith(fontWeight: FontWeight.w700),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 15,
                ),
                childScedule,
                CustomRowTitle(
                  title: "REASON".tr() + ": ",
                  subtitle: reasons,
                ),
                CustomRowTitle(
                  title: "REQUEST_FOR".tr() + ": ",
                  subtitle: nameChild,
                ),
                CustomRowTitle(
                  title: "REQUEST_BY".tr() + ": ",
                  subtitle: nameGuardian,
                ),
              ],
            ),
          ),
          Positioned(
            top: 0,
            right: 5,
            bottom: 0,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  margin: const EdgeInsets.only(top: 5),
                  padding:
                      const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                  decoration: BoxDecoration(
                      color: status == "0"
                          ? Colors.orange[100]
                          : status == "1"
                              ? Colors.green[100]
                              : Colors.red[100],
                      borderRadius: const BorderRadius.all(Radius.circular(5))),
                  child: Text(
                    status == "0"
                        ? "AWAITING".tr()
                        : status == "1"
                            ? "Approved"
                            : "Declined",
                    style: ThemeConstands.texttheme.subtitle2?.copyWith(
                      fontWeight: FontWeight.w700,
                      color: status == "0"
                          ? Colors.orange[800]
                          : status == "1"
                              ? Colors.green[800]
                              : Colors.red[800],
                    ),
                  ),
                ),
                const Icon(
                  Icons.arrow_forward_ios_outlined,
                  size: 18,
                ),
                const Icon(
                  Icons.arrow_forward_ios_outlined,
                  size: 18,
                  color: Colors.transparent,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
