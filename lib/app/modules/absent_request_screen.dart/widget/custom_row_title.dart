import 'package:flutter/cupertino.dart';

import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class CustomRowTitle extends StatelessWidget {
  final String subtitle;
  final String title;
  const CustomRowTitle({
    Key? key,
    required this.title,
    required this.subtitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        const SizedBox(
          width: 5,
        ),
        Text(
          title,
          style: ThemeConstands.texttheme.subtitle1
              ?.copyWith(fontWeight: FontWeight.w700),
        ),
        Expanded(
          child: Text(
            subtitle,
            style: ThemeConstands.texttheme.subtitle1?.copyWith(
                color: Colorconstands.iconColor.withOpacity(0.8), height: 2),overflow: TextOverflow.ellipsis,
          ),
        ),
        const SizedBox(width: 50,)
      ],
    );
  }
}
