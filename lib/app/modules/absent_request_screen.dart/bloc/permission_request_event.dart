part of 'permission_request_bloc.dart';

abstract class PermissionRequestEvent extends Equatable {
  const PermissionRequestEvent();

  @override
  List<Object> get props => [];
}

class PermissionrequestHistoryEvent extends PermissionRequestEvent{
  const PermissionrequestHistoryEvent();
}
class PermissionrequestOngoingEvent extends PermissionRequestEvent{
  const PermissionrequestOngoingEvent();
}

class PermissionrequestDetailEvent extends PermissionRequestEvent{
  final String idPermission;
  const PermissionrequestDetailEvent({required this.idPermission});
}
