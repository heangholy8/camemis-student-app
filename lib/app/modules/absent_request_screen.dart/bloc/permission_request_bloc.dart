import 'package:bloc/bloc.dart';
import 'package:camis_application_flutter/model/permission_request_model/permission_request.dart';
import 'package:camis_application_flutter/service/apis/permission_request/get_permission_request.dart';
import 'package:equatable/equatable.dart';

import '../../../../model/base/base_respone_model.dart';
import '../../../../model/permission_request_model/permissioo_detail.dart';
import '../../../../storages/get_storage.dart';

part 'permission_request_event.dart';
part 'permission_request_state.dart';

class PermissionRequestHistoryBloc extends Bloc<PermissionRequestEvent, PermissionRequestState> {
  final GetPermissionRequestApi getPermissionRequestApi;
  final GetStoragePref _pref = GetStoragePref(); 
  PermissionRequestHistoryBloc({required this.getPermissionRequestApi}) : super(PermissionRequestInitial()) {
    on<PermissionrequestHistoryEvent>((event, emit) async {
      var schoolYeaid = "";
      emit(const PermissionRequestLoadingState(message: "Loading....."));
      try{
          var data = await getPermissionRequestApi.getPermissionRequestApi(schoolYearId: schoolYeaid, status: "2");
          emit(PermissionRequestLoadtedState(permissionModel: data!));
      }catch(e){
        emit(const PermissionRequestErrorState(error: "Error data"));
      }
    });
  }
}
class PermissionRequestOngoingBloc extends Bloc<PermissionRequestEvent, PermissionRequestState> {
  final GetPermissionRequestApi getPermissionRequestApi;
  final GetStoragePref _pref = GetStoragePref(); 
  PermissionRequestOngoingBloc({required this.getPermissionRequestApi}) : super(PermissionRequestInitial()) {
    on<PermissionrequestOngoingEvent>((event, emit) async {
      var schoolYeaid = "";
      emit(const PermissionRequestLoadingState(message: "Loading....."));
      try{
          var data = await getPermissionRequestApi.getPermissionRequestApi(schoolYearId: schoolYeaid, status: "1");
          emit(PermissionRequestLoadtedState(permissionModel: data!));
      }catch(e){
        emit(const PermissionRequestErrorState(error: "Error data"));
      }
    });
  }
}

class PermissionRequestDetailBloc extends Bloc<PermissionRequestEvent, PermissionRequestState> {
  final GetPermissionRequestApi getPermissionRequestDetailApi;
  PermissionRequestDetailBloc({required this.getPermissionRequestDetailApi}) : super(PermissionRequestInitial()) {
    on<PermissionrequestDetailEvent>((event, emit) async {
      emit(const PermissionRequestLoadingState(message: "Loading....."));
      try{
          var data = await getPermissionRequestDetailApi.getDetailPermissionApi(idPermission: event.idPermission.toString());
          emit(PermissionRequestDetailLoadtedState(permissionDetailModel: data));
      }catch(e){
        emit(const PermissionRequestErrorState(error: "Error data"));
      }
    });
  }
}
