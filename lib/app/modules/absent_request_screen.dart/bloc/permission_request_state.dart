part of 'permission_request_bloc.dart';

abstract class PermissionRequestState extends Equatable {
  const PermissionRequestState();
  
  @override
  List<Object> get props => [];
}
class PermissionRequestInitial extends PermissionRequestState {}

class PermissionRequestLoadingState extends PermissionRequestState{
   final String message;
   const PermissionRequestLoadingState({required this.message});
}
class PermissionRequestLoadtedState extends PermissionRequestState{
  final BaseResponseModel<List<GetPermissionRequestModel>> permissionModel;
  const PermissionRequestLoadtedState({required this.permissionModel});
}

class PermissionRequestDetailLoadtedState extends PermissionRequestState{
  final BaseResponseModel<GetPermissionRequestDetailModel> permissionDetailModel;
  const PermissionRequestDetailLoadtedState({required this.permissionDetailModel});
}

class PermissionRequestErrorState extends PermissionRequestState {
  final String error;
  const PermissionRequestErrorState({required this.error,});
}
