import 'package:camis_application_flutter/widgets/preview_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../widgets/cached_image.dart';
import '../../../../widgets/custom_header.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';
import '../bloc/permission_request_bloc.dart';

class ViewDetailAbset extends StatefulWidget {
  const ViewDetailAbset({Key? key}) : super(key: key);

  @override
  State<ViewDetailAbset> createState() => _ViewDetailAbsetState();
}

class _ViewDetailAbsetState extends State<ViewDetailAbset> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
        backgroundColor: Colorconstands.primaryColor,
        body: SafeArea(
          bottom: false,
          child: Column(
            children: [
              Container(
                  margin: const EdgeInsets.only(left: 12.0),
                  child: CustomHeader(title: "Review Request")),
              Expanded(
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15),
                        topRight: Radius.circular(15)),
                    color: Color(0xFFF2F1F1),
                  ),
                  child: BlocBuilder<PermissionRequestDetailBloc,
                      PermissionRequestState>(builder: (context, state) {
                    if (state is PermissionRequestLoadingState) {
                      return Container(
                        child: _buildLoading(),
                      );
                    }
                    if (state is PermissionRequestDetailLoadtedState) {
                      var data = state.permissionDetailModel.data;
                      var fileImage = data!.file!.where(((element) {
                        return element.fileType!.contains("image");
                      })).toList();
                      var fileVoice = data.file!.where(((element) {
                        return element.fileType!.contains("audio");
                      })).toList();
                      return SingleChildScrollView(
                        child: Column(
                          children: [
                            Container(
                              alignment: Alignment.centerLeft,
                              margin: const EdgeInsets.symmetric(
                                  vertical: 12.0, horizontal: 18.0),
                              child: const Text(
                                "REQUEST FOR",
                                style: TextStyle(
                                    fontSize: 13.0,
                                    color: Colorconstands.darkGray),
                              ),
                            ),
                            Container(
                              decoration: const BoxDecoration(
                                color: Colorconstands.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0x14000000),
                                      offset: Offset(0, 1),
                                      blurRadius: 2.0)
                                ],
                              ),
                              child: Container(
                                margin: const EdgeInsets.symmetric(
                                    vertical: 12.0, horizontal: 18.0),
                                child: Column(
                                  children: [
                                    Container(
                                        margin: const EdgeInsets.symmetric(
                                            vertical: 8.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            const Text(
                                              "Student Name",
                                              style: TextStyle(
                                                  fontSize: 16.0,
                                                  color: Colorconstands.black),
                                            ),
                                            Text(
                                                translate=="en"?data.student!.nameEn.toString()==""?data.student!.name.toString():data.student!.nameEn.toString():data.student!.name.toString(),
                                                style: ThemeConstands
                                                    .texttheme.headline6!
                                                    .copyWith(
                                                        fontWeight:
                                                            FontWeight.bold)),
                                          ],
                                        )),
                                    const Divider(),
                                    Container(
                                        margin: const EdgeInsets.symmetric(
                                            vertical: 8.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            const Text(
                                              "Request Type",
                                              style: TextStyle(
                                                  fontSize: 16.0,
                                                  color: Colorconstands.black),
                                            ),
                                            Text(
                                                data.requestTypes == 1
                                                    ? "TIME"
                                                    : "DAY",
                                                style: ThemeConstands
                                                    .texttheme.subtitle1!
                                                    .copyWith(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: Colorconstands
                                                            .primaryColor)),
                                          ],
                                        )),
                                    const Divider(),
                                    Container(
                                        margin: const EdgeInsets.symmetric(
                                            vertical: 8.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            const Text(
                                              "Request Status",
                                              style: TextStyle(
                                                  fontSize: 16.0,
                                                  color: Colorconstands.black),
                                            ),
                                            Text(
                                                data.isApprove == 0
                                                    ? "AWAITING"
                                                    : data.isApprove == 1
                                                        ? "APPROVED"
                                                        : "DECLINED",
                                                style: ThemeConstands
                                                    .texttheme.subtitle1!
                                                    .copyWith(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: data.isApprove ==
                                                                0
                                                            ? Color(0xFFF0932B)
                                                            : data.isApprove ==
                                                                    0
                                                                ? Color(
                                                                    0xFF68BA6B)
                                                                : Colors.red)),
                                          ],
                                        )),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              alignment: Alignment.centerLeft,
                              margin: const EdgeInsets.only(
                                  top: 25.0,
                                  left: 18.0,
                                  right: 18.0,
                                  bottom: 12.0),
                              child: const Text(
                                "DETAIL",
                                style: TextStyle(
                                    fontSize: 13.0,
                                    color: Colorconstands.darkGray),
                              ),
                            ),
                            Container(
                              decoration: const BoxDecoration(
                                color: Colorconstands.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0x14000000),
                                      offset: Offset(0, 1),
                                      blurRadius: 2.0)
                                ],
                              ),
                              child: Container(
                                margin: const EdgeInsets.symmetric(
                                    vertical: 12.0, horizontal: 18.0),
                                child: Column(
                                  children: [
                                    Container(
                                        margin: const EdgeInsets.symmetric(
                                            vertical: 8.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            const Text(
                                              "Absent Date",
                                              style: TextStyle(
                                                  fontSize: 16.0,
                                                  color: Colorconstands.black),
                                            ),
                                            Text(data.startDate.toString(),
                                                style: ThemeConstands
                                                    .texttheme.subtitle1!
                                                    .copyWith(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: Colorconstands
                                                            .black)),
                                          ],
                                        )),
                                    const Divider(),
                                    data.requestTypes == 1
                                        ? Container()
                                        : Container(
                                            margin: const EdgeInsets.symmetric(
                                                vertical: 8.0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                const Text(
                                                  "Absent Day",
                                                  style: TextStyle(
                                                      fontSize: 16.0,
                                                      color:
                                                          Colorconstands.black),
                                                ),
                                                const SizedBox(
                                                  width: 15,
                                                ),
                                                Expanded(
                                                  child: Column(
                                                    children: [
                                                      Align(
                                                        alignment: Alignment
                                                            .centerRight,
                                                        child: Text(
                                                          data.startDate
                                                                  .toString() +
                                                              " (" +
                                                              "STARTDATE".tr() +
                                                              ")",
                                                          style: ThemeConstands
                                                              .texttheme
                                                              .subtitle1
                                                              ?.copyWith(
                                                                  color: Colorconstands
                                                                      .iconColor
                                                                      .withOpacity(
                                                                          0.8),
                                                                  height: 2),
                                                        ),
                                                      ),
                                                      Align(
                                                        alignment: Alignment
                                                            .centerRight,
                                                        child: Text(
                                                          data.endDate
                                                                  .toString() +
                                                              " (" +
                                                              "ENDDATE".tr() +
                                                              ")",
                                                          style: ThemeConstands
                                                              .texttheme
                                                              .subtitle1
                                                              ?.copyWith(
                                                            color: Colorconstands
                                                                .black,
                                                            height: 2,
                                                          ),
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            )),
                                    data.requestTypes == 2 ||
                                            data.schedule!.isEmpty
                                        ? Container()
                                        : Container(
                                            margin: const EdgeInsets.symmetric(
                                                vertical: 8.0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                const Text(
                                                  "Absent Subject",
                                                  style: TextStyle(
                                                      fontSize: 16.0,
                                                      color:
                                                          Colorconstands.black),
                                                ),
                                                const SizedBox(
                                                  width: 50,
                                                ),
                                                Expanded(
                                                  child: ListView.builder(
                                                    padding:
                                                        const EdgeInsets.all(0),
                                                    shrinkWrap: true,
                                                    itemCount:
                                                        data.schedule!.length,
                                                    physics:
                                                        const ScrollPhysics(),
                                                    itemBuilder:
                                                        (BuildContext context,
                                                            int index) {
                                                      return Container(
                                                        alignment: Alignment
                                                            .centerRight,
                                                        child: Text(
                                                          data.schedule![index]
                                                                  .startTime
                                                                  .toString() +
                                                              " - " +
                                                              data
                                                                  .schedule![
                                                                      index]
                                                                  .endTime
                                                                  .toString() +
                                                              " (" +
                                                              data
                                                                  .schedule![
                                                                      index]
                                                                  .event
                                                                  .toString() +
                                                              " )",
                                                          style: ThemeConstands
                                                              .texttheme
                                                              .subtitle1
                                                              ?.copyWith(
                                                            color: Colorconstands
                                                                .black,
                                                            height: 2,
                                                          ),
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                        ),
                                                      );
                                                    },
                                                  ),
                                                ),
                                              ],
                                            )),
                                    const Divider(),
                                    Container(
                                        margin: const EdgeInsets.symmetric(
                                            vertical: 8.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            const Text(
                                              "Resion",
                                              style: TextStyle(
                                                  fontSize: 16.0,
                                                  color: Colorconstands.black),
                                            ),
                                            Expanded(
                                              child: Container(
                                                  alignment:
                                                      Alignment.centerRight,
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 50),
                                                  child: Text(
                                                      translate=="en"?data.reason!.nameEn.toString()==""?data.reason!.name.toString():data.reason!.nameEn.toString():data.reason!.name.toString(),
                                                      style: ThemeConstands
                                                          .texttheme.subtitle1!
                                                          .copyWith(
                                                              color:
                                                                  Colorconstands
                                                                      .black))),
                                            ),
                                          ],
                                        )),
                                    data.description == "" ||
                                            data.description == null
                                        ? Container()
                                        : const Divider(),
                                    data.description == "" ||
                                            data.description == null
                                        ? Container()
                                        : Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              Container(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  margin: const EdgeInsets
                                                      .symmetric(vertical: 8.0),
                                                  child: const Text(
                                                    "Comment",
                                                    style: TextStyle(
                                                        fontSize: 16.0,
                                                        color: Colorconstands
                                                            .black),
                                                  )),
                                              Container(
                                                alignment: Alignment.centerLeft,
                                                margin:
                                                    const EdgeInsets.symmetric(
                                                        vertical: 8.0),
                                                child: Text(
                                                    data.description.toString(),
                                                    style: ThemeConstands
                                                        .texttheme.subtitle1!
                                                        .copyWith(
                                                            color: Colorconstands
                                                                .black)),
                                              ),
                                            ],
                                          ),
                                    data.file!.isEmpty
                                        ? Container()
                                        : const Divider(),
                                    data.file!.isEmpty
                                        ? Container()
                                        : Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              Container(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  margin: const EdgeInsets
                                                      .symmetric(vertical: 8.0),
                                                  child: const Text(
                                                    "Attachment",
                                                    style: TextStyle(
                                                        fontSize: 16.0,
                                                        color: Colorconstands
                                                            .black),
                                                  )),
                                              Container(
                                                  child: GridView.builder(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              0),
                                                      physics:
                                                          const ScrollPhysics(),
                                                      shrinkWrap: true,
                                                      itemCount:
                                                          fileImage.length,
                                                      gridDelegate:
                                                          const SliverGridDelegateWithFixedCrossAxisCount(
                                                              crossAxisCount:
                                                                  3),
                                                      itemBuilder:
                                                          (_, subindex) {
                                                        return Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(2.0),
                                                          child: InkWell(
                                                            onTap: (() {
                                                              Navigator.push(
                                                                context,
                                                                PageRouteBuilder(
                                                                  pageBuilder: (_,
                                                                          __,
                                                                          ___) =>
                                                                      PreviewImageNetwork(
                                                                    listimage:
                                                                        fileImage,
                                                                    activepage:
                                                                        subindex,
                                                                  ),
                                                                  transitionDuration:
                                                                      const Duration(
                                                                          seconds:
                                                                              0),
                                                                ),
                                                              );
                                                            }),
                                                            child:
                                                                CachedImageNetwork(
                                                              urlImage: fileImage[
                                                                      subindex]
                                                                  .fileShow!,
                                                            ),
                                                          ),
                                                        );
                                                      })),
                                            ],
                                          ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      );
                    } else {
                      return Container(
                        child: _buildLoading(),
                      );
                    }
                  }),
                ),
              )
            ],
          ),
        ));
  }

  Widget _buildLoading() => Container(
      margin: const EdgeInsets.only(right: 10.0),
      child: Center(
          child: Image.asset(
        "assets/images/gifs/loading.gif",
        height: 45,
        width: 45,
      )));
}
