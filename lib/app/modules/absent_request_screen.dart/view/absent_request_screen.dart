import 'dart:async';
import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:camis_application_flutter/app/modules/absent_request_screen.dart/bloc/permission_request_bloc.dart';
import 'package:camis_application_flutter/app/modules/absent_request_screen.dart/view/view_detail_abset.dart';
import 'package:camis_application_flutter/app/modules/request_absent_screen/view/by_day/request_absent_by_day_screen.dart';
import 'package:camis_application_flutter/widgets/button_widget/custom_bottom_request_absent.dart';
import 'package:camis_application_flutter/widgets/custom_header.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../widgets/no_connection_alert_widget.dart';
import '../../../../widgets/no_dart.dart';
import '../../../../widgets/shimmer_style.dart';
import '../../../../widgets/toast_nointernet.dart';
import '../../home_screen/view/home_screen.dart';
import '../widget/custom_absent_request_item.dart';

class OngoingAndHistoryAbsent extends StatefulWidget {
  const OngoingAndHistoryAbsent({Key? key}) : super(key: key);

  @override
  State<OngoingAndHistoryAbsent> createState() =>
      _OngoingAndHistoryAbsentState();
}

class _OngoingAndHistoryAbsentState extends State<OngoingAndHistoryAbsent> {
  var _tabControllerIndex = 0;
  String studentID = "";
  String classID = "";
  String? activeDate;
  bool isVisableReQuestButton = true;
  StreamSubscription? stsub;
  bool connection = true;

  @override
  void initState() {
    super.initState();
    //=============Check internet====================
    stsub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
      });
    });

    //=============Check internet====================
    activeDate = DateFormat("dd/MM/yyyy").format(DateTime.now());
    BlocProvider.of<PermissionRequestOngoingBloc>(context)
        .add(const PermissionrequestOngoingEvent());
    BlocProvider.of<PermissionRequestHistoryBloc>(context)
        .add(const PermissionrequestHistoryEvent());
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    final translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: WillPopScope(
        onWillPop: () async {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => HomeScreen(
              ),
            ),
          );
          return true;
        },
        child: SafeArea(
          bottom: false,
          child: Column(
            children: [
              Container(
                  margin: const EdgeInsets.only(left: 12.0),
                  child: CustomHeader(
                    title: "Absent Request",
                    nameScreen: "home",
                  )),
              Expanded(
                  child: Container(
                width: MediaQuery.of(context).size.width,
                padding: const EdgeInsets.symmetric(
                  vertical: 20,
                ),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(15),
                      topRight: Radius.circular(15)),
                  color: Colorconstands.white,
                ),
                child: Stack(
                  children: [
                    DefaultTabController(
                        length: 2,
                        child: Column(
                          children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: TabBar(
                                physics: const ScrollPhysics(),
                                labelStyle: ThemeConstands.texttheme.headline6,
                                labelColor: Colorconstands.primaryColor,
                                indicatorColor: Colors.blue[900],
                                indicatorSize: TabBarIndicatorSize.label,
                                indicatorWeight: 2,
                                isScrollable: true,
                                onTap: (index) {
                                  setState(() {
                                    _tabControllerIndex = index;
                                  });
                                },
                                tabs: const [
                                  Tab(
                                    child: Text("Ongoing"),
                                  ),
                                  Tab(
                                    child: Text("History"),
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Column(
                                children: [
                                  //================== check condition internet=====================
                                  connection == true
                                      ? Container()
                                      : const NoConnectWidget(),
                                  //================== end check condition internet=====================
                                  Expanded(
                                    child: Container(
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 10,
                                      ),
                                      child: TabBarView(
                                          physics: const ScrollPhysics(),
                                          children: [
                                            BlocBuilder<
                                                    PermissionRequestOngoingBloc,
                                                    PermissionRequestState>(
                                                builder: (context, state) {
                                              if (state
                                                  is PermissionRequestLoadingState) {
                                                return ListView.builder(
                                                  padding:
                                                      const EdgeInsets.all(0),
                                                  shrinkWrap: true,
                                                  itemBuilder:
                                                      (BuildContext contex,
                                                          index) {
                                                    return const ShimmerAbsentRequest();
                                                  },
                                                  itemCount: 5,
                                                );
                                              }
                                              if (state
                                                  is PermissionRequestLoadtedState) {
                                                var data =
                                                    state.permissionModel.data;
                                                return data!.isEmpty ||
                                                        data == null
                                                    ? Stack(
                                                        children: [
                                                          RefreshIndicator(
                                                            onRefresh:
                                                                connection ==
                                                                        false
                                                                    ? () async {
                                                                        showtoastInternet(
                                                                            context);
                                                                      }
                                                                    : () async {
                                                                        BlocProvider.of<PermissionRequestOngoingBloc>(context)
                                                                            .add(const PermissionrequestOngoingEvent());
                                                                      },
                                                            child: DataNotFound(
                                                              subtitle:
                                                                  "NO request has been made"
                                                                      .tr(),
                                                              title:
                                                                  "NO Request Found"
                                                                      .tr(),
                                                            ),
                                                          ),
                                                          connection == false
                                                              ? Container()
                                                              : Positioned(
                                                                  bottom: 0,
                                                                  left: 0,
                                                                  right: 0,
                                                                  child:
                                                                      BottomRequestAbsent(
                                                                    width:
                                                                        width,
                                                                    context:
                                                                        context,
                                                                    navigateDay:
                                                                        () {
                                                                      Navigator.of(
                                                                              context)
                                                                          .push(
                                                                        MaterialPageRoute(
                                                                          builder: (context) =>
                                                                              const RequestAbsentByDayScreen(),
                                                                        ),
                                                                      );
                                                                    },
                                                                    navigateTime:
                                                                        () {
                                                                      // BlocProvider.of<PermissionByTimeBloc>(context).add(GetSubjectByDayEvent(
                                                                      //     classId: classID
                                                                      //         .toString(),
                                                                      //     date: activeDate
                                                                      //         .toString(),
                                                                      //     studentId:
                                                                      //         studentID.toString()));
                                                                      // Navigator.of(
                                                                      //         context)
                                                                      //     .push(
                                                                      //   MaterialPageRoute(
                                                                      //     builder: (context) =>
                                                                      //         const RequestAbsentByTimeScreen(),
                                                                      //   ),
                                                                      // );
                                                                    },
                                                                  ),
                                                                )
                                                        ],
                                                      )
                                                    : Stack(
                                                        children: [
                                                          RefreshIndicator(
                                                            onRefresh:
                                                                connection ==
                                                                        false
                                                                    ? () async {
                                                                        showtoastInternet(
                                                                            context);
                                                                      }
                                                                    : () async {
                                                                        BlocProvider.of<PermissionRequestOngoingBloc>(context)
                                                                            .add(const PermissionrequestOngoingEvent());
                                                                      },
                                                            child: ListView
                                                                .builder(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      bottom:
                                                                          120,
                                                                      top: 5),
                                                              itemCount:
                                                                  data.length,
                                                              itemBuilder:
                                                                  (context,
                                                                      index) {
                                                                return GestureDetector(
                                                                  onTap: connection ==
                                                                          false
                                                                      ? () {
                                                                          showtoastInternet(
                                                                              context);
                                                                        }
                                                                      : (() {
                                                                          BlocProvider.of<PermissionRequestDetailBloc>(context)
                                                                              .add(PermissionrequestDetailEvent(idPermission: data[index].id.toString()));
                                                                          Navigator
                                                                              .push(
                                                                            context,
                                                                            MaterialPageRoute(builder: (context) => const ViewDetailAbset()),
                                                                          );
                                                                        }),
                                                                  child:
                                                                      AbsentRequestItem(
                                                                    enddateRequest: data[
                                                                            index]
                                                                        .endDate
                                                                        .toString(),
                                                                    dateRequest: data[
                                                                            index]
                                                                        .startDate
                                                                        .toString(),
                                                                    nameGuardian: translate ==
                                                                            "en"
                                                                        ? data[index].userRequest!.nameEn ==
                                                                                ""
                                                                            ? data[index]
                                                                                .userRequest!
                                                                                .name
                                                                                .toString()
                                                                            : data[index]
                                                                                .userRequest!
                                                                                .nameEn
                                                                                .toString()
                                                                        : data[index]
                                                                            .userRequest!
                                                                            .name
                                                                            .toString(),
                                                                    reasons: translate ==
                                                                            "en"
                                                                        ? data[index].reason!.nameEn.toString() ==
                                                                                ""
                                                                            ? data[index]
                                                                                .reason!
                                                                                .name
                                                                                .toString()
                                                                            : data[index]
                                                                                .reason!
                                                                                .nameEn
                                                                                .toString()
                                                                        : data[index]
                                                                            .reason!
                                                                            .name
                                                                            .toString(),
                                                                    nameChild: translate ==
                                                                            "en"
                                                                        ? data[index].student!.nameEn.toString() ==
                                                                                ""
                                                                            ? data[index]
                                                                                .student!
                                                                                .name
                                                                                .toString()
                                                                            : data[index]
                                                                                .student!
                                                                                .nameEn
                                                                                .toString()
                                                                        : data[index]
                                                                            .student!
                                                                            .name
                                                                            .toString(),
                                                                    status: data[
                                                                            index]
                                                                        .isApprove
                                                                        .toString(),
                                                                    childScedule: data[index]
                                                                            .schedule!
                                                                            .isEmpty
                                                                        ? Container()
                                                                        : ListView.builder(
                                                                            physics: const ScrollPhysics(),
                                                                            padding: const EdgeInsets.all(0),
                                                                            shrinkWrap: true,
                                                                            itemCount: data[index].schedule!.length,
                                                                            itemBuilder: (context, subindex) {
                                                                              return Align(
                                                                                alignment: Alignment.centerLeft,
                                                                                child: Text(
                                                                                  data[index].schedule![subindex].startTime.toString() + " - " + data[index].schedule![subindex].endTime.toString() + " (" + data[index].schedule![subindex].event.toString() + " )",
                                                                                  style: ThemeConstands.texttheme.subtitle1?.copyWith(color: Colorconstands.iconColor.withOpacity(0.8), height: 2),
                                                                                ),
                                                                              );
                                                                            }),
                                                                  ),
                                                                );
                                                              },
                                                            ),
                                                          ),
                                                          connection == false
                                                              ? Container()
                                                              : Positioned(
                                                                  bottom: 0,
                                                                  left: 0,
                                                                  right: 0,
                                                                  child:
                                                                      BottomRequestAbsent(
                                                                    width:
                                                                        width,
                                                                    context:
                                                                        context,
                                                                    navigateDay:
                                                                        () {
                                                                      Navigator.of(
                                                                              context)
                                                                          .push(
                                                                        MaterialPageRoute(
                                                                          builder: (context) =>
                                                                              const RequestAbsentByDayScreen(),
                                                                        ),
                                                                      );
                                                                    },
                                                                    navigateTime:
                                                                        () {
                                                                      // BlocProvider.of<PermissionByTimeBloc>(context).add(GetSubjectByDayEvent(
                                                                      //     classId: classID
                                                                      //         .toString(),
                                                                      //     date: activeDate
                                                                      //         .toString(),
                                                                      //     studentId:
                                                                      //         studentID.toString()));
                                                                      // Navigator.of(
                                                                      //         context)
                                                                      //     .push(
                                                                      //   MaterialPageRoute(
                                                                      //     builder: (context) =>
                                                                      //         const RequestAbsentByTimeScreen(),
                                                                      //   ),
                                                                      // );
                                                                    },
                                                                  ),
                                                                )
                                                        ],
                                                      );
                                              } else {
                                                return const Center(
                                                    child:
                                                        CircularProgressIndicator());
                                              }
                                            }),
                                            BlocBuilder<
                                                    PermissionRequestHistoryBloc,
                                                    PermissionRequestState>(
                                                builder: (context, state) {
                                              if (state
                                                  is PermissionRequestLoadingState) {
                                                return ListView.builder(
                                                  shrinkWrap: true,
                                                  itemBuilder:
                                                      (BuildContext contex,
                                                          index) {
                                                    return Container(
                                                      child:
                                                          const ShimmerAbsentRequest(),
                                                    );
                                                  },
                                                  itemCount: 5,
                                                );
                                              }
                                              if (state
                                                  is PermissionRequestLoadtedState) {
                                                var data =
                                                    state.permissionModel.data;
                                                return data!.isEmpty ||
                                                        data == null
                                                    ? RefreshIndicator(
                                                        onRefresh:
                                                            connection == false
                                                                ? () async {
                                                                    showtoastInternet(
                                                                        context);
                                                                  }
                                                                : () async {
                                                                    BlocProvider.of<PermissionRequestHistoryBloc>(
                                                                            context)
                                                                        .add(
                                                                            const PermissionrequestHistoryEvent());
                                                                  },
                                                        child: DataNotFound(
                                                          subtitle:
                                                              "NO request has been made"
                                                                  .tr(),
                                                          title:
                                                              "NO Request Found"
                                                                  .tr(),
                                                        ),
                                                      )
                                                    : RefreshIndicator(
                                                        onRefresh:
                                                            connection == false
                                                                ? () async {
                                                                    showtoastInternet(
                                                                        context);
                                                                  }
                                                                : () async {
                                                                    BlocProvider.of<PermissionRequestHistoryBloc>(
                                                                            context)
                                                                        .add(
                                                                            const PermissionrequestHistoryEvent());
                                                                  },
                                                        child: ListView.builder(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  vertical: 5),
                                                          itemCount:
                                                              data.length,
                                                          itemBuilder:
                                                              (context, index) {
                                                            return GestureDetector(
                                                              onTap:
                                                                  connection ==
                                                                          false
                                                                      ? () async {
                                                                          showtoastInternet(
                                                                              context);
                                                                        }
                                                                      : (() {
                                                                          BlocProvider.of<PermissionRequestDetailBloc>(context)
                                                                              .add(PermissionrequestDetailEvent(idPermission: data[index].id.toString()));
                                                                          Navigator
                                                                              .push(
                                                                            context,
                                                                            MaterialPageRoute(builder: (context) => const ViewDetailAbset()),
                                                                          );
                                                                        }),
                                                              child:
                                                                  AbsentRequestItem(
                                                                enddateRequest:
                                                                    data[index]
                                                                        .endDate
                                                                        .toString(),
                                                                dateRequest: data[
                                                                        index]
                                                                    .startDate
                                                                    .toString(),
                                                                nameGuardian: translate ==
                                                                        "en"
                                                                    ? data[index].userRequest!.nameEn ==
                                                                            ""
                                                                        ? data[index]
                                                                            .userRequest!
                                                                            .name
                                                                            .toString()
                                                                        : data[index]
                                                                            .userRequest!
                                                                            .nameEn
                                                                            .toString()
                                                                    : data[index]
                                                                        .userRequest!
                                                                        .name
                                                                        .toString(),
                                                                reasons: translate ==
                                                                        "en"
                                                                    ? data[index].reason!.nameEn.toString() ==
                                                                            ""
                                                                        ? data[index]
                                                                            .reason!
                                                                            .name
                                                                            .toString()
                                                                        : data[index]
                                                                            .reason!
                                                                            .nameEn
                                                                            .toString()
                                                                    : data[index]
                                                                        .reason!
                                                                        .name
                                                                        .toString(),
                                                                nameChild: translate ==
                                                                        "en"
                                                                    ? data[index].student!.nameEn.toString() ==
                                                                            ""
                                                                        ? data[index]
                                                                            .student!
                                                                            .name
                                                                            .toString()
                                                                        : data[index]
                                                                            .student!
                                                                            .nameEn
                                                                            .toString()
                                                                    : data[index]
                                                                        .student!
                                                                        .name
                                                                        .toString(),
                                                                status: data[
                                                                        index]
                                                                    .isApprove
                                                                    .toString(),
                                                                childScedule: data[
                                                                            index]
                                                                        .schedule!
                                                                        .isEmpty
                                                                    ? Container()
                                                                    : ListView.builder(
                                                                        padding: const EdgeInsets.all(0),
                                                                        physics: const ScrollPhysics(),
                                                                        shrinkWrap: true,
                                                                        itemCount: data[index].schedule!.length,
                                                                        itemBuilder: (context, subindex) {
                                                                          return Row(
                                                                            children: [
                                                                              Padding(
                                                                                padding: const EdgeInsets.only(left: 5.0),
                                                                                child: Align(
                                                                                  child: Text(
                                                                                    "Absent time: ",
                                                                                    style: ThemeConstands.texttheme.subtitle1?.copyWith(fontWeight: FontWeight.w700),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Expanded(
                                                                                child: Align(
                                                                                  alignment: Alignment.centerLeft,
                                                                                  child: Text(
                                                                                    data[index].schedule![subindex].startTime.toString() + " - " + data[index].schedule![subindex].endTime.toString() + " (" + data[index].schedule![subindex].event.toString() + " )",
                                                                                    style: ThemeConstands.texttheme.subtitle1?.copyWith(color: Colorconstands.iconColor.withOpacity(0.8), height: 2),
                                                                                    overflow: TextOverflow.ellipsis,
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                            ],
                                                                          );
                                                                        }),
                                                              ),
                                                            );
                                                          },
                                                        ),
                                                      );
                                              } else {
                                                return const Center(
                                                    child:
                                                        CircularProgressIndicator());
                                              }
                                            }),
                                          ]),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        )),
                  ],
                ),
              ))
            ],
          ),
        ),
      ),
    );
  }
}
