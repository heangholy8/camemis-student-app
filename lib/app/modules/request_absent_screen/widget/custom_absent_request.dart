import 'package:flutter/material.dart';

import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class CustomMaterialAbsentRequest extends StatelessWidget {
  final String title;
  final VoidCallback onPressed;
  const CustomMaterialAbsentRequest({
    Key? key,
    required this.title,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      highlightColor: Colors.white,
      splashColor: Colors.white,
      padding: const EdgeInsets.all(0),
      onPressed: onPressed,
      child: Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
          decoration: BoxDecoration(
              border:
                  Border.all(color: Colorconstands.darkGray.withOpacity(0.5)),
              borderRadius: const BorderRadius.all(Radius.circular(10)),
              color:const Color(0xFFF1F9FF)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                title,
                style: ThemeConstands.texttheme.titleMedium?.copyWith(
                    color: Colorconstands.darkGray, fontWeight: FontWeight.w600),
              ),
              const Icon(
                Icons.keyboard_arrow_down_sharp,
                color: Colorconstands.darkGray,
              )
            ],
          )),
    );
  }

}