import 'package:flutter/material.dart';

import '../../../core/themes/themes.dart';

class CustomLabel extends StatelessWidget {
  final String title;
  final bool isOptionsl;
  const CustomLabel({
    Key? key,
    required this.title,
    required this.isOptionsl,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          title,
          style: ThemeConstands.texttheme.subtitle1
              ?.copyWith(fontWeight: FontWeight.w600),
        ),
        isOptionsl
            ? Text(
                "*",
                style: ThemeConstands.texttheme.subtitle1
                    ?.copyWith(fontWeight: FontWeight.w600, color: Colors.red),
              )
            : SizedBox(),
      ],
    );
  }
}
