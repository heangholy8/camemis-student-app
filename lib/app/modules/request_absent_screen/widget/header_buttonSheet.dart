import 'package:flutter/material.dart';
import '../../../../widgets/model_bottomSheet.dart';
import '../../../core/themes/themes.dart';

class HeaderButtomSheet extends StatelessWidget {
  final String title;
  final String subtitle;
  final double initialChildSize;
  final Widget child;
  const HeaderButtomSheet({Key? key, required this.title, required this.subtitle, required this.initialChildSize, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BuildBottomSheet(
      initialChildSize: initialChildSize,
      child: Container(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Column(
            children: [
              Center(
                  child: Text(
                title,
                style: ThemeConstands.texttheme.headline6,
              )),
              Container(
                padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 70),
                child: Center(
                    child: Text(
                  subtitle,
                  style: ThemeConstands.texttheme.subtitle2,
                  textAlign: TextAlign.center,
                )),
              ),
            ],
          ),
        ),
      expanded: child,
    );
  }
}