import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../../../widgets/custom_header.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class CustomHeaderAbsent extends StatelessWidget {
  final String title;
  bool isExpanded;
  CustomHeaderAbsent({
    Key? key,
    this.isExpanded = false,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding:const EdgeInsets.only(top: 10,bottom: 10),
      child: Column(
        children: [
          Container(
          margin: const EdgeInsets.only(left: 8.0),
          child: CustomHeader(title: "ASKPERMISSION".tr())),
          isExpanded
              ? Center(
                  child: Text(
                    title,
                    style: ThemeConstands.texttheme.headlineSmall
                        ?.copyWith(color: Colorconstands.white),
                  ),
                )
              :const SizedBox(),
        ],
      ),
    );
  }
}
