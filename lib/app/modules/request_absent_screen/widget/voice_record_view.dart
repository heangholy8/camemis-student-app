import 'dart:async';
import 'dart:io';

import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:new_im_animations/im_animations.dart';
import 'package:record/record.dart';

import '../controller/delete_cache_file.dart';
import '../model/attachment_model.dart';

class VoiceRecordView extends StatefulWidget {
  final ValueChanged<bool> onStart;
  final ValueChanged<bool> onCancel;
  final ValueChanged<Duration> onDuration;
  final ValueChanged<BaseAttachment> onReceive;
  const VoiceRecordView(
      {Key? key,
      required this.onReceive,
      required this.onStart,
      required this.onCancel,
      required this.onDuration})
      : super(key: key);

  @override
  State<VoiceRecordView> createState() => _VoiceRecordViewState();
}

class _VoiceRecordViewState extends State<VoiceRecordView> {
  ValueNotifier<bool>? _visibleSlideToCancelNotifier;
  Record? _record;
  // RecorderController? _recorderController;
  // PlayerController? _playerController;

  final iconMicActive = Container(
      margin: const EdgeInsets.only(bottom: 10),
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: Colorconstands.errorColor,
        borderRadius: BorderRadius.circular(50),
      ),
      child: Icon(CupertinoIcons.mic, color: Colorconstands.white, size: 30));

  final iconMicUnActive = Container(
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: Colorconstands.primaryColor,
        borderRadius: BorderRadius.circular(50),
      ),
      child: Icon(CupertinoIcons.mic, color: Colorconstands.white, size: 30));
  Timer? _timer;

  @override
  void initState() {
    super.initState();
    _visibleSlideToCancelNotifier = ValueNotifier<bool>(false);
    _record = Record();
    _requestPermission();
    // _playerController = PlayerController();
    // _recorderController = RecorderController()
    //   ..sampleRate = 12000
    //   ..androidEncoder = AndroidEncoder.aac
    //   ..iosEncoder = IosEncoder.kAudioFormatMPEG4AAC;
  }

  void _startTimer() {
    _timer?.cancel();

    _timer = Timer.periodic(const Duration(seconds: 1), (Timer t) {
      final minutes = t.tick ~/ 60;
      final seconds = t.tick % 60;
      final duration = Duration(minutes: minutes, seconds: seconds);
      widget.onDuration(duration);
    });
  }

  void _requestPermission() async {
    try {
      await _record?.hasPermission();
    } catch (e) {
      debugPrint('$e');
    }
  }

  @override
  void dispose() {
    _timer?.cancel();
    _visibleSlideToCancelNotifier?.dispose();
    _record?.dispose();
    // _recorderController?.disposeFunc();
    // _playerController?.disposeFunc();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onLongPress: () async {
          try {
            // final hasPermission = await AnotherAudioRecorder.hasPermissions;
            // if(hasPermission){
            //   final tempDir = await getTemporaryDirectory();
            //   _anotherAudioRecorder = AnotherAudioRecorder(tempDir.path + '${DateTime.now().millisecondsSinceEpoch}');
            //   final initialized = await _anotherAudioRecorder!.initialized;
            //   if(initialized == null) return;
            //   if(initialized){
            //     _visibleSlideToCancelNotifier!.value = true;
            //     await _anotherAudioRecorder?.start();
            //   }
            // }
            // final session = await AudioSession.instance;
            // await session.configure(const AudioSessionConfiguration(
            //   avAudioSessionCategory: AVAudioSessionCategory.playAndRecord,
            //   avAudioSessionMode: AVAudioSessionMode.spokenAudio,
            //   androidAudioAttributes: AndroidAudioAttributes(
            //     contentType: AndroidAudioContentType.speech,
            //     flags: AndroidAudioFlags.none,
            //     usage: AndroidAudioUsage.voiceCommunication
            //   ),
            //   androidAudioFocusGainType: AndroidAudioFocusGainType.gain,
            //   androidWillPauseWhenDucked: true
            // ));
            //
            // final hasPermission = await FlutterAudioRec.hasPermissions;
            // if(hasPermission == null) return;
            // if(hasPermission){
            //   final tempDir = await getTemporaryDirectory();
            //   _audioRec = FlutterAudioRec(tempDir.path + '${DateTime.now().millisecondsSinceEpoch}');
            //   final initialized = await _audioRec!.initialized;
            //   if(initialized == null) return;
            //   if(initialized){
            //     _visibleSlideToCancelNotifier!.value = true;
            //     await _audioRec?.start();
            //   }
            // }
            if (await _record!.hasPermission()) {
              if (await _record!.isEncoderSupported(AudioEncoder.aacLc)) {
                await _record?.start(encoder: AudioEncoder.aacLc);
                _visibleSlideToCancelNotifier!.value = true;
                widget.onStart(true);
                _startTimer();
              } else {
                ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                    content: Text('AudioEncoder not supported.'),
                    behavior: SnackBarBehavior.floating));
              }
            } else {
              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                  content: Text('Permission denied'),
                  behavior: SnackBarBehavior.floating));
            }
            // final checkPermission = await _recorderController?.checkPermission();
            // if(checkPermission!){
            //   if(_recorderController!.hasPermission){
            //     //_visibleSlideToCancelNotifier!.value = true;
            //     await _recorderController?.record();
            //   }
            // }
          } on PlatformException catch (e) {
            debugPrint('$e');
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text('${e.message} (${e.code})'),
                behavior: SnackBarBehavior.floating));
            deleteCacheFile();
          } catch (e) {
            debugPrint('$e');
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text('$e'), behavior: SnackBarBehavior.floating));
            deleteCacheFile();
          }
        },
        onLongPressMoveUpdate: (details) async {
          if (details.offsetFromOrigin.dx < 0 &&
              details.offsetFromOrigin.dx < -100.0) {
            _timer?.cancel();
            if (!_visibleSlideToCancelNotifier!.value) return;
            _visibleSlideToCancelNotifier!.value = false;
            final path = await _record?.stop();
            final file = File.fromUri(Uri.parse(path!));
            if (await file.exists()) {
              await file.delete();
            }
            widget.onCancel(true);
          }
        },
        onLongPressEnd: (details) async {
          _timer?.cancel();
          if (!_visibleSlideToCancelNotifier!.value) return;
          _visibleSlideToCancelNotifier!.value = false;
          try {
            final path = await _record?.stop();
            if (path == null) return;
            final file = File.fromUri(Uri.parse(path));
            final bytes = await file.readAsBytes();
            final attachment = AttachmentAudio(
                title: file.absolute.path.split('/').last,
                extension: file.absolute.path.split('.').last,
                file: file,
                bytes: bytes);
            widget.onReceive(attachment);
          } catch (e) {
            debugPrint('$e');
            widget.onCancel(true);
          }
        },
        child: ValueListenableBuilder<bool>(
            valueListenable: _visibleSlideToCancelNotifier!,
            builder: (ctx, isRecording, child) {
              if (!isRecording) {
                return iconMicUnActive;
              }
              return ColorSonar(
                  innerWaveColor: Colorconstands.errorColor,
                  middleWaveColor: Colorconstands.errorColor.withAlpha(100),
                  outerWaveColor: Colorconstands.errorColor.withAlpha(50),
                  waveMotionEffect: Curves.fastLinearToSlowEaseIn,
                  wavesDisabled: !isRecording,
                  contentAreaRadius: 18,
                  waveFall: 18.0,
                  child: isRecording ? iconMicActive : iconMicUnActive);
            }));
  }
}

// class VoiceRecorder{
//   static const platform = MethodChannel('voice_recorder');
//   static Future<dynamic> startRecord() async{
//     try{
//       await platform.invokeMethod('voiceStartRecord');
//     } on PlatformException catch(e){
//       debugPrint('$e');
//     }
//   }
//
//   static Future<dynamic> stopRecord() async{
//     try{
//       await platform.invokeMethod('voiceStopRecord');
//     } on PlatformException catch(e){
//       debugPrint('$e');
//     }
//   }
// }
