import 'dart:io';
import 'dart:typed_data';
import 'package:just_audio/just_audio.dart' as audio;

abstract class BaseAttachment{
  final String? extension;
  final String? title;
  final File? file;

  const BaseAttachment({this.extension, this.title, this.file});

  void dispose();
}

class AttachmentFile extends BaseAttachment{
  const AttachmentFile({final String? extension, final String? title, final File? file}) : super(extension: extension, title: title, file: file);

  @override
  void dispose() {
    // TODO: implement dispose
  }
}

class AttachmentAudio extends BaseAttachment{
  // final PlayerController playerController;
  // const AttachmentAudio({required this.playerController, final String? extension, final String? title, final File? file}) : super(extension: extension, title: title, file: file);
  final audio.AudioPlayer player = audio.AudioPlayer();
  final Uint8List bytes;
  AttachmentAudio({required this.bytes, final String? extension, final String? title, final File? file}) : super(extension: extension, title: title, file: file);

  @override
  void dispose() async{
    // TODO: implement dispose
    await player.dispose();
  }
}