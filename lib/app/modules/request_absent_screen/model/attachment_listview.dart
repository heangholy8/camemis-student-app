import 'dart:math' show log, pow;
import 'dart:typed_data' show Uint8List;
import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_audio_waveforms/flutter_audio_waveforms.dart';
import 'package:fluttertoast/fluttertoast.dart';
// import 'package:rupp_client_app/widgets/custom_progressbar.dart';
import 'package:just_audio/just_audio.dart' as audio;

import '../../../../widgets/empty_widget.dart';
import '../../../../widgets/loading_widget.dart';
import 'attachment_model.dart';

const subtitle1 = TextStyle(fontSize: 10.0, color: Colors.black);
const titleMedium = TextStyle(fontSize: 12.0, fontWeight: FontWeight.w600);

abstract class AttachmentListView {
  final BaseAttachment attachment;
  final VoidCallback onDelete;
  const AttachmentListView({required this.attachment, required this.onDelete});
  Widget build(final BuildContext context);

  String formatBytes(int bytes, int decimals) {
    if (bytes <= 0) return "0 B";
    const suffixes = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
    final i = (log(bytes) / log(1024)).floor();
    return ((bytes / pow(1000, i)).toStringAsFixed(decimals)) +
        '\t' +
        suffixes[i];
  }
}

class AttachmentFileView extends AttachmentListView {
  const AttachmentFileView(
      {required BaseAttachment attachment, required VoidCallback onDelete})
      : super(attachment: attachment, onDelete: onDelete);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(children: [
        SizedBox(
            width: MediaQuery.of(context).size.width / 4,
            height: 60.0,
            child: Builder(builder: (ctx) {
              if (attachment.extension == 'jpeg' ||
                  attachment.extension == 'png' ||
                  attachment.extension == 'jpg') {
                print("AttachmentFileView: ${attachment.file}");
                return Stack(
                  children: [
                    Image.file(
                      attachment.file!,
                      height: 60,
                      width: 100,
                      fit: BoxFit.cover,
                      // errorBuilder: (ctx, str, _) =>
                      //     Center(child: CircularProgressIndicator())
                    ),
                    Positioned(
                      top: -15,
                      right: -15,
                      child: IconButton(
                          onPressed: onDelete,
                          icon: const Icon(
                            Icons.cancel,
                            size: 15,
                            color: Colors.red,
                          )),
                    )
                  ],
                );
              } else if (attachment.extension == 'pdf') {
                return Image.asset('assets/png/pdf.png',
                    errorBuilder: (ctx, str, _) => EmptyWidget.instance);
              } else if (attachment.extension == 'docx' ||
                  attachment.extension == 'doc') {
                return Image.asset('assets/png/word.png',
                    errorBuilder: (ctx, str, _) => EmptyWidget.instance);
              } else {
                return Container(color: Colors.black.withAlpha(12));
              }
            })),
      ]),
    );
    // return ListTile(
    //     title: Text(attachment.title!,
    //         style: Theme.of(context)
    //             .textTheme
    //             .copyWith(titleMedium: titleMedium)
    //             .titleMedium),
    //     subtitle: FutureBuilder<Uint8List>(
    //         future: attachment.file!.readAsBytes(),
    //         initialData: null,
    //         builder: (ctx, snapshot) {
    //           if (snapshot.hasData) {
    //             return Text(formatBytes(snapshot.data!.length, 2),
    //                 style: Theme.of(context)
    //                     .textTheme
    //                     .copyWith(subtitle1: subtitle1)
    //                     .subtitle1);
    //           } else {
    //             return EmptyWidget.instance;
    //           }
    //         }),
    //     leading: SizedBox(
    //         width: 32.0,
    //         height: 40.0,
    //         child: Builder(builder: (ctx) {
    //           if (attachment.extension == 'jpeg' ||
    //               attachment.extension == 'png' ||
    //               attachment.extension == 'jpg') {
    //             return ClipRRect(
    //                 borderRadius: BorderRadius.circular(1.0),
    //                 child: Image.file(attachment.file!,
    //                     fit: BoxFit.cover,
    //                     errorBuilder: (ctx, str, _) => EmptyWidget.instance));
    //           } else if (attachment.extension == 'pdf') {
    //             return Image.asset('assets/png/pdf.png',
    //                 errorBuilder: (ctx, str, _) => EmptyWidget.instance);
    //           } else if (attachment.extension == 'docx' ||
    //               attachment.extension == 'doc') {
    //             return Image.asset('assets/png/word.png',
    //                 errorBuilder: (ctx, str, _) => EmptyWidget.instance);
    //           } else {
    //             return Container(color: Colors.black.withAlpha(12));
    //           }
    //         })),
    //     trailing: AttachmentTrailing(onDelete: onDelete));
  }
}

const circleShape = CircleBorder();

class AttachmentAudioView extends AttachmentListView {
  // final PlayerController playerController;
  //final AssetsAudioPlayer _audioPlayer = AssetsAudioPlayer();
  // final _player = audio.AudioPlayer();
  AttachmentAudioView(
      {required BaseAttachment attachment, required VoidCallback onDelete})
      : super(attachment: attachment, onDelete: onDelete) {
    if (attachment is AttachmentAudio) {
      player.setFilePath(attachment.file!.path, preload: true);
    }
  }

  static const _playIcon = Icon(Icons.play_arrow_outlined,
      color: Colorconstands.primaryColor, size: 25.0);
  static const _pauseIcon = Icon(Icons.stop_circle_outlined,
      color: Colorconstands.primaryColor, size: 25.0);
  static const totalSample = 128;

  audio.AudioPlayer get player => (attachment as AttachmentAudio).player;
  List<double> get bytes {
    List<double> b = List.empty(growable: true);
    final byteData = (attachment as AttachmentAudio).bytes;
    for (var item in byteData.where((element) => element > 0)) {
      if (b.length == totalSample) break;
      b.add(item.toDouble());
    }
    return b;
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(

        // title: AudioFileWaveforms(
        //   size: const Size(double.infinity, 50),
        //   playerController: playerController,
        //   enableSeekGesture: true,
        //   animationCurve: Curves.ease
        // ),
        title: Row(children: [
      Container(
          decoration: BoxDecoration(
              color: Colorconstands.secondaryColor.withOpacity(0.5),
              borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(10),
                  bottomLeft: Radius.circular(10))),
          width: 45.0,
          height: 45.0,
          child: RawMaterialButton(
              child: StreamBuilder<audio.PlayerState>(
                  stream: player.playerStateStream,
                  initialData: null,
                  builder: (ctx, snapshot) {
                    if (snapshot.hasData) {
                      if (snapshot.data!.playing &&
                              snapshot.data!.processingState ==
                                  audio.ProcessingState.ready ||
                          snapshot.data!.playing &&
                              snapshot.data!.processingState ==
                                  audio.ProcessingState.completed) {
                        if (snapshot.data!.playing &&
                            snapshot.data!.processingState ==
                                audio.ProcessingState.ready) {
                          return _pauseIcon;
                        }
                        return FutureBuilder<void>(
                            future: player.pause(),
                            builder: (ctx, snapshot) {
                              return _playIcon;
                            });
                      }
                      if (!snapshot.data!.playing &&
                          snapshot.data!.processingState ==
                              audio.ProcessingState.ready) {
                        return _playIcon;
                      }
                      if (!snapshot.data!.playing &&
                          snapshot.data!.processingState ==
                              audio.ProcessingState.idle) {
                        return _playIcon;
                      }
                      return _playIcon;
                    } else {
                      return LoadingWidget.instance;
                    }
                  }),
              onPressed: () async {
                if (attachment is AttachmentAudio) {
                  try {
                    // final attachmentAudio = (attachment as AttachmentAudio);
                    // final path = attachmentAudio.file!.path.substring(2, attachmentAudio.file!.path.length);
                    // final audioFile = Audio.file(path);
                    // await _audioPlayer.open(audioFile);
                    // await _audioPlayer.play();

                    if (player.processingState == audio.ProcessingState.ready) {
                      if (player.playerState.playing) {
                        await player.stop();
                        return;
                      }
                      await player.play();
                    } else {
                      await player.load();
                      await player.play();
                    }

                    // final attachmentAudio = (attachment as AttachmentAudio);
                    // await _player.play(attachmentAudio.file!.path, isLocal: true);

                  } on audio.PlayerException catch (e) {
                    debugPrint('$e');
                    Fluttertoast.showToast(msg: '$e');
                  } on PlatformException catch (e) {
                    debugPrint('$e');
                    Fluttertoast.showToast(msg: '${e.code} - ${e.details}');
                  } catch (e) {
                    debugPrint('$e');
                    Fluttertoast.showToast(msg: '$e');
                  }
                }
              })),
      Expanded(
          child: Container(
        margin: const EdgeInsets.only(left: 5, right: 10),
        child: FutureBuilder<Duration?>(
            future: player.load(),
            initialData: Duration.zero,
            builder: (ctx, snapshotDuration) {
              if (!snapshotDuration.hasData) {
                return Text('Failed to loads',
                    style: ThemeConstands.texttheme.subtitle1);
              }
              return StreamBuilder<Duration>(
                  stream: player.positionStream,
                  builder: (ctx, snapshotPosition) {
                    return StreamBuilder<Duration>(
                        stream: player.bufferedPositionStream,
                        initialData: Duration.zero,
                        builder: (ctx, snapshotBuffer) {
                          if (snapshotDuration.data == Duration.zero) {
                            return LoadingWidget.instance;
                          }
                          return LayoutBuilder(builder: (ctx, constraints) {
                            return Container(
                              height: 45,
                              decoration: BoxDecoration(
                                  color: Colorconstands.secondaryColor
                                      .withOpacity(0.5),
                                  borderRadius: const BorderRadius.only(
                                      topRight: Radius.circular(10),
                                      bottomRight: Radius.circular(10))),
                              child: Center(
                                child: RectangleWaveform(
                                    width: constraints.maxWidth,
                                    height: 30.0,
                                    activeColor: Colorconstands.secondaryColor,
                                    inactiveColor: Colors.white,
                                    samples: bytes,
                                    absolute: false,
                                    isCentered: true,
                                    isRoundedRectangle: true,
                                    borderWidth: 0.0,
                                    activeBorderColor:
                                        Colorconstands.secondaryColor,
                                    inactiveBorderColor: Colors.transparent,
                                    maxDuration: snapshotDuration.data,
                                    elapsedDuration: snapshotPosition.data),
                              ),
                            );
                          });
                        });
                  });
            }),
      )),
      AttachmentTrailing(onDelete: onDelete)
    ]));
  }
}

class AttachmentListViewCreator {
  const AttachmentListViewCreator();

  AttachmentListView factoryMethod(
      {required final BaseAttachment attachment,
      required final VoidCallback onDelete}) {
    if (attachment is AttachmentAudio) {
      return AttachmentAudioView(attachment: attachment, onDelete: onDelete);
    } else {
      return AttachmentFileView(attachment: attachment, onDelete: onDelete);
    }
  }
}

class AttachmentTrailing extends StatelessWidget {
  final VoidCallback onDelete;
  const AttachmentTrailing({Key? key, required this.onDelete})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            color: Colorconstands.secondaryColor.withOpacity(0.5),
            borderRadius: const BorderRadius.all(Radius.circular(10))),
        width: 45.0,
        height: 45.0,
        child: ClipRRect(
            borderRadius: BorderRadius.circular(2.0),
            child: Material(
                type: MaterialType.transparency,
                child: InkWell(
                    child: const Icon(Icons.close,
                        size: 20.0, color: Colorconstands.primaryColor),
                    onTap: onDelete))));
  }
}
