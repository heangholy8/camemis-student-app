part of 'request_absent_bloc.dart';

abstract class RequestAbsentState extends Equatable {
  const RequestAbsentState();
  
  @override
  List<Object> get props => [];
}

class RequestAbsentInitial extends RequestAbsentState {}

class RequestAbsentLoadingState extends RequestAbsentState {
  final String message;
  const RequestAbsentLoadingState({
    required this.message,
  });
}

class RequestReasonLoadedState extends RequestAbsentState {
  final BaseResponseModel<List<GetAdmissionModel>>? getAdmissionModel;
  const RequestReasonLoadedState({required this.getAdmissionModel});
}

class RequestAbsentErrorState extends RequestAbsentState {
  final String error;

  const RequestAbsentErrorState({
    required this.error,
  });
}

