import 'package:bloc/bloc.dart';
import 'package:camis_application_flutter/service/apis/permission_request/get_admission.dart';
import 'package:equatable/equatable.dart';

import '../../../../model/base/base_respone_model.dart';
import '../../../../model/permission_request_model/admission_model.dart';

part 'request_absent_event.dart';
part 'request_absent_state.dart';

class RequestAbsentBloc extends Bloc<RequestAbsentEvent, RequestAbsentState> {
  RequestAbsentBloc() : super(RequestAbsentInitial()) {
    on<RequestAbsentEvent>((event, emit) {
     
    });
  }
}

class RequestReasonBloc extends Bloc<RequestAbsentEvent, RequestAbsentState> {
  final GetadmissionApi getadmissionApi;
  RequestReasonBloc({required this.getadmissionApi}) : super(RequestAbsentInitial()) {
    on<RequestReasonEvent>((event, emit) async{
      emit(const RequestAbsentLoadingState(message: "Loading....."));
      try{
        var data = await getadmissionApi.getadmissionApi();
        emit(RequestReasonLoadedState(getAdmissionModel: data));
      }
      catch(e){
        emit(const RequestAbsentErrorState(error: "Error Data"));
      }
    });
  }
}

