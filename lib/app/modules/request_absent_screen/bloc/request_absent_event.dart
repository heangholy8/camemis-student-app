part of 'request_absent_bloc.dart';

abstract class RequestAbsentEvent extends Equatable {
  const RequestAbsentEvent();

  @override
  List<Object> get props => [];
}

class RequestReasonEvent extends RequestAbsentEvent{
  const RequestReasonEvent();
}
