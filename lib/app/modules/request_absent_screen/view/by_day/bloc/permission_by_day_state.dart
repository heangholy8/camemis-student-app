part of 'permission_by_day_bloc.dart';

abstract class PermissionByDayState extends Equatable {
  const PermissionByDayState();
  
  @override
  List<Object> get props => [];
}

class PermissionByDayInitial extends PermissionByDayState {}


class PostPermissionLoadingState extends PermissionByDayState {
  const PostPermissionLoadingState();
}

class PostPermissionLoadedState extends PermissionByDayState {

    final BaseResponseModel<GetPermissionRequestDetailModel>? getAdmissionModel;
  const PostPermissionLoadedState({required this.getAdmissionModel});
}

class PostPermissionErrorState extends PermissionByDayState {
  final String error;

  const PostPermissionErrorState({
    required this.error,
  });
}
