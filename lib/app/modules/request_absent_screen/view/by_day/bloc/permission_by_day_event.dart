part of 'permission_by_day_bloc.dart';

abstract class PermissionByDayEvent extends Equatable {
  const PermissionByDayEvent();

  @override
  List<Object> get props => [];
}
class PostPermissByDay extends PermissionByDayEvent{
  final String studentId;
  final String comment;
  final String startDate;
  final String endDate;
  final String reason;
  const PostPermissByDay({required this.studentId,required this.comment,
   required this.startDate, required this.endDate, required this.reason,});
}
