import 'package:bloc/bloc.dart';
import 'package:camis_application_flutter/service/apis/permission_request/get_admission.dart';
import 'package:equatable/equatable.dart';

import '../../../../../../model/base/base_respone_model.dart';
import '../../../../../../model/permission_request_model/admission_model.dart';
import '../../../../../../model/permission_request_model/permissioo_detail.dart';
import '../../../../../../service/apis/permission_request/post_permission_date.dart';

part 'permission_by_day_event.dart';
part 'permission_by_day_state.dart';

class PermissionByDayBloc extends Bloc<PermissionByDayEvent, PermissionByDayState> {
  final PostPermission permission;
  BaseResponseModel<GetPermissionRequestDetailModel>? getAdmissionModel;
  PermissionByDayBloc({required this.permission})
      : super(PermissionByDayInitial()) {
    on<PostPermissByDay>((event, emit) async {
      emit(const PostPermissionLoadingState());
      try {
        var dataResponPostPermiss = await permission.postPermissionDayApi(
          studentId: event.studentId,
          comment: event.comment,
          startDate: event.startDate,
          endDate: event.endDate,
          reason: event.reason,
        );
        emit(PostPermissionLoadedState(getAdmissionModel: dataResponPostPermiss));
      } catch (e) {
        emit(PostPermissionErrorState(error: e.toString()));
      }
    });
  }
}
