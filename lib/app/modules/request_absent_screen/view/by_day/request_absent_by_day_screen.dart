import 'dart:async';
import 'dart:io' show File;
import 'package:camis_application_flutter/app/core/resources/asset_resource.dart';
import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:camis_application_flutter/app/modules/request_absent_screen/widget/header_buttonSheet.dart';
import 'package:camis_application_flutter/widgets/button_widget/button_customwidget.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import '../../../../../component/change_child/bloc/change_child_bloc.dart';
import '../../../../../storages/get_storage.dart';
import '../../../../../widgets/composite_widget.dart';
import '../../../../../widgets/custom_alertdialog.dart';
import '../../../../../widgets/empty_widget.dart';
import '../../../../../widgets/shimmer_style.dart';
import '../../../../../widgets/show_menu_gallary.dart';
import '../../../../../widgets/toast_nointernet.dart';
import '../../../../core/themes/themes.dart';
import '../../../absent_request_screen.dart/view/absent_request_screen.dart';
import '../../bloc/request_absent_bloc.dart';
import '../../model/attachment_listview.dart';
import '../../model/attachment_model.dart';
import '../../widget/custom_absent_request.dart';
import '../../widget/custom_header_absent.dart';
import '../../widget/custom_label.dart';
import '../../widget/voice_record_view.dart';
import 'bloc/permission_by_day_bloc.dart';

class RequestAbsentByDayScreen extends StatefulWidget {
  final String? day;
  const RequestAbsentByDayScreen({
    Key? key,
    this.day,
  }) : super(key: key);
  @override
  State<RequestAbsentByDayScreen> createState() =>
      _RequestAbsentByDayScreenState();
}

class _RequestAbsentByDayScreenState extends State<RequestAbsentByDayScreen> {
  final ImagePicker _picker = ImagePicker();

  List<XFile> _imageList = [];
  List<dynamic>? _documents = [];

  List<File> filesDoc = [];
  List<File> filesDocPath = [];
  bool otheroption = false;
  ValueNotifier<bool>? _visibleSlideToCancelNotifier;
  StreamController<Duration>? _voiceDurationController;
  String? studentName, date, timeTable, reason, comment;
  bool isRecording = false;
  static const _kTweenDuration = Duration(milliseconds: 200);
  final List<BaseAttachment> _attachments = List.empty(growable: true);
  final List<BaseAttachment> _voice = List.empty(growable: true);
  ValueNotifier<BaseAttachment?>? _attachmentNotifier;
  ValueNotifier<BaseAttachment?>? _voiceNotifier;
  final AttachmentListViewCreator _creator = const AttachmentListViewCreator();
  final _reasonController = TextEditingController();
  TextEditingController otherInput = TextEditingController();
  int? selectChild;
  String? activeDate;
  String? studentId;
  String? startDate;
  String? endDate;
  String? idreason;
  bool checkSubject = false;
  StreamSubscription? stsub;
  bool connection = true;
  @override
  void initState() {
    super.initState();
    //=============Check internet====================
    stsub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
      });
    });
    //=============Check internet====================
    activeDate = DateFormat("dd/MM/yyyy").format(DateTime.now());
    getChildrent();
    getDate();
    BlocProvider.of<RequestReasonBloc>(context).add(const RequestReasonEvent());
    BlocProvider.of<ChangeChildBloc>(context).add(GetChildrenEvent());
    _visibleSlideToCancelNotifier = ValueNotifier<bool>(false);
    _voiceDurationController = StreamController<Duration>.broadcast();
    _attachmentNotifier = ValueNotifier<BaseAttachment?>(null);
    _voiceNotifier = ValueNotifier<BaseAttachment?>(null);
    startDate = widget.day ?? activeDate;
    endDate = widget.day ?? activeDate;
  }

  @override
  void dispose() {
    stsub?.cancel();
    FilePicker.platform.clearTemporaryFiles().then((value) {
      if (value!) {
        debugPrint('Temporary files deleted');
      }
    }).onError((error, stackTrace) {
      debugPrint('$error');
    }).catchError((onError) {
      debugPrint('$onError');
    });
    for (var item in _attachments) {
      item.file!.exists().then((fileIsExist) {
        if (fileIsExist) {
          item.file?.delete().then((value) {}).onError((error, stackTrace) {
            debugPrint('$error');
          }).catchError((onError) {
            debugPrint('$onError');
          });
        }
      }).onError((error, stackTrace) {
        debugPrint('$error');
      }).catchError((onError) {
        debugPrint('$onError');
      });
      //
      item.dispose();
    }
    _voiceDurationController?.close();
    _attachmentNotifier?.dispose();
    _voiceNotifier?.dispose();
    _visibleSlideToCancelNotifier?.dispose();
    super.dispose();
  }
  // void _openFileExplorer() async {
  //    FilePickerResult? result = await FilePicker.platform.pickFiles(allowMultiple: true,allowedExtensions: ['pdf'],type: FileType.custom);
  //    setState(() {

  //    });
  //    PlatformFile file = result!.files.first;
  //     if (result != null){
  //       setState(() {
  //         List<File> filepath = result.paths.map((path) => File(path!)).toList();
  //         List<File> filename = result.names.map((name) => File(name!)).toList();
  //         filesDocPath = filepath;
  //         filesDoc = filename;
  //       });
  //     }
  //     else{
  //       return;
  //     }
  // }
  void _openImageExplorer() async {
    final List<XFile>? selected = await _picker.pickMultiImage();
    if (selected!.isNotEmpty) {
      _imageList = selected;
      //_imageList.add(selected);
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colorconstands.primaryColor,
        body: SafeArea(
          bottom: false,
          child: Column(children: [
            CustomHeaderAbsent(
              title: "",
            ),
            Expanded(
                child: Container(
              width: MediaQuery.of(context).size.width,
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15)),
                color: Colorconstands.white,
              ),
              child: SingleChildScrollView(
                child: BlocListener<PermissionByDayBloc, PermissionByDayState>(
                  listener: (context, state) {
                    if (state is PostPermissionLoadingState) {
                      showDialog(
                        barrierDismissible: false,
                        barrierColor: Colors.black38,
                        context: context,
                        builder: (context) {
                          return Dialog(
                            elevation: 5,
                              backgroundColor: Colorconstands.gray.withOpacity(0.8),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                            child: Container(
                              width: MediaQuery.of(context).size.width/5,
                              height: MediaQuery.of(context).size.width/5,
                              padding:const EdgeInsets.all(10),
                              child:const Center(
                                child: CircularProgressIndicator(),
                              ),
                            ),
                          );
                        },
                      );
                    }
                    if (state is PostPermissionLoadedState) {
                      Navigator.of(context).pop();
                      successRequest(context);
                    }
                    if(state is PostPermissionErrorState){
                      Navigator.of(context).pop();
                      successRequest(context);
                    }
                    else {
                    }
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      const CustomLabel(
                        title: "Student Name",
                        isOptionsl: true,
                      ),
                      //==============Get Child=========
                      getChildrent(),
                      //==============End Get Child=========
                      const CustomLabel(
                        title: "Selecte Date",
                        isOptionsl: false,
                      ),
                      //==============Get Date=========
                      getDate(),
                      //==============End Get Date=========
                      const CustomLabel(
                        title: "Selecte Reason",
                        isOptionsl: false,
                      ),
                      //==============Get Reason=========
                      getReason(),
                      //==============End Get Reason=========
                      const CustomLabel(
                        title: "Comment",
                        isOptionsl: false,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      isRecording
                          ? Container(
                              margin: const EdgeInsets.symmetric(vertical: 10),
                              child: Center(
                                child: Row(
                                  children: [
                                    Container(
                                        padding: const EdgeInsets.all(8),
                                        decoration: const BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(10),
                                                bottomLeft:
                                                    Radius.circular(10)),
                                            color:
                                                Colorconstands.backgroundColor),
                                        child: SvgPicture.asset(
                                          ImageAssets.delete_bin,
                                          height: 30,
                                        )),
                                    Expanded(
                                      child: Container(
                                          margin: const EdgeInsets.only(
                                              left: 3, right: 5),
                                          padding: const EdgeInsets.all(8),
                                          decoration: const BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                                topRight: Radius.circular(10),
                                                bottomRight:
                                                    Radius.circular(10)),
                                          ),
                                          child: const SizedBox()),
                                    ),
                                    Container(
                                        padding: const EdgeInsets.all(8),
                                        decoration: const BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(8)),
                                            color:
                                                Colorconstands.backgroundColor),
                                        child: SvgPicture.asset(
                                          ImageAssets.sent_icon,
                                          height: 30,
                                        ))
                                  ],
                                ),
                              ),
                            )
                          : const SizedBox(),
                      ColumnComposite(iComponent: [
                        Component(
                            child: GestureDetector(
                                onTap: () {},
                                child: Container(
                                    //padding: const EdgeInsets.all(16.0),
                                    decoration: BoxDecoration(
                                        color: const Color.fromRGBO(
                                            241, 249, 255, 1),
                                        border: Border.all(
                                            color: Colorconstands.darkGray
                                                .withOpacity(0.5)),
                                        borderRadius:
                                            BorderRadius.circular(10.0)),
                                    child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          TextFormField(
                                            textAlign: TextAlign.start,
                                            controller: _reasonController,
                                            keyboardType: TextInputType.text,
                                            minLines: 3,
                                            maxLines: null,
                                            decoration: const InputDecoration(
                                              contentPadding:
                                                  EdgeInsets.all(15),
                                              border: InputBorder.none,
                                              hintText: "Type a comment.....",
                                              hintStyle: TextStyle(
                                                  color:
                                                      Colorconstands.darkGray),
                                            ),
                                            style: const TextStyle(
                                                color: Colorconstands.darkGray,
                                                fontSize: 16),
                                          ),
                                          InkWell(
                                              borderRadius:
                                                  BorderRadius.circular(50.0),
                                              child: const SizedBox(
                                                  width: 50.0,
                                                  height: 50.0,
                                                  child: Icon(
                                                      Icons
                                                          .attach_file_outlined,
                                                      color: Colorconstands
                                                          .iconColor)),
                                              onTap: () {
                                                setState(() {
                                                  showModalBottomSheet(
                                                      backgroundColor:
                                                          Colors.transparent,
                                                      context: context,
                                                      builder:
                                                          (BuildContext bc) {
                                                        return ShowMenuGallary(
                                                          nameItem1:
                                                              'Upload Image'
                                                                  .tr(),
                                                          nameItem2: ''.tr(),
                                                          onPressedItem1: () {
                                                            setState(() {
                                                              _openImageExplorer();
                                                              Navigator.of(
                                                                      context)
                                                                  .pop();
                                                            });
                                                          },
                                                          onPressedItem2: () {
                                                            // _openFileExplorer();
                                                            // Navigator.of(context).pop();
                                                          },
                                                        );
                                                      });
                                                });
                                              }),
                                        ]))))
                      ]).build(context),
                      _imageList.isEmpty
                          ? Container()
                          : GridView.builder(
                              padding: const EdgeInsets.all(0),
                              physics: const ScrollPhysics(),
                              shrinkWrap: true,
                              gridDelegate:
                                  const SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 4),
                              itemBuilder: (_, index) => Padding(
                                padding: const EdgeInsets.all(2),
                                child: Stack(
                                  fit: StackFit.expand,
                                  children: [
                                    Image.file(
                                      File(_imageList[index].path),
                                      fit: BoxFit.cover,
                                    ),
                                    Positioned(
                                      right: 0,
                                      top: 4,
                                      child: Container(
                                        height: 25,
                                        decoration: const BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.red,
                                        ),
                                        child: IconButton(
                                          padding: const EdgeInsets.all(0),
                                          onPressed: () {
                                            setState(() {
                                              _imageList.removeAt(index);
                                            });
                                          },
                                          icon: const Icon(Icons.close_sharp,
                                              color: Colors.white),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              itemCount: _imageList.length,
                            ),
                      _imageList.isEmpty ? Container() : const Divider(),
                      // filesDocPath.isEmpty?Container():ListView.builder(
                      //   padding:const EdgeInsets.all(0),
                      //   physics:const ScrollPhysics(),
                      //   shrinkWrap: true,
                      //   itemCount: filesDoc.length,
                      //   itemBuilder: (context, index) {
                      //     return TextButton(onPressed: () {
                      //       setState(() {
                      //         Navigator.push(context, MaterialPageRoute(builder: (context){
                      //          return ViewPDF(pathPDF:filesDocPath[index].path.toString());
                      //        }));
                      //         // Navigator.of(context).push(MaterialPageRoute(builder : (context,)=> ViewDocuments(path: filesDocPath[index].path.toString(),filename: filesDoc[index].path.toString(),)));
                      //       });
                      //      },
                      //     child: Text(filesDoc[index].path));
                      //   }
                      // ),
                      // filesDocPath.isEmpty ? Container() : const Divider(),
                      Container(
                        decoration: const BoxDecoration(
                            color: Color(0xFFF1F9FF),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0))),
                        child: ValueListenableBuilder<BaseAttachment?>(
                            valueListenable: _attachmentNotifier!,
                            child: EmptyWidget.instance,
                            builder: (ctx, attachment, child) {
                              if (attachment == null || _attachments.isEmpty) {
                                return child!;
                              }
                              return Wrap(
                                  runSpacing: 3,
                                  spacing: MediaQuery.of(context).size.width,
                                  alignment: WrapAlignment.spaceBetween,
                                  runAlignment: WrapAlignment.spaceBetween,
                                  direction: Axis.horizontal,
                                  children: _attachments.map((e) {
                                    print(e.extension);
                                    return _creator
                                        .factoryMethod(
                                            attachment: e,
                                            onDelete: () async {
                                              if (mounted) {
                                                try {
                                                  if (e is AttachmentAudio) {
                                                    await e.player.stop();
                                                  }
                                                  if (await e.file!.exists()) {
                                                    await e.file!.delete();
                                                  }
                                                  //
                                                  setState(() {
                                                    _attachments.remove(e);
                                                  });
                                                } catch (e) {
                                                  debugPrint('$e');
                                                }
                                              }
                                            })
                                        .build(context);
                                  }).toList());
                            }),
                      ),
                      ValueListenableBuilder<BaseAttachment?>(
                          valueListenable: _voiceNotifier!,
                          child: EmptyWidget.instance,
                          builder: (ctx, attachment, child) {
                            if (attachment == null || _voice.isEmpty) {
                              return child!;
                            }
                            return Column(
                                children: _voice.map((e) {
                              print(e.extension);
                              return _creator
                                  .factoryMethod(
                                      attachment: e,
                                      onDelete: () async {
                                        if (mounted) {
                                          try {
                                            if (e is AttachmentAudio) {
                                              await e.player.stop();
                                            }
                                            if (await e.file!.exists()) {
                                              await e.file!.delete();
                                            }
                                            //
                                            setState(() {
                                              _voice.remove(e);
                                            });
                                          } catch (e) {
                                            debugPrint('');
                                          }
                                        }
                                      })
                                  .build(context);
                            }).toList());
                          }),
                      SizedBox(
                        height: 10,
                      ),
                      const Center(
                        child: Text("Record Voice",
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: Colorconstands.darkGray)),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      Center(
                        child: VoiceRecordView(onStart: (v) {
                          _visibleSlideToCancelNotifier!.value = v;
                        }, onDuration: (duration) {
                          _voiceDurationController?.sink.add(duration);
                        }, onCancel: (v) {
                          _visibleSlideToCancelNotifier!.value = !v;
                        }, onReceive: (audio) {
                          _voice.add(audio);
                          _voiceNotifier!.value = audio;
                          if (_visibleSlideToCancelNotifier!.value) {
                            _visibleSlideToCancelNotifier!.value = false;
                          }
                        }),
                      ),
                      const SizedBox(
                        height: 30,
                      ),

                      Center(
                        child: Row(children: [
                          Expanded(
                              child: ValueListenableBuilder<bool>(
                                  valueListenable:
                                      _visibleSlideToCancelNotifier!,
                                  child: Row(children: const [
                                    Icon(Icons.arrow_back_ios, size: 10.0),
                                    SizedBox(width: 8.0),
                                    Flexible(
                                        child: Text('Slide to Cancel',
                                            style: TextStyle(
                                                fontSize: 10.0,
                                                color: Colors.black)))
                                  ]),
                                  builder: (ctx, visible, child) {
                                    return TweenAnimationBuilder<double>(
                                        tween: visible
                                            ? Tween(begin: 0.0, end: 1.0)
                                            : Tween(begin: 1.0, end: 0.0),
                                        duration: _kTweenDuration,
                                        child: Row(children: [
                                          Flexible(
                                              child: Row(children: [
                                            const SizedBox(width: 16.0),
                                            Container(
                                                width: 6.0,
                                                height: 6.0,
                                                decoration: const BoxDecoration(
                                                    color: Colorconstands
                                                        .primaryColor,
                                                    shape: BoxShape.circle)),
                                            const SizedBox(width: 8.0),
                                            StreamBuilder<Duration>(
                                                stream:
                                                    _voiceDurationController!
                                                        .stream,
                                                initialData: Duration.zero,
                                                builder: (ctx, snapshot) {
                                                  return Text(
                                                      snapshot.data!.toString(),
                                                      style: ThemeConstands
                                                          .texttheme.subtitle1);
                                                })
                                          ])),
                                          Flexible(child: child!)
                                        ]),
                                        builder: (ctx, tween, child) {
                                          return Opacity(
                                              opacity: tween,
                                              child: Visibility(
                                                  visible: tween == 0.0
                                                      ? false
                                                      : true,
                                                  child: child!));
                                        });
                                  })),
                          Expanded(
                              flex: 0,
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: []))
                        ]),
                      ),
                      const SizedBox(height: 20),
                      Button_Custom(
                        radiusButton: 10,
                        titlebuttonColor: Colorconstands.white,
                        hightButton: 55,
                        buttonColor: Colorconstands.primaryColor,
                        titleButton: "Submit",
                        onPressed:connection == false ? () {showtoastInternet(context);}:  () async {
                          // if (_imageList.isNotEmpty) {
                          //   // for(int i =0 ; i<_imageList.length;i++){
                          //   //  var path = _imageList[i].path;
                          //   //  _documents!.add(await MultipartFile.fromFile(path,
                          //   //   filename: path.split('/').last,));
                          //   // }
                          // }
                          setState(() {
                            if(idreason==null){
                              unuccessRequest (context);
                            }
                            else{
                              BlocProvider.of<PermissionByDayBloc>(context)
                                        .add(PostPermissByDay(comment: _reasonController.text,studentId: studentId.toString()
                                        ,reason: idreason.toString(),startDate:startDate.toString(),endDate: endDate.toString() ));
                            }
                          });
                        },
                      ),
                      // const SizedBox(
                      //   height: 10,
                      // ),
                      // ButtonOutlineCustom(
                      //   titleButton: "Reset all",
                      //   buttonColor: Colorconstands.white,
                      //   radiusButton: 10,
                      //   onPressed: () {
                      //   },
                      //   hightButton: 55,
                      //   titlebuttonColor: Colorconstands.black,
                      //   borderColor: Colorconstands.white,
                      //   sizeText: 16,
                      //   widthButton: MediaQuery.of(context).size.width,
                      //   child: const SizedBox(),
                      // ),
                      const SizedBox(
                        height: 50,
                      )
                    ],
                  ),
                ),
              ),
            ))
          ]),
        ));
  }

  Widget getChildrent() {
    return BlocBuilder<ChangeChildBloc, ChangeChildState>(
      builder: (context, state) {
        if (state is ChangeChildLoadingState) {
          return const ShimmerButtonDropDown();
        } else if (state is ChangeChildLoadState) {
          final translate = context.locale.toString();
          var data = state.childrenModel!.data;
          studentName = translate == "en"
              ? data![selectChild!].nameEn.toString() == ""
                  ? data[selectChild!].name.toString()
                  : data[selectChild!].nameEn.toString()
              : data![selectChild!].name.toString();
          return CustomMaterialAbsentRequest(
            title: studentName!,
            onPressed: () {
              showModalBottomSheet(
                  isScrollControlled: true,
                  backgroundColor: Colors.transparent,
                  context: context,
                  builder: (BuildContext contex) {
                    return HeaderButtomSheet(
                      initialChildSize: 0.4,
                      subtitle:
                          'Excepteur sint occaecat cupidatat non proident, sunt in culpa',
                      title: 'SELECTCHILD'.tr(),
                      child: ListView.separated(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            return InkWell(
                              onTap: () {
                                setState(() {
                                  studentId = data[index].id;
                                  selectChild = index;
                                  Navigator.of(context).pop();
                                });
                              },
                              child: Container(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 0, horizontal: 30),
                                child: Center(
                                    child: Text(
                                  translate == "en"
                                      ? data[index].nameEn.toString() == ""
                                          ? data[index].name.toString()
                                          : data[index].nameEn.toString()
                                      : data[index].name.toString(),
                                  style: ThemeConstands.texttheme.headline6
                                      ?.copyWith(
                                          color: Colorconstands.secondaryColor),
                                )),
                              ),
                            );
                          },
                          separatorBuilder: (context, index) {
                            return Divider();
                          },
                          itemCount: data.length),
                    );
                  });
            },
          );
        } else {
          return _buildLoading();
        }
      },
    );
  }

  Widget getDate() {
    return CustomMaterialAbsentRequest(
      title: startDate == null && endDate == null
          ? activeDate.toString()
          : startDate! + " - " + endDate.toString(),
      onPressed: () {
        showModalBottomSheet(
            isScrollControlled: true,
            backgroundColor: Colors.transparent,
            context: context,
            builder: (BuildContext contex) {
              return HeaderButtomSheet(
                initialChildSize: 0.9,
                subtitle: 'Excepteur sint occaecat cupidatat non',
                title: 'SELECTDATE'.tr(),
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.75,
                  child: SfDateRangePicker(
                    minDate: DateTime.now(),
                    viewSpacing: 50,
                    enableMultiView: false,
                    allowViewNavigation: false,
                    navigationDirection:
                        DateRangePickerNavigationDirection.vertical,
                    showNavigationArrow: true,
                    showActionButtons: true,
                    initialDisplayDate: DateTime.now(),
                    onSubmit: (value) {
                      setState(() {
                        String? date;
                        if (value is PickerDateRange) {
                          final DateTime rangeStartDate = value.startDate!;
                          final DateTime rangeEndDate;
                          if (value.endDate == null) {
                            rangeEndDate = value.startDate!;
                          } else {
                            rangeEndDate = value.endDate!;
                          }
                          startDate =
                              DateFormat('dd/MM/yyyy').format(rangeStartDate);
                          endDate =
                              DateFormat('dd/MM/yyyy').format(rangeEndDate);
                          Navigator.pop(context);
                        }
                      });
                    },
                    onCancel: () {
                      Navigator.of(context).pop();
                    },
                    navigationMode: DateRangePickerNavigationMode.none,
                    view: DateRangePickerView.month,
                    monthCellStyle: const DateRangePickerMonthCellStyle(
                        weekendTextStyle: TextStyle(color: Colors.red)),
                    monthViewSettings: const DateRangePickerMonthViewSettings(
                        firstDayOfWeek: 1, weekendDays: <int>[7]),
                    selectionMode: DateRangePickerSelectionMode.range,
                    headerStyle: DateRangePickerHeaderStyle(
                        backgroundColor:
                            Colorconstands.primaryColor.withOpacity(0.5),
                        textAlign: TextAlign.center,
                        textStyle: ThemeConstands.texttheme.subtitle1?.copyWith(
                            fontWeight: FontWeight.w700,
                            color: Colorconstands.black)),
                  ),
                ),
              );
            });
      },
    );
  }

  Widget getReason() {
    return BlocBuilder<RequestReasonBloc, RequestAbsentState>(
      builder: (context, state) {
        if (state is RequestAbsentLoadingState) {
          return const ShimmerButtonDropDown();
        }
        if (state is RequestReasonLoadedState) {
          final translate = context.locale.toString();
          var data = state.getAdmissionModel!.data;
          return CustomMaterialAbsentRequest(
            title: reason != null ? reason! : "SELECTREASON".tr(),
            onPressed: () {
              showModalBottomSheet(
                  isScrollControlled: true,
                  backgroundColor: Colors.transparent,
                  context: context,
                  builder: (BuildContext contex) {
                    return HeaderButtomSheet(
                      initialChildSize: 0.4,
                      title: "SELECTREASON".tr(),
                      subtitle:
                          "Excepteur sint occaecat cupidatat non proident, sunt in culpa",
                      child: ListView.separated(
                          shrinkWrap: true,
                          physics: const ScrollPhysics(),
                          itemBuilder: (context, index) {
                            return InkWell(
                              onTap: () {
                                setState(() {
                                  Navigator.of(context).pop();
                                  reason = translate == "en"
                                      ? data![index].nameEn.toString() == ""
                                          ? data[index].name.toString()
                                          : data[index].nameEn.toString()
                                      : data![index].name.toString();
                                  idreason = data[index].id.toString();
                                });
                              },
                              child: Container(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 5.0, horizontal: 30),
                                child: Center(
                                    child: Text(
                                  translate == "en"
                                      ? data![index].nameEn.toString() == ""
                                          ? data[index].name.toString()
                                          : data[index].nameEn.toString()
                                      : data![index].name.toString(),
                                  style: ThemeConstands.texttheme.headline6
                                      ?.copyWith(
                                          color: Colorconstands.secondaryColor),
                                )),
                              ),
                            );
                          },
                          separatorBuilder: (context, index) {
                            return Divider();
                          },
                          itemCount: data!.length),
                    );
                  });
            },
          );
        } else {
          return _buildLoading();
        }
      },
    );
  }

  Future successRequest(context) async {
    showDialog(
      barrierDismissible: false,
      barrierColor: Colors.black38,
      context: context,
      builder: (context) {
        return CustomAlertDialog(
          blurColor: const Color(0xFF2699FB),
        child: Center(
          child: Icon(
          Icons.check_circle,
          color: Colorconstands.white,
          size: MediaQuery.of(context).size.width / 6,
          ),
        ),
          title: "Absent Request Submitted!",
          subtitle:
              "Excepteur sint occaecat cupidatat non proident, ",
          onPressed: () {
            Navigator.of(context).pop();
            Navigator.pushAndRemoveUntil(context,MaterialPageRoute(builder: (BuildContext context) =>const OngoingAndHistoryAbsent()), ModalRoute.withName('/'));
          },
          titleButton: 'OK',
          buttonColor: Colorconstands.secondaryColor,
          titlebuttonColor: Colorconstands.white,
        );
      },
    );
  }

  Future unuccessRequest (context)async{
    showDialog(
      barrierDismissible: false,
      barrierColor: Colors.black38,
      context: context,
      builder: (context) {
        return CustomAlertDialog(
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: SvgPicture.asset(
              "assets/icons/cross_circle.svg",
              width:
                  MediaQuery.of(context).size.width /
                      6,
            ),
          ),
          title: "Request Invalid!",
          subtitle:
              "Please select reason!",
          onPressed: () {
            Navigator.of(context).pop();
          },
          titleButton: 'Try Again!',
          buttonColor: const Color(0xFFEB4D4B),
          titlebuttonColor: Colorconstands.white,
        );
      },
    );
  }

  Widget _buildLoading() => Container(
      margin: const EdgeInsets.only(right: 10.0),
      child: Center(
          child: Image.asset(
        "assets/images/gifs/loading.gif",
        height: 45,
        width: 45,
      )));
}
