// import 'package:bloc/bloc.dart';
// import 'package:equatable/equatable.dart';

// import '../../../../../../model/attendance_schedule/get_attendance_schedule_by_day_model.dart';
// import '../../../../../../model/base/base_respone_model.dart';
// import '../../../../../../model/permission_request_model/permissioo_detail.dart';
// import '../../../../../../service/apis/permission_request/post_permission_date.dart';

// part 'permission_by_time_event.dart';
// part 'permission_by_time_state.dart';

// class PermissionByTimeBloc extends Bloc<PermissionByTimeEvent, PermissionByTimeState> {
//   final GetAttendanceSchedule getAttendanceScheduleByDayApi;
//   PermissionByTimeBloc({required this.getAttendanceScheduleByDayApi}) : super(PermissionByTimeInitial()) {
//     on<GetSubjectByDayEvent>((event, emit) async{
//       emit(const GetSubjectLoadingState());
//       try{
//         var data = await getAttendanceScheduleByDayApi.getAttendaceByDayApi(date: event.date, studentId: event.studentId, classId: event.classId);
//          emit(GetSubjectByDayLoadedState(getSubjectByDayModel: data));
//       }
//       catch(e){
//         emit(const GetSubjectErrorState());
//       }
//     });
//   }
// }

// class PermissionBySchaduleBloc extends Bloc<PermissionByTimeEvent, PermissionByTimeState> {
//   final PostPermission permission;
//   BaseResponseModel<GetPermissionRequestDetailModel>? getAdmissionModel;
//   PermissionBySchaduleBloc({required this.permission}): super(PermissionByTimeInitial()) {
//     on<PostPermissBySchaduleEvent>((event, emit) async {
//       emit(const GetSubjectLoadingState());
//       try {
//         var dataResponPostPermiss = await permission.postPermissionSchaduleApi(
//           scheduleid: event.schaduleid,
//           studentId: event.studentId,
//           comment: event.comment,
//           startDate: event.startDate,
//           endDate: event.endDate,
//           reason: event.reason,
//         );
//         emit(PostPermissionLoadedState(getAdmissionModel: dataResponPostPermiss));
//       } catch (e) {
//         emit(const GetSubjectErrorState());
//       }
//     });
//   }
// }
