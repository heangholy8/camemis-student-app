import 'package:path_provider/path_provider.dart' show getTemporaryDirectory;
import 'dart:io' show File;

void deleteCacheFile() async{
  final tempDir = await getTemporaryDirectory();
  if(await tempDir.exists()){
    for(var item in tempDir.listSync()){
      if(await File.fromUri(item.uri).exists()){
        await item.absolute.delete();
      }
    }
  }
}