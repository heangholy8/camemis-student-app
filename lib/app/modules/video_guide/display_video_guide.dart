import 'package:camis_application_flutter/app/modules/auth_screens/e_school_code.dart';
import 'package:camis_application_flutter/app/modules/time_line/school_time_line/e.schooltimeline.dart';
import 'package:flutter/material.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';
import 'package:flutter/services.dart';

import '../../../storages/save_storage.dart';
import '../../../widgets/button_widget/button_widget_custom.dart';
import '../../core/themes/themes.dart';
class VideoGuideScreen extends StatefulWidget {
  final String linkVideo;
  VideoGuideScreen({Key? key, required this.linkVideo}) : super(key: key);

  @override
  State<VideoGuideScreen> createState() => _VideoGuideScreenState();
}

class _VideoGuideScreenState extends State<VideoGuideScreen> with Toast {

  late YoutubePlayerController _controller;
  bool done = false;
  SaveStoragePref _saveStorage = SaveStoragePref();
  @override
  void initState() {
    setState(() {
        _controller = YoutubePlayerController(
        initialVideoId: YoutubePlayerController.convertUrlToId(widget.linkVideo)!,
        params: const YoutubePlayerParams(
          startAt:  Duration(minutes: 0, seconds: 00),
          showControls: true,
          autoPlay: true,
          enableCaption: false,
          mute:false,
          showFullscreenButton: true,
          showVideoAnnotations:true,
          useHybridComposition:false,
          privacyEnhanced:true,
          strictRelatedVideos:true,
        )
      );
      _controller.onEnterFullscreen = () {
        SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
        ]);
      };
      _controller.onExitFullscreen = () {
        SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ]);
      };
    });
    Future.delayed(const Duration(minutes: 1),(){
      setState(() {
        done = true;
      });
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstands.black,
          body: SafeArea(
            bottom: false,
            child: Column(
              children: [
               Container(
                margin:const EdgeInsets.only(top: 50),
                height: 65,
                 child: Row(
                    children: [
                      Container(
                        margin:const EdgeInsets.only(left: 0.0,right: 20),
                        child: IconButton(
                          onPressed: () {
                            showAlertLogout(
                              onPressed: () {
                                _saveStorage.saveSeenVideoGuide(videoGuideFeature: "SeenVideoDaskBoard");
                                Navigator.of(context).pop();
                                Navigator.of(context).pop();
                              },
                              title: "WATCH_BEFOR_OFTER".tr(),
                              titlebuttoncencel: "OK".tr(),
                              titlebuttonok: "SKIP".tr(),
                              context: context);
                          },
                          icon:const Icon(
                            Icons.close_rounded,
                            size: 23,
                            color: Colorconstands.white
                          ),
                        ),
                      ),
                      Container(
                        child: Text("VIDEO_GUIDE".tr(),style: ThemeConstands.texttheme.headline5!.copyWith(color: Colorconstands.white),),
                      )
                    ],
                  ),
               ),
                Expanded(
                  child: Center(child: YoutubePlayerControllerProvider(
                    controller: _controller,
                    child: LayoutBuilder(
                      builder: (context, snapshot) {
                        return YoutubePlayerIFrame(
                          aspectRatio: 1,
                          controller: _controller,
                        );
                      }
                    ),
                  )),
                ),
                done ==false?Container():Container(
                  child: ButtonWidgetCustom(
                      heightButton: 70,
                      buttonColor: Colorconstands.primaryColor,
                      onTap: () {
                        _saveStorage.saveSeenVideoGuide(videoGuideFeature: "SeenVideoDaskBoard");
                        Navigator.of(context).pop();
                      },
                      panddinHorButton: 22,
                      panddingVerButton: 12,
                      radiusButton: 12,
                      textStyleButton: ThemeConstands.button_SemiBold_16
                          .copyWith(color: Colorconstands.neutralWhite),
                      title: 'DONE'.tr(),
                    ),
                )
              ],
            )
          ),
        );
  }
}