import 'dart:io';
import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:camis_application_flutter/widgets/button_widget/button_back_widget.dart';
import 'package:camis_application_flutter/widgets/button_widget/button_customwidget.dart';
import 'package:camis_application_flutter/widgets/take_upload_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import '../../../../storages/get_storage.dart';
import '../../../core/themes/color_app.dart';

class ProfileSetup extends StatefulWidget {
  const ProfileSetup({Key? key}) : super(key: key);

  @override
  State<ProfileSetup> createState() => _ProfileSetupState();
}

class _ProfileSetupState extends State<ProfileSetup> {
  final ImagePicker _picker = ImagePicker();
  String? profileGuidian;
  String? nameGuidian;
var _image;
XFile? file;
void _imgFromCamera() async {
    XFile? pickedFile = await _picker.pickImage(source: ImageSource.camera);
    setState(() {
       _image = File(pickedFile!.path);
      file = pickedFile;
    });   
  }

  void _imgFromGallery() async {
    XFile? pickedFile = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
       _image = File(pickedFile!.path);
      file = pickedFile;
    });
  }
  @override
  void initState() {
    super.initState();
    getAuthModel();
    print(nameGuidian);
  }
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colorconstands.primaryBackgroundColor,
      body: GestureDetector(
        onTap: () {
        },
        child: SizedBox(
          height: height,
          width: width,
          child: Stack(
            children: [
              Positioned.fill(
                child: SizedBox(
                  height: height,
                  width: width,
                  child: SvgPicture.asset(
                    "assets/clip_images/school_code_background.svg",
                    fit: BoxFit.fill,
                    width: width,
                  ),
                ),
              ),
              Container(
                width: width,
                height: height,
                padding: const EdgeInsets.only(top: 35),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 18.0,vertical: 25.0),
                      child: Iconbuttonback(),
                    ),
                    Expanded(
                      child: SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        physics: const BouncingScrollPhysics(),
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 0.0, horizontal: 10),
                          width: width,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 10.0),
                                child: Column(
                                  children: [
                                    Text(
                                      "Profile Setup",
                                      style: ThemeConstands
                                          .texttheme.headline4!
                                          .copyWith(
                                        color: Colors.black,
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 18,
                                    ),
                                    Text(
                                      "You’re all set! Take a minute to upload your profile photo.",
                                      textAlign: TextAlign.center,
                                      style: ThemeConstands
                                          .texttheme.subtitle1!
                                          .copyWith(
                                        color: Colors.black,
                                      ),
                                    ),
                                   
                                  ],
                                ),
                              ),
                              Container(
                                margin:const EdgeInsets.only(top: 12),
                                decoration:const BoxDecoration(
                                      color: Colorconstands.white,
                                      shape: BoxShape.circle
                                    ),
                                child: MaterialButton(
                                  shape:const CircleBorder(),
                                  padding:const EdgeInsets.all(0),
                                  onPressed: (){
                                    setState(() {
                                      showModalBottomSheet(
                                      backgroundColor: Colors.transparent,
                                        context: context,
                                        builder: (BuildContext bc) {
                                         return ShowPiker(
                                            onPressedCamera: (){
                                              setState(() {
                                                 _imgFromCamera();
                                              Navigator.of(context).pop();
                                              });
                                             
                                            },
                                            onPressedGalary: (){
                                              setState(() {
                                                 _imgFromGallery();
                                              Navigator.of(context).pop();
                                              });
                                            },
                                         );
                                        });
                                    });
                                  },
                                  child: Container(
                                    margin:const EdgeInsets.all(2),
                                     decoration:const BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colorconstands.secondaryColor 
                                    ),
                                    height: MediaQuery.of(context).size.width/4,width:MediaQuery.of(context).size.width/4,
                                    child: CircleAvatar(
                                      backgroundColor: Colorconstands.secondaryColor ,
                                      radius: 56,
                                      child: Padding(
                                        padding: const EdgeInsets.all(0), // Border radius
                                        child: ClipOval(child: _image==null?profileGuidian==null?SvgPicture.asset("assets/images/svg/cameraIcon.svg",width: 30,height: 30,):Image.network(profileGuidian.toString(), height: MediaQuery.of(context).size.width/4,width:MediaQuery.of(context).size.width/4,fit: BoxFit.cover,):Image.file(_image, height: MediaQuery.of(context).size.width/4,width:MediaQuery.of(context).size.width/4,fit: BoxFit.cover,)),
                                      ),
                                    )
                                  ),
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.only(top: 5,bottom: 35),
                                child: Column(
                                  children: [
                                    Container(
                                      alignment: Alignment.center,
                                      margin: const EdgeInsets.only(top: 8,left: 17.0,right: 17.0),
                                      child: Align(
                                        child: Text(nameGuidian==null?"Guardian Name":nameGuidian.toString(),style: ThemeConstands.texttheme.headline6!.copyWith(color: Colorconstands.black,fontWeight: FontWeight.bold)),
                                      )
                                    ),
                                    Container(
                                      alignment: Alignment.center,
                                      margin: const EdgeInsets.only(top: 8,left: 17.0,right: 17.0),
                                      child: Align(
                                        child: Text("Parents/Guardain",style: ThemeConstands.texttheme.subtitle2!.copyWith(color: Colorconstands.darkGray),),
                                      )
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                margin:const EdgeInsets.only(top: 0,bottom: 10),
                                child: Button_Custom_with_icon(
                                  onPressed:_image==null? (){
                                    setState(() {
                                      showModalBottomSheet(
                                      backgroundColor: Colors.transparent,
                                        context: context,
                                        builder: (BuildContext bc) {
                                        return ShowPiker(
                                            onPressedCamera: (){
                                              setState(() {
                                                _imgFromCamera();
                                              Navigator.of(context).pop();
                                              });
                                            
                                            },
                                            onPressedGalary: (){
                                              setState(() {
                                                _imgFromGallery();
                                              Navigator.of(context).pop();
                                              });
                                            },
                                        );
                                        });
                                    });
                                  }:(() {
                                    
                                  }),
                                  buttonColor: Colorconstands.primaryColor,
                                  hightButton: 50,
                                  radiusButton: 15,
                                  maginRight: 18.0,
                                  maginleft: 18.0,
                                  titleButton: profileGuidian==null? "Add profile Picture":"Continue",
                                  titlebuttonColor:Colorconstands.white,
                                  child:profileGuidian==null? Container(
                                    margin:const EdgeInsets.only(right: 5),
                                    child:const Icon(Icons.camera_alt_outlined,color: Colorconstands.white,size: 20,),
                                  ):Container(),
                                ),
                              ),
                              Container(
                                alignment: Alignment.center,
                                margin: const EdgeInsets.only(top: 0,left: 17.0,right: 17.0),
                                child:_image==null?Align(
                                  child: Text("I’ll do it later",style: ThemeConstands.texttheme.subtitle2!.copyWith(color:Colorconstands.darkGray),),
                                ):CustomTextbutton(
                                  onPressed:() {
                                    setState(() {
                                      _image=null;
                                    });
                                  },
                                  size: 13.0,
                                  title: "Remove Photo",
                                  color:Colors.red,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void getAuthModel() async {
    GetStoragePref _pref = GetStoragePref();
    var authToken = await _pref.getJsonToken;
    setState(() {
      profileGuidian  = authToken.authUser!.profileMedia!.fileThumbnail.toString();
      nameGuidian = authToken.authUser!.nameEn.toString();
    });
  }
}