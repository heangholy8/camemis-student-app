class Lang {
  final String image;
  final String title;
  final String sublang;
  const Lang({required this.image, required this.title, required this.sublang});
}

const allLangs = [
  Lang(
      image: 'assets/images/country/Flag_of_Cambodia.svg',
      title: 'ខ្មែរ',
      sublang: 'km'),
  Lang(
      image: 'assets/images/country/Flag_of_the_United_Kingdom.svg',
      title: 'English',
      sublang: 'en'),
  // Lang(image:'assets/images/country/Flag_of_Vietnam.svg', title:'Vietname',sublang: 'en'),
  // Lang(image:'assets/images/country/Flag_of_the_China.svg', title:'Chainese',sublang: 'en'),
  // Lang(image:'assets/images/country/Flag_of_Indonesia_(bordered).svg', title:'Indonesai',sublang: 'en'),
  // Lang(image:'assets/images/country/south-korean-flag.svg', title:'Korean',sublang: 'en'),
];
