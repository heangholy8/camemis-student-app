import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../core/themes/color_app.dart';
class ChangeLanguage extends StatefulWidget {
  const ChangeLanguage({ Key? key }) : super(key: key);

  @override
  State<ChangeLanguage> createState() => _ChangeLanguageState();
}

class _ChangeLanguageState extends State<ChangeLanguage> {

  bool _isRadioSelected = true;
  bool selected = true;

  @override
  void initState() {
    super.initState();   
  }

  var khmer = const Locale("en");
  void updateLanguage(Locale locale, BuildContext context) {
    context.setLocale(locale);
  }
  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: SafeArea(
        bottom: false,
        child: Container(
          child: Column(
            children: <Widget>[
              Container(margin:const EdgeInsets.only(left: 12.0),
                height: 80,
                child: Row(
                  children: [
                    IconButton(onPressed: (){Navigator.of(context).pop();}, icon:const Icon(Icons.arrow_back_ios,size: 25,color: Colorconstands.white,)),
                    Text("CHOOSE_LANGUADE".tr(),style:const TextStyle(color:Colorconstands.white,fontSize: 18.0),)
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  decoration:const BoxDecoration(
                    color: Colorconstands.white,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(18.0),topRight: Radius.circular(18.0))
                  ),
                  margin:const EdgeInsets.only(top: 20,),
                  padding:const EdgeInsets.only(top: 25.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        height: 55,
                        child: Stack(
                          children: [
                            Positioned(
                              right: 15,
                              child: Container(
                                height: 50,
                                child:Radio(
                                  value:translate=="km"?selected:!selected,
                                  groupValue: _isRadioSelected,
                                  onChanged: (bool? newValue) {
                                    setState(() {
                                      _isRadioSelected = newValue!;
                                      updateLanguage(const Locale("km"), context);
                                      Navigator.pushReplacement(
                                          context, 
                                          PageRouteBuilder(
                                              pageBuilder: (context, animation1, animation2) =>const ChangeLanguage(),
                                              transitionDuration: Duration.zero,
                                              reverseTransitionDuration: Duration.zero,
                                          ),
                                      );
      
                                    });
                                  },
                              )
                              ),
                            ),
                            Container(
                              margin:const EdgeInsets.only(left: 20),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Container(child: SvgPicture.asset("assets/images/country/Flag_of_Cambodia.svg",width: 50,)),
                                  Expanded(
                                    child: Container(margin:const EdgeInsets.only(left: 20),child:const Text("ខ្មែរ",style: TextStyle(fontSize: 18),)),
                                  ),
                                  Container(width: 50,),
                                ],
                              ),
                            ),
                            
                          ],
                        ),
                      ),
                      Container(
                        height: 55,
                        child: Stack(
                          children: [
                            Positioned(
                              right: 15,
                              child: Container(
                                height: 50,
                                child:Radio(
                                  value:translate=="en"?selected:!selected,
                                  groupValue: _isRadioSelected,
                                  onChanged: (bool? newValue) {
                                    setState(() {
                                      _isRadioSelected = newValue!;
                                      updateLanguage(const Locale("en"), context);
                                      Navigator.pushReplacement(
                                          context, 
                                          PageRouteBuilder(
                                              pageBuilder: (context, animation1, animation2) =>const ChangeLanguage(),
                                              transitionDuration: Duration.zero,
                                              reverseTransitionDuration: Duration.zero,
                                          ),
                                      );
                                    });
                                  },
                              )
                              ),
                            ),
                            Container(
                              margin:const EdgeInsets.only(left: 20),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Container(child: SvgPicture.asset("assets/images/country/Flag_of_the_United_Kingdom.svg",width: 52,)),
                                  Expanded(
                                    child: Container(margin:const EdgeInsets.only(left: 20),child:const Text("English",style: TextStyle(fontSize: 18),)),
                                  ),
                                  Container(width: 50,),
                                ],
                              ),
                            ),
                            
                          ],
                        ),
                      ),
                    ]
                  )  
                )     
              )              
            ],
          ),
        ),
      ),
    );
  }
}