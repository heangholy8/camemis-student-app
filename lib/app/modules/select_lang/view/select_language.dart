import 'dart:io';
import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:camis_application_flutter/app/modules/select_lang/local_data/local_langModel.dart';
import 'package:camis_application_flutter/app/routes/app_routes.dart';
import 'package:camis_application_flutter/widgets/button_widget/button_customwidget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../storages/save_storage.dart';
class SelectLanguageScreen extends StatefulWidget {
 const SelectLanguageScreen({Key? key,}) : super(key: key);

  @override
  _SelectLanguageScreen createState() => _SelectLanguageScreen();
}

class _SelectLanguageScreen extends State<SelectLanguageScreen> {
  TextEditingController? _textEditingController = TextEditingController();
  List<Lang> langs = allLangs;
  final SaveStoragePref _savePref = SaveStoragePref();
  String langsuccess = 'Select-lang-success';

  var khmer = const Locale("en");
  void updateLanguage(Locale locale, BuildContext context) {
    context.setLocale(locale);
  }
  
  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return WillPopScope(
      onWillPop:() => exit(0),
      child: Scaffold(
        body: Stack(
          children: [
            Column(
            children: [
              Container(
                padding:const EdgeInsets.only(top: 95),
                color: Colorconstands.primaryColor,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Align(
                      child: Text('CHOOSE_LANGUADE'.tr(),style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.white,fontWeight: FontWeight.bold),),
                    ),
                    Container(
                      
                      decoration: BoxDecoration(
                        color: Colorconstands.white,
                        borderRadius: BorderRadius.circular(15)
                      ),
                      margin:const EdgeInsets.only(bottom: 20,left: 17,right: 17,top: 35),
                      child: TextFormField(
                        controller: _textEditingController,
                        decoration:InputDecoration(
                          border: InputBorder.none,
                          fillColor: Colorconstands.primaryColor,
                          prefixIcon:const Icon(Icons.search),
                          hintText: "SEARCH".tr(),
                          hintStyle: ThemeConstands.headline5_Medium_16
                        ),
                        onChanged: searchLangs,
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  color: Colorconstands.white,
                  child: ListView.builder(
                    padding:const EdgeInsets.all(0),
                    itemCount: langs.length,
                    itemBuilder: (context, index) {
                      final lang= langs[index];
                      return Container(
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                         color:translate==lang.sublang?Colorconstands.primaryBackgroundColor:Colorconstands.white,
                         border: Border(
                            bottom: BorderSide(
                            color: Colorconstands.darkGray.withOpacity(0.1),
                            width: 1,
                            )
                          )
                        ),
                        height: 85,
                        child: ListTile(
                          leading: Container(
                            height: 50,
                            width: 50,
                            child: ClipOval(child: SvgPicture.asset(lang.image,fit: BoxFit.cover,)),
                          ),
                          title: Text(lang.title,style:translate==langs[index].sublang? ThemeConstands.headline5_SemiBold_16: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.lightBlack),),
                          trailing:translate==langs[index].sublang?const Icon(Icons.check_circle_outline_sharp,color: Colorconstands.secondaryColor,):const Icon(null),
                          onTap: (){
                            setState(() {
                              updateLanguage(Locale(langs[index].sublang), context);
                            });
                          },
                        ),
                      );
                    }
                  ),
                ),
              ),
            ],
          ),
          Positioned(bottom: 0,left: 0,right: 0,child: Container(height: 55,color: Colorconstands.white,)),
          Positioned(
              bottom: 5,
              left: 0,
              right: 0,
              child: Container(
                alignment: Alignment.bottomCenter,
                height: 95,
                color: Colorconstands.white.withOpacity(0.6),
                child: Container(
                      margin:const EdgeInsets.only(bottom: 30,top: 10,left: 25.0,right: 25.0),
                      child: Button_Custom(
                        hightButton: 45,
                        buttonColor: Colorconstands.primaryColor,
                        radiusButton: 13,
                        titleButton: 'CONTINUE'.tr(),
                        titlebuttonColor: Colorconstands.white,
                        onPressed: () async{
                          setState(()  {
                            Navigator.of(context).pushReplacementNamed(Routes.OPTIONLOGINSCREEN,);
                            _savePref.saveSelectLangSuccess(selectLangSuccess: langsuccess);
                          });
                        },
                      ),
                    ),
              ),
            ),
          ],
          
        ),
      ),
    );
  }
  void searchLangs(String query){
    final suggestions = allLangs.where((Lang){
      final langTitle = Lang.title.toLowerCase();
      final input = query.toLowerCase();
      return langTitle.contains(input);
    }).toList();
    setState(() {
      langs= suggestions;
    });
  }
}