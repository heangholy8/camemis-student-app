import '../../../routes/e.route.dart';
import '../../homework_screen/bloc/homework_bloc.dart';
import '../../homework_screen/components/flexible_header_shimmer.dart';
import '../../homework_screen/widgets/custom_appbar_widget.dart';

class FlexibleOnlineHeaderWidget extends StatelessWidget {
  const FlexibleOnlineHeaderWidget({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 18.0,
        ),
        CustomAppbarWidget(
          width: MediaQuery.of(context).size.width,
          avatar: false,
          title: "Online Learning",
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          height: 350 - 84,
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(18.0),
              topRight: Radius.circular(18.0),
            ),
          ),
          child: BlocBuilder<HomeworkBloc, HomeworkState>(
            builder: (context, state) {
              if (state is HomeWorkLoadingState) {
                return const FlexibleHeaderShimmer();
              }
              if (state is OnlinelearningLoadedState) {
                var data = state.streamingModel.data;
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 18.0,
                        vertical: 18.0,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: MediaQuery.of(context).size.width / 1.8,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  data!.name.toString(),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  style: ThemeConstands.texttheme.headline5!
                                      .copyWith(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                const SizedBox(
                                  height: 18.0,
                                ),
                                RichText(
                                  text: TextSpan(
                                    text: "Date: ",
                                    style: ThemeConstands.texttheme.subtitle1!
                                        .copyWith(
                                      color: Colorconstands.darkGray,
                                      fontWeight: FontWeight.w600,
                                    ),
                                    children: [
                                      TextSpan(
                                        text: "${data.startDate}",
                                        style: ThemeConstands
                                            .texttheme.subtitle1!
                                            .copyWith(
                                          color: Colorconstands.darkGray,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 8.0,
                                ),
                                RichText(
                                  text: TextSpan(
                                    text: "Deadline: ",
                                    style: ThemeConstands.texttheme.subtitle1!
                                        .copyWith(
                                      color: Colorconstands.darkGray,
                                      fontWeight: FontWeight.w600,
                                    ),
                                    children: [
                                      TextSpan(
                                        text: "${data.endDate}",
                                        style: ThemeConstands
                                            .texttheme.subtitle1!
                                            .copyWith(
                                          color: Colorconstands.darkGray,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(right: 18.0),
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(
                                Radius.circular(4.0),
                              ),
                            ),
                            child: MaterialButton(
                              shape: const RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(4.0),
                                ),
                              ),
                              minWidth: 30,
                              padding: const EdgeInsets.all(0.0),
                              color: Colors.red,
                              onPressed: () {
                                print("Live click");
                              },
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 8.0,
                                  vertical: 6.0,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    const Icon(
                                      Icons.circle,
                                      size: 8.0,
                                      color: Colors.white,
                                    ),
                                    const SizedBox(
                                      width: 4,
                                    ),
                                    Text(
                                      "LIVE",
                                      style: ThemeConstands.texttheme.bodyText2!
                                          .copyWith(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                       
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 18.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Description: ",
                            style: ThemeConstands.texttheme.subtitle1!.copyWith(
                              color: Colorconstands.darkGray,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          Text(
                            "${data.description}",
                            style: ThemeConstands.texttheme.headline6,
                            maxLines: 4,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                  ],
                );
              }
              return const Center(
                child: Text("Please try again!!"),
              );
            },
          ),
        ),
      ],
    );
  }
}
