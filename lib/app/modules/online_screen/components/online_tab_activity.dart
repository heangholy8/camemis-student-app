// ignore_for_file: unrelated_type_equality_checks

import '../../../routes/e.route.dart';
import '../../homework_screen/bloc/homework_bloc.dart';

class OnlineTabActivity extends StatelessWidget {
  final double width;

  const OnlineTabActivity({
    Key? key,
    required this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeworkBloc, HomeworkState>(
      builder: (context, state) {
        if (state is HomeWorkLoadingState) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        if (state is OnlinelearningLoadedState) {
          var activities =
              state.streamingModel.data!.joinVirtualLearningActivitys;
          return Container(
            // color: Colors.white,
            padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 16),
            child: activities!.isNotEmpty
                ? ListView.separated(
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return SizedBox(
                        height: 50,
                        width: width,
                        child: ListTile(
                          leading: Container(
                            height: double.infinity,
                            width: 50,
                            decoration: BoxDecoration(
                              color: Colors.black,
                              image: DecorationImage(
                                image: NetworkImage(
                                  "${activities[index].profile!.profileMedia!.fileThumbnail}",
                                ),
                              ),
                              shape: BoxShape.circle,
                            ),
                          ),
                          title: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: Text(
                              "${activities[index].profile!.name}",
                              style:
                                  ThemeConstands.texttheme.subtitle1!.copyWith(
                                color: Colorconstands.darkGray,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          subtitle: Text(
                            "03:30-05:00 PM",
                            style: ThemeConstands.texttheme.bodyText2!.copyWith(
                              color: Colorconstands.darkGray.withOpacity(.6),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          trailing: Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 16.0, vertical: 8.0),
                            decoration: BoxDecoration(
                              color: activities[index].status == 1
                                  ? const Color(0xFFF1F8DE)
                                  : activities[index].status == 2
                                      ? const Color(0xFFFFE4E4)
                                      : const Color(0xFFFFF2E4),
                              borderRadius: const BorderRadius.all(
                                Radius.circular(5.0),
                              ),
                            ),
                            child: Text(
                              activities[index].status == 1
                                  ? "Joined"
                                  : activities[index].status == 2
                                      ? "Leaved"
                                      : "Exited",
                              style:
                                  ThemeConstands.texttheme.bodyText1!.copyWith(
                                color: activities[index].status == 1
                                    ? Colors.green
                                    : activities[index].status == 2
                                        ? Colors.red
                                        : const Color(0xFFF0932B),
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                    separatorBuilder: (context, index) {
                      return Container(
                        padding: const EdgeInsets.only(
                          left: 60.0,
                          top: 14,
                          bottom: 4,
                          right: 20,
                        ),
                        child: const Divider(),
                      );
                    },
                    itemCount: activities.length,
                  )
                : Container(
                    height: MediaQuery.of(context).size.height / 3,
                    width: MediaQuery.of(context).size.width,
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        Image.asset(
                          "assets/images/gifs/empty_data.gif",
                          width: MediaQuery.of(context).size.width / 1.8,
                        ),
                        Text(
                          "No Entry Available",
                          style: ThemeConstands.texttheme.headline6!
                              .copyWith(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
          );
        }
        return Container();
      },
    );
  }
}
