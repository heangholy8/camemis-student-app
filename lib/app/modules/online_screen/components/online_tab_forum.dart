import 'package:comment_tree/comment_tree.dart';
import '../../../routes/e.route.dart';
import '../../homework_screen/bloc/homework_bloc.dart';
import '../../teaching_learning_screen/widgets/custom_textfield_sender_widget.dart';

class OnlineTabForum extends StatefulWidget {
  final double width;
  const OnlineTabForum({
    Key? key,
    required this.width,
  }) : super(key: key);

  @override
  State<OnlineTabForum> createState() => _OnlineTabForumState();
}

class _OnlineTabForumState extends State<OnlineTabForum> {
  bool isreply = false;
  int sinat = 0;
  String mentionName = "";
  String? replyId;
  String? parentId;

  final ScrollController controller = ScrollController();

  late List<Map<String, String>> _listComment;

  late TextEditingController textEditController;
  @override
  void initState() {
    _listComment = [];
    textEditController = TextEditingController();
    textEditController.addListener(() {
      print(textEditController.text.toString());
    });

    super.initState();
  }

  @override
  void dispose() {
    textEditController.dispose();
    super.dispose();
  }

  void _scrollToIndex(index) {
    controller.animateTo(MediaQuery.of(context).size.height * index,
        duration: const Duration(seconds: 2), curve: Curves.easeIn);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeworkBloc, HomeworkState>(
      builder: (context, state) {
        if (state is HomeWorkLoadingState) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        if (state is OnlinelearningLoadedState) {
          var forum = state.streamingModel.data!.forums;
          replyId = state.streamingModel.data!.id.toString();
          parentId = state.streamingModel.data!.forums![0].parent.toString();
          return Stack(
            children: [
              GestureDetector(
                onTap: () {
                  FocusScope.of(context).unfocus();
                },
                child: Container(
                  child: forum!.isNotEmpty
                      ? Padding(
                          padding: const EdgeInsets.only(
                              left: 8.0, right: 8.0, bottom: 80),
                          child: ListView.builder(
                            shrinkWrap: true,
                            // controller: controller,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: forum.length,
                            itemBuilder: (context, index) {
                              return CommentTreeWidget<Comment, Comment>(
                                Comment(
                                  avatar:
                                      '${forum[index].userProfile?.profileMedia?.fileThumbnail}',
                                  userName: '${forum[index].userProfile?.name}',
                                  content: "${forum[index].comment}",
                                ),
                                List.generate(
                                  forum[index].children!.length,
                                  (subindex) => Comment(
                                    avatar:
                                        '${forum[index].userProfile?.profileMedia?.fileThumbnail}',
                                    userName:
                                        '${forum[index].userProfile?.name}',
                                    content:
                                        "${_listComment[subindex]['content']}",
                                  ),
                                ),
                                treeThemeData: TreeThemeData(
                                    lineColor: _listComment.isNotEmpty
                                        ? const Color(0xFFDEDEDE)
                                        : Colors.grey.withOpacity(.0),
                                    lineWidth: 1.5),
                                avatarRoot: (context, data) => PreferredSize(
                                  child: InkWell(
                                    onTap: () {
                                      print("Main Profile Clicked");
                                    },
                                    child: CircleAvatar(
                                      radius: 24,
                                      backgroundImage: NetworkImage(
                                          "${forum[index].userProfile!.profileMedia!.fileThumbnail}"),
                                    ),
                                  ),
                                  preferredSize: const Size.fromRadius(18),
                                ),
                                avatarChild: (context, data) => PreferredSize(
                                  child: InkWell(
                                    onTap: () {
                                      print("Child Profile Clicked");
                                    },
                                    child: CircleAvatar(
                                      radius: 18,
                                      backgroundImage:
                                          NetworkImage('${data.avatar}'),
                                    ),
                                  ),
                                  preferredSize: const Size.fromRadius(12),
                                ),
                                // Reply Container Box
                                contentChild: (context, data) {
                                  return Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        padding: const EdgeInsets.symmetric(
                                          vertical: 14,
                                          horizontal: 14,
                                        ),
                                        decoration: BoxDecoration(
                                          color: Colors.grey[100],
                                          borderRadius:
                                              BorderRadius.circular(12),
                                        ),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              '${data.userName}',
                                              style: ThemeConstands
                                                  .texttheme.subtitle1!
                                                  .copyWith(
                                                fontWeight: FontWeight.w600,
                                                color: Colors.black,
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 8,
                                            ),
                                            Text(
                                              ' ${data.content}',
                                              style: ThemeConstands
                                                  .texttheme.bodyText1
                                                  ?.copyWith(
                                                fontWeight: FontWeight.w500,
                                                color: Colorconstands.darkGray,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      DefaultTextStyle(
                                        style: Theme.of(context)
                                            .textTheme
                                            .caption!
                                            .copyWith(
                                              color: Colors.grey[700],
                                              fontWeight: FontWeight.bold,
                                            ),
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(top: 12),
                                          child: Row(
                                            children: [
                                              const SizedBox(
                                                width: 8,
                                              ),
                                              InkWell(
                                                onTap: () {
                                                  print("Like");
                                                },
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    SvgPicture.asset(
                                                      "assets/icons/fi_like_icon.svg",
                                                      width: 12,
                                                    ),
                                                    const SizedBox(
                                                      width: 6,
                                                    ),
                                                    Text(
                                                      'Like',
                                                      style: ThemeConstands
                                                          .texttheme.bodyText2!
                                                          .copyWith(
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color: Colorconstands
                                                            .darkGray,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              const SizedBox(
                                                width: 24,
                                              ),
                                              InkWell(
                                                onTap: () {
                                                  setState(() {});
                                                  // if (isreply == true) {
                                                  //   setState(() {
                                                  //     textEditController.text =
                                                  //         '{forum[index].userProfile?.name}';
                                                  //     isreply == true;

                                                  //     print("a" +
                                                  //         isreply.toString());
                                                  //   });
                                                  // } else {
                                                  //   setState(() {
                                                  //     isreply == false;
                                                  //     print("a" +
                                                  //         isreply.toString());
                                                  //   });
                                                  // }

                                                  print("Comment");
                                                },
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    SvgPicture.asset(
                                                      "assets/icons/fi_reply_icon.svg",
                                                      width: 12,
                                                    ),
                                                    const SizedBox(
                                                      width: 6,
                                                    ),
                                                    Text(
                                                      'Reply',
                                                      style: ThemeConstands
                                                          .texttheme.bodyText2!
                                                          .copyWith(
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color: Colorconstands
                                                            .darkGray,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  );
                                },
                                contentRoot: (context, data) {
                                  return Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 8, horizontal: 8),
                                        decoration: BoxDecoration(
                                          color: Colors.grey[100],
                                          borderRadius:
                                              BorderRadius.circular(12),
                                        ),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              '${data.userName}',
                                              style: ThemeConstands
                                                  .texttheme.subtitle1!
                                                  .copyWith(
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      color: Colors.black),
                                            ),
                                            const SizedBox(
                                              height: 4,
                                            ),
                                            Text(
                                              '${data.content}',
                                              style: ThemeConstands
                                                  .texttheme.bodyText1
                                                  ?.copyWith(
                                                fontWeight: FontWeight.w500,
                                                color: Colorconstands.darkGray,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),

                                      /// LIKE AND REPLY BUTTON
                                      DefaultTextStyle(
                                        style: Theme.of(context)
                                            .textTheme
                                            .caption!
                                            .copyWith(
                                                color: Colors.grey[700],
                                                fontWeight: FontWeight.bold),
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(top: 6),
                                          child: Row(
                                            children: [
                                              const SizedBox(
                                                width: 8,
                                              ),
                                              InkWell(
                                                onTap: () {
                                                  print("Like");
                                                  // setState(() {
                                                  //   _isLike = !_isLike;
                                                  // });
                                                },
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    SvgPicture.asset(
                                                      "assets/icons/fi_like_icon.svg",
                                                      width: 12,
                                                      color:
                                                          //  !_isLike
                                                          //     ? Colorconstands.darkGray
                                                          //     :
                                                          Colorconstands
                                                              .primaryColor,
                                                    ),
                                                    const SizedBox(
                                                      width: 6,
                                                    ),
                                                    Text(
                                                      'Like',
                                                      style: ThemeConstands
                                                          .texttheme.bodyText2!
                                                          .copyWith(
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color:
                                                            //  !_isLike
                                                            //     ? Colorconstands.darkGray
                                                            //     :
                                                            Colorconstands
                                                                .primaryColor,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              const SizedBox(
                                                width: 24,
                                              ),
                                              InkWell(
                                                onTap: () {
                                                  print("objectaaaa");
                                                  setState(
                                                    () {
                                                      // replyId = forum[index]
                                                      //     .id
                                                      //     .toString();
                                                      // parentId = forum[index]
                                                      //     .parent
                                                      //     .toString();
                                                      print("adsaf" +
                                                          replyId.toString() +
                                                          parentId.toString());
                                                      // if (isreply == false) {
                                                      //   isreply = true;
                                                      //   mentionName = data
                                                      //       .userName
                                                      //       .toString();
                                                      //   print("asdf" +
                                                      //       isreply.toString());
                                                      // } else {
                                                      //   isreply = false;
                                                      //   print(isreply);
                                                      // }
                                                    },
                                                  );
                                                },
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    SvgPicture.asset(
                                                      "assets/icons/fi_reply_icon.svg",
                                                      width: 12,
                                                    ),
                                                    const SizedBox(
                                                      width: 6,
                                                    ),
                                                    Text(
                                                      'Reply',
                                                      style: ThemeConstands
                                                          .texttheme.bodyText2!
                                                          .copyWith(
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color: Colorconstands
                                                            .darkGray,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 18,
                                      ),
                                    ],
                                  );
                                },
                              );
                            },
                          ),
                        )
                      : Container(
                          height: MediaQuery.of(context).size.height / 3,
                          width: MediaQuery.of(context).size.width,
                          alignment: Alignment.center,
                          child: Column(
                            children: [
                              Image.asset(
                                "assets/images/gifs/empty_data.gif",
                                width: MediaQuery.of(context).size.width / 1.8,
                              ),
                              Text(
                                "No Entry Available",
                                style: ThemeConstands.texttheme.headline6!
                                    .copyWith(fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                ),
              ),
              AnimatedPositioned(
                duration: const Duration(milliseconds: 200),
                bottom: 0,
                left: 0,
                right: 0,
                child: AnimatedContainer(
                  duration: const Duration(milliseconds: 200),
                  alignment: Alignment.topCenter,
                  width: MediaQuery.of(context).size.width,
                  height: 100,
                  color: Colors.grey.shade100,
                  child: CustomTextFieldSenderWidget(
                    controller: textEditController,
                    onTapcancel: () {
                      setState(() {
                        isreply = false;
                        FocusScope.of(context).unfocus();
                      });
                    },
                    isReply: isreply,
                    mentionName: textEditController.text,
                    suffixIcon: InkWell(
                      onTap: () {
                        BlocProvider.of<HomeworkBloc>(context)
                          ..add(
                            AddNewReplyCommentEvent(
                              replyId: replyId.toString(),
                              parentId: parentId.toString(),
                              commentText: textEditController.text,
                            
                            ),
                          )
                          ..add(
                            GetOnlineLearningEvent(liveId: replyId.toString()),
                          );

                        print(
                          "sinatsina" +
                              replyId.toString() +
                              parentId.toString() +
                              textEditController.text.toString(),
                        );
                        FocusScope.of(context).unfocus();
                        textEditController.clear();
                      },
                      child: Container(
                        margin: const EdgeInsets.all(4),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.grey.shade100,
                        ),
                        child: const Icon(
                          Icons.send,
                          size: 20,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        }
        if (state is HomeWorkErrorState) {
          return Center(
            child: Text(state.error.toString()),
          );
        }
        return Container();
      },
    );
  }
}
