part of 'online_bloc.dart';

abstract class OnlineState extends Equatable {
  const OnlineState();

  @override
  List<Object> get props => [];
}

class OnlineInitial extends OnlineState {}

class ReplyState extends OnlineState {
  final String mentionName;
  final bool isReply;
  final int count;
  const ReplyState({required this.count, required this.mentionName, required this.isReply});
}
