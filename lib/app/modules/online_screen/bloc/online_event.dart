part of 'online_bloc.dart';

abstract class OnlineEvent extends Equatable {
  const OnlineEvent();

  @override
  List<Object> get props => [];
}

class ReplyEvent extends OnlineEvent {
  final bool isReply;
  final int count;
  const ReplyEvent({required this.count,  required this.isReply});
}
