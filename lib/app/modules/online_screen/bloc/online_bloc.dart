import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'online_event.dart';
part 'online_state.dart';

class OnlineBloc extends Bloc<OnlineEvent, OnlineState> {
  OnlineBloc() : super(OnlineInitial()) {
    on<ReplyEvent>((event, emit) {
      emit(
        ReplyState(
          mentionName: "mentionName",
          isReply: event.isReply,
          count: event.count + 1,
        ),
      );
      print("asdf${event.count}" + event.isReply.toString());
    });
  }
}
