part of 'check_attendance_bloc.dart';

// @immutable
// abstract class CheckAttendanceState {}

// class CheckAttendanceInitial extends CheckAttendanceState {}

abstract class CheckAttendanceState extends Equatable {
  const CheckAttendanceState();
  
  @override
  List<Object> get props => [];
}

class CheckAttendanceInitial extends CheckAttendanceState {}

class GetCheckStudentAttendanceListLoading extends CheckAttendanceState{}

class GetCheckStudentAttendanceListLoaded extends CheckAttendanceState{
  final CheckAttendanceModel? checkAttendanceList;
  const GetCheckStudentAttendanceListLoaded({required this.checkAttendanceList});
}
 


class GetCheckStudentAttendanceListListError extends CheckAttendanceState{}