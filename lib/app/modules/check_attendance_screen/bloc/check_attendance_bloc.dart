import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import '../../../../model/check_attendance_model/check_attendance_model.dart';
import '../../../../service/apis/check_attendance_api/get_list_student_attendance.dart';
part 'check_attendance_event.dart';
part 'check_attendance_state.dart';

class CheckAttendanceBloc
    extends Bloc<CheckStudentAttendanceEvent, CheckAttendanceState> {
  final GetCheckStudentAttendanceList getCheckStudentAttendanceList;
  List<ListAttendance>? studentList;
  CheckAttendanceBloc({required this.getCheckStudentAttendanceList})
      : super(CheckAttendanceInitial()) {
    on<GetCheckStudentAttendanceEvent>((event, emit) async {
      emit(GetCheckStudentAttendanceListLoading());
      try {
        var dataCheckAttendance = await getCheckStudentAttendanceList.getCheckAttendanceList(class_id: event.idClass,date: event.date,schedule_id: event.idSchedule);
         studentList = dataCheckAttendance.data!.listAttendance;
        emit(GetCheckStudentAttendanceListLoaded(checkAttendanceList: dataCheckAttendance));
      } catch (e) {
        // studentList = [
        //   ListAttendance(
        //     studentId: "0",
        //     schoolCode: "0",
        //     code: "0",
        //     name: "គ្មានឈ្មោះ",
        //     nameEn: "No Name",
        //     gender: 0,
        //     genderName: "គ្មានភេទ",
        //     dob: "00/00/0000",
        //     phone: 0000,
        //     email: 00000,
        //     attendanceStatus: 0,
        //     profileMedia: null,
        //   ),
        // ];
        debugPrint("Error get student attendance $e.toString()");
        emit(GetCheckStudentAttendanceListListError());
      }
    });
  }
}
