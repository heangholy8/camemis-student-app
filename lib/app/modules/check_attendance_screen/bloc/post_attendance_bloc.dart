import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '../../../../model/check_attendance_model/check_attendance_model.dart';
import '../../../../model/check_attendance_model/respon_attendace_model.dart';
import '../../../../service/apis/check_attendance_api/get_list_student_attendance.dart';
part 'post_attendance_event.dart';
part 'post_attendance_state.dart';

class PostAttendanceBloc
    extends Bloc<PostAttendanceEvent, PostAttendanceState> {
  GetCheckStudentAttendanceList attendanceApi = GetCheckStudentAttendanceList();
  bool isUpdatingLoaded = false;
  PostAttendanceBloc() : super(PostAttendanceInitial()) {
    on<PostAttDEvent>((event, emit) async {
      emit(PostAttendanceLoading());
      try {
        var data = await attendanceApi.postAttendanceApi(data: event.data);
        emit(PostAttendanceSuccess(attendance: data));
      } catch (e) {
      
        debugPrint("Error post attendance $e");
        emit(PostAttendanceError(error: e.toString()));
      }
    });
  }
}
