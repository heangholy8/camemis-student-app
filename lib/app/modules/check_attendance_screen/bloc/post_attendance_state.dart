part of 'post_attendance_bloc.dart';

abstract class PostAttendanceState extends Equatable {
  const PostAttendanceState();

  @override
  List<Object> get props => [];
}

class PostAttendanceInitial extends PostAttendanceState {}

class PostAttendanceLoading extends PostAttendanceState {}

class PostAttendanceSuccess extends PostAttendanceState {
  final ResponCheckAttendanceModel? attendance;

  const PostAttendanceSuccess({required this.attendance});
}

class PostAttendanceError extends PostAttendanceState {
  final String error;
  const PostAttendanceError({required this.error});
}
