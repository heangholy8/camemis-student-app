import 'dart:async';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../mixins/toast.dart';
import '../../../../model/check_attendance_model/check_attendance_model.dart';
import '../../../../widgets/button_widget/custom_appbar_widget.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';
import '../../attendance_screen.dart/bloc/get_schedule_bloc.dart';
import '../bloc/check_attendance_bloc.dart';
import '../bloc/post_attendance_bloc.dart';

class AttendanceEntryScreen extends StatefulWidget {
  //=========== routFromScreen = 1? HomeScreen : routFromScreen = 2? ScheduleScreen
  final String studentId;
  const AttendanceEntryScreen({Key? key,required this.studentId}) : super(key: key);

  @override
  State<AttendanceEntryScreen> createState() => _AttendanceEntryScreenState();
}

class _AttendanceEntryScreenState extends State<AttendanceEntryScreen> with Toast {
  ScrollController scrollController = ScrollController();
  int isAcitve = 0;

  var classId;
  var date;
  bool connection = true;
  bool _isLoading = false;
  bool _isLast = false;
  bool _isNotFound = false;
  StreamSubscription? sub;
  final TextEditingController searchController = TextEditingController();
  List<ListAttendance>? _filterStudent;

  @override
  void initState() {
    sub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
        if (connection == true) {
        } else {}
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  int absent = 0, present = 0, late = 0;
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    var translate = context.locale.toString();
    return Scaffold(
      body: BlocListener<PostAttendanceBloc, PostAttendanceState>(
        listener: (context, state) {
          if (state is PostAttendanceLoading) {
            setState(() {
              _isLoading = true;
            });
          }
          else if (state is PostAttendanceSuccess) {
            setState(() {
              _isLoading = false;
              showSuccessDialog(
                title: "SUCCESSFULLY".tr(),
                discription: "SUCCESS_SAVE_ATTENDANCE".tr(),
                context: context,
                callback: () {
                  setState(() {
                    BlocProvider.of<GetScheduleBloc>(context).add(GetSchedule(date: date,classId: classId,childId: widget.studentId));
                     Navigator.of(context).pop();
                     Navigator.of(context).pop();
                  });
                },
              );
            });
          }
          else{
            setState(() {
              _isLoading = false;
            });
            showErrorDialog((
              ) {
                Navigator.of(context).pop();
              }, context,title:"FAIL".tr(),description: "FAIL_SAVE_ATTENDANCE".tr());
          }
        },
        child: SafeArea(
          bottom: false,
          child: GestureDetector(
            onTap: () {
              FocusScope.of(context).unfocus();
            },
            child: BlocConsumer<CheckAttendanceBloc, CheckAttendanceState>(
              listener: (context, state) {
                if(state is GetCheckStudentAttendanceListLoaded){
                  var datacheckStu = state.checkAttendanceList!.data;
                  setState(() {
                    date = datacheckStu!.date.toString();
                    classId = datacheckStu.classId.toString();
                  });
                  _filterStudent = BlocProvider.of<CheckAttendanceBloc>(context).studentList!;
                }
              },
              builder: (context, state) {
                if (state is GetCheckStudentAttendanceListLoading) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                } else if (state is GetCheckStudentAttendanceListLoaded) {
                  final dataCheckAttendance = state.checkAttendanceList!.data;
                  return Stack(
                    children: [
                      Column(
                        children: [
                          CustomAppBarWidget(
                            width: width,
                            title: "CHECKATTENDANCE".tr(),
                          ),
                          _buildTextFormField(
                              controller: searchController,
                              attendanceModel: state.checkAttendanceList!),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 18.0, vertical: 14),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    SvgPicture.asset(ImageAssets.book_notebook),
                                    const SizedBox(
                                      width: 4,
                                    ),
                                    Text(
                                    translate=="km"?  dataCheckAttendance!.subjectName!:dataCheckAttendance!.subjectNameEn!,
                                      style:ThemeConstands.headline5_SemiBold_16,
                                    )
                                  ],
                                ),
                                Row(
                                  children: [
                                    SvgPicture.asset(ImageAssets.award),
                                    const SizedBox(
                                      width: 4,
                                    ),
                                    Text(
                                      dataCheckAttendance.className!,
                                      style:ThemeConstands.headline5_SemiBold_16,
                                    )
                                  ],
                                ),
                                Row(
                                  children: [
                                    SvgPicture.asset(ImageAssets.CALENDER_REMOVE_ICON),
                                    const SizedBox(
                                      width: 4,
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          dataCheckAttendance.date!,
                                          style: ThemeConstands.headline5_SemiBold_16,
                                        ),
                                        const CircleAvatar(
                                          radius: 2,
                                          backgroundColor: Colors.black,
                                        ),
                                        Text(
                                          "${dataCheckAttendance.startTime!} ${dataCheckAttendance.endTime!}",
                                          style: ThemeConstands
                                              .headline5_SemiBold_16,
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: NotificationListener(
                              child: SingleChildScrollView(
                                controller: scrollController,
                                child: !_isNotFound
                                    ? Column(
                                        children: [
                                          _filterStudent!.where((element) => element.attendanceStatus == 2).isNotEmpty
                                              ? Container(
                                                  width: width,
                                                  padding: const EdgeInsets.symmetric(
                                                      vertical: 8,
                                                      horizontal: 18),
                                                  decoration:const BoxDecoration(
                                                    color: Colorconstands.neutralSecondBackground,
                                                    border: Border(top: BorderSide(color: Colorconstands.darkTextsDisabled),),
                                                  ),
                                                  child: Text(
                                                    "${"ABSENT".tr()} (${_filterStudent!.where((element) => element.attendanceStatus == 2).length})",
                                                    style: ThemeConstands.headline6_SemiBold_14
                                                        .copyWith(color: Colorconstands.alertsDecline),
                                                  ),
                                                )
                                              : Container(),
                                          ListView.separated(
                                            physics: const ScrollPhysics(),
                                            shrinkWrap: true,
                                            itemCount: _filterStudent!.where((element) => element.attendanceStatus ==2).length,
                                            itemBuilder: (context, indexabsent) {
                                              return Container(
                                                padding:const EdgeInsets.symmetric(
                                                        horizontal: 18,
                                                        vertical: 8),
                                                width: width,
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Row(
                                                      children: [
                                                        CircleAvatar(
                                                            radius: 30,
                                                            backgroundImage: NetworkImage(_filterStudent!.where((element) => element.attendanceStatus ==2) .toList()[
                                                                    indexabsent]
                                                                .profileMedia!
                                                                .fileShow
                                                                .toString())),
                                                        const SizedBox(
                                                          width: 10,
                                                        ),
                                                        SizedBox(
                                                          width: width / 1.8,
                                                          child: Text(
                                                          translate=="km"?  _filterStudent! .where((element) => element.attendanceStatus == 2).toList()[indexabsent].name.toString():
                                                          _filterStudent!.where((element) => element.attendanceStatus == 2).toList()[indexabsent].nameEn==" "?"No Name":
                                                           _filterStudent!.where((element) => element.attendanceStatus == 2).toList()[indexabsent].nameEn.toString(),
                                                            maxLines: null,
                                                            style: ThemeConstands
                                                                .headline4_Regular_18,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    GestureDetector(
                                                      onTap: () {
                                                        setState(() {
                                                          if (_filterStudent!
                                                                  .where((element) =>
                                                                      element
                                                                          .attendanceStatus ==
                                                                      2)
                                                                  .toList()[
                                                                      indexabsent]
                                                                  .attendanceStatus ==
                                                              1) {
                                                            _filterStudent!
                                                                .where((element) =>
                                                                    element
                                                                        .attendanceStatus ==
                                                                    2)
                                                                .toList()[
                                                                    indexabsent]
                                                                .attendanceStatus = 2;
                                                            absent = _filterStudent!
                                                                .where((element) =>
                                                                    element
                                                                        .attendanceStatus ==
                                                                    2)
                                                                .toList()
                                                                .length;
                                                          } else if (_filterStudent!
                                                                  .where((element) =>
                                                                      element
                                                                          .attendanceStatus ==
                                                                      2)
                                                                  .toList()[
                                                                      indexabsent]
                                                                  .attendanceStatus ==
                                                              2) {
                                                            _filterStudent!
                                                                .where((element) =>
                                                                    element
                                                                        .attendanceStatus ==
                                                                    2)
                                                                .toList()[
                                                                    indexabsent]
                                                                .attendanceStatus = 3;
                                                          } else {
                                                            _filterStudent!
                                                                .where((element) =>
                                                                    element
                                                                        .attendanceStatus ==
                                                                    2)
                                                                .toList()[
                                                                    indexabsent]
                                                                .attendanceStatus = 1;
                                                          }
                                                        });
                                                      },
                                                      child: Container(
                                                        width: 60,
                                                        height: 60,
                                                        decoration:
                                                            BoxDecoration(
                                                          color: _filterStudent!
                                                                      .where((element) =>
                                                                          element
                                                                              .attendanceStatus ==
                                                                          2)
                                                                      .toList()[
                                                                          indexabsent]
                                                                      .attendanceStatus ==
                                                                  1
                                                              ? Colorconstands
                                                                  .alertsPositive
                                                              : _filterStudent!
                                                                          .where((element) =>
                                                                              element.attendanceStatus ==
                                                                              2)
                                                                          .toList()[
                                                                              indexabsent]
                                                                          .attendanceStatus ==
                                                                      2
                                                                  ? Colorconstands
                                                                      .alertsDecline
                                                                  : Colorconstands
                                                                      .alertsAwaitingText,
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(18),
                                                        ),
                                                        child: Icon(
                                                          _filterStudent!
                                                                      .where((element) =>
                                                                          element
                                                                              .attendanceStatus ==
                                                                          2)
                                                                      .toList()[
                                                                          indexabsent]
                                                                      .attendanceStatus ==
                                                                  1
                                                              ? Icons
                                                                  .check_rounded
                                                              : _filterStudent!
                                                                          .where((element) =>
                                                                              element.attendanceStatus ==
                                                                              2)
                                                                          .toList()[
                                                                              indexabsent]
                                                                          .attendanceStatus ==
                                                                      2
                                                                  ? Icons
                                                                      .close_rounded
                                                                  : Icons
                                                                      .access_time,
                                                          size: 40,
                                                          color: Colors.white,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              );
                                            },
                                            separatorBuilder:
                                                (BuildContext context,
                                                    int index) {
                                              return const Padding(
                                                padding:
                                                    EdgeInsets.only(left: 100),
                                                child: Divider(
                                                  thickness: 0.5,
                                                  height: 1,
                                                ),
                                              );
                                            },
                                          ),
                                          //------------------------------------------------

                                          //---------------- Late -----------------------
                                          _filterStudent!
                                                  .where((element) =>
                                                      element
                                                          .attendanceStatus ==
                                                      3)
                                                  .toList()
                                                  .isNotEmpty
                                              ? Container(
                                                  width: width,
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      vertical: 8,
                                                      horizontal: 18),
                                                  decoration:
                                                      const BoxDecoration(
                                                    color: Colorconstands
                                                        .neutralSecondBackground,
                                                    border: Border(
                                                      top: BorderSide(
                                                        color: Colorconstands
                                                            .darkTextsDisabled,
                                                      ),
                                                    ),
                                                  ),
                                                  child: Text(
                                                    "${"LATE".tr()} (${_filterStudent!.where((element) => element.attendanceStatus == 3).length})",
                                                    style: ThemeConstands
                                                        .headline6_SemiBold_14
                                                        .copyWith(
                                                            color: Colorconstands
                                                                .alertsAwaitingText),
                                                  ),
                                                )
                                              : Container(),
                                          _filterStudent!
                                                  .where((element) =>
                                                      element
                                                          .attendanceStatus ==
                                                      3)
                                                  .toList()
                                                  .isEmpty
                                              ? Container()
                                              : ListView.separated(
                                                  physics:
                                                      const ScrollPhysics(),
                                                  shrinkWrap: true,
                                                  itemCount: _filterStudent!
                                                      .where((element) =>
                                                          element
                                                              .attendanceStatus ==
                                                          3)
                                                      .toList()
                                                      .length,
                                                  itemBuilder:
                                                      (context, indexabsent) {
                                                    return Container(
                                                      padding: const EdgeInsets
                                                              .symmetric(
                                                          horizontal: 18,
                                                          vertical: 8),
                                                      width: width,
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Row(
                                                            children: [
                                                              CircleAvatar(
                                                                radius: 30,
                                                                backgroundImage:
                                                                    NetworkImage(
                                                                  _filterStudent!
                                                                      .where((element) =>
                                                                          element
                                                                              .attendanceStatus ==
                                                                          3)
                                                                      .toList()[
                                                                          indexabsent]
                                                                      .profileMedia!
                                                                      .fileShow
                                                                      .toString(),
                                                                ),
                                                              ),
                                                              const SizedBox(
                                                                width: 10,
                                                              ),
                                                              SizedBox(
                                                                width:
                                                                    width / 1.8,
                                                                child: Text(
                                                                translate=="km"?  _filterStudent!.where((element) => element.attendanceStatus == 3).toList()[indexabsent].name.toString():
                                                                _filterStudent!.where((element) => element.attendanceStatus == 3).toList()[indexabsent].nameEn==" "?
                                                               "No Name" :_filterStudent!.where((element) => element.attendanceStatus == 3).toList()[indexabsent].name.toString(),
                                                                  maxLines:
                                                                      null,
                                                                  style: ThemeConstands
                                                                      .headline4_Medium_18,
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          GestureDetector(
                                                            onTap: () {
                                                              setState(() {
                                                                if (_filterStudent!
                                                                        .where((element) =>
                                                                            element.attendanceStatus ==
                                                                            3)
                                                                        .toList()[
                                                                            indexabsent]
                                                                        .attendanceStatus ==
                                                                    1) {
                                                                  _filterStudent!
                                                                      .where((element) =>
                                                                          element
                                                                              .attendanceStatus ==
                                                                          3)
                                                                      .toList()[
                                                                          indexabsent]
                                                                      .attendanceStatus = 2;
                                                                } else if (_filterStudent!
                                                                        .where((element) =>
                                                                            element.attendanceStatus ==
                                                                            3)
                                                                        .toList()[
                                                                            indexabsent]
                                                                        .attendanceStatus ==
                                                                    2) {
                                                                  _filterStudent!
                                                                      .where((element) =>
                                                                          element
                                                                              .attendanceStatus ==
                                                                          3)
                                                                      .toList()[
                                                                          indexabsent]
                                                                      .attendanceStatus = 3;
                                                                } else {
                                                                  _filterStudent!
                                                                      .where((element) =>
                                                                          element
                                                                              .attendanceStatus ==
                                                                          3)
                                                                      .toList()[
                                                                          indexabsent]
                                                                      .attendanceStatus = 1;
                                                                }
                                                              });
                                                              // for (var i
                                                              //     in dataCheckAttendance
                                                              //         .listAttendance!
                                                              //         .where((element) =>
                                                              //             element
                                                              //                 .attendanceStatus ==
                                                              //             3)
                                                              //         .toList()) {
                                                              //   print(
                                                              //       "Absetnsafd ${i.name} ${i.attendanceStatus}");
                                                              // }
                                                            },
                                                            child: Container(
                                                              width: 60,
                                                              height: 60,
                                                              decoration:
                                                                  BoxDecoration(
                                                                color: _filterStudent!
                                                                            .where((element) =>
                                                                                element.attendanceStatus ==
                                                                                3)
                                                                            .toList()[
                                                                                indexabsent]
                                                                            .attendanceStatus ==
                                                                        1
                                                                    ? Colorconstands
                                                                        .alertsPositive
                                                                    : _filterStudent!.where((element) => element.attendanceStatus == 3).toList()[indexabsent].attendanceStatus ==
                                                                            2
                                                                        ? Colorconstands
                                                                            .alertsDecline
                                                                        : Colorconstands
                                                                            .alertsAwaitingText,
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            18),
                                                              ),
                                                              child: Icon(
                                                                  _filterStudent!
                                                                              .where((element) =>
                                                                                  element.attendanceStatus ==
                                                                                  3)
                                                                              .toList()[
                                                                                  indexabsent]
                                                                              .attendanceStatus ==
                                                                          1
                                                                      ? Icons
                                                                          .check_rounded
                                                                      : _filterStudent!.where((element) => element.attendanceStatus == 3).toList()[indexabsent].attendanceStatus ==
                                                                              2
                                                                          ? Icons
                                                                              .close_rounded
                                                                          : Icons
                                                                              .access_time,
                                                                  size: 40,
                                                                  color: Colors
                                                                      .white),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    );
                                                  },
                                                  separatorBuilder:
                                                      (BuildContext context,
                                                          int index) {
                                                    return const Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 100),
                                                      child: Divider(
                                                        thickness: 0.5,
                                                        height: 1,
                                                      ),
                                                    );
                                                  },
                                                ),
                                          //------------------------------------------------
                                          _filterStudent!
                                                  .where((element) =>
                                                      element
                                                          .attendanceStatus ==
                                                      1)
                                                  .toList()
                                                  .isNotEmpty
                                              ? Column(
                                                  children: [
                                                    Container(
                                                      width: width,
                                                      padding: const EdgeInsets
                                                              .symmetric(
                                                          vertical: 8,
                                                          horizontal: 18),
                                                      decoration:
                                                          const BoxDecoration(
                                                        color: Colorconstands
                                                            .neutralSecondBackground,
                                                      ),
                                                      child: Text(
                                                        "${"PRESENT".tr()} (${_filterStudent!.where((element) => element.attendanceStatus == 1).length})",
                                                        style: ThemeConstands
                                                            .headline6_SemiBold_14
                                                            .copyWith(
                                                                color: Colorconstands
                                                                    .alertsPositive),
                                                      ),
                                                    ),
                                                    ListView.separated(
                                                      padding:const EdgeInsets.only(bottom: 130,left: 0,right: 0,top: 0),
                                                      physics:const ScrollPhysics(),
                                                      shrinkWrap: true,
                                                      itemCount: _filterStudent!
                                                          .where((element) =>
                                                              element
                                                                  .attendanceStatus ==
                                                              1)
                                                          .toList()
                                                          .length,
                                                      itemBuilder:
                                                          (context, index) {
                                                        return Container(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      18,
                                                                  vertical: 8),
                                                          width: width,
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [
                                                              Row(
                                                                children: [
                                                                  CircleAvatar(
                                                                      radius:
                                                                          30,
                                                                      backgroundImage: NetworkImage(_filterStudent!
                                                                          .where((element) =>
                                                                              element.attendanceStatus ==
                                                                              1)
                                                                          .toList()[
                                                                              index]
                                                                          .profileMedia!
                                                                          .fileShow
                                                                          .toString())),
                                                                  const SizedBox(
                                                                    width: 10,
                                                                  ),
                                                                  SizedBox(
                                                                    width:
                                                                        width /
                                                                            1.8,
                                                                    child: Text(
                                                                    translate=="km"?  _filterStudent! .where((element) => element.attendanceStatus == 1).toList()[index].name.toString():
                                                                    _filterStudent! .where((element) => element.attendanceStatus == 1).toList()[index].nameEn==" "?"No Name":
                                                                    _filterStudent! .where((element) => element.attendanceStatus == 1).toList()[index].nameEn.toString(),
                                                                      maxLines:
                                                                          null,
                                                                      style: ThemeConstands
                                                                          .headline4_Medium_18,
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                              InkWell(
                                                                onTap: () {
                                                                  setState(() {
                                                                    if (_filterStudent!
                                                                            .where((element) =>
                                                                                element.attendanceStatus ==
                                                                                1)
                                                                            .toList()[index]
                                                                            .attendanceStatus ==
                                                                        1) {
                                                                      _filterStudent!
                                                                          .where((element) =>
                                                                              element.attendanceStatus ==
                                                                              1)
                                                                          .toList()[
                                                                              index]
                                                                          .attendanceStatus = 2;
                                                                    } else if (_filterStudent!
                                                                            .where((element) =>
                                                                                element.attendanceStatus ==
                                                                                1)
                                                                            .toList()[index]
                                                                            .attendanceStatus ==
                                                                        2) {
                                                                      _filterStudent!
                                                                          .where((element) =>
                                                                              element.attendanceStatus ==
                                                                              1)
                                                                          .toList()[
                                                                              index]
                                                                          .attendanceStatus = 3;
                                                                    } else {
                                                                      _filterStudent!
                                                                          .where((element) =>
                                                                              element.attendanceStatus ==
                                                                              1)
                                                                          .toList()[
                                                                              index]
                                                                          .attendanceStatus = 1;
                                                                    }
                                                                  });
                                                                  debugPrint(
                                                                      "absent: ${dataCheckAttendance.listAttendance!.where((element) => element.attendanceStatus == 2).toList().length}");
                                                                },
                                                                child:
                                                                    Container(
                                                                  width: 60,
                                                                  height: 60,
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    color: _filterStudent!.where((element) => element.attendanceStatus == 1).toList()[index].attendanceStatus ==
                                                                            1
                                                                        ? Colorconstands
                                                                            .alertsPositive
                                                                        : _filterStudent!.where((element) => element.attendanceStatus == 1).toList()[index].attendanceStatus ==
                                                                                2
                                                                            ? Colorconstands.alertsDecline
                                                                            : Colorconstands.alertsAwaitingText,
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            18),
                                                                  ),
                                                                  child: Icon(
                                                                      _filterStudent!.where((element) => element.attendanceStatus == 1).toList()[index].attendanceStatus ==
                                                                              1
                                                                          ? Icons
                                                                              .check_rounded
                                                                          : _filterStudent!.where((element) => element.attendanceStatus == 1).toList()[index].attendanceStatus ==
                                                                                  2
                                                                              ? Icons
                                                                                  .close_rounded
                                                                              : Icons
                                                                                  .access_time,
                                                                      size: 40,
                                                                      color: Colors
                                                                          .white),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        );
                                                      },
                                                      separatorBuilder:
                                                          (BuildContext context,
                                                              int index) {
                                                        return const Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 100),
                                                          child: Divider(
                                                            thickness: 0.5,
                                                            height: 1,
                                                          ),
                                                        );
                                                      },
                                                    ),
                                                    _isLast
                                                        ? SizedBox(
                                                            width: width,
                                                            height: 120,
                                                          )
                                                        : Container(),
                                                  ],
                                                )
                                              : Container(),
                                        ],
                                      )
                                    : Container(
                                        alignment: Alignment.center,
                                        width: width,
                                        height: height / 1.6,
                                        child: Text(
                                          "NODDATA".tr(),
                                          style: ThemeConstands
                                              .headline3_SemiBold_20,
                                        ),
                                      ),
                              ),
                            ),
                          ),
                          // customeBottomBoxWidget(dataCheckAttendance),
                        ],
                      ),
                      AnimatedPositioned(
                        duration: const Duration(milliseconds: 15),
                        bottom:  0,
                        left: 0,
                        right: 0,
                        child: customeBottomBoxWidget(dataCheckAttendance),
                      ),
                      _isLoading
                          ? Positioned(
                              child: Container(
                                color:
                                    Colorconstands.primaryColor.withOpacity(.6),
                                width: width,
                                height: height,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      "RECORDEDATT".tr(),
                                      style: ThemeConstands
                                          .headline2_SemiBold_24
                                          .copyWith(color: Colors.white),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      "DESRECORDEDATT".tr(),
                                      textAlign: TextAlign.center,
                                      style: ThemeConstands.headline5_Medium_16
                                          .copyWith(color: Colors.white),
                                    ),
                                    const SizedBox(
                                      height: 20,
                                    ),
                                    const CircularProgressIndicator(
                                      color: Colors.white,
                                    ),
                                  ],
                                ),
                              ),
                            )
                          : Container(),
                    ],
                  );
                } else {
                  return Center(
                    child: Text("WE_DETECT".tr(),style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.lightBlack),textAlign: TextAlign.center,),
                  );
                }
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget customeBottomBoxWidget(Data dataCheckAttendance) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 15),
      padding: const EdgeInsets.symmetric(vertical: 10),
      alignment: Alignment.center,
      decoration: const BoxDecoration(
        color: Colorconstands.neutralWhite,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(8),
          topRight: Radius.circular(8),
        ),
        boxShadow: [
          BoxShadow(
            color: Color(0x919E9E9E),
            offset: Offset(0, 0),
            blurRadius: 3.0,
            spreadRadius: 1.0,
          ),
        ],
      ),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 22),
                  child: Text(
                    "ATTENDANCE".tr(),
                    style: ThemeConstands.headline5_SemiBold_16
                        .copyWith(color: Colorconstands.neutralDarkGrey),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 22),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        width: 25,
                        padding: const EdgeInsets.symmetric(
                          vertical: 3,
                        ),
                        decoration: BoxDecoration(
                          color: Colorconstands.alertsPositive,
                          borderRadius: BorderRadius.circular(4),
                        ),
                        child: Text(
                          "${_filterStudent!.where((element) => element.attendanceStatus == 1).length}",
                          style: ThemeConstands.headline5_SemiBold_16
                              .copyWith(color: Colorconstands.neutralWhite),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      const SizedBox(
                        width: 8,
                      ),
                      customBoxWidget(
                        background: Colorconstands.alertsDecline,
                        title: dataCheckAttendance.listAttendance!
                                .where(
                                    (element) => element.attendanceStatus == 2)
                                .isEmpty
                            ? "-"
                            : "${dataCheckAttendance.listAttendance!.where((element) => element.attendanceStatus == 2).length}",
                      ),
                      const SizedBox(
                        width: 8,
                      ),
                      Container(
                        width: 25,
                        padding: const EdgeInsets.symmetric(
                          vertical: 3,
                        ),
                        decoration: BoxDecoration(
                          color: Colorconstands.alertsAwaitingText,
                          borderRadius: BorderRadius.circular(2),
                        ),
                        child: Text(
                          dataCheckAttendance.listAttendance!
                                  .where((element) =>
                                      element.attendanceStatus == 3)
                                  .isEmpty
                              ? "-"
                              : "${dataCheckAttendance.listAttendance!.where((element) => element.attendanceStatus == 3).length}",
                          style: ThemeConstands.headline6_SemiBold_14
                              .copyWith(color: Colorconstands.neutralWhite),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          AnimatedContainer(
            duration: const Duration(milliseconds: 30),
            child: GestureDetector(
                onTap: () {
                  BlocProvider.of<PostAttendanceBloc>(context).add(
                    PostAttDEvent(data: dataCheckAttendance),
                  );
                },
                child: Container(
                  height: 48,
                  margin: const EdgeInsets.all(16),
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(9),
                      ),
                      color: Colorconstands.primaryColor),
                  child: Center(
                    child: Text(
                      "SAVE".tr(),
                      style: ThemeConstands.headline4_Regular_18.copyWith(
                        fontWeight: FontWeight.w600,
                        color: Colors.white,
                      ),
                    ),
                  ),
                )),
          ),
        ],
      ),
    );
  }

  Widget customBoxWidget({String? title, Color? background}) {
    return Container(
      width: 25,
      padding: const EdgeInsets.symmetric(
        vertical: 3,
      ),
      decoration: BoxDecoration(
        color: background ?? Colorconstands.alertsDecline,
        borderRadius: BorderRadius.circular(4),
      ),
      child: Text(
        title.toString(),
        style: ThemeConstands.headline6_SemiBold_14
            .copyWith(color: Colorconstands.neutralWhite),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _buildTextFormField(
      {required TextEditingController controller,
      required CheckAttendanceModel attendanceModel}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18.0),
      child: TextFormField(
        onChanged: ((value) {
          _filter(
            query: value,
            allResult: attendanceModel.data!.listAttendance,
          );
        }),
        controller: controller,
        maxLines: 1,
        style: ThemeConstands.subtitle1_Regular_16,
        decoration: InputDecoration(
          prefixIcon: Padding(
            padding: const EdgeInsets.all(14.0),
            child: SvgPicture.asset(
              ImageAssets.SEARCH_ICON,
              width: 18,
              height: 18,
            ),
          ),
          enabledBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(12.0)),
            borderSide: BorderSide(color: Color(0xFFE5E5E5)),
          ),
          focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(12.0)),
            borderSide: BorderSide(color: Color(0xFFE5E5E5)),
          ),
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 18, vertical: 12),
          hintText: "SEACHSTUDENT".tr(),
          hintStyle: ThemeConstands.subtitle1_Regular_16.copyWith(
            color: const Color(0xFFE4E4E4),
          ),
        ),
      ),
    );
  }

  void _filter({
    required String query,
    required List<ListAttendance>? allResult,
  }) {
    List<ListAttendance>? results = [];

    if (query.isEmpty || query == " ") {
      setState(() {
        results = allResult;
      });
    } else {
      results = allResult!
          .where((element) =>
              element.name!.toLowerCase().contains(query.toLowerCase()))
          .toList();
      if (results.isNotEmpty) {
        setState(() {
          _isNotFound = false;
        });
        debugPrint("NotFound: $_isNotFound");
        // for (var element in results) {
        //   debugPrint("Student list: ${element.name}");
        // }
      } else {
        setState(() {
          _isNotFound = true;
          debugPrint("NotFound: $_isNotFound");
        });
      }
    }
    setState(() {
      _filterStudent = results;
      // for (var element in _filterStudent!) {
      //   debugPrint("Student listtttttttt: ${element.name} ${_filterStudent!.length}");
      // }
    });
  }
}
