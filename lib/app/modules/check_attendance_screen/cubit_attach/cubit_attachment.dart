// ignore_for_file: avoid_print

import 'dart:io';

import 'package:flutter_bloc/flutter_bloc.dart';

enum AttachSelecteStatus { initial, loading, success, failure }

class AttachCubit extends Cubit<AttachSelecteStatus> {
  AttachCubit() : super(AttachSelecteStatus.initial);
    List<File> filesMedia = [];
   String title = "" ;
   String description = "";

  Future<void> pickImgVdoFileCubit({required List<File> listMedia}) async {
    try {
      emit(AttachSelecteStatus.loading);
      filesMedia = listMedia;
      emit(AttachSelecteStatus.success);
    } catch (e) {
      print("Error selecting Media from Device's $e");
      emit(AttachSelecteStatus.failure);
    }
  }

  Future<void> addMoreListCubit({required List<File> listMedia}) async {
    try {
      emit(AttachSelecteStatus.loading);
      filesMedia.addAll(listMedia);
      emit(AttachSelecteStatus.success);
    } catch (e) {
      print("Error selecting Media from Device's $e");
      emit(AttachSelecteStatus.failure);
    }
  }

  void removeAllFile(){ 
    emit(AttachSelecteStatus.loading);
    try {
 
        filesMedia = [];
        title = "";
        description = "";
      emit(AttachSelecteStatus.success);      
    } catch (e) {
      emit(AttachSelecteStatus.failure);

    }
  }

  void removeFile({required int index}) {
    try {
      emit(AttachSelecteStatus.loading);
      filesMedia.removeAt(index);
      if(filesMedia.isEmpty){
        filesMedia = [];
        emit(AttachSelecteStatus.success);
      }
      emit(AttachSelecteStatus.success);
    } catch (e) {
      emit(AttachSelecteStatus.failure);
    }
   
  }
}
