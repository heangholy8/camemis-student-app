part of 'list_school_bloc.dart';

abstract class ListSchoolState extends Equatable {
  const ListSchoolState();
  
  @override
  List<Object> get props => [];
}

class ListSchoolInitial extends ListSchoolState {}

class ListSchoolLoading extends ListSchoolState{}

class ListSchoolLoaded extends ListSchoolState{
  final ListSchoolModel? listSchool;
  const ListSchoolLoaded({required this.listSchool});
}

class ListSchoolError extends ListSchoolState{}
