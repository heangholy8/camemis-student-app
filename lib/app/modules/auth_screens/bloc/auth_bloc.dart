import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:camis_application_flutter/helper/check_platform_device.dart';
import 'package:camis_application_flutter/storages/save_storage.dart';
import 'package:camis_application_flutter/service/apis/auth_api/authentication_api.dart';
import 'package:equatable/equatable.dart';
part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthApi authApi;
  final SaveStoragePref _savePref = SaveStoragePref();
  AuthBloc({required this.authApi}) : super(UnAUthenticated()) {
    on<SignInRequested>((event, emit) async {
      emit(Loading());
      try {
        var platform = checkPlatformDevice();
        await authApi.signInRequestApi(
          schoolCode: event.schoolCode,
          loginName: event.loginName,
          password: event.password,
          platform: platform, 
        ).then((value) {
          _savePref.saveJsonToken(authModel: json.encode(value));
        });
        emit(Authenticated());
        } catch (e) {
        emit(
          AuthError(e.toString()),
        );
        emit(UnAUthenticated());
      }
    });
  }
}
