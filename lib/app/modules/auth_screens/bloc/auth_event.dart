// ignore_for_file: must_be_immutable

part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object> get props => [];
}

class SignInRequested extends AuthEvent {
  final String schoolCode;
  final String loginName;
  final String password;
  String? deviceToken;
  String? platform;

  SignInRequested(
      {required this.schoolCode,
      required this.loginName,
      required this.password,
      this.deviceToken,
      this.platform});
}


class SignOutRequeted extends AuthEvent{}