import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:flutter/material.dart';

class ErrorExpanedWidget extends StatelessWidget {
  final bool isExpaned;
  Widget? child;
  ErrorExpanedWidget({Key? key, required this.isExpaned, this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
        alignment: Alignment.center,
        padding: const EdgeInsets.all(0),
        duration: const Duration(milliseconds: 200),
        margin: const EdgeInsets.symmetric(vertical: 18),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(18)),
        height: isExpaned ? 100 : 0,
        child: AnimatedOpacity(
          opacity: isExpaned ? 1 : 0,
          duration: const Duration(milliseconds: 200),
          child: child ??
              const Text(
                "No SchoolCode",
                style: ThemeConstands.subtitle1_Regular_16,
              ),
        ));
  }
}
