import 'package:camis_application_flutter/app/core/resources/asset_resource.dart';
import 'package:camis_application_flutter/app/modules/home_screen/e_home.dart';
import 'package:flutter/material.dart';

import '../../../../widgets/expansion/expand_section.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class CustomListSchoolExpand extends StatefulWidget {
  final String titleHead;
  bool isExpand;
  Widget widget;
  CustomListSchoolExpand({
    Key? key,
    required this.titleHead,
    required this.widget,
    required this.isExpand,
  }) : super(key: key);

  @override
  State<CustomListSchoolExpand> createState() => _CustomListSchoolExpandState();
}

class _CustomListSchoolExpandState extends State<CustomListSchoolExpand>
    with TickerProviderStateMixin {
  late Animation _arrowAnimation;
  late AnimationController _arrowAnimationController;
  final GlobalKey expansionTileKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    _arrowAnimationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 300));
    _arrowAnimation =
        Tween(begin: 0.0, end: 3.14).animate(_arrowAnimationController);
  }

  void _scrollToSelectedContent({required GlobalKey expansionTileKey}) {
    final keyContext = expansionTileKey.currentContext;
    if (keyContext != null) {
      Future.delayed(const Duration(milliseconds: 200)).then((value) {
        Scrollable.ensureVisible(keyContext,
            duration: const Duration(milliseconds: 200));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      key: expansionTileKey,
      decoration: BoxDecoration(
          border: Border.all(color: Colorconstands.neutralGrey, width: 1.2),
          color: Colorconstands.lightBackgroundsWhite,
          borderRadius: BorderRadius.circular(16.0)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          MaterialButton(
            highlightColor: Colors.transparent,
            focusColor: Colors.transparent,
            splashColor: Colors.transparent,
            padding: const EdgeInsets.all(0),
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16.0),
              ),
            ),
            onPressed: () {
              setState(() {
                widget.isExpand = !widget.isExpand;
                _arrowAnimationController.isCompleted
                    ? _arrowAnimationController.reverse()
                    : _arrowAnimationController.forward();
                _scrollToSelectedContent(expansionTileKey: expansionTileKey);
              });
            },
            child: Container(
              padding: const EdgeInsets.only(
                  left: 16, top: 16, bottom: 16, right: 21),
              child: Row(
                children: [
                  widget.titleHead=="សូមជ្រើសរើសឈ្មោះសាលា"?Container():SvgPicture.asset(ImageAssets.SCHOOL_ICON),
                  const SizedBox(width: 12,),
                  Expanded(
                    child: Text(
                      widget.titleHead,
                      style: ThemeConstands.headline5_Medium_16.copyWith(
                              color: Colorconstands.neutralDarkGrey)
                          
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: AnimatedBuilder(
                      animation: _arrowAnimationController,
                      builder: (context, child) => Transform.rotate(
                        angle: _arrowAnimation.value,
                        child: const Icon(
                          Icons.expand_more_outlined,
                          size: 24.0,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        
          AnimatedCrossFade(
            firstChild: Container(height: 1, color: Colorconstands.neutralGrey),
            secondChild: const SizedBox(),
            crossFadeState: widget.isExpand
                ? CrossFadeState.showFirst
                : CrossFadeState.showSecond,
            duration: const Duration(milliseconds: 400),
          ),
          ExpandedSection(expand: widget.isExpand, child: widget.widget),
        ],
      ),
    );
  }
}
