import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:camis_application_flutter/model/state_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../../core/themes/color_app.dart';

class CustomFormWidget extends StatefulWidget {
  final List<Widget> items;
  final bool error;
  final Widget? buttonCallback;
  final StateContext? state;
  const CustomFormWidget(
      {Key? key,
      this.error = false,
      required this.items,
      required this.buttonCallback,
      this.state})
      : super(key: key);

  @override
  State<CustomFormWidget> createState() => _CustomFormWidgetState();
}

class _CustomFormWidgetState extends State<CustomFormWidget> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Column(
        children: [
          Column(children: widget.items),
          SizedBox(
            height: MediaQuery.of(context).size.height * .02,
          ),
          AnimatedOpacity(
            duration: const Duration(milliseconds: 200),
            opacity: widget.state!.state != null
                ? widget.state!.state! is ErrorState
                    ? 1
                    : 0
                : 0,
            child: Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 8.0,
                horizontal: 8.0,
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  SvgPicture.asset(
                    "assets/icons/error_icon.svg",
                    width: 20,
                    height: 20,
                  ),
                  const SizedBox(
                    width: 4.0,
                  ),
                  const Text(
                    "Having some fields is not correct!",
                    style: TextStyle(
                      color: Colorconstands.errorColor,
                      fontSize: 15,
                      fontFamily: "Poppins",
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * .01,
          ),
          widget.buttonCallback!,
          SizedBox(
            height: MediaQuery.of(context).size.height * .01,
          ),
        ],
      ),
    );
  }
}

class FormItem extends StatelessWidget {
  final String label;
  final Widget child;
  const FormItem({Key? key, required this.label, required this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (label.isEmpty) {
      return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Text(label, style: const TextStyle(color: Colorconstands.darkGray)),
        child
      ]);
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(
              label,
              style: ThemeConstands.texttheme.subtitle1!
                  .copyWith(color: Colors.black, fontWeight: FontWeight.w600),
            ),
            const SizedBox(
              width: 4.0,
            ),
            Text(
              "*",
              style: ThemeConstands.texttheme.subtitle1!.copyWith(
                color: Colorconstands.primaryColor,
                fontWeight: FontWeight.w600,
              ),
            ),
          ],
        ),
        const SizedBox(height: 10.0),
        child
      ],
    );
  }
}
