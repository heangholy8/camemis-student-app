// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';

import '../../../../widgets/custom_alertdialog.dart';
import '../../../core/themes/color_app.dart';

Future<dynamic> CustomDialogWidget(
    {BuildContext? context, bool errorDialog = false}) {
  return showDialog(
    barrierDismissible: false,
    barrierColor: Colors.black38,
    context: context!,
    builder: (context) {
      return CustomAlertDialog(
        child: Icon(
          Icons.check_circle,
          color: errorDialog ? Colors.red : Colorconstands.primaryColor,
          size: MediaQuery.of(context).size.width / 6,
        ),
        title: !errorDialog ? "Code Entry Successful!" : "Code Entry Error ",
        subtitle: "Excepteur sint occaecat cupidatat non proident, ",
        onPressed: () {
          Navigator.of(context).pop();
        },
        titleButton:  !errorDialog ?'Try Again':"Continue",
        buttonColor: const Color(0xFFF07575),
        titlebuttonColor: Colorconstands.white,
      );
    },
  );
}
