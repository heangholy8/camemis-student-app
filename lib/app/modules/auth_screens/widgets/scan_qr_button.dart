// ignore_for_file: avoid_print
import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ButtonScanQRWidget extends StatelessWidget {
  final String title;
  final String? icon;
  final Color? backgroundColor;
  final Color? borderColor;
  final Color? textColor;
  final double? paddingVertical;

  final VoidCallback? onpressed;

  const ButtonScanQRWidget(
      {Key? key,
      required this.title,
      this.icon,
      this.backgroundColor,
      this.borderColor,
      this.textColor,
      this.onpressed, this.paddingVertical})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 26),
      decoration: BoxDecoration(
        color: backgroundColor ?? Colorconstands.primaryColor,
        borderRadius: BorderRadius.circular(14.0),
      ),
      child: MaterialButton(
        disabledColor: Colorconstands.gray,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(14.0),
          side: BorderSide(
              color: onpressed == null
                  ? Colorconstands.gray
                  : borderColor ?? Theme.of(context).primaryColor),
        ),
        padding:  EdgeInsets.symmetric(horizontal: 30.0, vertical:paddingVertical?? 18.0),
        onPressed: onpressed,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(
              icon.toString(),
            ),
            SizedBox(
              width: icon == null ? 0 : 24,
            ),
            Text(
              title,
              textAlign: TextAlign.center,
              style: ThemeConstands.texttheme.subtitle1!.copyWith(
                  color: textColor ?? Colors.white,
                  fontWeight: FontWeight.w600),
            ),
          ],
        ),
      ),
    );
  }
}
