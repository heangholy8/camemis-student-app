import 'dart:async';
import 'dart:io';
import 'package:camis_application_flutter/app/modules/home_screen/view/home_screen.dart';
import 'package:camis_application_flutter/widgets/button_widget/button_icon_left_with_title.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mobile_scanner/mobile_scanner.dart';
import '../../../../helper/qrcode_split.dart';
import '../../../../helper/request_camera_permission.dart';
import '../../../../mixins/toast.dart';
import '../../../../storages/get_storage.dart';
import '../../../../widgets/no_connection_alert_widget.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';
import '../../../routes/app_routes.dart';
import '../bloc/auth_bloc.dart';
import '../widgets/scan_view_widget.dart';

class QrCodeLoginScreen extends StatefulWidget {
  const QrCodeLoginScreen({Key? key}) : super(key: key);

  @override
  State<QrCodeLoginScreen> createState() => _QrCodeLoginScreenState();
}

class _QrCodeLoginScreenState extends State<QrCodeLoginScreen> with Toast {
  late MobileScannerController _mobileScannerController;
  GetStoragePref _getStoragePref = GetStoragePref();
  bool _isFlash = false;
  StreamSubscription? stsub;
  bool connection = true;
  bool loadingsumit = false;
  String? phoneNumber;
  String? comfrimProfile;

  void getPhoneStore()async{
    var getPhone = await _getStoragePref.getPhoneNumber();
     var comfrimAccount= await _getStoragePref.getComfrimProfile();
    setState(() {
      phoneNumber = getPhone;
      comfrimProfile = comfrimAccount;
    });
   }

  Future checkUserConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        setState(() {
          connection = true;
        });
      }
    } on SocketException catch (_) {
      setState(() {
        connection = false;
      });
    }
  }
  @override
  initState() {
    super.initState();
    getPhoneStore();
    checkUserConnection();
    stsub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
      });
    });
    _mobileScannerController = MobileScannerController();

    requestCameraPermission();
  }

  @override
  void dispose() {
    _mobileScannerController.dispose();
    stsub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      body: GestureDetector(
         onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Container(
          child: BlocListener<AuthBloc, AuthState>(
            listener: (context, state) {
              if(state is Loading){
                  setState(() {
                    _mobileScannerController.stop();
                    loadingsumit = true;
                  });
                  
                }
                else if(state is Authenticated){
                  setState(() async{
                    GetStoragePref _prefAuth = GetStoragePref();
                  var auth = await _prefAuth.getJsonToken;
                    _mobileScannerController;
                    HapticFeedback.vibrate();
                    loadingsumit = false;
                    setState((){
                    // if(auth.authUser!.isverify==0 ){
                    //   Navigator.pushNamedAndRemoveUntil(context, Routes.LOGINOTP,
                    //     (Route<dynamic> route) => false);
                    // }
                    // else {
                    //   if(phoneNumber!=""){
                        if (comfrimProfile != "") {
                        Navigator.pushAndRemoveUntil(
                            context,
                            PageRouteBuilder(
                              pageBuilder: (context, animation1, animation2) =>
                                  const HomeScreen(),
                              transitionDuration: Duration.zero,
                              reverseTransitionDuration: Duration.zero,
                            ),
                            (route) => false);
                            } else {
                            Navigator.pushNamedAndRemoveUntil(context,
                                Routes.ACCOUNTCONFIRM, (Route<dynamic> route) => false);
                          }
                      // }
                      // else{
                      //    Navigator.pushNamedAndRemoveUntil(context, Routes.LOGINOTP,
                      //   (Route<dynamic> route) => false);
                      // }
                    // }
                  });
                   });
                }
                else if(state is UnAUthenticated){
                  setState(() {
                    _mobileScannerController.stop();
                    loadingsumit = false;
                    showErrorDialog(() {
                      _mobileScannerController.start();
                        Navigator.of(context).pop();
                      },context);
                  });
                }
                else{
                  setState(() {
                    _mobileScannerController.stop();
                    loadingsumit = false;
                    showErrorDialog(() {
                      _mobileScannerController.start();
                        Navigator.of(context).pop();
                      },context);
                  });
                }
            },
            child: Stack(
              children: [
                Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  top: 0,
                  child: ScanViewWidget(
                    mobileScannerController: _mobileScannerController,
                    onDetect: (barcode, args) async {
                      if(connection==false){
                      }
                      else if (barcode.rawValue != null) {
                        final String code =barcode.rawValue!;
                        qrCodeSplit(code, context);
                      } else {
                        debugPrint('Failed to scan Barcode');
                      }
                    },
                  ),
                ),
                Positioned(
                  top: 0,
                  left: 0,
                  right: 0,
                  bottom: 0,
                  child: Column(
                    children: [
                      Expanded(
                        child: Container(
                          color:const Color(0x891855AB),
                        ),
                      ),
                      Container(
                        height: 230,
                        child: Row(
                          children: [
                            Expanded(
                              child: Container(
                                color:const Color(0x891855AB),
                              ),
                            ),
                            Container(width: 230,
                            decoration: BoxDecoration(
                              border: Border.all(color:const Color(0x891855AB),)
                            ),
                            child: Stack(
                              children: [
                          //============= top Rigeht radius ========================
                                Positioned(
                                  top: -57.3,
                                  left: -57.3,
                                  child: SvgPicture.asset("assets/images/1.svg",color:const Color(0x891855AB),width: 480,height: 480,),
                                ),
                                Positioned(
                                  top: -1,
                                  left: -1,
                                  child: SvgPicture.asset("assets/images/1.svg",width: 60,height: 60,),
                                ),
                                Positioned(
                                  top: -0.1,
                                  left: -0.1,
                                  child: SvgPicture.asset("assets/images/1.svg"),
                                ),
                          //============= top Rigeht radius ========================
                                Positioned(
                                  top: -53.3,
                                  right: -53.3,
                                  child: SvgPicture.asset("assets/images/2.svg",color:const Color(0x891855AB),width: 480,height: 480,),
                                ),
                                Positioned(
                                  top: -1,
                                  right: -1,
                                  child: SvgPicture.asset("assets/images/2.svg",width: 65,height: 65,),
                                ),
                                Positioned(
                                  top: -0.1,
                                  right: -0.1,
                                  child: SvgPicture.asset("assets/images/2.svg"),
                                ),
                          //============= Buttom Rigeht radius ========================
                                Positioned(
                                  bottom: -58.7,
                                  right: -58.7,
                                  child: SvgPicture.asset("assets/images/4.svg",color:const Color(0x891855AB),width: 480,height: 480,),
                                ),
                                Positioned(
                                  bottom: -1,
                                  right: -1,
                                  child: SvgPicture.asset("assets/images/4.svg",width: 60,height: 60,),
                                ),
                                Positioned(
                                  bottom: -0.1,
                                  right: -0.1,
                                  child: SvgPicture.asset("assets/images/4.svg"),
                                ),
      
                          //============= Buttom Left radius ========================
                                Positioned(
                                  bottom: -52.5,
                                  left: -52.5,
                                  child: SvgPicture.asset("assets/images/3.svg",color:const Color(0x891855AB),width: 480,height: 480,),
                                ),
                                Positioned(
                                  bottom: -1,
                                  left: -1,
                                  child: SvgPicture.asset("assets/images/3.svg",width: 67,height: 67,),
                                ),
                                Positioned(
                                  bottom: -0.1,
                                  left: -0.1,
                                  child: SvgPicture.asset("assets/images/3.svg"),
                                ),
                              ],
                            ),),
                            Expanded(
                              child: Container(
                                color:const Color(0x891855AB),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Container(
                          color:const Color(0x891855AB),
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: 60,
                  left: 0,
                  right: 0,
                  child: Column(
                    children: [
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              alignment: Alignment.center,
                              child: SvgPicture.asset(
                                ImageAssets.scan_qr_icon,
                                color: Colorconstands.neutralWhite,
                                width: 36,
                                height: 36,
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.only(
                                left: 18,
                              ),
                              child: Text(
                                "SSCANQRCODE".tr(),
                                style: ThemeConstands.headline2_SemiBold_24
                                    .copyWith(color: Colorconstands.neutralWhite),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin:
                            const EdgeInsets.only(top: 28, left: 22, right: 22),
                        child: Text("SSCANORLOGIN".tr(),
                            style: ThemeConstands.subtitle1_Regular_16
                                .copyWith(color: Colorconstands.neutralWhite),
                            textAlign: TextAlign.center),
                      ),
                      Container(
                        child: Text(
                          "CAMEMIS App",
                          style: ThemeConstands.subtitle1_Regular_16.copyWith(
                              color: Colorconstands.neutralWhite,
                              fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  top: 290,
                  child: Center(
                    child: InkWell(
                      onTap: () async {
                        await _mobileScannerController.toggleTorch();
                        setState(() {
                          _isFlash = !_isFlash;
                        });
                      },
                      child: Container(
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                          color: Colorconstands.primaryColor,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: !_isFlash
                              ? const Icon(
                                  Icons.flash_on,
                                  color: Colors.white,
                                  size: 34,
                                )
                              : const Icon(
                                  Icons.flash_off,
                                  color: Colors.white,
                                  size: 34,
                                ),
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  left: 0,
                  right: 0,
                  bottom: 0,
                  child: Container(
                    padding:
                        const EdgeInsets.only(bottom: 35, left: 27, right: 27),
                    decoration: const BoxDecoration(
                        color: Colorconstands.neutralSecondary,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(12),
                            topRight: Radius.circular(12))),
                    child: Column(
                      children: [
                        const Divider(
                          height: 1,
                          color: Colorconstands.neutralGrey,
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 20),
                          child: Text(
                            "DONTHAVEQR".tr(),
                            style: ThemeConstands.headline5_SemiBold_16
                                .copyWith(color: Colorconstands.neutralDarkGrey),
                          ),
                        ),
                        Container(
                            margin: const EdgeInsets.only(top: 15),
                            color: Colorconstands.neutralSecondary,
                            child: ButtonIconLeftWithTitle(
                              onTap: () {
                                Navigator.pushReplacementNamed(
                                    context, Routes.ENTRYCODELOGINSCREEN);
                              },
                              buttonColor: Colorconstands.neutralWhite,
                              iconImage: ImageAssets.login_code_icon,
                              weightButton: MediaQuery.of(context).size.width,
                              panddinHorButton: 28,
                              panddingVerButton: 12,
                              radiusButton: 8,
                              speaceTitleAndImage: 16,
                              textStyleButton: ThemeConstands.button_SemiBold_16
                                  .copyWith(
                                      color: Colorconstands.mainColorSecondary),
                              title: 'ENTERCODE'.tr(),
                              iconColor: Colorconstands.mainColorSecondary,
                            )),
                      ],
                    ),
                  ),
                ),
                loadingsumit == false?Container():Positioned(
                        top: 0,left: 0,right: 0,bottom: 0,
                        child: Container(
                          color: Colorconstands.subject13.withOpacity(0.6),
                          child:const Center(
                            child: CircularProgressIndicator(color: Colorconstands.neutralWhite,),
                          ),
                        ),
                      ),
                connection==true?Container():Positioned(
                  bottom: 0,left: 0,right: 0,top: 0,
                  child: Container(
                    color:const Color(0x7B9C9595),
                  ),
                ),
            AnimatedPositioned(
                  bottom:connection==true? -150:0,left: 0,right: 0,
                  duration:const Duration(milliseconds: 500), 
                  child:const NoConnectWidget(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}


