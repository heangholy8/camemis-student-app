// ignore_for_file: prefer_interpolation_to_compose_strings

import 'dart:async';
import 'dart:io';
import 'package:camis_application_flutter/app/modules/auth_screens/bloc/bloc/list_school_bloc.dart';
import 'package:camis_application_flutter/app/modules/home_screen/view/home_screen.dart';
import 'package:camis_application_flutter/model/authenication_model/list_school.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../mixins/toast.dart';
import '../../../../storages/get_storage.dart';
import '../../../../widgets/button_widget/button_icon_left_with_title.dart';
import '../../../../widgets/button_widget/button_widget_custom.dart';
import '../../../../widgets/expansion/expand_section.dart';
import '../../../../widgets/no_connection_alert_widget.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';
import '../../../routes/app_routes.dart';
import '../bloc/auth_bloc.dart';
import '../widgets/error_expaned_widget.dart';

class LoginCodeScreen extends StatefulWidget {
  const LoginCodeScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<LoginCodeScreen> createState() => _LoginCodeScreenState();
}

class _LoginCodeScreenState extends State<LoginCodeScreen> with Toast {
  final GetStoragePref _getStoragePref = GetStoragePref();
  bool checkfocusschoolcode = false;
  bool checkfocususer = false;
  bool checkfocuspass = false;
  bool selectedSchool = false;
  int? isSeleted;
  String? schoolListName;
  bool passwordshow = true;
  bool loadingsumit = false;
  bool isExpanded = false;
  String schoolName = 'CHOOSESCHOOL'.tr();
  StreamSubscription? sub;
  bool connection = true;
  String? schoolCode;
  String? phoneNumber;
  String? comfrimProfile;
  TextEditingController usercodeController = TextEditingController();
  TextEditingController passcodeController = TextEditingController();
  TextEditingController searchController = TextEditingController();
  void getPhoneStore() async {
    var getPhone = await _getStoragePref.getPhoneNumber();
    var comfrimAccount= await _getStoragePref.getComfrimProfile();
    setState(() {
      phoneNumber = getPhone;
      comfrimProfile = comfrimAccount;
    });
  }

  List<Datum>? _foundData;

  FocusNode myfocus = FocusNode();

  Future checkUserConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        setState(() {
          connection = true;
        });
      }
    } on SocketException catch (_) {
      setState(() {
        connection = false;
      });
    }
  }

  @override
  void initState() {
    getPhoneStore();
    setState(() {
      checkUserConnection();
      //=============Check internet====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          connection = (event != ConnectivityResult.none);
        });
      });
    });
    //=============Eend Check internet====================

    super.initState();
  }

  @override
  void didChangeDependencies() {
    setState(() {
      _foundData = BlocProvider.of<ListSchoolBloc>(context).preventData;
    });
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: Stack(
        children: [
          Positioned(
            top: 0,
            right: 0,
            child: Image.asset(
              "assets/images/Oval.png",
              width: MediaQuery.of(context).size.width / 1.7,
            ),
          ),
          Positioned(
            top: 150,
            left: 0,
            child: Image.asset(
              "assets/images/Oval (1).png",
            ),
          ),
          Positioned(
            bottom: 35,
            left: 0,
            child: Image.asset(
              "assets/images/Oval (3).png",
            ),
          ),
          Positioned(
            bottom: MediaQuery.of(context).size.height / 5,
            right: MediaQuery.of(context).size.width / 4,
            child: Center(
              child: Image.asset(
                "assets/images/Oval (2).png",
              ),
            ),
          ),
          BlocListener<AuthBloc, AuthState>(
            listener: (context, state) {
              if (state is Loading) {
                setState(() {
                  loadingsumit = true;
                });
              } else if (state is Authenticated) {
                setState(() async {
                  GetStoragePref _prefAuth = GetStoragePref();
                  var auth = await _prefAuth.getJsonToken;
                  HapticFeedback.vibrate();
                  loadingsumit = false;
                  setState(() {
                    // if (auth.authUser!.isverify == 0) {
                    //   Navigator.pushNamedAndRemoveUntil(context,
                    //       Routes.LOGINOTP, (Route<dynamic> route) => false);
                    // } else {
                    //   if (phoneNumber != "") {
                      if (comfrimProfile != "") {
                        Navigator.pushAndRemoveUntil(
                            context,
                            PageRouteBuilder(
                              pageBuilder: (context, animation1, animation2) =>
                                  const HomeScreen(),
                              transitionDuration: Duration.zero,
                              reverseTransitionDuration: Duration.zero,
                            ),
                            (route) => false);
                            } else {
                            Navigator.pushNamedAndRemoveUntil(context,
                                Routes.ACCOUNTCONFIRM, (Route<dynamic> route) => false);
                          }
                    
                      // } else {
                      //   Navigator.pushNamedAndRemoveUntil(context,
                      //       Routes.LOGINOTP, (Route<dynamic> route) => false);
                  //     }
                  //   }
                    }
                  );
                });
              } else if (state is UnAUthenticated) {
                setState(() {
                  loadingsumit = false;
                  showErrorDialog(() {
                    Navigator.of(context).pop();
                  }, context);
                });
              }
            },
            child: Container(
              margin: const EdgeInsets.only(top: 70),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(ImageAssets.login_code_icon,
                          color: Colorconstands.neutralWhite,
                          width: 36,
                          height: 36),
                      Container(
                        margin: const EdgeInsets.only(
                          left: 18,
                        ),
                        child: Text(
                          'ENTERCODE'.tr().toUpperCase(),
                          style: ThemeConstands.headline2_SemiBold_24
                              .copyWith(color: Colorconstands.neutralWhite),
                        ),
                      )
                    ],
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 28, left: 22, right: 22),
                    child: Text('SENTERCODELOGIN'.tr(),
                        style: ThemeConstands.subtitle1_Regular_16
                            .copyWith(color: Colorconstands.neutralWhite),
                        textAlign: TextAlign.center),
                  ),
                  Text(
                    "CAMEMIS App",
                    style: ThemeConstands.subtitle1_Regular_16.copyWith(
                        color: Colorconstands.neutralWhite,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(
                        top: 23.0,
                      ),
                      decoration: const BoxDecoration(
                        color: Colorconstands.neutralSecondary,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(12.0),
                          topRight: Radius.circular(12.0),
                        ),
                      ),
                      child: Stack(
                        children: [
                          Positioned(
                            bottom: 0,
                            top: 0,
                            left: 0,
                            right: 0,
                            child: SingleChildScrollView(
                              child: Container(
                                padding: const EdgeInsets.only(
                                  left: 27,
                                  right: 27,
                                ),
                                child: Column(
                                  children: [
                                    const SizedBox(
                                      height: 40,
                                    ),
                                    Container(
                                      alignment: Alignment.centerLeft,
                                      width: MediaQuery.of(context).size.width,
                                      height: 48,
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 18),
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                            color: Colorconstands.neutralGrey),
                                        color: Colors.white,
                                        borderRadius: const BorderRadius.all(
                                            Radius.circular(12)),
                                      ),
                                      child: Row(
                                        children: [
                                          Expanded(
                                            child: Focus(
                                              onFocusChange: (value) {
                                                setState(() {
                                                  isExpanded = value;
                                                });
                                              },
                                              child: TextFormField(
                                                scrollPhysics:
                                                    const BouncingScrollPhysics(),
                                                controller: searchController,
                                                style: ThemeConstands
                                                    .subtitle1_Regular_16,
                                                focusNode: myfocus,
                                                onChanged: (value) => _filter(
                                                    value,
                                                    BlocProvider.of<
                                                                ListSchoolBloc>(
                                                            context)
                                                        .preventData!),
                                                decoration: InputDecoration(
                                                  contentPadding:
                                                      const EdgeInsets
                                                              .symmetric(
                                                          vertical: 4.0,
                                                          horizontal: 8.0),
                                                  enabledBorder:
                                                      InputBorder.none,
                                                  focusedBorder:
                                                      InputBorder.none,
                                                  border:
                                                      const UnderlineInputBorder(),
                                                  labelText:
                                                      'CHOOSESCHOOL'.tr(),
                                                  labelStyle: ThemeConstands
                                                      .subtitle1_Regular_16
                                                      .copyWith(
                                                    color: Colorconstands
                                                        .lightTrunks,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          InkWell(
                                            onTap: () {
                                              setState(() {
                                                isExpanded = !isExpanded;
                                                if (isExpanded) {
                                                  myfocus.requestFocus();
                                                } else {
                                                  myfocus.unfocus();
                                                }
                                              });
                                            },
                                            child: SizedBox(
                                              width: 38,
                                              height: 38,
                                              child: !isExpanded
                                                  ? const Icon(
                                                      Icons.keyboard_arrow_down)
                                                  : const Icon(
                                                      Icons.keyboard_arrow_up,
                                                      color: Colorconstands
                                                          .darkGray,
                                                    ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    BlocBuilder<ListSchoolBloc,
                                        ListSchoolState>(
                                      builder: (context, state) {
                                        if (state is ListSchoolLoading) {
                                          return ErrorExpanedWidget(
                                            isExpaned: isExpanded,
                                            child:
                                                const CircularProgressIndicator(),
                                          );
                                        } else if (state is ListSchoolLoaded) {
                                          return dropDownSchoolList(
                                              translate: translate,
                                              selectedName:
                                                  searchController.text,
                                              isExpaned: isExpanded);
                                        }
                                        return ErrorExpanedWidget(
                                          isExpaned: isExpanded,
                                        );
                                      },
                                    ),
                                    Container(
                                      margin: const EdgeInsets.only(top: 16),
                                      padding: EdgeInsets.zero,
                                      decoration: BoxDecoration(
                                        color: Colorconstands.lightGohan,
                                        borderRadius: BorderRadius.circular(12),
                                        border: Border.all(
                                            color: checkfocususer == false
                                                ? Colorconstands.neutralGrey
                                                : Colorconstands.primaryColor,
                                            width: checkfocususer == false
                                                ? 1
                                                : 2),
                                      ),
                                      child: Focus(
                                        onFocusChange: (hasfocus) {
                                          setState(() {
                                            hasfocus
                                                ? checkfocususer = true
                                                : checkfocususer = false;
                                          });
                                        },
                                        child: TextFormField(
                                          controller: usercodeController,
                                          onChanged: ((value) {
                                            setState(() {
                                              usercodeController.text;
                                            });
                                          }),
                                          style: ThemeConstands
                                              .subtitle1_Regular_16
                                              .copyWith(color: Colors.black),
                                          decoration: InputDecoration(
                                            border: UnderlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        16.0)),
                                            contentPadding:
                                                const EdgeInsets.only(
                                                    top: 8,
                                                    bottom: 5,
                                                    left: 18,
                                                    right: 18),
                                            labelText: 'USERCODE'.tr(),
                                            labelStyle: ThemeConstands
                                                .subtitle1_Regular_16
                                                .copyWith(
                                                    color: Colorconstands
                                                        .lightTrunks),
                                            enabledBorder:
                                                const UnderlineInputBorder(
                                                    borderSide: BorderSide(
                                                        color: Colors
                                                            .transparent)),
                                            focusedBorder:
                                                const UnderlineInputBorder(
                                                    borderSide: BorderSide(
                                                        color: Colors
                                                            .transparent)),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: const EdgeInsets.only(
                                          top: 16, bottom: 24),
                                      padding: EdgeInsets.zero,
                                      decoration: BoxDecoration(
                                        color: Colorconstands.lightGohan,
                                        borderRadius: BorderRadius.circular(12),
                                        border: Border.all(
                                            color: checkfocuspass == false
                                                ? Colorconstands.neutralGrey
                                                : Colorconstands.primaryColor,
                                            width: checkfocuspass == false
                                                ? 1
                                                : 2),
                                      ),
                                      child: Focus(
                                        onFocusChange: (hasfocus) {
                                          setState(() {
                                            hasfocus
                                                ? checkfocuspass = true
                                                : checkfocuspass = false;
                                          });
                                        },
                                        child: TextFormField(
                                          controller: passcodeController,
                                          onChanged: ((value) {
                                            setState(() {
                                              passcodeController.text;
                                            });
                                          }),
                                          obscureText: passwordshow,
                                          style: ThemeConstands
                                              .subtitle1_Regular_16,
                                          decoration: InputDecoration(
                                            suffix: GestureDetector(
                                                onTap: () {
                                                  setState(() {
                                                    if (passwordshow) {
                                                      passwordshow = false;
                                                    } else {
                                                      passwordshow = true;
                                                    }
                                                  });
                                                },
                                                child: Text(
                                                    passwordshow == true
                                                        ? 'SHOW'.tr()
                                                        : 'HIDE'.tr(),
                                                    style: ThemeConstands
                                                        .headline6_Regular_14_20height
                                                        .copyWith(
                                                      color: Colorconstands
                                                          .lightBulma,
                                                      decoration: TextDecoration
                                                          .underline,
                                                    ))),
                                            border: UnderlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(16.0),
                                            ),
                                            contentPadding:
                                                const EdgeInsets.only(
                                                    top: 8,
                                                    bottom: 5,
                                                    left: 18,
                                                    right: 18),
                                            labelText: 'PASSWORD'.tr(),
                                            labelStyle: ThemeConstands
                                                .subtitle1_Regular_16
                                                .copyWith(
                                                    color: Colorconstands
                                                        .lightTrunks),
                                            enabledBorder:
                                                const UnderlineInputBorder(
                                                    borderSide: BorderSide(
                                                        color: Colors
                                                            .transparent)),
                                            focusedBorder:
                                                const UnderlineInputBorder(
                                                    borderSide: BorderSide(
                                                        color: Colors
                                                            .transparent)),
                                          ),
                                        ),
                                      ),
                                    ),
                                    ButtonWidgetCustom(
                                      buttonColor: Colorconstands.primaryColor,
                                      onTap: searchController.text == "" ||
                                              usercodeController.text == "" ||
                                              passcodeController.text == "" ||
                                              connection == false
                                          ? null
                                          : () {
                                              BlocProvider.of<AuthBloc>(context).add(SignInRequested(
                                                      schoolCode: schoolCode.toString(),
                                                      loginName:usercodeController.text.toString(),
                                                      password:passcodeController.text.toString()));

                                              print("school code : $schoolCode");
                                              print("login name code : ${usercodeController.text}");
                                              print("pass code : ${passcodeController.text}");
                                            },
                                      panddinHorButton: 22,
                                      panddingVerButton: 12,
                                      radiusButton: 8,
                                      textStyleButton: ThemeConstands
                                          .button_SemiBold_16
                                          .copyWith(
                                              color:
                                                  Colorconstands.neutralWhite),
                                      title: 'CONTINUE'.tr(),
                                    ),
                                    Container(
                                      height: 160,
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.height - 160,
            left: 0,
            right: 0,
            child: Container(
              height: 160,
              color: Colorconstands.neutralSecondary,
              padding: const EdgeInsets.symmetric(horizontal: 28),
              child: Column(
                children: [
                  const Divider(
                    height: 1,
                    color: Colorconstands.neutralGrey,
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20),
                    child: Text(
                      'DONTHAVECODE'.tr(),
                      style: ThemeConstands.headline5_SemiBold_16
                          .copyWith(color: Colorconstands.neutralDarkGrey),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 15),
                    color: Colorconstands.neutralSecondary,
                    child: ButtonIconLeftWithTitle(
                      onTap: () {
                        Navigator.pushReplacementNamed(
                            context, Routes.QRCODESCREEN);
                      },
                      buttonColor: Colorconstands.neutralWhite,
                      iconImage: ImageAssets.scan_qr_icon,
                      weightButton: MediaQuery.of(context).size.width,
                      panddinHorButton: 28,
                      panddingVerButton: 12,
                      radiusButton: 8,
                      speaceTitleAndImage: 16,
                      textStyleButton: ThemeConstands.button_SemiBold_16
                          .copyWith(color: Colorconstands.mainColorSecondary),
                      title: 'SCANQR'.tr(),
                      iconColor: Colorconstands.mainColorSecondary,
                    ),
                  ),
                ],
              ),
            ),
          ),
          loadingsumit == false
              ? Container()
              : Positioned(
                  top: 0,
                  left: 0,
                  right: 0,
                  bottom: 0,
                  child: Container(
                    color: Colorconstands.subject13.withOpacity(0.6),
                    child: const Center(
                      child: CircularProgressIndicator(
                        color: Colorconstands.neutralWhite,
                      ),
                    ),
                  )),
          connection == true
              ? Container()
              : Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  top: 0,
                  child: Container(
                    color: const Color(0x7B9C9595),
                  ),
                ),
          AnimatedPositioned(
            bottom: connection == true ? -150 : 0,
            left: 0,
            right: 0,
            duration: const Duration(milliseconds: 500),
            child: const NoConnectWidget(),
          ),
        ],
      ),
    );
  }

  Widget dropDownSchoolList(
      {required String translate,
      required String selectedName,
      bool isExpaned = false}) {
    return ExpandedSection(
        expand: isExpanded,
        child: ListView.separated(
          itemCount: _foundData!.length,
          separatorBuilder: ((context, index) {
            return const Divider(
              height: 1,
            );
          }),
          physics: const ScrollPhysics(),
          padding: const EdgeInsets.all(0),
          shrinkWrap: true,
          itemBuilder: ((context, index) {
            return Container(
              height: 44,
              width: MediaQuery.of(context).size.width,
              decoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(19)),
              child: MaterialButton(
                height: 40,
                elevation: 0.0,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topLeft: index == 0
                        ? const Radius.circular(8.0)
                        : const Radius.circular(0),
                    topRight: index == 0
                        ? const Radius.circular(8.0)
                        : const Radius.circular(0),
                    bottomLeft: index == _foundData!.length - 1
                        ? const Radius.circular(8.0)
                        : const Radius.circular(0),
                    bottomRight: index == _foundData!.length - 1
                        ? const Radius.circular(8.0)
                        : const Radius.circular(0),
                  ),
                ),
                color: isSeleted == index
                    ? Colorconstands.primaryColor
                    : Colors.white,
                padding:
                    const EdgeInsets.symmetric(horizontal: 12, vertical: 0),
                onPressed: () {
                  myfocus.unfocus();
                  setState(() {
                    isSeleted = index;
                    schoolCode = _foundData?[isSeleted!].id.toString();
                    //  BlocProvider.of<ListSchoolBloc>(context).schoolId = _foundData?[isSeleted!].id;
                    if (translate == "km") {
                      schoolListName = _foundData?[isSeleted!].schoolName;
                    } else if (translate == "en") {
                      schoolListName = _foundData?[isSeleted!].schoolNameEn;
                    } else {
                      schoolListName = "No School";
                    }
                    searchController.text = schoolListName!;
                    isExpanded = false;
                  });
                },
                child: Text(
                  translate == "km"
                      ? _foundData![index].schoolName.toString()
                      : _foundData![index].schoolNameEn.toString(),
                  style: ThemeConstands.subtitle1_Regular_16.copyWith(
                      color: isSeleted == index
                          ? Colorconstands.white
                          : Colors.black),
                ),
              ),
            );
          }),
        )

        //  ListView.builder(
        //     padding: const EdgeInsets.all(0),
        //     physics: const NeverScrollableScrollPhysics(),
        //     shrinkWrap: true,
        //     itemCount: _foundData?.length,
        //     scrollDirection: Axis.vertical,
        //     itemBuilder: (context, index) {
        //       if (_foundData!.isEmpty) {
        //         return const Text("No School right now");
        //       } else {
        //         return
        //         Container(
        //           height: 44,
        //           width: MediaQuery.of(context).size.width,
        //           decoration:
        //               BoxDecoration(borderRadius: BorderRadius.circular(19)),
        //           child: MaterialButton(
        //             height: 40,
        //             elevation: 0.0,
        //             color: isSeleted == index
        //                 ? Colorconstands.primaryColor
        //                 : Colors.white,
        //             padding:
        //                 const EdgeInsets.symmetric(horizontal: 12, vertical: 0),
        //             onPressed: () {
        //               myfocus.unfocus();
        //               setState(() {
        //                 isSeleted = index;
        //                 BlocProvider.of<ListSchoolBloc>(context).schoolId =
        //                     _foundData?[isSeleted!].id;

        //                 if (translate == "km") {
        //                   schoolListName = _foundData?[isSeleted!].schoolName;
        //                 } else if (translate == "en") {
        //                   schoolListName = _foundData?[isSeleted!].schoolNameEn;
        //                 } else {
        //                   schoolListName = "No School";
        //                 }
        //                 searchController.text = schoolListName!;

        //                 print("sinatSchool" + searchController.text.toString());
        //                 isExpanded = false;
        //               });
        //             },
        //             child: Text(
        //               translate == "km"
        //                   ? _foundData![index].schoolName.toString()
        //                   : _foundData![index].schoolNameEn.toString(),
        //               style: ThemeConstands.subtitle1_Regular_16.copyWith(
        //                   color: isSeleted == index
        //                       ? Colorconstands.white
        //                       : Colors.black),
        //             ),
        //           ),
        //         );

        //       }
        //     }),

        );
  }

  void _filter(String query, List<Datum> allResult) {
    List<Datum> results = [];
    if (query.isEmpty) {
      results = allResult;
    } else {
      results = allResult
          .where((element) =>
              element.schoolName!.toLowerCase().contains(query.toLowerCase()) ||
              element.schoolNameEn!.toLowerCase().contains(query.toLowerCase()))
          .toList();

      for (var testing in results) {
        debugPrint("searching ..." + testing.schoolNameEn);
      }
    }
    setState(() {
      _foundData = results;
    });
  }
}
