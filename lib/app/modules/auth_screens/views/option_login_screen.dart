// ignore_for_file: avoid_print

import 'dart:async';
import 'dart:io';
import 'package:camis_application_flutter/app/core/resources/asset_resource.dart';
import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:camis_application_flutter/app/modules/auth_screens/views/code_entry_screen.dart';
import 'package:camis_application_flutter/app/modules/auth_screens/views/scan_qr_screen.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../../../widgets/button_widget/button_icon_left_with_title.dart';
import '../../../core/themes/color_app.dart';
import '../bloc/bloc/list_school_bloc.dart';

class OptionLoginScreen extends StatefulWidget {
  OptionLoginScreen({
    Key? key,
  }) : super(key: key);
  @override
  State<OptionLoginScreen> createState() => _OptionLoginScreenState();
}

class _OptionLoginScreenState extends State<OptionLoginScreen> {
  bool isoffline = true;
  // Future checkUserConnection() async {
  //   try {
  //     final result = await InternetAddress.lookup('google.com');
  //     if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
  //       setState(() {
  //         isoffline = true;
  //       });
  //     }
  //   } on SocketException catch (_) {
  //     setState(() {
  //       isoffline = false;
  //     });
  //   }
  // }
  @override
  void initState() {
    BlocProvider.of<ListSchoolBloc>(context).add(GetListSchoolEvent());
    // setState(() {
    //   //checkUserConnection();

    // });
    super.initState();
  }

  // @override
  // dispose() {
  //   super.dispose();
  //   //cancel internent connection subscription after you are done
  // }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: WillPopScope(
        onWillPop: () {
          return exit(0);
        },
        child: Stack(
          children: [
            Positioned(
              top: 0,
              right: 0,
              child: Image.asset(
                "assets/images/Oval.png",
                width: MediaQuery.of(context).size.width / 1.7,
              ),
            ),
            Positioned(
              top: 150,
              left: 0,
              child: Image.asset(
                "assets/images/Oval (1).png",
              ),
            ),
            Positioned(
              bottom: 35,
              left: 0,
              child: Image.asset(
                "assets/images/Oval (3).png",
              ),
            ),
            Positioned(
              bottom: MediaQuery.of(context).size.height / 5,
              right: MediaQuery.of(context).size.width / 4,
              child: Center(
                  child: Image.asset(
                "assets/images/Oval (2).png",
              )),
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 75),
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    child: Column(
                      children: [
                        Text(
                          "Powered by:",
                          style: ThemeConstands.caption_Regular_12
                              .copyWith(color: Colors.transparent),
                        ),
                        Text(
                          "CAMIS Solutions",
                          style: ThemeConstands.caption_Regular_12
                              .copyWith(color: Colors.transparent),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 5, left: 15),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              margin: const EdgeInsets.only(top: 0, right: 12),
                              child: SvgPicture.asset(
                                ImageAssets.logoCamis,
                                width: 45,
                                height: 45,
                              ),
                            ),
                            Container(
                                child: Text(
                              "CAMEMIS",
                              style: ThemeConstands.headline1_SemiBold_32
                                  .copyWith(
                                      color: Colorconstands.neutralWhite,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 40),
                              textAlign: TextAlign.center,
                            )),
                          ],
                        ),
                        Container(
                          margin: const EdgeInsets.only(left: 18),
                          child: Text(
                            "Recommended for Student",
                            style: ThemeConstands.caption_Regular_12.copyWith(
                              color: Colorconstands.neutralWhite,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      children: [
                        Container(
                            child: ButtonIconLeftWithTitle(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        const QrCodeLoginScreen()));
                          },
                          buttonColor: Colorconstands.mainColorSecondary,
                          iconImage: ImageAssets.scan_qr_icon,
                          weightButton: MediaQuery.of(context).size.width,
                          panddinHorButton: 28,
                          panddingVerButton: 12,
                          radiusButton: 8,
                          speaceTitleAndImage: 16,
                          textStyleButton: ThemeConstands.button_SemiBold_16
                              .copyWith(color: Colorconstands.neutralWhite),
                          title: 'SCANQR'.tr(),
                          iconColor: Colorconstands.neutralWhite,
                        )),
                        const SizedBox(
                          height: 24,
                        ),
                        Container(
                            child: ButtonIconLeftWithTitle(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        const LoginCodeScreen()));
                          },
                          buttonColor: Colorconstands.neutralWhite,
                          iconImage: ImageAssets.login_code_icon,
                          weightButton: MediaQuery.of(context).size.width,
                          panddinHorButton: 28,
                          panddingVerButton: 12,
                          radiusButton: 8,
                          speaceTitleAndImage: 16,
                          textStyleButton: ThemeConstands.button_SemiBold_16
                              .copyWith(color: Colorconstands.primaryColor),
                          title: 'ENTERCODE'.tr(),
                          iconColor: Colorconstands.primaryColor,
                        ))
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
