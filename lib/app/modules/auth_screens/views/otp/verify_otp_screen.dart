import 'dart:async';
import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:camis_application_flutter/storages/save_storage.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pinput/pinput.dart';
import '../../../account_confirm_screen/view/account_confirm_screen.dart';

class VerifyOTPScreen extends StatefulWidget {
  final phone;
  VerifyOTPScreen({Key? key, required this.phone}) : super(key: key);
  @override
  State<VerifyOTPScreen> createState() => _VerifyOTPScreenState();
}
class _VerifyOTPScreenState extends State<VerifyOTPScreen> {
  SaveStoragePref _saveStoragePref = SaveStoragePref();
  final defaulPinTheme = PinTheme(
      constraints: const BoxConstraints(minWidth: 46, minHeight: 56),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8.0),
          color: Colorconstands.neutralWhite),
      margin: const EdgeInsets.all(2),
      textStyle: ThemeConstands.headline4_Medium_18
          .copyWith(fontWeight: FontWeight.bold),
      padding: const EdgeInsets.only(left: 22, right: 22, top: 16, bottom: 12));

  final _formKey = GlobalKey<FormState>();
  final FirebaseAuth auth = FirebaseAuth.instance;
  TextEditingController pinputController = TextEditingController();
  int? resentToken;
  late String _verificationId;
  var phonauth = PhoneAuthCredential;
  late int secondsRemaining;
  late Timer timer;
  @override
  void initState() {
    phoneSignIn();
    super.initState();
    secondsRemaining = 30;
    timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (secondsRemaining != 0) {
        setState(() {
          secondsRemaining--;
        });
      } else {
        setState(() {
          timer.cancel();
        });
      }
    });
  }

  @override
  void dispose() {
    pinputController.dispose();
    super.dispose();
  }

  Future<void> phoneSignIn() async {
      final phone = '+855 ${widget.phone.substring(1)}';
      auth.verifyPhoneNumber(
        phoneNumber: phone,
        timeout: const Duration(seconds: 30),
        verificationCompleted: (phoneAuthCredential) async {
          await auth.signInWithCredential(phoneAuthCredential).then((value) {
            if (value.user != null && timer.isActive) {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const AccountConfirmScreen(),
                  ),
                  (route) => false);
            } else {
              showDialog(
                context: context,
                builder: (context) => const AlertDialog(
                  title: Text("បរាជ័យ"),
                  content: Text("សូមបញ្ចូលលេខកូដម្តងទៀត"),
                ),
              );
            }
          });
        },
        verificationFailed: (error) {
          const snackBar = SnackBar(
            content: Text('Verification Failed'),
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16.0),
          );
          ScaffoldMessenger.of(context).showSnackBar(snackBar);

          print("Get Error   ${error.message.toString()}");
        },
        codeSent: (verificationId, forceResendingToken) {
          setState(() {
            _verificationId = verificationId;
            resentToken = forceResendingToken;
          });
        },
        forceResendingToken: resentToken,
        codeAutoRetrievalTimeout: (verificationId) {
          setState(() {
            _verificationId = verificationId;
          });
        },
      );
  
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(
            top: 0,
            right: 0,
            child: Image.asset(
              "assets/images/Oval.png",
              width: MediaQuery.of(context).size.width / 1.7,
            ),
          ),
          Positioned(
            top: 35,
            left: 0,
            right: 0,
            bottom: 0,
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        margin:const EdgeInsets.only(left:28),
                        alignment: Alignment.centerLeft,
                        height: 45,
                        width: 45,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: const Icon(
                            Icons.arrow_back_ios_new_rounded,
                            size: 21,
                            color: Colorconstands.neutralWhite,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: Container(
                    margin:const EdgeInsets.only(top: 25),
                    padding: const EdgeInsets.only(top: 48, left: 27, right: 27),
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(8.0),
                            topRight: Radius.circular(8.0)),
                        color: Colorconstands.lightBeerusBeerus),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'ENTERPINCODE'.tr(),
                          style: ThemeConstands.headline2_SemiBold_24.copyWith(
                            color: Colorconstands.neutralDarkGrey,
                            overflow: TextOverflow.fade,
                          ),
                        ),
                        const SizedBox(
                          height: 27,
                        ),
                        Text(
                          'INPUTPIN'.tr(),
                          textAlign: TextAlign.center,
                          style: ThemeConstands.headline4_Regular_18.copyWith(
                            color: Colorconstands.neutralDarkGrey,
                            overflow: TextOverflow.fade,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 24, bottom: 16),
                          child: Form(
                            key: _formKey,
                            child: Pinput(
                              length: 6,
                              forceErrorState: true,
                              controller: pinputController,
                              keyboardType: TextInputType.number,
                                inputFormatters: [
                                  FilteringTextInputFormatter.digitsOnly,
                                ],
                              pinputAutovalidateMode: PinputAutovalidateMode.onSubmit,
                              defaultPinTheme: defaulPinTheme,
                              androidSmsAutofillMethod:
                                  AndroidSmsAutofillMethod.smsRetrieverApi,
                              autofocus: true,
                              smsCodeMatcher: PinputConstants.defaultSmsCodeMatcher,
                              listenForMultipleSmsOnAndroid: false,
                              useNativeKeyboard: true,
                              onChanged: (value) {
                                //0972257409
                
                                print("---> value ---> $value");
                              },
                              onCompleted: (value) async {
                                try {
                                  print(" verificationid ==> ${_verificationId}");
                                  final crediential = PhoneAuthProvider.credential(
                                      verificationId: _verificationId,
                                      smsCode: value);
                                  await auth
                                      .signInWithCredential(crediential)
                                      .catchError((value) async {
                                    showDialog(
                                      context: context,
                                      builder: (context) => AlertDialog(
                                        title: Text(
                                          "FAIL".tr(),
                                          style: ThemeConstands.headline3_SemiBold_20
                                              .copyWith(color: Colors.red),
                                        ),
                                        content: Text(
                                          "ENTER_CODE_AGAIN".tr(),
                                          style: ThemeConstands.caption_Regular_12,
                                        ),
                                      ),
                                    );
                                    pinputController.clear();
                                  }).then((value) {
                                    if (value.user != null) {
                                      _saveStoragePref.savePhoneNumber(phoneNumber: widget.phone);
                                      Navigator.pushAndRemoveUntil(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => const AccountConfirmScreen(),
                                          ),
                                          (route) => false);
                                    }
                                  });
                                } catch (e) {
                                  FocusScope.of(context).unfocus();
                                }
                              },
                            ),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'NOTCODE'.tr(),
                              style: ThemeConstands.headline5_Medium_16.copyWith(
                                color: Colorconstands.neutralDarkGrey,
                                decoration: TextDecoration.none,
                                overflow: TextOverflow.visible,
                              ),
                            ),
                            const SizedBox(
                              width: 8.0,
                            ),
                            GestureDetector(
                                onTap: () {
                                  phoneSignIn();
                                  setState(() {
                                    final snackBar = SnackBar(
                                      content: Text('RESUBMIT'.tr()),
                                      padding:const EdgeInsets.symmetric(
                                          horizontal: 16, vertical: 16.0),
                                    );
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackBar);
                                  });
                                },
                                child: Text(
                                  'RESENT'.tr(),
                                  style: ThemeConstands.headline5_Medium_16.copyWith(
                                    color: Colorconstands.primaryColor,
                                    decoration: TextDecoration.underline,
                                    overflow: TextOverflow.visible,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ))
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
