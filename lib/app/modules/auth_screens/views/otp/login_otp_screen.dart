import 'dart:async';
import 'dart:io';
import 'package:camis_application_flutter/app/modules/auth_screens/e_school_code.dart';
import 'package:camis_application_flutter/app/modules/auth_screens/views/otp/verify_otp_screen.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import '../../../../../storages/get_storage.dart';
import '../../../../../widgets/no_connection_alert_widget.dart';


class LoginOTPScreen extends StatefulWidget {
  LoginOTPScreen({Key? key}) : super(key: key);
  static String verify = "";

  @override
  State<LoginOTPScreen> createState() => _LoginOTPScreenState();
}

class _LoginOTPScreenState extends State<LoginOTPScreen> with Toast {
  late TextEditingController phoneNumberController;
  final auth = FirebaseAuth.instance;
  final GlobalKey<FormState> formKey = GlobalKey();
  bool connection = true;
  StreamSubscription? sub;

  var checkFocusOTP;

  @override
  void initState() {
    setState(() {
      //=============Check internet====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          connection = (event != ConnectivityResult.none);
        });
      });
      //=============Eend Check internet====================
    });
    phoneNumberController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    phoneNumberController.dispose();
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colorconstands.primaryColor,
        body: WillPopScope(
          onWillPop:(){
            exit(0);
          },
          child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: Stack(
              alignment: Alignment.center,
              children: [
                Positioned(
                  top: 0,
                  right: 0,
                  child: Image.asset("assets/images/Oval.png", width: MediaQuery.of(context).size.width / 1.7),
                ),
                Positioned(
                  top: 100,
                  left: 0,
                  right: 0,
                  bottom: 0,
                  child: Container(
                    padding: const EdgeInsets.only(top: 48, left: 27, right: 27),
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(12.0),
                            topRight: Radius.circular(12.0)),
                        color: Colorconstands.neutralWhite),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'ENTERPHONENUMBER'.tr(),
                          style: ThemeConstands.headline2_SemiBold_24.copyWith(color: Colorconstands.neutralDarkGrey, overflow: TextOverflow.fade,),
                        ),
                        const SizedBox(
                          height: 27,
                        ),
                        Text(
                          'PLEASEENTERPHONE'.tr(),
                          style: ThemeConstands.headline4_Regular_18.copyWith(
                            color: Colorconstands.neutralDarkGrey,
                            overflow: TextOverflow.fade,
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 24, bottom: 30),
                          decoration: BoxDecoration(
                            color: Colorconstands.lightGohan,
                            borderRadius: BorderRadius.circular(12),
                            border: Border.all(
                                color: checkFocusOTP == false
                                    ? Colorconstands.neutralGrey
                                    : Colorconstands.primaryColor,
                                width: checkFocusOTP == false ? 1 : 2),
                          ),
                          child: Focus(
                            onFocusChange: (hasfocus) {
                              setState(() {
                                hasfocus
                                    ? checkFocusOTP = true
                                    : checkFocusOTP = false;
                              });
                            },
                            child: Form(
                              key: formKey,
                              child: TextFormField(
                                controller: phoneNumberController,
                                onChanged: (value) {
                                  setState(() {
                                    phoneNumberController.text;
                                  });
                                },
                                keyboardType: TextInputType.number,
                                  inputFormatters: [
                                    FilteringTextInputFormatter.digitsOnly,
                                  ],
                                style: ThemeConstands.subtitle1_Regular_16,
                                decoration: InputDecoration(
                                  border: UnderlineInputBorder(
                                      borderRadius: BorderRadius.circular(16.0)),
                                  contentPadding: const EdgeInsets.only(top: 8, bottom: 5, left: 18, right: 18),
                                  labelText: 'PHONENUMBER'.tr(),
                                  labelStyle: ThemeConstands.subtitle1_Regular_16.copyWith(color: Colorconstands.lightTrunks),
                                  enabledBorder: const UnderlineInputBorder(borderSide: BorderSide(color: Colors.transparent)),
                                  focusedBorder: const UnderlineInputBorder(borderSide: BorderSide(color: Colors.transparent)),
                                ),
                              ),
                            ),
                          ),
                        ),
                        connection==false?Container(height: 0,): MaterialButton(
                          minWidth: double.maxFinite,
                          onPressed: phoneNumberController.text.isNotEmpty
                              ? () async{
                                GetStoragePref _prefAuth = GetStoragePref();
                                var auth = await _prefAuth.getJsonToken;
                                  setState(() {
                                    // if(auth.authUser!.==1){
                                    //   if(phoneNumberController.text == "0${auth.authUser!.phone}"){
                                    //     Navigator.push(context, MaterialPageRoute(builder: (context) => VerifyOTPScreen(phone: phoneNumberController.text),));
                                    //   }
                                    //   else if(phoneNumberController.text == "088123123"){
                                    //      Navigator.push(context, MaterialPageRoute(builder: (context) => VerifyOTPScreen(phone: phoneNumberController.text),));
                                    //   }
                                    //   else{
                                    //     showMessage(
                                    //       context,
                                    //       "PHONE_IS_NOT_CORRECT".tr(),
                                    //       300.0,
                                    //       30.0);
                                    //   }
                                    // }
                                    // else{
                                    //   Navigator.push(context, MaterialPageRoute(builder: (context) => VerifyOTPScreen(phone: phoneNumberController.text),));
                                    // }
                                  });
                                }
                              : null,
                          padding: const EdgeInsets.symmetric(vertical: 12.0),
                          disabledColor: Colorconstands.mainColorUnderlayer,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                          color: Colorconstands.primaryColor,
                          elevation: 0,
                          child: Text('CONTINUE'.tr(), style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.neutralWhite, overflow: TextOverflow.fade),
                          ),
                        ),
                        
                      ],
                    ),
                  ),
                ),
                AnimatedPositioned(
                      bottom: connection == true ? -150 : 0,
                      left: 0,
                      right: 0,
                      duration: const Duration(milliseconds: 500),
                      child: const NoConnectWidget(),
                    ),
              ],
            ),
          ),
        ));
  }
}


/* () async {
  //010926316
  final phone ='+855 ${phoneNumberController.text.substring(1)}';
  await FirebaseAuth.instance.verifyPhoneNumber(
    phoneNumber: phone,
    verificationCompleted:(phoneAuthCredential) async {},
    verificationFailed: (error) {},
    codeSent: (verificationId,forceResendingToken) {LoginOTPScreen.verify =verificationId;Navigator.push(context,MaterialPageRoute(builder: (context) =>OTPVerifyScreen(),));},
    timeout: const Duration(seconds: 60),
    codeAutoRetrievalTimeout:(verificationId) {},
    );
                                      } */                                 