import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../model/teacher_list_model/teacher_list_model.dart';
import '../../../../service/apis/teacher_list/teacher_list_api.dart';
part 'teacher_list_event.dart';
part 'teacher_list_state.dart';

class TeacherListBloc extends Bloc<TeacherListEvent, TeacherListState> {
  final GetTeacherListApi teacherListApi;
  TeacherListBloc({required this.teacherListApi}) : super(TeacherListInitial()) {
    on<GetTeacherListEvent>((event, emit) async{
      emit(GetTeacherLoading());
      try {
        var datateacherList = await teacherListApi.getTeacherRequestApi(idClass: event.idClass);
        emit(GetTeacherLoaded(teacherListModel: datateacherList));
      } catch (e) {
        print(e);
        emit(GetTeacherListError());
      }
    });
  }
}
