import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';

class ContactPairentWidget extends StatelessWidget {
  final String profile;
  final String name;
  final String role;
  final String phone;
  final bool instructor;
  VoidCallback onTap;
  ContactPairentWidget({Key? key,required this.instructor,required this.profile,required this.name,required this.role,required this.onTap, required this.phone}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed:  phone==""?null:onTap,
      padding: const EdgeInsets.all(0),
      color: Colorconstands.screenWhite,
      disabledColor:const Color(0xFFEEEBEB),
      child: Container(
        
        padding: const EdgeInsets.symmetric(vertical: 14, horizontal: 22),
        child: Row(
          children: [
            profile==""?Container():Container(
                child: CircleAvatar(
              radius: 30, // Image radius
              backgroundImage: NetworkImage(profile),
            )),
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(left: 18, right: 18),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(name,
                            style: ThemeConstands.headline4_Medium_18.copyWith(
                              color: Colorconstands.lightBulma,
                            )),
                       instructor==true?const Icon(Icons.star_rounded,size: 20,color: Colorconstands.primaryColor,):Container()
                      ],
                    ),
                    Text(role,
                        style: ThemeConstands.caption_Regular_12.copyWith(
                          color: Colorconstands.lightBulma,
                        )),
                  ],
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(color: Colorconstands.mainColorSecondary.withOpacity(0.1), shape: BoxShape.circle),
              child: SvgPicture.asset(phone==""?"assets/icons/svg/call-slash.svg":ImageAssets.call_outlinr_icon),
            )
          ],
        ),
      ),
    );
  }
}