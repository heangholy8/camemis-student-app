import 'dart:async';
import 'package:camis_application_flutter/service/apis/profile_user/bloc/get_profile_user_bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../../component/change_child/bloc/change_child_bloc.dart';
import '../../../../mixins/toast.dart';
import '../../../../widgets/error_widget/error_request.dart';
import '../../../../widgets/no_connection_alert_widget.dart';
import '../../../../widgets/no_dart.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';
import '../../attendance_screen.dart/widgets/schedule_empty.dart';
import '../bloc/teacher_list_bloc.dart';
import '../widget/contact_pairent_widget.dart';

class TeacherListScreen extends StatefulWidget {
  int activeChild ;
  TeacherListScreen({Key? key,this.activeChild = 0}) : super(key: key);

  @override
  State<TeacherListScreen> createState() => _TeacherListScreenState();
}

class _TeacherListScreenState extends State<TeacherListScreen> with Toast {
  bool? classmonitor = false;
  bool connection = true;
  StreamSubscription? sub;
  String? idClass;
  int conditionGetTeacher = 1;

  bool loadingDataTeacherSuccess = true;
  @override
  void initState() {
    setState(() {
      //=============Check internet====================
      sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
          connection = (event != ConnectivityResult.none);
          if (connection == true) {
            //BlocProvider.of<ChangeChildBloc>(context).add(GetChildrenEvent());
          } else {}
        });
      });
      //=============Eend Check internet====================
    });
    super.initState();
  }

  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: Stack(
        children: [
          Positioned(
            top: 0,
            right: 0,
            child: Image.asset("assets/images/Oval.png"),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(
                        top: 50, left: 22, right: 22, bottom: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: const Icon(
                            Icons.arrow_back_ios_new_rounded,
                            size: 25,
                            color: Colorconstands.neutralWhite,
                          ),
                        ),
                        Container(
                            alignment: Alignment.center,
                            child: Text(
                              "TEACHER_LIST".tr(),
                              style:
                                  ThemeConstands.headline3_SemiBold_20.copyWith(
                                color: Colorconstands.neutralWhite,
                              ),
                              textAlign: TextAlign.center,
                            )),
                        Container(
                          width: 32, height: 32,
                          //child: SvgPicture.asset(ImageAssets.question_circle,color: Colorconstands.neutralWhite,width: 28,height: 28,),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(top: 10),
                      width: MediaQuery.of(context).size.width,
                      child: Container(
                        decoration: const BoxDecoration(
                            color: Colorconstands.neutralWhite,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(12),
                                topRight: Radius.circular(12))),
                        child: BlocBuilder<GetProfileUserBloc, GetProfileUserState>(
                          builder: (context, state) {
                            if (state is ChangeChildLoadingState) {
                              return const Center(
                                child: CircularProgressIndicator(),
                              );
                            } else if (state is ProfileUserLoaded) {
                              var datachild = state.profileusermodel.data;
                              if(datachild! != null){
                                if (connection == true &&
                                    loadingDataTeacherSuccess == true) {
                                  if(datachild.currentClass==null){}
                                  else{
                                    BlocProvider.of<TeacherListBloc>(context).add(
                                      GetTeacherListEvent(
                                          idClass: datachild.currentClass!.classId.toString()));
                                  }
                                  loadingDataTeacherSuccess = false;
                                }
                              }
                              return datachild == null
                                  ? Container(
                                      color: Colorconstands.neutralWhite,
                                      child: DataNotFound(
                                        subtitle: "NONE_CHILD_DES".tr(),
                                        title: "NONE_CHILD".tr(),
                                      ),
                                    )
                                  :Column(
                                      children: [
                                        Expanded(
                                          child: Container(
                                            // decoration: BoxDecoration(
                                            //     color: Colorconstands.primaryColor
                                            //         .withOpacity(0.9),
                                            //     borderRadius:
                                            //         const BorderRadius.only(
                                            //             topLeft:
                                            //                 Radius.circular(12),
                                            //             topRight:
                                            //                 Radius.circular(12))),
                                            child: BlocBuilder<TeacherListBloc,
                                                TeacherListState>(
                                              builder: (context, state) {
                                                if (state is GetTeacherLoading) {
                                                  return const Center(
                                                    child:
                                                        CircularProgressIndicator(),
                                                  );
                                                } else if (state
                                                    is GetTeacherLoaded) {
                                                  var datateacher = state
                                                      .teacherListModel!.data;
                                                  loadingDataTeacherSuccess =
                                                      true;
                                                  return datateacher!.isEmpty
                                                      ? DataNotFound(
                                                          subtitle: "",
                                                          title: translate == "km"
                                                              ? "គ្មាន​ទិន្នន័យ"
                                                              : "No Date entry",
                                                        )
                                                      : Container(
                                                          child:
                                                              ListView.separated(
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(0),
                                                            itemCount: datateacher
                                                                .length,
                                                            itemBuilder: (context,
                                                                indexteacher) {
                                                              return Container(
                                                                child:
                                                                    ContactPairentWidget(
                                                                  instructor: datateacher[indexteacher].isInstructor!,
                                                                  phone: datateacher[
                                                                          indexteacher]
                                                                      .teacherPhone
                                                                      .toString(),
                                                                  name: translate ==
                                                                          "km"
                                                                      ? datateacher[
                                                                              indexteacher]
                                                                          .teacherName
                                                                          .toString()
                                                                      : datateacher[
                                                                              indexteacher]
                                                                          .teacherNameEn
                                                                          .toString(),
                                                                  onTap: () {
                                                                    if (datateacher[
                                                                                indexteacher]
                                                                            .teacherPhone ==
                                                                        "") {
                                                                      showMessage(
                                                                          context,
                                                                          translate ==
                                                                                  "km"
                                                                              ? "គ្មានលេខទូរស័ព្ទ"
                                                                              : "No phone Number",
                                                                          250.0,
                                                                          25.0);
                                                                    } else {
                                                                      String phone = datateacher[indexteacher].teacherPhone!.replaceAll(' ','');
                                                                      _launchLink(
                                                                          "tel:$phone");
                                                                    }
                                                                  },
                                                                  profile: datateacher[indexteacher].profileMedia!.fileShow ==
                                                                              "" ||
                                                                          datateacher[indexteacher].profileMedia!.fileShow ==
                                                                              null
                                                                      ? datateacher[
                                                                              indexteacher]
                                                                          .profileMedia!
                                                                          .fileThumbnail
                                                                          .toString()
                                                                      : datateacher[
                                                                              indexteacher]
                                                                          .profileMedia!
                                                                          .fileShow
                                                                          .toString(),
                                                                  role: translate ==
                                                                          "km"
                                                                      ? datateacher[
                                                                              indexteacher]
                                                                          .subjectName
                                                                          .toString()
                                                                      : datateacher[
                                                                              indexteacher]
                                                                          .subjectNameEn
                                                                          .toString(),
                                                                ),
                                                              );
                                                            },
                                                            separatorBuilder:
                                                                (context, ind) {
                                                              return Container(
                                                                  margin:
                                                                      const EdgeInsets
                                                                              .only(
                                                                          left:
                                                                              100),
                                                                  child:
                                                                      const Divider(
                                                                    height: 1,
                                                                    thickness:
                                                                        0.5,
                                                                  ));
                                                            },
                                                          ),
                                                        );
                                                } else {
                                                  return Container(
                                                    child: ErrorRequestData(
                                                      onPressed: (){
                                                        Navigator.pushReplacement(
                                                            context, 
                                                            PageRouteBuilder(
                                                                pageBuilder: (context, animation1, animation2) =>  TeacherListScreen(),
                                                                transitionDuration: Duration.zero,
                                                                reverseTransitionDuration: Duration.zero,
                                                            ),
                                                        );
                                                      },
                                                      discription: '', 
                                                      hidebutton: true, 
                                                      title: 'WE_DETECT_ERROR'.tr(),
                                        
                                                    ),
                                                  );
                                                }
                                              },
                                            ),
                                          ),
                                        )
                                      ],
                                    );
                            } else {
                               return Container(
                                  child: ErrorRequestData(
                                    onPressed: (){
                                       Navigator.pushReplacement(
                                          context, 
                                          PageRouteBuilder(
                                              pageBuilder: (context, animation1, animation2) =>  TeacherListScreen(),
                                              transitionDuration: Duration.zero,
                                              reverseTransitionDuration: Duration.zero,
                                          ),
                                      );
                                    },
                                    discription: '', 
                                    hidebutton: true, 
                                    title: 'WE_DETECT_ERROR'.tr(),

                                  ),
                                );
                            }
                          },
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          // connection==true?Container():Positioned(
          //   bottom: 0,left: 0,right: 0,top: 0,
          //   child: Container(
          //     color:const Color(0x7B9C9595),
          //   ),
          // ),
          AnimatedPositioned(
            bottom: connection == true ? -150 : 0,
            left: 0,
            right: 0,
            duration: const Duration(milliseconds: 500),
            child: const NoConnectWidget(),
          ),
        ],
      ),
    );
  }

  PopupMenuItem _buildPopupMenuItem(String title, IconData iconData) {
    return PopupMenuItem(
      mouseCursor: MouseCursor.uncontrolled,
      padding: const EdgeInsets.all(0),
      child: Container(
        padding: const EdgeInsets.only(bottom: 5),
        child: Container(
          height: 50,
          decoration: BoxDecoration(
              color: Colorconstands.gray300.withOpacity(0.4),
              borderRadius: const BorderRadius.all(Radius.circular(12))),
          margin: const EdgeInsets.symmetric(horizontal: 7),
          padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
          child: Row(
            children: [
              Icon(
                iconData,
                color: Colorconstands.primaryColor,
                size: 20,
              ),
              const SizedBox(
                width: 8.0,
              ),
              Text(
                title,
                style: ThemeConstands.headline6_Regular_14_20height,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _launchLink(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceWebView: false, forceSafariVC: false);
    } else {}
  }
}
