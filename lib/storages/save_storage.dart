import 'package:camis_application_flutter/storages/key_storage.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SaveStoragePref with keyStoragePref {

  void saveJsonToken({required String authModel}) async {
    try {
      SharedPreferences _pref = await SharedPreferences.getInstance();
      _pref
          .setString(jsonToken, authModel)
          .then((value) => debugPrint("Success to store json in cache"));
    } catch (e) {
      // debugPrint("Unsuccess to store json auth in cache");
    }
  }

  // Save  newFeed in Timeline Screen
  void saveTimelineFeed({required String timelineModel}) async {
    try {
      SharedPreferences _pref = await SharedPreferences.getInstance();
      _pref.setString(timelineSocial, timelineModel);
    } catch (e) {
      print("Timeline new Feed" + e.toString());
    }
  }

  // Save  Phone number
  void savePhoneNumber({required String phoneNumber}) async {
    try {
      SharedPreferences _pref = await SharedPreferences.getInstance();
      _pref.setString(phoneNum, phoneNumber);
    } catch (e) {
      print("Phone Number" + e.toString());
    }
  }

  void saveSeenVideoGuide({required String videoGuideFeature}) async {
    try {
      SharedPreferences _pref = await SharedPreferences.getInstance();
      _pref.setString(videoGuide, videoGuideFeature);
    } catch (e) {
      print("videoGuideFeature" + e.toString());
    }
  }

  void saveRedirectApp({required String saveredirectapp}) async {
    try {
      SharedPreferences _pref = await SharedPreferences.getInstance();
      _pref.setString(redirectApp, saveredirectapp);
    } catch (e) {
      print("App redirect to ABA" + e.toString());
    }
  }


  void saveListSubject({required List<String> listSubject}) async {
    try {
      SharedPreferences _pref = await SharedPreferences.getInstance();
      _pref
          .setStringList("subject", listSubject)
          .then((value) => print("save subject is successfully!!!"));
    } catch (e) {
      print(e.toString());
    }
  }

  void saveSelectLangSuccess({required String selectLangSuccess}) async {
    try {
      SharedPreferences _pref = await SharedPreferences.getInstance();
      _pref.setString(langSuccess.toString(), selectLangSuccess).then((value) {});
    } catch (e) {
      print(e.toString());
    }
  }

  void savePreDataGuardain({required String preData}) async {
    try {
      SharedPreferences _pref = await SharedPreferences.getInstance();
      _pref.setString(preGuardian, preData).then(
            (value) => print("Save Pre data of Guardian is Successfully"),
          );
    } catch (e) {
      print("Fail to Save Pre Data Guardian information");
    }
  }

  void saveLeadingSuccess({required String leadingSuccess}) async {
    try {
      SharedPreferences _pref = await SharedPreferences.getInstance();
      _pref.setString(leadingViewSuccess.toString(), leadingSuccess).then((value) {
         //print("Save School Year ID Successfully");
      });
    } catch (e) {
      print(e.toString());
    }
  }

  void saveComfrimProfileSuccess({required String comfrimProfile}) async {
    try {
      SharedPreferences _pref = await SharedPreferences.getInstance();
      _pref.setString(comfrimProfileSecuess.toString(), comfrimProfile).then((value) {
      });
    } catch (e) {
      print(e.toString());
    }
  }
  //=================End Save defauld child ==============
}
