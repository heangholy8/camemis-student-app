// ignore_for_file: avoid_print, unnecessary_null_comparison
import 'dart:convert';
import 'package:camis_application_flutter/app/modules/time_line/school_time_line/e.schooltimeline.dart';
import 'package:camis_application_flutter/model/authenication_model/auth_model.dart';
import 'package:camis_application_flutter/storages/key_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../model/user_profile/user_profile.dart';

class GetStoragePref with keyStoragePref {
  Future<AuthModel> get getJsonToken async {
    try {
      SharedPreferences _pref = await SharedPreferences.getInstance();
      String? data = _pref.getString(jsonToken) ?? "";
      AuthModel? authModel;
      if (data != null) {
        var result = json.decode(data.toString());
        authModel = AuthModel.fromJson(result);
        return authModel;
      }
      return authModel ?? AuthModel();
    } catch (e) {
      throw Exception("Get JsonToke Unsuccess");
    }
  }

  // Get New Feed
  Future<SchoolTimelineModel> get getTimeline async {
    try {
      SharedPreferences _pref = await SharedPreferences.getInstance();
      String? data = _pref.getString(timelineSocial) ?? "";
      SchoolTimelineModel? newFeed;
      if (data != null) {
        var result = json.decode(data.toString());
        newFeed = SchoolTimelineModel.fromJson(result);
        return newFeed;
      }
      return newFeed ?? SchoolTimelineModel();
    } catch (e) {
      throw Exception("Get School time line Unsuccess");
    }
  }

  // Get Pre-Guardian Data From Preffer
  Future<UserInfoModel> get getPreGuardianData async {
    try {
      SharedPreferences _pref = await SharedPreferences.getInstance();
      String? preData = _pref.getString(preGuardian) ?? "";
      UserInfoModel? userinfoModel;
      if (preData != null) {
        var result = json.decode(preData.toString());
        userinfoModel = UserInfoModel.fromJson(result);
        return userinfoModel;
      }
      return userinfoModel ?? UserInfoModel();
    } catch (e) {
      throw Exception("Get JsonToke Unsuccess");
    }
  }

  Future<String?> getSelectLangSuccess() async {
    try {
      final SharedPreferences _pref = await SharedPreferences.getInstance();
      var lang = _pref.getString(langSuccess.toString()) ?? "";
      if (lang != "") {
        print("SelectLang is save in cache");
        // print(schoolguid);
        return lang;
      } else {
        print("Fail store SelectLang");
        return "";
      }
    } catch (e) {
      print(e.toString());
      return "";
    }
  }

  Future<String?> getPhoneNumber() async {
    try {
      final SharedPreferences _pref = await SharedPreferences.getInstance();
      var phone = _pref.getString(phoneNum.toString()) ?? "";
      if (phone != "") {
        print("phoneNum is save in cache");
        return phone;
      } else {
        print("Fail store phoneNum");
        return "";
      }
    } catch (e) {
      print(e.toString());
      return "";
    }
  }

  Future<String?> getSeenVideoGuide() async {
    try {
      final SharedPreferences _pref = await SharedPreferences.getInstance();
      var video = _pref.getString(videoGuide.toString()) ?? "seenVideoSucess";
      if (video != "") {
        print("videoGuide is save in cache");
        return video;
      } else {
        print("Fail store videoGuide");
        return "";
      }
    } catch (e) {
      print(e.toString());
      return "";
    }
  }

  Future<String?> getRediractApp() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var redirect = _prefs.getString(redirectApp.toString()) ?? "";
      if (redirect != "") {
        print("rediract is save in cache");
        // print(schoolguid);
        return redirect;
      } else {
        print("Fail store rediract");
        return "";
      }
    } catch (e) {
      print(e.toString());
      return "";
    }
  }

  Future<String?> getComfrimProfile() async {
    try {
      final SharedPreferences _pref = await SharedPreferences.getInstance();
      var comfrimProfile = _pref.getString(comfrimProfileSecuess.toString()) ?? "";
      if (comfrimProfile != "") {
        print("ComfrimProfile is save in cache");
        return comfrimProfile;
      } else {
        print("Fail store ComfrimProfile");
        return "";
      }
    } catch (e) {
      print(e.toString());
      return "";
    }
  }

  //=======================================
}
