class keyStoragePref {
  static const String _jsonToken = 'jsonToken';
  static const String _langSuccess = 'LangSuccess';
  static const String _leadingSuccess = 'LeadingSuccess';
  static const String _preData = "preDataGuardain";
  static const String _timelineSocial = 'socialfeed';
  static const String _phoneNumber = 'phoneNumber';
  static const String _saveRedirectApp = 'saveRedirectApp';
  static const String _comfrimProfileSecuess = 'comfrimProfileSecuess';
  static const String _videoGuide = 'videoGuide';

  String get jsonToken => _jsonToken;
  String get langSuccess => _langSuccess;
  String get leadingViewSuccess => _leadingSuccess;
  String get preGuardian => _preData;
  String get timelineSocial => _timelineSocial;
  String get phoneNum => _phoneNumber;
  String get redirectApp => _saveRedirectApp;
  String get comfrimProfileSecuess => _comfrimProfileSecuess;
  String get videoGuide => _videoGuide;
}
