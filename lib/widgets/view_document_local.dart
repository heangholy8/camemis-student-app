import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class ViewPDF extends StatelessWidget {
  String pathPDF = "";
  ViewPDF({required this.pathPDF});

  @override
  Widget build(BuildContext context) {
    return Scaffold( //view PDF 
        appBar: AppBar(
          title: Text("Pdf"),
          backgroundColor: Colorconstands.primaryColor,
        ),
        body: SfPdfViewer.asset(pathPDF),
    );
  }
}