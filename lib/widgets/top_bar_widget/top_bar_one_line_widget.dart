import 'package:flutter/material.dart';

import '../../app/core/themes/color_app.dart';
import '../../app/core/themes/themes.dart';

class TopBarOneLineWidget extends StatelessWidget {
  final String titleTopBar;
  const TopBarOneLineWidget({Key? key, required this.titleTopBar});

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: const Icon(
            Icons.arrow_back_ios_new,
            size: 20,
            color: Colorconstands.darkTextsRegular,
          ),
        ),
        Expanded(
            child: Text(
          titleTopBar,
          style: ThemeConstands.headline3_SemiBold_20.copyWith(
            color: Colorconstands.neutralDarkGrey,
          ),
          textAlign: TextAlign.center,
        )),
      ],
    ));
  }
}
