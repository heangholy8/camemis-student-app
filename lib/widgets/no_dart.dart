import 'package:flutter/material.dart';

import '../app/core/themes/color_app.dart';
import '../app/core/themes/themes.dart';

class DataNotFound extends StatefulWidget {
  final String? title;
  final String? subtitle;
  const DataNotFound({Key? key,required this.title,required this.subtitle}) : super(key: key);

  @override
  State<DataNotFound> createState() => _DataNotFoundState();
}

class _DataNotFoundState extends State<DataNotFound> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      //shrinkWrap: true,
      padding:EdgeInsets.only(top: MediaQuery.of(context).size.height/7),
      children: [
        Container(
          alignment: Alignment.center,
          child: Stack(
            children: [
              Image.asset("assets/images/no_schedule_image.png", width: MediaQuery.of(context).size.width/1.5,),
              Positioned(
                bottom: 0,left: 0,right: 0,
                child: Column(
                  children:[
                    Text(widget.title.toString(),style: ThemeConstands.texttheme.headline6!.copyWith(fontWeight: FontWeight.bold)),
                    Text(widget.subtitle.toString(),style:const TextStyle(color: Colorconstands.black,fontSize: 14.0,),)
                  ],
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}