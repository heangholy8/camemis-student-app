import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../app/core/themes/color_app.dart';

class CardFeatureMore extends StatelessWidget {
  final double radiusButton;
  final double? heightButton;
  final double? heightCircle;
  final double? weightCircle;
  final double? weightButton;
  final double panddingVerButton;
  final TextStyle textStyleButton;
  final double panddinHorButton;
  final VoidCallback? onTap;
  final String title;
  final Color buttonColor;
  final String iconImage;
  const CardFeatureMore({Key? key,required this.radiusButton, required this.onTap, required this.title, this.heightButton, this.weightButton, required this.panddingVerButton, required this.panddinHorButton, required this.textStyleButton, required this.buttonColor, required this.iconImage,this.heightCircle, this.weightCircle,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: weightButton,
      height: heightButton,
      decoration: BoxDecoration(
        color: buttonColor,
        borderRadius: BorderRadius.circular(radiusButton),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.2),
            spreadRadius: 0.5,
            blurRadius: 9,
            offset:const Offset(2, 2),
          )
        ]
      ),
      child: MaterialButton(
        elevation: 0,
         color: buttonColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(radiusButton))),
        padding:const EdgeInsets.all(0),
        onPressed: onTap,
        child: Stack(
          children: [
            Positioned(
              bottom: -7,
              right: -7,
              child: Container(
                padding:const EdgeInsets.all(9),
                width: weightCircle??56,height: heightCircle ?? 56,
                decoration: BoxDecoration(
                  color: Colorconstands.mainColorForecolor.withOpacity(0.25),
                  shape: BoxShape.circle
                ),
                child: SvgPicture.asset(iconImage,color: Colorconstands.mainColorSecondary,),
              ),
            ),
            Positioned(
              right: 40,left: 0,
              top: 0,bottom: 0,
              child: Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.symmetric(vertical: panddingVerButton,horizontal: panddinHorButton),
                child: Text(title,style: textStyleButton,textAlign: TextAlign.left,),
              ),
            ),
            
          ],
        ),
      ),
    );
  }
}