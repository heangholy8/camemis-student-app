import 'package:camis_application_flutter/app/modules/time_line/time_line.dart';
import 'package:camis_application_flutter/app/routes/e.route.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../app/core/resources/asset_resource.dart';
import '../../app/modules/attendance_screen.dart/view/schedule_screen.dart';
import '../../app/modules/result_screen/view/result_screen_V2.dart';
import '../../component/change_child/bloc/change_child_bloc.dart';

class BottomNavigateBar extends StatefulWidget {
  final int isActive;
  final bool paid;
  const BottomNavigateBar(
      {Key? key, required this.isActive, required this.paid})
      : super(key: key);

  @override
  State<BottomNavigateBar> createState() => _BottomNavigateBarState();
}

class _BottomNavigateBarState extends State<BottomNavigateBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.paid == true ? 86 : 141,
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(8), topRight: Radius.circular(8)),
        color: widget.paid == false
            ? Colorconstands.mainColorForecolor
            : Colors.transparent,
      ),
      child: Column(
        children: [
          widget.paid == true
              ? Container()
              : Container(
                  padding: const EdgeInsets.only(left: 18, right: 18),
                  height: 55,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                          child: Text(
                        "UNLOCK_FUNTIONS".tr(),
                        style: ThemeConstands.headline6_Medium_14
                            .copyWith(color: Colorconstands.neutralWhite),
                      )),
                      Container(
                        child: MaterialButton(
                          color: Colorconstands.primaryColor,
                          shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(6))),
                          padding: const EdgeInsets.symmetric(
                              vertical: 5, horizontal: 12),
                          onPressed: () {
                            Navigator.pushNamed(
                                context, Routes.PAYMENTOPTIONSCREEN);
                          },
                          child: Text(
                            "PAY_NOW".tr(),
                            style: ThemeConstands.headline6_SemiBold_14
                                .copyWith(color: Colorconstands.neutralWhite),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
          Container(
            alignment: Alignment.bottomCenter,
            height: 86,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(12), topRight: Radius.circular(12)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.3),
                  spreadRadius: 0.5,
                  blurRadius: 9,
                  offset: const Offset(2, 2),
                )
              ],
            ),
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                children: [
                  Expanded(
                    child: MaterialButton(
                      padding: const EdgeInsets.all(0),
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      onPressed: () {
                        if (widget.isActive != 1) {
                          Navigator.pushAndRemoveUntil(
                              context,
                              PageRouteBuilder(
                                pageBuilder:
                                    (context, animation1, animation2) =>
                                        HomeScreen(),
                                transitionDuration: Duration.zero,
                                reverseTransitionDuration: Duration.zero,
                              ),
                              (route) => false);
                        } else {}
                      },
                      child: Container(
                        child: Column(
                          children: [
                            Container(
                              width: 60,
                              height: 4,
                              decoration: BoxDecoration(
                                  color: widget.isActive == 1
                                      ? Colorconstands.primaryColor
                                      : Colors.white,
                                  borderRadius: BorderRadius.circular(6)),
                            ),
                            Container(
                              margin: const EdgeInsets.symmetric(vertical: 12),
                              child: SvgPicture.asset(widget.isActive == 1
                                  ? ImageAssets.home_icon
                                  : ImageAssets.home_outline_icon),
                            ),
                            Container(
                              child: Text("HOME".tr(),style: ThemeConstands.overline_Semibold_12.copyWith(color:widget.isActive==1? Colorconstands.primaryColor: Colorconstands.neutralDarkGrey,fontWeight:widget.isActive==1? FontWeight.w700:FontWeight.w400,height: 1),textAlign: TextAlign.center,),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(width: 15,),
                  Expanded(
                    child: MaterialButton(
                      padding:const EdgeInsets.all(0),
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      onPressed:(){
                        if(widget.isActive!=2){
                           Navigator.pushAndRemoveUntil(
                              context,
                              PageRouteBuilder(
                              pageBuilder: (context, animation1, animation2) => ScheduleScreen(),
                              transitionDuration: Duration.zero,
                              reverseTransitionDuration: Duration.zero,
                          ),
                              (route) => false);
                      }else{}},
                      child: Container(
                        child: Column(
                          children: [
                            Container(
                              width: 60,
                              height: 4,
                              decoration: BoxDecoration(
                                color:widget.isActive==2?Colorconstands.primaryColor:Colors.white,
                                borderRadius: BorderRadius.circular(6)
                              ),
                            ),
                            Container(
                              margin:const EdgeInsets.symmetric(vertical: 12),
                              child: SvgPicture.asset(widget.isActive==2?ImageAssets.task_icon :ImageAssets.task_outline_icon),
                            ),
                            Container(
                              child: Text("SCHEDULE".tr(),style: ThemeConstands.overline_Semibold_12.copyWith(color:widget.isActive==2? Colorconstands.primaryColor: Colorconstands.neutralDarkGrey,fontWeight:widget.isActive==2? FontWeight.w700:FontWeight.w400,height: 1),textAlign: TextAlign.center,),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  // Expanded(
                  //   child: MaterialButton(
                  //     padding: const EdgeInsets.all(0),
                  //     splashColor: Colors.transparent,
                  //     highlightColor: Colors.transparent,
                  //     onPressed: () {
                  //       if (widget.isActive != 2) {
                  //         Navigator.pushAndRemoveUntil(
                  //             context,
                  //             PageRouteBuilder(
                  //               pageBuilder:
                  //                   (context, animation1, animation2) =>
                  //                       ScheduleScreen(),
                  //               transitionDuration: Duration.zero,
                  //               reverseTransitionDuration: Duration.zero,
                  //             ),
                  //             (route) => false);
                  //       } else {}
                  //     },
                  //     child: Container(
                  //       child: Column(
                  //         children: [
                  //           Container(
                  //             width: 60,
                  //             height: 4,
                  //             decoration: BoxDecoration(
                  //                 color: widget.isActive == 2
                  //                     ? Colorconstands.primaryColor
                  //                     : Colors.white,
                  //                 borderRadius: BorderRadius.circular(6)),
                  //           ),
                  //           Container(
                  //             margin: const EdgeInsets.symmetric(vertical: 12),
                  //             child: SvgPicture.asset(widget.isActive == 2
                  //                 ? ImageAssets.task_icon
                  //                 : ImageAssets.task_outline_icon),
                  //           ),
                  //           Container(
                  //             child: Text("RESULTS".tr(),style: ThemeConstands.overline_Semibold_12.copyWith(color:widget.isActive==3? Colorconstands.primaryColor: Colorconstands.neutralDarkGrey,
                  //             fontWeight:widget.isActive==3? FontWeight.w700:FontWeight.w400,height: 1),textAlign: TextAlign.center,),
                  //           ),
                  //         ],
                  //       ),
                  //     ),
                  //   ),
                  // ),
                  const SizedBox(
                    width: 15,
                  ),
                  Expanded(
                    child: MaterialButton(
                      padding: const EdgeInsets.all(0),
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      onPressed: () {
                        if (widget.isActive != 3) {
                          Navigator.pushAndRemoveUntil(
                              context,
                              PageRouteBuilder(
                                pageBuilder:
                                    (context, animation1, animation2) =>
                                        ResultScreenV2(),
                                transitionDuration: Duration.zero,
                                reverseTransitionDuration: Duration.zero,
                              ),
                              (route) => false);
                        } else {}
                      },
                      child: Container(
                        child: Column(
                          children: [
                            Container(
                              width: 60,
                              height: 4,
                              decoration: BoxDecoration(
                                  color: widget.isActive == 3
                                      ? Colorconstands.primaryColor
                                      : Colors.white,
                                  borderRadius: BorderRadius.circular(6)),
                            ),
                            Container(
                              margin: const EdgeInsets.symmetric(vertical: 12),
                              child: SvgPicture.asset(
                                widget.isActive == 3
                                    ? ImageAssets.doucment_check_icon
                                    : ImageAssets.doucment_check_icon_menu,
                                color: widget.isActive == 3
                                    ? Colorconstands.primaryColor
                                    : Colorconstands.lightBlack,
                              ),
                            ),
                            Container(
                              child: Text("RESULTS".tr(),
                                  style: ThemeConstands.overline_Semibold_12
                                      .copyWith(
                                          color: widget.isActive == 3
                                              ? Colorconstands.primaryColor
                                              : Colorconstands.neutralDarkGrey,
                                          fontWeight: widget.isActive == 3
                                              ? FontWeight.w700
                                              : FontWeight.w400,
                                          height: 1)),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 15,
                  ),
                  // Expanded(
                  //   child: GestureDetector(
                  //     onTap:(){
                  //       if(widget.isActive!=4){
                  //       Navigator.pushReplacement(
                  //         context,
                  //         PageRouteBuilder(
                  //             pageBuilder: (context, animation1, animation2) => const NotifictaionScreen(),
                  //             transitionDuration: Duration.zero,
                  //             reverseTransitionDuration: Duration.zero,
                  //         ),
                  //     );
                  //     }else{}},
                  //     child: Container(
                  //       child: Column(
                  //         children: [
                  //           Container(
                  //             width: 60,
                  //             height: 4,
                  //             decoration: BoxDecoration(
                  //               color:widget.isActive==4?Colorconstands.primaryColor:Colors.white,
                  //               borderRadius: BorderRadius.circular(6)
                  //             ),
                  //           ),
                  //           Container(
                  //            margin:const EdgeInsets.symmetric(vertical: 12),
                  //             child: SvgPicture.asset(widget.isActive==4?ImageAssets.notification_icon:ImageAssets.notification_outline_icon),
                  //           ),
                  //           Container(
                  //             child: Text("ជូនដំណឹង",style: ThemeConstands.overline_Semibold_12.copyWith(color:widget.isActive==4? Colorconstands.primaryColor: Colorconstands.neutralDarkGrey,
                  //             fontWeight:widget.isActive==4? FontWeight.w700:FontWeight.w400,height: 1)),
                  //           ),
                  //         ],
                  //       ),
                  //     ),
                  //   ),
                  // ),
                  // const SizedBox(width: 15,),
                  Expanded(
                    child: MaterialButton(
                      padding: const EdgeInsets.all(0),
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      onPressed: () {
                        if (widget.isActive != 4) {
                          Navigator.pushAndRemoveUntil(
                              context,
                              PageRouteBuilder(
                                pageBuilder:
                                    (context, animation1, animation2) => TimeLineScreen(
                                      selectIndex: 0,
                                    ),
                                transitionDuration: Duration.zero,
                                reverseTransitionDuration: Duration.zero,
                              ),
                              (route) => false);
                        } else {}
                      },
                      child: Column(
                        children: [
                          Container(
                            width: 60,
                            height: 4,
                            decoration: BoxDecoration(
                                color: widget.isActive == 4
                                    ? Colorconstands.primaryColor
                                    : Colors.white,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                          Container(
                            margin: const EdgeInsets.symmetric(vertical: 12),
                            child: SvgPicture.asset(widget.isActive == 4
                                ? ImageAssets.tilemline_icon
                                : ImageAssets.tilemline_outline_icon),
                          ),
                          Container(
                            child: Text("TIMELINE".tr(),style: ThemeConstands.overline_Semibold_12.copyWith(color: widget.isActive==4? Colorconstands.primaryColor: Colorconstands.neutralDarkGrey,fontWeight:widget.isActive==4? FontWeight.w700:FontWeight.w400,height: 1),textAlign: TextAlign.center,),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: MaterialButton(
                      padding: const EdgeInsets.all(0),
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      onPressed: () {
                        if (widget.isActive != 5) {
                          Navigator.pushAndRemoveUntil(
                              context,
                              PageRouteBuilder(
                                pageBuilder:
                                    (context, animation1, animation2) =>
                                        const MoreScreen(),
                                transitionDuration: Duration.zero,
                                reverseTransitionDuration: Duration.zero,
                              ),
                              (route) => false);
                        } else {}
                      },
                      child: Container(
                        child: Column(
                          children: [
                            Container(
                              width: 60,
                              height: 4,
                              decoration: BoxDecoration(
                                  color: widget.isActive == 5
                                      ? Colorconstands.primaryColor
                                      : Colors.white,
                                  borderRadius: BorderRadius.circular(6)),
                            ),
                            Container(
                              margin: const EdgeInsets.symmetric(vertical: 12),
                              child: SvgPicture.asset(widget.isActive == 5
                                  ? ImageAssets.more_icon
                                  : ImageAssets.more_outline_icon),
                            ),
                            Container(
                              child: Text("MORE".tr(),style: ThemeConstands.overline_Semibold_12.copyWith(color: widget.isActive==5? Colorconstands.primaryColor: Colorconstands.neutralDarkGrey,fontWeight:widget.isActive==5? FontWeight.w700:FontWeight.w400,height: 1),textAlign: TextAlign.center,),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
