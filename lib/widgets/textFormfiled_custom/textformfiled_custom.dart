import 'package:camis_application_flutter/app/modules/auth_screens/e_school_code.dart';
class TextFormFiledCustom extends StatelessWidget {
  final OnChanged onChanged;
  final Function onFocusChange;
  final String title;
  final TextEditingController controller;
  final bool checkFourcus;
  final bool enable;
  final TextInputType keyboardType;

  const TextFormFiledCustom({Key? key, required this.onChanged, required this.onFocusChange, required this.title, required this.controller, required this.checkFourcus, required this.enable, required this.keyboardType}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:
          const EdgeInsets.only(top: 16),
      padding: EdgeInsets.zero,
      decoration: BoxDecoration(
        color: Colorconstands.lightGohan,
        borderRadius:
            BorderRadius.circular(12),
        border: Border.all(
            color:checkFourcus == false
                    ? Colorconstands.neutralGrey: Colorconstands.primaryColor,
            width:checkFourcus == false ? 1: 2),
      ),
      child: Focus(
        onFocusChange: onFocusChange(),
        child: TextFormField(
          keyboardType:keyboardType,
          enabled: enable,
          controller:controller,
          onChanged: onChanged,
          style: ThemeConstands.subtitle1_Regular_16,
          decoration: InputDecoration(
            border: UnderlineInputBorder(
                borderRadius:
                    BorderRadius.circular(
                        16.0)),
            contentPadding:
                const EdgeInsets.only(
                    top: 8,
                    bottom: 5,
                    left: 18,
                    right: 18),
            labelText: title,
            labelStyle: ThemeConstands
                .subtitle1_Regular_16
                .copyWith(
                    color: Colorconstands
                        .lightTrunks),
            enabledBorder:
                const UnderlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors
                            .transparent)),
            focusedBorder:
                const UnderlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors
                            .transparent)),
          ),
        ),
      ),
    );
  }
}