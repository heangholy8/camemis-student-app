import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class CustomYoutubePlayer extends StatefulWidget {
  final String youtubeUrl;
  const CustomYoutubePlayer({Key? key, required this.youtubeUrl}) : super(key: key);

  @override
  State<CustomYoutubePlayer> createState() => _CustomYoutubePlayerState();
}

class _CustomYoutubePlayerState extends State<CustomYoutubePlayer> {
  late YoutubePlayerController _controller;
  @override
  void initState() {
    _controller = YoutubePlayerController(
      initialVideoId: YoutubePlayerController.convertUrlToId(widget.youtubeUrl)!,
      params: const YoutubePlayerParams(
        startAt:  Duration(minutes: 0, seconds: 00),
        showControls: true,
        autoPlay: true,
        mute:false,
        showFullscreenButton: true,
        showVideoAnnotations:true,
        useHybridComposition:true,
        privacyEnhanced:true,
        strictRelatedVideos:true,
      )
    );
    _controller.onEnterFullscreen = () {
      SystemChrome.setPreferredOrientations([
       DeviceOrientation.portraitUp,
       DeviceOrientation.portraitDown,
      ]);
    };
    _controller.onExitFullscreen = () {
      SystemChrome.setPreferredOrientations([
       DeviceOrientation.portraitUp,
       DeviceOrientation.portraitDown,
     ]);
    };
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return YoutubePlayerControllerProvider(
      controller: _controller,
      child: LayoutBuilder(
        builder: (context, snapshot) {
          return YoutubePlayerIFrame(
            aspectRatio: 1,
            controller: _controller,
          );
        }
      ),
    );
  }
}