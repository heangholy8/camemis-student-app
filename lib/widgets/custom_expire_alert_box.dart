import '../app/modules/PaymentAndBillScreen/bloc/payment_option_bloc.dart';
import '../app/modules/PaymentAndBillScreen/view/selected_payment_option.dart';
import '../app/modules/auth_screens/e_school_code.dart';

class CustomExpireAlertBox extends StatelessWidget {
  const CustomExpireAlertBox({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Container(
            width: 305,
            padding: const EdgeInsets.symmetric(vertical: 29),
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      color: Colors.black.withOpacity(0.5),
                      blurRadius: 2,
                      offset: const Offset(0, 0))
                ],
                color: Colors.lightBlue.shade50,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20))),
            child: Text(
              "Your account is \nabout to expire!",
              textAlign: TextAlign.center,
              style: ThemeConstands.texttheme.headline4?.copyWith(
                  color: const Color.fromRGBO(24, 85, 171, 1),
                  fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            width: 305,
            padding: const EdgeInsets.symmetric(vertical: 15),
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      color: Colors.black.withOpacity(0.5),
                      blurRadius: 2,
                      offset: const Offset(0, 2))
                ],
                color: Colors.white,
                borderRadius: const BorderRadius.only(
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20))),
            child: Column(
              children: [
                Text(
                  "Your CAMEMIS account will \nexpire in:",
                  textAlign: TextAlign.center,
                  style: ThemeConstands.texttheme.headline6?.copyWith(
                    color: Colors.black,
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
                Text(
                  "3 days",
                  textAlign: TextAlign.center,
                  style: ThemeConstands.texttheme.headline3?.copyWith(
                      color: const Color.fromRGBO(24, 85, 171, 1),
                      fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 5,
                ),
                Text(
                  "Due date: 06/08/2022",
                  textAlign: TextAlign.center,
                  style: ThemeConstands.texttheme.subtitle1?.copyWith(
                    color: Colors.black,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  "To continue using the CAMEMIS app, \nPlease renew your service fee below.",
                  textAlign: TextAlign.center,
                  style: ThemeConstands.texttheme.subtitle2?.copyWith(
                    color: Colors.grey.shade600,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 1.6,
                  height: 50,
                  decoration: const BoxDecoration(
                      color: Colorconstands.primaryColor,
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: MaterialButton(
                    onPressed: () {
                      BlocProvider.of<PaymentOptionBloc>(context)
                          .add(GetPaymentOptionEvent());
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => const SelectedPaymentOption(),
                        ),
                      );
                    },
                    child: Text(
                      "Pay Now",
                      style: ThemeConstands.texttheme.subtitle1?.copyWith(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 1.6,
                  height: 50,
                  decoration: const BoxDecoration(
                      color: Colorconstands.white,
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: MaterialButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      "I’ll do it later",
                      style: ThemeConstands.texttheme.subtitle1?.copyWith(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
