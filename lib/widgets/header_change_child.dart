
import 'package:flutter/material.dart';

import '../app/core/themes/color_app.dart';
import '../app/core/themes/themes.dart';

class HeaderChangeChild extends StatelessWidget {
  const HeaderChangeChild({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: const EdgeInsets.only(top: 30.0),
          alignment: Alignment.center,
          child: Align(
            child: Text("Choose Childrent",
                style: ThemeConstands.texttheme.headline6!
                    .copyWith(color: Colorconstands.black)),
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(
              horizontal: 30, vertical: 20),
          child: Align(
            child: Text(
              "Excepteur sint occaecat cupidatat non proident, sunt in culpa",
              style: ThemeConstands.texttheme.subtitle2!
                  .copyWith(color: Colorconstands.black),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ],
    );
  }
}