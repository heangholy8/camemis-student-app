import 'dart:io';
import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ShowPiker extends StatelessWidget {
  VoidCallback onPressedCamera;
  VoidCallback onPressedGalary;
  ShowPiker(
      {Key? key, required this.onPressedCamera, required this.onPressedGalary})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        height: 200,
        margin: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Container(
              height: 45,
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(12),
                    topRight: Radius.circular(12)),
                color: Colors.grey[200],
              ),
              child: MaterialButton(
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(15))),
                padding: const EdgeInsets.all(0),
                onPressed: onPressedCamera,
                child: Align(
                    alignment: Alignment.center,
                    child: Text('CAMERA'.tr(),
                        style: ThemeConstands.subtitle1_Regular_16
                            .copyWith(color: Colorconstands.primaryColor))),
              ),
            ),
            Container(
              height: 1,
              color: const Color(0xFFEFEBEB),
            ),
            Container(
              height: 45,
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    bottomLeft: Radius.circular(12),
                    bottomRight: Radius.circular(12)),
                color: Colors.grey[200],
              ),
              child: MaterialButton(
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12))),
                  padding: const EdgeInsets.all(0),
                  onPressed: onPressedGalary,
                  child: Align(
                      alignment: Alignment.center,
                      child: Text('GALLERY'.tr(),
                          style: ThemeConstands.subtitle1_Regular_16
                              .copyWith(color: Colorconstands.primaryColor)))),
            ),
            Container(
              height: 45,
              margin: const EdgeInsets.only(top: 7, bottom: 12),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: Colorconstands.neutralWhite,
              ),
              child: MaterialButton(
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12))),
                  padding: const EdgeInsets.all(0),
                  child: Align(
                      alignment: Alignment.center,
                      child: Text('CANCEL'.tr(),
                          style: ThemeConstands.button_SemiBold_16.copyWith(
                              color: Colorconstands.primaryColor,
                              fontWeight: FontWeight.bold))),
                  onPressed: () {
                    Navigator.of(context).pop();
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
