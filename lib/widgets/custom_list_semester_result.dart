// import 'package:camis_application_flutter/model/result/semester_result.dart';
// import 'package:easy_localization/easy_localization.dart';
// import 'package:flutter/src/foundation/key.dart';
// import 'package:flutter/src/widgets/framework.dart';

// import '../app/modules/auth_screens/e_school_code.dart';

// class CustomListListSemesterResult extends StatefulWidget {
//   TermData data;
//   CustomListListSemesterResult({Key? key, required this.data})
//       : super(key: key);

//   @override
//   State<CustomListListSemesterResult> createState() =>
//       _CustomListListSemesterResultState();
// }

// class _CustomListListSemesterResultState
//     extends State<CustomListListSemesterResult> {
//   @override
//   Widget build(BuildContext context) {
//     final translate = context.locale.toString();
//     return ListView.separated(
//       padding:const EdgeInsets.all(0),
//       shrinkWrap: true,
//       physics: const ScrollPhysics(),
//       itemCount: widget.data.termSubjects!.length,
//       itemBuilder: (context, index) {
//         return Container(
//           padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
//           width: MediaQuery.of(context).size.width,
//           child: Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: [
//               Expanded(
//                 child: Container(
//                   child: Center(
//                     child: Text(
//                       (index + 1).toString(),
//                       style: ThemeConstands.texttheme.subtitle1,
//                     ),
//                   ),
//                 ),
//               ),
//               Expanded(
//                 child: Container(
//                   child: Align(
//                     alignment: Alignment.centerLeft,
//                     child: Text(
//                      translate=="en"? widget.data.termSubjects![index].nameEn.toString()==""?widget.data.termSubjects![index].name.toString()
//                      :widget.data.termSubjects![index].nameEn.toString():widget.data.termSubjects![index].name.toString(),
//                       textAlign: TextAlign.left,
//                       style: ThemeConstands.texttheme.subtitle1,
//                     ),
//                   ),
//                 ),
//               ),
//               Expanded(
//                 child: Container(
//                   child: Center(
//                     child: Text(
//                       widget.data.termSubjects![index].score.toString() ==
//                               "null"
//                           ? "- - -"
//                           : widget.data.termSubjects![index].score
//                               .toString(),
//                       style: ThemeConstands.texttheme.subtitle1,
//                     ),
//                   ),
//                 ),
//               ),
//               Expanded(
//                 child: Container(
//                   child: Center(
//                     child: Text(
//                       widget.data.termSubjects![index].rank.toString() ==
//                               "null"
//                           ? "- - -"
//                           : widget.data.termSubjects![index].rank
//                               .toString(),
//                       style: ThemeConstands.texttheme.subtitle1?.copyWith(
//                           color: widget.data.termSubjects![index]
//                                       .letterGradeEn
//                                       .toString() ==
//                                   "F"
//                               ? Colors.red
//                               : Colorconstands.primaryColor),
//                     ),
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         );
//       }, separatorBuilder: (BuildContext context, int index) { 
//         return const Divider();
//        },
//     );
//   }

//   Widget _buildLoading() => Container(
//       margin: const EdgeInsets.only(right: 10.0),
//       child: Center(
//           child: Image.asset(
//         "assets/images/gifs/loading.gif",
//         height: 45,
//         width: 45,
//       )));
// }
