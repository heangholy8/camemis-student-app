// ignore_for_file: must_be_immutable

import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:flutter/material.dart';

class CustomTextFieldWidget extends StatelessWidget {
  final String? title;
  final String? hint;
  final TextEditingController? controller;
  final ValueChanged<String>? onChanged;
  final FormFieldValidator<String>? validator;
  final Color? borderColor;
  final Color? borderValidate;
  final Widget? suffixIcon;
  final Color? fillcolor;
  final TextInputType? keytype;
  final Color? titleColor;
  final Widget? prefix;
  bool secureText;
  bool? validated;

  CustomTextFieldWidget({
    Key? key,
    this.title,
    this.hint,
    this.titleColor,
    this.keytype,
    this.fillcolor,
    this.controller,
    this.onChanged,
    this.validator,
    this.borderColor,
    this.borderValidate,
    this.suffixIcon,
    this.secureText = false,
    this.validated = false,
    this.prefix,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 18, vertical: 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "$title ",
            style: ThemeConstands.texttheme.subtitle1!.copyWith(
              color: Colors.black,
              fontWeight: FontWeight.w500,
            ),
          ),
          title!.isNotEmpty
              ? const SizedBox(
                  height: 8,
                )
              : const SizedBox(),
          SizedBox(
            height: 56,
            child: TextFormField(
              controller: controller,
              obscureText: secureText,
              autocorrect: true,
              decoration: InputDecoration(
                prefixIcon: prefix,
                hintText: hint ?? "Enter School Code",
                hintStyle: ThemeConstands.texttheme.subtitle1!.copyWith(
                    fontWeight: FontWeight.w600,
                    color: const Color(0xFF8B8B8B)),
                fillColor: fillcolor ?? Colors.white,
                filled: true,
                suffixIcon: suffixIcon,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12.0),
                  borderSide: const BorderSide(),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12.0),
                  borderSide: BorderSide(
                    color: !validated! ? Colorconstands.primaryColor : Colors.red,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12.0),
                  borderSide: BorderSide(
                    color: !validated!
                        ? const Color(0xFFDADADA).withOpacity(.7)
                        : Colors.red,
                  ),
                ),
              ),
              onChanged: onChanged,
              validator: validator,
              keyboardType: keytype ?? TextInputType.text,
              style: ThemeConstands.texttheme.subtitle1!.copyWith(
                fontWeight: FontWeight.w600,
                color: !validated! ? Colorconstands.primaryColor : Colors.red,
              ),
            ),
          )
        ],
      ),
    );
  }
}

typedef OnFieldSubmitted = void Function(String? value);
typedef OnChanged = void Function(String? value);

class CustomTextField extends StatelessWidget {
  final TextEditingController? controller;
  final TextInputType? textInputType;
  final String? hintText;
  final OnFieldSubmitted? onFieldSubmitted;
  final OnChanged? onChanged;
  final FocusNode? focusNode;
  final int? maxLines;
  final bool expands;
  final bool errorState;
  final Widget? suffixIcon;
  bool? securetext;
  final GestureDetector? secureTap;
  CustomTextField(
      {Key? key,
      this.controller,
      this.onFieldSubmitted,
      this.onChanged,
      this.focusNode,
      this.maxLines = 1,
      this.expands = false,
      this.errorState = false,
      this.hintText = '',
      this.textInputType = TextInputType.text,
      this.suffixIcon,
      this.securetext = false,
      this.secureTap})
      : super(key: key);

  static InputBorder enabledDefaultBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(18.0),
    borderSide: BorderSide(
      color: const Color(0xFFDADADA).withOpacity(.7),
    ),
  );

  // static InputBorder focusedDefaultBorder = OutlineInputBorder(
  //   borderRadius: BorderRadius.circular(18.0),
  //   borderSide: const BorderSide(color: Colorconstands.primaryColor),
  // );

  // static TextStyle defaultHintStyle = ThemeConstands.texttheme.subtitle1!.copyWith(
  //       fontWeight: FontWeight.w600,
  //       color: errorState ? Colors.red : Colorconstands.primaryColor,
  //     );

  static InputBorder customErrorBorder = OutlineInputBorder(
      borderRadius: BorderRadius.circular(18.0),
      borderSide: const BorderSide(
        color: Colorconstands.errorColor,
      ));

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      
      
      obscureText: securetext!,
      controller: controller,
      focusNode: focusNode,
      keyboardType: textInputType,
      onFieldSubmitted: onFieldSubmitted,
      onChanged: onChanged,
      maxLines: maxLines,
      expands: expands,
      textAlignVertical: TextAlignVertical.top,
      decoration: InputDecoration(
        
        fillColor: Colors.white,
        filled: true,
        hintText: hintText,
        suffixIcon: suffixIcon,
        hintStyle: ThemeConstands.texttheme.subtitle1!.copyWith(
          fontWeight: FontWeight.w600,
          color: const Color(0xFF8B8B8B),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(18.0),
          borderSide: const BorderSide(),
        ),
        enabledBorder: errorState ? customErrorBorder : enabledDefaultBorder,
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(18.0),
          borderSide: BorderSide(
              color: errorState
                  ? Colorconstands.errorColor
                  : Colorconstands.primaryColor),
        ),
      ),
      style: ThemeConstands.texttheme.subtitle1!.copyWith(
        fontWeight: FontWeight.w600,
        color:
            errorState ? Colorconstands.errorColor : Colorconstands.primaryColor,
      ),
    );
  }
}
