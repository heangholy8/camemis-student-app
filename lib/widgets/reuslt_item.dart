import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../app/core/themes/themes.dart';

class ResultItem extends StatelessWidget {
  final String title;
  String? score;
  String? rank;
  ResultItem({
    Key? key,
    required this.title,
    this.score,
    this.rank,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 15, right: 10, left: 20),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                flex: 2,
                child: Text(title, style: ThemeConstands.texttheme.subtitle1),
              ),
              Expanded(
                child: Center(
                  child: Text("${score}",
                      style: ThemeConstands.texttheme.subtitle1
                          ?.copyWith(fontWeight: FontWeight.w600)),
                ),
              ),
              Expanded(
                child: Center(
                  child: Text(rank == "0" ? "" : "${rank}",
                      style: ThemeConstands.texttheme.subtitle1?.copyWith(
                          fontWeight: FontWeight.w600, color: Colors.red)),
                ),
              ),
            ],
          ),
          const Divider(
            indent: 0,
            endIndent: 0,
            thickness: 1,
            height: 1,
          )
        ],
      ),
    );
  }
}
