import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import '../app/core/themes/themes.dart';

class ViewDocuments extends StatefulWidget {
  final String? path;
  final String? filename;
  const ViewDocuments({Key? key, this.path, this.filename}) : super(key: key);

  @override
  State<ViewDocuments> createState() => _ViewDocumentsState();
}

class _ViewDocumentsState extends State<ViewDocuments> {
  final GlobalKey<SfPdfViewerState> _pdfViewStateKey = GlobalKey();
  late PdfViewerController controller;
  OverlayEntry? _overlayEntry;
  @override
  void initState() {
    super.initState();
    controller = PdfViewerController();
    setState(() {
      widget.path;
    });
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 80,
        leadingWidth: 70,
        automaticallyImplyLeading: true,
        centerTitle: true,
        title: Container(
          margin: const EdgeInsets.only(top: 0.0),
          child: Text(widget.filename == null ? "Document" : widget.filename!,
              style: ThemeConstands.texttheme.headline5!.copyWith(
                color: Colors.white,
                fontWeight: FontWeight.w400,
              ),
              textAlign: TextAlign.center),
        ),
        // bottom: PreferredSize(
        //     child: Container(
        //       margin: const EdgeInsets.only(bottom: 5),
        //       child: Row(
        //         mainAxisAlignment: MainAxisAlignment.end,
        //         children: [
        //           IconButton(
        //               onPressed: () {
        //                 setState(() {
        //                   controller.previousPage();
        //                 });
        //               },
        //               icon: const Icon(
        //                 Icons.arrow_back_ios,
        //                 color: Colors.white,
        //               )),
        //           Text(
        //             controller.pageNumber.toString() +
        //                 " of/ " +
        //                 controller.pageCount.toString(),
        //             style: const TextStyle(
        //               fontSize: 15,
        //               color: Colors.white,
        //             ),
        //           ),
        //           IconButton(
        //               onPressed: () {
        //                 setState(() {
        //                   controller.nextPage();
        //                 });
        //               },
        //               icon: const Icon(
        //                 Icons.arrow_forward_ios_outlined,
        //                 color: Colors.white,
        //               )),
        //         ],
        //       ),
        //     ),
        //     preferredSize: Size.fromHeight(4.0)),

        actions: <Widget>[
          Container(
            margin: const EdgeInsets.only(right: 7.0),
            child: IconButton(
                onPressed: () {
                  _pdfViewStateKey.currentState!.openBookmarkView();
                },
                icon: const Icon(Icons.bookmark_border_sharp)),
          ),
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: SfPdfViewer.network(
              widget.path.toString(),
              controller: controller,
              canShowScrollHead: true,
              scrollDirection: PdfScrollDirection.vertical,
              key: _pdfViewStateKey,
              enableDocumentLinkAnnotation: true,
              enableTextSelection: true,
              onDocumentLoaded: (PdfDocumentLoadedDetails loading) {
                setState(() {});
              },
              onPageChanged: (value) {
                setState(() {
                  controller.pageNumber.toString();
                });

                // print(value);
              },
              onTextSelectionChanged: (PdfTextSelectionChangedDetails details) {
                if (details.selectedText == null && _overlayEntry != null) {
                  _overlayEntry?.remove();

                  _overlayEntry = null;
                } else if (details.selectedText != null &&
                    _overlayEntry == null) {
                  _showContextMenu(context, details);
                }
              },
            ),
          ),
          Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.only(bottom: 25),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                IconButton(
                    onPressed: () {
                      setState(() {
                        controller.previousPage();
                      });
                    },
                    icon: const Icon(
                      Icons.arrow_back_ios,
                      color: Colors.black,
                    )),
                Text(
                  controller.pageNumber.toString() +
                      " of/ " +
                      controller.pageCount.toString(),
                  style: const TextStyle(
                    fontSize: 15,
                    color: Colors.black,
                  ),
                ),
                IconButton(
                    onPressed: () {
                      setState(() {
                        controller.nextPage();
                      });
                    },
                    icon: const Icon(
                      Icons.arrow_forward_ios_outlined,
                      color: Colors.black,
                    )),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _showContextMenu(
      BuildContext context, PdfTextSelectionChangedDetails details) {
    final OverlayState? _overlayState = Overlay.of(context);
    _overlayEntry = OverlayEntry(
      builder: (context) => Positioned(
        top: details.globalSelectedRegion!.center.dy - 55,
        left: details.globalSelectedRegion?.bottomLeft.dx,
        child: ElevatedButton(
          child: const Text(
            'Copy',
            style: TextStyle(fontSize: 17),
          ),
          onPressed: () {
            Clipboard.setData(
              ClipboardData(
                text: details.selectedText,
              ),
            );
            controller.clearSelection();
          },
          // color: Colors.white,
          // elevation: 10,
        ),
      ),
    );
    _overlayState?.insert(_overlayEntry!);
  }
}
