import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../app/core/themes/color_app.dart';
import '../app/core/themes/themes.dart';

class ShowMenuGallary extends StatelessWidget {
  VoidCallback onPressedItem1;
  VoidCallback onPressedItem2;
  String nameItem1;
  String nameItem2;
  ShowMenuGallary({Key? key,required this.onPressedItem1,required this.onPressedItem2,required this.nameItem1,required this.nameItem2}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
            child: Container(
              height: 200,
              margin: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                   Container(
                     height: 45,
                     decoration: BoxDecoration(
                       borderRadius: BorderRadius.only(topLeft:const Radius.circular(12),topRight:const Radius.circular(12),bottomLeft: Radius.circular(nameItem2 ==""?12:0),bottomRight: Radius.circular(nameItem2 ==""?12:0)),
                       color: Colors.grey[200],
                       
                     ),
                     child: MaterialButton(
                       shape:const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15))),
                       padding:  const EdgeInsets.all(0),
                        child: Align(alignment: Alignment.center,child: Text(nameItem1,style: ThemeConstands.texttheme.headline6!.copyWith(color: Colorconstands.primaryColor))),
                        onPressed: onPressedItem1,
                   ),),
                   nameItem2==""?Container(): Container(height: 1,color: Colors.grey,),
                   nameItem2 ==""?Container(): Container(
                     height: 45,
                     decoration: BoxDecoration(
                       borderRadius:const BorderRadius.only(bottomLeft: Radius.circular(12),bottomRight: Radius.circular(12)),
                       color: Colors.grey[200],
                     ),
                     child: MaterialButton(
                       shape:const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12))),
                       padding:  const EdgeInsets.all(0),
                        child: Align(alignment: Alignment.center,child: Text(nameItem2,style: ThemeConstands.texttheme.headline6!.copyWith(color: Colorconstands.primaryColor))),
                        onPressed: onPressedItem2
                      ),
                   ),
                   Container(
                     height: 45,
                     margin:const EdgeInsets.only(top: 3),
                     decoration: BoxDecoration(
                       borderRadius: BorderRadius.circular(12),
                       color: Colorconstands.white,
                     ),
                     child: MaterialButton(
                       shape:const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12))),
                       padding:  const EdgeInsets.all(0),
                        child: Align(alignment: Alignment.center,child: Text('CANCEL'.tr(),style: ThemeConstands.texttheme.headline6!.copyWith(color: Colorconstands.primaryColor,fontWeight: FontWeight.bold))),
                        onPressed: () {
                          Navigator.of(context).pop();
                        }),
                   ),
                ],
              ),
            ),
          );
  }
}