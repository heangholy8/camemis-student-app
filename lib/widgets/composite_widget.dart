import 'package:flutter/material.dart';

abstract class IComponent {
  Widget build(final BuildContext context);
}

class Component extends IComponent {
  Widget child;
  Component({required this.child});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return child;
  }
}

class ColumnComposite extends IComponent {
  final List<IComponent> iComponent;

  ColumnComposite({required this.iComponent});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: iComponent.map((e) {
          final index = iComponent.indexWhere((element) => element == e);
          return Padding(
              padding: EdgeInsets.only(
                  bottom: index == iComponent.length - 1 ? 16.0 : 0),
              child: e.build(context));
        }).toList());
  }
}

class RowComposite extends IComponent {
  final List<IComponent> iComponent;

  RowComposite({required this.iComponent});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: iComponent.map((e) {
              final index = iComponent.indexWhere((element) => element == e);
              return Padding(
                  padding: EdgeInsets.only(
                      bottom: index == iComponent.length - 1 ? 16.0 : 0),
                  child: e.build(context));
            }).toList()));
  }
}

class WrapComposite extends IComponent {
  final List<IComponent> iComponent;

  WrapComposite({required this.iComponent});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Wrap(
        spacing: 12.0,
        runSpacing: 12.0,
        children: iComponent.map((e) {
          final index = iComponent.indexWhere((element) => element == e);
          return Padding(
              padding: EdgeInsets.only(
                  bottom: index == iComponent.length - 1 ? 10.0 : 0),
              child: e.build(context));
        }).toList());
  }
}
