import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:flutter/material.dart';

class BuildBottomSheet extends StatelessWidget {
  final Widget? child;
  final Widget? expanded;
  final double? initialChildSize;
  const BuildBottomSheet({ Key? key, this.child,this.expanded,required this.initialChildSize }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DraggableScrollableSheet(
      initialChildSize: initialChildSize!,
      minChildSize: 0.2,
      maxChildSize: 0.9,
      builder: (_,controller){
        return Container(
        padding:const EdgeInsets.only(top:12.0,),
        
        decoration:const BoxDecoration(
          color: Colorconstands.white,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(15),topRight: Radius.circular(15))
        ),
        child: Column(
          children: [
            Container(
               margin:const EdgeInsets.only(bottom: 12.0),
              height:4,
              width: MediaQuery.of(context).size.width/7,
               decoration: BoxDecoration(
                 color: Colorconstands.darkGray.withOpacity(0.4),
                  borderRadius: BorderRadius.circular(2)
                ),
            ),
            Expanded(
              flex: 1,
              child: ListView(
                controller: controller,
                children: [
                  child!,
                  expanded!,
                ],
              ),
            ),
          ],
        ),
      );
      }
    );
  }
}