// import 'package:camis_application_flutter/model/result/yearly_result.dart';
// import 'package:camis_application_flutter/widgets/reuslt_item.dart';
// import 'package:easy_localization/easy_localization.dart';
// import '../app/modules/auth_screens/e_school_code.dart';

// class ListYearResultItem extends StatefulWidget {
//   YearData data;
//   ListYearResultItem({Key? key, required this.data}) : super(key: key);

//   @override
//   State<ListYearResultItem> createState() => _ListYearResultItemState();
// }

// class _ListYearResultItemState extends State<ListYearResultItem> {
//   @override
//   Widget build(BuildContext context) {
//     final translate = context.locale.toString();
//     return ListView(
//       padding: const EdgeInsets.all(0),
//       physics: const NeverScrollableScrollPhysics(),
//       shrinkWrap: true,
//       children: [
//         ListView.separated(
//           padding: const EdgeInsets.all(0),
//           shrinkWrap: true,
//           physics: const ScrollPhysics(),
//           itemCount: widget.data.yearSubjects!.length,
//           itemBuilder: (context, index) {
//             return Container(
//               padding:
//                   const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
//               width: MediaQuery.of(context).size.width,
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: [
//                   Expanded(
//                     child: Center(
//                       child: Text(
//                         (index + 1).toString(),
//                         style: ThemeConstands.texttheme.subtitle1,
//                       ),
//                     ),
//                   ),
//                   Expanded(
//                     child: Align(
//                       alignment: Alignment.centerLeft,
//                       child: Text(
//                        translate=="en"?widget.data.yearSubjects![index].nameEn.toString()==""?widget.data.yearSubjects![index].name.toString():widget.data.yearSubjects![index].nameEn.toString()
//                        :widget.data.yearSubjects![index].name.toString(),
//                         textAlign: TextAlign.left,
//                         style: ThemeConstands.texttheme.subtitle1,
//                       ),
//                     ),
//                   ),
//                   Expanded(
//                     child: Center(
//                       child: Text(
//                         widget.data.yearSubjects![index].score.toString() ==
//                                 "null"
//                             ? "- - -"
//                             : widget.data.yearSubjects![index].score
//                                 .toString(),
//                         style: ThemeConstands.texttheme.subtitle1,
//                       ),
//                     ),
//                   ),
//                   Expanded(
//                     child: Center(
//                       child: Text(
//                         widget.data.yearSubjects![index].rank.toString() ==
//                                 "null"
//                             ? "- - -"
//                             : widget.data.yearSubjects![index].rank
//                                 .toString(),
//                         style: ThemeConstands.texttheme.subtitle1?.copyWith(
//                             fontWeight: FontWeight.bold,
//                             color: widget.data.yearSubjects![index]
//                                         .letterGradeEn
//                                         .toString() ==
//                                     "F"
//                                 ? Colors.red
//                                 : Colorconstands.primaryColor),
//                       ),
//                     ),
//                   ),
//                 ],
//               ),
//             );
//           },
//           separatorBuilder: (BuildContext context, int index) {
//             return const Divider(
//               indent: 25,
//               endIndent: 15,
//               thickness: 0.5,
//             );
//           },
//         ),
//         Column(
//           mainAxisSize: MainAxisSize.max,
//           children: [
//             ResultItem(
//               title: "YEAR_TOTAL_SCORE".tr(),
//               score: widget.data.yearResult!.avgScore.toString() == "null"
//                   ? "- - -"
//                   : widget.data.yearResult!.avgScore.toString(),
//               rank: widget.data.yearResult!.rank.toString() == "null"
//                   ? "- - -"
//                   : widget.data.yearResult!.rank.toString(),
//             ),
//             ResultItem(
//               title: "AVERAGE_FIRST_SEMESTER".tr(),
//               score: widget.data.yearResult!.firstSemesterAvg.toString() !=
//                       "null"
//                   ? widget.data.yearResult!.firstSemesterAvg.toString()
//                   : "- - -",
//               rank: widget.data.yearResult!.firstSemesterRank.toString() ==
//                       "null"
//                   ? "- - -"
//                   : widget.data.yearResult!.firstSemesterRank.toString(),
//             ),
//             ResultItem(
//               title: "AVERAGE_SECOND_SEMESTER".tr(),
//               score: widget.data.yearResult!.secondSemesterAvg.toString() !=
//                       "null"
//                   ? widget.data.yearResult!.secondSemesterAvg.toString()
//                   : "- - -",
//               rank: widget.data.yearResult!.secondSemesterRank.toString() ==
//                       "null"
//                   ? "- - -"
//                   : widget.data.yearResult!.secondSemesterRank.toString(),
//             ),
//           ],
//         ),
//       ],
//     );
//   }
// }
