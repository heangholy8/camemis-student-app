import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../app/core/themes/color_app.dart';
import '../app/core/themes/themes.dart';

class NoConnectWidget extends StatelessWidget {
  const NoConnectWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      decoration:const BoxDecoration(
        color: Colorconstands.neutralWhite,
        boxShadow: [
          BoxShadow(
            color: Color(0xFFD5D2D2),
            blurRadius: 8.0, // soften the shadow
            spreadRadius: 5.0, //extend the shadow
            offset: Offset(
              5.0, // Move to right 5  horizontally
              5.0, // Move to bottom 5 Vertically
            ),
          )
        ],
      ),
      child: Row(
        children: [
          Container(
            child: Image.asset("assets/images/NoInternet_connect.png",height: 120,width: 120,),
          ),
          Expanded(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Text("NOINTERNET".tr(),style: ThemeConstands.headline4_Medium_18.copyWith(color: Colorconstands.lightBlack,)),
                  ),
                  const SizedBox(height: 12,),
                  Container(
                    child: Text("PLESE_CHECK_INTERNET".tr(),style: ThemeConstands.headline6_Regular_14_20height.copyWith(color: Colorconstands.darkTextsPlaceholder,)),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}