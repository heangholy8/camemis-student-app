import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerChild extends StatelessWidget {
  const ShimmerChild({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 20,
      ),
      child: Shimmer.fromColors(
        baseColor: const Color(0xFF3688F4),
        highlightColor: const Color(0xFF3BFFF5),
        child: Container(
          margin: const EdgeInsets.only(bottom: 5),
          child: Row(
            children: [
              Container(
                height: 65,
                width: 66,
                decoration: const BoxDecoration(
                  color: Color(0x622195F3),
                  shape: BoxShape.circle,
                ),
              ),
              const SizedBox(
                width: 10.0,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 20,
                    width: MediaQuery.of(context).size.width / 2.5,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: const Color(0x622195F3),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 20,
                    width: MediaQuery.of(context).size.width / 3,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: const Color(0x622195F3),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

class ShimmerTimeLine extends StatelessWidget {
  const ShimmerTimeLine({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(
        top: 35,
      ),
      child: Shimmer.fromColors(
        baseColor: const Color(0xFF3688F4),
        highlightColor: const Color(0xFF3BFFF5),
        child: Container(
          margin: const EdgeInsets.only(bottom: 5),
          child: Column(
            children: [
              Container(
                margin: const EdgeInsets.symmetric(
                  horizontal: 18,
                ),
                child: Row(
                  children: [
                    Container(
                      height: 65,
                      width: 66,
                      decoration: const BoxDecoration(
                        color: Color(0x622195F3),
                        shape: BoxShape.circle,
                      ),
                    ),
                    const SizedBox(
                      width: 7.0,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: 17,
                          width: MediaQuery.of(context).size.width / 2.5,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: const Color(0x622195F3),
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Container(
                          height: 15,
                          width: MediaQuery.of(context).size.width / 3,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: const Color(0x622195F3),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.all(18.0),
                height: 60,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: const Color(0x622195F3),
                ),
              ),
              Container(
                height: 230,
                width: MediaQuery.of(context).size.width,
                color: const Color(0x622195F3),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class ShimmerAbsentRequest extends StatelessWidget {
  const ShimmerAbsentRequest({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(
        top: 5,
      ),
      child: Shimmer.fromColors(
        baseColor: const Color(0xFF3688F4),
        highlightColor: const Color(0xFF3BFFF5),
        child: Container(
          margin: const EdgeInsets.only(bottom: 5),
          child: Container(
            decoration: BoxDecoration(
                color: const Color(0x622195F3),
                borderRadius: BorderRadius.circular(12.0)),
            height: 85,
            width: MediaQuery.of(context).size.width,
          ),
        ),
      ),
    );
  }
}

class ShimmerButtonDropDown extends StatelessWidget {
  const ShimmerButtonDropDown({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(
        top: 5,
      ),
      child: Shimmer.fromColors(
        baseColor: const Color(0xFF3688F4),
        highlightColor: const Color(0xFF3BFFF5),
        child: Container(
          margin: const EdgeInsets.only(bottom: 5),
          child: Container(
            decoration: BoxDecoration(
                color: const Color(0x622195F3),
                borderRadius: BorderRadius.circular(12.0)),
            height: 55,
            width: MediaQuery.of(context).size.width,
          ),
        ),
      ),
    );
  }
}

class ShimmerTimeTable extends StatelessWidget {
  const ShimmerTimeTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(
        top: 5,
      ),
      child: Shimmer.fromColors(
        baseColor: const Color(0xFF3688F4),
        highlightColor: const Color(0xFF3BFFF5),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              margin: const EdgeInsets.only(top: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    decoration: const BoxDecoration(
                        color: Color(0x622195F3),
                        borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(12.0),
                            topRight: Radius.circular(12.0))),
                    height: 12,
                    width: 25,
                  ),
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(left: 12.0, right: 22),
                      decoration: BoxDecoration(
                          color: const Color(0x622195F3),
                          borderRadius: BorderRadius.circular(6.0)),
                      height: 22,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 15, horizontal: 22),
              child: Container(
                decoration: BoxDecoration(
                    color: const Color(0x622195F3),
                    borderRadius: BorderRadius.circular(12.0)),
                height: 160,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ShimmerDetailPayment extends StatelessWidget {
  const ShimmerDetailPayment({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.only(
          top: 5,
        ),
        child: Shimmer.fromColors(
          baseColor: const Color(0xFF3688F4),
          highlightColor: const Color(0xFF3BFFF5),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                decoration: const BoxDecoration(
                    color: Color(0x622195F3),
                    borderRadius: BorderRadius.all(Radius.circular(12))),
                height: 25,
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                margin: const EdgeInsets.only(top: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      decoration: const BoxDecoration(
                          color: Color(0x622195F3),
                          borderRadius: BorderRadius.all(Radius.circular(12))),
                      height: 25,
                      width: 65,
                    ),
                    Container(
                      decoration: const BoxDecoration(
                          color: Color(0x622195F3),
                          borderRadius: BorderRadius.all(Radius.circular(12))),
                      height: 25,
                      width: 105,
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 15, horizontal: 0),
                child: Container(
                  decoration: BoxDecoration(
                      color: const Color(0x622195F3),
                      borderRadius: BorderRadius.circular(12.0)),
                  height: 300,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ShimmerComment extends StatelessWidget {
  const ShimmerComment({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.only(
          top: 5,
        ),
        child: Shimmer.fromColors(
          baseColor: Colorconstands.gray,
          highlightColor: Colorconstands.darkGray.withOpacity(0.1),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                margin:
                    const EdgeInsets.only(left: 12.0, top: 12.0, bottom: 12.0),
                child: Row(
                  children: [
                    Container(
                      decoration: const BoxDecoration(
                          color: Colorconstands.gray, shape: BoxShape.circle),
                      height: 45,
                      width: 45,
                    ),
                    const SizedBox(
                      width: 12.0,
                    ),
                    Container(
                      decoration: const BoxDecoration(
                          color: Colorconstands.gray,
                          borderRadius: BorderRadius.all(Radius.circular(12))),
                      height: 75,
                      width: 150,
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 65, bottom: 12.0),
                child: Row(
                  children: [
                    Container(
                      decoration: const BoxDecoration(
                          color: Colorconstands.gray, shape: BoxShape.circle),
                      height: 45,
                      width: 45,
                    ),
                    const SizedBox(
                      width: 12.0,
                    ),
                    Container(
                      decoration: const BoxDecoration(
                          color: Colorconstands.gray,
                          borderRadius: BorderRadius.all(Radius.circular(12))),
                      height: 75,
                      width: 100,
                    ),
                  ],
                ),
              ),
              Container(
                margin:
                    const EdgeInsets.only(left: 12.0, top: 12.0, bottom: 12.0),
                child: Row(
                  children: [
                    Container(
                      decoration: const BoxDecoration(
                          color: Colorconstands.gray, shape: BoxShape.circle),
                      height: 45,
                      width: 45,
                    ),
                    const SizedBox(
                      width: 12.0,
                    ),
                    Container(
                      decoration: const BoxDecoration(
                          color: Colorconstands.gray,
                          borderRadius: BorderRadius.all(Radius.circular(12))),
                      height: 75,
                      width: 150,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ShimmerSchedule extends StatelessWidget {
  const ShimmerSchedule({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Color(0xFFF1F9FF),
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12),
          topRight: Radius.circular(12),
        ),
      ),
      child: Column(
        children: [
          Container(
            color: const Color(0x622195F3),
            height: 135,
          ),
          Expanded(
            child: ListView.builder(
              padding: const EdgeInsets.all(0),
              shrinkWrap: true,
              itemBuilder:
                  (BuildContext contex, index) {
                return const ShimmerTimeTable();
              },
              itemCount: 5,
            ),
          ),
        ],
      ),
    );
  }
}
class ShimmerTimeTableDask extends StatelessWidget {
  const ShimmerTimeTableDask({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(
        top: 5,
      ),
      child: Shimmer.fromColors(
        baseColor: const Color(0xFF3688F4),
        highlightColor: const Color(0xFF3BFFF5),
        child:Container(
              margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 22),
              child: Container(
                decoration: BoxDecoration(
                    color: const Color(0x622195F3),
                    borderRadius: BorderRadius.circular(12.0)),
                height: 90,
              ),
            ),
      ),
    );
  }
}

class ShimmerDask extends StatelessWidget {
  const ShimmerDask({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        color: Colorconstands.neutralWhite,
        child: Shimmer.fromColors(
          baseColor: const Color(0xFF3688F4),
          highlightColor: const Color(0xFF3BFFF5),
          child:Column(
            children: [
              Container(
                child: Container(
                  decoration: const BoxDecoration(
                      color: Color(0x622195F3),),
                  height: 65,
                ),
              ),
              const SizedBox(height: 25,),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 22),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: const Color(0x622195F3),
                          borderRadius: BorderRadius.circular(12.0)),
                      height: 20,width: 100,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: const Color(0x622195F3),
                          borderRadius: BorderRadius.circular(12.0)),
                      height: 20,width: 100,
                    ),
                  ],
                ),
              ),
              ListView.builder(
                physics:const ScrollPhysics(),
                shrinkWrap: true,
                itemCount: MediaQuery.of(context).size.width>1000? 7:3,
                itemBuilder: (context, snapshot) {
                  return Container(
                    margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 22),
                    child: Column(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              color: const Color(0x622195F3),
                              borderRadius: BorderRadius.circular(12.0)),
                          height: 90,
                        ),
                      ],
                    ),
                  );
                }
              ),
              const SizedBox(height: 25,),
              Container(
                 margin: const EdgeInsets.symmetric(horizontal: 22),
                child: Row(
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      decoration: BoxDecoration(
                          color: const Color(0x622195F3),
                          borderRadius: BorderRadius.circular(12.0)),
                      height: 20,width: 100,
                    ),
                  ],
                ),
              ),
              Container(
                 margin: const EdgeInsets.symmetric(horizontal: 22,vertical: 25),
                decoration: BoxDecoration(
                    color: const Color(0x622195F3),
                    borderRadius: BorderRadius.circular(12.0)),
                height: 155,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
