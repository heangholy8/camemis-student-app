import 'package:flutter/material.dart';

import '../../app/core/themes/themes.dart';
class CustomAppBarWidget extends StatelessWidget {
  final String title;
  final Widget? suffix;
  const CustomAppBarWidget({
    Key? key,
    required this.width,
    required this.title,
    this.suffix,
  }) : super(key: key);

  final double width;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: kToolbarHeight + 10,
      width: width,
      padding: const EdgeInsets.all(9),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: const Icon(
                  Icons.chevron_left,
                  size: 32,
                ),
              ),
              const SizedBox(
                width: 18,
              ),
              Text(
                title,
                style: ThemeConstands.headline3_SemiBold_20,
              )
            ],
          ),
          suffix ?? Container(),
        ],
      ),
    );
  }
}
