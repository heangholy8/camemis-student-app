import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:flutter/material.dart';

class Button_Custom extends StatelessWidget {
  String titleButton;
  double? hightButton;
  double? widthButton;
  double radiusButton;
  double maginRight;
  double maginleft;
  Color? titlebuttonColor;
  Color? buttonColor;
  VoidCallback? onPressed;

  Button_Custom(
      {Key? key,
      this.titleButton = "",
      this.onPressed,
      this.hightButton,
      this.widthButton,
      this.radiusButton = 0,
      this.maginRight = 0,
      this.maginleft = 0,
      this.buttonColor,
      this.titlebuttonColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widthButton,
      margin: EdgeInsets.only(right: maginRight, left: maginleft),
      child: MaterialButton(
        elevation: 0,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(radiusButton))),
        height: hightButton,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
              child: Text(
                titleButton,
                style: ThemeConstands.headline4_Medium_18.copyWith(color: titlebuttonColor))
            ),
          ],
        ),
        color: buttonColor,
        onPressed: onPressed,
      ),
    );
  }
}

class ButtonOutlineCustom extends StatelessWidget {
  String titleButton;
  double? hightButton;
  double? widthButton;
  double radiusButton;
  double maginRight;
  double maginleft;
  Color? titlebuttonColor;
  Color? buttonColor;
  Color? borderColor;
  VoidCallback? onPressed;
  Widget? child;
  double? sizeText;
  ButtonOutlineCustom(
      {Key? key,
      this.borderColor,
      this.titleButton = "",
      this.onPressed,
      this.hightButton,
      this.widthButton,
      this.radiusButton = 0,
      this.maginRight = 0,
      this.maginleft = 0,
      this.buttonColor,
      this.sizeText,
      this.titlebuttonColor,
      this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widthButton,
      margin: EdgeInsets.only(right: maginRight, left: maginleft),
      child: MaterialButton(
        shape: RoundedRectangleBorder(
            side: BorderSide(color: borderColor!),
            borderRadius: BorderRadius.all(Radius.circular(radiusButton))),
        height: hightButton,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            child!,
            Center(
                child: Text(
              titleButton,
              style: TextStyle(
                color: titlebuttonColor,
                fontSize: sizeText,
              ),
            )),
          ],
        ),
     //   color: buttonColor,
        onPressed: onPressed,
      ),
    );
  }
}

class CustomTextbutton extends StatelessWidget {
  VoidCallback? onPressed;
  String? title;
  double? size;
  Color? color;
  FontWeight? fontWeight;
  CustomTextbutton(
      {Key? key,
      this.color,
      required this.onPressed,
      required this.size,
      required this.title,
      this.fontWeight})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      child: Text(
        title!,
        style: TextStyle(fontSize: size, color: color, fontWeight: fontWeight),
      ),
    );
  }
}

class Button_Custom_with_icon extends StatelessWidget {
  String titleButton;
  double? hightButton;
  double? widthButton;
  double radiusButton;
  double maginRight;
  double maginleft;
  Color? titlebuttonColor;
  Color? buttonColor;
  VoidCallback? onPressed;
  Widget child;
  Button_Custom_with_icon(
      {Key? key,
      required this.child,
      this.titleButton = "",
      this.onPressed,
      this.hightButton,
      this.widthButton,
      this.radiusButton = 0,
      this.maginRight = 0,
      this.maginleft = 0,
      this.buttonColor,
      this.titlebuttonColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widthButton,
      margin: EdgeInsets.only(right: maginRight, left: maginleft),
      child: MaterialButton(
        elevation: 0,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(radiusButton))),
        height: hightButton,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            child,
            Center(
                child: Text(
              titleButton,
              style: TextStyle(color: titlebuttonColor, fontSize: 16),
            )),
          ],
        ),
        color: buttonColor,
        onPressed: onPressed,
      ),
    );
  }
}
