import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
class ButtonIconLeftWithTitle extends StatelessWidget {
  final double radiusButton;
  double speaceTitleAndImage;
  final double? heightButton;
  final double? weightButton;
  final double panddingVerButton;
  final TextStyle textStyleButton;
  final double panddinHorButton;
  final VoidCallback? onTap;
  final String title;
  final Color buttonColor;
  final Color iconColor;
  final String iconImage;
  ButtonIconLeftWithTitle({Key? key, required this.radiusButton, required this.onTap, required this.title, this.heightButton, this.weightButton, required this.panddingVerButton, required this.panddinHorButton, required this.textStyleButton, required this.buttonColor, required this.iconImage, required this.iconColor,this.speaceTitleAndImage = 6,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: weightButton,
      height: heightButton,
      child: MaterialButton(
        color: buttonColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(radiusButton))),
        padding:const EdgeInsets.all(0),
        onPressed: onTap,
        child: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(vertical: panddingVerButton,horizontal: panddinHorButton),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset(iconImage,color: iconColor),
              SizedBox(width: speaceTitleAndImage,),
              Text(title,style: textStyleButton,textAlign: TextAlign.center,),
            ],
          ),
        ),
      ),
    );
  }
}