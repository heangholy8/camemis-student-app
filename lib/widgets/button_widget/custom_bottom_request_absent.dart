import 'package:camis_application_flutter/storages/get_storage.dart';
import 'package:camis_application_flutter/widgets/button_widget/button_customwidget.dart';
import 'package:camis_application_flutter/widgets/model_bottomSheet.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../app/core/themes/color_app.dart';
import '../../app/core/themes/themes.dart';

class BottomRequestAbsent extends StatefulWidget {
  final BuildContext context;
  final Function navigateDay;
  final Function navigateTime;
  final bool checktimeline;
  const BottomRequestAbsent({
    Key? key,
    this.checktimeline = false,
    required this.width,
    required this.context,
    required this.navigateDay,
    required this.navigateTime,
  }) : super(key: key);

  final double width;

  @override
  State<BottomRequestAbsent> createState() => _BottomRequestAbsentState();
}

class _BottomRequestAbsentState extends State<BottomRequestAbsent> with TickerProviderStateMixin {
 int? selectChild;
  String studentID = "";
  String classID = "";
  String? activeDate;

   AnimationController? _controller;
   Animation<double>? _animation;

   @override
  void initState() {

    super.initState();
    setState(() {
      _controller = AnimationController(vsync:this,duration:const Duration(seconds: 3),);
      _animation =  CurvedAnimation(parent: _controller!, curve: Curves.easeInOut);
    });
    activeDate = DateFormat("dd/MM/yyyy").format(DateTime.now());
  } 
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(bottom: 15,top: 15.0),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Colors.white.withOpacity(0.5),
            Colors.white.withOpacity(1),
          ],
        ),
      ),
      child: GestureDetector(
        onTap:() {
        widget.checktimeline==false? showModalBottomSheet(
              isScrollControlled: true,
              backgroundColor: Colors.transparent,
              context: context,
              builder: (BuildContext contex) {
                return BuildBottomSheet(
                  initialChildSize: 0.5,
                  child: Container(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Column(
                      children: [
                        Center(
                          child: Text(
                            "SELECTYPRPERMISSION".tr(),
                            style: ThemeConstands.texttheme.headline6,
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10, horizontal: 70),
                          child: Center(
                              child: Text(
                            "Excepteur sint occaecat cupidatat non proident, sunt in culpa",
                            style: ThemeConstands.texttheme.subtitle2,
                            textAlign: TextAlign.center,
                          )),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Button_Custom(
                          titleButton: "Timetable",
                          buttonColor: Colorconstands.primaryColor,
                          radiusButton: 10,
                          onPressed: () {
                            Navigator.of(context).pop();
                            widget.navigateTime();
                          },
                          hightButton: 65,
                          maginRight: 20,
                          maginleft: 20,
                          titlebuttonColor: Colorconstands.white,
                        ),
                        Container(
                          margin:const EdgeInsets.symmetric(vertical: 12.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Expanded(
                                child: Divider(
                                  indent: 20,
                                  endIndent: 10,
                                  thickness: 1,
                                  height: 10,
                                  color: Colorconstands.darkGray
                                      .withOpacity(0.09),
                                ),
                              ),
                              Text(
                                "or",
                                style: TextStyle(
                                    color: Colorconstands.darkGray
                                        .withOpacity(0.5)),
                              ),
                              Expanded(
                                child: Divider(
                                  indent: 10,
                                  endIndent: 20,
                                  thickness: 1,
                                  height: 10,
                                  color: Colorconstands.darkGray
                                      .withOpacity(0.09),
                                ),
                              )
                            ],
                          ),
                        ),
                        ButtonOutlineCustom(
                          titleButton: "Day",
                          buttonColor: Colorconstands.white,
                          radiusButton: 10,
                          onPressed: () {
                            Navigator.of(context).pop();
                            widget.navigateDay();
                          },
                          hightButton: 65,
                          maginRight: 20,
                          maginleft: 20,
                          titlebuttonColor: Colorconstands.primaryColor,
                          borderColor: Colorconstands.primaryColor,
                          sizeText: 16,
                          widthButton: MediaQuery.of(context).size.width,
                          child: const SizedBox(),
                        ),
                      ],
                    ),
                  ),
                  expanded: const SizedBox(),
                );
              }):widget.navigateTime();
        },
        child: Center(
          child: Container(
          padding:const EdgeInsets.symmetric(vertical: 8.0,horizontal: 12.0),
           decoration:const BoxDecoration(
            color: Colorconstands.primaryColor,
            borderRadius: BorderRadius.all(Radius.circular(22.0))
           ),
           child: Row(
            mainAxisSize: MainAxisSize.min,
             mainAxisAlignment: MainAxisAlignment.center,
             children:[
              const Icon(
                 Icons.add,
                 size: 32,
                 color: Colors.white,
               ),
               const SizedBox(width: 10.0,),
               Text(
                "ASKPERMISSION".tr(),
                style:const TextStyle(
                  fontSize: 18,
                  color: Colorconstands.white,
                ),
              ),
             ],
           ),
              ),
        ),
      ),
    );
  }

  // void getlocaldataModel() async {
  //   GetStoragePref _pref = GetStoragePref();
  //   var selectedChild = await _pref.getSelectedChild();
  //   var childId = await _pref.getChildId();
  //   var classid = await _pref.getClassId();
  //   setState(() {
  //     selectChild = selectedChild;
  //     studentID = childId.toString();
  //     classID = classid.toString();
  //   });
  // }
}
