import 'package:flutter/material.dart';
import '../../app/core/themes/color_app.dart';
class ButtonWidgetCustom extends StatelessWidget {
  final double radiusButton;
  final double? heightButton;
  final double? weightButton;
  final double panddingVerButton;
  TextStyle? textStyleButton;
  final double panddinHorButton;
  final VoidCallback? onTap;
   String? title;
   Color? buttonColor;
  ButtonWidgetCustom({Key? key, required this.radiusButton, required this.onTap, required this.title, this.heightButton, this.weightButton, required this.panddingVerButton, required this.panddinHorButton, required this.textStyleButton, required this.buttonColor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: weightButton,
      height: heightButton,
      child: MaterialButton(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        elevation: 0,
        color: buttonColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(radiusButton))),
        padding:const EdgeInsets.all(0),
        onPressed: onTap,
        child: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(vertical: panddingVerButton,horizontal: panddinHorButton),
          child: Text(title!,style: textStyleButton,textAlign: TextAlign.center,),
        ),
      ),
    );
  }
}