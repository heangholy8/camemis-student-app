import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../app/core/themes/color_app.dart';
class ButtonSettingWidget extends StatelessWidget {
  final double radiusButton;
  final double? heightButton;
  final double? weightButton;
  final double panddingVerButton;
  final TextStyle textStyleButton;
  final double panddinHorButton;
  final VoidCallback? onTap;
  final String title;
  final Color buttonColor;
  final String iconImageleft;
  final String iconImageright;
  const ButtonSettingWidget({Key? key, required this.radiusButton, required this.onTap, required this.title, this.heightButton, this.weightButton, required this.panddingVerButton, required this.panddinHorButton, required this.textStyleButton, required this.buttonColor, required this.iconImageleft, required this.iconImageright,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(radiusButton),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.1),
            spreadRadius: 0.5,
            blurRadius: 9,
            offset:const Offset(2, 2),
          )
        ]
      ),
      width: weightButton,
      height: heightButton,
      child: MaterialButton(
        elevation: 0,
        color: buttonColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(radiusButton))),
        padding:const EdgeInsets.all(0),
        onPressed: onTap,
        child: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(vertical: panddingVerButton,horizontal: panddinHorButton),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                padding:const EdgeInsets.all(4),
                decoration: BoxDecoration(
                  color: Colorconstands.mainColorForecolor.withOpacity(0.25),
                  shape: BoxShape.circle
                ),
                child: SvgPicture.asset(iconImageleft,color: Colorconstands.mainColorSecondary,width: 24,height: 24,)),
              const SizedBox(width: 12,),
              Expanded(child: Text(title,style: textStyleButton,textAlign: TextAlign.left,)),
              const SizedBox(width: 12,),
              SvgPicture.asset(iconImageright,color: Colorconstands.darkTextsRegular,),
            ],
          ),
        ),
      ),
    );
  }
}