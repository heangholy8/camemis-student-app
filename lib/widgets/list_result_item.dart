// import 'package:camis_application_flutter/model/result/semester_result.dart';
// import 'package:camis_application_flutter/widgets/custom_list_semester_result.dart';
// import 'package:camis_application_flutter/widgets/reuslt_item.dart';
// import 'package:easy_localization/easy_localization.dart';
// import 'package:flutter/cupertino.dart';
// import 'custom_list_header.dart';
// class ListResultItem extends StatefulWidget {
//   TermData data;
//   ListResultItem({Key? key, required this.data}) : super(key: key);

//   @override
//   State<ListResultItem> createState() => _ListResultItemState();
// }

// class _ListResultItemState extends State<ListResultItem> {
//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       children: [
//         CustomListHeader(
//           isMonth: false,
//         ),
//         CustomListListSemesterResult(
//           data: widget.data,
//         ),
//         widget.data.termResult != null
//             ? Column(
//                 children: [
//                   ResultItem(
//                     title: "SEMESTER_EXAM_RESULT".tr(),
//                     score: widget.data.termExamResult!.totalScore!.toString() !=
//                                 "null" ||
//                             widget.data.termExamResult!.totalScore!
//                                     .toString() !=
//                                 ""
//                         ? widget.data.termExamResult!.totalScore!.toString()
//                         : "- - -",
//                     rank: widget.data.termExamResult!.rank!.toString() !=
//                                 "null" ||
//                             widget.data.termExamResult!.rank!.toString() != ""
//                         ? widget.data.termExamResult!.rank!.toString()
//                         : "- - -",
//                   ),
//                   ResultItem(
//                       title: "AVERAGE_SERMESTER_EXAM_SCORE".tr(),
//                       score: widget.data.termExamResult!.avgScore!.toString() !=
//                                   "null" ||
//                               widget.data.termExamResult!.avgScore!
//                                       .toString() !=
//                                   ""
//                           ? widget.data.termExamResult!.avgScore!.toString()
//                           : "- - -",
//                       rank: "0"),
//                   ResultItem(
//                     title: "AVERAGE_MONTHLY_SCORE".tr(),
//                     score: widget.data.termResult!.monthAvg!.toString() !=
//                                 "null" ||
//                             widget.data.termResult!.monthAvg.toString() != ""
//                         ? widget.data.termResult!.monthAvg!.toString()
//                         : "- - -",
//                     rank: widget.data.termResult!.rank!.toString() != "null" ||
//                             widget.data.termResult!.rank!.toString() != ""
//                         ? widget.data.termResult!.rank!.toString()
//                         : "- - -",
//                   ),
//                 ],
//               )
//             : Column(
//                 mainAxisSize: MainAxisSize.max,
//                 children: [
//                   ResultItem(
//                     title:"SEMESTER_EXAM_RESULT".tr(),
//                     score: "- - -",
//                     rank: "- - -",
//                   ),
//                   ResultItem(
//                       title: "AVERAGE_SERMESTER_EXAM_SCORE".tr(),
//                       score: "- - -",
//                       rank: "0"),
//                   ResultItem(
//                     title: "AVERAGE_MONTHLY_SCORE".tr(),
//                     score: "- - -",
//                     rank: "- - -",
//                   ),
//                 ],
//               ),
//       ],
//     );
//   }
// }
