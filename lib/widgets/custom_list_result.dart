import 'package:camis_application_flutter/app/core/resources/asset_resource.dart';
import 'package:camis_application_flutter/model/result/monthly_result.dart';
import 'package:camis_application_flutter/service/apis/result_api/get_monthly_result.dart';
import 'package:easy_localization/easy_localization.dart';
import '../app/modules/auth_screens/e_school_code.dart';
class CustomListResult extends StatefulWidget {
  bool isMonth = true;
  MonthlyData data;
  CustomListResult({
    Key? key,
    this.isMonth = true,
    required this.data,
  }) : super(key: key);

  @override
  State<CustomListResult> createState() => _CustomListResultState();
}

class _CustomListResultState extends State<CustomListResult> {
  final MonthlyResultApi monthlyResultApi = MonthlyResultApi();
  @override
  void initState() {
    // resultModel = monthlyResultApi.getMonthlyResultApi(
    //     studenId: "38551833374350007371",
    //     classId: "210",
    //     month: "1",
    //     term: "FIRST_SEMESTER");
    // print("Result Model: ${resultModel.toString()}");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Container(
      child: ListView.builder(
        padding:const EdgeInsets.all(0),
        shrinkWrap: true,
        physics: const ScrollPhysics(),
        itemCount: widget.data.subject!.length,
        itemBuilder: (context, index) {
          return Column(
            children: [
              Container(
                padding:const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                width: MediaQuery.of(context).size.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Container(
                        child: Center(
                          child: Text(
                            (index + 1).toString(),
                            style: ThemeConstands.texttheme.subtitle2,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                           translate=="en"? widget.data.subject![index].nameEn.toString():widget.data.subject![index].name.toString(),
                            textAlign: TextAlign.left,
                            style: ThemeConstands.texttheme.subtitle2,
                          ),
                        ),
                      ),
                    ),
                    widget.isMonth
                        ? Expanded(
                            child: Container(
                              child: Center(
                                child: Text(
                                  widget.data.subject![index].score
                                              .toString() ==
                                          "null"
                                      ? "0/${widget.data.subject![index].maxScore.toString()}"
                                      : widget.data.subject![index]
                                                  .absentExam !=
                                              1
                                          ? "${widget.data.subject![index].score.toString()}" +
                                              "/" +
                                              "${widget.data.subject![index].maxScore.toString()}"
                                          : "NORANK".tr(),
                                  style: ThemeConstands.texttheme.subtitle2
                                      ?.copyWith(
                                          color: widget.data.subject![index]
                                                      .absentExam !=
                                                  1
                                              ? Colors.black
                                              : Colors.red),
                                ),
                              ),
                            ),
                          )
                        :const SizedBox(),
                    widget.isMonth
                        ? Expanded(
                            child: Container(
                              child: Center(
                                child: Text(
                                  widget.data.subject![index].letterGrade.toString() =="null" ||
                                          widget.data.subject![index].gradingEn.toString() ==
                                              "null"
                                      ? "- - -"
                                      :widget.data.subject![index].letterGrade.toString()+ translate =="en"?"":
                                          "(" +
                                          widget.data.subject![index]
                                              .gradingEn
                                              .toString() +
                                          ")",
                                  textAlign: TextAlign.center,
                                  style: ThemeConstands.texttheme.subtitle2
                                      ?.copyWith(
                                          color: widget.data.subject![index]
                                                      .gradingEn
                                                      .toString() ==
                                                  "F"
                                              ? Colors.red
                                              : Colorconstands.primaryColor),
                                ),
                              ),
                            ),
                          )
                        :const SizedBox(),
                    widget.isMonth
                        ?const SizedBox()
                        : Expanded(
                            child: Container(
                              child: Center(
                                child: Text(
                                    "${widget.data.subject![index].score.toString()}",
                                    style: ThemeConstands.texttheme.subtitle2),
                              ),
                            ),
                          ),
                    Expanded(
                      child: Container(
                        child: Center(
                          child: Text(
                            widget.data.subject![index].rank.toString() ==
                                    "null"
                                ? "- - -"
                                : "${widget.data.subject![index].rank.toString()}" +
                                    "/" +
                                    "${widget.data.totalStudent.toString()}",
                            style: ThemeConstands.texttheme.subtitle2?.copyWith(
                                color: widget.data.subject![index].grading
                                            .toString() ==
                                        "F"
                                    ? Colors.red
                                    : Colorconstands.primaryColor),
                          ),
                        ),
                      ),
                    ),
                    SvgPicture.asset(ImageAssets.comment)
                  ],
                ),
              ),
              const Divider(
                indent: 25,
                endIndent: 15,
                thickness: 1,
              )
            ],
          );
        },
      ),
    );
  }

  Widget _buildLoading() => Container(
      margin: const EdgeInsets.only(right: 10.0),
      child: Center(
          child: Image.asset(
        "assets/images/gifs/loading.gif",
        height: 45,
        width: 45,
      )));
}
