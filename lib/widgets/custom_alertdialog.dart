import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'button_widget/button_customwidget.dart';

// ignore: must_be_immutable
class CustomAlertDialog extends StatelessWidget {
  final String title;
  final String subtitle;
  String titleButton;
  double? widthButton;
  Color? titlebuttonColor;
  Color? buttonColor;
  VoidCallback? onPressed;
  Color? blurColor;
  Widget child;
  CustomAlertDialog(
      {Key? key,
      required this.child,
      required this.title,
      required this.subtitle,
      this.buttonColor,
      this.onPressed,
      required this.titleButton,
      this.titlebuttonColor,
      this.blurColor,
      this.widthButton})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 5,
      backgroundColor: Colorconstands.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: SizedBox(
        width: MediaQuery.of(context).size.width / 2,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: const EdgeInsets.only(top: 25),
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width / 6,
              height: MediaQuery.of(context).size.width / 6,
              decoration: BoxDecoration(
                  color: blurColor??Colorconstands.errorColor.withOpacity(0.1),
                  shape: BoxShape.circle),
              child: Container(
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: child,
              ),
            ),
            Container(
                margin: const EdgeInsets.only(top: 35, left: 17.0, right: 17.0),
                child: Align(
                  child: Text(
                    title,
                    style: ThemeConstands.texttheme.headline5!.copyWith(
                        color: Colorconstands.black,
                        fontWeight: FontWeight.w600),
                    textAlign: TextAlign.center,
                  ),
                )),
            Container(
              margin: const EdgeInsets.only(top: 8, left: 17.0, right: 17.0),
              child: Align(
                child: Text(
                  subtitle,
                  style: ThemeConstands.texttheme.subtitle1!.copyWith(
                      color: Colorconstands.darkGray,
                      fontWeight: FontWeight.w500),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 25, bottom: 25),
              child: Button_Custom(
                onPressed: onPressed,
                buttonColor: buttonColor,
                hightButton: 45,
                radiusButton: 12,
                maginRight: 17.0,
                maginleft: 17.0,
                titleButton: titleButton,
                titlebuttonColor: titlebuttonColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class CustomAlertDialog_Profile extends StatelessWidget {
  final String title;
  final String subtitle;
  String titleButton;
  double? widthButton;
  Color? titlebuttonColor;
  Color? buttonColor;
  VoidCallback? onPressed;
  Widget child;
  String? profileGuidian;
  CustomAlertDialog_Profile(
      {Key? key,
      required this.profileGuidian,
      required this.child,
      required this.title,
      required this.subtitle,
      this.buttonColor,
      this.onPressed,
      required this.titleButton,
      this.titlebuttonColor,
      this.widthButton})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 5,
      backgroundColor: Colorconstands.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width / 2,
        child: Stack(
          children: [
            Positioned(
                top: 0,
                left: 0,
                right: 0,
                child: Container(
                  decoration: const BoxDecoration(
                      color: Colorconstands.primaryColor,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(15.0),
                          topRight: Radius.circular(15.0))),
                  alignment: Alignment.centerLeft,
                  width: MediaQuery.of(context).size.width,
                  child: ClipRRect(
                      borderRadius:
                          const BorderRadius.only(topLeft: Radius.circular(15)),
                      child: SvgPicture.asset(
                        "assets/images/svg/editprofile_popup_background.svg",
                        height: MediaQuery.of(context).size.width / 4.5,
                      )),
                )),
            Container(
              margin:
                  EdgeInsets.only(top: MediaQuery.of(context).size.width / 12),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      margin: const EdgeInsets.all(5),
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colorconstands.secondaryColor),
                      height: MediaQuery.of(context).size.width / 4,
                      width: MediaQuery.of(context).size.width / 4,
                      child: CircleAvatar(
                        backgroundColor: Colorconstands.white,
                        radius: 56,
                        child: Padding(
                          padding: const EdgeInsets.all(3), // Border radius
                          child: ClipOval(
                              child: Image.network(
                            profileGuidian.toString(),
                            height: MediaQuery.of(context).size.width / 4,
                            width: MediaQuery.of(context).size.width / 4,
                            fit: BoxFit.cover,
                          )),
                        ),
                      )),
                  Container(
                      margin: const EdgeInsets.only(
                          top: 10, left: 17.0, right: 17.0),
                      child: Align(
                        child: Text(
                          title,
                          style: ThemeConstands.texttheme.headline6!
                              .copyWith(color: Colorconstands.black),
                          textAlign: TextAlign.center,
                        ),
                      )),
                  Container(
                      margin: const EdgeInsets.only(
                          top: 8, left: 17.0, right: 17.0),
                      child: Align(
                        child: Text(
                          subtitle,
                          style: ThemeConstands.texttheme.subtitle2!
                              .copyWith(color: Colorconstands.darkGray),
                          textAlign: TextAlign.center,
                        ),
                      )),
                  Container(
                    margin: const EdgeInsets.only(top: 25, bottom: 25),
                    child: Button_Custom_with_icon(
                      onPressed: onPressed,
                      buttonColor: buttonColor,
                      hightButton: 40,
                      radiusButton: 22,
                      widthButton: 150,
                      titleButton: titleButton,
                      titlebuttonColor: titlebuttonColor,
                      child: Container(
                        margin: const EdgeInsets.only(right: 5),
                        child: const Icon(
                          Icons.edit_outlined,
                          size: 20,
                          color: Colorconstands.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class CustomAlertDialog_Guide extends StatelessWidget {
  final String title;
  final String subtitle;
  String titleButton;
  double? widthButton;
  Color? titlebuttonColor;
  Color? buttonColor;
  VoidCallback? onPressed;
  VoidCallback? onPressedSkip;
  Widget child;

  CustomAlertDialog_Guide(
      {Key? key,
      required this.onPressedSkip,
      required this.child,
      required this.title,
      required this.subtitle,
      this.buttonColor,
      this.onPressed,
      required this.titleButton,
      this.titlebuttonColor,
      this.widthButton})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 5,
      backgroundColor: Colorconstands.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width / 1,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: const EdgeInsets.only(top: 25),
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width / 6,
              height: MediaQuery.of(context).size.width / 6,
              decoration: const BoxDecoration(
                  color: Color.fromARGB(178, 158, 213, 226),
                  shape: BoxShape.circle),
              child: Container(
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: child,
              ),
            ),
            Container(
                margin: const EdgeInsets.only(top: 35, left: 17.0, right: 17.0),
                child: Align(
                  child: Text(
                    title,
                    style: ThemeConstands.texttheme.headline5!.copyWith(
                        color: Colorconstands.black,
                        fontWeight: FontWeight.w600),
                    textAlign: TextAlign.center,
                  ),
                )),
            Container(
              margin: const EdgeInsets.only(top: 8, left: 17.0, right: 17.0),
              child: Align(
                child: Text(
                  subtitle,
                  style: ThemeConstands.texttheme.subtitle1!.copyWith(
                      color: Colorconstands.darkGray,
                      fontWeight: FontWeight.w500),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 25),
              child: Button_Custom(
                onPressed: onPressed,
                buttonColor: buttonColor,
                hightButton: 45,
                radiusButton: 14,
                maginRight: 17.0,
                maginleft: 17.0,
                titleButton: titleButton,
                titlebuttonColor: titlebuttonColor,
              ),
            ),
            Container(
                margin: const EdgeInsets.only(top: 15, bottom: 25),
                child: CustomTextbutton(
                  onPressed: onPressedSkip,
                  size: 18,
                  title: 'Skip',
                )),
          ],
        ),
      ),
    );
  }
}
