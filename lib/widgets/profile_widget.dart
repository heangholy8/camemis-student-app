import 'package:camis_application_flutter/app/core/themes/color_app.dart';
import 'package:flutter/material.dart';

class Profile extends StatelessWidget {
  String imageProfile;
  double height;
  double width;
  double pandding;
  VoidCallback onPressed;
  String namechild;
  double sizetextname;
   Profile({ Key? key,required this.imageProfile,required this.height,required this.sizetextname,required this.namechild,required this.width,required this.pandding,required this.onPressed }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(pandding),
      height: height,
      width: width,
      decoration:const BoxDecoration(
            color: Colorconstands.white,
            shape: BoxShape.circle
          ),
      child: MaterialButton(
        color: Colorconstands.secondaryColor,
        shape:const CircleBorder(),
        padding:const EdgeInsets.all(0),
        onPressed: onPressed,
        child: Stack(
          children: [
             Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(5),
              height: height,
              width: width,
              child: Text(namechild,style: TextStyle(color: Colorconstands.white,fontSize:sizetextname))),
            imageProfile!=""?Container(
              height: height,
              width: width,
              child: CircleAvatar(backgroundImage: NetworkImage(imageProfile),backgroundColor: Colors.transparent,))
              :Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(5),
              height: height,
              width: width,
              child: Text(namechild,style: TextStyle(color: Colorconstands.white,fontSize:sizetextname))),

          ],
        )
      ),
    );
  }
}