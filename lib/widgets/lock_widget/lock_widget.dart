import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter_svg/svg.dart';
import '../../app/core/resources/asset_resource.dart';

class LockWidget extends StatelessWidget {
  const LockWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 90,
      height: 30,
      decoration: BoxDecoration(
        color:const Color(0x2A82A0C9),
        borderRadius: BorderRadius.circular(8)
      ),
      child:DottedBorder(
        borderType: BorderType.RRect,
          radius:const Radius.circular(8),
          dashPattern:const [5, 5],
          color:const Color(0xFF5D90BC),
          strokeWidth: 1,
          child: Center(child: SvgPicture.asset(ImageAssets.lockIcon)),
      )
    );
  }
}