import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'model_bottomSheet.dart';


void showtoastInternet(context){
  showModalBottomSheet(
    backgroundColor: Colors.transparent,
    isScrollControlled: true,
    context: context,
    isDismissible: true,
    builder: (context) =>BuildBottomSheet(
      initialChildSize: 0.45,
      child:Container(
        margin:const EdgeInsets.only(top: 18.0),
       child: Image.asset(
          "assets/images/gifs/no_connection.gif",
          width: MediaQuery.of(context).size.width/3,
          height: MediaQuery.of(context).size.height/3,
        ),
      ),
      expanded: Container(
        alignment: Alignment.center,
        child: Text("NOINTERNET".tr()+"!",style: ThemeConstands.texttheme.headline6),
      ),
      
    ),
  );
}