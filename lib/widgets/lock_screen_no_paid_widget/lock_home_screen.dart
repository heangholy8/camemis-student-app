import 'package:blurrycontainer/blurrycontainer.dart';
import 'package:camis_application_flutter/app/modules/auth_screens/e_school_code.dart';
import '../../app/core/resources/asset_resource.dart';
import '../../app/routes/app_routes.dart';
import '../button_widget/button_customwidget.dart';

class LockScreenUnpaidHomeScreenWidget extends StatelessWidget {
  final String title ,subtitle;
  const LockScreenUnpaidHomeScreenWidget({Key? key, required this.title, required this.subtitle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 60,
      bottom: 0,
      left: 0,
      right: 0,
      child: Container(
        margin: const EdgeInsets.only(top: 0),
        color: Colorconstands.mainColorSecondary.withOpacity(0.1),
        child: BlurryContainer(
          blur: 4,
          elevation: 0,
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(ImageAssets.lock),
                Text(
                  title,
                  style: ThemeConstands.headline5_SemiBold_16
                              .copyWith(fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                ),
                const SizedBox(
                  height: 15,
                ),
                Text(
                  subtitle,
                  textAlign: TextAlign.center,
                          style: ThemeConstands.headline6_Regular_14_20height
                              .copyWith(fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
        ),
       
      ),
    );
  }
}