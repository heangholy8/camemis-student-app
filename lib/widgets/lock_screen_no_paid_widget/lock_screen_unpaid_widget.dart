import 'package:blurrycontainer/blurrycontainer.dart';
import 'package:camis_application_flutter/app/modules/auth_screens/e_school_code.dart';
import '../../app/core/resources/asset_resource.dart';

class LockScreenUnpaidWidget extends StatelessWidget {
  final String title ,subtitle;
  const LockScreenUnpaidWidget({Key? key, required this.title, required this.subtitle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 90,
      bottom: 0,
      left: 0,
      right: 0,
      child: Container(
        margin: const EdgeInsets.only(top: 10),
        color: Colorconstands.mainColorSecondary.withOpacity(0.1),
        child: BlurryContainer(
          blur: 4,
          elevation: 0,
          padding: const EdgeInsets.only(top: 0),
          child:  Container(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
               Container(
                child: Image.asset(ImageAssets.lock,width: 160,)),
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(top: 40),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          title,
                          style: ThemeConstands.headline5_SemiBold_16
                              .copyWith(fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        Text(
                          subtitle,
                          textAlign: TextAlign.center,
                          style: ThemeConstands.headline6_Regular_14_20height
                              .copyWith(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
       
      ),
    );
  }
}