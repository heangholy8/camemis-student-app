import 'package:flutter/material.dart';

class EmptyWidget extends StatelessWidget {
  const EmptyWidget._();

  static EmptyWidget? _emptyWidget;
  static EmptyWidget get instance {
    if (_emptyWidget == null) {
      return _emptyWidget = const EmptyWidget._();
    } else {
      return _emptyWidget!;
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return const SizedBox.shrink();
  }
}
