import 'package:camis_application_flutter/widgets/cached_image.dart';
import 'package:flutter/material.dart';

import '../app/core/themes/color_app.dart';

class PreviewImageNetwork extends StatefulWidget {
  final List listimage;
  late int? activepage;
  PreviewImageNetwork({Key? key, required this.listimage, required this.activepage}) : super(key: key);

  @override
  State<PreviewImageNetwork> createState() => _PreviewImageNetworkState();
}

class _PreviewImageNetworkState extends State<PreviewImageNetwork> {
  @override
  Widget build(BuildContext context) {
    PageController pagec = PageController(initialPage:widget.listimage.length==1?0:widget.activepage!);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        actions: [
          widget.listimage.length==1?Container(): Container(
            margin:const EdgeInsets.only(top: 15,right: 18),
            child: Text((widget.activepage! +1).toString()+" / "+widget.listimage.length.toString(),style:const TextStyle(color: Colorconstands.white,fontSize: 18.0),),
          ),
        ]
      ),
      backgroundColor: Colorconstands.black,
      body: SafeArea(
        bottom: false,
        child: Container(
          color: Colors.black,
          child: PageView.builder(
            controller: pagec,
            itemCount: widget.listimage.length,
            onPageChanged: (index){
              setState(() {
                widget.activepage=index;
              });
            },
            itemBuilder: (context, index) {
              return Center(
                child: InteractiveViewer(
                  clipBehavior: Clip.none,
                  child: ClipRRect(
                    child: CachedImageNetwork(urlImage: widget.listimage[index].fileThumbnail),
                  ),
                ),
              );
            }
          ),
        ),
      ),
    );
  }
}