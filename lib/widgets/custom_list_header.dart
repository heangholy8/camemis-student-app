import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../app/core/themes/color_app.dart';
import '../app/core/themes/themes.dart';

class CustomListHeader extends StatelessWidget {
  bool isMonth;
  CustomListHeader({Key? key, this.isMonth = true}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding:const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      width: MediaQuery.of(context).size.width,
      color: Colorconstands.iconColor.withOpacity(0.1),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Container(
              child: Center(
                child: Text(
                  "NO".tr(),
                  style: ThemeConstands.texttheme.subtitle1
                      ?.copyWith(fontWeight: FontWeight.w700),
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              padding:const EdgeInsets.only(left: 0),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "SUBJECT".tr(),
                  style: ThemeConstands.texttheme.subtitle1
                      ?.copyWith(fontWeight: FontWeight.w700),
                ),
              ),
            ),
          ),
          isMonth
              ? Expanded(
                  child: Container(
                    padding:const EdgeInsets.only(left: 2),
                    child: Center(
                      child: Text(
                        "SCORE".tr(),
                        style: ThemeConstands.texttheme.subtitle1
                            ?.copyWith(fontWeight: FontWeight.w700),
                      ),
                    ),
                  ),
                )
              :const SizedBox(),
          isMonth
              ? Expanded(
                  child: Container(
                    padding:const EdgeInsets.only(left: 3),
                    child: Center(
                      child: Text(
                        "GRADE".tr(),
                        style: ThemeConstands.texttheme.subtitle1
                            ?.copyWith(fontWeight: FontWeight.w700),
                      ),
                    ),
                  ),
                )
              : const SizedBox(),
          isMonth
              ? const SizedBox()
              : Expanded(
                  child: Container(
                    padding:const EdgeInsets.only(left: 3),
                    child: Center(
                      child: Text(
                        "AVERAGE".tr(),
                        style:  ThemeConstands.texttheme.subtitle1
                            ?.copyWith(fontWeight: FontWeight.w700),
                      ),
                    ),
                  ),
                ),
          Expanded(
            child: Container(
              padding:const EdgeInsets.only(left: 0),
              child: Center(
                child: Tooltip(
                  message: "RANK".tr(),
                  child: Text(
                    "RANK".tr(),
                    overflow: TextOverflow.ellipsis,
                    style: ThemeConstands.texttheme.subtitle1
                        ?.copyWith(fontWeight: FontWeight.w700),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
