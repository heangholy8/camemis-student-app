import '../app/modules/auth_screens/e_school_code.dart';
import '../app/modules/result_screen/data/result_datas.dart';

class CustomListResultFake extends StatefulWidget {
  bool isMonth = true;
  CustomListResultFake({
    Key? key,
    this.isMonth = true,
  }) : super(key: key);

  @override
  State<CustomListResultFake> createState() => _CustomListResultFakeState();
}

class _CustomListResultFakeState extends State<CustomListResultFake> {
  @override
  void initState() {
    // resultModel = monthlyResultApi.getMonthlyResultApi(
    //     studenId: "38551833374350007371",
    //     classId: "210",
    //     month: "1",
    //     term: "FIRST_SEMESTER");
    // print("Result Model: ${resultModel.toString()}");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        padding: EdgeInsets.all(0),
        shrinkWrap: true,
        physics: const ScrollPhysics(),
        itemCount: resultDatas.length,
        itemBuilder: (context, index) {
          return Column(
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                width: MediaQuery.of(context).size.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Container(
                        child: Center(
                          child: Text(
                            (index + 1).toString(),
                            style: ThemeConstands.texttheme.subtitle2,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            resultDatas[index].subjectName.toString(),
                            textAlign: TextAlign.left,
                            style: ThemeConstands.texttheme.subtitle2,
                          ),
                        ),
                      ),
                    ),
                    widget.isMonth
                        ? Expanded(
                            child: Container(
                            child: Center(
                              child: Text(
                                resultDatas[index].tottalscore.toString(),
                                style: ThemeConstands.texttheme.subtitle2
                                    ?.copyWith(color: Colors.black),
                              ),
                            ),
                          ))
                        : SizedBox(),
                    widget.isMonth
                        ? Expanded(
                            child: Container(
                              child: Center(
                                child: Text(
                                  resultDatas[index].gradeKH.toString(),
                                  textAlign: TextAlign.center,
                                  style: ThemeConstands.texttheme.subtitle2
                                      ?.copyWith(
                                          color: Colorconstands.primaryColor),
                                ),
                              ),
                            ),
                          )
                        : SizedBox(),
                    widget.isMonth
                        ? SizedBox()
                        : Expanded(
                            child: Container(
                              child: Center(
                                child: Text(resultDatas[index].score.toString(),
                                    style: ThemeConstands.texttheme.subtitle2),
                              ),
                            ),
                          ),
                    Expanded(
                      child: Container(
                        child: Center(
                          child: Text(
                            resultDatas[index].rank.toString() +
                                "/" +
                                resultDatas[index].totalStudent.toString(),
                            style: ThemeConstands.texttheme.subtitle2
                                ?.copyWith(color: Colorconstands.primaryColor),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const Divider(
                indent: 25,
                endIndent: 15,
                thickness: 1,
              )
            ],
          );
        },
      ),
    );
  }

  Widget _buildLoading() => Container(
      margin: const EdgeInsets.only(right: 10.0),
      child: Center(
          child: Image.asset(
        "assets/images/gifs/loading.gif",
        height: 45,
        width: 45,
      )));
}
