import 'package:camis_application_flutter/app/core/themes/themes.dart';
import 'package:camis_application_flutter/app/modules/time_line/school_time_line/e.schooltimeline.dart';
import 'package:flutter/material.dart';

import '../button_widget/button_customwidget.dart';

class ErrorRequestData extends StatefulWidget {
  final String title;
  final String discription;
  final bool hidebutton;
  final VoidCallback onPressed;
  const ErrorRequestData({Key? key, required this.title, required this.discription, required this.hidebutton, required this.onPressed}) : super(key: key);

  @override
  State<ErrorRequestData> createState() => _ErrorRequestDataState();
}

class _ErrorRequestDataState extends State<ErrorRequestData> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin:const EdgeInsets.all(22),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(widget.title,style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.lightBlack),textAlign: TextAlign.center,),
            Text(widget.discription,style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.lightBlack),),
            widget.hidebutton==false?Container():Container(
              margin: const EdgeInsets.only(top: 25, bottom: 25),
              child: Button_Custom(
                onPressed: widget.onPressed,
                buttonColor: Colorconstands.primaryColor,
                hightButton: 45,
                radiusButton: 12,
                maginRight: 17.0,
                maginleft: 17.0,
                titleButton: "TRY_AGAIN".tr(),
                titlebuttonColor: Colorconstands.neutralWhite,
              ),
            ),
          ],
        ),
      ),
    );
  }
}