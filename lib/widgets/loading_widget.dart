import 'package:flutter/material.dart';

class LoadingWidget extends StatelessWidget {
  const LoadingWidget._();

  static LoadingWidget? _loadingWidget;
  static LoadingWidget get instance {
    if (_loadingWidget == null) {
      return _loadingWidget = const LoadingWidget._();
    } else {
      return _loadingWidget!;
    }
  }

  @override
  Widget build(BuildContext context) {
    return const Center(child: CircularProgressIndicator.adaptive());
  }
}
