import 'dart:async';

import 'package:camis_application_flutter/app/routes/e.route.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

class CustomHeaderWithImage extends StatefulWidget {
  final String? title;
  final Widget? changechild;
  const CustomHeaderWithImage({
    Key? key,
    required this.title, this.changechild,
  }) : super(key: key);

  @override
  State<CustomHeaderWithImage> createState() => _CustomHeaderWithImageState();
}

class _CustomHeaderWithImageState extends State<CustomHeaderWithImage> {
  StreamSubscription? stsub;
  bool connection = true;
  //=================
  @override
  void initState() {
    super.initState();
    //=============Check internet====================
    stsub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
      });
    });
   
   //=============Check internet====================
  }

  @override
  void dispose() {
    stsub?.cancel();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          margin: const EdgeInsets.only(left: 8,right: 8),
          child: IconButton(icon:const Icon(Icons.arrow_back_ios_new_outlined,size: 25,color: Colorconstands.white,), onPressed: () { setState(() {
            if(connection==true){
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) => HomeScreen(),
                ),
              );
            }
            else{
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) => HomeScreen(),
                ),
              );
            }
          }); },),
        ),
        Expanded(
          child: Text(
            widget.title ?? "",
            style: ThemeConstands.texttheme.headlineSmall
                ?.copyWith(color: Colorconstands.white),
          ),
        ),
        Container(
          alignment: Alignment.centerRight,
          margin:const EdgeInsets.symmetric(horizontal: 15),
          child: widget.changechild,
        )
      ],
    );
  }
}
