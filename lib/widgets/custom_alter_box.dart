import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../app/core/themes/color_app.dart';
import '../app/core/themes/themes.dart';
import '../app/modules/PaymentAndBillScreen/bloc/payment_option_bloc.dart';
import '../app/modules/PaymentAndBillScreen/view/selected_payment_option.dart';

class CustomAlert extends StatelessWidget {
  const CustomAlert({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 22, horizontal: 20),
        decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                  color: Colors.black.withOpacity(0.5),
                  blurRadius: 2,
                  offset: const Offset(0, 2))
            ],
            color: const Color.fromRGBO(241, 249, 255, 1),
            borderRadius: const BorderRadius.all(const Radius.circular(20))),
        width: 250,
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          Text(
            "Your Account has Expired!",
            textAlign: TextAlign.center,
            overflow: TextOverflow.clip,
            style: ThemeConstands.texttheme.headline5
                ?.copyWith(fontWeight: FontWeight.w500),
          ),
          const SizedBox(
            height: 30,
          ),
          const Text(
            "Excepteur sint occaecat cupidatat non proident",
            textAlign: TextAlign.center,
            overflow: TextOverflow.clip,
          ),
          const SizedBox(
            height: 30,
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 50,
            decoration: const BoxDecoration(
                color: Colorconstands.primaryColor,
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: MaterialButton(
              onPressed: () {
                BlocProvider.of<PaymentOptionBloc>(context)
                    .add(GetPaymentOptionEvent());
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => const SelectedPaymentOption(),
                  ),
                );
              },
              child: Text(
                "Pay Service Fees",
                style: ThemeConstands.texttheme.button
                    ?.copyWith(color: Colors.white),
              ),
            ),
          ),
        ]),
      ),
    );
  }
}
