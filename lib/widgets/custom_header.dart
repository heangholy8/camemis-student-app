import 'dart:async';

import 'package:camis_application_flutter/app/modules/PaymentAndBillScreen/view/payment_history.dart';
import 'package:camis_application_flutter/app/modules/home_screen/view/home_screen.dart';
import 'package:camis_application_flutter/app/routes/e.route.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import '../app/core/themes/color_app.dart';
import '../app/core/themes/themes.dart';

class CustomHeader extends StatefulWidget {
  String nameScreen = "";
  String? title;
  CustomHeader({Key? key, required this.title, this.nameScreen = ""})
      : super(key: key);

  @override
  State<CustomHeader> createState() => _CustomHeaderState();
}

class _CustomHeaderState extends State<CustomHeader> {
  StreamSubscription? stsub;
  bool connection = true;
  //=================
  @override
  void initState() {
    super.initState();
    //=============Check internet====================
    stsub = Connectivity().onConnectivityChanged.listen((event) {
      setState(() {
        connection = (event != ConnectivityResult.none);
      });
    });
   
   //=============Check internet====================
  }

  @override
  void dispose() {
    stsub?.cancel();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 18.0, bottom: 18.0),
      child: Row(
        children: [
          IconButton(
              onPressed: () {
                if (widget.nameScreen == "payment") {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const MoreScreen()));
                } else if (widget.nameScreen == "home") {
                 Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => HomeScreen(),
                    ),
                  );
                } else {
                  Navigator.of(context).pop();
                }
              },
              icon: const Icon(
                Icons.arrow_back_ios_new,
                color: Colorconstands.white,
                size: 25,
              )),
          const SizedBox(width: 12.0),
          Text(
            widget.title ?? "",
            style: ThemeConstands.texttheme.headlineSmall
                ?.copyWith(color: Colorconstands.white),
          ),
        ],
      ),
    );
  }
}
