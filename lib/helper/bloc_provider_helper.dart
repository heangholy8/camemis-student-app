import 'package:camis_application_flutter/app/modules/PaymentAndBillScreen/bloc/bloc/aba_deep_link_bloc.dart';
import 'package:camis_application_flutter/app/modules/PaymentAndBillScreen/bloc/bloc/check_payment_expire_bloc.dart';
import 'package:camis_application_flutter/app/modules/PaymentAndBillScreen/bloc/bloc/create_khqr_bloc.dart';
import 'package:camis_application_flutter/app/modules/PaymentAndBillScreen/bloc/check_transaction_payment/bloc/check_transaction_bloc.dart';
import 'package:camis_application_flutter/app/modules/PaymentAndBillScreen/bloc/current_payment_bloc.dart';
import 'package:camis_application_flutter/app/modules/account_confirm_screen/bloc/update_profile_bloc.dart';
import 'package:camis_application_flutter/app/modules/attendance_screen.dart/bloc/get_date_bloc.dart';
import 'package:camis_application_flutter/app/modules/attendance_screen.dart/bloc/get_schedule_bloc.dart';
import 'package:camis_application_flutter/app/modules/auth_screens/bloc/bloc/list_school_bloc.dart';
import 'package:camis_application_flutter/app/modules/time_line/testing_pagination/bloc/pagination_bloc.dart';
import 'package:camis_application_flutter/component/list_month/bloc/list_month_bloc.dart';
import 'package:camis_application_flutter/service/apis/attendance_schedule/get_date_in_month_api.dart';
import 'package:camis_application_flutter/service/apis/auth_api/get_list_school.dart';
import 'package:camis_application_flutter/service/apis/check_payment_transection/get_transaction_api.dart';
import 'package:camis_application_flutter/service/apis/check_update_api/check_update.dart';
import 'package:camis_application_flutter/service/apis/payment_api/get_current_payment.dart';
import 'package:camis_application_flutter/service/apis/payment_api/get_payment_deep_link_api.dart';
import 'package:camis_application_flutter/service/apis/payment_api/get_payment_khqr_api.dart';
import 'package:camis_application_flutter/service/apis/profile_user/bloc/get_profile_user_bloc.dart';
import 'package:camis_application_flutter/service/apis/profile_user/get_profile_user.dart';
import 'package:camis_application_flutter/service/apis/profile_user/update_profile_child.dart';
import 'package:camis_application_flutter/state_bloc/check_version_update/bloc/check_version_update_bloc.dart';
import '../app/modules/PaymentAndBillScreen/bloc/bloc/create_payment_bloc.dart';
import '../app/modules/check_attendance_screen/bloc/check_attendance_bloc.dart';
import '../app/modules/check_attendance_screen/bloc/post_attendance_bloc.dart';
import '../app/modules/child_list_screen/bloc/update_child_bloc.dart';
import '../app/modules/edit_profile/bloc/change_info_user_bloc.dart';
import '../app/modules/edit_profile/component/bloc/changepassword_bloc.dart';
import '../app/modules/guide_line/bloc/user_guideline_bloc.dart';
import '../app/modules/online_screen/bloc/online_bloc.dart';
import '../app/modules/request_absent_screen/view/by_day/bloc/permission_by_day_bloc.dart';
import '../app/modules/teacher_list_screen/bloc/teacher_list_bloc.dart';
import '../app/modules/teaching_learning_screen/bloc/teaching_bloc.dart';
import '../app/modules/time_line/class_time_line/bloc/class_time_line_bloc.dart';
import '../service/apis/attendance_schedule/get_schedule_api.dart';
import '../service/apis/change_info_apis/update_info_user_api.dart';
import '../service/apis/check_attendance_api/get_list_student_attendance.dart';
import '../service/apis/create_payment/create_payment_api.dart';
import '../service/apis/payment_api/check_payment_expire.dart';
import '../service/apis/permission_request/post_permission_date.dart';
import '../service/apis/profile_user/edit_profile_user_api.dart';
import '../service/apis/teacher_list/teacher_list_api.dart';
import 'route.export.dart';

List<BlocProvider> _listBlocProvider = [
  BlocProvider<AuthBloc>(
    create: (context) => AuthBloc(
      authApi: AuthApi(),
    ),
  ),
  BlocProvider<ChangeInfoUserBloc>(
      create: (context) => ChangeInfoUserBloc(
          changeInforUser: UpdateUserInfoApi(),
          changePassword: ChangeUserInformationApi())),
  BlocProvider<PermissionRequestHistoryBloc>(
      create: (context) => PermissionRequestHistoryBloc(
          getPermissionRequestApi: GetPermissionRequestApi())
        ..add(const PermissionrequestHistoryEvent())),
  BlocProvider<PermissionRequestOngoingBloc>(
      create: (context) => PermissionRequestOngoingBloc(
          getPermissionRequestApi: GetPermissionRequestApi())
        ..add(const PermissionrequestOngoingEvent())),
  BlocProvider<PermissionRequestDetailBloc>(
      create: (context) => PermissionRequestDetailBloc(
          getPermissionRequestDetailApi: GetPermissionRequestApi())),
  //BlocProvider<SchoolTimeLineBloc>(create: (context) => SchoolTimeLineBloc(getSchooltimelineapi: GetSchoolTimeLineApi()).add(const SchoolTimeLineFetchEvent())),
  BlocProvider<SchoolTimeLineDetailBloc>(
      create: (context) => SchoolTimeLineDetailBloc(
          getSchooltimelinedetailapi: GetSchoolTimeLineApi())),
  BlocProvider<ChangeChildBloc>(
      create: (context) => ChangeChildBloc(childrenApi: GetChildrenApi())),
  BlocProvider<MonthlyBloc>(
      create: (context) => MonthlyBloc(currenApi: GetChildrenApi())),
  BlocProvider<MonthlyResultBloc>(
      create: (context) => MonthlyResultBloc(currenApi: MonthlyResultApi())),
  BlocProvider<TermResultBloc>(
      create: (context) => TermResultBloc(currenFirstApi: TermResultApi())),
  BlocProvider<YearResultBloc>(
      create: (context) => YearResultBloc(currenApi: YearResultApi())),
  BlocProvider<DetailResultBloc>(
      create: (context) => DetailResultBloc(currenApi: DetailResultApi())),
  // BlocProvider<FirstTermResultBloc>(
  //     create: (context) =>
  //         FirstTermResultBloc(currenFirstApi: TermResultApi())),
  // BlocProvider<SecondTermResultBloc>(
  //     create: (context) =>
  //         SecondTermResultBloc(currenFirstApi: TermResultApi())),
  BlocProvider<RequestReasonBloc>(
    create: ((context) => RequestReasonBloc(getadmissionApi: GetadmissionApi())
      ..add(const RequestReasonEvent())),
  ),
  BlocProvider<TeachingBloc>(
      create: (context) => TeachingBloc(teachingApi: TeachingApi())),
  BlocProvider<HomeworkBloc>(
      create: (context) => HomeworkBloc(teachingApi: TeachingApi())),
  BlocProvider<PaymentBlocBloc>(
      create: (context) => PaymentBlocBloc(currenApi: PaymentHistoryApi())),
  BlocProvider<PaymentOptionBloc>(
      create: (context) => PaymentOptionBloc(currenApi: PaymentOptionApi())),
  BlocProvider<PaymentDetailBloc>(
      create: (context) => PaymentDetailBloc(currenApi: PaymentDetailApi())),
  BlocProvider<ClassTimeLineBloc>(
      create: (context) =>
          ClassTimeLineBloc(getSchooltimelineapi: GetSchoolTimeLineApi())),
  BlocProvider<ClassTimeLineDetailBloc>(
      create: (context) => ClassTimeLineDetailBloc(
          getClasstimelinedetailapi: GetSchoolTimeLineApi())),
  BlocProvider<CreatePaymentBloc>(
      create: (context) => CreatePaymentBloc(currenApi: CreatePaymentApi())),
  BlocProvider<AbaDeepLinkBloc>(
      create: (context) => AbaDeepLinkBloc(deepLinkApi: GetABADeepLinkApi())),
      BlocProvider<CreateKhqrBloc>(
      create: (context) => CreateKhqrBloc(khqrApi: GetKHQRApi())),
  BlocProvider<OnlineBloc>(create: ((context) => OnlineBloc())),
  BlocProvider<PermissionByDayBloc>(
    create: (context) => PermissionByDayBloc(permission: PostPermission()),
  ),
  // BlocProvider<PermissionBySchaduleBloc>(
  //   create: (context) => PermissionBySchaduleBloc(permission: PostPermission()),
  // ),
  BlocProvider<GetProfileUserBloc>(
    create: (context) =>
        GetProfileUserBloc(getprofileuserapi: GetProfileUserApi()),
  ),
  BlocProvider<SchoolSocialBloc>(
    create: (context) => SchoolSocialBloc(GetSchoolTimeLineApi()),
  ),
  BlocProvider<GetDateBloc>(
    create: (context) => GetDateBloc(listDate: GetListDateApi()),
  ),
  BlocProvider<GetScheduleBloc>(
    create: (context) => GetScheduleBloc(mySchedule: GetScheduleApi()),
  ),
  BlocProvider<SchoolSocialBloc>(
    create: (context) => SchoolSocialBloc(GetSchoolTimeLineApi()),
  ),
  BlocProvider<UserGuidelineBloc>(
    create: (context) => UserGuidelineBloc(
      getUserGuidelineapi: GetProfileUserApi(),
    ),
  ),
  BlocProvider<ChangepasswordBloc>(
    create: ((context) => ChangepasswordBloc(api: ChangeUserInformationApi())),
  ),
  BlocProvider<TeacherListBloc>(
    create: (context) => TeacherListBloc(teacherListApi: GetTeacherListApi(),
    ),
  ),
  BlocProvider<ListSchoolBloc>(
    create: (context) => ListSchoolBloc(getListSchool: GetListSchoolApi(),
    ),
  ),
  BlocProvider<UpdateProfileBloc>(
    create: (context) => UpdateProfileBloc(upDateProfile: UpdateProfileUserApi(),
    ),
  ),
  BlocProvider<UpdateChildBloc>(
    create: (context) => UpdateChildBloc( upDateChildProfifle: UpdateProfileChildApi(),
    ),
  ),
  BlocProvider<CurrentPaymentBloc>(
    create: (context) => CurrentPaymentBloc(currentPayment: PaymentCurrentApi(),
    ),
  ),
  BlocProvider<CheckTransactionBloc>(
    create: (context) => CheckTransactionBloc(transactionApi: GetTransactionApi(),
    ),
  ),
  BlocProvider<CheckPaymentExpireBloc>(
    create: (context) => CheckPaymentExpireBloc(paymentCheckApi: PaymentExpireApi(),
    ),
  ),
  BlocProvider<CheckVersionUpdateBloc>(
    create: (context) => CheckVersionUpdateBloc(checkUpdateVersion: CheckUpdateVersionApi(),
    ),
  ),
  BlocProvider<ListMonthBloc>(
    create: (context) => ListMonthBloc(currenApi: GetChildrenApi(),
    ),
  ),
  BlocProvider<CheckAttendanceBloc>(create: (context) => CheckAttendanceBloc(getCheckStudentAttendanceList: GetCheckStudentAttendanceList(),),),
  BlocProvider<PostAttendanceBloc>(create: (context) => PostAttendanceBloc(),),
  BlocProvider<PaginationBloc>(create: (context)=> PaginationBloc(getSchooltimelinedetailapi: GetSchoolTimeLineApi())),
  // BlocProvider<Testing02Bloc>(create: (context)=> Testing02Bloc(getSchoolTimeLineApi: GetSchoolTimeLineApi())),
  
];

List<BlocProvider> get listBlocProvider => _listBlocProvider;
