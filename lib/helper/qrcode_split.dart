import '../app/modules/auth_screens/e_school_code.dart';

void qrCodeSplit(String code, context) {
  var newCode = code.split(";");
  var schoolId = newCode[0].split(":");
  var loginName = newCode[1].split(":");
  var password = newCode[2].split(":");
  debugPrint(
    schoolId[1].toString() + " " + loginName[1].toString() + " " + password[1],
  );
  BlocProvider.of<AuthBloc>(context).add(
    SignInRequested(
      schoolCode: schoolId[1].toString(),
      loginName: loginName[1].toString(),
      password: password[1].toString(),
    ),
  );
}
