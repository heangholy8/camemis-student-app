import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
    OverlayEntry? _overlayEntry ;
 void showContextMenu(
      BuildContext context, PdfTextSelectionChangedDetails details,PdfViewerController pdfviewController) {
    final OverlayState? _overlayState = Overlay.of(context);
    _overlayEntry = OverlayEntry(
      builder: (context) => Positioned(
        top: details.globalSelectedRegion!.center.dy - 55,
        left: details.globalSelectedRegion?.bottomLeft.dx,
        child: ElevatedButton(
          child: const Text(
            'Copy',
            style: TextStyle(fontSize: 17),
          ),
          onPressed: () {
            Clipboard.setData(
              ClipboardData(
                text: details.selectedText,

              ),
            );
            pdfviewController.clearSelection();
          },
          // color: Colors.white,
          // elevation: 10,
        ),
      ),
    );
    _overlayState?.insert(_overlayEntry!);
  }