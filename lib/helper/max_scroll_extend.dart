
import '../app/routes/e.route.dart';

void scrollDown({required ScrollController controller}) {
  controller.animateTo(
    controller.position.maxScrollExtent,
    duration: Duration(seconds: 2),
    curve: Curves.fastOutSlowIn,
  );
}
