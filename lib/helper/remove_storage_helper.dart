import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../app/modules/auth_screens/views/option_login_screen.dart';

class RemoveStorageHelper {
  removeToken(context) async {
    await DefaultCacheManager().emptyCache();
    try {
      SharedPreferences _pref = await SharedPreferences.getInstance();
        _pref.remove("jsonToken");
       // _pref.remove("LeadingSuccess");
       //_pref.remove("GuidSuccess");
       Navigator.pushAndRemoveUntil(
            context,
            PageRouteBuilder(
            pageBuilder: (context, animation1, animation2) => OptionLoginScreen(),
            transitionDuration: Duration.zero,
            reverseTransitionDuration: Duration.zero,
        ),
            (route) => false);
       print("Token was removed from cache");
    } catch (e) {
      print("Error Remove token" + e.toString());
    }
  }
}
