/**
 * Automatically generated file. DO NOT MODIFY
 */
package ai.picovoice.flutter.voiceprocessor;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "ai.picovoice.flutter.voiceprocessor";
  public static final String BUILD_TYPE = "debug";
}
