/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.glidersi.flutter_audio_rec.flutter_audio_rec;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.glidersi.flutter_audio_rec.flutter_audio_rec";
  public static final String BUILD_TYPE = "debug";
}
